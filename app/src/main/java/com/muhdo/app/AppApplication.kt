package com.muhdo.app

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.app.Application
import android.content.Context
import android.content.DialogInterface
import android.provider.Settings
import android.util.Log
import android.view.WindowManager
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.FirebaseApp
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import com.muhdo.app.apiModel.mealModel.MealQuestionnaireModel
import com.muhdo.app.apiModel.v3.firebase.ForceUpdateEvent
import com.muhdo.app.helper.dagger.component.AppComponent
import com.muhdo.app.utils.PreferenceConnector
import com.muhdo.app.utils.v3.TempUtil
import org.greenrobot.eventbus.EventBus


class AppApplication : Application() {


    lateinit var appComponent: AppComponent
    var mealModel = MealQuestionnaireModel()
    var navigationhashMap = LinkedHashMap<String, String>()
    var queanshashMap = LinkedHashMap<String, Any>()
    var answerhashMap = LinkedHashMap<String, Any>()
    val TAG = "AppApplication"
    override fun onCreate() {
        super.onCreate()
        //  Stetho.initializeWithDefaults(this)
        signIn("android@muhdo.com", "muhdo123!@#@")

    }

    fun signIn(email: String, password: String) {
        var fbAuth = FirebaseAuth.getInstance()

        fbAuth.signInWithEmailAndPassword(email, password)
            .addOnCompleteListener(OnCompleteListener<AuthResult> { task ->
                if (task.isSuccessful) {
                    /*var intent = Intent(this, LoggedInActivity::class.java)
                    intent.putExtra("id", fbAuth.currentUser?.email)
                    startActivity(intent)*/
                    readFirebaseDatabase()

                } else {

                }
            })

    }


    fun readFirebaseDatabase() {
        if (!FirebaseApp.getApps(this).isEmpty()) {
            FirebaseDatabase.getInstance().setPersistenceEnabled(true)
        }
        val firebaseDatabase = FirebaseDatabase.getInstance()
        val firebaseReference: DatabaseReference = firebaseDatabase.getReference("Versions")

        //offline
        val versionReference = FirebaseDatabase.getInstance().getReference("Versions")
        versionReference.keepSynced(true)
        //

        val postListener = object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                var baseUrl: String? = ""
                var version: String? = ""
                var forceToDownload: Boolean? = false
                var forceToLogout: Boolean? = false
                var playStoreLink: String? = ""
                val VERSION = "Version" + getVersionName()!!.replace(".", "_");
                if (dataSnapshot.hasChild("Version_Android")) {
                    baseUrl =
                        dataSnapshot.child("Version_Android").child(VERSION).child("base_url")
                            .getValue(String::class.java)


                    version =
                        dataSnapshot.child("Version_Android").child("version")
                            .getValue(String::class.java)
                    playStoreLink = dataSnapshot.child("Version_Android").child("play_store_link")
                        .getValue(String::class.java)
                    forceToDownload =
                        dataSnapshot.child("Version_Android").child(VERSION)
                            .child("force_to_download")
                            .getValue(Boolean::class.java)
                    forceToLogout =
                        dataSnapshot.child("Version_Android").child(VERSION)
                            .child("force_to_logout")
                            .getValue(Boolean::class.java)
                    Log.w(
                        TAG, "BaseURL:" + baseUrl + "\n" +
                                "version:" + version + "\n" +
                                "forceToDownload:" + forceToDownload + "\n" +
                                "forceToLogout:" + forceToLogout + "\n" +
                                "playStoreLink:" + playStoreLink
                    )
                    if (playStoreLink == null) {
                        playStoreLink =
                            "https://play.google.com/store/apps/details?id=com.muhdo.app"
                    }
                    if (baseUrl != null)
                        PreferenceConnector.writeString(
                            applicationContext,
                            PreferenceConnector.BASE_URL,
                            baseUrl!!
                        )
                    checkVersion(version, playStoreLink, forceToDownload, forceToLogout)
                }
            }

            override fun onCancelled(databaseError: DatabaseError) {
                // Getting Post failed, log a message
                Log.w(TAG, "loadPost:onCancelled", databaseError.toException())
            }
        }
        firebaseReference.addValueEventListener(postListener)
    }

    fun checkVersion(
        updatedVersion: String?,
        playStoreLink: String?,
        forceToDownload: Boolean?,
        forceToLogout: Boolean?
    ) {

        val forceUpdateEvent: ForceUpdateEvent = ForceUpdateEvent()
        forceUpdateEvent.url = playStoreLink

        val versionName = getVersionName()
        if (forceToLogout != null) {
            TempUtil.log("force logout inside", forceToDownload.toString())
            PreferenceConnector.writeBoolean(
                applicationContext,
                PreferenceConnector.FORCE_LOGOUT_STATUS,
                forceToLogout
            )
        }
        if (/*versionName?.trim().equals(updatedVersion?.trim()) && */forceToDownload!!) {
            val forceUpdateEvent: ForceUpdateEvent = ForceUpdateEvent()
            forceUpdateEvent.url = playStoreLink
            EventBus.getDefault().post(forceUpdateEvent)
            PreferenceConnector.writeBoolean(
                applicationContext,
                PreferenceConnector.FORCE_UPDATE_STATUS,
                true
            )

            PreferenceConnector.writeString(
                applicationContext,
                PreferenceConnector.PLAY_STORE_URL,
                playStoreLink!!
            )
        } else {
            PreferenceConnector.writeBoolean(
                applicationContext,
                PreferenceConnector.FORCE_UPDATE_STATUS,
                false
            )
        }
        EventBus.getDefault().post(forceUpdateEvent)
        Log.e(TAG, "Version: " + versionName + " \nUpdated Version:" + updatedVersion)
    }

    fun getVersionName(): String? {
        /* try {
             val pInfo = applicationContext().packageManager
                 .getPackageInfo(packageName, 0)
             return pInfo.versionName
         } catch (e: PackageManager.NameNotFoundException) {
             e.printStackTrace()
             return null
         }*/
        return BuildConfig.VERSION_NAME.replace("_", ".");


    }

    fun showAlertDialog(context: Context) {
        /** define onClickListener for dialog  */
        val listener = object : DialogInterface.OnClickListener {
            override fun onClick(dialog: DialogInterface, which: Int) {
                // do some stuff eg: context.onCreate(super)
            }
        }

        /** create builder for dialog  */
        val builder = AlertDialog.Builder(context)
            .setCancelable(false)
            .setMessage("Messag...")
            .setTitle("Title")
            .setPositiveButton("OK", listener)
        /** create dialog & set builder on it  */
        val dialog = builder.create()
        /** this required special permission but u can use aplication context  */
        dialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT)
        /** show dialog  */
        dialog.show()
    }

    companion object {
        var isFuture = false
        private var mHmapheight = LinkedHashMap<String, String>()
        private var instance: AppApplication? = null
        fun applicationContext(): AppApplication {
            if (instance == null)
                instance = AppApplication()

            return instance!!
        }

        @SuppressLint("HardwareIds")
        fun getAndroidId(context: Context): String {
            return Settings.Secure.getString(
                context.contentResolver,
                Settings.Secure.ANDROID_ID
            )
        }

        fun setHeightMap(key: String, value: String) {
            val removeDot = value.split(".")
            if (removeDot.isNotEmpty() && removeDot.size > 1) {
                mHmapheight[key] = removeDot[0]
            } else {
                mHmapheight[key] = value
            }
        }

        fun getHeightMap(): HashMap<String, String> {
            return mHmapheight
        }
    }

    fun applicationContext(): AppApplication {
        if (instance == null)
            instance = AppApplication()

        return instance!!
    }
}