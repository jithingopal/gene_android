package com.muhdo.app.data

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty


@Entity(tableName = "MealData")
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
class MealData {
    constructor(
        _id: Int,
        goal: String,
        isVegetarian: String,
        isVegan: String,
        isFish: String,
        height: String,
        weight: String,
        weightMeasure: String,
        heightMeasure: String,
        alcoholFree: String,
        activity: String,
        mealPerDay: String
    ) {
        this._id = _id
        this.goal = goal
        this.isVegetarian = isVegetarian
        this.isVegan = isVegan
        this.isFish = isFish
        this.height = height
        this.weight = weight
        this.weightMeasure = weightMeasure
        this.heightMeasure = heightMeasure
        this.alcoholFree = alcoholFree
        this.activity = activity
        this.mealPerDay = mealPerDay
    }

    @PrimaryKey(autoGenerate = true)
    var _id: Int = 0

    @ColumnInfo(name = "goal")
    @JsonProperty("goal")
    var goal: String = ""

    @ColumnInfo(name = "isVegetarian")
    @JsonProperty("isVegetarian")
    var isVegetarian: String = ""

    @ColumnInfo(name = "isVegan")
    @JsonProperty("isVegan")
    var isVegan: String = ""

    @ColumnInfo(name = "isFish")
    @JsonProperty("isFish")
    var isFish: String = ""



    @ColumnInfo(name = "height")
    @JsonProperty("height")
    var height: String = ""

    @ColumnInfo(name = "weight")
    @JsonProperty("weight")
    var weight: String = ""

    @ColumnInfo(name = "weightMeasure")
    @JsonProperty("weightMeasure")
    var weightMeasure: String = ""

    @ColumnInfo(name = "heightMeasure")
    @JsonProperty("heightMeasure")
    var heightMeasure: String = ""

    @ColumnInfo(name = "alcoholFree")
    @JsonProperty("alcoholFree")
    var alcoholFree: String = ""

    @ColumnInfo(name = "activity")
    @JsonProperty("activity")
    var activity: String = ""

    @ColumnInfo(name = "mealPerDay")
    @JsonProperty("mealPerDay")
    var mealPerDay: String = ""

    @Ignore
    @JsonIgnore
    val additionalProperties = HashMap<String, Any>()


}