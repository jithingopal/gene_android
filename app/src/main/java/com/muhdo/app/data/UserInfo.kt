package com.muhdo.app.data

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty

@Entity(tableName = "UserInfo")
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
class UserInfo {

    @PrimaryKey(autoGenerate = true)
    var _id: Int = 0

    @ColumnInfo(name = "name")
    @JsonProperty("name")
    var name: String = ""

    @ColumnInfo(name = "job")
    @JsonProperty("job")
    var job: String = ""

    constructor(_id: Int, name: String, job: String, id: String, createdAt: String) {
        this._id = _id
        this.name = name
        this.job = job
        this.id = id
        this.createdAt = createdAt
    }

    @ColumnInfo(name = "id")
    @JsonProperty("id")
    var id: String = ""

    @ColumnInfo(name = "createdAt")
    @JsonProperty("createdAt")
    var createdAt: String = ""

    @Ignore
    @JsonIgnore
    val additionalProperties = HashMap<String, Any>()

}