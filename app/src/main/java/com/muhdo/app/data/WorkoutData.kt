package com.muhdo.app.data

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty


@Entity(tableName = "WorkoutData")
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
class WorkoutData {

//    constructor(
//        _id: Int,
//        new: String,
//        new1: String,
//        new2: String,
//        new3: String,
//        new4: String,
//        new5: String,
//        new6: String,
//        new7: String,
//        new8: String
//    ) {
//        this._id = _id
//        this.new = new
//        this.new1 = new1
//        this.new2 = new2
//        this.new3 = new3
//        this.new4 = new4
//        this.new5 = new5
//        this.new6 = new6
//        this.new7 = new7
//        this.new8 = new8
//    }

    @PrimaryKey(autoGenerate = true)
    var _id: Int = 0

    @ColumnInfo(name = "new")
    @JsonProperty("new")
    var new: String = ""
//
    @ColumnInfo(name = "new1")
    @JsonProperty("new1")
    var new1: String = ""

    @ColumnInfo(name = "new2")
    @JsonProperty("new2")
    var new2: String = ""

    @ColumnInfo(name = "new3")
    @JsonProperty("new3")
    var new3: String = ""

    @ColumnInfo(name = "new4")
    @JsonProperty("new4")
    var new4: String = ""


    @ColumnInfo(name = "new5")
    @JsonProperty("new5")
    var new5: String = ""

    @ColumnInfo(name = "new6")
    @JsonProperty("new6")
    var new6: String = ""
//
    @ColumnInfo(name = "new7")
    @JsonProperty("new7")
    var new7: String = ""

    @ColumnInfo(name = "new8")
    @JsonProperty("new8")
    var new8: String = ""
//
//
//    constructor(
//        _id: Int,
//        new: String,
//        new1: String,
//        new2: String,
//        new3: String,
//        new4: String,
//        new5: String,
//        new6: String,
//        new7: String,
//        new8: String
//    ) {
//        this._id = _id
//        this.new = new
//        this.new1 = new1
//        this.new2 = new2
//        this.new3 = new3
//        this.new4 = new4
//        this.new5 = new5
//        this.new6 = new6
//        this.new7 = new7
//        this.new8 = new8
//    }
//    @ColumnInfo(name = "trainingPerWeek")
//    @JsonProperty("trainingPerWeek")
//    var trainingPerWeek: String = ""
//
//    @ColumnInfo(name = "trainingMin")
//    @JsonProperty("trainingMin")
//    var trainingMin: String = ""
//
//    @ColumnInfo(name = "trainingMax")
//    @JsonProperty("trainingMax")
//    var trainingMax: String = ""
//
//    @ColumnInfo(name = "equipment")
//    @JsonProperty("equipment")
//    var equipment: String = ""
//
//    @ColumnInfo(name = "experience")
//    @JsonProperty("experience")
//    var experience: String = ""
//
//    @ColumnInfo(name = "isAgainstExercise")
//    @JsonProperty("isAgainstExercise")
//    var isAgainstExercise: String = ""
//
//    @ColumnInfo(name = "ligamentPain")
//    @JsonProperty("ligamentPain")
//    var ligamentPain: String = ""
//
//    @ColumnInfo(name = "isPregnant")
//    @JsonProperty("isPregnant")
//    var isPregnant: String = ""
//
//
//    @ColumnInfo(name = "medicalHistory")
//    @JsonProperty("medicalHistory")
//    var medicalHistory: MutableList<String>? = null


//    @ColumnInfo(name = "workoutGoal")
//    @JsonProperty("workoutGoal")
//    var workoutGoal: String = ""
//
//    @ColumnInfo(name = "trainingPerWeek")
//    @JsonProperty("trainingPerWeek")
//    var trainingPerWeek: String = ""
//
//    @ColumnInfo(name = "trainingMin")
//    @JsonProperty("trainingMin")
//    var trainingMin: String = ""
//
//    @ColumnInfo(name = "trainingMax")
//    @JsonProperty("trainingMax")
//    var trainingMax: String = ""
//
//
//
//    @ColumnInfo(name = "equipment")
//    @JsonProperty("equipment")
//    var equipment: String = ""
//
//    @ColumnInfo(name = "experience")
//    @JsonProperty("experience")
//    var experience: String = ""
//
//    @ColumnInfo(name = "isAgainstExercise")
//    @JsonProperty("isAgainstExercise")
//    var isAgainstExercise: String = ""
//
//    @ColumnInfo(name = "ligamentPain")
//    @JsonProperty("ligamentPain")
//    var ligamentPain: String = ""
//
//    @ColumnInfo(name = "isPregnant")
//    @JsonProperty("isPregnant")
//    var isPregnant: String = ""



    /* @ColumnInfo(name = "goal")
     @JsonProperty("goal")
     var goal: String = ""

     @ColumnInfo(name = "trainingPerWeek")
     @JsonProperty("trainingPerWeek")
     var trainingPerWeek: String = ""

     @ColumnInfo(name = "trainingMin")
     @JsonProperty("trainingMin")
     var trainingMin: String = ""

     @ColumnInfo(name = "trainingMax")
     @JsonProperty("trainingMax")
     var trainingMax: String = ""

     @ColumnInfo(name = "equipment")
     @JsonProperty("equipment")
     var equipment: String = ""

     @ColumnInfo(name = "experience")
     @JsonProperty("experience")
     var experience: String = ""

     @ColumnInfo(name = "isAgainstExercise")
     @JsonProperty("isAgainstExercise")
     var isAgainstExercise: String = ""

     @ColumnInfo(name = "ligamentPain")
     @JsonProperty("ligamentPain")
     var ligamentPain: String = ""

     @ColumnInfo(name = "isPregnant")
     @JsonProperty("isPregnant")
     var isPregnant: String = ""


     @ColumnInfo(name = "medicalHistory")
     @JsonProperty("medicalHistory")
     var medicalHistory: MutableList<String>? = null

     constructor(
         _id: Int,
         goal: String,
         trainingPerWeek: String,
         trainingMin: String,
         trainingMax: String,
         equipment: String,
         experience: String,
         isAgainstExercise: String,
         ligamentPain: String,
         isPregnant: String,
         medicalHistory: MutableList<String>?
     ) {
         this._id = _id
         this.goal = goal
         this.trainingPerWeek = trainingPerWeek
         this.trainingMin = trainingMin
         this.trainingMax = trainingMax
         this.equipment = equipment
         this.experience = experience
         this.isAgainstExercise = isAgainstExercise
         this.ligamentPain = ligamentPain
         this.isPregnant = isPregnant
         this.medicalHistory = medicalHistory
     }*/

    @Ignore
    @JsonIgnore
    val additionalProperties = HashMap<String, Any>()
}