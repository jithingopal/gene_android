package com.muhdo.app.room

import androidx.room.*
import com.muhdo.app.data.WorkoutData

@Dao
interface WorkoutDAO {

    @Insert
    fun insert(info: WorkoutData)

    @Delete
    fun delete(info: WorkoutData)
//
    @Update
    fun update(info: WorkoutData)

    @Query("Select * from WorkoutData")
    fun getWorkoutData(): WorkoutData

}