package com.muhdo.app.room

import androidx.room.*
import com.muhdo.app.data.MealData

@Dao
interface MealDAO {

    @Insert
    fun insert(info: MealData)

    @Delete
    fun delete(info: MealData)
//
    @Update
    fun update(info: MealData)

    @Query("Select * from MealData")
    fun getMealData(): MealData

}