package com.muhdo.app.room

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.muhdo.app.R
import com.muhdo.app.data.MealData
import com.muhdo.app.data.UserInfo
import com.muhdo.app.data.WorkoutData

@Database(entities = arrayOf(UserInfo::class, MealData::class, WorkoutData::class), version = 1, exportSchema = false)
//@Database(entities = arrayOf(UserInfo::class), version = 1, exportSchema = false)

abstract class AppDatabase : RoomDatabase() {

    abstract fun userInfo(): UserInfoDAO
    abstract fun mealData(): MealDAO
    abstract fun workoutData(): WorkoutDAO

    companion object {

        private var INSTANCE: AppDatabase? = null

        fun getInstance(context: Context): AppDatabase? {
            if (INSTANCE == null) {
                INSTANCE = Room.databaseBuilder(
                    context.applicationContext,
                    AppDatabase::class.java,
                    context.getString(R.string.db_name)
                )
                    .allowMainThreadQueries()
                    .build()
            }
            return INSTANCE
        }

        fun destroyInstance() {
            INSTANCE = null
        }

    }

}