package com.muhdo.app.fragment.v3.account.dialog_fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.muhdo.app.R
import com.muhdo.app.adapter.v3.AdditionalKitIdRecyAdpter
import com.muhdo.app.apiModel.v3.KitID
import com.muhdo.app.databinding.V3FragmentDialogAdditionalKitIdBinding

class AdditionalKitIDListDialogFragment : DialogFragment() {
    companion object newInstance {
        fun newInstance(): AdditionalKitIDListDialogFragment {
            val f =
                AdditionalKitIDListDialogFragment()
            return f
        }
    }

    private lateinit var binding: V3FragmentDialogAdditionalKitIdBinding
    lateinit var kitIdList: MutableList<KitID>
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view =
            inflater.inflate(R.layout.v3_fragment_dialog_additional_kit_id, container, false)
        binding = V3FragmentDialogAdditionalKitIdBinding.bind(view)
        binding.imgClose.setOnClickListener {
            dismiss()
        }
        var mAdapter = AdditionalKitIdRecyAdpter(activity!!, kitIdList)
        binding.recyclerView.addItemDecoration(
            DividerItemDecoration(
                context,
                DividerItemDecoration.VERTICAL
            )
        )
        binding.recyclerView.layoutManager =
            LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        binding.recyclerView.adapter = mAdapter
        return view

    }

    override fun onStart() {
        super.onStart()
        dialog?.let {
            val width = ViewGroup.LayoutParams.MATCH_PARENT
            val height = ViewGroup.LayoutParams.MATCH_PARENT
            it.window?.setLayout(width, height)
        }
    }

    fun setKitIDList(kitIdList: MutableList<KitID>) {
        this.kitIdList = kitIdList
    }


}