package com.muhdo.app.fragment

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.annotation.SuppressLint
import android.os.Build.VERSION
import android.os.Build.VERSION_CODES
import android.os.Bundle
import android.os.Handler
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.view.animation.AnticipateInterpolator
import android.view.animation.OvershootInterpolator
import android.widget.AdapterView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import com.muhdo.app.R
import com.muhdo.app.adapter.CustomSpinnerAdapter
import com.muhdo.app.apiModel.keyData.GenericProfileData
import com.muhdo.app.apiModel.keyData.GenericProfileModel
import com.muhdo.app.apiModel.keyData.SectionByCategoryData
import com.muhdo.app.apiModel.keyData.SectionByCategoryModel
import com.muhdo.app.databinding.FragmentKeydata2Binding
import com.muhdo.app.repository.ApiUtilis
import com.muhdo.app.repository.ErrorModel
import com.muhdo.app.repository.NetworkManager
import com.muhdo.app.repository.ServiceListener
import com.muhdo.app.utils.AnimatorUtils
import com.muhdo.app.utils.v3.Constants
import com.timingsystemkotlin.backuptimingsystem.Utility
import org.jetbrains.anko.toast
import org.json.JSONObject
import java.util.*

class ResultKeyDataFragment : Fragment() {
    internal lateinit var binding: FragmentKeydata2Binding
    private var modeID: String? = ""
    private var genoTypeId: String? = ""
    private var categoryId1: String = ""
    private var newCategoryId1: String = ""
    private val genericList: MutableList<String> = ArrayList()
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_keydata2, container, false)
        binding = FragmentKeydata2Binding.bind(view)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        genoTypeId = this.arguments!!.getString(Constants.SECTION_ID)
        modeID = this.arguments!!.getString(Constants.MODE_ID)
        System.out.println("genoTypeId----> " + genoTypeId + "modeID-----> " + modeID)
        categoryId1 = Utility.categoryID
        System.out.println("categoryId==>  $categoryId1")
        if (genoTypeId != null) {
            getGenericProfile(genoTypeId!!)
        } else {
            Utility.displayShortSnackBar(binding.parentLayout, "Something went wrong")
        }

        binding.fab.setOnClickListener {
            it.isSelected = !it.isSelected
            if (it.isSelected) {
                showMenu()
            } else {
                hideMenu()
            }
        }

        binding.imgBackToTop.setOnClickListener {
            val handler = Handler()
            handler.postDelayed({
                // for autoLogin user
                binding.scrollView.fullScroll(View.FOCUS_UP)
            }, 3000)

        }
        binding.arcLayout.getChildAt(0).setOnClickListener {
            modeID = "5c8c96a79c54381add6884dd"
            getKeyData(newCategoryId1, genoTypeId!!)
            hideMenu()
            binding.fab.setImageResource(R.drawable.fit_for_life_icon)
            context?.toast("Working")
        }
        binding.arcLayout.getChildAt(1).setOnClickListener {
            modeID = "5c8c96a79c54381add6884dc"
            getKeyData(newCategoryId1, genoTypeId!!)
            hideMenu()
            binding.fab.setImageResource(R.drawable.fat_loss_icon)
        }
        binding.arcLayout.getChildAt(2).setOnClickListener {
            modeID = "5c8c96a79c54381add6884db"
            getKeyData(newCategoryId1, genoTypeId!!)
            hideMenu()
            binding.fab.setImageResource(R.drawable.muscle_build_icon)
        }
        binding.arcLayout.getChildAt(3).setOnClickListener {
            modeID = "5c8c96a79c54381add6884da"
            getKeyData(newCategoryId1, genoTypeId!!)
            hideMenu()
            binding.fab.setImageResource(R.drawable.fitness_icon)
        }
    }


    @RequiresApi(VERSION_CODES.HONEYCOMB)
    private fun createShowItemAnimator(item: View): Animator {
        val dx = binding.fab.x - item.x
        val dy = binding.fab.y - item.y

        item.rotation = 0f
        item.translationX = dx
        item.translationY = dy

        return ObjectAnimator.ofPropertyValuesHolder(
            item,
            AnimatorUtils.rotation(0f, 720f),
            AnimatorUtils.translationX(dx, 0f),
            AnimatorUtils.translationY(dy, 0f)
        )
    }

    @SuppressLint("ObsoleteSdkInt")
    @RequiresApi(VERSION_CODES.HONEYCOMB)
    private fun createHideItemAnimator(item: View): Animator {
        val dx = binding.fab.x - item.x
        val dy = binding.fab.y - item.y

        val anim = ObjectAnimator.ofPropertyValuesHolder(
            item,
            AnimatorUtils.rotation(720f, 0f),
            AnimatorUtils.translationX(0f, dx),
            AnimatorUtils.translationY(0f, dy)
        )

        if (VERSION.SDK_INT >= VERSION_CODES.HONEYCOMB) {
            anim.addListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator) {
                    super.onAnimationEnd(animation)
                    item.translationX = 0f
                    item.translationY = 0f
                }
            })
        }
        return anim
    }

    @SuppressLint("ObsoleteSdkInt")
    @RequiresApi(VERSION_CODES.HONEYCOMB)
    private fun showMenu() {
        val animList = ArrayList<Animator>()
        var i = 0
        val len = binding.arcLayout.childCount
        while (i < len) {
            animList.add(createShowItemAnimator(binding.arcLayout.getChildAt(i)))
            i++
        }
        val animSet = if (VERSION.SDK_INT >= VERSION_CODES.HONEYCOMB) {
            AnimatorSet()
        } else {
            TODO("VERSION.SDK_INT < HONEYCOMB")
        }
        if (VERSION.SDK_INT >= VERSION_CODES.HONEYCOMB) {
            animSet.duration = 400
        }
        if (VERSION.SDK_INT >= VERSION_CODES.HONEYCOMB) {
            animSet.interpolator = OvershootInterpolator()
        }
        if (VERSION.SDK_INT >= VERSION_CODES.HONEYCOMB) {
            animSet.playTogether(animList)
        }
        if (VERSION.SDK_INT >= VERSION_CODES.HONEYCOMB) {
            animSet.start()
        }
    }

    @SuppressLint("ObsoleteSdkInt")
    @RequiresApi(VERSION_CODES.HONEYCOMB)
    private fun hideMenu() {
        val animList = ArrayList<Animator>()

        for (i in binding.arcLayout.childCount - 1 downTo 0) {
            animList.add(createHideItemAnimator(binding.arcLayout.getChildAt(i)))
        }

        val animSet = if (VERSION.SDK_INT >= VERSION_CODES.HONEYCOMB) {
            AnimatorSet()
        } else {
            TODO("VERSION.SDK_INT < HONEYCOMB")
        }
        if (VERSION.SDK_INT >= VERSION_CODES.HONEYCOMB) {
            animSet.duration = 400
        }
        if (VERSION.SDK_INT >= VERSION_CODES.DONUT) {
            if (VERSION.SDK_INT >= VERSION_CODES.HONEYCOMB) {
                animSet.interpolator = AnticipateInterpolator()
            }
        }
        if (VERSION.SDK_INT >= VERSION_CODES.HONEYCOMB) {
            animSet.playTogether(animList)
        }
        if (VERSION.SDK_INT >= VERSION_CODES.HONEYCOMB) {
            animSet.addListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator) {
                    super.onAnimationEnd(animation)
                    binding.menuLayout.visibility = View.INVISIBLE
                }
            })
        }
        if (VERSION.SDK_INT >= VERSION_CODES.HONEYCOMB) {
            animSet.start()
        }
    }


    private fun getGenericProfile(genoTypeId: String) {
        binding.progressBar.visibility = VISIBLE
        val manager = NetworkManager()
        if (manager.isConnectingToInternet(requireContext())) {
            manager.createApiRequest(
                ApiUtilis.getAPIInstance(activity!!).getGenericProfile(
                    genoTypeId
                ),
                object : ServiceListener<GenericProfileModel> {
                    override fun getServerResponse(
                        response: GenericProfileModel,
                        requestcode: Int
                    ) {
                        binding.progressBar.visibility = GONE
                        setSpinnerData(response.getData()!!)
                    }

                    override fun getError(error: ErrorModel, requestcode: Int) {

                        binding.progressBar.visibility = GONE
                        val json = JSONObject(error.getMessage()!!)
                        if (json.has("message") && activity != null && activity!!.applicationContext != null) {
                            Toast.makeText(
                                activity!!.applicationContext,
                                json.getString("message"),
                                Toast.LENGTH_LONG
                            )
                                .show()
                        }
                    }
                })
        } else {
            binding.progressBar.visibility = GONE
            Utility.displayShortSnackBar(
                binding.parentLayout,
                resources.getString(R.string.check_internet)
            )

        }
    }


    private fun setSpinnerData(response: List<GenericProfileData>) {
        var selectedPosition = 0
        for (i in 0 until response.size) {
            genericList.add(response[i].getTitle().toString())
            if (categoryId1 != "" && response[i].getId() == categoryId1) {
                selectedPosition = i
                Utility.categoryStatus = false
            }
        }
        System.out.println("categoryId==>*****  $categoryId1$selectedPosition")
        if (genericList.size > 0 && activity != null) {
            val spinnerAdapter = CustomSpinnerAdapter(activity!!, genericList)
            binding.spinner.adapter = spinnerAdapter
            binding.spinner.setSelection(selectedPosition)
            binding.spinner.onItemSelectedListener =
                object : AdapterView.OnItemSelectedListener {
                    override fun onItemSelected(
                        parent: AdapterView<*>,
                        view: View,
                        position: Int,
                        id: Long
                    ) {
                        getKeyData(response[position].getId()!!, genoTypeId!!)
                        newCategoryId1 = response[position].getId()!!
                    }

                    override fun onNothingSelected(parent: AdapterView<*>) {


                    }
                }
        }
    }


    private fun getKeyData(categoryId: String, genoTypeId: String) {

        binding.progressBar.visibility = VISIBLE
        val manager = NetworkManager()
        if (manager.isConnectingToInternet(requireContext())) {
            System.out.println("genoTypeId****--> " + genoTypeId + "modeID******--> " + modeID)

            manager.createApiRequest(
                ApiUtilis.getAPIInstance(activity!!).getKeyDataByCategory(
                    Utility.getUserID(requireContext()),
                    modeID!!,
                    "false",
                    "true",
                    categoryId,
                    genoTypeId
                ),
                object : ServiceListener<SectionByCategoryModel> {
                    override fun getServerResponse(
                        response: SectionByCategoryModel,
                        requestcode: Int
                    ) {
                        binding.progressBar.visibility = GONE
                        if (response.getCode() == 200 && response.getMessage() == "Sections for user found") {
//                            binding.mainLayout.visibility = VISIBLE
                            binding.txtNoDataFound.visibility = GONE
                            setData(response.getData()!!)
                        }
                    }

                    override fun getError(error: ErrorModel, requestcode: Int) {

                        binding.progressBar.visibility = GONE

                        val json = JSONObject(error.getMessage()!!)
                        binding.txtNoDataFound.visibility = VISIBLE

                        binding.txtNoDataFound.text = json.getString("message")
                    }
                })
        } else {
            binding.progressBar.visibility = GONE
            Utility.displayShortSnackBar(
                binding.parentLayout,
                resources.getString(R.string.check_internet)
            )

        }
    }

    private fun setData(data: SectionByCategoryData) {
        if (data.getDescription() != null) {
            if (VERSION.SDK_INT >= VERSION_CODES.N) {
                binding.txtDescription.text =
                    Html.fromHtml(data.getDescription(), Html.FROM_HTML_MODE_COMPACT)
            } else {
                binding.txtDescription.text = Html.fromHtml(data.getDescription())
            }
        } else {
            binding.txtDescription.visibility = GONE
        }
        if (data.getIndicatorDescription() != null) {
            if (VERSION.SDK_INT >= VERSION_CODES.N) {
                binding.txtIndicatorDescription.text = Html.fromHtml(
                    data.getIndicatorDescription(),
                    Html.FROM_HTML_MODE_COMPACT
                )
            } else {
                binding.txtIndicatorDescription.text = Html.fromHtml(data.getIndicatorDescription())
            }
        } else {
            binding.txtIndicatorDescription.visibility = GONE
        }
        if (data.getInterests() != null) {
            if (VERSION.SDK_INT >= VERSION_CODES.N) {
                binding.txtInterests.text = Html.fromHtml(
                    "Genes of interest: " + data.getInterests(),
                    Html.FROM_HTML_MODE_COMPACT
                )
            } else {
                binding.txtInterests.text =
                    Html.fromHtml("Genes of interest: " + data.getDescription())
            }
        } else {
            binding.txtInterests.visibility = GONE
        }
        if (data.getMessage() != null) {
            if (VERSION.SDK_INT >= VERSION_CODES.N) {
                binding.txtMessage.text =
                    Html.fromHtml(data.getMessage(), Html.FROM_HTML_MODE_COMPACT)
            } else {
                binding.txtMessage.text = Html.fromHtml(data.getMessage())
            }
        } else {
            binding.txtMessage.visibility = GONE
        }
        binding.speedView.speedTo(data.getActiveIndicator()!!.getValue()!!.toFloat(), 0)

        val handler = Handler()
        handler.postDelayed({
            // for autoLogin user
            binding.scrollView.fullScroll(View.FOCUS_UP)
        }, 3000)
    }
}