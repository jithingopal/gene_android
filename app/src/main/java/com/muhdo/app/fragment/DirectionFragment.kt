package com.muhdo.app.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.muhdo.app.R
import com.muhdo.app.adapter.DirectionAdapter
import com.muhdo.app.databinding.FragmentDirectionBinding
import org.json.JSONArray

class DirectionFragment : Fragment() {
    internal lateinit var binding: FragmentDirectionBinding
    lateinit var adapter: DirectionAdapter
    val list = arrayListOf<String>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_direction, container, false)
        binding = FragmentDirectionBinding.bind(view)
        val data = this.arguments!!.getString("recipeInfo")
        val jsonArray = JSONArray(data)
        setData(jsonArray)


        return view
    }


    private fun setData(data: JSONArray){
        adapter = DirectionAdapter(activity!!.applicationContext, data)
        binding.listRecipeDirection.layoutManager =
                LinearLayoutManager(activity!!.applicationContext)
        binding.listRecipeDirection.isNestedScrollingEnabled = true
        binding.listRecipeDirection.itemAnimator = DefaultItemAnimator()
        binding.listRecipeDirection.adapter = adapter
    }

}