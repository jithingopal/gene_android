package com.muhdo.app.fragment.v3.guide.workout

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Base64
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.muhdo.app.R
import com.muhdo.app.adapter.v3.WorkoutOverviewParentRecyAdpter
import com.muhdo.app.apiModel.v3.UserIdReq
import com.muhdo.app.apiModel.v3.workoutplan.workout_overview.WorkoutOverviewData
import com.muhdo.app.apiModel.v3.workoutplan.workout_overview.WorkoutOverviewResultPojo
import com.muhdo.app.databinding.V3FragmentWorkoutOverviewBinding
import com.muhdo.app.repository.ApiUtilis
import com.muhdo.app.repository.NetworkManager2
import com.muhdo.app.utils.v3.TempUtil
import com.timingsystemkotlin.backuptimingsystem.Utility
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class WorkoutOverviewFragment : Fragment() {
    lateinit var binding: V3FragmentWorkoutOverviewBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.v3_fragment_workout_overview, container, false)
        binding = V3FragmentWorkoutOverviewBinding.bind(view)

        binding.recyclerWorkoutOverviewParent.layoutManager =
            LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        readWorkoutOverviewResultFromServer()
        return view
    }

    @SuppressLint("WrongConstant")
    private fun replaceFragment(fragment: Fragment) {
        if (activity != null) {
            val transaction = activity!!.supportFragmentManager.beginTransaction()
            transaction.replace(R.id.flContent, fragment)
            transaction.addToBackStack("share")
            transaction.commit()
        }
    }

    private fun readWorkoutOverviewResultFromServer() {

        val userId = Base64.decode(
            Utility.getUserID(activity!!.applicationContext),
            Base64.DEFAULT
        )
        val decodeUserId = String(userId, charset("UTF-8"))
        binding.progressBar.visibility = View.VISIBLE
        var jsonParams: JSONObject = JSONObject()
        jsonParams.put("user_id", Utility.getUserID(activity!!.applicationContext))
        val requestBody = UserIdReq(
            Utility.getUserID(activity!!.applicationContext)
        )
        TempUtil.log("Jithin", jsonParams.toString())
        val manager = NetworkManager2()
        if (manager.isConnectingToInternet(binding.parentLayout.context)) {
            val call =
                ApiUtilis.getAPIInstance(activity!!)
                    .getWorkoutOverview(requestBody)
            call.enqueue(object : Callback<WorkoutOverviewResultPojo> {
                override fun onResponse(
                    call: Call<WorkoutOverviewResultPojo>,
                    response: Response<WorkoutOverviewResultPojo>
                ) {
                    binding.progressBar.visibility = View.GONE
                    var resultPojo: WorkoutOverviewResultPojo? = response.body()
                    try {
                        if (resultPojo != null && resultPojo!!.statusCode == 200) {
                            var data = resultPojo.data
                            TempUtil.log("Jithin", resultPojo.message.toString())

                            updateListAdapter(resultPojo.data!!)
//                            loadListener.onLoadingCompleted(true)
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }

                override fun onFailure(call: Call<WorkoutOverviewResultPojo>, t: Throwable) {
                    // Log error here since request failed
                    binding.progressBar.visibility = View.GONE
                    TempUtil.log("Login", t.toString())

                    Utility.displayShortSnackBar(
                        binding.parentLayout,
                        "Server connection error!!, please try again later."
                    )


                }
            })
        } else {
            binding.progressBar.visibility = View.GONE

            Utility.displayShortSnackBar(
                binding.parentLayout,
                binding.parentLayout.context.resources.getString(R.string.check_internet)
            )

        }

    }

    fun updateListAdapter(data: MutableList<WorkoutOverviewData>?) {
        activity
        var mAdapter:WorkoutOverviewParentRecyAdpter= WorkoutOverviewParentRecyAdpter(activity!!,data!!)
        binding.recyclerWorkoutOverviewParent.addItemDecoration(
            DividerItemDecoration(
                binding.recyclerWorkoutOverviewParent.getContext(),
                DividerItemDecoration.VERTICAL
            )
        );

        binding.recyclerWorkoutOverviewParent.adapter = mAdapter
        binding.recyclerWorkoutOverviewParent.adapter?.notifyDataSetChanged()
    }
}
