package com.muhdo.app.fragment.v3.guide.workout

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.muhdo.app.R
import com.muhdo.app.databinding.V3LayoutPlanChooseExperianceBinding
import com.muhdo.app.utils.v3.getSelectedRadioButtonText

class ChooseExperianceFragment : Fragment() {
    lateinit var binding: V3LayoutPlanChooseExperianceBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view =
            inflater.inflate(R.layout.v3_layout_plan_choose_experiance, container, false)
        binding = V3LayoutPlanChooseExperianceBinding.bind(view)
        return view
    }

    fun getSelectedExperiance(): Int? {
        when (binding.rgExperiance.checkedRadioButtonId) {
            -1 -> {
                return null
            }
            R.id.rbNovice -> {
                return 46
            }
            R.id.rbIntermediate -> {
                return 47
            }
            R.id.rbAdvanced -> {
                return 48
            }
            R.id.rbHighAdvanced -> {
                return 49
            }
            R.id.rbExpert -> {
                return 50
            }
            R.id.rbElite -> {
                return 51
            }
            else -> {
                return null
            }
        }
    }

}
