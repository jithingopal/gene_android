package com.muhdo.app.fragment

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.annotation.SuppressLint
import android.app.ProgressDialog
import android.content.Context
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.text.Html
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.view.animation.AnticipateInterpolator
import android.view.animation.OvershootInterpolator
import android.widget.AdapterView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.core.content.res.ResourcesCompat
import androidx.fragment.app.Fragment
import com.muhdo.app.R
import com.muhdo.app.adapter.CustomSpinnerAdapter
import com.muhdo.app.apiModel.keyData.GenericProfileData
import com.muhdo.app.apiModel.keyData.GenericProfileModel
import com.muhdo.app.apiModel.keyData.SectionByCategoryData
import com.muhdo.app.apiModel.keyData.SectionByCategoryModel
import com.muhdo.app.databinding.FragmentKeydata3Binding
import com.muhdo.app.repository.ApiUtilis
import com.muhdo.app.repository.ErrorModel
import com.muhdo.app.repository.NetworkManager
import com.muhdo.app.repository.ServiceListener
import com.muhdo.app.utils.AnimatorUtils
import com.muhdo.app.utils.PreferenceConnector
import com.muhdo.app.utils.v3.Constants
import com.timingsystemkotlin.backuptimingsystem.Utility
import kotlinx.android.synthetic.main.fragment_keydata.*
import org.json.JSONObject


class KeyDataFragment2 : Fragment() {
    private var progressDialog: ProgressDialog? = null
    internal lateinit var binding: FragmentKeydata3Binding
    private var modeID: String? = ""
    var genoTypeId: String? = ""
    private var categoryId1: String = ""
    var newCategoryId1: String = ""
    var showArc = false
    private val genericList: MutableList<String> = java.util.ArrayList()
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_keydata3, container, false)
        binding = FragmentKeydata3Binding.bind(view)

        return view
    }

    @RequiresApi(Build.VERSION_CODES.HONEYCOMB)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (getUserVisibleHint()) { // fragment is visible

        }
    }

    override fun onStart() {
        super.onStart()
        loadData();
    }

    private fun loadData() {
        Log.d("frag", "fragment visible to the user key data frag 2")

        genoTypeId = this.arguments!!.getString(Constants.SECTION_ID)
        modeID = this.arguments!!.getString(Constants.MODE_ID)
        categoryId1 = Utility.categoryID
        var modeTitle = this.arguments!!.getString(Constants.MODE_TITLE)
        //   Log.d("data","mode id, section id, category id "+modeID+" "+genoTypeId+" "+categoryId1)
        Log.d("data", "key data frag 2 categoryId = " + categoryId1)
        Log.d("data", "key data frag 2 sectionId = " + genoTypeId)
        Log.d("data", "key data frag 2 modeId = " + modeID)
        System.out.println("categoryId==>  $categoryId1")
        if (genoTypeId != null) {
            binding.progressBar.visibility = View.VISIBLE
            getGenericProfile(genoTypeId!!)
        } else {
            Utility.displayShortSnackBar(binding.parentLayout, "Something went wrong")
        }

        binding.fab.setOnClickListener {
            if (showArc == false) {
                showMenu()
                showArc = true
            } else {
                showArc = false
                hideMenu()
            }

        }

        binding.imgBackToTop.setOnClickListener {
            val handler = Handler()
            handler.postDelayed({
                // for autoLogin user
                //binding.scrollView.fullScroll(View.FOCUS_UP)
                binding.scrollView.smoothScrollTo(0, 0);
            }, 0)

        }

//-------------------------------------------------------------------------------------------------------------------------------------
        val modeText =
            com.muhdo.app.utils.v3.Constants.getLastLoadedResultText(activity!!.applicationContext)//getContext()?.let { it1 -> PreferenceConnector.readString(it1, PreferenceConnector.MODE_TEXT, "") }

        if (modeText.equals("Fitness & Endurance", true)) {
            fab.setImageResource(R.drawable.fitness_icon)
        } else if (modeText.equals("Build Muscle", true)) {
            fab.setImageResource(R.drawable.muscle_build_icon)
        } else if (modeText.equals("Fat Loss", true)) {
            fab.setImageResource(R.drawable.fat_loss_icon)
        } else if (modeText.equals("Health & Wellbeing", true)) {
            fab.setImageResource(R.drawable.fit_for_life_icon)
        } else {
            //default one
            fab.setImageResource(R.drawable.fat_loss_icon)
        }

//--------------------------------------------------------------------------------------------------

        binding.arcLayout.getChildAt(0).setOnClickListener {
            com.muhdo.app.utils.v3.Constants.setLastLoadedResultText("Health & Wellbeing")
            /*   getContext()?.let { it1 -> PreferenceConnector.writeString(it1, PreferenceConnector.MODE_TEXT, "Health & Wellbeing") }
               getContext()?.let { it1 -> PreferenceConnector.writeString(it1, PreferenceConnector.MODE_ID, "5c8c96a79c54381add6884dd") }
   */
            /* val tabPosition= getContext()?.let { it1 -> PreferenceConnector.readInt(it1, PreferenceConnector.TAB_POS, 0) }
             if(tabPosition==1){
                 KeyDataFragment.getInstance().loadData()
                  var  fm = getActivity()!!.getSupportFragmentManager();
             if (fm != null) {
               var  fragments = fm.getFragments();
                 for (i in 0..fragments!!.size - 1) {
                    var fragment = fragments.get(i);
                     if (fragment != null) {
                         // found the current fragment
                        if(fragment.retainInstance.)
                         // if you want to check for specific fragment class
                       //fragment.targetFragment.Key
                     }
                 }
             }

             }
 */

            modeID = "5c8c96a79c54381add6884dd"
            showArc = false
            getKeyData(newCategoryId1, genoTypeId!!)
            hideMenu()
            binding.fab.setImageResource(R.drawable.fit_for_life_icon)
        }
        binding.arcLayout.getChildAt(1).setOnClickListener {
            /* getContext()?.let { it1 -> PreferenceConnector.writeString(it1, PreferenceConnector.MODE_TEXT, "Fat Loss") }
             getContext()?.let { it1 -> PreferenceConnector.writeString(it1, PreferenceConnector.MODE_ID, "5c8c96a79c54381add6884dc") }
            */
            com.muhdo.app.utils.v3.Constants.setLastLoadedResultText("Fat Loss")
            val tabPosition = getContext()?.let { it1 ->
                PreferenceConnector.readInt(
                    it1,
                    PreferenceConnector.TAB_POS,
                    0
                )
            }
            if (tabPosition == 1) {
                // KeyDataFragment.getInstance().loadData()
            }
            modeID = "5c8c96a79c54381add6884dc"
            showArc = false
            getKeyData(newCategoryId1, genoTypeId!!)
            hideMenu()
            binding.fab.setImageResource(R.drawable.fat_loss_icon)
        }
        binding.arcLayout.getChildAt(2).setOnClickListener {
            /*getContext()?.let { it1 -> PreferenceConnector.writeString(it1, PreferenceConnector.MODE_TEXT, "Build Muscle") }
            getContext()?.let { it1 -> PreferenceConnector.writeString(it1, PreferenceConnector.MODE_ID, "5c8c96a79c54381add6884db") }
            */
            com.muhdo.app.utils.v3.Constants.setLastLoadedResultText("Build Muscle")
            val tabPosition = getContext()?.let { it1 ->
                PreferenceConnector.readInt(
                    it1,
                    PreferenceConnector.TAB_POS,
                    0
                )
            }
            if (tabPosition == 1) {
                //KeyDataFragment.getInstance().loadData()
            }
            modeID = "5c8c96a79c54381add6884db"
            showArc = false
            getKeyData(newCategoryId1, genoTypeId!!)
            hideMenu()
            binding.fab.setImageResource(R.drawable.muscle_build_icon)
        }
        binding.arcLayout.getChildAt(3).setOnClickListener {
            /* getContext()?.let { it1 -> PreferenceConnector.writeString(it1, PreferenceConnector.MODE_TEXT, "Fitness") }
             getContext()?.let { it1 -> PreferenceConnector.writeString(it1, PreferenceConnector.MODE_ID, "5c8c96a79c54381add6884da") }
             */
            com.muhdo.app.utils.v3.Constants.setLastLoadedResultText("Fitness & Endurance")
            val tabPosition = getContext()?.let { it1 ->
                PreferenceConnector.readInt(
                    it1,
                    PreferenceConnector.TAB_POS,
                    0
                )
            }
            if (tabPosition == 1) {
                //  KeyDataFragment.getInstance().loadData()
            }
            modeID = "5c8c96a79c54381add6884da"
            showArc = false
            getKeyData(newCategoryId1, genoTypeId!!)
            hideMenu()
            binding.fab.setImageResource(R.drawable.fitness_icon)
        }
    }


    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser);

        if (isVisibleToUser && isResumed()) { // fragment is visible and have created
            loadData();
        }
    }


    @RequiresApi(Build.VERSION_CODES.HONEYCOMB)
    private fun createShowItemAnimator(item: View): Animator {

        val dx = fab.x - item.x
        val dy = fab.y - item.y

        item.rotation = 0f
        item.translationX = dx
        item.translationY = dy

        return ObjectAnimator.ofPropertyValuesHolder(
            item,
            AnimatorUtils.rotation(0f, 720f),
            AnimatorUtils.translationX(dx, 0f),
            AnimatorUtils.translationY(dy, 0f)
        )
    }

    @SuppressLint("ObsoleteSdkInt")
    @RequiresApi(Build.VERSION_CODES.HONEYCOMB)
    private fun createHideItemAnimator(item: View): Animator {
        val dx = fab.x - item.x
        val dy = fab.y - item.y

        val anim = ObjectAnimator.ofPropertyValuesHolder(
            item,
            AnimatorUtils.rotation(720f, 0f),
            AnimatorUtils.translationX(0f, dx),
            AnimatorUtils.translationY(0f, dy)
        )

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            anim.addListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator) {
                    super.onAnimationEnd(animation)
                    item.translationX = 0f
                    item.translationY = 0f
                }
            })
        }

        return anim
    }

    @SuppressLint("ObsoleteSdkInt")
    @RequiresApi(Build.VERSION_CODES.HONEYCOMB)
    private fun showMenu() {
        menu_layout.visibility = View.VISIBLE

        val animList = java.util.ArrayList<Animator>()

        var i = 0
        val len = arc_layout.childCount
        while (i < len) {
            animList.add(createShowItemAnimator(arc_layout.getChildAt(i)))
            i++
        }

        val animSet = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            AnimatorSet()
        } else {
            TODO("VERSION.SDK_INT < HONEYCOMB")
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            animSet.duration = 400
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            animSet.interpolator = OvershootInterpolator()
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            animSet.playTogether(animList)
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            animSet.start()
        }
    }

    @SuppressLint("ObsoleteSdkInt")
    @RequiresApi(Build.VERSION_CODES.HONEYCOMB)
    private fun hideMenu() {


        val animList = java.util.ArrayList<Animator>()

        for (i in arc_layout.childCount - 1 downTo 0) {
            animList.add(createHideItemAnimator(arc_layout.getChildAt(i)))
        }

        val animSet = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            AnimatorSet()
        } else {
            TODO("VERSION.SDK_INT < HONEYCOMB")
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            animSet.duration = 400
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.DONUT) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                animSet.interpolator = AnticipateInterpolator()
            }
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            animSet.playTogether(animList)
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            animSet.addListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator) {
                    super.onAnimationEnd(animation)
                    menu_layout.visibility = View.INVISIBLE
                }
            })
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            animSet.start()
        }

    }


    private fun getGenericProfile(genoTypeId: String) {


        val manager = NetworkManager()
        if (manager.isConnectingToInternet(requireContext())) {
            manager.createApiRequest(
                ApiUtilis.getAPIInstance(activity!!).getGenericProfile(
                    genoTypeId
                ),
                object : ServiceListener<GenericProfileModel> {
                    override fun getServerResponse(
                        response: GenericProfileModel,
                        requestcode: Int
                    ) {
                        binding.progressBar.visibility = View.GONE
                        Log.d("data", "getGenericProfile()call " + genoTypeId)
                        setSpinnerData(response.getData()!!)
                    }

                    override fun getError(error: ErrorModel, requestcode: Int) {

                        binding.progressBar.visibility = View.GONE
                        val json = JSONObject(error.getMessage()!!)
                        if (json.has("message") && activity != null && activity!!.applicationContext != null) {
                            Toast.makeText(
                                activity!!.applicationContext,
                                json.getString("message"),
                                Toast.LENGTH_LONG
                            )
                                .show()
                        }
                    }
                })
        } else {
            binding.progressBar.visibility = View.GONE
            Utility.displayShortSnackBar(
                binding.parentLayout,
                resources.getString(R.string.check_internet)
            )

        }
    }

    private fun getGenericProfileClearSpinnerData(genoTypeId: String) {

        binding.progressBar.visibility = View.VISIBLE
        val manager = NetworkManager()
        if (manager.isConnectingToInternet(requireContext())) {
            manager.createApiRequest(
                ApiUtilis.getAPIInstance(activity!!).getGenericProfile(
                    genoTypeId
                ),
                object : ServiceListener<GenericProfileModel> {
                    override fun getServerResponse(
                        response: GenericProfileModel,
                        requestcode: Int
                    ) {
                        binding.progressBar.visibility = View.GONE
                        Log.d("data", "getGenericProfile()call " + genoTypeId)
                        setSpinnerData(response.getData()!!)
                    }

                    override fun getError(error: ErrorModel, requestcode: Int) {

                        binding.progressBar.visibility = View.GONE
                        val json = JSONObject(error.getMessage()!!)
                        if (json.has("message") && activity != null && activity!!.applicationContext != null) {
                            Toast.makeText(
                                activity!!.applicationContext,
                                json.getString("message"),
                                Toast.LENGTH_LONG
                            )
                                .show()
                        }
                    }
                })
        } else {
            binding.progressBar.visibility = View.GONE
            Utility.displayShortSnackBar(
                binding.parentLayout,
                resources.getString(R.string.check_internet)
            )

        }
    }


    private fun setSpinnerData(response: List<GenericProfileData>) {
        var selectedPosition = 0
        Log.d("data", "set spinner data cate id = " + categoryId1)
        genericList.clear()
        for (i in 0 until response.size) {
            genericList.add(response[i].getTitle().toString())

            Log.d("data", "inside for ----" + response[i].getId() + " == " + categoryId1)
            if (categoryId1 != "" && response[i].getId() == categoryId1) {
                Log.d(
                    "data",
                    "inside if match ----" + response[i].getId() + " == " + categoryId1 + " " + i
                )
                selectedPosition = i
                Utility.categoryStatus = false
            }
        }
        System.out.println("categoryId==>*****  $categoryId1$selectedPosition")
        if (genericList.size > 0 && activity != null) {

            val spinnerAdapter: CustomSpinnerAdapter = CustomSpinnerAdapter(activity!!, genericList)
            binding.spinner.adapter = spinnerAdapter
            Log.d("data", "spinner data inside if = " + selectedPosition)
            binding.spinner.setSelection(selectedPosition)
            binding.spinner.setBackgroundColor(resources.getColor(R.color.colorPrimaryDark))
            binding.spinner.onItemSelectedListener =
                object : AdapterView.OnItemSelectedListener {
                    override fun onItemSelected(
                        parent: AdapterView<*>,
                        view: View,
                        position: Int,
                        id: Long
                    ) {
                        binding.progressBar.visibility = View.VISIBLE
                        //showProgressDialog()
                        getKeyData(response[position].getId()!!, genoTypeId!!)
                        newCategoryId1 = response[position].getId()!!
                        val spinnerVal = genericList.get(position).toString()
                        Log.wtf("data", "select" + spinnerVal)
                        updateSpeedViewValue(spinnerVal)
                    }

                    override fun onNothingSelected(parent: AdapterView<*>) {

                    }
                }
        }
    }

    private fun updateSpeedViewValue(spinnerVal: String) {
        if (spinnerVal.equals("Muscle Power")) {
            binding.textView1Speed.visibility = View.VISIBLE
            binding.textView2Speed.visibility = View.VISIBLE
            binding.textView3Speed.visibility = View.VISIBLE
            binding.textView4Speed.visibility = View.GONE
            binding.textView5Speed.visibility = View.VISIBLE
            binding.textView6Speed.visibility = View.VISIBLE
            binding.textView7Speed.visibility = View.VISIBLE
            binding.textView1Speed.text = "Normal"
            binding.textView2Speed.text = "Above\nNormal"
            binding.textView3Speed.text = "Gifted"
            binding.textView5Speed.text = "Highly\nGifted"
            binding.textView6Speed.text = "Elite"
            binding.textView7Speed.text = "Super\nElite"
        } else if (spinnerVal.equals("Muscle Stamina")) {
            binding.textView1Speed.visibility = View.VISIBLE
            binding.textView2Speed.visibility = View.VISIBLE
            binding.textView3Speed.visibility = View.VISIBLE
            binding.textView4Speed.visibility = View.GONE
            binding.textView5Speed.visibility = View.VISIBLE
            binding.textView6Speed.visibility = View.VISIBLE
            binding.textView7Speed.visibility = View.VISIBLE
            binding.textView1Speed.text = "Normal"
            binding.textView2Speed.text = "Above\nNormal"
            binding.textView3Speed.text = "Gifted"
            binding.textView5Speed.text = "Highly\nGifted"
            binding.textView6Speed.text = "Elite"
            binding.textView7Speed.text = "Super\nElite"
        } else if (spinnerVal.equals("O2 Usage")) {
            binding.textView1Speed.visibility = View.VISIBLE
            binding.textView2Speed.visibility = View.GONE
            binding.textView3Speed.visibility = View.GONE
            binding.textView4Speed.visibility = View.VISIBLE
            binding.textView5Speed.visibility = View.GONE
            binding.textView6Speed.visibility = View.GONE
            binding.textView7Speed.visibility = View.VISIBLE
            binding.textView1Speed.text = "Normal"
            binding.textView4Speed.text = "Gifted"
            binding.textView7Speed.text = "Highly\nGifted"
        } else if (spinnerVal.equals("Anaerobic Threshold")) {
            binding.textView1Speed.visibility = View.VISIBLE
            binding.textView2Speed.visibility = View.GONE
            binding.textView3Speed.visibility = View.GONE
            binding.textView4Speed.visibility = View.GONE
            binding.textView5Speed.visibility = View.GONE
            binding.textView6Speed.visibility = View.GONE
            binding.textView7Speed.visibility = View.VISIBLE
            binding.textView1Speed.text = "Normal"
            binding.textView7Speed.text = "Gifted"
        } else if (spinnerVal.equals("Recovery")) {
            binding.textView1Speed.visibility = View.VISIBLE
            binding.textView2Speed.visibility = View.GONE
            binding.textView3Speed.visibility = View.GONE
            binding.textView4Speed.visibility = View.VISIBLE
            binding.textView5Speed.visibility = View.GONE
            binding.textView6Speed.visibility = View.GONE
            binding.textView7Speed.visibility = View.VISIBLE
            binding.textView1Speed.text = "Slow"
            binding.textView7Speed.text = "Fast"
            binding.textView4Speed.text = "Normal"

        } else if (spinnerVal.equals("Muscle Mass")) {
            binding.textView1Speed.visibility = View.VISIBLE
            binding.textView2Speed.visibility = View.GONE
            binding.textView3Speed.visibility = View.GONE
            binding.textView4Speed.visibility = View.VISIBLE
            binding.textView5Speed.visibility = View.GONE
            binding.textView6Speed.visibility = View.GONE
            binding.textView7Speed.visibility = View.VISIBLE
            binding.textView1Speed.text = "Normal"
            binding.textView4Speed.text = "Increased"
            binding.textView7Speed.text = "Gifted"
        } else if (spinnerVal.equals("Injury Risk")) {
            binding.textView1Speed.visibility = View.VISIBLE
            binding.textView2Speed.visibility = View.GONE
            binding.textView3Speed.visibility = View.GONE
            binding.textView4Speed.visibility = View.VISIBLE
            binding.textView5Speed.visibility = View.GONE
            binding.textView6Speed.visibility = View.GONE
            binding.textView7Speed.visibility = View.VISIBLE
            binding.textView1Speed.text = "Increased\nRisk"
            binding.textView4Speed.text = "Normal\nRisk"
            binding.textView7Speed.text = "Slightly\nProtected"
        } else if (spinnerVal.equals("Inflammation")) {
            binding.textView1Speed.visibility = View.VISIBLE
            binding.textView2Speed.visibility = View.GONE
            binding.textView3Speed.visibility = View.GONE
            binding.textView4Speed.visibility = View.VISIBLE
            binding.textView5Speed.visibility = View.GONE
            binding.textView6Speed.visibility = View.GONE
            binding.textView7Speed.visibility = View.VISIBLE
            binding.textView1Speed.text = "Lower\nResponse"
            binding.textView4Speed.text = "Normal"
            binding.textView7Speed.text = "Increased\nResponse"
        } else if (spinnerVal.equals("Lean Body Mass")) {
            binding.textView1Speed.visibility = View.VISIBLE
            binding.textView2Speed.visibility = View.GONE
            binding.textView3Speed.visibility = View.GONE
            binding.textView4Speed.visibility = View.VISIBLE
            binding.textView5Speed.visibility = View.GONE
            binding.textView6Speed.visibility = View.GONE
            binding.textView7Speed.visibility = View.VISIBLE
            binding.textView1Speed.text = "Normal"
            binding.textView4Speed.text = "Good"
            binding.textView7Speed.text = "Gifted"
        } else if (spinnerVal.equals("Power to Weight")) {
            binding.textView1Speed.visibility = View.VISIBLE
            binding.textView2Speed.visibility = View.GONE
            binding.textView3Speed.visibility = View.VISIBLE
            binding.textView4Speed.visibility = View.GONE
            binding.textView5Speed.visibility = View.VISIBLE
            binding.textView6Speed.visibility = View.GONE
            binding.textView7Speed.visibility = View.VISIBLE
            binding.textView1Speed.text = "Normal"
            binding.textView3Speed.text = "Good"
            binding.textView5Speed.text = "Gifted"
            binding.textView7Speed.text = "Very\nGifted"
        } else if (spinnerVal.equals("Exercise Affect on Weight")) {
            binding.textView1Speed.visibility = View.VISIBLE
            binding.textView2Speed.visibility = View.GONE
            binding.textView3Speed.visibility = View.GONE
            binding.textView4Speed.visibility = View.VISIBLE
            binding.textView5Speed.visibility = View.GONE
            binding.textView6Speed.visibility = View.GONE
            binding.textView7Speed.visibility = View.VISIBLE
            binding.textView1Speed.text = "Normal"
            binding.textView4Speed.text = "Good\nResponse"
            binding.textView7Speed.text = "Great\nResponse"
        } else if (spinnerVal.equals("Carbs")) {
            binding.textView1Speed.visibility = View.VISIBLE
            binding.textView2Speed.visibility = View.GONE
            binding.textView3Speed.visibility = View.GONE
            binding.textView4Speed.visibility = View.VISIBLE
            binding.textView5Speed.visibility = View.GONE
            binding.textView6Speed.visibility = View.GONE
            binding.textView7Speed.visibility = View.VISIBLE
            binding.textView1Speed.text = "Lower\nRisk"
            binding.textView4Speed.text = "Normal\nRisk"
            binding.textView7Speed.text = "Increased\nRisk"
        } else if (spinnerVal.equals("Saturated Fats")) {
            binding.textView1Speed.visibility = View.VISIBLE
            binding.textView2Speed.visibility = View.GONE
            binding.textView3Speed.visibility = View.VISIBLE
            binding.textView4Speed.visibility = View.GONE
            binding.textView5Speed.visibility = View.VISIBLE
            binding.textView6Speed.visibility = View.GONE
            binding.textView7Speed.visibility = View.VISIBLE
            binding.textView1Speed.text = "Normal\nResponse"
            binding.textView3Speed.text = "Increased\nRisk"
            binding.textView5Speed.text = "High\nRisk"
            binding.textView7Speed.text = "Highest\nRisk"
        } else if (spinnerVal.equals("Unsaturated Fats")) {
            binding.textView1Speed.visibility = View.VISIBLE
            binding.textView2Speed.visibility = View.GONE
            binding.textView3Speed.visibility = View.GONE
            binding.textView4Speed.visibility = View.VISIBLE
            binding.textView5Speed.visibility = View.GONE
            binding.textView6Speed.visibility = View.GONE
            binding.textView7Speed.visibility = View.VISIBLE
            binding.textView1Speed.text = "Reduced\nBenefit"
            binding.textView4Speed.text = "Beneficial"
            binding.textView7Speed.text = "Very\nBeneficial"
        } else if (spinnerVal.equals("Protein")) {
            binding.textView1Speed.visibility = View.VISIBLE
            binding.textView2Speed.visibility = View.GONE
            binding.textView3Speed.visibility = View.GONE
            binding.textView4Speed.visibility = View.VISIBLE
            binding.textView5Speed.visibility = View.GONE
            binding.textView6Speed.visibility = View.GONE
            binding.textView7Speed.visibility = View.VISIBLE
            binding.textView1Speed.text = "Normal\nResponse"
            binding.textView4Speed.text = "Good\nResponse"
            binding.textView7Speed.text = "Great\nResponse"
        } else if (spinnerVal.equals("Sugar Response")) {
            binding.textView1Speed.visibility = View.VISIBLE
            binding.textView2Speed.visibility = View.GONE
            binding.textView3Speed.visibility = View.GONE
            binding.textView4Speed.visibility = View.VISIBLE
            binding.textView5Speed.visibility = View.GONE
            binding.textView6Speed.visibility = View.GONE
            binding.textView7Speed.visibility = View.VISIBLE
            binding.textView1Speed.text = "Normal\nResponse"
            binding.textView4Speed.text = "Increased\nResponse"
            binding.textView7Speed.text = "Highest\nResponse"
        } else if (spinnerVal.equals("Sweet Taste")) {
            binding.textView1Speed.visibility = View.VISIBLE
            binding.textView2Speed.visibility = View.GONE
            binding.textView3Speed.visibility = View.GONE
            binding.textView4Speed.visibility = View.VISIBLE
            binding.textView5Speed.visibility = View.GONE
            binding.textView6Speed.visibility = View.GONE
            binding.textView7Speed.visibility = View.VISIBLE
            binding.textView1Speed.text = "Less\nLikely"
            binding.textView4Speed.text = "Normal"
            binding.textView7Speed.text = "Increased\nRisk"
        } else if (spinnerVal.equals("Bitter Taste")) {
            binding.textView1Speed.visibility = View.VISIBLE
            binding.textView2Speed.visibility = View.GONE
            binding.textView3Speed.visibility = View.GONE
            binding.textView4Speed.visibility = View.GONE
            binding.textView5Speed.visibility = View.GONE
            binding.textView6Speed.visibility = View.GONE
            binding.textView7Speed.visibility = View.VISIBLE
            binding.textView1Speed.text = "Less\nLikely"
            binding.textView7Speed.text = "Bitter\nTaster"
        } else if (spinnerVal.equals("Likelihood to Snack")) {
            binding.textView1Speed.visibility = View.VISIBLE
            binding.textView2Speed.visibility = View.GONE
            binding.textView3Speed.visibility = View.GONE
            binding.textView4Speed.visibility = View.VISIBLE
            binding.textView5Speed.visibility = View.GONE
            binding.textView6Speed.visibility = View.GONE
            binding.textView7Speed.visibility = View.VISIBLE
            binding.textView1Speed.text = "Less\nLikely"
            binding.textView4Speed.text = "Normal"
            binding.textView7Speed.text = "Highly\nLikely"
        } else if (spinnerVal.equals("Lactose Intolerance Risk")) {
            binding.textView1Speed.visibility = View.VISIBLE
            binding.textView2Speed.visibility = View.GONE
            binding.textView3Speed.visibility = View.GONE
            binding.textView4Speed.visibility = View.GONE
            binding.textView5Speed.visibility = View.GONE
            binding.textView6Speed.visibility = View.GONE
            binding.textView7Speed.visibility = View.VISIBLE
            binding.textView1Speed.text = "Not\nIntolerant"
            binding.textView7Speed.text = "Possible\nIntolerance"
        } else if (spinnerVal.equals("Metabolic Rate")) {
            binding.textView1Speed.visibility = View.VISIBLE
            binding.textView2Speed.visibility = View.GONE
            binding.textView3Speed.visibility = View.GONE
            binding.textView4Speed.visibility = View.VISIBLE
            binding.textView5Speed.visibility = View.GONE
            binding.textView6Speed.visibility = View.GONE
            binding.textView7Speed.visibility = View.VISIBLE
            binding.textView1Speed.text = "Fast"
            binding.textView4Speed.text = "Normal"
            binding.textView7Speed.text = "Slow"
        } else if (spinnerVal.equals("Fat Distribution")) {
            binding.textView1Speed.visibility = View.VISIBLE
            binding.textView2Speed.visibility = View.GONE
            binding.textView3Speed.visibility = View.GONE
            binding.textView4Speed.visibility = View.VISIBLE
            binding.textView5Speed.visibility = View.GONE
            binding.textView6Speed.visibility = View.GONE
            binding.textView7Speed.visibility = View.VISIBLE
            binding.textView1Speed.text = "Good"
            binding.textView4Speed.text = "Normal"
            binding.textView7Speed.text = "Bad"
        } else if (spinnerVal.equals("Yo/Yo Diet Response")) {
            binding.textView1Speed.visibility = View.VISIBLE
            binding.textView2Speed.visibility = View.GONE
            binding.textView3Speed.visibility = View.GONE
            binding.textView4Speed.visibility = View.VISIBLE
            binding.textView5Speed.visibility = View.GONE
            binding.textView6Speed.visibility = View.GONE
            binding.textView7Speed.visibility = View.VISIBLE
            binding.textView1Speed.text = "Good"
            binding.textView4Speed.text = "Bad"
            binding.textView7Speed.text = "Very\nBad"
        } else if (spinnerVal.equals("Vitamin D")) {
            binding.textView1Speed.visibility = View.VISIBLE
            binding.textView2Speed.visibility = View.GONE
            binding.textView3Speed.visibility = View.GONE
            binding.textView4Speed.visibility = View.VISIBLE
            binding.textView5Speed.visibility = View.GONE
            binding.textView6Speed.visibility = View.GONE
            binding.textView7Speed.visibility = View.VISIBLE
            binding.textView1Speed.text = "No\nRisk"
            binding.textView4Speed.text = "Normal\nRisk"
            binding.textView7Speed.text = "Increased\nRisk"
        } else if (spinnerVal.equals("Vitamin A")) {
            binding.textView1Speed.visibility = View.VISIBLE
            binding.textView2Speed.visibility = View.GONE
            binding.textView3Speed.visibility = View.GONE
            binding.textView4Speed.visibility = View.VISIBLE
            binding.textView5Speed.visibility = View.GONE
            binding.textView6Speed.visibility = View.GONE
            binding.textView7Speed.visibility = View.VISIBLE
            binding.textView1Speed.text = "No\nRisk"
            binding.textView4Speed.text = "Normal\nRisk"
            binding.textView7Speed.text = "At\nRisk"
        } else if (spinnerVal.equals("Iron")) {
            binding.textView1Speed.visibility = View.VISIBLE
            binding.textView2Speed.visibility = View.GONE
            binding.textView3Speed.visibility = View.GONE
            binding.textView4Speed.visibility = View.GONE
            binding.textView5Speed.visibility = View.GONE
            binding.textView6Speed.visibility = View.GONE
            binding.textView7Speed.visibility = View.VISIBLE
            binding.textView1Speed.text = "Normal\nRisk"
            binding.textView7Speed.text = "Increased\nRisk"
        } else if (spinnerVal.equals("Magnesium")) {
            binding.textView1Speed.visibility = View.VISIBLE
            binding.textView2Speed.visibility = View.GONE
            binding.textView3Speed.visibility = View.GONE
            binding.textView4Speed.visibility = View.VISIBLE
            binding.textView5Speed.visibility = View.GONE
            binding.textView6Speed.visibility = View.GONE
            binding.textView7Speed.visibility = View.VISIBLE
            binding.textView1Speed.text = "Normal\nRisk"
            binding.textView4Speed.text = "Increased\nRisk"
            binding.textView7Speed.text = "High\nRisk"
        } else if (spinnerVal.equals("Potassium")) {
            binding.textView1Speed.visibility = View.VISIBLE
            binding.textView2Speed.visibility = View.GONE
            binding.textView3Speed.visibility = View.GONE
            binding.textView4Speed.visibility = View.GONE
            binding.textView5Speed.visibility = View.GONE
            binding.textView6Speed.visibility = View.GONE
            binding.textView7Speed.visibility = View.VISIBLE
            binding.textView1Speed.text = "Normal\nRisk"
            binding.textView7Speed.text = "Increased\nRisk"
        } else if (spinnerVal.equals("Sodium")) {
            binding.textView1Speed.visibility = View.VISIBLE
            binding.textView2Speed.visibility = View.GONE
            binding.textView3Speed.visibility = View.GONE
            binding.textView4Speed.visibility = View.GONE
            binding.textView5Speed.visibility = View.GONE
            binding.textView6Speed.visibility = View.GONE
            binding.textView7Speed.visibility = View.VISIBLE
            binding.textView1Speed.text = "Normal\nRisk"
            binding.textView7Speed.text = "Increased\nRisk"
        } else if (spinnerVal.equals("Vitamin B12")) {
            binding.textView1Speed.visibility = View.VISIBLE
            binding.textView2Speed.visibility = View.GONE
            binding.textView3Speed.visibility = View.GONE
            binding.textView4Speed.visibility = View.VISIBLE
            binding.textView5Speed.visibility = View.GONE
            binding.textView6Speed.visibility = View.GONE
            binding.textView7Speed.visibility = View.VISIBLE
            binding.textView1Speed.text = "Normal\nRisk"
            binding.textView4Speed.text = "Increased\nRisk"
            binding.textView7Speed.text = "High\nRisk"
        } else if (spinnerVal.equals("Folate")) {
            binding.textView1Speed.visibility = View.VISIBLE
            binding.textView2Speed.visibility = View.GONE
            binding.textView3Speed.visibility = View.VISIBLE
            binding.textView4Speed.visibility = View.GONE
            binding.textView5Speed.visibility = View.VISIBLE
            binding.textView6Speed.visibility = View.GONE
            binding.textView7Speed.visibility = View.VISIBLE
            binding.textView1Speed.text = "No\nRisk"
            binding.textView3Speed.text = "Minimal\nRisk"
            binding.textView5Speed.text = "Increased\nRisk"
            binding.textView7Speed.text = "High\nRisk"
        } else if (spinnerVal.equals("Selenium")) {
            binding.textView1Speed.visibility = View.VISIBLE
            binding.textView2Speed.visibility = View.GONE
            binding.textView3Speed.visibility = View.GONE
            binding.textView4Speed.visibility = View.VISIBLE
            binding.textView5Speed.visibility = View.GONE
            binding.textView6Speed.visibility = View.GONE
            binding.textView7Speed.visibility = View.VISIBLE
            binding.textView1Speed.text = "Normal\nRisk"
            binding.textView4Speed.text = "Normal\nRisk"
            binding.textView7Speed.text = "Increased\nRisk"
        } else if (spinnerVal.equals("Omega-3")) {
            binding.textView1Speed.visibility = View.VISIBLE
            binding.textView2Speed.visibility = View.GONE
            binding.textView3Speed.visibility = View.GONE
            binding.textView4Speed.visibility = View.VISIBLE
            binding.textView5Speed.visibility = View.GONE
            binding.textView6Speed.visibility = View.GONE
            binding.textView7Speed.visibility = View.VISIBLE
            binding.textView1Speed.text = "Normal\nBenefit"
            binding.textView4Speed.text = "Beneficial"
            binding.textView7Speed.text = "Very\nBeneficial"
        } else if (spinnerVal.equals("Calcium")) {
            binding.textView1Speed.visibility = View.VISIBLE
            binding.textView2Speed.visibility = View.GONE
            binding.textView3Speed.visibility = View.GONE
            binding.textView4Speed.visibility = View.GONE
            binding.textView5Speed.visibility = View.GONE
            binding.textView6Speed.visibility = View.GONE
            binding.textView7Speed.visibility = View.VISIBLE
            binding.textView1Speed.text = "Normal\nRisk"
            binding.textView7Speed.text = "Increased\nRisk"
        } else if (spinnerVal.equals("BCAA's")) {
            binding.textView1Speed.visibility = View.VISIBLE
            binding.textView2Speed.visibility = View.GONE
            binding.textView3Speed.visibility = View.GONE
            binding.textView4Speed.visibility = View.GONE
            binding.textView5Speed.visibility = View.GONE
            binding.textView6Speed.visibility = View.GONE
            binding.textView7Speed.visibility = View.VISIBLE
            binding.textView1Speed.text = "No\nBenefit"
            binding.textView7Speed.text = "Beneficial"
        } else if (spinnerVal.equals("Choline")) {
            binding.textView1Speed.visibility = View.VISIBLE
            binding.textView2Speed.visibility = View.GONE
            binding.textView3Speed.visibility = View.GONE
            binding.textView4Speed.visibility = View.VISIBLE
            binding.textView5Speed.visibility = View.GONE
            binding.textView6Speed.visibility = View.GONE
            binding.textView7Speed.visibility = View.VISIBLE
            binding.textView1Speed.text = "Normal\nBenefit"
            binding.textView4Speed.text = "Beneficial"
            binding.textView7Speed.text = "Very\nBeneficial"
        } else if (spinnerVal.equals("Glutamine")) {
            binding.textView1Speed.visibility = View.VISIBLE
            binding.textView2Speed.visibility = View.GONE
            binding.textView3Speed.visibility = View.GONE
            binding.textView4Speed.visibility = View.GONE
            binding.textView5Speed.visibility = View.GONE
            binding.textView6Speed.visibility = View.GONE
            binding.textView7Speed.visibility = View.VISIBLE
            binding.textView1Speed.text = "Beneficial"
            binding.textView7Speed.text = "Non\nBeneficial"
        } else if (spinnerVal.equals("Creatine")) {
            binding.textView1Speed.visibility = View.VISIBLE
            binding.textView2Speed.visibility = View.GONE
            binding.textView3Speed.visibility = View.VISIBLE
            binding.textView4Speed.visibility = View.GONE
            binding.textView5Speed.visibility = View.VISIBLE
            binding.textView6Speed.visibility = View.GONE
            binding.textView7Speed.visibility = View.VISIBLE
            binding.textView1Speed.text = "Non\nBeneficial"
            binding.textView3Speed.text = "Some\nBenefit"
            binding.textView5Speed.text = "Beneficial"
            binding.textView7Speed.text = "Highly\nBeneficial"
        } else if (spinnerVal.equals("Beta-Alanine")) {
            binding.textView1Speed.visibility = View.VISIBLE
            binding.textView2Speed.visibility = View.GONE
            binding.textView3Speed.visibility = View.VISIBLE
            binding.textView4Speed.visibility = View.GONE
            binding.textView5Speed.visibility = View.VISIBLE
            binding.textView6Speed.visibility = View.GONE
            binding.textView7Speed.visibility = View.VISIBLE
            binding.textView1Speed.text = "Non\nBeneficial"
            binding.textView3Speed.text = "Some\nBenefit"
            binding.textView5Speed.text = "Beneficial"
            binding.textView7Speed.text = "Highly\nBeneficial"
        } else if (spinnerVal.equals("Arginine")) {
            binding.textView1Speed.visibility = View.VISIBLE
            binding.textView2Speed.visibility = View.GONE
            binding.textView3Speed.visibility = View.VISIBLE
            binding.textView4Speed.visibility = View.GONE
            binding.textView5Speed.visibility = View.VISIBLE
            binding.textView6Speed.visibility = View.GONE
            binding.textView7Speed.visibility = View.VISIBLE
            binding.textView1Speed.text = "Non\nBeneficial"
            binding.textView3Speed.text = "Beneficial"
            binding.textView5Speed.text = "Very\nBeneficial"
            binding.textView7Speed.text = "Highly\nBeneficial"
        } else if (spinnerVal.equals("Caffeine Sensitivity")) {
            binding.textView1Speed.visibility = View.VISIBLE
            binding.textView2Speed.visibility = View.GONE
            binding.textView3Speed.visibility = View.VISIBLE
            binding.textView4Speed.visibility = View.GONE
            binding.textView5Speed.visibility = View.VISIBLE
            binding.textView6Speed.visibility = View.GONE
            binding.textView7Speed.visibility = View.VISIBLE
            binding.textView1Speed.text = "Normal\nResponse"
            binding.textView3Speed.text = "Slight\nSensitivity"
            binding.textView5Speed.text = "Sensitivity"
            binding.textView7Speed.text = "Higher\nSensitivity"
        } else if (spinnerVal.equals("Genetic Bone Mineral Density")) {
            binding.textView1Speed.visibility = View.VISIBLE
            binding.textView2Speed.visibility = View.GONE
            binding.textView3Speed.visibility = View.GONE
            binding.textView4Speed.visibility = View.VISIBLE
            binding.textView5Speed.visibility = View.GONE
            binding.textView6Speed.visibility = View.GONE
            binding.textView7Speed.visibility = View.VISIBLE
            binding.textView1Speed.text = "Lower\nRisk"
            binding.textView4Speed.text = "Normal\nRisk"
            binding.textView7Speed.text = "Slight\nRisk"
        } else if (spinnerVal.equals("Genetic Obesity Risk")) {
            binding.textView1Speed.visibility = View.VISIBLE
            binding.textView2Speed.visibility = View.GONE
            binding.textView3Speed.visibility = View.GONE
            binding.textView4Speed.visibility = View.VISIBLE
            binding.textView5Speed.visibility = View.GONE
            binding.textView6Speed.visibility = View.GONE
            binding.textView7Speed.visibility = View.VISIBLE
            binding.textView1Speed.text = "Lower\nRisk"
            binding.textView4Speed.text = "Normal\nRisk"
            binding.textView7Speed.text = "Increased\nRisk"
        } else if (spinnerVal.equals("Genetic Type 2 Diabetes Risk")) {
            binding.textView1Speed.visibility = View.VISIBLE
            binding.textView2Speed.visibility = View.GONE
            binding.textView3Speed.visibility = View.GONE
            binding.textView4Speed.visibility = View.VISIBLE
            binding.textView5Speed.visibility = View.GONE
            binding.textView6Speed.visibility = View.GONE
            binding.textView7Speed.visibility = View.VISIBLE
            binding.textView1Speed.text = "Lower\nRisk"
            binding.textView4Speed.text = "Normal\nRisk"
            binding.textView7Speed.text = "Increased\nRisk"
        } else if (spinnerVal.equals("Genetic Hypertension Risk")) {
            binding.textView1Speed.visibility = View.VISIBLE
            binding.textView2Speed.visibility = View.GONE
            binding.textView3Speed.visibility = View.GONE
            binding.textView4Speed.visibility = View.GONE
            binding.textView5Speed.visibility = View.GONE
            binding.textView6Speed.visibility = View.GONE
            binding.textView7Speed.visibility = View.VISIBLE
            binding.textView1Speed.text = "Lower\nRisk"
            binding.textView7Speed.text = "Slight\nRisk"
        } else if (spinnerVal.equals("Genetic Infection Risk (colds/flu)")) {
            binding.textView1Speed.visibility = View.VISIBLE
            binding.textView2Speed.visibility = View.GONE
            binding.textView3Speed.visibility = View.GONE
            binding.textView4Speed.visibility = View.GONE
            binding.textView5Speed.visibility = View.GONE
            binding.textView6Speed.visibility = View.GONE
            binding.textView7Speed.visibility = View.VISIBLE
            binding.textView1Speed.text = "Normal\nRisk"
            binding.textView7Speed.text = "Slight\nRisk"
        } else if (spinnerVal.equals("Warrior Vs Worrier")) {
            binding.textView1Speed.visibility = View.VISIBLE
            binding.textView2Speed.visibility = View.GONE
            binding.textView3Speed.visibility = View.GONE
            binding.textView4Speed.visibility = View.VISIBLE
            binding.textView5Speed.visibility = View.GONE
            binding.textView6Speed.visibility = View.GONE
            binding.textView7Speed.visibility = View.VISIBLE
            binding.textView1Speed.text = "Worrier"
            binding.textView4Speed.text = "Mixed"
            binding.textView7Speed.text = "Warrior"
        }


    }


    private fun getKeyData(categoryId: String, genoTypeId: String) {

        Log.d("data", "get key data " + categoryId + " " + genoTypeId)
        binding.progressBar.visibility = View.VISIBLE

        val manager = NetworkManager()
        if (manager.isConnectingToInternet(requireContext())) {

            // System.out.println("genoTypeId****--> $genoTypeId  modeID******--> $modeID categoryId==>   $categoryId")

            //val userId = Base64.decode(Utility.getUserID(requireContext()), Base64.DEFAULT)
            //val decodeUserId = String(userId, charset("UTF-8"))

            manager.createApiRequest(
                ApiUtilis.getAPIInstance(activity!!).getKeyDataByCategory(
                    Utility.getUserID(requireContext()),
                    modeID!!,
                    "false",
                    "true",
                    categoryId,
                    genoTypeId
                ),
                object : ServiceListener<SectionByCategoryModel> {
                    override fun getServerResponse(
                        response: SectionByCategoryModel,
                        requestcode: Int
                    ) {
                        binding.progressBar.visibility = View.GONE
                        //dismissProgressDialog()
                        if (response.getCode() == 200 && response.getMessage() == "Sections for user found") {

                            setData(response.getData()!!)
                        }

                    }

                    override fun getError(error: ErrorModel, requestcode: Int) {
                        //dismissProgressDialog()
                        binding.progressBar.visibility = View.GONE

                    }
                })
        } else {
            binding.progressBar.visibility = View.GONE
            //    dismissProgressDialog()
            Utility.displayShortSnackBar(
                binding.parentLayout,
                resources.getString(R.string.check_internet)
            )

        }
    }

    private fun setData(data: SectionByCategoryData) {

        val degree = 10 * data.getActiveIndicator()!!.getDegree()!!.toInt() / 18
        // Log.d("data","degree--- "+data.getActiveIndicator()!!.getDegree()+" "+degree)
        if (data.getDescription() != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                binding.txtDescription.text =
                    Html.fromHtml(data.getDescription(), Html.FROM_HTML_MODE_COMPACT)
            } else {
                binding.txtDescription.text = Html.fromHtml(data.getDescription())
            }
        } else {
            binding.txtDescription.visibility = View.GONE
        }
        if (data.getIndicatorDescription() != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                binding.txtIndicatorDescription.text = Html.fromHtml(
                    data.getIndicatorDescription(),
                    Html.FROM_HTML_MODE_COMPACT
                )
            } else {
                binding.txtIndicatorDescription.text = Html.fromHtml(data.getIndicatorDescription())
            }
        } else {
            binding.txtIndicatorDescription.visibility = View.GONE
        }
        if (data.getInterests() != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                binding.txtInterests.text = Html.fromHtml(
                    "Genes of interest: " + data.getInterests(),
                    Html.FROM_HTML_MODE_COMPACT
                );
            } else {
                binding.txtInterests.text =
                    Html.fromHtml("Genes of interest: " + data.getDescription())
            }
        } else {
            binding.txtInterests.visibility = View.GONE
        }
        if (data.getMessage() != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                binding.txtMessage.text =
                    Html.fromHtml(data.getMessage(), Html.FROM_HTML_MODE_COMPACT)
            } else {
                binding.txtMessage.text = Html.fromHtml(data.getMessage())
            }
        } else {
            binding.txtMessage.visibility = View.GONE
        }
        if (data.getActiveIndicator()!!.getIndex() != null) {


            binding.speedView.speedTo(degree.toFloat(), 0)
        } else {
            binding.speedView.speedTo(degree.toFloat(), 0)
        }

        // binding.txtSectionName.text = data.getActiveIndicator()!!.getTitle()

        val typefaceNormal = context?.let { ResourcesCompat.getFont(it, R.font.opensans_semibold) }
        val typefaceBold = context?.let { ResourcesCompat.getFont(it, R.font.roboto_medium) }
        /*if(data.getActiveIndicator()!!.getTitle().equals("GIFTED")){
            binding.textViewSpeedGifted.setTypeface(typefaceBold)
            binding.textViewSpeedNormal.setTypeface(typefaceNormal)
            binding.textViewSpeedAboveNormal.setTypeface(typefaceNormal)
            binding.textViewSpeedHighlyGifted.setTypeface(typefaceNormal)
            binding.textViewSpeedElite.setTypeface(typefaceNormal)
        }
        else if(data.getActiveIndicator()!!.getTitle().equals("NORMAL")){
            binding.textViewSpeedNormal.setTypeface(typefaceBold)
            binding.textViewSpeedGifted.setTypeface(typefaceNormal)
            binding.textViewSpeedAboveNormal.setTypeface(typefaceNormal)
            binding.textViewSpeedHighlyGifted.setTypeface(typefaceNormal)
            binding.textViewSpeedElite.setTypeface(typefaceNormal)
        }
        else if(data.getActiveIndicator()!!.getTitle().equals("ABOVE NORMAL")){
            binding.textViewSpeedAboveNormal.setTypeface(typefaceBold)
            binding.textViewSpeedGifted.setTypeface(typefaceNormal)
            binding.textViewSpeedHighlyGifted.setTypeface(typefaceNormal)
            binding.textViewSpeedElite.setTypeface(typefaceNormal)
            binding.textViewSpeedNormal.setTypeface(typefaceNormal)
        }

        else if(data.getActiveIndicator()!!.getTitle().equals("HIGHLY GIFTED")){
            binding.textViewSpeedHighlyGifted.setTypeface(typefaceBold)
            binding.textViewSpeedGifted.setTypeface(Typeface.DEFAULT)
            binding.textViewSpeedAboveNormal.setTypeface(Typeface.DEFAULT)
            binding.textViewSpeedElite.setTypeface(Typeface.DEFAULT)
            binding.textViewSpeedNormal.setTypeface(Typeface.DEFAULT)
        }

        else if(data.getActiveIndicator()!!.getTitle().equals("ELITE")){
            binding.textViewSpeedElite.setTypeface(typefaceBold)
            binding.textViewSpeedGifted.setTypeface(typefaceNormal)
            binding.textViewSpeedHighlyGifted.setTypeface(typefaceNormal)
            binding.textViewSpeedAboveNormal.setTypeface(typefaceNormal)
            binding.textViewSpeedNormal.setTypeface(typefaceNormal)

        }*/

        /*val handler = Handler()
              handler.postDelayed({
                  // for autoLogin user

                  binding.scrollView.fullScroll(View.FOCUS_UP)

              }, 3000)*/

    }


    fun Dialog(context: Context) {
        try {
            progressDialog = ProgressDialog(getContext())
            progressDialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
            progressDialog!!.setProgressStyle(ProgressDialog.STYLE_SPINNER)
            progressDialog!!.setMessage(getString(R.string.please_wait))
            progressDialog!!.setCancelable(false)
        } catch (ignore: Exception) {
        }

    }

    fun showProgressDialog() {
        try {
            if (progressDialog != null) {
                progressDialog!!.show()
            }
        } catch (ignore: Exception) {
        }

    }

    fun dismissProgressDialog() {
        try {
            if (progressDialog != null) {
                progressDialog!!.dismiss()
            }
        } catch (ignore: Exception) {
        }

    }
}