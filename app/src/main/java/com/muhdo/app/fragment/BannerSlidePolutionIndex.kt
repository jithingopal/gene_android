package com.muhdo.app.fragment

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.muhdo.app.R
import com.muhdo.app.apiModel.pollution.AirIndexData
import com.muhdo.app.apiModel.pollution.AirIndexModel
import com.muhdo.app.databinding.FragmentBannerSlidePollutionIndexBinding
import com.muhdo.app.repository.ApiUtilis
import com.muhdo.app.repository.ErrorModel
import com.muhdo.app.repository.NetworkManager
import com.muhdo.app.repository.ServiceListener
import com.timingsystemkotlin.backuptimingsystem.Utility
import java.util.*
import kotlin.math.roundToInt


private lateinit var binding: FragmentBannerSlidePollutionIndexBinding
var response: AirIndexData? = null

class BannerSlidePollutionIndex : Fragment() {
    private lateinit var completeResponse: AirIndexModel
    private var percentage = 0

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view =
            inflater.inflate(R.layout.fragment_banner_slide_pollution_index, container, false)
        binding = FragmentBannerSlidePollutionIndexBinding.bind(view)
        if (Utility.getLat(requireContext()).isNullOrEmpty() && Utility.getLong(requireContext()).isNullOrEmpty()) {
            Toast.makeText(requireContext(), "Unable to fetch location", Toast.LENGTH_SHORT).show()
        } else {
            getPollutionData(Utility.getLong(requireContext()), Utility.getLat(requireContext()))
        }
        return view
    }

    private fun getPollutionData(long: String?, lat: String?) {
        // binding.progressBar.visibility = View.VISIBLE
        if (!lat.isNullOrEmpty() && !long.isNullOrEmpty()) {

            val params = HashMap<String, String>()
            params["latlong"] = "$lat, $long"
            params["user_id"] = Utility.getUserID(requireContext())

            val manager = NetworkManager()
            if (manager.isConnectingToInternet(requireContext())) {
                manager.createApiRequest(
                    ApiUtilis.getAPIInstance(activity!!).getPollutionDataV2(
                        params
                    ),
                    //  RetrofitDevHelper.getRetrofitInstance(2).getPollutionDataV2(params)
                    //  manager.createApiRequest(
                    //        ApiUtilis.getAPIService(Constants.POLLUTION_API).getPollutionData(params),
                    object : ServiceListener<AirIndexModel> {
                        override fun getServerResponse(response: AirIndexModel, requestcode: Int) {
                            //  binding.progressBar.visibility = View.GONE
                            if (response.getStatusCode() == 200) {
                                completeResponse = response
                                completeResponse.data?.let { setData(it) }
                            } else {
                                if (activity != null) {
                                    Toast.makeText(
                                        activity,
                                        "Something went wrong",
                                        Toast.LENGTH_LONG
                                    )
                                        .show()
                                }
                            }
                        }

                        override fun getError(error: ErrorModel, requestcode: Int) {
                            if (activity != null && activity!!.applicationContext != null) {
                                //
                                // Toast.makeText(activity!!.applicationContext, "server error", Toast.LENGTH_LONG).show()
                            }
                            //  binding.progressBar.visibility = View.GONE
                        }
                    })
            } else {
                //binding.progressBar.visibility = View.GONE
                Toast.makeText(
                    requireContext(),
                    "Please check internet connection!",
                    Toast.LENGTH_LONG
                )
                    .show()
            }
        }
    }

    private fun setData(data: AirIndexData) {
        response = data
        //   binding.dcpAirPollution.setBottomText(data.getCategory().toString())
        //  binding.complianceObservedCompliance.text = data.getCategory()
        data.getAqi()?.let { airIndexValue ->
            Log.d("aqiValue", airIndexValue.toString())
            when (airIndexValue) {
                //Green
                in 0..50 -> {
                    percentage = (airIndexValue * 22.5 / 12.5 * 100 / 360).roundToInt()
                    // percentage = (25 * airIndexValue) / 50
                }
                //Yellow
                in 51..100 -> {
                    percentage =
                        25 + (((airIndexValue - 50) * 22.5 / 12.5 * 100 / 360).roundToInt())
                    //     percentage = 25 + (25 * airIndexValue) / 100
                }
                //Red
                in 101..150 -> {
                    percentage =
                        50 + (((airIndexValue - 100) * 22.5 / 12.5 * 100 / 360).roundToInt())
                }
                //Maroon 1st
                in 151..500 -> {
                    percentage =
                        75 + (((airIndexValue - 150) * 22.5 / 87.5 * 100 / 360).roundToInt())
                    //    percentage = 50 + (25 * airIndexValue) / 100
                }
                else -> {
                    percentage = 100
                }
                /*//Maroon 2nd
                 TODO("To be done as per changes in AQI")
                in 201..300 -> {
                    percentage =
                        50 + (((airIndexValue - 100) * 22.5 / 12.5 * 100 / 360).roundToInt())
                    //    percentage = 50 + (25 * airIndexValue) / 100
                }
                //Maroon 3rd
                in 301..400 -> {
                    percentage =
                        50 + (((airIndexValue - 100) * 22.5 / 12.5 * 100 / 360).roundToInt())
                    //    percentage = 50 + (25 * airIndexValue) / 100
                }
                //Maroon 4th
                in 401..999 -> {
                    percentage = 75 + ((airIndexValue - 150) * 22.5 / 50 * 100 / 360).roundToInt()
                }*/
            }
            binding.dcpAirPollution.setProgress(
                airIndexValue,
                percentage.toFloat(),
                data.getCategory().toString().toUpperCase(Locale.ENGLISH)
            )
        }

        /*      data.getAqi()?.toFloat()
                  ?.let { binding.dcpAirPollution.setProgress(it, data.getCategory().toString()) }*/
        // binding.compliancePercentage.text = data.getAqi().toString()
        //     val progress = ((data.getAqi()!! * 100) / 500)
        // binding.circleProgressBar.progress = progress
        // val anim = ProgressBarAnimation(binding.circleProgressBar, 0F, progress.toFloat())
        // anim.duration = 1000
        //  binding.circleProgressBar.startAnimation(anim)
    }

    @SuppressLint("WrongConstant")
    private fun replaceFragment(fragment: Fragment) {

        val transaction = activity!!.supportFragmentManager.beginTransaction()
        transaction.replace(R.id.flContent, fragment)
        transaction.addToBackStack("share")
        transaction.commit()
    }
}
