package com.muhdo.app.fragment

import android.annotation.SuppressLint
import android.os.Bundle
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.google.gson.Gson
import com.muhdo.app.R
import com.muhdo.app.apiModel.workout.WorkoutPlanData
import com.muhdo.app.apiModel.workout.WorkoutPlanModel
import com.muhdo.app.databinding.FragmentMyPlanBinding
import com.muhdo.app.repository.ApiUtilis
import com.muhdo.app.repository.ErrorModel
import com.muhdo.app.repository.NetworkManager
import com.muhdo.app.repository.ServiceListener
import com.muhdo.app.utils.v3.Constants
import com.timingsystemkotlin.backuptimingsystem.Utility

class MyPlanFragment : Fragment(), View.OnClickListener {
    override fun onClick(p0: View?) {

        when (p0!!.id) {
            R.id.img_previous -> {
                if (weekCount > 0) {
                    weekCount--
                    if (activity != null && isAdded)
                        data?.getData()?.let {
                            addBottomDots(weekCount)
                        }
                }
            }
            R.id.img_next -> {
                if (weekCount < 3) {
                    weekCount++
                    if (activity != null && isAdded)
                        data?.getData()?.let {
                            addBottomDots(weekCount)
                        }
                }
            }
        }
    }

    internal lateinit var binding: FragmentMyPlanBinding
    private var weekCount: Int = 0
    private var differenceCount: Int = 0
    private var data: WorkoutPlanModel? = null
    internal lateinit var parentFragment: Fragment

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_my_plan, container, false)
        binding = FragmentMyPlanBinding.bind(view)
        binding.imgNext.setOnClickListener(this)
        binding.imgPrevious.setOnClickListener(this)
        getWorkoutList()

        return view
    }

    fun setChildFraMang(parentFragment: Fragment) {
        this.parentFragment = parentFragment
    }

    private fun getWorkoutList() {
        binding.progressBar.visibility = View.VISIBLE
        val manager = NetworkManager()
        if (manager.isConnectingToInternet(activity!!.applicationContext)) {
            manager.createApiRequest(

                ApiUtilis.getAPIInstance(activity!!).getWorkoutPlan(
                    Utility.getUserID(
                        requireContext()
                    )
                ),
                object : ServiceListener<WorkoutPlanModel> {
                    override fun getServerResponse(response: WorkoutPlanModel, requestcode: Int) {
                        data = response
                        binding.progressBar.visibility = View.GONE
                        if (activity != null && isAdded)
                            data?.getData()?.let {
                                addBottomDots(weekCount)
                            }
                    }

                    override fun getError(error: ErrorModel, requestcode: Int) {
                        binding.progressBar.visibility = View.GONE

                    }
                })
        } else {
            binding.progressBar.visibility = View.GONE
            Utility.displayShortSnackBar(
                binding.parentLayout,
                resources.getString(R.string.check_internet)
            )

        }

    }

    @SuppressLint("ResourceType")
    private fun passData(response: WorkoutPlanData, position: Int) {

        if (activity != null && isAdded) {
            val transaction = childFragmentManager.beginTransaction()
            if (weekCount > 0) {
                transaction.setCustomAnimations(R.anim.exit, R.anim.enter)
            }

            val bundle = Bundle()
            val gson = Gson()
            when (position) {
                0 -> {

                    bundle.putString(Constants.WEEKLY_WORKOUT_MESSAGE, gson.toJson(response))
                }
                1 -> {
                    bundle.putString(Constants.WEEKLY_WORKOUT_MESSAGE, gson.toJson(response))

                }
                2 -> {
                    bundle.putString(Constants.WEEKLY_WORKOUT_MESSAGE, gson.toJson(response))

                }
                3 -> {
                    bundle.putString(Constants.WEEKLY_WORKOUT_MESSAGE, gson.toJson(response))

                }

            }


            val fragInfo = DisplayWorkoutFragment()
            fragInfo.arguments = bundle
            transaction.replace(R.id.flContent, fragInfo)
            transaction.commit()
        }

    }

    private fun addBottomDots(currentPage: Int) {
        differenceCount = currentPage - differenceCount
        when (currentPage) {
            0 -> {
                binding.txtWeek.text = activity?.resources?.getString(R.string.week_1)
                binding.imgPrevious.visibility = View.GONE
                binding.imgNext.visibility = View.VISIBLE
            }
            1 -> {
                binding.txtWeek.text = requireActivity().resources.getString(R.string.week_2)
                binding.imgPrevious.visibility = View.VISIBLE
                binding.imgNext.visibility = View.VISIBLE
            }
            2 -> {
                binding.txtWeek.text = requireActivity().resources.getString(R.string.week_3)
                binding.imgPrevious.visibility = View.VISIBLE
                binding.imgNext.visibility = View.VISIBLE
            }
            3 -> {
                binding.txtWeek.text = requireActivity().resources.getString(R.string.week_4)
                binding.imgPrevious.visibility = View.VISIBLE
                binding.imgNext.visibility = View.VISIBLE
            }

        }
        data?.getData()?.let {
            passData(it, currentPage)
        }

        val dots = arrayOfNulls<TextView>(4)
        binding.layoutDots.removeAllViews()
        for (i in dots.indices) {

            dots[i] = TextView(activity)
            dots[i]!!.text = Html.fromHtml("&#8226;")
            dots[i]!!.textSize = 35f
            dots[i]!!.setTextColor(ContextCompat.getColor(activity!!, R.color.secondary_color))
            binding.layoutDots.addView(dots[i])
        }

        if (dots.isNotEmpty())
            dots[currentPage]!!.setTextColor(
                ContextCompat.getColor(
                    activity!!,
                    R.color.color_dark_blue
                )
            )
    }

}