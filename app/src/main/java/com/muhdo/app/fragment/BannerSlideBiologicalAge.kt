package com.muhdo.app.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.muhdo.app.R
import com.muhdo.app.databinding.FragmentBannerSleepIndexBinding
import com.muhdo.app.databinding.FragmentBannerSlideBiologicalAgeBinding
import com.muhdo.app.utils.PreferenceConnector


private lateinit var binding: FragmentBannerSlideBiologicalAgeBinding
class BannerSlideBiologicalAge : Fragment() {


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val view = inflater.inflate(R.layout.fragment_banner_slide_biological_age, container, false)
        binding = FragmentBannerSlideBiologicalAgeBinding.bind(view)
        val biologicalAge = Integer.parseInt(PreferenceConnector.readString(activity!!,PreferenceConnector.BIOLOGICAL_AGE,"0"))
        binding.biologicalAge.text = biologicalAge.toString()

        return view
    }
}
