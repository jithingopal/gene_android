package com.muhdo.app.fragment.v3.guide.meal

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.muhdo.app.R
import com.muhdo.app.databinding.V3LayoutPlanMealChooseAcytivityLevelBinding
import com.muhdo.app.databinding.V3LayoutPlanWorkoutChooseEquipmentBinding
import com.muhdo.app.utils.v3.TempUtil

class ChooseActivityLevelFragment : Fragment() {
    lateinit var binding: V3LayoutPlanMealChooseAcytivityLevelBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view =
            inflater.inflate(R.layout.v3_layout_plan_meal_choose_acytivity_level, container, false)
        binding= V3LayoutPlanMealChooseAcytivityLevelBinding.bind(view)
        setupActivityLevelListener()
        return view
    }

    fun getActivitylevel():Int?{
        if(binding.cbActivityLevel7.isChecked){
            return 29
        }else if(binding.cbActivityLevel6.isChecked){
            return 28
        }else if(binding.cbActivityLevel5.isChecked){
            return 27

        }else if(binding.cbActivityLevel4.isChecked){
            return 26

        }else if(binding.cbActivityLevel3.isChecked){
            return 25

        }else if(binding.cbActivityLevel2.isChecked){
            return 24

        }else if(binding.cbActivityLevel1.isChecked){
            return 23
        } else {
            return null
        }
    }
private fun removeAllCheckBoxListeners(){
    binding.cbActivityLevel1.setOnCheckedChangeListener(null)
    binding.cbActivityLevel2.setOnCheckedChangeListener(null)
    binding.cbActivityLevel3.setOnCheckedChangeListener(null)
    binding.cbActivityLevel4.setOnCheckedChangeListener(null)
    binding.cbActivityLevel5.setOnCheckedChangeListener(null)
    binding.cbActivityLevel6.setOnCheckedChangeListener(null)
    binding.cbActivityLevel7.setOnCheckedChangeListener(null)
}
    private fun setupActivityLevelListener() {
        binding.layoutCb1.setOnClickListener{ TempUtil.log("layoutCb1","cbActivityLevel1");binding.cbActivityLevel1.isChecked=true}
        binding.layoutCb2.setOnClickListener{TempUtil.log("layoutCb2","cbActivityLevel2");binding.cbActivityLevel2.isChecked=true}
        binding.layoutCb3.setOnClickListener{TempUtil.log("layoutCb3","cbActivityLevel3");binding.cbActivityLevel3.isChecked=true}
        binding.layoutCb4.setOnClickListener{TempUtil.log("layoutCb4","cbActivityLevel4");binding.cbActivityLevel4.isChecked=true}
        binding.layoutCb5.setOnClickListener{TempUtil.log("layoutCb5","cbActivityLevel5");binding.cbActivityLevel5.isChecked=true}
        binding.layoutCb6.setOnClickListener{TempUtil.log("layoutCb6","cbActivityLevel6");binding.cbActivityLevel6.isChecked=true}
        binding.layoutCb7.setOnClickListener{TempUtil.log("layoutCb7","cbActivityLevel7");binding.cbActivityLevel7.isChecked=true}
        binding.cbActivityLevel1.setOnCheckedChangeListener { buttonView, isChecked ->
            removeAllCheckBoxListeners()
            binding.cbActivityLevel1.isChecked = true
            binding.cbActivityLevel2.isChecked = false
            binding.cbActivityLevel3.isChecked = false
            binding.cbActivityLevel4.isChecked = false
            binding.cbActivityLevel5.isChecked = false
            binding.cbActivityLevel6.isChecked = false
            binding.cbActivityLevel7.isChecked = false
            binding.tvActivityLevelIndicatorText.setText(R.string.activity_text_1)
            setupActivityLevelListener()
            TempUtil.log("cbActivityLevel1","layoutCb1" )
        }

        binding.cbActivityLevel2.setOnCheckedChangeListener { buttonView, isChecked ->
            removeAllCheckBoxListeners()
            binding.cbActivityLevel1.isChecked = true
            binding.cbActivityLevel2.isChecked = true
            binding.cbActivityLevel3.isChecked = false
            binding.cbActivityLevel4.isChecked = false
            binding.cbActivityLevel5.isChecked = false
            binding.cbActivityLevel6.isChecked = false
            binding.cbActivityLevel7.isChecked = false
            binding.tvActivityLevelIndicatorText.setText(R.string.activity_text_2)
            setupActivityLevelListener()
            TempUtil.log("cbActivityLevel2","layoutCb2" )
        }
        binding.cbActivityLevel3.setOnCheckedChangeListener { buttonView, isChecked ->
            removeAllCheckBoxListeners()
            binding.cbActivityLevel1.isChecked = true
            binding.cbActivityLevel2.isChecked = true
            binding.cbActivityLevel3.isChecked = true
            binding.cbActivityLevel4.isChecked = false
            binding.cbActivityLevel5.isChecked = false
            binding.cbActivityLevel6.isChecked = false
            binding.cbActivityLevel7.isChecked = false
            binding.tvActivityLevelIndicatorText.setText(R.string.activity_text_3)
            setupActivityLevelListener()
            TempUtil.log("cbActivityLevel3","layoutCb3" )
        }
        binding.cbActivityLevel4.setOnCheckedChangeListener { buttonView, isChecked ->
            removeAllCheckBoxListeners()
            binding.cbActivityLevel1.isChecked = true
            binding.cbActivityLevel2.isChecked = true
            binding.cbActivityLevel3.isChecked = true
            binding.cbActivityLevel4.isChecked = true
            binding.cbActivityLevel5.isChecked = false
            binding.cbActivityLevel6.isChecked = false
            binding.cbActivityLevel7.isChecked = false
            binding.tvActivityLevelIndicatorText.setText(R.string.activity_text_4)
            setupActivityLevelListener()
            TempUtil.log("cbActivityLevel4","layoutCb4" )
        }
        binding.cbActivityLevel5.setOnCheckedChangeListener { buttonView, isChecked ->
            removeAllCheckBoxListeners()
            binding.cbActivityLevel1.isChecked = true
            binding.cbActivityLevel2.isChecked = true
            binding.cbActivityLevel3.isChecked = true
            binding.cbActivityLevel4.isChecked = true
            binding.cbActivityLevel5.isChecked = true
            binding.cbActivityLevel6.isChecked = false
            binding.cbActivityLevel7.isChecked = false
            binding.tvActivityLevelIndicatorText.setText(R.string.activity_text_5)
            setupActivityLevelListener()
            TempUtil.log("cbActivityLevel5","layoutCb5" )
        }
        binding.cbActivityLevel6.setOnCheckedChangeListener { buttonView, isChecked ->
            removeAllCheckBoxListeners()
            binding.cbActivityLevel1.isChecked = true
            binding.cbActivityLevel2.isChecked = true
            binding.cbActivityLevel3.isChecked = true
            binding.cbActivityLevel4.isChecked = true
            binding.cbActivityLevel5.isChecked = true
            binding.cbActivityLevel6.isChecked = true
            binding.cbActivityLevel7.isChecked = false
            binding.tvActivityLevelIndicatorText.setText(R.string.activity_text_6)
            setupActivityLevelListener()
            TempUtil.log("cbActivityLevel6","layoutCb6" )
        }
        binding.cbActivityLevel7.setOnCheckedChangeListener { buttonView, isChecked ->
            removeAllCheckBoxListeners()
            binding.cbActivityLevel1.isChecked = true
            binding.cbActivityLevel2.isChecked = true
            binding.cbActivityLevel3.isChecked = true
            binding.cbActivityLevel4.isChecked = true
            binding.cbActivityLevel5.isChecked = true
            binding.cbActivityLevel6.isChecked = true
            binding.cbActivityLevel7.isChecked = true
            binding.tvActivityLevelIndicatorText.setText(R.string.activity_text_7)
            setupActivityLevelListener()
            TempUtil.log("cbActivityLevel7","layoutCb7" )
        }

    }

}
