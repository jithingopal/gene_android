package com.muhdo.app.fragment

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.jaygoo.widget.OnRangeChangedListener
import com.jaygoo.widget.RangeSeekBar
import com.muhdo.app.R
import com.muhdo.app.databinding.FragmentWorkoutQestionAnswerBinding
import com.muhdo.app.utils.Events
import com.muhdo.app.utils.GlobalBus
import com.muhdo.app.utils.v3.Constants
import com.timingsystemkotlin.backuptimingsystem.Utility
import kotlinx.android.synthetic.main.layout_workout_equipment.view.*
import org.greenrobot.eventbus.Subscribe


class WorkOutQuestionAnswerFragment : Fragment() {
    private var edtHeightValue: String = ""
    var callback: OnDataInputListener? = null

    interface OnDataInputListener {
        /* TODO: Update argument type and name */
        fun onDataInput(questionType: String, questionData: String)
    }

    private lateinit var binding: FragmentWorkoutQestionAnswerBinding
    private var isGenderSelected = false
    internal var count = 0


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        GlobalBus.getBus()!!.register(this@WorkOutQuestionAnswerFragment)
        val view = inflater.inflate(R.layout.fragment_workout_qestion_answer, container, false)
        binding = FragmentWorkoutQestionAnswerBinding.bind(view)
        activity!!.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
        val data = this.arguments!!.getString(Constants.WORKOUT_QUESTION_TYPE)
        showView(data!!)
        validateGoalQuestionnaire()
        validatePersonalQuestionnaire()
        validateBMIQuestionnaire()
        validateTrainingQuestionnaire()
        validateEquipmentQuestionnaire()
        validateExperienceQuestionnaire()
        validateMedicalDetailsQuestionnaire()
        validateMedicalDiagnoseQuestionnaire()

        return view
    }

    private fun showView(data: String) {
        when (data) {
            Constants.WORKOUT_QUESTION_TYPE_GOAL -> {

                binding.layoutGoal.root.visibility = VISIBLE
            }
            Constants.WORKOUT_QUESTION_TYPE_PERSONAL -> {
                binding.layoutPersonalDetails.root.visibility = VISIBLE
            }
            Constants.WORKOUT_QUESTION_TYPE_BMI -> {
                binding.layoutBmi.btnCm.isChecked = true
                binding.layoutBmi.btnKg.isChecked = true
                binding.layoutBmi.root.visibility = VISIBLE
            }
            Constants.WORKOUT_QUESTION_TYPE_TRAINING -> {
                binding.layoutWorkoutTrainingDetails.root.visibility = VISIBLE
            }
            Constants.WORKOUT_QUESTION_TYPE_EQUIPMENT -> {
                binding.layoutWorkoutEquipment.root.visibility = VISIBLE
            }
            Constants.WORKOUT_QUESTION_TYPE_EXPERIENCE -> {

                binding.layoutWorkoutExperience.root.visibility = VISIBLE
            }
            Constants.WORKOUT_QUESTION_TYPE_MEDICAL -> {
                binding.layoutWorkoutMedical.root.visibility = VISIBLE
            }
            Constants.WORKOUT_QUESTION_TYPE_DIAGNOSED -> {
                binding.layoutMedicalDiagnose.root.visibility = VISIBLE
            }


        }
    }


    private fun validateExperienceQuestionnaire() {

        binding.layoutWorkoutExperience.radioExperience.setOnCheckedChangeListener { _, checkedId ->
            when (checkedId) {
                R.id.btn_experiment_novice -> {

                    Utility.listValue[9] = "46"
                    binding.layoutWorkoutExperience.btnExperimentNovice.isChecked = true
                    binding.layoutWorkoutExperience.btnExperimentIntermediate.isChecked = false
                    binding.layoutWorkoutExperience.btnExperimentAdvanced.isChecked = false
                    binding.layoutWorkoutExperience.btnExperimentHighlyAdvanced.isChecked = false
                    binding.layoutWorkoutExperience.btnExperimentExpert.isChecked = false
                    binding.layoutWorkoutExperience.btnExperimentElite.isChecked = false
                    Utility.setExperience(activity!!.applicationContext, true)
                    getPage(6)
                }
                R.id.btn_experiment_intermediate -> {
                    Utility.listValue[9] = "47"
                    binding.layoutWorkoutExperience.btnExperimentNovice.isChecked = false
                    binding.layoutWorkoutExperience.btnExperimentIntermediate.isChecked = true
                    binding.layoutWorkoutExperience.btnExperimentAdvanced.isChecked = false
                    binding.layoutWorkoutExperience.btnExperimentHighlyAdvanced.isChecked = false
                    binding.layoutWorkoutExperience.btnExperimentExpert.isChecked = false
                    binding.layoutWorkoutExperience.btnExperimentElite.isChecked = false
                    Utility.setExperience(activity!!.applicationContext, true)
                    getPage(6)
                }
                R.id.btn_experiment_advanced -> {

                    Utility.listValue[9] = "48"
                    binding.layoutWorkoutExperience.btnExperimentNovice.isChecked = false
                    binding.layoutWorkoutExperience.btnExperimentIntermediate.isChecked = false
                    binding.layoutWorkoutExperience.btnExperimentAdvanced.isChecked = true
                    binding.layoutWorkoutExperience.btnExperimentHighlyAdvanced.isChecked = false
                    binding.layoutWorkoutExperience.btnExperimentExpert.isChecked = false
                    binding.layoutWorkoutExperience.btnExperimentElite.isChecked = false
                    Utility.setExperience(activity!!.applicationContext, true)
                    getPage(6)
                }
                R.id.btn_experiment_highly_advanced -> {

                    Utility.listValue[9] = "49"
                    binding.layoutWorkoutExperience.btnExperimentNovice.isChecked = false
                    binding.layoutWorkoutExperience.btnExperimentIntermediate.isChecked = false
                    binding.layoutWorkoutExperience.btnExperimentAdvanced.isChecked = false
                    binding.layoutWorkoutExperience.btnExperimentHighlyAdvanced.isChecked = true
                    binding.layoutWorkoutExperience.btnExperimentExpert.isChecked = false
                    binding.layoutWorkoutExperience.btnExperimentElite.isChecked = false
                    Utility.setExperience(activity!!.applicationContext, true)
                    getPage(6)
                }
                R.id.btn_experiment_expert -> {

                    Utility.listValue[9] = "50"
                    binding.layoutWorkoutExperience.btnExperimentNovice.isChecked = false
                    binding.layoutWorkoutExperience.btnExperimentIntermediate.isChecked = false
                    binding.layoutWorkoutExperience.btnExperimentAdvanced.isChecked = false
                    binding.layoutWorkoutExperience.btnExperimentHighlyAdvanced.isChecked = false
                    binding.layoutWorkoutExperience.btnExperimentExpert.isChecked = true
                    binding.layoutWorkoutExperience.btnExperimentElite.isChecked = false
                    Utility.setExperience(activity!!.applicationContext, true)
                    getPage(6)
                }
                R.id.btn_experiment_elite -> {

                    Utility.listValue[9] = "51"
                    binding.layoutWorkoutExperience.btnExperimentNovice.isChecked = false
                    binding.layoutWorkoutExperience.btnExperimentIntermediate.isChecked = false
                    binding.layoutWorkoutExperience.btnExperimentAdvanced.isChecked = false
                    binding.layoutWorkoutExperience.btnExperimentHighlyAdvanced.isChecked = false
                    binding.layoutWorkoutExperience.btnExperimentExpert.isChecked = false
                    binding.layoutWorkoutExperience.btnExperimentElite.isChecked = true
                    Utility.setExperience(activity!!.applicationContext, true)
                    getPage(6)
                }

            }

        }
    }


    private fun validateEquipmentQuestionnaire() {
        binding.layoutWorkoutEquipment
        if (Utility.listValue[0] == "B" || Utility.listValue[0] == "C") {
            binding.layoutWorkoutEquipment.radioGoal.btn_equipment_indoor.visibility = VISIBLE
        }


        binding.layoutWorkoutEquipment.radioGoal.setOnCheckedChangeListener { _, checkedId ->
            when (checkedId) {
                R.id.btn_equipment_gym -> {
                    Utility.listValue[1] = "21"
                    binding.layoutWorkoutEquipment.btnEquipmentGym.isChecked = true
                    binding.layoutWorkoutEquipment.btnEquipmentOutdoor.isChecked = false
                    binding.layoutWorkoutEquipment.btnEquipmentIndoor.isChecked = false
                    Utility.setEquipment(activity!!.applicationContext, true)
                    getPage(5)
                }
                R.id.btn_equipment_outdoor -> {
                    Utility.listValue[1] = "22"
                    binding.layoutWorkoutEquipment.btnEquipmentGym.isChecked = false
                    binding.layoutWorkoutEquipment.btnEquipmentOutdoor.isChecked = true
                    binding.layoutWorkoutEquipment.btnEquipmentIndoor.isChecked = false
                    Utility.setEquipment(activity!!.applicationContext, true)
                    getPage(5)
                }
                R.id.btn_equipment_indoor -> {

                    Utility.listValue[1] = "23"

                    binding.layoutWorkoutEquipment.btnEquipmentGym.isChecked = false
                    binding.layoutWorkoutEquipment.btnEquipmentOutdoor.isChecked = false
                    binding.layoutWorkoutEquipment.btnEquipmentIndoor.isChecked = true
                    Utility.setEquipment(activity!!.applicationContext, true)
                    getPage(5)
                }

            }

        }
    }

    @Subscribe
    fun getPage(page: Int) {

        val sendPageCount = Events.sendPageCount(page)

        GlobalBus.getBus()!!.post(sendPageCount)


    }

    @Subscribe
    fun getMessage(activityFragmentMessage: Events.ActivityFragmentMessage) {
    }


    override fun onDestroyView() {
        super.onDestroyView()
        // unregister the registered event.
        GlobalBus.getBus()!!.unregister(this)
    }


    private fun validateGoalQuestionnaire() {

        binding.layoutGoal.radioGoal.setOnCheckedChangeListener { _, checkedId ->
            when (checkedId) {
                R.id.btn_body_building -> {
                    Utility.listValue[0] = "A"
                    binding.layoutGoal.btnBodyBuilding.isChecked = true
                    binding.layoutGoal.btnFatLoss.isChecked = false
                    binding.layoutGoal.btnHealth.isChecked = false
                    binding.layoutGoal.btnFlexibility.isChecked = false
                    binding.layoutGoal.btnEndurance.isChecked = false
                    binding.layoutGoal.btnPowerLifter.isChecked = false
                    binding.layoutGoal.btnBodyFatLoss.isChecked = false
                    Utility.setGoalDone(activity!!.applicationContext, true)
                    getPage(1)
                }
                R.id.btn_fat_loss -> {
                    Utility.listValue[0] = "B"
                    binding.layoutGoal.btnBodyBuilding.isChecked = false
                    binding.layoutGoal.btnFatLoss.isChecked = true
                    binding.layoutGoal.btnHealth.isChecked = false
                    binding.layoutGoal.btnFlexibility.isChecked = false
                    binding.layoutGoal.btnEndurance.isChecked = false
                    binding.layoutGoal.btnPowerLifter.isChecked = false
                    binding.layoutGoal.btnBodyFatLoss.isChecked = false
                    Utility.setGoalDone(activity!!.applicationContext, true)
                    getPage(1)
                }
                R.id.btn_health -> {

                    Utility.listValue[0] = "C"
                    binding.layoutGoal.btnBodyBuilding.isChecked = false
                    binding.layoutGoal.btnFatLoss.isChecked = false
                    binding.layoutGoal.btnHealth.isChecked = true
                    binding.layoutGoal.btnFlexibility.isChecked = false
                    binding.layoutGoal.btnEndurance.isChecked = false
                    binding.layoutGoal.btnPowerLifter.isChecked = false
                    binding.layoutGoal.btnBodyFatLoss.isChecked = false
                    Utility.setGoalDone(activity!!.applicationContext, true)
                    getPage(1)
                }
                R.id.btn_flexibility -> {

                    Utility.listValue[0] = "D"
                    binding.layoutGoal.btnBodyBuilding.isChecked = false
                    binding.layoutGoal.btnFatLoss.isChecked = false
                    binding.layoutGoal.btnHealth.isChecked = false
                    binding.layoutGoal.btnFlexibility.isChecked = true
                    binding.layoutGoal.btnEndurance.isChecked = false
                    binding.layoutGoal.btnPowerLifter.isChecked = false
                    binding.layoutGoal.btnBodyFatLoss.isChecked = false
                    Utility.setGoalDone(activity!!.applicationContext, true)
                    getPage(1)
                }
                R.id.btn_endurance -> {

                    Utility.listValue[0] = "E"
                    binding.layoutGoal.btnBodyBuilding.isChecked = false
                    binding.layoutGoal.btnFatLoss.isChecked = false
                    binding.layoutGoal.btnHealth.isChecked = false
                    binding.layoutGoal.btnFlexibility.isChecked = false
                    binding.layoutGoal.btnEndurance.isChecked = true
                    binding.layoutGoal.btnPowerLifter.isChecked = false
                    binding.layoutGoal.btnBodyFatLoss.isChecked = false
                    Utility.setGoalDone(activity!!.applicationContext, true)
                    getPage(1)
                }
                R.id.btn_power_lifter -> {

                    Utility.listValue[0] = "F"
                    binding.layoutGoal.btnBodyBuilding.isChecked = false
                    binding.layoutGoal.btnFatLoss.isChecked = false
                    binding.layoutGoal.btnHealth.isChecked = false
                    binding.layoutGoal.btnFlexibility.isChecked = false
                    binding.layoutGoal.btnEndurance.isChecked = false
                    binding.layoutGoal.btnPowerLifter.isChecked = true
                    binding.layoutGoal.btnBodyFatLoss.isChecked = false
                    Utility.setGoalDone(activity!!.applicationContext, true)
                    getPage(1)
                }
                R.id.btn_body_fat_loss -> {

                    Utility.listValue[0] = "G"

                    binding.layoutGoal.btnBodyBuilding.isChecked = false
                    binding.layoutGoal.btnFatLoss.isChecked = false
                    binding.layoutGoal.btnHealth.isChecked = false
                    binding.layoutGoal.btnFlexibility.isChecked = false
                    binding.layoutGoal.btnEndurance.isChecked = false
                    binding.layoutGoal.btnPowerLifter.isChecked = false
                    binding.layoutGoal.btnBodyFatLoss.isChecked = true
                    Utility.setGoalDone(activity!!.applicationContext, true)
                    getPage(1)
                }
            }

        }
    }

    private fun validateMedicalDetailsQuestionnaire() {
        binding.layoutWorkoutMedical.radioExercise.setOnCheckedChangeListener { _, checkedId ->
            when (checkedId) {
                R.id.btn_exercise -> {
                    Utility.list[0] = true
                    Utility.setDR(activity!!.applicationContext, true)
                }
                R.id.btn_not_exercise -> {
                    Utility.list[0] = false
                    Utility.setDR(activity!!.applicationContext, true)
                }
            }

        }

        binding.layoutWorkoutMedical.radioInjury.setOnCheckedChangeListener { _, checkedId ->
            when (checkedId) {
                R.id.btn_injury_affect -> {
                    Utility.list[1] = true
                    Utility.setInjured(activity!!.applicationContext, true)
                }
                R.id.btn_injury_not_affect -> {
                    Utility.list[1] = false
                    Utility.setInjured(activity!!.applicationContext, true)
                }
            }

        }

        binding.layoutWorkoutMedical.radioPregnant.setOnCheckedChangeListener { _, checkedId ->
            when (checkedId) {
                R.id.btn_is_pregnant -> {
                    Utility.list[2] = true
                    Utility.setPregnant(activity!!.applicationContext, true)
                }
                R.id.btn_not_pregnant -> {
                    Utility.list[2] = false
                    Utility.setPregnant(activity!!.applicationContext, true)
                }
            }

        }
    }


    private fun validateBMIQuestionnaire() {
        Utility.listValue[2] = "cm"
        Utility.listValue[3] = "kg"
        binding.layoutBmi.btnCm.isChecked = true
        binding.layoutBmi.btnKg.isChecked = true
        formatHeightEdtTxt()
        formatWeightEdtTxt()
        binding.layoutBmi.radioHeightMeasure.setOnCheckedChangeListener { _, checkedId ->
            when (checkedId) {

                R.id.btn_cm -> {
                    Utility.listValue[2] = "cm"
                    if (edtHeightValue.isEmpty())
                        edtHeightValue = binding.layoutBmi.edtHeight.text.toString().trim()
                    binding.layoutBmi.edtHeight.setText(
                        Utility.convertFeetAndInchesToCentimeter(
                            edtHeightValue
                        )
                    )
                }
                R.id.btn_ft -> {
                    Utility.listValue[2] = "ft"
                    if (edtHeightValue.isEmpty())
                        edtHeightValue = binding.layoutBmi.edtHeight.text.toString().trim()
                    binding.layoutBmi.edtHeight.setText(Utility.centimeterToFeet(edtHeightValue))

                }
            }

        }


        binding.layoutBmi.radioWeightMeasure.setOnCheckedChangeListener { _, checkedId ->
            when (checkedId) {
                R.id.btn_kg -> {
                    Utility.listValue[3] = "kg"
                    binding.layoutBmi.edtWeight.setText(Utility.poundTokG(binding.layoutBmi.edtWeight.text.toString()))

                }
                R.id.btn_lbs -> {
                    Utility.listValue[3] = "lbs"
                    binding.layoutBmi.edtWeight.setText(Utility.kGToPound(binding.layoutBmi.edtWeight.text.toString()))

                }
            }

        }

    }


    private fun formatHeightEdtTxt() {
        binding.layoutBmi.edtHeight.addTextChangedListener(object : TextWatcher {


            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {

            }

            @SuppressLint("SetTextI18n")
            override fun afterTextChanged(s: Editable) {

                val height = binding.layoutBmi.edtHeight.text.toString()
                if ((binding.layoutBmi.btnCm.isChecked || binding.layoutBmi.btnFt.isChecked) && height != "0" && height !=
                    "0.0" && !TextUtils.isEmpty(binding.layoutBmi.edtHeight.text.toString())
                ) {
                    Utility.setBMIIndex(activity!!.applicationContext, true)
                    Utility.listValue[4] = binding.layoutBmi.edtHeight.text.toString()
                } else {
                    Utility.setBMIIndex(activity!!.applicationContext, false)
                }
            }
        })
    }


    private fun formatWeightEdtTxt() {
        binding.layoutBmi.edtWeight.addTextChangedListener(object : TextWatcher {


            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}

            @SuppressLint("SetTextI18n")
            override fun afterTextChanged(s: Editable) {
                val weight = binding.layoutBmi.edtWeight.text.toString()
                if (weight != "0" && weight != "0.0" && !TextUtils.isEmpty(weight)) {
                    if (binding.layoutBmi.btnKg.isChecked && binding.layoutBmi.edtWeight.text.toString().toDouble() < 226.0) {
                        Utility.setBMIIndex1(activity!!.applicationContext, true)
                        Utility.listValue[5] = binding.layoutBmi.edtWeight.text.toString()
                    } else if (binding.layoutBmi.btnLbs.isChecked && binding.layoutBmi.edtWeight.text.toString().toDouble() < 500.0) {
                        Utility.setBMIIndex1(activity!!.applicationContext, true)
                        Utility.listValue[5] = binding.layoutBmi.edtWeight.text.toString()
                    }
                } else {
                    Utility.setBMIIndex1(activity!!.applicationContext, false)
                }
            }
        })
    }

    private fun formatEdtTxt() {
        binding.layoutPersonalDetails.edtDob.addTextChangedListener(object : TextWatcher {


            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}

            @SuppressLint("SetTextI18n")
            override fun afterTextChanged(s: Editable) {
                if (count <= binding.layoutPersonalDetails.edtDob.text.toString().length && (binding.layoutPersonalDetails.edtDob.text.toString().length == 2 || binding.layoutPersonalDetails.edtDob.text.toString().length == 5)) {
                    binding.layoutPersonalDetails.edtDob.setText(binding.layoutPersonalDetails.edtDob.text.toString() + "/")
                    val pos = binding.layoutPersonalDetails.edtDob.text.length
                    binding.layoutPersonalDetails.edtDob.setSelection(pos)
                } else if (count >= binding.layoutPersonalDetails.edtDob.text.toString().length && (binding.layoutPersonalDetails.edtDob.text.toString().length == 2 || binding.layoutPersonalDetails.edtDob.text.toString().length == 5)) {
                    binding.layoutPersonalDetails.edtDob.setText(
                        binding.layoutPersonalDetails.edtDob.text.toString().substring(
                            0,
                            binding.layoutPersonalDetails.edtDob.text.toString().length - 1
                        )
                    )
                    val pos = binding.layoutPersonalDetails.edtDob.text.length
                    binding.layoutPersonalDetails.edtDob.setSelection(pos)
                }
                count = binding.layoutPersonalDetails.edtDob.text.toString().length
                val date = binding.layoutPersonalDetails.edtDob.text.toString()
                if (binding.layoutPersonalDetails.edtDob.text.length == 10) {
                    if ((date.substring(0, 2).toInt() < 32 && date.substring(
                            3,
                            5
                        ).toInt() < 13 && date.substring(
                            6,
                            10
                        ).toInt() < 2015)
                    )
                        Utility.setPersonalDetails(activity!!.applicationContext, true)
                    Utility.listValue[10] = date
                }

                if (binding.layoutPersonalDetails.edtDob.text.length == 10 && activity != null) {
                    val imm =
                        activity!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager?
                    imm!!.hideSoftInputFromWindow(
                        binding.layoutPersonalDetails.edtDob.windowToken,
                        0
                    )
                }

            }
        })
    }


    private fun validatePersonalQuestionnaire() {
        formatEdtTxt()
        binding.layoutPersonalDetails.radioSex.setOnCheckedChangeListener { _, checkedId ->
            when (checkedId) {
                R.id.btn_female -> {
                    Utility.listValue[11] = "female"
                    isGenderSelected = true
                    Utility.setGender(activity!!.applicationContext, true)
                }
                R.id.btn_male -> {
                    isGenderSelected = true
                    Utility.listValue[11] = "male"
                    Utility.setGender(activity!!.applicationContext, true)
                }
            }

        }

    }


    private fun validateMedicalDiagnoseQuestionnaire() {
        Utility.setDiagnosed(activity!!.applicationContext, true)
        binding.layoutMedicalDiagnose.txtDiabetes.setOnClickListener {
            if (binding.layoutMedicalDiagnose.txtDiabetes.currentTextColor == ContextCompat.getColor(
                    activity!!,
                    R.color.color_blue
                )
            ) {
                binding.layoutMedicalDiagnose.txtDiabetes.setTextColor(
                    ContextCompat.getColor(
                        activity!!,
                        R.color.white
                    )
                )
                binding.layoutMedicalDiagnose.txtDiabetes.setBackgroundResource(R.drawable.blue_button_bg)
                Utility.diseaseList.add("diabetes")
            } else {
                binding.layoutMedicalDiagnose.txtDiabetes.setTextColor(
                    ContextCompat.getColor(
                        activity!!,
                        R.color.color_blue
                    )
                )
                binding.layoutMedicalDiagnose.txtDiabetes.setBackgroundResource(R.drawable.blue_button_uncheck_bg)
                Utility.diseaseList.remove("diabetes")
            }

        }
        binding.layoutMedicalDiagnose.txtCancer.setOnClickListener {
            if (binding.layoutMedicalDiagnose.txtCancer.currentTextColor == ContextCompat.getColor(
                    activity!!,
                    R.color.color_blue
                )
            ) {
                binding.layoutMedicalDiagnose.txtCancer.setTextColor(
                    ContextCompat.getColor(
                        activity!!,
                        R.color.white
                    )
                )
                binding.layoutMedicalDiagnose.txtCancer.setBackgroundResource(R.drawable.blue_button_bg)
                Utility.diseaseList.add("cancer")
            } else {
                binding.layoutMedicalDiagnose.txtCancer.setTextColor(
                    ContextCompat.getColor(
                        activity!!,
                        R.color.color_blue
                    )
                )
                binding.layoutMedicalDiagnose.txtCancer.setBackgroundResource(R.drawable.blue_button_uncheck_bg)
                Utility.diseaseList.remove("cancer")
            }

        }
        binding.layoutMedicalDiagnose.txtHeartDisease.setOnClickListener {
            if (binding.layoutMedicalDiagnose.txtHeartDisease.currentTextColor == ContextCompat.getColor(
                    activity!!,
                    R.color.color_blue
                )
            ) {
                binding.layoutMedicalDiagnose.txtHeartDisease.setTextColor(
                    ContextCompat.getColor(
                        activity!!,
                        R.color.white
                    )
                )
                binding.layoutMedicalDiagnose.txtHeartDisease.setBackgroundResource(R.drawable.blue_button_bg)
                Utility.diseaseList.add("heart_disease")
            } else {
                binding.layoutMedicalDiagnose.txtHeartDisease.setTextColor(
                    ContextCompat.getColor(
                        activity!!,
                        R.color.color_blue
                    )
                )
                binding.layoutMedicalDiagnose.txtHeartDisease.setBackgroundResource(R.drawable.blue_button_uncheck_bg)
                Utility.diseaseList.remove("heart_disease")
            }

        }
        binding.layoutMedicalDiagnose.txtHighBp.setOnClickListener {
            if (binding.layoutMedicalDiagnose.txtHighBp.currentTextColor == ContextCompat.getColor(
                    activity!!,
                    R.color.color_blue
                )
            ) {
                binding.layoutMedicalDiagnose.txtHighBp.setTextColor(
                    ContextCompat.getColor(
                        activity!!,
                        R.color.white
                    )
                )
                binding.layoutMedicalDiagnose.txtHighBp.setBackgroundResource(R.drawable.blue_button_bg)
                Utility.diseaseList.add("high_blood_pressure")
            } else {
                binding.layoutMedicalDiagnose.txtHighBp.setTextColor(
                    ContextCompat.getColor(
                        activity!!,
                        R.color.color_blue
                    )
                )
                binding.layoutMedicalDiagnose.txtHighBp.setBackgroundResource(R.drawable.blue_button_uncheck_bg)
                Utility.diseaseList.remove("high_blood_pressure")
            }

        }
        binding.layoutMedicalDiagnose.txtArthritis.setOnClickListener {
            if (binding.layoutMedicalDiagnose.txtArthritis.currentTextColor == ContextCompat.getColor(
                    activity!!,
                    R.color.color_blue
                )
            ) {
                binding.layoutMedicalDiagnose.txtArthritis.setTextColor(
                    ContextCompat.getColor(
                        activity!!,
                        R.color.white
                    )
                )
                binding.layoutMedicalDiagnose.txtArthritis.setBackgroundResource(R.drawable.blue_button_bg)
                Utility.diseaseList.add("arthritis")
            } else {
                binding.layoutMedicalDiagnose.txtArthritis.setTextColor(
                    ContextCompat.getColor(
                        activity!!,
                        R.color.color_blue
                    )
                )
                binding.layoutMedicalDiagnose.txtArthritis.setBackgroundResource(R.drawable.blue_button_uncheck_bg)
                Utility.diseaseList.remove("arthritis")
            }

        }
        binding.layoutMedicalDiagnose.txtMuscleDisease.setOnClickListener {
            if (binding.layoutMedicalDiagnose.txtMuscleDisease.currentTextColor == ContextCompat.getColor(
                    activity!!,
                    R.color.color_blue
                )
            ) {
                binding.layoutMedicalDiagnose.txtMuscleDisease.setTextColor(
                    ContextCompat.getColor(
                        activity!!,
                        R.color.white
                    )
                )
                binding.layoutMedicalDiagnose.txtMuscleDisease.setBackgroundResource(R.drawable.blue_button_bg)
                Utility.diseaseList.add("muscle_wasting_disease")
            } else {
                binding.layoutMedicalDiagnose.txtMuscleDisease.setTextColor(
                    ContextCompat.getColor(
                        activity!!,
                        R.color.color_blue
                    )
                )
                binding.layoutMedicalDiagnose.txtMuscleDisease.setBackgroundResource(R.drawable.blue_button_uncheck_bg)
                Utility.diseaseList.remove("muscle_wasting_disease")
            }

        }
        binding.layoutMedicalDiagnose.txtOsteoporosis.setOnClickListener {
            if (binding.layoutMedicalDiagnose.txtOsteoporosis.currentTextColor == ContextCompat.getColor(
                    activity!!,
                    R.color.color_blue
                )
            ) {
                binding.layoutMedicalDiagnose.txtOsteoporosis.setTextColor(
                    ContextCompat.getColor(
                        activity!!,
                        R.color.white
                    )
                )
                binding.layoutMedicalDiagnose.txtOsteoporosis.setBackgroundResource(R.drawable.blue_button_bg)
                Utility.diseaseList.add("osteoporosis")
            } else {
                binding.layoutMedicalDiagnose.txtOsteoporosis.setTextColor(
                    ContextCompat.getColor(
                        activity!!,
                        R.color.color_blue
                    )
                )
                binding.layoutMedicalDiagnose.txtOsteoporosis.setBackgroundResource(R.drawable.blue_button_uncheck_bg)
                Utility.diseaseList.remove("osteoporosis")
            }

        }
        binding.layoutMedicalDiagnose.txtInflammatoryDisease.setOnClickListener {
            if (binding.layoutMedicalDiagnose.txtInflammatoryDisease.currentTextColor == ContextCompat.getColor(
                    activity!!,
                    R.color.color_blue
                )
            ) {
                binding.layoutMedicalDiagnose.txtInflammatoryDisease.setTextColor(
                    ContextCompat.getColor(
                        activity!!,
                        R.color.white
                    )
                )
                binding.layoutMedicalDiagnose.txtInflammatoryDisease.setBackgroundResource(R.drawable.blue_button_bg)
                Utility.diseaseList.add("inflammatory_bowel_disease")
            } else {
                binding.layoutMedicalDiagnose.txtInflammatoryDisease.setTextColor(
                    ContextCompat.getColor(
                        activity!!,
                        R.color.color_blue
                    )
                )
                binding.layoutMedicalDiagnose.txtInflammatoryDisease.setBackgroundResource(R.drawable.blue_button_uncheck_bg)
                Utility.diseaseList.remove("inflammatory_bowel_disease")
            }

        }
        binding.layoutMedicalDiagnose.txtTachcardia.setOnClickListener {
            if (binding.layoutMedicalDiagnose.txtTachcardia.currentTextColor == ContextCompat.getColor(
                    activity!!,
                    R.color.color_blue
                )
            ) {
                binding.layoutMedicalDiagnose.txtTachcardia.setTextColor(
                    ContextCompat.getColor(
                        activity!!,
                        R.color.white
                    )
                )
                binding.layoutMedicalDiagnose.txtTachcardia.setBackgroundResource(R.drawable.blue_button_bg)
                Utility.diseaseList.add("tachcardia")
            } else {
                binding.layoutMedicalDiagnose.txtTachcardia.setTextColor(
                    ContextCompat.getColor(
                        activity!!,
                        R.color.color_blue
                    )
                )
                binding.layoutMedicalDiagnose.txtTachcardia.setBackgroundResource(R.drawable.blue_button_uncheck_bg)
                Utility.diseaseList.remove("tachcardia")
            }

        }
        binding.layoutMedicalDiagnose.txtBradycardia.setOnClickListener {
            if (binding.layoutMedicalDiagnose.txtBradycardia.currentTextColor == ContextCompat.getColor(
                    activity!!,
                    R.color.color_blue
                )
            ) {
                binding.layoutMedicalDiagnose.txtBradycardia.setTextColor(
                    ContextCompat.getColor(
                        activity!!,
                        R.color.white
                    )
                )
                binding.layoutMedicalDiagnose.txtBradycardia.setBackgroundResource(R.drawable.blue_button_bg)
                Utility.diseaseList.add("bradycardia")
            } else {
                binding.layoutMedicalDiagnose.txtBradycardia.setTextColor(
                    ContextCompat.getColor(
                        activity!!,
                        R.color.color_blue
                    )
                )
                binding.layoutMedicalDiagnose.txtBradycardia.setBackgroundResource(R.drawable.blue_button_uncheck_bg)
                Utility.diseaseList.remove("bradycardia")
            }

        }

        binding.layoutMedicalDiagnose.txtLimbsInjury.setOnClickListener {
            if (binding.layoutMedicalDiagnose.txtLimbsInjury.currentTextColor == ContextCompat.getColor(
                    activity!!,
                    R.color.color_blue
                )
            ) {
                binding.layoutMedicalDiagnose.txtLimbsInjury.setTextColor(
                    ContextCompat.getColor(
                        activity!!,
                        R.color.white
                    )
                )
                binding.layoutMedicalDiagnose.txtLimbsInjury.setBackgroundResource(R.drawable.blue_button_bg)
                Utility.diseaseList.add("injury_with_incapacitated_limbs")
            } else {
                binding.layoutMedicalDiagnose.txtLimbsInjury.setTextColor(
                    ContextCompat.getColor(
                        activity!!,
                        R.color.color_blue
                    )
                )
                binding.layoutMedicalDiagnose.txtLimbsInjury.setBackgroundResource(R.drawable.blue_button_uncheck_bg)
                Utility.diseaseList.remove("injury_with_incapacitated_limbs")
            }

        }

    }


    @SuppressLint("ResourceType")
    private fun validateTrainingQuestionnaire() {


        binding.layoutWorkoutTrainingDetails.btnTrainingLevel1.setOnClickListener {
            Utility.listValue[6] = "1"
            Utility.setTraining(activity!!.applicationContext, true)
            binding.layoutWorkoutTrainingDetails.btnTrainingLevel1.isChecked = true
            binding.layoutWorkoutTrainingDetails.btnTrainingLevel2.isChecked = false
            binding.layoutWorkoutTrainingDetails.btnTrainingLevel3.isChecked = false
            binding.layoutWorkoutTrainingDetails.btnTrainingLevel4.isChecked = false
            binding.layoutWorkoutTrainingDetails.btnTrainingLevel5.isChecked = false
            binding.layoutWorkoutTrainingDetails.btnTrainingLevel6.isChecked = false
            binding.layoutWorkoutTrainingDetails.btnTrainingLevel7.isChecked = false

        }


        binding.layoutWorkoutTrainingDetails.btnTrainingLevel2.setOnClickListener {
            Utility.listValue[6] = "2"
            Utility.setTraining(activity!!.applicationContext, true)
            binding.layoutWorkoutTrainingDetails.btnTrainingLevel1.isChecked = true
            binding.layoutWorkoutTrainingDetails.btnTrainingLevel2.isChecked = true
            binding.layoutWorkoutTrainingDetails.btnTrainingLevel3.isChecked = false
            binding.layoutWorkoutTrainingDetails.btnTrainingLevel4.isChecked = false
            binding.layoutWorkoutTrainingDetails.btnTrainingLevel5.isChecked = false
            binding.layoutWorkoutTrainingDetails.btnTrainingLevel6.isChecked = false
            binding.layoutWorkoutTrainingDetails.btnTrainingLevel7.isChecked = false

        }

        binding.layoutWorkoutTrainingDetails.btnTrainingLevel3.setOnClickListener {
            Utility.listValue[6] = "3"
            Utility.setTraining(activity!!.applicationContext, true)
            binding.layoutWorkoutTrainingDetails.btnTrainingLevel1.isChecked = true
            binding.layoutWorkoutTrainingDetails.btnTrainingLevel2.isChecked = true
            binding.layoutWorkoutTrainingDetails.btnTrainingLevel3.isChecked = true
            binding.layoutWorkoutTrainingDetails.btnTrainingLevel4.isChecked = false
            binding.layoutWorkoutTrainingDetails.btnTrainingLevel5.isChecked = false
            binding.layoutWorkoutTrainingDetails.btnTrainingLevel6.isChecked = false
            binding.layoutWorkoutTrainingDetails.btnTrainingLevel7.isChecked = false

        }

        binding.layoutWorkoutTrainingDetails.btnTrainingLevel4.setOnClickListener {
            Utility.listValue[6] = "4"
            Utility.setTraining(activity!!.applicationContext, true)
            binding.layoutWorkoutTrainingDetails.btnTrainingLevel1.isChecked = true
            binding.layoutWorkoutTrainingDetails.btnTrainingLevel2.isChecked = true
            binding.layoutWorkoutTrainingDetails.btnTrainingLevel3.isChecked = true
            binding.layoutWorkoutTrainingDetails.btnTrainingLevel4.isChecked = true
            binding.layoutWorkoutTrainingDetails.btnTrainingLevel5.isChecked = false
            binding.layoutWorkoutTrainingDetails.btnTrainingLevel6.isChecked = false
            binding.layoutWorkoutTrainingDetails.btnTrainingLevel7.isChecked = false

        }

        binding.layoutWorkoutTrainingDetails.btnTrainingLevel5.setOnClickListener {
            Utility.listValue[6] = "5"
            Utility.setTraining(activity!!.applicationContext, true)
            binding.layoutWorkoutTrainingDetails.btnTrainingLevel1.isChecked = true
            binding.layoutWorkoutTrainingDetails.btnTrainingLevel2.isChecked = true
            binding.layoutWorkoutTrainingDetails.btnTrainingLevel3.isChecked = true
            binding.layoutWorkoutTrainingDetails.btnTrainingLevel4.isChecked = true
            binding.layoutWorkoutTrainingDetails.btnTrainingLevel5.isChecked = true
            binding.layoutWorkoutTrainingDetails.btnTrainingLevel6.isChecked = false
            binding.layoutWorkoutTrainingDetails.btnTrainingLevel7.isChecked = false

        }

        binding.layoutWorkoutTrainingDetails.btnTrainingLevel6.setOnClickListener {
            Utility.listValue[6] = "6"
            Utility.setTraining(activity!!.applicationContext, true)
            binding.layoutWorkoutTrainingDetails.btnTrainingLevel1.isChecked = true
            binding.layoutWorkoutTrainingDetails.btnTrainingLevel2.isChecked = true
            binding.layoutWorkoutTrainingDetails.btnTrainingLevel3.isChecked = true
            binding.layoutWorkoutTrainingDetails.btnTrainingLevel4.isChecked = true
            binding.layoutWorkoutTrainingDetails.btnTrainingLevel5.isChecked = true
            binding.layoutWorkoutTrainingDetails.btnTrainingLevel6.isChecked = true
            binding.layoutWorkoutTrainingDetails.btnTrainingLevel7.isChecked = false

        }

        binding.layoutWorkoutTrainingDetails.btnTrainingLevel7.setOnClickListener {
            Utility.listValue[6] = "7"
            Utility.setTraining(activity!!.applicationContext, true)
            binding.layoutWorkoutTrainingDetails.btnTrainingLevel1.isChecked = true
            binding.layoutWorkoutTrainingDetails.btnTrainingLevel2.isChecked = true
            binding.layoutWorkoutTrainingDetails.btnTrainingLevel3.isChecked = true
            binding.layoutWorkoutTrainingDetails.btnTrainingLevel4.isChecked = true
            binding.layoutWorkoutTrainingDetails.btnTrainingLevel5.isChecked = true
            binding.layoutWorkoutTrainingDetails.btnTrainingLevel6.isChecked = true
            binding.layoutWorkoutTrainingDetails.btnTrainingLevel7.isChecked = true

        }

        binding.layoutWorkoutTrainingDetails.btnLeaveIt.setOnCheckedChangeListener { _, b ->
            if (b) {
                binding.layoutWorkoutTrainingDetails.btnLeaveIt.isChecked = true
                binding.layoutWorkoutTrainingDetails.btnTrainingLevel1.isChecked = false
                binding.layoutWorkoutTrainingDetails.btnTrainingLevel2.isChecked = false
                binding.layoutWorkoutTrainingDetails.btnTrainingLevel3.isChecked = false
                binding.layoutWorkoutTrainingDetails.btnTrainingLevel4.isChecked = false
                binding.layoutWorkoutTrainingDetails.btnTrainingLevel5.isChecked = false
                binding.layoutWorkoutTrainingDetails.btnTrainingLevel6.isChecked = false
                binding.layoutWorkoutTrainingDetails.btnTrainingLevel7.isChecked = false
                Utility.listValue[6] = "8"
                Utility.setTraining(activity!!.applicationContext, true)
            } else {
                Utility.setTraining(activity!!.applicationContext, false)
                binding.layoutWorkoutTrainingDetails.btnLeaveIt.isChecked = false
            }

        }
        Utility.setTrainingRange(activity!!.applicationContext, true)
        Utility.listValue[7] = "30"
        Utility.listValue[8] = "45"

        binding.layoutWorkoutTrainingDetails.trainingSeekBar.setOnRangeChangedListener(object :
            OnRangeChangedListener {
            override fun onRangeChanged(
                view: RangeSeekBar,
                leftValue: Float,
                rightValue: Float,
                isFromUser: Boolean
            ) {
                println(rightValue.toString() + " * " + leftValue)

                when (leftValue) {
                    0.0F -> {
                        Utility.listValue[7] = "30"
                    }
                    33.333336F -> {
                        Utility.listValue[8] = "45"
                    }
                    66.66667F -> {
                        Utility.listValue[7] = "60"
                    }
                    100.0F -> {
                        Utility.setTrainingRange(activity!!.applicationContext, true)
                        Utility.listValue[7] = "65"
                    }
                }

                when (rightValue) {
                    0.0F -> {
                        Utility.listValue[8] = "30"
                    }
                    33.333336F -> {
                        Utility.listValue[8] = "45"
                    }
                    66.66667F -> {
                        Utility.listValue[7] = "60"
                    }
                    100.0F -> {
                        Utility.setTrainingRange(activity!!.applicationContext, true)
                        Utility.listValue[8] = "65"
                    }
                }


            }

            override fun onStartTrackingTouch(view: RangeSeekBar, isLeft: Boolean) {

            }

            override fun onStopTrackingTouch(view: RangeSeekBar, isLeft: Boolean) {

            }
        })


    }

}
