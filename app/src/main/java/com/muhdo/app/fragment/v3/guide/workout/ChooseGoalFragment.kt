package com.muhdo.app.fragment.v3.guide.workout

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import com.muhdo.app.R
import com.muhdo.app.adapter.v3.GenoTypeAdapterForMyProfile
import com.muhdo.app.apiModel.keyData.GenoTypeModel
import com.muhdo.app.databinding.V3FragmentChooseGoalFragmentBinding
import com.muhdo.app.interfaces.v3.OnGenoTypeItemSelectedListener
import com.muhdo.app.repository.ApiUtilis
import com.muhdo.app.repository.ErrorModel
import com.muhdo.app.repository.NetworkManager
import com.muhdo.app.repository.ServiceListener

class ChooseGoalFragment : Fragment(), OnGenoTypeItemSelectedListener {

    var genoTypeId: String = ""
    var genoName: String = ""
     var fragment :Fragment?=null

    lateinit var binding: V3FragmentChooseGoalFragmentBinding


    var adapter: GenoTypeAdapterForMyProfile? = null
    override fun onItemSelected(id: String?, name: String?) {
        genoName = name!!
        genoTypeId = id!!
        if(fragment==null) {
            replaceFragment(WorkoutGuideQuestionnaireFragment())
        }else{
            replaceFragment(fragment!!)
        }
    }

     fun setGoToFragment(goToFragment: Fragment?) {
        this.fragment=goToFragment
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    fun getSelectedGoalId(): String {
        return genoTypeId
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view =
            inflater.inflate(R.layout.v3_fragment_choose_goal_fragment, container, false)
        binding = V3FragmentChooseGoalFragmentBinding.bind(view)

        binding.genoTypeGrid.layoutManager = GridLayoutManager(requireActivity(), 2)
        getGenotypeModes()

        return view
    }

    private fun setGenotypeList(data: GenoTypeModel) {
        if (activity != null && activity!!.applicationContext != null) {
            adapter = GenoTypeAdapterForMyProfile(activity!!, data.getData()!!)
            adapter?.setGenoTypeItemSelectListener(this)
            binding.genoTypeGrid.adapter = adapter
        }
    }

    private fun getGenotypeModes() {
        binding.progressBar.visibility = View.VISIBLE
        val manager = NetworkManager()
        if (manager.isConnectingToInternet(requireContext())) {
            manager.createApiRequest(
                ApiUtilis.getAPIInstance(activity!!).getGenoType(),
                object : ServiceListener<GenoTypeModel> {
                    override fun getServerResponse(response: GenoTypeModel, requestcode: Int) {
                        binding.progressBar.visibility = View.GONE
                        setGenotypeList(response)
                    }

                    override fun getError(error: ErrorModel, requestcode: Int) {

                        binding.progressBar.visibility = View.GONE
                    }
                })
        } else {
            binding.progressBar.visibility = View.GONE
            Toast.makeText(requireContext(), "Please check internet connection!", Toast.LENGTH_LONG)
                .show()
        }
    }

    private fun replaceFragment(fragment: Fragment) {
        if (activity != null) {
            val transaction = activity!!.supportFragmentManager.beginTransaction()
            transaction.replace(R.id.flContent, fragment)
            transaction.addToBackStack("share")
            transaction.commit()
        }
    }

}
