package com.muhdo.app.fragment.v3.account

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.muhdo.app.R
import com.muhdo.app.apiModel.v3.KitID
import com.muhdo.app.apiModel.v3.KitIdInfoResult
import com.muhdo.app.apiModel.v3.UserIdReq
import com.muhdo.app.databinding.V3FragmentKitIdInfoBinding
import com.muhdo.app.fragment.v3.account.dialog_fragment.AdditionalKitIDListDialogFragment
import com.muhdo.app.repository.ApiUtilis
import com.muhdo.app.repository.NetworkManager2
import com.muhdo.app.ui.v3.ScannerActivityForResult
import com.muhdo.app.utils.PreferenceConnector
import com.timingsystemkotlin.backuptimingsystem.Utility
import org.jetbrains.anko.toast
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


/*@author jithingopal@emvigotech.com*/

/*shows all Kit Id information and status of Kit ID which is in processing*/
class KitIdInfoFragment : Fragment() {
    lateinit var binding: V3FragmentKitIdInfoBinding
    var kitIdList: MutableList<KitID>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view =
            inflater.inflate(R.layout.v3_fragment_kit_id_info, container, false)
        binding = V3FragmentKitIdInfoBinding.bind(view)
        /*reading and showing parent  kit id to header view from local*/
        val kitId =
            PreferenceConnector.readString(activity!!, PreferenceConnector.KIT_ID, "0")
                .toString()
        binding.edParentKitId.setText(kitId)

        binding.btnAddKitId.setOnClickListener {
            val i = Intent(context, ScannerActivityForResult::class.java)
            context!!.startActivity(i)
        }

        binding.btnViewAdditionalKitID.setOnClickListener {
            if (kitIdList != null) {
                // Create and show the dialog.
                val newFragment = AdditionalKitIDListDialogFragment.newInstance()
                newFragment.setKitIDList(kitIdList!!)
                newFragment.show(childFragmentManager, "Dialog")
            } else {
                activity?.toast(R.string.no_additional_kit_ids_found)
            }
        }
        readAllKitIdFromServer()
        return view
    }

    override fun onResume() {
        super.onResume()
        readAllKitIdFromServer()
    }

    /*read all kit id from server and shows the Information to the views. */
    private fun readAllKitIdFromServer() {
        binding.progressBar.visibility = View.VISIBLE
       // var jsonParams: JSONObject = JSONObject()
     //   jsonParams.put("user_id", Utility.getUserID(activity!!.applicationContext))
        val requestBody = UserIdReq(
            Utility.getUserID(activity!!.applicationContext)
        )
        val manager = NetworkManager2()
        if (manager.isConnectingToInternet(binding.parentLayout.context)) {
            val call =
                ApiUtilis.getAPIInstance(activity!!)
                    .fetchAllKitIdInfo(requestBody)
            call.enqueue(object : Callback<KitIdInfoResult> {
                override fun onResponse(
                    call: Call<KitIdInfoResult>,
                    response: Response<KitIdInfoResult>
                ) {
                    try {

                        binding.progressBar.visibility = View.GONE
                        var resultPojo: KitIdInfoResult? = response.body()
                        if (resultPojo != null && resultPojo!!.statusCode == 200) {
                            var parentKitID: KitID = KitID(
                                status = resultPojo.data.status,
                                _id = resultPojo.data._id,
                                date_of_scanning = resultPojo.data.date_of_scanning,
                                kit_id = resultPojo.data.kit_id
                            )
                            //reading all child kit ids
                            kitIdList = resultPojo.data.children

                            //adding parent kit to the first position of List
                            if (kitIdList != null) {
                                if (kitIdList!!.size > 0) {
                                    kitIdList?.add(0, parentKitID)
                                }
                            } else {
                                kitIdList = ArrayList<KitID>(0)
                                kitIdList?.add(parentKitID)

                            }
                            binding.edParentKitId.setText(parentKitID.kit_id)   //showing parent kit id in the header view


                            /*setting progress status to the view if there is at least one kit id and processing is not completed*/
                            if (kitIdList!!.size > 0) {
                                binding.btnViewAdditionalKitID.visibility = View.VISIBLE
                                binding.btnAddKitId.visibility = View.VISIBLE
                                val lastKitID = kitIdList?.get((kitIdList!!.size - 1))
                                /*when (lastKitID!!.status) {
                                    "DISPATCHED" -> {
                                        binding.layoutKitIdProgressStatus.visibility = View.VISIBLE
                                        binding.tvKitIDSelected.setText(lastKitID!!.kit_id)
                                        binding.tvProgress1.visibility = View.VISIBLE
                                        binding.tvProgress2.visibility = View.INVISIBLE
                                        binding.tvProgress3.visibility = View.INVISIBLE
                                    }
                                    "RECEIVED" -> {
                                        binding.layoutKitIdProgressStatus.visibility = View.VISIBLE
                                        binding.tvKitIDSelected.setText(lastKitID!!.kit_id)
                                        binding.tvProgress1.visibility = View.VISIBLE
                                        binding.tvProgress2.visibility = View.VISIBLE
                                        binding.tvProgress3.visibility = View.INVISIBLE
                                    }
                                    "PROCESSING" -> {
                                        binding.layoutKitIdProgressStatus.visibility = View.VISIBLE
                                        binding.tvKitIDSelected.setText(lastKitID!!.kit_id)
                                        binding.tvProgress1.visibility = View.VISIBLE
                                        binding.tvProgress2.visibility = View.VISIBLE
                                        binding.tvProgress3.visibility = View.VISIBLE
                                    }
                                    else -> {
                                        binding.layoutKitIdProgressStatus.visibility = View.GONE
                                    }
                                }*/

                            }
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }

                override fun onFailure(call: Call<KitIdInfoResult>, t: Throwable) {

                    // Log error here since request failed
                    try {
                        if (binding.progressBar != null) binding.progressBar.visibility = View.GONE
                        Utility.displayShortSnackBar(
                            binding.parentLayout,
                            "Server connection error!!, please try again later."
                        )
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }


                }
            })
        } else {
            binding.progressBar.visibility = View.GONE

            Utility.displayShortSnackBar(
                binding.parentLayout,
                binding.parentLayout.context.resources.getString(R.string.check_internet)
            )

        }

    }

}
