package com.muhdo.app.fragment.v3

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import androidx.viewpager.widget.ViewPager
import com.muhdo.app.R
import com.muhdo.app.databinding.V3FragmentGeneticActionPlanResultBinding
import com.muhdo.app.utils.v3.TempUtil


class GeneticActionPlanResultFragment : Fragment() {
    lateinit var binding: V3FragmentGeneticActionPlanResultBinding
    var tab_icons = arrayOf<Int>(
        R.drawable.v3_ic_diet_genetic_action_plan,
        R.drawable.v3_ic_vitamin,
        R.drawable.ic_physical,
        R.drawable.v3_ic_health
    )
    var firstSelectPos = 0

    var tab_label =
        arrayOf<Int>(R.string.diet, R.string.vitamins, R.string.physical, R.string.health)

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        val view =
            inflater.inflate(R.layout.v3_fragment_genetic_action_plan_result, container, false)
        binding = V3FragmentGeneticActionPlanResultBinding.bind(view)
        setupTabIcons()
        selectTabItem(firstSelectPos)

        return view
    }

    @SuppressLint("WrongConstant")
    private fun replaceFragment(fragment: Fragment) {
        val transaction = activity!!.supportFragmentManager.beginTransaction()
        transaction.replace(R.id.flContent, fragment)
        transaction.addToBackStack("share")
        transaction.commit()

    }

    private fun selectTabItem(position: Int) {
        binding.tabLayout.getTabAt(position)!!.select();
    }

    public fun selectedTabPosition(position: Int) {
        firstSelectPos = position
    }

    @SuppressLint("ResourceAsColor", "InflateParams")
    private fun setupTabIcons() {
        val fragmentAdapter = ViewPagerAdapter(activity!!.supportFragmentManager)
        binding.viewPager.adapter = fragmentAdapter

        binding.indicator.setViewPager(binding.viewPager)
        binding.viewPager.offscreenPageLimit = 4
        val density = resources.displayMetrics.density
        binding.tabLayout.setupWithViewPager(binding.viewPager)
        val adapter = ViewPagerAdapter(activity!!.supportFragmentManager)
        binding.viewPager.adapter = adapter
        for (i in 0 until 4) {
            val fragment = GeneticActionPlanResultItemFragment()
            fragment.setActionPlanViewType(com.muhdo.app.utils.v3.Constants.ACTION_PLAN_TYPES[i])
            adapter.addFrag(fragment, getString(tab_label[i]))
        }
        binding.viewPager.adapter = adapter

        for (i in 0 until 4) {
            val tabView = LayoutInflater.from(activity!!.applicationContext)
                .inflate(R.layout.custom_tab, null)
            val tabLabel = tabView.findViewById(R.id.tab) as TextView
            val tabImage = tabView.findViewById(R.id.tab_img) as ImageView
            tabImage.setColorFilter(
                ContextCompat.getColor(
                    activity!!.applicationContext,
                    R.color.v3_tint_tab_unselected_color
                ), android.graphics.PorterDuff.Mode.SRC_IN
            );
            tabLabel.text = getString(tab_label[i])
            tabImage.setImageResource(tab_icons[i])
            tabLabel.setTextColor(context!!.resources.getColor(R.color.v3_tint_tab_unselected_color))
            binding.tabLayout.getTabAt(i)!!.customView = tabView
        }
        val tabLabel =
            binding.tabLayout.getTabAt(0)!!.customView!!.findViewById(R.id.tab) as TextView
        val tabImage =
            binding.tabLayout.getTabAt(0)!!.customView!!.findViewById(R.id.tab_img) as ImageView
        tabImage.setColorFilter(
            ContextCompat.getColor(
                activity!!.applicationContext,
                R.color.v3_tint_tab_selected_color
            ), android.graphics.PorterDuff.Mode.SRC_IN
        )
        tabLabel.setTextColor(context!!.resources.getColor(R.color.v3_tint_tab_selected_color))
        binding.viewPager.addOnPageChangeListener((object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {

            }

            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {

            }

            override fun onPageSelected(position: Int) {
                for (i in 0 until 4) {
                    val tabLabel =
                        binding.tabLayout.getTabAt(i)!!.customView!!.findViewById(R.id.tab) as TextView
                    val tabImage =
                        binding.tabLayout.getTabAt(i)!!.customView!!.findViewById(R.id.tab_img) as ImageView
                    tabImage.setColorFilter(
                        ContextCompat.getColor(
                            activity!!.applicationContext,
                            R.color.v3_tint_tab_unselected_color
                        ), android.graphics.PorterDuff.Mode.SRC_IN
                    )
                    tabLabel.setTextColor(context!!.resources.getColor(R.color.v3_tint_tab_unselected_color))
                }

                val tabLabel =
                    binding.tabLayout.getTabAt(position)!!.customView!!.findViewById(R.id.tab) as TextView
                val tabImage =
                    binding.tabLayout.getTabAt(position)!!.customView!!.findViewById(R.id.tab_img) as ImageView
                tabImage.setColorFilter(
                    ContextCompat.getColor(
                        activity!!.applicationContext,
                        R.color.v3_tint_tab_selected_color
                    ), android.graphics.PorterDuff.Mode.SRC_IN
                )
                tabLabel.setTextColor(context!!.resources.getColor(R.color.v3_tint_tab_selected_color))
            }
        }))
    }

    internal inner class ViewPagerAdapter(manager: FragmentManager) :
        FragmentStatePagerAdapter(manager) {
        private val mFragmentList = arrayListOf<Fragment>()
        private val mFragmentTitleList = arrayListOf<String>()

        override fun getItem(position: Int): Fragment {

            return mFragmentList.get(position)
        }

        override fun getCount(): Int {
            return mFragmentList.size
        }

        fun addFrag(fragment: Fragment, title: String) {
            mFragmentList.add(fragment)
            mFragmentTitleList.add(title)
        }

        override fun getPageTitle(position: Int): CharSequence? {
            TempUtil.log("data", "tab pos = $position")
            return mFragmentTitleList[position]
        }
    }

    fun birthdayCakeCandles(ar: Array<Int>) =
        ar.count { it == ar.max() }

}