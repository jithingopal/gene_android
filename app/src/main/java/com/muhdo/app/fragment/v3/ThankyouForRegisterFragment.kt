package com.muhdo.app.fragment.v3

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.muhdo.app.R
import com.muhdo.app.databinding.V3FragmentThankYouForRegisteringBinding

class ThankyouForRegisterFragment : Fragment() {
    lateinit var binding: V3FragmentThankYouForRegisteringBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view =
            inflater.inflate(R.layout.v3_fragment_thank_you_for_registering, container, false)
        binding = V3FragmentThankYouForRegisteringBinding.bind(view)
        return view
    }

    private fun replaceFragment(fragment: Fragment) {
        val transaction = activity?.supportFragmentManager?.beginTransaction()
        transaction?.replace(R.id.flContent, fragment)
        transaction?.addToBackStack("share")
        transaction?.commit()
    }
}
