package com.muhdo.app.fragment.v3

import android.annotation.SuppressLint
import android.app.Dialog
import android.os.Bundle
import android.text.Html
import android.util.Base64
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.muhdo.app.R
import com.muhdo.app.apiModel.v3.Answer
import com.muhdo.app.apiModel.v3.QuestionnaireAnswer
import com.muhdo.app.databinding.V3FragmentQuestionnaireHolderLifestyleBinding
import com.muhdo.app.repository.ApiUtilis
import com.muhdo.app.utils.v3.*
import com.timingsystemkotlin.backuptimingsystem.Utility
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.v3_fragment_questionnaire_holder_lifestyle.view.*
import kotlinx.android.synthetic.main.v3_layout_lifestyle_2.*
import kotlinx.android.synthetic.main.v3_layout_lifestyle_3.*
import kotlinx.android.synthetic.main.v3_layout_lifestyle_4.*


class QuestionnaireLifestyleFragment : Fragment() {
    private var isSkipped: Boolean = false
    private var currentPage = 0

    lateinit var dialog: Dialog;
    internal lateinit var binding: V3FragmentQuestionnaireHolderLifestyleBinding
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view =
            inflater.inflate(R.layout.v3_fragment_questionnaire_holder_lifestyle, container, false)
        binding = V3FragmentQuestionnaireHolderLifestyleBinding.bind(view)
        addBottomDots(currentPage)
        setupDialogInfo()

        binding.btnNext.setOnClickListener {
            onNext()
        }
        binding.btnPrevious.setOnClickListener {
            onPrevious()
        }
        binding.layoutLifestyle1.layout_lifestyle1_bind.findViewById<View>(R.id.btn_questionnaire_info)
            .setOnClickListener {
                onShowInfoDialog()
            }
        binding.layoutLifestyle2.layout_lifestyle2_bind.findViewById<View>(R.id.btn_questionnaire_info)
            .setOnClickListener {
                onShowInfoDialog()
            }
        binding.layoutLifestyle3.layout_lifestyle3_bind.findViewById<View>(R.id.btn_questionnaire_info)
            .setOnClickListener {
                onShowInfoDialog()
            }
        binding.layoutLifestyle4.layout_lifestyle4_bind.findViewById<View>(R.id.btn_questionnaire_info)
            .setOnClickListener {
                onShowInfoDialog()
            }
        binding.layoutLifestyle1Bind.tvSkip.setOnClickListener {
            uncheckAllCheckboxes(
                binding.layoutLifestyle1Bind.cbOption1,
                binding.layoutLifestyle1Bind.cbOption2,
                binding.layoutLifestyle1Bind.cbOption3,
                binding.layoutLifestyle1Bind.cbOption4,
                binding.layoutLifestyle1Bind.cbOption5,
                binding.layoutLifestyle1Bind.cbOption6,
                binding.layoutLifestyle1Bind.cbOption7,
                binding.layoutLifestyle1Bind.cbOption8,
                binding.layoutLifestyle1Bind.cbOption9,
                binding.layoutLifestyle1Bind.cbOption10,
                binding.layoutLifestyle1Bind.cbOption11,
                binding.layoutLifestyle1Bind.cbOption12,
                binding.layoutLifestyle1Bind.cbOption13,
                binding.layoutLifestyle1Bind.cbOption14,
                binding.layoutLifestyle1Bind.cbOption15,
                binding.layoutLifestyle1Bind.cbOption16,
                binding.layoutLifestyle1Bind.cbOption17,
                binding.layoutLifestyle1Bind.cbOption18,
                binding.layoutLifestyle1Bind.cbOption19,
                binding.layoutLifestyle1Bind.cbOption20,
                binding.layoutLifestyle1Bind.cbOption21,
                binding.layoutLifestyle1Bind.cbOption22,
                binding.layoutLifestyle1Bind.cbOption23,
                binding.layoutLifestyle1Bind.cbOption24
            )
            isSkipped = true
            onNext()
        }
        setupActivityLevelListener()
        /*val content = SpannableString("SKIP")
        content.setSpan(UnderlineSpan(), 0, content.length, 0)
        binding.layoutLifestyle1Bind.tvSkip.setText(content)*/
        return view

    }

    private fun setupDialogInfo() {
        //This function is working fine.
        dialog = Dialog(activity)
        val viewDialog =
            View.inflate(activity, R.layout.v3_dialog_info_questionnaire_lifestyle, null)
        val closeButton = viewDialog.findViewById<ImageView>(R.id.img_close)
        closeButton.setOnClickListener { dialog.dismiss() }
        dialog.setContentView(viewDialog)
        dialog.setCancelable(false)
    }

    private fun onShowInfoDialog() {
        if (!dialog.isShowing)
            dialog.show()
    }

    private fun changecbStressLevelIndicator() {
        if (binding.layoutLifestyle4Bind.cbStressLevel3.isChecked) {
            binding.layoutLifestyle4Bind.txtDailyStressLevel.setText("High")
        } else if (binding.layoutLifestyle4Bind.cbStressLevel2.isChecked) {
            binding.layoutLifestyle4Bind.txtDailyStressLevel.setText("Medium")
        } else if (binding.layoutLifestyle4Bind.cbStressLevel1.isChecked) {
            binding.layoutLifestyle4Bind.txtDailyStressLevel.setText("Low")
        }
    }

    private fun removeAllCheckBoxListeners() {
        binding.layoutLifestyle4Bind.cbStressLevel1.setOnCheckedChangeListener(null)
        binding.layoutLifestyle4Bind.cbStressLevel2.setOnCheckedChangeListener(null)
        binding.layoutLifestyle4Bind.cbStressLevel3.setOnCheckedChangeListener(null)
    }

    private fun setupActivityLevelListener() {
        binding.layoutLifestyle4Bind.cbStressLevel1.setOnCheckedChangeListener { buttonView, isChecked ->
            removeAllCheckBoxListeners()
            binding.layoutLifestyle4Bind.cbStressLevel1.isChecked = true
            binding.layoutLifestyle4Bind.cbStressLevel2.isChecked = false
            binding.layoutLifestyle4Bind.cbStressLevel3.isChecked = false
            setupActivityLevelListener()
            changecbStressLevelIndicator()
        }

        binding.layoutLifestyle4Bind.cbStressLevel2.setOnCheckedChangeListener { buttonView, isChecked ->
            removeAllCheckBoxListeners()
            binding.layoutLifestyle4Bind.cbStressLevel1.isChecked = true
            binding.layoutLifestyle4Bind.cbStressLevel2.isChecked = true
            binding.layoutLifestyle4Bind.cbStressLevel3.isChecked = false
            setupActivityLevelListener()
            changecbStressLevelIndicator()
        }

        binding.layoutLifestyle4Bind.cbStressLevel3.setOnCheckedChangeListener { buttonView, isChecked ->
            removeAllCheckBoxListeners()
            binding.layoutLifestyle4Bind.cbStressLevel1.isChecked = true
            binding.layoutLifestyle4Bind.cbStressLevel2.isChecked = true
            binding.layoutLifestyle4Bind.cbStressLevel3.isChecked = true
            setupActivityLevelListener()
            changecbStressLevelIndicator()
        }
    }

    private fun onNext() {
        if (currentPage == 0) {
            if (atleastOneCheckBoxSelected(
                    binding.layoutLifestyle1Bind.cbOption1,
                    binding.layoutLifestyle1Bind.cbOption2,
                    binding.layoutLifestyle1Bind.cbOption3,
                    binding.layoutLifestyle1Bind.cbOption4,
                    binding.layoutLifestyle1Bind.cbOption5,
                    binding.layoutLifestyle1Bind.cbOption6,
                    binding.layoutLifestyle1Bind.cbOption7,
                    binding.layoutLifestyle1Bind.cbOption8,
                    binding.layoutLifestyle1Bind.cbOption9,
                    binding.layoutLifestyle1Bind.cbOption10,
                    binding.layoutLifestyle1Bind.cbOption11,
                    binding.layoutLifestyle1Bind.cbOption12,
                    binding.layoutLifestyle1Bind.cbOption13,
                    binding.layoutLifestyle1Bind.cbOption14,
                    binding.layoutLifestyle1Bind.cbOption15,
                    binding.layoutLifestyle1Bind.cbOption16,
                    binding.layoutLifestyle1Bind.cbOption17,
                    binding.layoutLifestyle1Bind.cbOption18,
                    binding.layoutLifestyle1Bind.cbOption19,
                    binding.layoutLifestyle1Bind.cbOption20,
                    binding.layoutLifestyle1Bind.cbOption21,
                    binding.layoutLifestyle1Bind.cbOption22,
                    binding.layoutLifestyle1Bind.cbOption23,
                    binding.layoutLifestyle1Bind.cbOption24
                ) || isSkipped
            ) {
                if (atleastOneCheckBoxSelected(
                        binding.layoutLifestyle1Bind.cbOption1,
                        binding.layoutLifestyle1Bind.cbOption2,
                        binding.layoutLifestyle1Bind.cbOption3,
                        binding.layoutLifestyle1Bind.cbOption4,
                        binding.layoutLifestyle1Bind.cbOption5,
                        binding.layoutLifestyle1Bind.cbOption6,
                        binding.layoutLifestyle1Bind.cbOption7,
                        binding.layoutLifestyle1Bind.cbOption8,
                        binding.layoutLifestyle1Bind.cbOption9,
                        binding.layoutLifestyle1Bind.cbOption10,
                        binding.layoutLifestyle1Bind.cbOption11,
                        binding.layoutLifestyle1Bind.cbOption12,
                        binding.layoutLifestyle1Bind.cbOption13,
                        binding.layoutLifestyle1Bind.cbOption14,
                        binding.layoutLifestyle1Bind.cbOption15,
                        binding.layoutLifestyle1Bind.cbOption16,
                        binding.layoutLifestyle1Bind.cbOption17,
                        binding.layoutLifestyle1Bind.cbOption18,
                        binding.layoutLifestyle1Bind.cbOption19,
                        binding.layoutLifestyle1Bind.cbOption20,
                        binding.layoutLifestyle1Bind.cbOption21,
                        binding.layoutLifestyle1Bind.cbOption22,
                        binding.layoutLifestyle1Bind.cbOption23,
                        binding.layoutLifestyle1Bind.cbOption24
                    )
                )
                    isSkipped = false
                binding.btnPrevious.visibility = View.VISIBLE
                binding.layoutLifestyle1.visibility = View.GONE
                binding.layoutLifestyle2.visibility = View.VISIBLE
                binding.layoutLifestyle3.visibility = View.GONE
                binding.layoutLifestyle4.visibility = View.GONE
                currentPage = 1
            } else
                context?.showAlertDialog(getString(R.string.answer_or_skip_message))
        } else if (currentPage == 1) {
            if (rg_not_exercise.checkedRadioButtonId != -1 && rg_injury_muscle.checkedRadioButtonId != -1 && rg_pregnant.checkedRadioButtonId != -1) {
                binding.btnPrevious.visibility = View.VISIBLE
                binding.layoutLifestyle1.visibility = View.GONE
                binding.layoutLifestyle2.visibility = View.GONE
                binding.layoutLifestyle3.visibility = View.VISIBLE
                binding.layoutLifestyle4.visibility = View.GONE
                currentPage = 2
            } else
                context?.showAlertDialog(getString(R.string.answer_all_question_message))
        } else if (currentPage == 2) {
            if (rg_sleep_hours.checkedRadioButtonId != -1) {
                binding.btnPrevious.visibility = View.VISIBLE
                binding.layoutLifestyle1.visibility = View.GONE
                binding.layoutLifestyle2.visibility = View.GONE
                binding.layoutLifestyle3.visibility = View.GONE
                binding.layoutLifestyle4.visibility = View.VISIBLE
                currentPage = 3
            } else
                context?.showAlertDialog(getString(R.string.answer_all_question_message))
        } else if (currentPage == 3) {
            if (rg_consume_alcohol.checkedRadioButtonId != -1 && rg_regular_smoke.checkedRadioButtonId != -1 &&
                atleastOneCheckBoxSelected(cb_stress_level_1, cb_stress_level_2, cb_stress_level_3)
            ) {
                getAllAnswersOfQuestionnaire(isSkipped)
            } else
                context?.showAlertDialog(getString(R.string.answer_all_question_message))
        }
        addBottomDots(currentPage)
    }

    private fun getAllAnswersOfQuestionnaire(isSkipped: Boolean) {

        val userId = Base64.decode(Utility.getUserID(activity!!.applicationContext), Base64.DEFAULT)
        val decodeUserId = String(userId, charset("UTF-8"))
        val answers = arrayListOf<Answer>()
        val diseasesAnswer: Answer
        if (isSkipped) {
            diseasesAnswer = Answer(
                question = Constants.LIFESTYLE.DISEASE,
                answer = listOf(Constants.ANSWER.SKIP)
            )
        } else {
            diseasesAnswer = Answer(
                question = Constants.LIFESTYLE.DISEASE,
                answer = Constants.createDiseaseArray(
                    getAllSelectedCheckboxTexts(
                        binding.layoutLifestyle1Bind.cbOption1,
                        binding.layoutLifestyle1Bind.cbOption2,
                        binding.layoutLifestyle1Bind.cbOption3,
                        binding.layoutLifestyle1Bind.cbOption4,
                        binding.layoutLifestyle1Bind.cbOption5,
                        binding.layoutLifestyle1Bind.cbOption6,
                        binding.layoutLifestyle1Bind.cbOption7,
                        binding.layoutLifestyle1Bind.cbOption8,
                        binding.layoutLifestyle1Bind.cbOption9,
                        binding.layoutLifestyle1Bind.cbOption10,
                        binding.layoutLifestyle1Bind.cbOption11,
                        binding.layoutLifestyle1Bind.cbOption12,
                        binding.layoutLifestyle1Bind.cbOption13,
                        binding.layoutLifestyle1Bind.cbOption14,
                        binding.layoutLifestyle1Bind.cbOption15,
                        binding.layoutLifestyle1Bind.cbOption16,
                        binding.layoutLifestyle1Bind.cbOption17,
                        binding.layoutLifestyle1Bind.cbOption18,
                        binding.layoutLifestyle1Bind.cbOption19,
                        binding.layoutLifestyle1Bind.cbOption20,
                        binding.layoutLifestyle1Bind.cbOption21,
                        binding.layoutLifestyle1Bind.cbOption22,
                        binding.layoutLifestyle1Bind.cbOption23,
                        binding.layoutLifestyle1Bind.cbOption24
                    )
                )
            )
        }
        answers.add(diseasesAnswer)
        val physicianNoExercise = Answer(
            question = Constants.LIFESTYLE.PHYSICIAN_NO_EXERCISE,
            answer = listOf(binding.layoutLifestyle2Bind.rgNotExercise.getSelectedRadioButtonText())
        )
        answers.add(physicianNoExercise)
        val muscleInjury = Answer(
            question = Constants.LIFESTYLE.MUSCLE_INJURY,
            answer = listOf(binding.layoutLifestyle2Bind.rgInjuryMuscle.getSelectedRadioButtonText())
        )
        answers.add(muscleInjury)
        val pregnant = Answer(
            question = Constants.LIFESTYLE.PREGNANT,
            answer = listOf(binding.layoutLifestyle2Bind.rgPregnant.getSelectedRadioButtonText())
        )
        answers.add(pregnant)
        val sleepHours = Answer(
            question = Constants.LIFESTYLE.SLEEP_DURATION,
            answer = listOf(
                Constants.createSleepHoursConstantValue(binding.layoutLifestyle3Bind.rgSleepHours.getSelectedRadioButtonText())
            )
        )
        answers.add(sleepHours)
        val consumeAlcohol = Answer(
            question = Constants.LIFESTYLE.CONSUME_ALCOHOL,
            answer = listOf(
                binding.layoutLifestyle4Bind.rgConsumeAlcohol.getSelectedRadioButtonText()
            )
        )
        answers.add(consumeAlcohol)
        val smoke = Answer(
            question = Constants.LIFESTYLE.SMOKE,
            answer = listOf(
                binding.layoutLifestyle4Bind.rgRegularSmoke.getSelectedRadioButtonText()
            )
        )
        answers.add(smoke)
        val stressLevel = Answer(
            question = Constants.LIFESTYLE.STRESS_LEVEL,
            answer = listOf(
                getNumberOfSelectedCheckbox(
                    binding.layoutLifestyle4Bind.cbStressLevel1,
                    binding.layoutLifestyle4Bind.cbStressLevel2,
                    binding.layoutLifestyle4Bind.cbStressLevel3
                )
            )
        )
        answers.add(stressLevel)
        val requestBody = QuestionnaireAnswer(
            user_id = decodeUserId,
            answers = answers,
            category = Constants.QUESTIONNAIRE.LIFESTYLE
        )
        uploadAnswers(requestBody)
    }

    private fun uploadAnswers(requestBody: QuestionnaireAnswer) {
        binding.progressBar.visibility = View.VISIBLE
        ApiUtilis.getAPIInstance(activity!!).postReviewQuestionnaireAnswers(requestBody)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ success ->
                run {
                    binding.progressBar.visibility = View.GONE
                    Utility.displayShortSnackBar(binding.root, success.message)
                    // Log.d("response from Exercise", success.message)
                    replaceFragment(QuestionnaireHomeFragment())
                    TempUtil.isLifestyleQuestionnaireCompleted = true
                }
            }, { error ->
                run {
                    binding.progressBar.visibility = View.GONE
                    Utility.displayShortSnackBar(binding.root, error.localizedMessage)
                    // Log.d("response from Exercise ", error.localizedMessage)
                }
            })
    }

    @SuppressLint("WrongConstant")
    private fun replaceFragment(fragment: Fragment) {
        val transaction = activity!!.supportFragmentManager.beginTransaction()
        transaction.replace(R.id.flContent, fragment)
        transaction.addToBackStack("share")
        transaction.commit()
    }

    private fun onPrevious() {
        if (currentPage == 0) {
            binding.btnPrevious.visibility = View.GONE
            currentPage = 0
        } else if (currentPage == 1) {
            if (isSkipped)
                isSkipped = false
            binding.btnPrevious.visibility = View.GONE
            binding.layoutLifestyle1.visibility = View.VISIBLE
            binding.layoutLifestyle2.visibility = View.GONE
            binding.layoutLifestyle3.visibility = View.GONE
            binding.layoutLifestyle4.visibility = View.GONE
            currentPage = 0
        } else if (currentPage == 2) {
            binding.btnPrevious.visibility = View.VISIBLE
            binding.layoutLifestyle1.visibility = View.GONE
            binding.layoutLifestyle2.visibility = View.VISIBLE
            binding.layoutLifestyle3.visibility = View.GONE
            binding.layoutLifestyle4.visibility = View.GONE
            currentPage = 1
        } else if (currentPage == 3) {
            binding.btnPrevious.visibility = View.VISIBLE
            binding.layoutLifestyle1.visibility = View.GONE
            binding.layoutLifestyle2.visibility = View.GONE
            binding.layoutLifestyle3.visibility = View.VISIBLE
            binding.layoutLifestyle4.visibility = View.GONE
            currentPage = 2
        }
        addBottomDots(currentPage)
    }

    private fun addBottomDots(currentPage: Int) {
        val dots = arrayOfNulls<TextView>(4)
        binding.layoutDots.removeAllViews()
        for (i in dots.indices) {
            dots[i] = TextView(activity)
            dots[i]!!.text = Html.fromHtml("&#8226;")
            dots[i]!!.textSize = 35f
            dots[i]!!.setTextColor(
                ContextCompat.getColor(
                    activity!!.applicationContext,
                    R.color.secondary_color
                )
            )
            binding.layoutDots.addView(dots[i])
        }

        if (dots.isNotEmpty()) {
            dots[currentPage]!!.setTextColor(
                ContextCompat.getColor(
                    activity!!.applicationContext,
                    R.color.color_dark_blue
                )
            )
        }
    }
}