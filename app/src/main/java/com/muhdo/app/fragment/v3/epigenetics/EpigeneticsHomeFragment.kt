package com.muhdo.app.fragment.v3.epigenetics

import android.animation.Animator
import android.app.AlertDialog
import android.app.Dialog
import android.content.Intent
import android.graphics.Typeface
import android.os.Bundle
import android.text.SpannableString
import android.text.style.UnderlineSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.muhdo.app.R
import com.muhdo.app.adapter.v3.EpigeneticOverviewRecyAdpter
import com.muhdo.app.apiModel.v3.EpigenBaseReq
import com.muhdo.app.apiModel.v3.UserIdReq
import com.muhdo.app.apiModel.v3.epigenetics.EpigenHomeResponse
import com.muhdo.app.apiModel.v3.epigenetics.EpigenItem
import com.muhdo.app.apiModel.v3.epigenetics.ScoreDataHome
import com.muhdo.app.databinding.V3FragmentEpigeneticHomeBinding
import com.muhdo.app.interfaces.v3.OnSwipeTouchListener
import com.muhdo.app.repository.ApiUtilis
import com.muhdo.app.repository.ErrorModel
import com.muhdo.app.repository.NetworkManager
import com.muhdo.app.repository.ServiceListener
import com.muhdo.app.ui.DashboardActivity
import com.muhdo.app.ui.epigenticResult.model.EpigResultResponse
import com.muhdo.app.ui.epigenticResult.model.EpigenticHealth
import com.muhdo.app.utils.v3.ItemAnimation
import com.muhdo.app.utils.v3.TempUtil
import com.muhdo.app.utils.v3.showAlertDialog
import com.timingsystemkotlin.backuptimingsystem.Utility
import org.jetbrains.anko.toast

/**/
class EpigeneticsHomeFragment : Fragment() {
    val SWIPE_LEFT_TO_RIGHT = 100
    val SWIPE_RIGHT_TO_LEFT = 101

    var epiGenOverviewAdapter: EpigeneticOverviewRecyAdpter? = null
    internal lateinit var binding: V3FragmentEpigeneticHomeBinding
    var epigenticHealthData: List<ScoreDataHome>? = null
    var isShowingFirstTimeAfterLoading = true
    var epigResult: EpigenHomeResponse? = null
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.v3_fragment_epigenetic_home, container, false)
        binding = V3FragmentEpigeneticHomeBinding.bind(view)
        epiGenOverviewAdapter = EpigeneticOverviewRecyAdpter(
            activity!!,
            ArrayList<ScoreDataHome>(0), epigResult?.data?.samples
        )
        TempUtil.epiGenDialogAlreadyChangedOnce = false
        binding.recyclerEpigeneticOverviewResult.adapter = epiGenOverviewAdapter
        binding.recyclerEpigeneticOverviewResult.layoutManager =
            LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        getEpigenticResultHomeData()
        binding.imgViewGoUp.setOnClickListener { onQuickViewUpButtonPressed() }
        binding.imgViewGoDown.setOnClickListener { onQuickViewDownButtonPressed() }
        binding.layoutAgeAtSample.setOnClickListener { showFilpAnimationAgeAtSample() }
        binding.tvTabText1.setOnClickListener {
            if(selectedTabPostion!=0){//show previous
                setFilpAnimationToQuickView(SWIPE_LEFT_TO_RIGHT)

            }
        }
        binding.tvTabText2.setOnClickListener {
            if(selectedTabPostion==0){//show previous
                setFilpAnimationToQuickView(SWIPE_RIGHT_TO_LEFT)

            }
        }
        showEpigenDialogInfo()
        return view
    }

    private fun setupTabs() {
        if (epigResult != null && epigResult?.data != null && epigResult?.data?.previous != null) {
            binding.nestedScrollView.setOnTouchListener(swipeTouchListener)
            binding.layoutTabHeader.visibility = View.VISIBLE
            binding.layoutTabIndicator.visibility = View.VISIBLE
        } else {
            binding.layoutTabHeader.visibility = View.GONE
            binding.layoutTabIndicator.visibility = View.GONE
            binding.nestedScrollView.setOnTouchListener(null)
        }
    }

    private fun replaceFragment(fragment: Fragment) {
        val transaction = activity!!.supportFragmentManager.beginTransaction()
        transaction.replace(R.id.flContent, fragment)
        transaction.addToBackStack("share")
        transaction.commit()

    }

    private var currentHighlatedPos = -1;

    /*This function works  while you press the up button in the Big circle in the QuickView section*/
    private fun onQuickViewUpButtonPressed() {

        currentHighlatedPos--
        if (currentHighlatedPos == -1) {
            currentHighlatedPos = 4
        }
        var score: Float? =
            epigenticHealthData?.get(currentHighlatedPos)?.score
        binding.tvHighlated.setText(
            epigenticHealthData?.get(currentHighlatedPos)?.title
        )

        if (isShowingFirstTimeAfterLoading) {
            ItemAnimation.animate(binding.tvHighlated, -1, ItemAnimation.FADE_IN)
            ItemAnimation.animate(binding.tvBiologicalAge, -1, ItemAnimation.FADE_IN)
            ItemAnimation.animate(binding.tvChronologicalAge, -1, ItemAnimation.FADE_IN)
            isShowingFirstTimeAfterLoading = !isShowingFirstTimeAfterLoading

            //show floating points only if there is some value other than zero
            var result: Float = score!! - score!!.toInt()
            if ((result != 0f)) {
                binding.tvChronologicalAge.setText(epigenticHealthData?.get(currentHighlatedPos)?.score.toString())
            } else {
                binding.tvChronologicalAge.setText(epigenticHealthData?.get(currentHighlatedPos)?.score?.toInt().toString())
            }

        } else {
            //show floating points only if there is some value other than zero

            var result: Float = score!! - score!!.toInt()
            if ((result != 0f)) {
                setFilpAnimationToTextView(score!!.toString())
            } else {
                setFilpAnimationToTextView(score!!.toInt().toString())
            }
        }


    }

    /*This function works  while you press the down button in the Big circle in the QuickView section*/
    private fun onQuickViewDownButtonPressed() {
        if (epigenticHealthData != null) {
            if (currentHighlatedPos >= epigenticHealthData!!.size - 1) {
                currentHighlatedPos = -1
            }
            currentHighlatedPos++

            var score: Float? =
                epigenticHealthData?.get(currentHighlatedPos)?.score
            binding.tvHighlated.setText(
                epigenticHealthData?.get(currentHighlatedPos)?.title
            )

            if (isShowingFirstTimeAfterLoading) {
                ItemAnimation.animate(binding.tvHighlated, -1, ItemAnimation.FADE_IN)
                ItemAnimation.animate(binding.tvBiologicalAge, -1, ItemAnimation.FADE_IN)
                ItemAnimation.animate(binding.tvChronologicalAge, -1, ItemAnimation.FADE_IN)
                isShowingFirstTimeAfterLoading = !isShowingFirstTimeAfterLoading

                //show floating points only if there is some value other than zero
                var result: Float = score!! - score!!.toInt()
                if ((result != 0f)) {
                    binding.tvChronologicalAge.setText(epigenticHealthData?.get(currentHighlatedPos)?.score.toString())
                } else {
                    binding.tvChronologicalAge.setText(epigenticHealthData?.get(currentHighlatedPos)?.score?.toInt().toString())
                }
            } else {
                var result: Float = score!! - score!!.toInt()
                if ((result != 0f)) {
                    setFilpAnimationToTextView(score!!.toString())
                } else {
                    setFilpAnimationToTextView(score!!.toInt().toString())

                }
            }
        }
    }

    /*get list items from server*/
    /*JIT_EDIT
    private fun getEpigenticResultData() {
        binding.progressBar.visibility = View.VISIBLE
        val manager = NetworkManager()
        if (manager.isConnectingToInternet(requireContext())) {
            val kitId = Utility.getKitID(activity!!)
            val userId= Utility.getUserID(activity!!.applicationContext)
            val epigenReq = EpigenBaseReq(userId,kitId)

            manager.createApiRequest(
                ApiUtilis.getAPIService(com.muhdo.app.utils.v3.Constants.BASE_URL_V3).getEpigenticResultDataForSingleKitIdV3(
                    epigenReq
                ),

                object : ServiceListener<EpigResultResponse> {
                    override fun getServerResponse(
                        epigResultObj: EpigResultResponse,
                        requestcode: Int
                    ) {
                        binding.progressBar.visibility = View.GONE
                        try {
                            if (epigResultObj.code == 200) {
                                if (epigResultObj != null) {
                                    epigResult = epigResultObj!!
                                    setResultData(epigResultObj)
                                } else {
                                }
                            } else if (epigResultObj.code == 401) {
                                TempUtil.log(
                                    "EpiHome",
                                    epigResultObj.code.toString() + " " + epigResultObj.message
                                )
                                context?.showAlertDialog(epigResultObj.message!!)

                            } else {
//                                showDialogBoxWhenAccountSuspended(context,epigResultObj.message.toString())
                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }

                    override fun getError(error: ErrorModel, requestcode: Int) {
                        binding.progressBar.visibility = View.GONE

                        Utility.displayShortSnackBar(
                            binding.parentLayout,
                            error.getMessage()!!
                        )
                    }
                })
        } else {
            binding.progressBar.visibility = View.GONE
            Utility.displayShortSnackBar(
                binding.parentLayout,
                context!!.getString(R.string.check_internet)
            )
        }
    }*/

    /*get current and previous list items from server*/
    private fun getEpigenticResultHomeData() {
        binding.progressBar.visibility = View.VISIBLE
        val manager = NetworkManager()
        if (manager.isConnectingToInternet(requireContext())) {
            val kitId = Utility.getKitID(activity!!)
            val userId = Utility.getUserID(activity!!.applicationContext)
            val epigenReq = EpigenBaseReq(userId, kitId)

            manager.createApiRequest(
                ApiUtilis.getAPIInstance(activity!!).getEpigenticResultDataForHomeV3(
                    UserIdReq(userId)
                ),

                object : ServiceListener<EpigenHomeResponse> {
                    override fun getServerResponse(
                        epigResultObj: EpigenHomeResponse,
                        requestcode: Int
                    ) {
                        binding.progressBar.visibility = View.GONE
                        try {
                            if (epigResultObj.statusCode == 200) {
                                if (epigResultObj != null) {
                                    epigResult = epigResultObj
                                    setResultData(epigResultObj)
                                } else {
                                }
                            } else if (epigResultObj.statusCode == 401) {
                                TempUtil.log(
                                    "EpiHome",
                                    epigResultObj.statusCode.toString() + " " + epigResultObj.message
                                )
                                context?.showAlertDialog(epigResultObj.message!!)

                            } else {
//                                showDialogBoxWhenAccountSuspended(context,epigResultObj.message.toString())
                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }

                    override fun getError(error: ErrorModel, requestcode: Int) {
                        binding.progressBar.visibility = View.GONE

                        Utility.displayShortSnackBar(
                            binding.parentLayout,
                            error.getMessage()!!
                        )
                    }
                })
        } else {
            binding.progressBar.visibility = View.GONE
            Utility.displayShortSnackBar(
                binding.parentLayout,
                context!!.getString(R.string.check_internet)
            )
        }
    }

    var biologicalAge: String = ""

    /*show the response data from the server to UI, ie.. To the header view and Recycler view*/
    private fun setResultData(epigResultObj: EpigenHomeResponse) {
        if (epigResultObj.data != null) {
            setupTabs()

//            var currentShowingEpiItem: EpigenItem? =null
            /*if (selectedTabPostion == 0){
                currentShowingEpiItem=  epigResult?.data?.current
            }else{
                currentShowingEpiItem=  epigResult?.data?.previous
            }

            biologicalAge = currentShowingEpiItem?.age_at_sample.toString()
            if (biologicalAge.isNotEmpty()) {
                binding.tvBiologicalAge.text = biologicalAge
            }
            if (currentShowingEpiItem != null) {

                var scoreListItems: ArrayList<ScoreDataHome> = ArrayList<ScoreDataHome>(0)
                scoreListItems.add(currentShowingEpiItem.biological_info)
                scoreListItems.add(currentShowingEpiItem.eye_info)
                scoreListItems.add(currentShowingEpiItem.hearing_info)
                scoreListItems.add(currentShowingEpiItem.memory_info)
                scoreListItems.add(currentShowingEpiItem.inflammation_info)

                epigenticHealthData = scoreListItems
                epiGenOverviewAdapter?.updatetList(
                    scoreListItems,
                    biologicalAge.toFloat()
                )
                onQuickViewDownButtonPressed()
            }*/
            setFilpAnimationToQuickView(SWIPE_LEFT_TO_RIGHT)
        } else {
            if (epigResultObj.message != null) {
                val builder = AlertDialog.Builder(activity!!)
                builder.setMessage(epigResultObj.message!!)
                builder.setCancelable(false)
                builder.setPositiveButton(R.string.btn_ok) { dialog, _ ->

                    val i = Intent(activity, DashboardActivity::class.java)
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
                    startActivity(i)
                    dialog.dismiss()
                    activity!!.finish()
                }

                val alert = builder.create()
                alert.show()
            }
        }
    }

    /*To show the flip animation to the TextView which shows the score value of the corresponding selected epi-gen Item in the list view*/
    fun setFilpAnimationToTextView(text: String) {
        binding.tvChronologicalAge.setRotationY(0f)
        binding.tvChronologicalAge.animate().rotationY(90f)
            .setListener(object : Animator.AnimatorListener {

                override fun onAnimationStart(animation: Animator) {}

                override fun onAnimationRepeat(animation: Animator) {}

                override fun onAnimationEnd(animation: Animator) {
                    binding.tvChronologicalAge.setText(text)
                    binding.tvChronologicalAge.setRotationY(270f)
                    binding.tvChronologicalAge.animate().rotationY(360f).setListener(null)

                }

                override fun onAnimationCancel(animation: Animator) {}
            })
    }

    fun setFilpAnimationToQuickView(swipe_to_from: Int) {
        isShowingFirstTimeAfterLoading =
            true//so that the fade in animation will work again at the first time.
        binding.layoutAgeAtSample.visibility = View.INVISIBLE
        binding.recyclerEpigeneticOverviewResult.visibility = View.INVISIBLE
        binding.tvOverviewTitle.visibility = View.INVISIBLE
        var firstRotation = 0f;
        var secondRotation = 90f;
        var thirdRotation = 270f;
        var fourthRotation = 360f;
        if (swipe_to_from == SWIPE_RIGHT_TO_LEFT) {
            firstRotation = 360f;
            secondRotation = 270f;
            thirdRotation = 90f;
            fourthRotation = 0f;
        }
        binding.layoutQuickView.setRotationY(firstRotation)
        binding.layoutQuickView.animate().rotationY(secondRotation)
            .setListener(object : Animator.AnimatorListener {

                override fun onAnimationStart(animation: Animator) {}

                override fun onAnimationRepeat(animation: Animator) {}

                override fun onAnimationEnd(animation: Animator) {
//                    binding.tvChronologicalAge.setText(text)
                    binding.layoutQuickView.setRotationY(thirdRotation)
                    binding.layoutQuickView.animate().rotationY(fourthRotation).setListener(null)
                    if (epigResult != null) {

                        var currentShowingEpiItem: EpigenItem? = null
                        if (selectedTabPostion == 0) {
                            currentShowingEpiItem = epigResult?.data?.current
                            TempUtil.currentSelectedEpigeneticTab=1
                        } else {
                            currentShowingEpiItem = epigResult?.data?.previous
                            TempUtil.currentSelectedEpigeneticTab=2
                        }
                        biologicalAge = currentShowingEpiItem?.age_at_sample.toString()
                        if (biologicalAge.isNotEmpty()) {
                            binding.tvBiologicalAge.text = biologicalAge
                        }


                        ItemAnimation.animate(binding.layoutAgeAtSample, -1, ItemAnimation.FADE_IN)
                        ItemAnimation.animate(binding.tvOverviewTitle, -1, ItemAnimation.FADE_IN)

                        binding.layoutAgeAtSample.visibility = View.VISIBLE
                        binding.tvOverviewTitle.visibility = View.VISIBLE
                        binding.recyclerEpigeneticOverviewResult.visibility = View.VISIBLE
                        var scoreListItems: ArrayList<ScoreDataHome> = ArrayList<ScoreDataHome>(0)
                        try {
                            scoreListItems.add(currentShowingEpiItem!!.biological_info)
                            scoreListItems.add(currentShowingEpiItem!!.eye_info)
                            scoreListItems.add(currentShowingEpiItem!!.hearing_info)
                            scoreListItems.add(currentShowingEpiItem!!.memory_info)
                            scoreListItems.add(currentShowingEpiItem!!.inflammation_info)
                        }catch (e:java.lang.Exception){e.printStackTrace()}
                        epigenticHealthData = scoreListItems

                        epiGenOverviewAdapter?.updatetList(
                            scoreListItems,
                            currentShowingEpiItem?.age_at_sample!!.toFloat(),
                            epigResult?.data?.samples
                        )
                        currentHighlatedPos = -1
                        onQuickViewDownButtonPressed()
                    }

                    tabIndicatorUpdation()
                }

                override fun onAnimationCancel(animation: Animator) {}
            })
    }

    var selectedTabPostion = 0

    /*the function will show a flip animation to the age at sample container.
    The visibility of the texts changes once if the container changes*/
    fun showFilpAnimationAgeAtSample() {
        binding.layoutAgeAtSample.setRotationY(0f)
        binding.layoutAgeAtSample.animate().rotationY(90f)
            .setListener(object : Animator.AnimatorListener {

                override fun onAnimationStart(animation: Animator) {}

                override fun onAnimationRepeat(animation: Animator) {}

                override fun onAnimationEnd(animation: Animator) {
                    if (binding.tvAgeAtSample.visibility == View.GONE) {
                        binding.tvAgeAtSample.visibility = View.VISIBLE
                        binding.tvLabelAge.visibility = View.GONE
                        binding.tvBiologicalAge.visibility = View.GONE
                    } else {

                        binding.tvAgeAtSample.visibility = View.GONE
                        binding.tvLabelAge.visibility = View.VISIBLE
                        binding.tvBiologicalAge.visibility = View.VISIBLE
                    }
                    binding.layoutAgeAtSample.setRotationY(270f)
                    binding.layoutAgeAtSample.animate().rotationY(360f).setListener(null)

                }

                override fun onAnimationCancel(animation: Animator) {}
            })
    }

    /*This function will show an information dialog about epigenetic screens.
    The dialog will appear whenever the user comes to the epigenetic Home screen,
    Till he click on the Don't show this messaage again button.*/
    private fun showEpigenDialogInfo() {
        if (Utility.isFirstTimeEpigenDialog(activity!!)) {
            val dialog = Dialog(activity)
            val viewDialog = View.inflate(activity, R.layout.v3_dialog_info_epigen_home, null)
            val closeButton = viewDialog.findViewById<ImageView>(R.id.img_close)
            closeButton.setOnClickListener { dialog.dismiss() }
            dialog.setContentView(viewDialog)
            val tvTitle = viewDialog.findViewById<TextView>(R.id.tvTitle)
            val tvDescription = viewDialog.findViewById<TextView>(R.id.tvDescription)
            val tvOk = viewDialog.findViewById<TextView>(R.id.tvOk)
            val tvDontShowAgain = viewDialog.findViewById<TextView>(R.id.tvDontShowAgain)
            tvTitle.setText(getString(R.string.v3_epi_gen_header_title))
            tvDescription.setText(getString(R.string.v3_epi_gen_info_dialog))
            val text = SpannableString(getString(R.string.v3_btn_dont_show_again))
            text.setSpan(UnderlineSpan(), 0, text.length, 0)
            tvDontShowAgain.setText(text)
            tvOk.setOnClickListener { dialog.dismiss() }
            tvDontShowAgain.setOnClickListener {
                Utility.setFirstTimeEpigenDialog(
                    activity!!,
                    false
                ); dialog.dismiss()
            }
            dialog.setCancelable(false)
            dialog.show()
            if (!TempUtil.epiGenDialogAlreadyChangedOnce) {
                dialog.show()
                TempUtil.epiGenDialogAlreadyChangedOnce = true
            }

        }

    }


    val swipeTouchListener = object : OnSwipeTouchListener(activity) {
        override fun onSwipeLeft() {
            super.onSwipeLeft()
            setFilpAnimationToQuickView(SWIPE_RIGHT_TO_LEFT)
        }

        override fun onSwipeRight() {
            super.onSwipeRight()
            setFilpAnimationToQuickView(SWIPE_LEFT_TO_RIGHT)

        }
    }

    fun tabIndicatorUpdation() {
        if (selectedTabPostion == 0)//current postion
        {
            binding.tabIndicator2.setBackgroundColor(
                ContextCompat.getColor(
                    activity!!,
                    R.color.white
                )
            )
            binding.tvTabText2.setTextColor(ContextCompat.getColor(activity!!, R.color.white))
            binding.tvTabText2.setTypeface(
                Typeface.createFromAsset(
                    activity!!.getAssets(),
                    "roboto_medium.ttf"
                )
            )
            binding.tabIndicator1.setBackgroundColor(
                ContextCompat.getColor(
                    activity!!,
                    android.R.color.transparent
                )
            )
            binding.tvTabText1.setTextColor(
                ContextCompat.getColor(
                    activity!!,
                    R.color.v3_color_epi_arrows
                )
            )
            binding.tvTabText1.setTypeface(
                Typeface.createFromAsset(
                    activity!!.getAssets(),
                    "roboto_regular.ttf"
                )
            )
            selectedTabPostion = 1
        } else {

            binding.tabIndicator1.setBackgroundColor(
                ContextCompat.getColor(
                    activity!!,
                    R.color.white
                )
            )
            binding.tvTabText1.setTextColor(ContextCompat.getColor(activity!!, R.color.white))
            binding.tvTabText1.setTypeface(
                Typeface.createFromAsset(
                    activity!!.getAssets(),
                    "roboto_medium.ttf"
                )
            )
            binding.tabIndicator2.setBackgroundColor(
                ContextCompat.getColor(
                    activity!!,
                    android.R.color.transparent
                )
            )
            binding.tvTabText2.setTextColor(
                ContextCompat.getColor(
                    activity!!,
                    R.color.v3_color_epi_arrows
                )
            )
            binding.tvTabText2.setTypeface(
                Typeface.createFromAsset(
                    activity!!.getAssets(),
                    "roboto_regular.ttf"
                )
            )
            selectedTabPostion = 0
        }
    }
}
