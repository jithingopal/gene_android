package com.muhdo.app.fragment.v3.dna


import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.muhdo.app.R
import com.muhdo.app.adapter.v3.DnaResultListAdapter
import com.muhdo.app.apiModel.keyData.Indicator
import com.muhdo.app.apiModel.keyData.KeyDataModel
import com.muhdo.app.callback.KeyDataClickListener
import com.muhdo.app.repository.ApiUtilis
import com.muhdo.app.repository.ErrorModel
import com.muhdo.app.repository.NetworkManager
import com.muhdo.app.repository.ServiceListener
import com.timingsystemkotlin.backuptimingsystem.Utility
import kotlinx.android.synthetic.main.fragment_dna_result_list.*

class DnaResultListFragment(private val keyDataClickListener: KeyDataClickListener) : Fragment(),
    KeyDataClickListener {

    override fun dataChangeFromSpinner(title: String?, status: String?, percent: Int?) {

    }

    override fun onHeaderItemChange(indicator: Indicator) {
        keyDataClickListener.onHeaderItemChange(indicator)
    }

    override fun OnItemClickListener(categoryId: String?, sectionId: String?, modeId: String?) {
        keyDataClickListener.OnItemClickListener(categoryId, sectionId, modeId)
    }

    private lateinit var keyDataList: List<Indicator>
    private var dnaOverviewAdapter: DnaResultListAdapter? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_dna_result_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        getKeyData(Utility.modeID_V3!!)
    }


    private fun getKeyData(genoTypeId: String) {

        //val userId = Base64.decode(Utility.getUserID(requireContext()), Base64.DEFAULT)
        //val decodeUserId = String(userId, charset("UTF-8"))

        progress_bar.visibility = View.VISIBLE
        val manager = NetworkManager()
        if (manager.isConnectingToInternet(requireContext())) {
            manager.createApiRequest(
                ApiUtilis.getAPIInstance(activity!!).getKeyData(
                    Utility.getUserID(requireContext()),
                    genoTypeId,
                    "true",
                    "true"
                ),
                object : ServiceListener<KeyDataModel> {
                    @RequiresApi(Build.VERSION_CODES.HONEYCOMB)
                    override fun getServerResponse(response: KeyDataModel, requestcode: Int) {
                        if (progress_bar != null) {
                            progress_bar.visibility = View.GONE
                        }
                        if (response.getCode() == 200 && response.getData() != null)
                            setData(response)
                    }

                    override fun getError(error: ErrorModel, requestcode: Int) {
                        progress_bar.visibility = View.GONE
                    }
                })
        } else {
            progress_bar.visibility = View.GONE
            Utility.displayShortSnackBar(
                parent_layout,
                resources.getString(R.string.check_internet)
            )

        }
    }

    private fun setData(data: KeyDataModel) {
        try{populateView(data.getData()!!.getIndicators()!!)}catch (e:Exception){e.printStackTrace()}
    }

    private fun populateView(indicators: List<Indicator>) {
        keyDataList = indicators
        if (dnaOverviewAdapter != null) {
            rv_dna_overview.layoutManager =
                LinearLayoutManager(context, RecyclerView.VERTICAL, false)
            rv_dna_overview.adapter = dnaOverviewAdapter
            //   rv_dna_overview.adapter?.notifyDataSetChanged()
            //  dnaOverviewAdapter?.notifyDataSetChanged()
            dnaOverviewAdapter?.setData(indicators)
        } else {
            dnaOverviewAdapter = DnaResultListAdapter(this)
            rv_dna_overview.layoutManager =
                LinearLayoutManager(context, RecyclerView.VERTICAL, false)
            rv_dna_overview.adapter = dnaOverviewAdapter
            dnaOverviewAdapter?.setData(indicators)
        }
    }

    fun headerTabSwiped(position: Int) {
        fetchKeyData(position)
    }

    private fun fetchKeyData(position: Int) {
        when (position) {
            0 -> {
                // "Health & Wellbeing"
                val modeId = "5c8c96a79c54381add6884dd"
                Utility.modeID_V3 = modeId
                getKeyData(modeId)
            }
            1 -> {
                // "Fat Loss"
                val modeId = "5c8c96a79c54381add6884dc"
                Utility.modeID_V3 = modeId
                getKeyData(modeId)
            }
            2 -> {
                //"Fitness & Endurance"
                val modeId = "5c8c96a79c54381add6884da"
                Utility.modeID_V3 = modeId
                getKeyData(modeId)
            }
            3 -> {
                // "Build Muscle"
                val modeId = "5c8c96a79c54381add6884db"
                Utility.modeID_V3 = modeId
                getKeyData(modeId)
            }
        }
    }
}
