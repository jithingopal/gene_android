package com.muhdo.app.fragment.v3.guide.meal.choose_meal

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.muhdo.app.R
import com.muhdo.app.adapter.v3.ChooseMealPlanParentRecyAdpter
import com.muhdo.app.apiModel.mealModel.Recipe
import com.muhdo.app.apiModel.mealModel.daysModel.Monday
import com.muhdo.app.apiModel.v3.GeneticActionPlanResultData
import com.muhdo.app.databinding.V3FragmentChooseMealForDayBinding

class ChooseMealForDayFragment(private  val dayPosition:Int) : Fragment() {


    internal lateinit var binding: V3FragmentChooseMealForDayBinding

    var adapter: ChooseMealPlanParentRecyAdpter? = null
    var data: MutableList<GeneticActionPlanResultData>? = null
    var parentItem:List<List<Monday>>?=null
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.v3_fragment_choose_meal_for_day, container, false)
        binding = V3FragmentChooseMealForDayBinding.bind(view)
        binding.recyclerChooseMealParent.layoutManager =
            LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        updateListAdapter()

        return view
    }
    public fun setParentItems(parentItem:List<List<Monday>>?){
        this.parentItem=parentItem
    }

    @SuppressLint("WrongConstant")
    private fun replaceFragment(fragment: Fragment) {
        val transaction = activity!!.supportFragmentManager.beginTransaction()
        transaction.replace(R.id.flContent, fragment)
        transaction.addToBackStack("share")
        transaction.commit()

    }


    private fun updateListAdapter() {

        if (parentItem != null) {
            adapter =
                ChooseMealPlanParentRecyAdpter(activity!!,    parentItem,dayPosition )
            /*binding.recyclerChooseMealParent.addItemDecoration(
                DividerItemDecoration(
                    binding.recyclerChooseMealParent.getContext(),
                    DividerItemDecoration.VERTICAL
                )
            )*/
            binding.recyclerChooseMealParent.adapter = adapter
            binding.recyclerChooseMealParent.adapter?.notifyDataSetChanged()

        }
    }

    fun  getSelectedMealList(): MutableList<Recipe>{
        return adapter!!.getSelectedRecipe()
    }



}
