package com.muhdo.app.fragment.v3

import android.annotation.SuppressLint
import android.app.Dialog
import android.os.Bundle
import android.text.Html
import android.util.Base64
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.muhdo.app.R
import com.muhdo.app.apiModel.v3.Answer
import com.muhdo.app.apiModel.v3.QuestionnaireAnswer
import com.muhdo.app.databinding.V3FragmentQuestionnaireHolderExerciseBinding
import com.muhdo.app.repository.ApiUtilis
import com.muhdo.app.utils.v3.*
import com.timingsystemkotlin.backuptimingsystem.Utility
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.v3_fragment_questionnaire_holder_exercise.view.*
import kotlinx.android.synthetic.main.v3_layout_exercise_1.*
import kotlinx.android.synthetic.main.v3_layout_exercise_1.view.*

class QuestionnaireExerciseFragment : Fragment() {
    private var isSkipped: Boolean = false
    private var currentPage = 0

    lateinit var dialog: Dialog;
    internal lateinit var binding: V3FragmentQuestionnaireHolderExerciseBinding
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view =
            inflater.inflate(R.layout.v3_fragment_questionnaire_holder_exercise, container, false)
        binding = V3FragmentQuestionnaireHolderExerciseBinding.bind(view)
        addBottomDots(currentPage)
        setupDialogInfo()

        binding.btnNext.setOnClickListener {
            onNext()
        }
        binding.btnPrevious.setOnClickListener {
            onPrevious()
        }
        binding.layoutExercise1.layout_exercise_1.findViewById<View>(R.id.btn_questionnaire_info)
            .setOnClickListener {
                onShowInfoDialog()
            }
        binding.layoutExercise2.layout_exercise_2.findViewById<View>(R.id.btn_questionnaire_info)
            .setOnClickListener {
                onShowInfoDialog()
            }
        binding.layoutExercise2Bind.tvSkip.setOnClickListener {
            isSkipped = true
            uncheckAllCheckboxes(
                binding.layoutExercise2Bind.btnCbOption1,
                binding.layoutExercise2Bind.btnCbOption2,
                binding.layoutExercise2Bind.btnCbOption3,
                binding.layoutExercise2Bind.btnCbOption4,
                binding.layoutExercise2Bind.btnCbOption5,
                binding.layoutExercise2Bind.btnCbOption6
            )



            getAllAnswersOfQuestionnaire()
        }
        setupActivityLevelListener()

        binding.layoutExercise1Bind.rbExerciseActiveNo.setOnCheckedChangeListener { compoundButton, isChecked ->
            if (isChecked)
                binding.layoutExercise1.layout_activity_level_holder.visibility = View.GONE
            else
                binding.layoutExercise1.layout_activity_level_holder.visibility = View.VISIBLE
        }
        return view
    }

    private fun setupDialogInfo() {
        dialog = Dialog(activity)
        val viewDialog =
            View.inflate(activity, R.layout.v3_dialog_info_questionnaire_exercise, null)
        val closeButton = viewDialog.findViewById<ImageView>(R.id.img_close)
        closeButton.setOnClickListener { dialog.dismiss() }
        dialog.setContentView(viewDialog)
        dialog.setCancelable(false)
    }

    private fun changeActivityLevelIndicator() {
        if (binding.layoutExercise1Bind.cbActivityLevel5.isChecked) {
            binding.layoutExercise1Bind.txtDailyStressLevel.setText(R.string.exercise_stress_level_option_5)
        } else if (binding.layoutExercise1Bind.cbActivityLevel4.isChecked) {
            binding.layoutExercise1Bind.txtDailyStressLevel.setText(R.string.exercise_stress_level_option_4)
        } else if (binding.layoutExercise1Bind.cbActivityLevel3.isChecked) {
            binding.layoutExercise1Bind.txtDailyStressLevel.setText(R.string.exercise_stress_level_option_3)
        } else if (binding.layoutExercise1Bind.cbActivityLevel2.isChecked) {
            binding.layoutExercise1Bind.txtDailyStressLevel.setText(R.string.exercise_stress_level_option_2)
        } else if (binding.layoutExercise1Bind.cbActivityLevel1.isChecked) {
            binding.layoutExercise1Bind.txtDailyStressLevel.setText(R.string.exercise_stress_level_option_1)
        }
    }

    private fun removeAllCheckBoxListeners() {
        binding.layoutExercise1Bind.cbActivityLevel1.setOnCheckedChangeListener(null)
        binding.layoutExercise1Bind.cbActivityLevel2.setOnCheckedChangeListener(null)
        binding.layoutExercise1Bind.cbActivityLevel3.setOnCheckedChangeListener(null)
        binding.layoutExercise1Bind.cbActivityLevel4.setOnCheckedChangeListener(null)
        binding.layoutExercise1Bind.cbActivityLevel5.setOnCheckedChangeListener(null)
    }

    private fun setupActivityLevelListener() {
        binding.layoutExercise1Bind.cbActivityLevel1.setOnCheckedChangeListener { buttonView, isChecked ->
            removeAllCheckBoxListeners()
            binding.layoutExercise1Bind.cbActivityLevel1.isChecked = true
            binding.layoutExercise1Bind.cbActivityLevel2.isChecked = false
            binding.layoutExercise1Bind.cbActivityLevel3.isChecked = false
            binding.layoutExercise1Bind.cbActivityLevel4.isChecked = false
            binding.layoutExercise1Bind.cbActivityLevel5.isChecked = false
            setupActivityLevelListener()
            changeActivityLevelIndicator()
        }

        binding.layoutExercise1Bind.cbActivityLevel2.setOnCheckedChangeListener { buttonView, isChecked ->
            removeAllCheckBoxListeners()
            binding.layoutExercise1Bind.cbActivityLevel1.isChecked = true
            binding.layoutExercise1Bind.cbActivityLevel2.isChecked = true
            binding.layoutExercise1Bind.cbActivityLevel3.isChecked = false
            binding.layoutExercise1Bind.cbActivityLevel4.isChecked = false
            binding.layoutExercise1Bind.cbActivityLevel5.isChecked = false
            setupActivityLevelListener()
            changeActivityLevelIndicator()
        }

        binding.layoutExercise1Bind.cbActivityLevel3.setOnCheckedChangeListener { buttonView, isChecked ->
            removeAllCheckBoxListeners()
            binding.layoutExercise1Bind.cbActivityLevel1.isChecked = true
            binding.layoutExercise1Bind.cbActivityLevel2.isChecked = true
            binding.layoutExercise1Bind.cbActivityLevel3.isChecked = true
            binding.layoutExercise1Bind.cbActivityLevel4.isChecked = false
            binding.layoutExercise1Bind.cbActivityLevel5.isChecked = false
            setupActivityLevelListener()
            changeActivityLevelIndicator()
        }

        binding.layoutExercise1Bind.cbActivityLevel4.setOnCheckedChangeListener { buttonView, isChecked ->
            removeAllCheckBoxListeners()
            binding.layoutExercise1Bind.cbActivityLevel1.isChecked = true
            binding.layoutExercise1Bind.cbActivityLevel2.isChecked = true
            binding.layoutExercise1Bind.cbActivityLevel3.isChecked = true
            binding.layoutExercise1Bind.cbActivityLevel4.isChecked = true
            binding.layoutExercise1Bind.cbActivityLevel5.isChecked = false
            setupActivityLevelListener()
            changeActivityLevelIndicator()
        }

        binding.layoutExercise1Bind.cbActivityLevel5.setOnCheckedChangeListener { buttonView, isChecked ->
            removeAllCheckBoxListeners()
            binding.layoutExercise1Bind.cbActivityLevel1.isChecked = true
            binding.layoutExercise1Bind.cbActivityLevel2.isChecked = true
            binding.layoutExercise1Bind.cbActivityLevel3.isChecked = true
            binding.layoutExercise1Bind.cbActivityLevel4.isChecked = true
            binding.layoutExercise1Bind.cbActivityLevel5.isChecked = true
            setupActivityLevelListener()
            changeActivityLevelIndicator()
        }


    }


    private fun onShowInfoDialog() {
        if (!dialog.isShowing)
            dialog.show()
    }

    private fun onNext() {
        if (currentPage == 0) {
            if (rb_exercise_active_yes.isChecked && atleastOneCheckBoxSelected(
                    cb_activity_level_1,
                    cb_activity_level_2,
                    cb_activity_level_3,
                    cb_activity_level_4,
                    cb_activity_level_5
                )
                || rb_exercise_active_no.isChecked
            ) {
                if (rb_exercise_active_no.isChecked)
                    uncheckAllCheckboxes(
                        cb_activity_level_1,
                        cb_activity_level_2,
                        cb_activity_level_3,
                        cb_activity_level_4,
                        cb_activity_level_5
                    )
                binding.btnPrevious.visibility = View.VISIBLE
                binding.layoutExercise1.visibility = View.GONE
                binding.layoutExercise2.visibility = View.VISIBLE
                currentPage = 1
            }
            /* if (rg_exercise_active.checkedRadioButtonId != -1 && atleastOneCheckBoxSelected(
                     cb_activity_level_1,
                     cb_activity_level_2,
                     cb_activity_level_3,
                     cb_activity_level_4,
                     cb_activity_level_5
                 )
             ) {*/
            else context?.showAlertDialog(getString(R.string.answer_all_question_message))
        } else if (currentPage == 1) {
            if (atleastOneCheckBoxSelected(
                    binding.layoutExercise2Bind.btnCbOption1,
                    binding.layoutExercise2Bind.btnCbOption2,
                    binding.layoutExercise2Bind.btnCbOption3,
                    binding.layoutExercise2Bind.btnCbOption4,
                    binding.layoutExercise2Bind.btnCbOption5,
                    binding.layoutExercise2Bind.btnCbOption6
                ) || isSkipped
            )
                getAllAnswersOfQuestionnaire()
            else
                context?.showAlertDialog(getString(R.string.answer_all_question_message))
        }
        addBottomDots(currentPage)
    }

    private fun getAllAnswersOfQuestionnaire() {
        TempUtil.log(
            "Exercise answers ",
            binding.layoutExercise1Bind.rgExerciseActive.getSelectedRadioButtonText()
                    + getNumberOfSelectedCheckbox(
                binding.layoutExercise1Bind.cbActivityLevel1,
                binding.layoutExercise1Bind.cbActivityLevel2,
                binding.layoutExercise1Bind.cbActivityLevel3,
                binding.layoutExercise1Bind.cbActivityLevel4,
                binding.layoutExercise1Bind.cbActivityLevel5
            )
        )

        TempUtil.log(
            "Exercise answers 2",
            getAllSelectedCheckboxTexts(
                binding.layoutExercise2Bind.btnCbOption1,
                binding.layoutExercise2Bind.btnCbOption2,
                binding.layoutExercise2Bind.btnCbOption3,
                binding.layoutExercise2Bind.btnCbOption4,
                binding.layoutExercise2Bind.btnCbOption5
            ).toString()
        )
        val userId = Base64.decode(Utility.getUserID(activity!!.applicationContext), Base64.DEFAULT)
        val decodeUserId = String(userId, charset("UTF-8"))
        val answers = arrayListOf<Answer>()
        val exerciseActive = Answer(
            question = Constants.EXERCISE.YOURSELF_ACTIVE,
            answer = listOf(binding.layoutExercise1Bind.rgExerciseActive.getSelectedRadioButtonText())
        )
        answers.add(exerciseActive)
        val activityLevel: Answer
        if (binding.layoutExercise1Bind.rbExerciseActiveYes.isChecked) {
            activityLevel = Answer(
                question = Constants.EXERCISE.EFFORT_LEVEL,
                answer = listOf(
                    getNumberOfSelectedCheckbox(
                        binding.layoutExercise1Bind.cbActivityLevel1,
                        binding.layoutExercise1Bind.cbActivityLevel2,
                        binding.layoutExercise1Bind.cbActivityLevel3,
                        binding.layoutExercise1Bind.cbActivityLevel4,
                        binding.layoutExercise1Bind.cbActivityLevel5
                    )
                )
            )
        } else {
            activityLevel = Answer(
                question = Constants.EXERCISE.EFFORT_LEVEL,
                answer = listOf("0")
            )
        }
        answers.add(activityLevel)
        val impactItems: Answer
        if (isSkipped) {
            impactItems = Answer(
                question = Constants.EXERCISE.STOPPING_YOU,
                answer = listOf(Constants.ANSWER.SKIP)
            )
        } else {
            impactItems = Answer(
                question = Constants.EXERCISE.STOPPING_YOU,
                answer = Constants.createExerciseImpactsArray(
                    getAllSelectedCheckboxTexts(
                        binding.layoutExercise2Bind.btnCbOption1,
                        binding.layoutExercise2Bind.btnCbOption2,
                        binding.layoutExercise2Bind.btnCbOption3,
                        binding.layoutExercise2Bind.btnCbOption4,
                        binding.layoutExercise2Bind.btnCbOption5
                    )
                )
            )
        }
        answers.add(impactItems)
        val requestBody = QuestionnaireAnswer(
            user_id = decodeUserId,
            answers = answers,
            category = Constants.QUESTIONNAIRE.EXERCISE
        )
        uploadAnswers(requestBody)
    }

    private fun uploadAnswers(requestBody: QuestionnaireAnswer) {
        binding.progressBar.visibility = View.VISIBLE
        ApiUtilis.getAPIInstance(activity!!).postReviewQuestionnaireAnswers(requestBody)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ success ->
                run {
                    binding.progressBar.visibility = View.GONE
                    Utility.displayShortSnackBar(binding.root, success.message)
                    // Log.d("response from Exercise", success.message)
                    replaceFragment(QuestionnaireHomeFragment())
                    TempUtil.isLifestyleQuestionnaireCompleted = true
                }
            }, { error ->
                run {
                    binding.progressBar.visibility = View.GONE
                    Utility.displayShortSnackBar(binding.root, error.localizedMessage)
                    // Log.d("response from Exercise ", error.localizedMessage)
                }
            })
    }


    @SuppressLint("WrongConstant")
    private fun replaceFragment(fragment: Fragment) {
        val transaction = activity!!.supportFragmentManager.beginTransaction()
        transaction.replace(R.id.flContent, fragment)
        transaction.addToBackStack("share")
        transaction.commit()

    }

    private fun onPrevious() {

        if (currentPage == 0) {

            binding.btnPrevious.visibility = View.GONE
            binding.btnNext.visibility = View.VISIBLE
            currentPage = 0

        } else if (currentPage == 1) {
            if (isSkipped)
                isSkipped = false
            binding.btnPrevious.visibility = View.GONE
            binding.btnNext.visibility = View.VISIBLE

            binding.layoutExercise1.visibility = View.VISIBLE
            binding.layoutExercise2.visibility = View.GONE
            currentPage = 0
        } else if (currentPage == 2) {

            binding.btnPrevious.visibility = View.VISIBLE
            binding.btnNext.visibility = View.VISIBLE

            binding.layoutExercise1.visibility = View.GONE
            binding.layoutExercise2.visibility = View.VISIBLE
            currentPage = 1
        }
        addBottomDots(currentPage)

    }

    private fun addBottomDots(currentPage: Int) {


        val dots = arrayOfNulls<TextView>(2)
        binding.layoutDots.removeAllViews()
        for (i in dots.indices) {

            dots[i] = TextView(activity)
            dots[i]!!.text = Html.fromHtml("&#8226;")
            dots[i]!!.textSize = 35f
            dots[i]!!.setTextColor(
                ContextCompat.getColor(
                    activity!!.applicationContext,
                    R.color.secondary_color
                )
            )
            binding.layoutDots.addView(dots[i])
        }

        if (dots.isNotEmpty()) {
            dots[currentPage]!!.setTextColor(
                ContextCompat.getColor(
                    activity!!.applicationContext,
                    R.color.color_dark_blue
                )
            )
        }
    }
}
