package com.muhdo.app.fragment.v3

import android.annotation.SuppressLint
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.util.Base64
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.muhdo.app.R
import com.muhdo.app.adapter.v3.GeneticOverviewResultRecyAdpter
import com.muhdo.app.apiModel.v3.GeneticOverviewReq
import com.muhdo.app.apiModel.v3.GeneticOverviewResultData
import com.muhdo.app.apiModel.v3.GeneticOverviewResultPojo
import com.muhdo.app.databinding.V3FragmentGeneticOverviewResultItemBinding
import com.muhdo.app.interfaces.v3.OnOverviewLoadListener
import com.muhdo.app.repository.ApiUtilis
import com.muhdo.app.repository.NetworkManager2
import com.muhdo.app.ui.DashboardActivity
import com.muhdo.app.utils.PreferenceConnector
import com.muhdo.app.utils.v3.Constants
import com.muhdo.app.utils.v3.TempUtil
import com.timingsystemkotlin.backuptimingsystem.Utility
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class GeneticOverviewResultItemFragment : Fragment() {
    lateinit var loadListener: OnOverviewLoadListener;


    internal lateinit var binding: V3FragmentGeneticOverviewResultItemBinding
    var adapter: GeneticOverviewResultRecyAdpter? = null
    var data: MutableList<GeneticOverviewResultData>? = null

    var OVERVIEW_TYPE: String = "SLEEP"
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view =
            inflater.inflate(R.layout.v3_fragment_genetic_overview_result_item, container, false)
        binding = V3FragmentGeneticOverviewResultItemBinding.bind(view)
        binding.recyclerGeneticOverviewResult.layoutManager =
            LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        updateListAdapter()
        if (isNetworkAvailable())
            readOverviewResultFromServer()
        setTitleDescription()
        binding.btnViewDnaResults.setOnClickListener {
            val title: String? = PreferenceConnector.readString(
                activity!!.applicationContext,
                PreferenceConnector.MODE_TEXT,
                "Fitness"
            )
            val id: String? = PreferenceConnector.readString(
                activity!!.applicationContext,
                PreferenceConnector.MODE_ID,
                "5c8c96a79c54381add6884da"
            )

            val i = Intent(activity, DashboardActivity::class.java)
            i.putExtra("id", id)
            i.putExtra("title", title)
            i.putExtra(
                Constants.DASHBOARD_ACTION,
                Constants.DASHBOARD_RESULT
            )
            startActivity(i)

/*            var resultsFragment: ResultsFragment = ResultsFragment()
            resultsFragment.setData(id!!, title!!)
            replaceFragment(resultsFragment)*/
        }
        binding.btnGenerateActionPlan.setOnClickListener {
            fetchQuestionnaireStatus()
        }
        return view
    }


    private fun isNetworkAvailable(): Boolean {
        val isConnected = context?.let { NetworkManager2().isConnectingToInternet(it) } ?: false
        if (!isConnected)
            Utility.displayLongSnackBar(
                binding.btnViewDnaResults,
                getString(R.string.check_internet)
            )
        return isConnected
    }

    fun setOnLoadListener(loadListener: OnOverviewLoadListener) {
        this.loadListener = loadListener
    }

    fun setNoItemInfoText() {
        if (data != null) {
            if (data!!.size <= 0) {
                binding.tvNoListItemInfo.visibility = View.VISIBLE
                if (OVERVIEW_TYPE.equals(Constants.GENETIC_OVERVIEW_TYPES[0])) {
                    binding.tvNoListItemInfo.setText(R.string.no_item_text_for_diet)

                } else if (OVERVIEW_TYPE.equals(Constants.GENETIC_OVERVIEW_TYPES[1])) {
                    binding.tvNoListItemInfo.setText(R.string.no_item_text_for_vitamin)

                } else if (OVERVIEW_TYPE.equals(Constants.GENETIC_OVERVIEW_TYPES[2])) {
                    binding.tvNoListItemInfo.setText(R.string.no_item_text_for_health_risk)

                } else if (OVERVIEW_TYPE.equals(Constants.GENETIC_OVERVIEW_TYPES[3])) {
                    binding.tvNoListItemInfo.setText(R.string.no_item_text_for_health_gift)

                } else if (OVERVIEW_TYPE.equals(Constants.GENETIC_OVERVIEW_TYPES[4])) {
                    binding.tvNoListItemInfo.setText(R.string.no_item_text_for_health_warning)

                } else if (OVERVIEW_TYPE.equals(Constants.GENETIC_OVERVIEW_TYPES[5])) {
                    binding.tvNoListItemInfo.setText(R.string.no_item_text_for_sleep)

                }
            } else {
                binding.tvNoListItemInfo.visibility = View.GONE
            }


        }
    }

    fun setTitleDescription() {
        if (OVERVIEW_TYPE.equals(Constants.GENETIC_OVERVIEW_TYPES[0])) {
            binding.tvTitleDescription.setText(R.string.genetic_overview_result_item_main_description_diet_weight_concerns)

        } else if (OVERVIEW_TYPE.equals(Constants.GENETIC_OVERVIEW_TYPES[1])) {
            binding.tvTitleDescription.setText(R.string.genetic_overview_result_item_main_description_vitamin_deficiencies)

        } else if (OVERVIEW_TYPE.equals(Constants.GENETIC_OVERVIEW_TYPES[2])) {
            binding.tvTitleDescription.setText(R.string.genetic_overview_result_item_main_description_physical_health_risks)

        } else if (OVERVIEW_TYPE.equals(Constants.GENETIC_OVERVIEW_TYPES[3])) {
            binding.tvTitleDescription.setText(R.string.genetic_overview_result_item_main_description_physical_health_gifts)

        } else if (OVERVIEW_TYPE.equals(Constants.GENETIC_OVERVIEW_TYPES[4])) {
            binding.tvTitleDescription.setText(R.string.genetic_overview_result_item_main_description_health_warnings)

        } else if (OVERVIEW_TYPE.equals(Constants.GENETIC_OVERVIEW_TYPES[5])) {
            binding.tvTitleDescription.setText(R.string.genetic_overview_result_item_main_description_sleep_issues)

        }
    }

    fun setOverViewType(type: String) {
        OVERVIEW_TYPE = type
    }

    private fun updateListAdapter() {
        if (data != null) {
            setNoItemInfoText()
            adapter =
                GeneticOverviewResultRecyAdpter(activity!!.applicationContext, this, data!!)
            binding.recyclerGeneticOverviewResult.addItemDecoration(
                DividerItemDecoration(
                    binding.recyclerGeneticOverviewResult.getContext(),
                    DividerItemDecoration.VERTICAL
                )
            )

            binding.recyclerGeneticOverviewResult.adapter = adapter
            binding.recyclerGeneticOverviewResult.adapter?.notifyDataSetChanged()
        }
    }

    @SuppressLint("WrongConstant")
    private fun replaceFragment(fragment: Fragment) {
        val transaction = activity!!.supportFragmentManager.beginTransaction()
        transaction.replace(R.id.flContent, fragment)
        transaction.addToBackStack("share")
        transaction.commit()

    }

    private fun readOverviewResultFromServer() {

        val userId = Base64.decode(
            Utility.getUserID(activity!!.applicationContext),
            Base64.DEFAULT
        )
        val decodeUserId = String(userId, charset("UTF-8"))
        binding.progressBar.visibility = View.VISIBLE
        val jsonParams = JSONObject()
        jsonParams.put("user_id", decodeUserId)
        jsonParams.put("type", OVERVIEW_TYPE)
        val requestBody = GeneticOverviewReq(
            decodeUserId, OVERVIEW_TYPE
        )
        TempUtil.log("Jithin", jsonParams.toString())
        val manager = NetworkManager2()
        if (manager.isConnectingToInternet(binding.parentLayout.context)) {
            val call =
                ApiUtilis.getAPIInstance(activity!!)
                    .postGeneticOverviewList(requestBody)
            call.enqueue(object : Callback<GeneticOverviewResultPojo> {
                override fun onResponse(
                    call: Call<GeneticOverviewResultPojo>,
                    response: Response<GeneticOverviewResultPojo>
                ) {
                    binding.progressBar.visibility = View.GONE
                    val resultPojo: GeneticOverviewResultPojo? = response.body()
                    try {
                        if (resultPojo != null && resultPojo.statusCode == 200) {
                            data = resultPojo.data
                            TempUtil.log("Jithin", resultPojo.message.toString())
                            updateListAdapter()
                            loadListener.onLoadingCompleted(true)
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }

                override fun onFailure(call: Call<GeneticOverviewResultPojo>, t: Throwable) {
                    // Log error here since request failed
                    binding.progressBar.visibility = View.GONE
                    TempUtil.log("Login", t.toString())

                    Utility.displayShortSnackBar(
                        binding.parentLayout,
                        "Server connection error!!, please try again later."
                    )


                }
            })
        } else {
            binding.progressBar.visibility = View.GONE
            Utility.displayShortSnackBar(
                binding.btnViewDnaResults,
                binding.parentLayout.context.resources.getString(R.string.check_internet)
            )
        }
    }

    private fun fetchQuestionnaireStatus() {
        binding.progressBar.visibility = View.VISIBLE
        val userId = Base64.decode(Utility.getUserID(activity!!.themedContext), Base64.DEFAULT)
        val decodeUserId = String(userId, charset("UTF-8"))
        ApiUtilis.getAPIInstance(activity!!).getQuestionnaireStatus(userID = decodeUserId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ success ->
                run {
                    binding.progressBar.visibility = View.GONE
                    var isQuestionnaireCompleted: Boolean = false;

                    if (success.data?.get(0)?.status!! &&
                        success.data?.get(1)?.status!! &&
                        success.data?.get(2)?.status!! &&
                        success.data?.get(3)?.status!!
                    ) {
                        isQuestionnaireCompleted = true
                    }
                    if (isQuestionnaireCompleted) {

                        TempUtil.currentFragment =
                            TempUtil.NO_FRAGMENTS_SELECTED// binding.navigationView.selectedItemId
                        replaceFragment(GeneticActionPlanHomeFragment())

                    } else {
                        var dialog: androidx.appcompat.app.AlertDialog.Builder =
                            androidx.appcompat.app.AlertDialog.Builder(activity!!.themedContext)
                        dialog.setMessage(R.string.fill_questionnaire_warning_while_generate_action_plan)
                        dialog.setPositiveButton(
                            R.string.btn_go_to_questionnaire_warning,
                            DialogInterface.OnClickListener { dialog, id ->
                                dialog.cancel()
                                TempUtil.currentFragment =
                                    1000// binding.navigationView.selectedItemId
                                replaceFragment(QuestionnaireHomeFragment())
                                dialog.cancel()
                            })
                        dialog.setNegativeButton(
                            R.string.btn_cancel,
                            DialogInterface.OnClickListener { dialog, id ->
                                dialog.cancel()
                            })
                        dialog.show()

                    }
                }
            }, { error ->
                run {
                    binding.progressBar.visibility = View.GONE
                }
            })
    }

}
