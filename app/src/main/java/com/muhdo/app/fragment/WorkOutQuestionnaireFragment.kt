package com.muhdo.app.fragment

import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.text.Html
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.muhdo.app.R
import com.muhdo.app.apiModel.mealModel.daysModel.UpdateFavoriteModel
import com.muhdo.app.apiModel.workout.WorkoutQuestionnaireRequest
import com.muhdo.app.data.WorkoutData
import com.muhdo.app.databinding.FragmentQuestionAnswerBinding
import com.muhdo.app.repository.ApiUtilis
import com.muhdo.app.repository.ErrorModel
import com.muhdo.app.repository.NetworkManager
import com.muhdo.app.repository.ServiceListener
import com.muhdo.app.ui.workout.WorkoutLoading
import com.muhdo.app.utils.Events
import com.muhdo.app.utils.GlobalBus
import com.muhdo.app.utils.v3.Constants
import com.timingsystemkotlin.backuptimingsystem.Utility
import org.greenrobot.eventbus.Subscribe

class WorkOutQuestionnaireFragment : Fragment() {

    internal lateinit var binding: FragmentQuestionAnswerBinding
    private var currentPage = 0

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        GlobalBus.getBus()!!.register(this@WorkOutQuestionnaireFragment)
        val view = inflater.inflate(R.layout.fragment_question_answer, container, false)
        binding = FragmentQuestionAnswerBinding.bind(view)
        addBottomDots(currentPage)
        resetData()
        init()
        binding.btncontinue.setOnClickListener {
            when (currentPage) {
                0 -> {
                    if (Utility.isGoalDone(activity!!.applicationContext)) {
                        currentPage++
                        addBottomDots(currentPage)

                    } else {
                        showAlert("Please select your goal")
                    }
                }
                1 -> {
                    if (Utility.isPersonalDetails(activity!!.applicationContext) &&
                        Utility.isGender(activity!!.applicationContext)
                    ) {
                        currentPage++
                        addBottomDots(currentPage)

                    } else {

                        if (!Utility.isPersonalDetails(activity!!.applicationContext)) {
                            showAlert("Please enter correct date of birth")
                        } else if (!Utility.isGender(activity!!.applicationContext)) {
                            showAlert("Please select your sex")
                        }


//
                    }

                }
                2 -> {
                    if (Utility.isBMIIndex(activity!!.applicationContext) && Utility.isBMIIndex1(
                            activity!!.applicationContext
                        )
                    ) {
                        currentPage++
                        addBottomDots(currentPage)

                    } else {
                        showAlert("Please enter your height and weight")
                    }

                }
                3 -> {
                    if (Utility.isTraining(activity!!.applicationContext) && Utility.isTrainingRange(
                            activity!!.applicationContext
                        )
                    ) {
                        currentPage++
                        addBottomDots(currentPage)


                    } else {
                        showAlert("Please select the length of time you have to train")
                    }

                }
                4 -> {
                    if (Utility.isEquipment(activity!!.applicationContext)) {
                        currentPage++
                        addBottomDots(currentPage)

                    } else {
                        showAlert("Please indicate if you have equipment available")
                    }

                }
                5 -> {
                    if (Utility.isExperience(activity!!.applicationContext)) {
                        currentPage++
                        addBottomDots(currentPage)

                    } else {
                        showAlert("Please indicate your training experience")
                    }

                }
                6 -> {
                    if (Utility.isDR(activity!!.applicationContext) &&
                        Utility.isInjured(activity!!.applicationContext) &&
                        Utility.isPregnant(activity!!.applicationContext)
                    ) {
                        currentPage++
                        addBottomDots(currentPage)

                    } else {
                        showAlert("Please complete your medical details")
                    }

                }
                7 -> {
                    if (Utility.isDiagnosed(activity!!.applicationContext)) {
                        sendDataToServer()

//
                    } else {
                        showAlert("Please complete your medical details")
                    }

                }


            }

            binding.imgClose.setOnClickListener {
                activity!!.fragmentManager.popBackStack()
            }

        }
        return view

    }


    private fun init() {
        for (i in 0..20) {
            Utility.list.add(false)
            Utility.listValue.add("")
        }
    }

    private fun resetData() {
        Utility.setGoalDone(activity!!.applicationContext, false)
        Utility.setPersonalDetails(activity!!.applicationContext, false)
        Utility.setBMIIndex(activity!!.applicationContext, false)
        Utility.setBMIIndex1(activity!!.applicationContext, false)
        Utility.setTraining(activity!!.applicationContext, false)
        Utility.setTrainingRange(activity!!.applicationContext, false)
        Utility.setEquipment(activity!!.applicationContext, false)
        Utility.setExperience(activity!!.applicationContext, false)
        Utility.setDR(activity!!.applicationContext, false)
        Utility.setInjured(activity!!.applicationContext, false)
        Utility.setPregnant(activity!!.applicationContext, false)
        Utility.setDiagnosed(activity!!.applicationContext, false)
        Utility.setGender(activity!!.applicationContext, false)

    }

    private fun addBottomDots(currentPage: Int) {

        when (currentPage) {
            1 -> {
                binding.btncontinue.visibility = View.VISIBLE
            }
            4 -> {
                binding.btncontinue.visibility = View.INVISIBLE
            }
            6 -> {
                binding.btncontinue.visibility = View.VISIBLE
            }
        }

        val dots = arrayOfNulls<TextView>(8)
        binding.layoutDots.removeAllViews()
        for (i in dots.indices) {

            dots[i] = TextView(activity)
            dots[i]!!.text = Html.fromHtml("&#8226;")
            dots[i]!!.textSize = 35f
            dots[i]!!.setTextColor(
                ContextCompat.getColor(
                    activity!!.applicationContext,
                    R.color.secondary_color
                )
            )
            binding.layoutDots.addView(dots[i])
        }

//        .getColor(context, R.color.color_name)
        if (dots.isNotEmpty()) {
            dots[currentPage]!!.setTextColor(
                ContextCompat.getColor(
                    activity!!.applicationContext,
                    R.color.color_dark_blue
                )
            )
        }
        loadFragment(currentPage)
    }

    private fun loadFragment(position: Int) {
        val transaction = childFragmentManager.beginTransaction()
        //  val transaction = activity!!.supportFragmentManager.beginTransaction()
        val bundle = Bundle()
        when (position) {
            0 -> {
                bundle.putString(
                    Constants.WORKOUT_QUESTION_TYPE,
                    Constants.WORKOUT_QUESTION_TYPE_GOAL
                )
            }
            1 -> {
                bundle.putString(
                    Constants.WORKOUT_QUESTION_TYPE,
                    Constants.WORKOUT_QUESTION_TYPE_PERSONAL
                )
            }
            2 -> {
                bundle.putString(
                    Constants.WORKOUT_QUESTION_TYPE,
                    Constants.WORKOUT_QUESTION_TYPE_BMI
                )
            }
            3 -> {
                bundle.putString(
                    Constants.WORKOUT_QUESTION_TYPE,
                    Constants.WORKOUT_QUESTION_TYPE_TRAINING
                )
            }
            4 -> {
                bundle.putString(
                    Constants.WORKOUT_QUESTION_TYPE,
                    Constants.WORKOUT_QUESTION_TYPE_EQUIPMENT
                )
            }
            5 -> {
                bundle.putString(
                    Constants.WORKOUT_QUESTION_TYPE,
                    Constants.WORKOUT_QUESTION_TYPE_EXPERIENCE
                )
            }
            6 -> {
                bundle.putString(
                    Constants.WORKOUT_QUESTION_TYPE,
                    Constants.WORKOUT_QUESTION_TYPE_MEDICAL
                )
            }
            7 -> {
                bundle.putString(
                    Constants.WORKOUT_QUESTION_TYPE,
                    Constants.WORKOUT_QUESTION_TYPE_DIAGNOSED
                )
            }
        }
        val fragInfo = WorkOutQuestionAnswerFragment()
        fragInfo.arguments = bundle
        transaction.replace(R.id.flContent, fragInfo)
        transaction.commit()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        // unregister the registered event.
        GlobalBus.getBus()!!.unregister(this)
    }

    @Subscribe
    fun getMessage(sendPageCount: Events.sendPageCount) {
        currentPage = sendPageCount.page
        addBottomDots(currentPage)
    }


    private fun showAlert(msg: String) {
        val builder = AlertDialog.Builder(activity)
        builder.setMessage(msg)

        builder.setPositiveButton("OK") { _, _ ->

        }

        val alert = builder.create()
        alert.show()
    }


    private fun sendDataToServer() {
        binding.progressBar.visibility = View.VISIBLE
        binding.flContent.visibility = View.GONE
        val manager = NetworkManager()
        var height: Int
        var weight: Int

        height = if (Utility.listValue[4].contains(".")) {
            // full file name
            val parts = Utility.listValue[4].split("\\.".toRegex()).dropLastWhile { it.isEmpty() }
                .toTypedArray() // String array, each element is text between dots

            val beforeFirstDot = parts[0]
            Integer.parseInt(beforeFirstDot)

        } else {
            Integer.parseInt(Utility.listValue[4])
        }
        weight = if (Utility.listValue[5].contains(".")) {
            // full file name
            val parts = Utility.listValue[5].split("\\.".toRegex()).dropLastWhile { it.isEmpty() }
                .toTypedArray() // String array, each element is text between dots

            val beforeFirstDot = parts[0]
            Integer.parseInt(beforeFirstDot)
        } else {
            Integer.parseInt(Utility.listValue[5])
        }

        if (height <= 0) {
            height = 25
        }

        if (weight <= 0) {
            weight = 25
        }

        val activityList: MutableList<Int> = arrayListOf()
        activityList.add(Utility.listValue[7].toInt())
        activityList.add(Utility.listValue[8].toInt())
        val workoutQuestionnaireRequest = WorkoutQuestionnaireRequest(
            Utility.listValue[0],
            Utility.formatDate(Utility.listValue[10]),
            height,
            Utility.listValue[2],
            weight,
            Utility.listValue[3],
            Utility.listValue[6].toInt(),
            activityList,
            Utility.listValue[1].toInt(),
            Utility.list[0],
            Utility.listValue[11],
            Utility.list[1], Utility.list[2],
            Utility.diseaseList, Utility.listValue[9].toInt(), "workout",
            Utility.getUserID(requireContext())
        )
        val data = WorkoutData()
        data.new = Utility.listValue[0]
        data.new1 = Utility.listValue[6]
        data.new2 = Utility.listValue[7]
        data.new3 = Utility.listValue[8]
        data.new4 = Utility.listValue[1]
        data.new5 = Utility.listValue[9]
        data.new6 = Utility.list[0].toString()
        data.new7 = Utility.list[1].toString()
        data.new8 = Utility.list[2].toString()


        Log.wtf("data", "dob" + Utility.listValue[10])


        if (manager.isConnectingToInternet(activity!!.applicationContext)) {
            manager.createApiRequest(
                ApiUtilis.getAPIInstance(activity!!).sendWorkOutSurvey(workoutQuestionnaireRequest),
                object : ServiceListener<UpdateFavoriteModel> {
                    override fun getServerResponse(
                        response: UpdateFavoriteModel,
                        requestcode: Int
                    ) {
                        binding.progressBar.visibility = View.GONE
                        binding.flContent.visibility = View.VISIBLE
                        if (response.getCode() == 200) {
                            if (response.getData()!!.getMessage().equals("yes")) {
                                Utility.diseaseList.clear()

                                /*val workoutData = Utility.getWorkoutData(activity!!)
                                if (workoutData != null) {
                                    Utility.deleteWorkoutInfo(activity!!.applicationContext, data)
                                    Utility.saveWorkoutInfo(activity!!.applicationContext, data)
                                } else {
                                    Utility.saveWorkoutInfo(activity!!.applicationContext, data)
                                }*/
                                Utility.setWorkoutStatus(requireContext(), true)
                                /* val intent = Intent(activity, WorkoutLoading::class.java)
                                 intent.putExtra("show_alert", response.getData()!!.getMessage())
                                 intent.putExtra("show_alert_msg", response.getMessage())
                                 startActivity(intent)
 */
                                displayAlert(response.getMessage())

                            } else {
                                Utility.diseaseList.clear()
                                /*val workoutData = Utility.getWorkoutData(activity!!)
                                if (workoutData != null) {
                                    Utility.deleteWorkoutInfo(activity!!.applicationContext, data)
                                    Utility.saveWorkoutInfo(activity!!.applicationContext, data)
                                } else {
                                    Utility.saveWorkoutInfo(activity!!.applicationContext, data)
                                }*/
                                Utility.setWorkoutStatus(requireContext(), true)
                                val intent = Intent(activity, WorkoutLoading::class.java)
                                intent.putExtra("show_alert", response.getData()!!.getMessage())
                                startActivity(intent)

                            }

                        }

                        /* binding.flContent.visibility = View.VISIBLE
                         val workoutData = Utility.getWorkoutData(activity!!)
                         if (workoutData != null) {
                             Utility.deleteWorkoutInfo(activity!!.applicationContext, data)
                             Utility.saveWorkoutInfo(activity!!.applicationContext, data)
                         } else {
                             Utility.saveWorkoutInfo(activity!!.applicationContext, data)
                           }
                          Utility.setWorkoutStatus(requireContext(), true)
                         val i = Intent(activity, WorkoutLoading::class.java)
                         startActivity(i)*/
                    }

                    override fun getError(error: ErrorModel, requestcode: Int) {
                        binding.progressBar.visibility = View.GONE
                        binding.flContent.visibility = View.VISIBLE
                        Utility.displayShortSnackBar(
                            binding.parentLayout,
                            error.getMessage().toString()
                        )
                    }
                })
        } else {
            binding.progressBar.visibility = View.GONE
            binding.flContent.visibility = View.VISIBLE
            Utility.displayShortSnackBar(
                binding.parentLayout,
                resources.getString(R.string.check_internet)
            )
        }
    }


    private fun displayAlert(message: String?) {
        val dialogBuilder = AlertDialog.Builder(requireContext())
        dialogBuilder.setMessage(message)
            .setCancelable(false)
            .setPositiveButton("Ok") { _, _ ->
                val intent = Intent(activity, WorkoutLoading::class.java)
                startActivity(intent)

            }


        val alert = dialogBuilder.create()
        alert.show()
    }

}