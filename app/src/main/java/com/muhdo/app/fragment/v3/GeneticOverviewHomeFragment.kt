package com.muhdo.app.fragment.v3

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.muhdo.app.R
import com.muhdo.app.databinding.V3FragmentGeneticOverviewHomeBinding

class GeneticOverviewHomeFragment : Fragment() {

    internal lateinit var binding: V3FragmentGeneticOverviewHomeBinding
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.v3_fragment_genetic_overview_home, container, false)

        binding = V3FragmentGeneticOverviewHomeBinding.bind(view)
        binding.btnDietWeightConcerns.setOnClickListener{
            var geneticOverview=GeneticOverviewResultFragment()
            geneticOverview.selectedTabPosition(0)
            replaceFragment(geneticOverview)
        }
        binding.btnVitaminDeficiencies.setOnClickListener{
            var geneticOverview=GeneticOverviewResultFragment()
            geneticOverview.selectedTabPosition(1)
            replaceFragment(geneticOverview)
        }
        binding.btnPhysicalHealth.setOnClickListener{
            var geneticOverview=GeneticOverviewResultFragment()
            geneticOverview.selectedTabPosition(2)
            replaceFragment(geneticOverview)
        }
        binding.btnPhysicalHealthGifts.setOnClickListener{
            var geneticOverview=GeneticOverviewResultFragment()
            geneticOverview.selectedTabPosition(3)
            replaceFragment(geneticOverview)
        }
        binding.btnHealthWarning.setOnClickListener{
            var geneticOverview=GeneticOverviewResultFragment()
            geneticOverview.selectedTabPosition(4)
            replaceFragment(geneticOverview)
        }
        binding.btnSleepIssues.setOnClickListener{
            var geneticOverview=GeneticOverviewResultFragment()
            geneticOverview.selectedTabPosition(5)
            replaceFragment(geneticOverview)
        }
        return view
    }

    @SuppressLint("WrongConstant")
    private fun replaceFragment(fragment: Fragment) {
        val transaction = activity!!.supportFragmentManager.beginTransaction()
        transaction.replace(R.id.flContent, fragment)
        transaction.addToBackStack("share")
        transaction.commit()

    }




}
