package com.muhdo.app.fragment.v3.account.dialog_fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.muhdo.app.R
import com.muhdo.app.adapter.v3.AdditionalKitIdRecyAdpter
import com.muhdo.app.apiModel.v3.KitID
import com.muhdo.app.databinding.V3FragmentDialogAdditionalKitIdBinding
import com.muhdo.app.databinding.V3FragmentDialogEditPasswordBinding

class EditPasswordDialogFragment : DialogFragment() {
    companion object newInstance {
        fun newInstance(): EditPasswordDialogFragment {
            val f =
                EditPasswordDialogFragment()
            return f
        }
    }

    private lateinit var binding:  V3FragmentDialogEditPasswordBinding
    lateinit var kitIdList: MutableList<KitID>
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view =
            inflater.inflate(R.layout.v3_fragment_dialog_edit_password, container, false)
        binding = V3FragmentDialogEditPasswordBinding.bind(view)
        binding.imgClose.setOnClickListener {
            dismiss()
        }
        binding.btnChangePassword.setOnClickListener{
            
        }


        return view

    }

    override fun onStart() {
        super.onStart()
        dialog?.let {
            val width = ViewGroup.LayoutParams.MATCH_PARENT
            val height = ViewGroup.LayoutParams.MATCH_PARENT
            it.window?.setLayout(width, height)
        }
    }




}