package com.muhdo.app.fragment.v3

import android.annotation.SuppressLint
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.util.Base64
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.muhdo.app.R
import com.muhdo.app.apiModel.v3.questionnairestatus.Data
import com.muhdo.app.databinding.V3LayoutQuestionnairHomeBinding
import com.muhdo.app.repository.ApiUtilis
import com.muhdo.app.ui.DashboardActivity
import com.muhdo.app.utils.PreferenceConnector
import com.muhdo.app.utils.v3.Constants
import com.muhdo.app.utils.v3.TempUtil
import com.timingsystemkotlin.backuptimingsystem.Utility
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class QuestionnaireHomeFragment : Fragment() {
    var isQuestionnaireCompleted: Boolean = false

    internal lateinit var binding: V3LayoutQuestionnairHomeBinding
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.v3_layout_questionnair_home, container, false)

        binding = V3LayoutQuestionnairHomeBinding.bind(view)
        fetchQuestionnaireStatus()
        binding.btnDiet.setOnClickListener {
            replaceFragment(QuestionnaireDietFragment())
        }
        binding.btnGeneralHealth.setOnClickListener {
            replaceFragment(QuestionnaireGeneralHealthFragment())
        }
        binding.btnExercise.setOnClickListener {
            replaceFragment(QuestionnaireExerciseFragment())
        }
        binding.btnLifeStyle.setOnClickListener {
            replaceFragment(QuestionnaireLifestyleFragment())
        }
        binding.btnViewDnaResults.setOnClickListener {
            var title: String? = PreferenceConnector.readString(
                activity!!.applicationContext,
                PreferenceConnector.MODE_TEXT,
                "Fitness"
            )
            var id: String? = PreferenceConnector.readString(
                activity!!.applicationContext,
                PreferenceConnector.MODE_ID,
                "5c8c96a79c54381add6884da"
            )
//            var resultsFragment: ResultsFragment = ResultsFragment()
//            resultsFragment.setData(id!!, title!!)
//            replaceFragment(resultsFragment)

            val i = Intent(activity, DashboardActivity::class.java)
            i.putExtra(
                Constants.DASHBOARD_ACTION,
                Constants.DASHBOARD_RESULT
            )
            startActivity(i)
        }
        binding.btnGenerateActionPlan.setOnClickListener {
            if (isQuestionnaireCompleted) {
                replaceFragment(GeneticActionPlanHomeFragment())

            } else {
                val dialog: androidx.appcompat.app.AlertDialog.Builder =
                    androidx.appcompat.app.AlertDialog.Builder(context!!)
                dialog.setMessage(R.string.fill_questionnaire_warning_while_generate_action_plan)

                dialog.setNegativeButton(
                    R.string.btn_ok,
                    DialogInterface.OnClickListener { dialog, id ->
                        dialog.cancel()
                    })
                dialog.show()

            }
        }
        return view
    }

    @SuppressLint("CheckResult")
    private fun fetchQuestionnaireStatus() {
        binding.progressBar.visibility = View.VISIBLE
        val userId = Base64.decode(Utility.getUserID(activity!!.applicationContext), Base64.DEFAULT)
        val decodeUserId = String(userId, charset("UTF-8"))
        ApiUtilis.getAPIInstance(activity!!).getQuestionnaireStatus(userID = decodeUserId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ success ->
                run {
                    binding.progressBar.visibility = View.GONE

                    for (i in 0 until success.data!!.size) {
                        getStatusValue(success.data?.get(i))
                    }

                    if (success.data[0].status &&
                        success.data[1].status &&
                        success.data[2].status &&
                        success.data[3].status
                    ) {
                        isQuestionnaireCompleted = true
                    }
                }
            }, { error ->
                run {
                    binding.progressBar.visibility = View.GONE
                }
            })
    }

    private fun getStatusValue(textValue: Data) {
        when (textValue.category) {

            Constants.QUESTIONNAIRE.GENERAL_HEALTH -> {
                if (textValue.status) binding.overlayUpdateGeneralHealth.visibility = View.VISIBLE
            }

            Constants.QUESTIONNAIRE.DIET -> {
                if (textValue.status) binding.overlayUpdateDiet.visibility = View.VISIBLE
            }
            Constants.QUESTIONNAIRE.LIFESTYLE -> {
                if (textValue.status) binding.overlayUpdateLifestyle.visibility = View.VISIBLE
            }
            Constants.QUESTIONNAIRE.EXERCISE -> {
                if (textValue.status) binding.overlayUpdateExercise.visibility = View.VISIBLE
            }

        }

    }

    private fun initCompletionIndicators() {

        if (TempUtil.isExerciseQuestionnaireCompleted) {
            binding.txtLabelExercise.setCompoundDrawablesWithIntrinsicBounds(
                0,
                0,
                R.drawable.v3_ic_done,
                0
            )
        }
        if (TempUtil.isDietQuestionnaireCompleted) {
            binding.txtLabelDiet.setCompoundDrawablesWithIntrinsicBounds(
                0,
                0,
                R.drawable.v3_ic_done,
                0
            )
        }
        if (TempUtil.isGeneralHealthQuestionnaireCompleted) {
            binding.txtLabelGeneralHealth.setCompoundDrawablesWithIntrinsicBounds(
                0,
                0,
                R.drawable.v3_ic_done,
                0
            )
        }
        if (TempUtil.isLifestyleQuestionnaireCompleted) {
            binding.txtLabelLifestyle.setCompoundDrawablesWithIntrinsicBounds(
                0,
                0,
                R.drawable.v3_ic_done,
                0
            )
        }
    }

    @SuppressLint("WrongConstant")
    private fun replaceFragment(fragment: Fragment) {
        val transaction = activity!!.supportFragmentManager.beginTransaction()
        transaction.replace(R.id.flContent, fragment)
        transaction.addToBackStack("share")
        transaction.commit()
    }

}
