package com.muhdo.app.fragment

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.View.VISIBLE
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.muhdo.app.R
import com.muhdo.app.adapter.*
import com.muhdo.app.apiModel.mealModel.UpdateFavouriteRequest
import com.muhdo.app.apiModel.mealModel.daysModel.Monday
import com.muhdo.app.apiModel.mealModel.daysModel.UpdateFavoriteModel
import com.muhdo.app.databinding.FragmentMealBinding
import com.muhdo.app.repository.ApiUtilis
import com.muhdo.app.repository.ErrorModel
import com.muhdo.app.repository.NetworkManager
import com.muhdo.app.repository.ServiceListener
import com.muhdo.app.utils.v3.Constants
import com.timingsystemkotlin.backuptimingsystem.Utility
import kotlinx.android.synthetic.main.list_breakfast.view.*
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject


class MealFragment : Fragment() {

    private lateinit var meal1Adapter: Meal1Adapter
    private lateinit var meal2Adapter: Meal2Adapter
    private lateinit var meal3Adapter: Meal3Adapter
    private lateinit var meal4Adapter: Meal4Adapter
    private lateinit var meal5Adapter: Meal5Adapter
    private lateinit var meal6Adapter: Meal6Adapter
    lateinit var data: List<List<Monday>>
    private var weekList = arrayListOf<String>()
    private val mealList = arrayListOf<String>()
    private lateinit var weekDay: String
    private var totalCount = 0
    private lateinit var binding: FragmentMealBinding
    private lateinit var dataArray: JSONArray

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_meal, container, false)
        binding = FragmentMealBinding.bind(view)
        weekDay = this.arguments!!.getString(Constants.ACTIVITY_FRAGMENT_WEEK)!!
//        weekList = this.arguments!!.getStringArrayList(Constants.ACTIVITY_FRAGMENT_WEEK_LIST)!!
        val data1 = this.arguments!!.getString(Constants.ACTIVITY_FRAGMENT_MESSAGE)
        if (data1 != "") {
            dataArray = JSONArray(data1)
            totalCount = dataArray.length() * 7
            System.out.println("myValue==>  $ ")
            setData(dataArray)
        }


        return view
    }


    @SuppressLint("SetTextI18n")
    fun setData(data: JSONArray) {
        when (data.length()) {
            1 -> {
                binding.layoutMeal1.visibility = VISIBLE
                setMeal1Data(data.get(0) as JSONArray)
                binding.txtMeal1.text = "Meal 1"
//                var a = data.get(0) as JSONArray
//                if(a.length() > 0) {
//                    val json = JSONObject(a.get(0).toString())
//                    binding.txtMeal1.text = json.getString("type")
//                }
            }
            2 -> {
                binding.layoutMeal1.visibility = VISIBLE
                binding.layoutMeal2.visibility = VISIBLE
                setMeal1Data(data.get(0) as JSONArray)
                setMeal2Data(data.get(1) as JSONArray)

                binding.txtMeal1.text = "Meal 1"
                binding.txtMeal2.text = "Meal 2"
//                var a = data.get(0) as JSONArray
//                if(a.length() > 0) {
//                    val json = JSONObject(a.get(0).toString())
//                    binding.txtMeal1.text = json.getString("type")
//                }
//
//                var b = data.get(1) as JSONArray
//                if(b.length() > 0) {
//                    val json1 = JSONObject(b.get(0).toString())
//                    binding.txtMeal2.text = json1.getString("type")
//                }
            }
            3 -> {
                binding.layoutMeal1.visibility = VISIBLE
                binding.layoutMeal2.visibility = VISIBLE
                binding.layoutMeal3.visibility = VISIBLE
                setMeal1Data(data.get(0) as JSONArray)
                setMeal2Data(data.get(1) as JSONArray)
                setMeal3Data(data.get(2) as JSONArray)


                binding.txtMeal1.text = "Meal 1"
                binding.txtMeal2.text = "Meal 2"
                binding.txtMeal3.text = "Meal 3"

//                var a = data.get(0) as JSONArray
//                if(a.length() > 0) {
//                    val json = JSONObject(a.get(0).toString())
//                    binding.txtMeal1.text = json.getString("type")
//                }
//
//                var b = data.get(1) as JSONArray
//                if(b.length() > 0) {
//                    val json1 = JSONObject(b.get(0).toString())
//                    binding.txtMeal2.text = json1.getString("type")
//                }
//
//                var c = data.get(2) as JSONArray
//                if(c.length() > 0) {
//                    val json2 = JSONObject(c.get(0).toString())
//                    binding.txtMeal3.text = json2.getString("type")
//                }

            }
            4 -> {
                binding.layoutMeal1.visibility = VISIBLE
                binding.layoutMeal2.visibility = VISIBLE
                binding.layoutMeal3.visibility = VISIBLE
                binding.layoutMeal4.visibility = VISIBLE
                setMeal1Data(data.get(0) as JSONArray)
                setMeal2Data(data.get(1) as JSONArray)
                setMeal3Data(data.get(2) as JSONArray)
                setMeal4Data(data.get(3) as JSONArray)


                binding.txtMeal1.text = "Meal 1"
                binding.txtMeal2.text = "Meal 2"
                binding.txtMeal3.text = "Meal 3"
                binding.txtMeal4.text = "Meal 4"


//                var a = data.get(0) as JSONArray
//                if(a.length() > 0) {
//                    val json = JSONObject(a.get(0).toString())
//                    binding.txtMeal1.text = json.getString("type")
//                }
//
//                var b = data.get(1) as JSONArray
//                if(b.length() > 0) {
//                    val json1 = JSONObject(b.get(0).toString())
//                    binding.txtMeal2.text = json1.getString("type")
//                }
//
//                var c = data.get(2) as JSONArray
//                if(c.length() > 0) {
//                    val json2 = JSONObject(c.get(0).toString())
//                    binding.txtMeal3.text = json2.getString("type")
//                }
//
//                var d = data.get(3) as JSONArray
//                if(d.length() > 0) {
//                    val json3 = JSONObject(d.get(0).toString())
//                    binding.txtMeal4.text = json3.getString("type")
//                }
            }
            5 -> {
                binding.layoutMeal1.visibility = VISIBLE
                binding.layoutMeal2.visibility = VISIBLE
                binding.layoutMeal3.visibility = VISIBLE
                binding.layoutMeal4.visibility = VISIBLE
                binding.layoutMeal5.visibility = VISIBLE
                setMeal1Data(data.get(0) as JSONArray)
                setMeal2Data(data.get(1) as JSONArray)
                setMeal3Data(data.get(2) as JSONArray)
                setMeal4Data(data.get(3) as JSONArray)
                setMeal5Data(data.get(4) as JSONArray)

//                var a = data.get(0) as JSONArray
//                if(a.length() > 0) {
//                    val json = JSONObject(a.get(0).toString())
//                    binding.txtMeal1.text = json.getString("type")
//                }
//
//                var b = data.get(1) as JSONArray
//                if(b.length() > 0) {
//                    val json1 = JSONObject(b.get(0).toString())
//                    binding.txtMeal2.text = json1.getString("type")
//                }
//
//                var c = data.get(2) as JSONArray
//                if(c.length() > 0) {
//                    val json2 = JSONObject(c.get(0).toString())
//                    binding.txtMeal3.text = json2.getString("type")
//                }
//
//                var d = data.get(3) as JSONArray
//                if(d.length() > 0) {
//                    val json3 = JSONObject(d.get(0).toString())
//                    binding.txtMeal4.text = json3.getString("type")
//                }
//
//                var e = data.get(4) as JSONArray
//                if(e.length() > 0) {
//                    val json4 = JSONObject(e.get(0).toString())
//                    binding.txtMeal5.text = json4.getString("type")
//                }

                binding.txtMeal1.text = "Meal 1"
                binding.txtMeal2.text = "Meal 2"
                binding.txtMeal3.text = "Meal 3"
                binding.txtMeal4.text = "Meal 4"
                binding.txtMeal5.text = "Meal 5"

            }
            6 -> {
                binding.layoutMeal1.visibility = VISIBLE
                binding.layoutMeal2.visibility = VISIBLE
                binding.layoutMeal3.visibility = VISIBLE
                binding.layoutMeal4.visibility = VISIBLE
                binding.layoutMeal5.visibility = VISIBLE
                binding.layoutMeal6.visibility = VISIBLE
                setMeal1Data(data.get(0) as JSONArray)
                setMeal2Data(data.get(1) as JSONArray)
                setMeal3Data(data.get(2) as JSONArray)
                setMeal4Data(data.get(3) as JSONArray)
                setMeal5Data(data.get(4) as JSONArray)
                setMeal6Data(data.get(5) as JSONArray)


                binding.txtMeal1.text = "Meal 1"
                binding.txtMeal2.text = "Meal 2"
                binding.txtMeal3.text = "Meal 3"
                binding.txtMeal4.text = "Meal 4"
                binding.txtMeal5.text = "Meal 5"
                binding.txtMeal6.text = "Meal 6"

//                var a = data.get(0) as JSONArray
//                if(a.length() > 0) {
//                    val json = JSONObject(a.get(0).toString())
//                    binding.txtMeal1.text = json.getString("type")
//                }
//
//                var b = data.get(1) as JSONArray
//                if(b.length() > 0) {
//                    val json1 = JSONObject(b.get(0).toString())
//                    binding.txtMeal2.text = json1.getString("type")
//                }
//
//                var c = data.get(2) as JSONArray
//                if(c.length() > 0) {
//                    val json2 = JSONObject(c.get(0).toString())
//                    binding.txtMeal3.text = json2.getString("type")
//                }
//
//                var d = data.get(3) as JSONArray
//                if(d.length() > 0) {
//                    val json3 = JSONObject(d.get(0).toString())
//                    binding.txtMeal4.text = json3.getString("type")
//                }
//
//                var e = data.get(4) as JSONArray
//                if(e.length() > 0) {
//                    val json4 = JSONObject(e.get(0).toString())
//                    binding.txtMeal5.text = json4.getString("type")
//                }
//
//                var f = data.get(5) as JSONArray
//                if(f.length() > 0) {
//                    val json5 = JSONObject(f.get(0).toString())
//                    binding.txtMeal6.text = json5.getString("type")
//                }
            }
        }

    }

    private fun setMeal1Data(meal_1Data: JSONArray) {
        if (meal_1Data.length() > 0) {

            meal1Adapter = Meal1Adapter(
                activity!!.applicationContext,
                meal_1Data,
                this@MealFragment,
                -1,
                weekDay
            )
            binding.meal1List.layoutManager =
                LinearLayoutManager(
                    activity!!.applicationContext,
                    LinearLayoutManager.HORIZONTAL,
                    false
                )
            binding.meal1List.isNestedScrollingEnabled = true
            binding.meal1List.itemAnimator = DefaultItemAnimator()
            binding.meal1List.adapter = meal1Adapter
        }
    }

    private fun setMeal2Data(meal_2Data: JSONArray) {
        if (meal_2Data.length() > 0) {
            meal2Adapter = Meal2Adapter(
                activity!!.applicationContext,
                meal_2Data,
                this@MealFragment,
                -1,
                weekDay
            )
            binding.meal2List.layoutManager =
                LinearLayoutManager(
                    activity!!.applicationContext,
                    LinearLayoutManager.HORIZONTAL,
                    false
                )
            binding.meal2List.isNestedScrollingEnabled = true
            binding.meal2List.itemAnimator = DefaultItemAnimator()
            binding.meal2List.adapter = meal2Adapter
        }
    }

    private fun setMeal3Data(meal_3Data: JSONArray) {
        if (meal_3Data.length() > 0) {
            meal3Adapter = Meal3Adapter(
                activity!!.applicationContext,
                meal_3Data,
                this@MealFragment,
                -1,
                weekDay
            )
            binding.meal3List.layoutManager =
                LinearLayoutManager(
                    activity!!.applicationContext,
                    LinearLayoutManager.HORIZONTAL,
                    false
                )
            binding.meal3List.isNestedScrollingEnabled = true
            binding.meal3List.itemAnimator = DefaultItemAnimator()
            binding.meal3List.adapter = meal3Adapter
        }
    }

    private fun setMeal4Data(meal_4Data: JSONArray) {
        if (meal_4Data.length() > 0) {
            meal4Adapter = Meal4Adapter(
                activity!!.applicationContext,
                meal_4Data,
                this@MealFragment,
                -1,
                weekDay
            )
            binding.meal4List.layoutManager =
                LinearLayoutManager(
                    activity!!.applicationContext,
                    LinearLayoutManager.HORIZONTAL,
                    false
                )
            binding.meal4List.isNestedScrollingEnabled = true
            binding.meal4List.itemAnimator = DefaultItemAnimator()
            binding.meal4List.adapter = meal4Adapter
        }
    }

    private fun setMeal5Data(meal_5Data: JSONArray) {
        if (meal_5Data.length() > 0) {
            meal5Adapter = Meal5Adapter(
                activity!!.applicationContext,
                meal_5Data,
                this@MealFragment,
                -1,
                weekDay
            )
            binding.meal5List.layoutManager =
                LinearLayoutManager(
                    activity!!.applicationContext,
                    LinearLayoutManager.HORIZONTAL,
                    false
                )
            binding.meal5List.isNestedScrollingEnabled = true
            binding.meal5List.itemAnimator = DefaultItemAnimator()
            binding.meal5List.adapter = meal5Adapter
        }
    }

    private fun setMeal6Data(meal_6Data: JSONArray) {
        if (meal_6Data.length() > 0) {
            meal6Adapter = Meal6Adapter(
                activity!!.applicationContext,
                meal_6Data,
                this@MealFragment,
                -1,
                weekDay
            )
            binding.meal6List.layoutManager =
                LinearLayoutManager(
                    activity!!.applicationContext,
                    LinearLayoutManager.HORIZONTAL,
                    false
                )
            binding.meal6List.isNestedScrollingEnabled = true
            binding.meal6List.itemAnimator = DefaultItemAnimator()
            binding.meal6List.adapter = meal6Adapter
        }
    }


    fun getRecipeData1(id: String, position: Int, day: String, meal: String) {
        meal1Adapter.activityPosition = position
        binding.meal1List.adapter = meal1Adapter
        binding.meal1List.smoothScrollToPosition(position)
        if (!weekList.contains(day) && !mealList.contains(meal)) {
            weekList.add(day)
            mealList.add(meal)

            val mealObject = JSONObject()
            try {
                mealObject.put("recipe_id", id)
                mealObject.put("day", day)
                mealObject.put("meal", meal)

            } catch (e: JSONException) {
                // TODO Auto-generated catch block
                e.printStackTrace()
            }
            Utility.jsonList.put(mealObject)
            Utility.idList.add(id)
        } else {
            for (i in 0 until Utility.jsonList.length()) {
                val jsonObject = Utility.jsonList.getJSONObject(i)
                if (day == jsonObject.getString("day") && meal == jsonObject.getString("meal")) {
                    Utility.jsonList.remove(i)
                    Utility.idList.remove(jsonObject.getString("recipe_id"))
                    break
                }
            }
            val mealObject = JSONObject()
            try {
                mealObject.put("recipe_id", id)
                mealObject.put("day", day)
                mealObject.put("meal", meal)


            } catch (e: JSONException) {
                // TODO Auto-generated catch block
                e.printStackTrace()
            }
            Utility.jsonList.put(mealObject)
            Utility.idList.add(id)
        }
    }

    fun getRecipeData2(id: String, position: Int, day: String, meal: String) {

        meal2Adapter.activityPosition = position
        binding.meal2List.adapter = meal2Adapter
        binding.meal2List.smoothScrollToPosition(position)
        if (!weekList.contains(day) && !mealList.contains(meal)) {
            weekList.add(day)
            mealList.add(meal)
            val mealObject = JSONObject()
            try {
                mealObject.put("recipe_id", id)
                mealObject.put("day", day)
                mealObject.put("meal", meal)


            } catch (e: JSONException) {
                // TODO Auto-generated catch block
                e.printStackTrace()
            }
            Utility.jsonList.put(mealObject)
            Utility.idList.add(id)
            System.out.println("FirstTime Added====> 1")
        } else {
            for (i in 0 until Utility.jsonList.length()) {
                val jsonObject = Utility.jsonList.getJSONObject(i)
                if (day == jsonObject.getString("day") && meal == jsonObject.getString("meal")) {
                    Utility.jsonList.remove(i)
                    Utility.idList.remove(jsonObject.getString("recipe_id"))
                    System.out.println("Removed ====> 1")
                    break
                }
            }
            val mealObject = JSONObject()
            try {
                mealObject.put("recipe_id", id)
                mealObject.put("day", day)
                mealObject.put("meal", meal)


            } catch (e: JSONException) {
                // TODO Auto-generated catch block
                e.printStackTrace()
            }
            Utility.jsonList.put(mealObject)
            Utility.idList.add(id)
            System.out.println("Added====> 1")
        }
    }

    fun getRecipeData3(id: String, position: Int, day: String, meal: String) {
        meal3Adapter.activityPosition = position
        binding.meal3List.adapter = meal3Adapter
        binding.meal3List.smoothScrollToPosition(position)
        if (!weekList.contains(day) && !mealList.contains(meal)) {
            weekList.add(day)
            mealList.add(meal)
            val mealObject = JSONObject()
            Utility.idList.add(id)
            try {
                mealObject.put("recipe_id", id)
                mealObject.put("day", day)
                mealObject.put("meal", meal)

            } catch (e: JSONException) {
                // TODO Auto-generated catch block
                e.printStackTrace()
            }
            Utility.jsonList.put(mealObject)
            System.out.println("FirstTime Added====> 2")
        } else {
            for (i in 0 until Utility.jsonList.length()) {
                val jsonObject = Utility.jsonList.getJSONObject(i)
                if (day == jsonObject.getString("day") && meal == jsonObject.getString("meal")) {
                    Utility.jsonList.remove(i)
                    Utility.idList.remove(id)
                    System.out.println("Removed ====> 2")
                    break
                }
            }
            val mealObject = JSONObject()
            try {
                mealObject.put("recipe_id", id)
                mealObject.put("day", day)
                mealObject.put("meal", meal)

            } catch (e: JSONException) {
                // TODO Auto-generated catch block
                e.printStackTrace()
            }
            Utility.jsonList.put(mealObject)
            Utility.idList.add(id)
            System.out.println("Added====> 2")
        }
    }

    fun getRecipeData4(id: String, position: Int, day: String, meal: String) {

        meal4Adapter.activityPosition = position
        binding.meal4List.adapter = meal4Adapter
        binding.meal4List.smoothScrollToPosition(position)
        if (!weekList.contains(day) && !mealList.contains(meal)) {
            weekList.add(day)
            mealList.add(meal)
            val mealObject = JSONObject()
            Utility.idList.add(id)
            try {
                mealObject.put("recipe_id", id)
                mealObject.put("day", day)
                mealObject.put("meal", meal)

            } catch (e: JSONException) {
                // TODO Auto-generated catch block
                e.printStackTrace()
            }
            Utility.jsonList.put(mealObject)
        } else {
            for (i in 0 until Utility.jsonList.length()) {
                val jsonObject = Utility.jsonList.getJSONObject(i)
                if (day == jsonObject.getString("day") && meal == jsonObject.getString("meal")) {
                    Utility.jsonList.remove(i)
                    Utility.idList.remove(id)
                    break
                }
            }
            val mealObject = JSONObject()
            try {
                mealObject.put("recipe_id", id)
                mealObject.put("day", day)
                mealObject.put("meal", meal)

            } catch (e: JSONException) {
                // TODO Auto-generated catch block
                e.printStackTrace()
            }
            Utility.jsonList.put(mealObject)
            Utility.idList.add(id)
        }
    }

    fun getRecipeData5(id: String, position: Int, day: String, meal: String) {

        meal5Adapter.activityPosition = position
        binding.meal5List.adapter = meal5Adapter
        binding.meal5List.smoothScrollToPosition(position)
        if (!weekList.contains(day) && !mealList.contains(meal)) {
            weekList.add(day)
            mealList.add(meal)
            val mealObject = JSONObject()
            Utility.idList.add(id)
            try {
                mealObject.put("recipe_id", id)
                mealObject.put("day", day)
                mealObject.put("meal", meal)

            } catch (e: JSONException) {
                // TODO Auto-generated catch block
                e.printStackTrace()
            }
            Utility.jsonList.put(mealObject)
        } else {
            for (i in 0 until Utility.jsonList.length()) {
                val jsonObject = Utility.jsonList.getJSONObject(i)
                if (day == jsonObject.getString("day") && meal == jsonObject.getString("meal")) {
                    Utility.jsonList.remove(i)
                    Utility.idList.remove(id)
                    break
                }
            }
            val mealObject = JSONObject()
            try {
                mealObject.put("recipe_id", id)
                mealObject.put("day", day)
                mealObject.put("meal", meal)

            } catch (e: JSONException) {
                // TODO Auto-generated catch block
                e.printStackTrace()
            }
            Utility.jsonList.put(mealObject)
            Utility.idList.add(id)
        }
    }

    fun getRecipeData6(id: String, position: Int, day: String, meal: String) {

        meal6Adapter.activityPosition = position
        binding.meal6List.adapter = meal6Adapter
        binding.meal6List.smoothScrollToPosition(position)
        if (!weekList.contains(day) && !mealList.contains(meal)) {
            weekList.add(day)
            mealList.add(meal)
            val mealObject = JSONObject()
            Utility.idList.add(id)
            try {
                mealObject.put("recipe_id", id)
                mealObject.put("day", day)
                mealObject.put("meal", meal)

            } catch (e: JSONException) {
                // TODO Auto-generated catch block
                e.printStackTrace()
            }
            Utility.jsonList.put(mealObject)
        } else {
            for (i in 0 until Utility.jsonList.length()) {
                val jsonObject = Utility.jsonList.getJSONObject(i)
                if (weekList.contains(jsonObject.getString("day")) && mealList.contains(meal)) {
                    Utility.jsonList.remove(i)
                    Utility.idList.remove(id)
                    break
                }
            }
            val mealObject = JSONObject()
            try {
                mealObject.put("recipe_id", id)
                mealObject.put("day", day)
                mealObject.put("meal", meal)

            } catch (e: JSONException) {
                // TODO Auto-generated catch block
                e.printStackTrace()
            }
            Utility.jsonList.put(mealObject)
            Utility.idList.add(id)
        }
    }

    fun updateFavorite(id: String, view: View, status: Boolean) {
        val manager = NetworkManager()

        val requestParams =
            UpdateFavouriteRequest(status, id, Utility.getUserID(activity!!.applicationContext))

        if (manager.isConnectingToInternet(activity!!.applicationContext)) {
            manager.createApiRequest(
                ApiUtilis.getAPIInstance(activity!!).updateFavouriteRecipe(requestParams),
                object : ServiceListener<UpdateFavoriteModel> {
                    override fun getServerResponse(
                        response: UpdateFavoriteModel,
                        requestcode: Int
                    ) {
                        view.isClickable = true
                        if (status) {
                            Utility.favouriteIdList.add(id)
                            if (Utility.unFavouriteIdList.contains(id)) {
                                Utility.unFavouriteIdList.remove(id)
                            }
                        } else if (!status) {
                            Utility.unFavouriteIdList.add(id)
                            if (Utility.favouriteIdList.contains(id)) {
                                Utility.favouriteIdList.remove(id)
                            }
                        }
                    }

                    override fun getError(error: ErrorModel, requestcode: Int) {
                        if (status) {
                            view.check_favourite.isChecked = false
                        } else if (!status) {
                            view.check_favourite.isChecked = true
                        }
                        Utility.displayShortSnackBar(
                            binding.parentLayout,
                            error.getMessage().toString()
                        )

                    }
                })
        } else {

            Utility.displayShortSnackBar(
                binding.parentLayout,
                activity!!.resources.getString(R.string.check_internet)
            )
        }
    }

}