package com.muhdo.app.fragment.v3.health_insights

import android.os.Bundle
import android.util.Base64
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.JsonObject
import com.muhdo.app.R
import com.muhdo.app.adapter.v3.HealthInsightsOverviewRecyAdpter
import com.muhdo.app.apiModel.healthinsights.ResponseHealthInisghts
import com.muhdo.app.databinding.V3FragmentHealthInsightDetailsResultBinding
import com.muhdo.app.fragment.HomeFragment
import com.muhdo.app.fragment.v3.GeneticOverviewHomeFragment
import com.muhdo.app.interfaces.v3.OnChangeInnerFragment
import com.muhdo.app.interfaces.v3.OnHealthInsightsHeaderValuesChanged
import com.muhdo.app.repository.ApiUtilis
import com.muhdo.app.utils.v3.TempUtil
import com.timingsystemkotlin.backuptimingsystem.Utility
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class HealthInsightsDetailsResultsFragment : Fragment() {
    lateinit var onChangeInnerFragment: OnChangeInnerFragment
    var tabPosition: Int = -1
    internal lateinit var binding: V3FragmentHealthInsightDetailsResultBinding
    internal lateinit var overviewRecyAdapter: HealthInsightsOverviewRecyAdpter
    private lateinit var completeResponse: ResponseHealthInisghts
    public var onHealthInsightsHeaderValuesChanged: OnHealthInsightsHeaderValuesChanged? = null
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view =
            inflater.inflate(R.layout.v3_fragment_health_insight_details_result, container, false)
        binding = V3FragmentHealthInsightDetailsResultBinding.bind(view)
        Utility.sectionList.clear()
        Utility.modeList.clear()
        val value: String = arguments?.get("apiRoute").toString()
        val title = arguments?.getInt("title")
        getDataForFields(value)
        binding.tvTitleOverview.setText(getString(title!!) + " " + getString(R.string.overview))
        TempUtil.epiGenDialogAlreadyChangedOnce = false
        return view
    }

    private fun initData(completeResponse: ResponseHealthInisghts) {
        if (completeResponse != null) {
            if (onHealthInsightsHeaderValuesChanged != null) {
                if (completeResponse.data.size > 0)
                    onHealthInsightsHeaderValuesChanged!!.headerValuesChanged(
                        completeResponse.data[0].title,
                        completeResponse.data[0].status,
                        completeResponse.data[0].output!!.indicator,
                        tabPosition
                    )
            }
            overviewRecyAdapter = HealthInsightsOverviewRecyAdpter(
                activity!!,
                completeResponse.data,
                onHealthInsightsHeaderValuesChanged,
                tabPosition,
                childFragmentManager,onChangeInnerFragment
            )

            binding.recyclerView.adapter = overviewRecyAdapter
            binding.recyclerView.layoutManager =
                LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        }
    }

    private fun getDataForFields(key: String) {
        binding.progressBar.visibility = View.VISIBLE
        val userId = Base64.decode(
            Utility.getUserID(activity!!.applicationContext),
            Base64.DEFAULT
        )
        val decodeUserId = String(userId, charset("UTF-8"))
        val requestBody = JsonObject()
        requestBody.addProperty("user_id", decodeUserId)
        requestBody.addProperty("type", key)
        ApiUtilis.getAPIInstance(activity!!).getDataForHealthInsights(requestBody)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ success ->
                run {
                    try {
                        binding.progressBar.visibility = View.GONE
                        completeResponse = success
                        initData(completeResponse)
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }

                }
            }, { error ->
                run {
                    try {
                        binding.progressBar.visibility = View.GONE
                        Utility.displayShortSnackBar(binding.parentLayout, error.localizedMessage)
                        TempUtil.log("Error", error.localizedMessage)
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            })
    }

    private fun replaceFragment(fragment: Fragment) {
        val transaction = getChildFragmentManager().beginTransaction()
        transaction.replace(R.id.linearLayoutContainer, fragment)
        transaction.addToBackStack("share")
        transaction.commit()
    }
}