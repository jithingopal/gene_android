package com.muhdo.app.fragment

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.muhdo.app.R
import com.muhdo.app.databinding.FragmentPasswordBinding

class PasswordFragment : Fragment() {


    internal lateinit var binding: FragmentPasswordBinding
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_password, container, false)
        binding = FragmentPasswordBinding.bind(view)


        binding.btnSend.setOnClickListener {
            if(binding.edtMsg.text.isEmpty()){
                binding.edtMsg.error = "Please enter message!!"
                binding.edtMsg.requestFocus()
            }
            else{
                val intent = Intent(Intent.ACTION_SEND)
                val recipients = arrayOf("info@muhdo.com")
                intent.putExtra(Intent.EXTRA_EMAIL, recipients)
                intent.putExtra(Intent.EXTRA_SUBJECT, "")
                intent.putExtra(Intent.EXTRA_TEXT, binding.edtMsg.text)
                intent.type = "text/html"
                startActivity(Intent.createChooser(intent, "Send mail"))
            }
        }
        return view
    }


}