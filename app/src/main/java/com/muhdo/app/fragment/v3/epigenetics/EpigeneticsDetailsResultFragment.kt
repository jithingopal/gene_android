package com.muhdo.app.fragment.v3.epigenetics

import android.annotation.SuppressLint
import android.app.Dialog
import android.os.Bundle
import android.text.Html
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.muhdo.app.R
import com.muhdo.app.adapter.v3.EpigeneticOverviewRecyAdpter
import com.muhdo.app.apiModel.v3.EpigenBaseReq
import com.muhdo.app.databinding.V3FragmentEpigeneticDetailsResultBinding
import com.muhdo.app.repository.ApiUtilis
import com.muhdo.app.repository.ErrorModel
import com.muhdo.app.repository.NetworkManager
import com.muhdo.app.repository.ServiceListener
import com.muhdo.app.ui.epigenticResult.model.EpigResultResponse
import com.muhdo.app.ui.epigenticResult.model.EpigenticHealth
import com.muhdo.app.utils.v3.TempUtil
import com.muhdo.app.utils.v3.showAlertDialog
import com.timingsystemkotlin.backuptimingsystem.Utility


class EpigeneticsDetailsResultFragment : Fragment() {
    var epiGenOverviewAdapter: EpigeneticOverviewRecyAdpter? = null
    internal lateinit var binding: V3FragmentEpigeneticDetailsResultBinding
    var epigenticHealth: EpigenticHealth? = null
    lateinit var dialog: Dialog;
    var position: Int = 0
    var biologicalAge: Int = 0
    var tvDescription: TextView? = null
    var tvTitle: TextView? = null
    var showExpansionOutcome = false;
    var showExpansionRecommendation = false;
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view =
            inflater.inflate(R.layout.v3_fragment_epigenetic_details_result, container, false)
        binding = V3FragmentEpigeneticDetailsResultBinding.bind(view)
        setDataToViews()
        setupDialogInfo()
        setExpansionOutcomeLayout(showExpansionOutcome)
        showExpansionRecommendationLayout(showExpansionRecommendation)
        binding.tvViewMoreOutcome.setOnClickListener {
            showExpansionOutcome = !showExpansionOutcome
            setExpansionOutcomeLayout(showExpansionOutcome)
        }
        binding.tvViewMoreRecommendations.setOnClickListener {
            showExpansionRecommendation = !showExpansionRecommendation
            showExpansionRecommendationLayout(showExpansionRecommendation)
        }
        binding.tvViewLessRecommendations.setOnClickListener {
            showExpansionRecommendation = !showExpansionRecommendation
            showExpansionRecommendationLayout(showExpansionRecommendation)
        }
        return view
    }

    @SuppressLint("WrongConstant")
    private fun replaceFragment(fragment: Fragment) {
        val transaction = activity!!.supportFragmentManager.beginTransaction()
        transaction.replace(R.id.flContent, fragment)
        transaction.addToBackStack("share")
        transaction.commit()

    }

    private fun setupDialogInfo() {
        dialog = Dialog(activity)
        val viewDialog = View.inflate(activity, R.layout.v3_dialog_info_epigen_result, null)
        val closeButton = viewDialog.findViewById<ImageView>(R.id.img_close)
        closeButton.setOnClickListener { dialog.dismiss() }
        dialog.setContentView(viewDialog)
        tvTitle = viewDialog.findViewById<TextView>(R.id.tvTitle)
        tvDescription = viewDialog.findViewById<TextView>(R.id.tvDescription)
        dialog.setCancelable(false)
//        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;


    }

    private fun showDialog(title: String?, description: String?) {
        if (tvTitle != null) {
            tvDescription?.setText(Html.fromHtml(description.toString()))
            tvTitle?.setText(title)
            dialog.show()
        }
    }

    fun setEpigeneticHealth(epigenticHealth: EpigenticHealth) {
        this.epigenticHealth = epigenticHealth
    }

    fun setDataToViews() {
        var score: Float? =
            epigenticHealth?.data?.scoreData?.score?.toFloat()
        if (epigenticHealth != null && epigenticHealth?.data != null) {
            TempUtil.log("EpigenDetailsResult", "Inside setdata")

            binding.tvTitle.setText(epigenticHealth?.trackingTitle?.replace("Score", "Age"))
            binding.tvTitle.setText(epigenticHealth?.trackingTitle?.replace("_", " "))
            binding.tvTitle.setText(
                epigenticHealth?.trackingTitle?.replace("_", " ")
            )
            /*if (position != 0 && position != 4) {
                binding.tvTitle.setText(
                    epigenticHealth?.trackingTitle?.replace(
                        "_Score",
                        " Age"
                    )?.replace("_", " ")
                )
                score = score?.plus(biologicalAge.toFloat())

//                binding.tvChronologicalAge.setText(score.toString())
            } else {
                binding.tvTitle.setText(
                    epigenticHealth?.trackingTitle?.replace("_", " ")
                )
//            binding.tvChronologicalAge.setText(epigenticHealthData?.get(currentHighlatedPos)?.data?.scoreData?.score)
            }*/
            if (epigenticHealth?.data?.info != null) {
                binding.tvInfo.setText(Html.fromHtml(epigenticHealth?.data?.info.toString()))
            }
            if (epigenticHealth?.data?.introduction != null) {
                binding.tvIntroduction.setText(Html.fromHtml(epigenticHealth?.data?.introduction.toString()))
            }

            if (epigenticHealth?.data?.outcome != null) {
                binding.tvOutcomes.setText(Html.fromHtml(epigenticHealth?.data?.outcome.toString()))
            }

            if (epigenticHealth?.data?.recommendations != null) {
                binding.tvRecommendations.setText(Html.fromHtml(epigenticHealth?.data?.recommendations.toString()))
                setRecommendationList()
            }

            if (epigenticHealth?.data?.scoreData != null) {
                binding.tvScore.setText(score.toString())
                val sliderColor: String? = epigenticHealth?.data?.scoreData?.color
                if (!sliderColor!!.isEmpty()) {
                    if (sliderColor.equals("Yellow")) {
                        binding.imgIndicator.setImageResource(R.drawable.v3_ic_slider_yellow)
                        binding.tvStatus.setText(
                            context!!.resources.getString(R.string.status) + " " + context!!.resources.getString(
                                R.string.normal
                            )
                        )
                    } else if (sliderColor.equals("Red")) {
                        binding.imgIndicator.setImageResource(R.drawable.v3_ic_slider_red)
                        binding.tvStatus.setText(
                            context!!.resources.getString(R.string.status) + " " + context!!.resources.getString(
                                R.string.unhealthy
                            )
                        )
                    } else if (sliderColor.equals("Green")) {
                        binding.imgIndicator.setImageResource(R.drawable.v3_ic_slider_green)
                        binding.tvStatus.setText(
                            context!!.resources.getString(R.string.status) + " " + context!!.resources.getString(
                                R.string.v3_healthy
                            )
                        )
                    }
                }
            }
            when (position) {
                0 -> {
                    binding.imgIcon.setImageResource(R.drawable.v3_ic_biological_age)
                }
                1 -> {
                    binding.imgIcon.setImageResource((R.drawable.v3_ic_eye_health))

                }
                2 -> {
                    binding.imgIcon.setImageResource((R.drawable.v3_ic_hearing))

                }
                3 -> {
                    binding.imgIcon.setImageResource((R.drawable.v3_ic_mental_health))

                }
                4 -> {
                    binding.imgIcon.setImageResource((R.drawable.v3_ic_inflammation))

                }
            }

        } else {

            TempUtil.log("EpigenDetailsResult", "outside setdata")
        }
    }

    private fun setRecommendationList() {
        binding.layoutRecomendationContainer.removeAllViews()
        if (epigenticHealth != null && epigenticHealth?.data != null && epigenticHealth?.data?.recommendationInfoList != null) {
            val recommendationsInfoList = epigenticHealth?.data?.recommendationInfoList!!
            for (i in 0 until recommendationsInfoList.size) {
                val view: View = View.inflate(activity, R.layout.v3_item_recommendation, null)
                val imgIcon = view.findViewById<ImageView>(R.id.imgIcon)
                val tvRecommendationItem = view.findViewById<TextView>(R.id.tvRecommendationItem)
                tvRecommendationItem.setText(recommendationsInfoList.get(i).title)
                val viewGap: View = View.inflate(activity, R.layout.v3_layout_gap, null)
                imgIcon.setImageResource(getRecommendImageResourceIcon(recommendationsInfoList.get(i).img!!))
                view.setOnClickListener {
                    showDialog(
                        recommendationsInfoList.get(i).title,
                        recommendationsInfoList.get(i).text
                    )
                }
                binding.layoutRecomendationContainer.addView(view)
                binding.layoutRecomendationContainer.addView(viewGap)

            }
        }
    }

    fun getRecommendImageResourceIcon(imageName: String): Int {
        when (imageName) {
            "vitamin-d.png" -> {
                return R.drawable.v3_ic_epi_recommend_vitamin_d
            }
            "calorie-restriction.png" -> {
                //to be changed when push is done
                return R.drawable.v3_ic_epi_recommend_calorie_restriction
            }
            "antioxidant-level.png" -> {
                return R.drawable.v3_ic_epi_recommend_antioxidant_levels
            }
            "physical-activity.png" -> {
                return R.drawable.v3_ic_epi_recommend_physical_activity
            }
            "vitamin-a.png" -> {
                return R.drawable.v3_ic_epi_recommend_vitamin_a
            }
            "blue-light.png" -> {
                return R.drawable.v3_ic_epi_recommend_blue_light
            }
            "green-tea.png" -> {
                return R.drawable.v3_ic_epi_recommend_green_tea
            }
            "zinc.png" -> {
                return R.drawable.v3_ic_epi_recommend_zinc
            }
            "folate.png" -> {
                return R.drawable.v3_ic_epi_recommend_folate
            }
            "exercise.png" -> {
                return R.drawable.v3_ic_epi_recommend_exercise
            }
            "L-Glutamine.png" -> {
                return R.drawable.v3_ic_epi_recommend_l_glutamine
            }
            "omega-3-fatty-acids.png" -> {
                return R.drawable.v3_ic_epi_recommend_omega_3_ala
            }
            "omega-3-EPA-and-DHA.png" -> {
                return R.drawable.v3_ic_epi_recommend_omega_3_epa_and_dha
            }
            "omega-3-ALA.png" -> {
                return R.drawable.v3_ic_epi_recommend_omega_3_ala
            }
            "omega-7-(Palmitoleic-acid).png" -> {
                return R.drawable.v3_ic_epi_recommend_omega_7
            }
            "dietary-fibre.png" -> {
                return R.drawable.v3_ic_epi_recommend_dietary_fibre
            }
            "decrease-Omega-6-foods.png" -> {
                return R.drawable.v3_ic_epi_recommend_decrease_omega_6_foods
            }
            "metabolic-rate.png" -> {
                return R.drawable.v3_ic_epi_recommend_metabolic_rate
            }
            else -> {
                //to be changed when push is done
                return R.drawable.v3_ic_epi_recommend_calorie_restriction
            }
        }
    }

    fun setExpansionOutcomeLayout(show: Boolean) {
        if (show) {
            binding.layoutOutcomes.setBackgroundResource(R.drawable.v3_bg_epigen_recommendation_highlated)
            binding.tvOutcomesTitle.setTextColor(activity!!.resources.getColor(R.color.white))
            binding.tvOutcomes.setTextColor(activity!!.resources.getColor(R.color.white))
            binding.tvViewMoreOutcome.setTextColor(activity!!.resources.getColor(R.color.v3_epi_gen_icon_unselected))
            binding.tvViewMoreOutcome.setText(R.string.v3_view_less)
            binding.tvOutcomes.setText(Html.fromHtml(epigenticHealth?.data?.outcome))

        } else {
            binding.layoutOutcomes.setBackgroundResource(R.drawable.v3_bg_epigen_recommendation_normal)
            binding.tvOutcomesTitle.setTextColor(activity!!.resources.getColor(R.color.black))
            binding.tvOutcomes.setTextColor(activity!!.resources.getColor(R.color.black))
            binding.tvViewMoreOutcome.setTextColor(activity!!.resources.getColor(R.color.v3_epi_gen_colorPrimaryLight))
            binding.tvViewMoreOutcome.setText(R.string.v3_view_more)
            var outcomeText: String =
                Html.fromHtml(epigenticHealth?.data?.outcome.toString()).toString()
            if (outcomeText.length > 100) {
                outcomeText = outcomeText.substring(0, 99) + "..."
            }
            binding.tvOutcomes.setText(outcomeText)
        }
    }

    fun showExpansionRecommendationLayout(show: Boolean) {
        if (show) {
            binding.layoutRecomendations.setBackgroundResource(R.drawable.v3_bg_epigen_recommendation_highlated)
            binding.tvRecommendationsTitle.setTextColor(activity!!.resources.getColor(R.color.white))
            binding.tvRecommendations.setTextColor(activity!!.resources.getColor(R.color.white))
            binding.tvViewMoreRecommendations.visibility = View.GONE
            binding.tvViewLessRecommendations.visibility = View.VISIBLE
            binding.tvRecommendations.setText(Html.fromHtml(epigenticHealth?.data?.recommendations))
            binding.layoutRecomendationContainer.visibility = View.VISIBLE
        } else {
            binding.layoutRecomendations.setBackgroundResource(R.drawable.v3_bg_epigen_recommendation_normal)
            binding.tvRecommendationsTitle.setTextColor(activity!!.resources.getColor(R.color.black))
            binding.tvRecommendations.setTextColor(activity!!.resources.getColor(R.color.black))
            binding.tvViewMoreRecommendations.visibility = View.VISIBLE
            binding.tvViewLessRecommendations.visibility = View.GONE
            var recommendationText: String =
                Html.fromHtml(epigenticHealth?.data?.recommendations.toString()).toString()
            if (recommendationText.length > 100) {
                recommendationText = recommendationText.substring(0, 99) + "..."
            }
            binding.tvRecommendations.setText(recommendationText)
            binding.layoutRecomendationContainer.visibility = View.GONE
        }
    }

}
