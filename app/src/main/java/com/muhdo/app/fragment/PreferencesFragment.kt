package com.muhdo.app.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.muhdo.app.R
import com.muhdo.app.databinding.FragmentPreferencesBinding
import com.timingsystemkotlin.backuptimingsystem.Utility
import kotlinx.android.synthetic.main.fragment_preferences.view.*

class PreferencesFragment : Fragment() {


    internal lateinit var binding: FragmentPreferencesBinding
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_preferences, container, false)
        binding = FragmentPreferencesBinding.bind(view)

       val mealData = Utility.getMealData(activity!!)
        if (mealData != null) {
            when (mealData.goal) {
                "A" -> {
                    binding.checkBuildMuscle.isChecked = true
                }
                "B" -> {
                    binding.checkFatLoss.isChecked = true
                }
                "C" -> {
                    binding.checkWeightGain.isChecked = true
                }
                "D" -> {
                    binding.checkEnduranceSport.isChecked = true
                }
                "E" -> {
                    binding.checkGeneralHealth.isChecked = true
                }
            }

            when (mealData.isVegan) {
                "true" -> {
                    binding.radioVegan.btn_is_vegan.isChecked = true
                }
                "false" -> {
                    binding.radioVegan.btn_not_vegan.isChecked = true
                }
            }

            when (mealData.isVegetarian) {
                "true" -> {
                    binding.radioVegetarian.btn_is_vegetarian.isChecked = true
                }
                "false" -> {
                    binding.radioVegetarian.btn_not_vegetarian.isChecked = true
                }
            }

            when (mealData.isFish) {
                "true" -> {
                    binding.radioFish.btn_eat_fish.isChecked = true
                }
                "false" -> {
                    binding.radioFish.btn_no_fish.isChecked = true
                }
            }

            when (mealData.alcoholFree) {
                "true" -> {
                    binding.radioAlcohol.btn_consume_alcohol.isChecked = true
                }
                "false" -> {
                    binding.radioAlcohol.btn_no_alcohol.isChecked = true
                }
            }

            when (mealData.mealPerDay) {
                "2" -> {
                    binding.slider.position = 0
                    binding.mealText.text = "2"
                }
                "3" -> {
                    binding.slider.position = 1
                    binding.mealText.text = "3"
                }
                "4" -> {
                    binding.slider.position = 2
                    binding.mealText.text = "4"
                }
                "5" -> {
                    binding.slider.position = 3
                    binding.mealText.text = "5"
                }
                "6" -> {
                    binding.slider.position = 4
                    binding.mealText.text = "6"
                }
            }



            when (mealData.activity) {
                "23" -> {
                    binding.txtActivityLevel.text = activity!!.resources.getString(R.string.activity_text_1)
                    binding.activityLevel1.isChecked = true
                    binding.activityLevel2.isChecked = false
                    binding.activityLevel3.isChecked = false
                    binding.activityLevel4.isChecked = false
                    binding.activityLevel5.isChecked = false
                    binding.activityLevel6.isChecked = false
                    binding.activityLevel7.isChecked = false
                }
                "24" -> {
                    binding.txtActivityLevel.text = activity!!.resources.getString(R.string.activity_text_2)
                    binding.activityLevel1.isChecked = true
                    binding.activityLevel2.isChecked = true
                    binding.activityLevel3.isChecked = false
                    binding.activityLevel4.isChecked = false
                    binding.activityLevel5.isChecked = false
                    binding.activityLevel6.isChecked = false
                    binding.activityLevel7.isChecked = false
                }
                "25" -> {
                    binding.txtActivityLevel.text = activity!!.resources.getString(R.string.activity_text_3)
                    binding.activityLevel1.isChecked = true
                    binding.activityLevel2.isChecked = true
                    binding.activityLevel3.isChecked = true
                    binding.activityLevel4.isChecked = false
                    binding.activityLevel5.isChecked = false
                    binding.activityLevel6.isChecked = false
                    binding.activityLevel7.isChecked = false
                }
                "26" -> {
                    binding.txtActivityLevel.text = activity!!.resources.getString(R.string.activity_text_4)
                    binding.activityLevel1.isChecked = true
                    binding.activityLevel2.isChecked = true
                    binding.activityLevel3.isChecked = true
                    binding.activityLevel4.isChecked = true
                    binding.activityLevel5.isChecked = false
                    binding.activityLevel6.isChecked = false
                    binding.activityLevel7.isChecked = false
                }
                "27" -> {
                    binding.txtActivityLevel.text = activity!!.resources.getString(R.string.activity_text_5)
                    binding.activityLevel1.isChecked = true
                    binding.activityLevel2.isChecked = true
                    binding.activityLevel3.isChecked = true
                    binding.activityLevel4.isChecked = true
                    binding.activityLevel5.isChecked = true
                    binding.activityLevel6.isChecked = false
                    binding.activityLevel7.isChecked = false
                }
                "28" -> {
                    binding.txtActivityLevel.text = activity!!.resources.getString(R.string.activity_text_6)
                    binding.activityLevel1.isChecked = true
                    binding.activityLevel2.isChecked = true
                    binding.activityLevel3.isChecked = true
                    binding.activityLevel4.isChecked = true
                    binding.activityLevel5.isChecked = true
                    binding.activityLevel6.isChecked = true
                    binding.activityLevel7.isChecked = false
                }
                "29" -> {
                    binding.txtActivityLevel.text = activity!!.resources.getString(R.string.activity_text_7)
                    binding.activityLevel1.isChecked = true
                    binding.activityLevel2.isChecked = true
                    binding.activityLevel3.isChecked = true
                    binding.activityLevel4.isChecked = true
                    binding.activityLevel5.isChecked = true
                    binding.activityLevel6.isChecked = true
                    binding.activityLevel7.isChecked = true
                }
            }


        } else {
            Toast.makeText(activity, "No Data Found", Toast.LENGTH_LONG).show()
        }

        val workoutData = Utility.getWorkoutData(activity!!)
        if (workoutData != null) {
            when (workoutData.new) {
                "A" -> {
                    binding.checkWorkoutBodyBuilding.isChecked = true
                }
                "B" -> {
                    binding.checkWorkoutFatLoss.isChecked = true
                }
                "C" -> {
                    binding.checkWorkoutHealth.isChecked = true
                }
                "D" -> {
                    binding.checkWorkoutFlexibility.isChecked = true
                }
                "E" -> {
                    binding.checkWorkoutEndurance.isChecked = true
                }
                "F" -> {
                    binding.checkWorkoutPowerLifter.isChecked = true
                }
                "G" -> {
                    binding.checkWorkoutBodyFat.isChecked = true
                }

            }

            when (workoutData.new1) {
                "1" -> {
                    binding.btnTrainingLevel1.isChecked = true
                    binding.btnTrainingLevel2.isChecked = false
                    binding.btnTrainingLevel3.isChecked = false
                    binding.btnTrainingLevel4.isChecked = false
                    binding.btnTrainingLevel5.isChecked = false
                    binding.btnTrainingLevel6.isChecked = false
                    binding.btnTrainingLevel7.isChecked = false

                }
                "2" -> {
                    binding.btnTrainingLevel1.isChecked = true
                    binding.btnTrainingLevel2.isChecked = true
                    binding.btnTrainingLevel3.isChecked = false
                    binding.btnTrainingLevel4.isChecked = false
                    binding.btnTrainingLevel5.isChecked = false
                    binding.btnTrainingLevel6.isChecked = false
                    binding.btnTrainingLevel7.isChecked = false
                }
                "3" -> {
                    binding.btnTrainingLevel1.isChecked = true
                    binding.btnTrainingLevel2.isChecked = true
                    binding.btnTrainingLevel3.isChecked = true
                    binding.btnTrainingLevel4.isChecked = false
                    binding.btnTrainingLevel5.isChecked = false
                    binding.btnTrainingLevel6.isChecked = false
                    binding.btnTrainingLevel7.isChecked = false
                }
                "4" -> {
                    binding.btnTrainingLevel1.isChecked = true
                    binding.btnTrainingLevel2.isChecked = true
                    binding.btnTrainingLevel3.isChecked = true
                    binding.btnTrainingLevel4.isChecked = true
                    binding.btnTrainingLevel5.isChecked = false
                    binding.btnTrainingLevel6.isChecked = false
                    binding.btnTrainingLevel7.isChecked = false
                }
                "5" -> {
                    binding.btnTrainingLevel1.isChecked = true
                    binding.btnTrainingLevel2.isChecked = true
                    binding.btnTrainingLevel3.isChecked = true
                    binding.btnTrainingLevel4.isChecked = true
                    binding.btnTrainingLevel5.isChecked = true
                    binding.btnTrainingLevel6.isChecked = false
                    binding.btnTrainingLevel7.isChecked = false
                }
                "6" -> {
                    binding.btnTrainingLevel1.isChecked = true
                    binding.btnTrainingLevel2.isChecked = true
                    binding.btnTrainingLevel3.isChecked = true
                    binding.btnTrainingLevel4.isChecked = true
                    binding.btnTrainingLevel5.isChecked = true
                    binding.btnTrainingLevel6.isChecked = true
                    binding.btnTrainingLevel7.isChecked = false
                }
                "7" -> {
                    binding.btnTrainingLevel1.isChecked = true
                    binding.btnTrainingLevel2.isChecked = true
                    binding.btnTrainingLevel3.isChecked = true
                    binding.btnTrainingLevel4.isChecked = true
                    binding.btnTrainingLevel5.isChecked = true
                    binding.btnTrainingLevel6.isChecked = true
                    binding.btnTrainingLevel7.isChecked = true
                }
                "8" -> {
                    binding.btnTrainingLevel1.isChecked = false
                    binding.btnTrainingLevel2.isChecked = false
                    binding.btnTrainingLevel3.isChecked = false
                    binding.btnTrainingLevel4.isChecked = false
                    binding.btnTrainingLevel5.isChecked = false
                    binding.btnTrainingLevel6.isChecked = false
                    binding.btnTrainingLevel7.isChecked = false
                }


            }

            when (workoutData.new2) {
                "30" -> {
                    binding.trainingSeekBar.left = 0
                }
                "45" -> {
                    binding.trainingSeekBar.left = 1
                }
                "60" -> {
                    binding.trainingSeekBar.left = 2
                }

            }

            when (workoutData.new3) {
                "30" -> {
                    binding.trainingSeekBar.right = 0
                }
                "45" -> {
                    binding.trainingSeekBar.right = 1
                }
                "60" -> {
                    binding.trainingSeekBar.right = 2
                }

            }


            when (workoutData.new4) {
                "21" -> {
                    binding.checkFullyEquipped.isChecked = true
                }
                "23" -> {
                    binding.checkHomeTraining.isChecked = true
                }
            }

            when (workoutData.new5) {
                "46" -> {
                    binding.checkNovice.isChecked = true
                }
                "47" -> {
                    binding.checkIntermediate.isChecked = true
                }
                "48" -> {
                    binding.checkAdvanced.isChecked = true
                }
                "49" -> {
                    binding.checkHigherAdvanced.isChecked = true
                }
                "50" -> {
                    binding.checkExpert.isChecked = true
                }
                "51" -> {
                    binding.checkElite.isChecked = true
                }
            }


            when (workoutData.new6) {
                "true" -> {
                    binding.radioAgainstExercise.btn_against_exercise.isChecked = true
                }
                "false" -> {
                    binding.radioAgainstExercise.btn_not_against_exercise.isChecked = true
                }
            }

            when (workoutData.new7) {
                "true" -> {
                    binding.radioPerWeek.btn_injuries.isChecked = true
                }
                "false" -> {
                    binding.radioPerWeek.btn_not_injuries.isChecked = true
                }
            }

            when (workoutData.new8) {
                "true" -> {
                    binding.radioPregnant.btn_pregnant.isChecked = true
                }
                "false" -> {
                    binding.radioPregnant.btn_not_pregnant.isChecked = true
                }
            }

        } else {
            Toast.makeText(activity, "No Data Found", Toast.LENGTH_LONG).show()
        }

        return view
    }


}