package com.muhdo.app.fragment

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.muhdo.app.R
import com.muhdo.app.databinding.FragmentBannerSleepIndexBinding
import com.muhdo.app.utils.PreferenceConnector
import com.muhdo.app.utils.ProgressBarAnimation

private lateinit var binding: FragmentBannerSleepIndexBinding

class BannerSleepIndexFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_banner_sleep_index, container, false)
        binding = FragmentBannerSleepIndexBinding.bind(view)

        val sleepIndex =
            context?.let { PreferenceConnector.readString(it, PreferenceConnector.SLEEP_INDEX, "") }
        Log.d("data", "sleep index" + sleepIndex)
        binding.textViewSleepIndexHours.text = sleepIndex

        binding.frameSleep.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                /*     Log.d("data","click sleep index frame")
                 val intent = Intent(activity, ResultChooseMode::class.java)
                 val title = "SLEEP"
                 intent.putExtra("title", title)
                 intent.putExtra("position", "1")
                 activity!!.startActivity(intent)*/

            }

        })
        if (!sleepIndex.isNullOrEmpty())
            setData(sleepIndex)
        return view
    }


    private fun setData(data: String?) {
        try {
            val progress = (data!!.toFloat() * 10)
            val progressInt = progress.toInt()
            binding.circleProgressBar.progress = progressInt
            val anim = ProgressBarAnimation(binding.circleProgressBar, 0F, progressInt.toFloat())
            anim.duration = 1000
            binding.circleProgressBar.startAnimation(anim)
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }
}
