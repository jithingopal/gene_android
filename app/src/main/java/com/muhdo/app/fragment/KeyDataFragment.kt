package com.muhdo.app.fragment

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.app.AlertDialog
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnticipateInterpolator
import android.view.animation.OvershootInterpolator
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import com.github.mikephil.charting.data.PieData
import com.github.mikephil.charting.data.PieDataSet
import com.github.mikephil.charting.data.PieEntry
import com.github.mikephil.charting.formatter.PercentFormatter
import com.muhdo.app.R
import com.muhdo.app.adapter.SpeedAdapter
import com.muhdo.app.apiModel.keyData.DietChart
import com.muhdo.app.apiModel.keyData.Indicator
import com.muhdo.app.apiModel.keyData.KeyDataModel
import com.muhdo.app.repository.ApiUtilis
import com.muhdo.app.repository.ErrorModel
import com.muhdo.app.repository.NetworkManager
import com.muhdo.app.repository.ServiceListener
import com.muhdo.app.utils.AnimatorUtils
import com.muhdo.app.utils.Events
import com.muhdo.app.utils.GlobalBus
import com.muhdo.app.utils.PreferenceConnector
import com.muhdo.app.utils.v3.Constants
import com.timingsystemkotlin.backuptimingsystem.Utility
import kotlinx.android.synthetic.main.fragment_keydata.*
import org.greenrobot.eventbus.Subscribe


class KeyDataFragment : Fragment() {
    companion object {
        @JvmStatic
        fun getInstance(): KeyDataFragment {
            val fragment = KeyDataFragment()
            return fragment
        }
    }

    lateinit var mSpeedAdapter: SpeedAdapter
    var imageBackToTop = null
    var showArc = false
    var context = this

//    var genoTypeId: String = ""

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        GlobalBus.getBus()!!.register(this@KeyDataFragment)

        return inflater.inflate(R.layout.fragment_keydata, container, false)
    }

    @RequiresApi(Build.VERSION_CODES.HONEYCOMB)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }

    public fun loadData() {
        Log.d("frag", "fragment visible key data fragment")
        try {
            val modeId = getContext()?.let { it1 ->
                PreferenceConnector.readString(
                    it1,
                    PreferenceConnector.MODE_ID,
                    ""
                )
            }
            var genoTypeId = this.arguments!!.getString(Constants.MODE_ID)
            if (!modeId.equals("")) {
                getKeyData(modeId.toString())
            } else {
                getKeyData(genoTypeId)
            }
        } catch (e: Exception) {

        }

        fab.setOnClickListener {
            if (showArc == false) {
                showMenu()
                showArc = true
            } else {
                showArc = false
                hideMenu()
            }

        }

        /* val userId = Base64.decode(Utility.getUserID(requireContext()), Base64.DEFAULT)
         val decodeUserId = String(userId, charset("UTF-8"))
 */
        //-------------------------------------------------------------------------------------------------------------------------------------
//        val modeText= getContext()?.let { it1 -> PreferenceConnector.readString(it1, PreferenceConnector.MODE_TEXT, "") }
        val modeText =
            com.muhdo.app.utils.v3.Constants.getLastLoadedResultText(activity!!.applicationContext)//getContext()?.let { it1 -> PreferenceConnector.readString(it1, PreferenceConnector.MODE_TEXT, "") }

        if (modeText.equals("Fitness & Endurance", true)) {
            fab.setImageResource(R.drawable.fitness_icon)
            getKeyData("5c8c96a79c54381add6884da")

        } else if (modeText.equals("Build Muscle", true)) {
            fab.setImageResource(R.drawable.muscle_build_icon)
            getKeyData("5c8c96a79c54381add6884db")
        } else if (modeText.equals("Fat Loss", true)) {
            fab.setImageResource(R.drawable.fat_loss_icon)
            getKeyData("5c8c96a79c54381add6884dc")
        } else if (modeText.equals("Health & Wellbeing", true)) {
            fab.setImageResource(R.drawable.fit_for_life_icon)
            getKeyData("5c8c96a79c54381add6884dd")
        } else {
            //default one
            fab.setImageResource(R.drawable.fat_loss_icon)
            getKeyData("5c8c96a79c54381add6884da")
        }
//------------------------------------------------------------------------------------------------------------------------------------------

        arc_layout.getChildAt(0).setOnClickListener {
            /*getContext()?.let { it1 -> PreferenceConnector.writeString(it1, PreferenceConnector.MODE_TEXT, "Health & Wellbeing") }
            getContext()?.let { it1 -> PreferenceConnector.writeString(it1, PreferenceConnector.MODE_ID, "5c8c96a79c54381add6884dd") }
*/
            com.muhdo.app.utils.v3.Constants.setLastLoadedResultText("Health & Wellbeing")
            showArc = false
            getKeyData("5c8c96a79c54381add6884dd")
            hideMenu()
            fab.setImageResource(R.drawable.fit_for_life_icon)
        }
        arc_layout.getChildAt(1).setOnClickListener {
            /* getContext()?.let { it1 -> PreferenceConnector.writeString(it1, PreferenceConnector.MODE_TEXT, "Fat Loss") }
             getContext()?.let { it1 -> PreferenceConnector.writeString(it1, PreferenceConnector.MODE_ID, "5c8c96a79c54381add6884dc") }
 */          com.muhdo.app.utils.v3.Constants.setLastLoadedResultText("Fat Loss")
            showArc = false
            getKeyData("5c8c96a79c54381add6884dc")
            hideMenu()
            fab.setImageResource(R.drawable.fat_loss_icon)
        }
        arc_layout.getChildAt(2).setOnClickListener {
            /*  getContext()?.let { it1 -> PreferenceConnector.writeString(it1, PreferenceConnector.MODE_TEXT, "Build Muscle") }
              getContext()?.let { it1 -> PreferenceConnector.writeString(it1, PreferenceConnector.MODE_ID, "5c8c96a79c54381add6884db") }
  */
            com.muhdo.app.utils.v3.Constants.setLastLoadedResultText("Build Muscle")
            showArc = false
            getKeyData("5c8c96a79c54381add6884db")
            hideMenu()
            fab.setImageResource(R.drawable.muscle_build_icon)
//            muscle
//            Toast.makeText(activity, "33333", Toast.LENGTH_LONG).show()
        }
        arc_layout.getChildAt(3).setOnClickListener {
            /* getContext()?.let { it1 -> PreferenceConnector.writeString(it1, PreferenceConnector.MODE_TEXT, "Fitness & Endurance") }
             getContext()?.let { it1 -> PreferenceConnector.writeString(it1, PreferenceConnector.MODE_ID, "5c8c96a79c54381add6884da") }*/
            com.muhdo.app.utils.v3.Constants.setLastLoadedResultText("Fitness & Endurance")
            showArc = false
            getKeyData("5c8c96a79c54381add6884da")
            hideMenu()
            fab.setImageResource(R.drawable.fitness_icon)

//            fitness
//            Toast.makeText(activity, "33333", Toast.LENGTH_LONG).show()
        }



        img_back_to_top.setOnClickListener {
            val handler = Handler()
            handler.postDelayed({
                // for autoLogin user
                //binding.scrollView.fullScroll(View.FOCUS_UP)
                scroll_view.smoothScrollTo(0, 0);
            }, 0)

        }
    }


    override fun onStart() {
        super.onStart()
        Log.d("frag", "onStart key data fragment")
        loadData();

    }

    override fun onResume() {
        super.onResume()
        Log.d("frag", "onResume key data fragment")
    }


    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser);

        if (isVisibleToUser && isResumed()) { // fragment is visible and have created
            loadData();
        }
    }

    @RequiresApi(Build.VERSION_CODES.HONEYCOMB)
    private fun setPieChart(data: List<DietChart>) {

        pieChart.setUsePercentValues(true)

        val yvalues = ArrayList<PieEntry>()

        // val xVals = ArrayList<String>()


        for (i in 0..data!!.size - 1) {
            yvalues.add(
                PieEntry(
                    data[i].getResult()!!.toFloat()
                    //data!![i].getTitle()!!

                )
            )

            //Log.wtf("data","piechart"+data[i].getResult()!!.toFloat())
            //xVals.add(data!![i].getTitle()!!)

        }
        pieChart.setEntryLabelColor(activity!!.resources.getColor(R.color.colorPrimary))

//        pieChart.isUsePercentValuesEnabled = true

//        for (i in 0 until count) {
//            entries.add(
//                PieEntry(
//                    (Math.random() * range + range / 5).toFloat(),
//                    "Qwerty",
//                    resources.getDrawable(R.drawable.star)
//                )
//            )
//        }


        val dataSet = PieDataSet(yvalues, "")

//        val data = PieData(xVals, dataSet)
        // In Percentage
        dataSet.valueFormatter = PercentFormatter()


        dataSet.colors = mutableListOf(
            ContextCompat.getColor(activity!!, R.color.color_sat_fats),
            ContextCompat.getColor(activity!!, R.color.color_un_fats),
            ContextCompat.getColor(activity!!, R.color.color_protein),
            ContextCompat.getColor(activity!!, R.color.color_carbs)

        )




        pieChart.legend.isWordWrapEnabled = true
        val legend = pieChart.legend
//        legend.textSize = 14f
        //legend.position = Legend.LegendPosition.BELOW_CHART_CENTER

        val data = PieData(dataSet)
        data.setValueFormatter(PercentFormatter(pieChart))
        //dataSet.setColors(ColorTemplate.COLORFUL_COLORS)
        pieChart.isDrawHoleEnabled = false
        data.setValueTextSize(13f)
        // data.setValueTypeface()
        pieChart.animateXY(1400, 1400)
        pieChart.setDescription(null)
        pieChart.data = data
        pieChart.setUsePercentValues(true)
        pieChart.legend.isEnabled = false
        pieChart.isRotationEnabled = false


    }

    private fun populateView(data: List<Indicator>) {
        recycler_speedView.layoutManager = GridLayoutManager(activity, 2)
//        mSpeedList = ArrayList()
//        mSpeedList.add(RecyclerData("MUSCLE POWER", "NORMAL", 50, 4000))
//        mSpeedList.add(RecyclerData("MUSCLE STAMINA", "HIGHLY GIFTED", 0, 0))
        mSpeedAdapter = SpeedAdapter(activity, data, this@KeyDataFragment)
        recycler_speedView.adapter = mSpeedAdapter
    }

    @RequiresApi(Build.VERSION_CODES.HONEYCOMB)
    private fun createShowItemAnimator(item: View): Animator {

        val dx = fab.getX() - item.x
        val dy = fab.getY() - item.y

        item.rotation = 0f
        item.translationX = dx
        item.translationY = dy

        return ObjectAnimator.ofPropertyValuesHolder(
            item,
            AnimatorUtils.rotation(0f, 720f),
            AnimatorUtils.translationX(dx, 0f),
            AnimatorUtils.translationY(dy, 0f)
        )
    }

    @RequiresApi(Build.VERSION_CODES.HONEYCOMB)
    private fun createHideItemAnimator(item: View): Animator {
        val dx = fab.x - item.x
        val dy = fab.y - item.y

        val anim = ObjectAnimator.ofPropertyValuesHolder(
            item,
            AnimatorUtils.rotation(720f, 0f),
            AnimatorUtils.translationX(0f, dx),
            AnimatorUtils.translationY(0f, dy)
        )

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            anim.addListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator) {
                    super.onAnimationEnd(animation)
                    item.translationX = 0f
                    item.translationY = 0f
                }
            })
        }

        return anim
    }

    @RequiresApi(Build.VERSION_CODES.HONEYCOMB)
    private fun showMenu() {
        menu_layout.setVisibility(View.VISIBLE)

        val animList = java.util.ArrayList<Animator>()

        var i = 0
        val len = arc_layout.getChildCount()
        while (i < len) {
            animList.add(createShowItemAnimator(arc_layout.getChildAt(i)))
            i++
        }

        val animSet = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            AnimatorSet()
        } else {
            TODO("VERSION.SDK_INT < HONEYCOMB")
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            animSet.duration = 400
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            animSet.interpolator = OvershootInterpolator()
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            animSet.playTogether(animList)
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            animSet.start()
        }
    }

    @RequiresApi(Build.VERSION_CODES.HONEYCOMB)
    private fun hideMenu() {

        val animList = java.util.ArrayList<Animator>()

        for (i in arc_layout.getChildCount() - 1 downTo 0) {
            animList.add(createHideItemAnimator(arc_layout.getChildAt(i)))
        }

        val animSet = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            AnimatorSet()
        } else {
            TODO("VERSION.SDK_INT < HONEYCOMB")
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            animSet.duration = 400
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.DONUT) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                animSet.interpolator = AnticipateInterpolator()
            }
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            animSet.playTogether(animList)
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            animSet.addListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator) {
                    super.onAnimationEnd(animation)
                    menu_layout.setVisibility(View.INVISIBLE)
                }
            })
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            animSet.start()
        }

    }


    private fun getKeyData(genoTypeId: String) {

        //val userId = Base64.decode(Utility.getUserID(requireContext()), Base64.DEFAULT)
        //val decodeUserId = String(userId, charset("UTF-8"))

        progress_bar.visibility = View.VISIBLE
        val manager = NetworkManager()
        if (manager.isConnectingToInternet(requireContext())) {
            manager.createApiRequest(
                ApiUtilis.getAPIInstance(activity!!).getKeyData(
                    Utility.getUserID(requireContext()),
                    genoTypeId,
                    "true",
                    "true"
                ),
                object : ServiceListener<KeyDataModel> {
                    @RequiresApi(Build.VERSION_CODES.HONEYCOMB)
                    override fun getServerResponse(response: KeyDataModel, requestcode: Int) {
                        if (progress_bar != null) {
                            progress_bar.visibility = View.GONE
                        }
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                            try {
                                if (response.getCode() == 200 && response.getData() != null) {
                                    setData(response)
                                }
                            } catch (ex: Exception) {

                            }
                        }
                    }

                    override fun getError(error: ErrorModel, requestcode: Int) {
                        progress_bar.visibility = View.GONE
                        if (activity != null && activity!!.applicationContext != null) {
                            //Toast.makeText(activity!!.applicationContext,"server error", Toast.LENGTH_LONG).show()
                            displayAlert()
                        }
                        // Utility.displayShortSnackBar(
//                            parent_layout,
//                            error.getMessage()!!
//                        )
//                        val json = JSONObject(error.getMessage()!!)
//                        if(json.has("message") && activity != null && activity!!.applicationContext != null) {
//                            Toast.makeText(activity!!.applicationContext, json.getString("message"), Toast.LENGTH_LONG)
//                                .show()
//                        }

                    }
                })
        } else {
            progress_bar.visibility = View.GONE
            Utility.displayShortSnackBar(
                parent_layout,
                resources.getString(R.string.check_internet)
            )

        }
    }

    @RequiresApi(Build.VERSION_CODES.HONEYCOMB)
    private fun setData(data: KeyDataModel) {
        setPieChart(data.getData()!!.getChart()!!)
        populateView(data.getData()!!.getIndicators()!!)
    }


    private fun displayAlert() {
        val dialogBuilder = AlertDialog.Builder(requireContext())
        dialogBuilder.setMessage("We are currently processing your data. As you have just registered please allow 15 minutes for the report to generate")
            .setCancelable(false)
            .setPositiveButton("Ok") { _, _ ->
                // val i = Intent(requireContext(), DashboardActivity::class.java)
                // startActivity(i)
            }


        val alert = dialogBuilder.create()
        alert.show()
    }

    @Subscribe
    fun getCategoryData(categoryId: String, sectionId: String, modeId: String) {
        Utility.categoryID = categoryId
        Utility.categoryStatus = true
        Utility.sectionId = sectionId
        val fragmentActivityMessageEvent =
            Events.FragmentActivityMessage(categoryId, sectionId, modeId)
        GlobalBus.getBus()!!.post(fragmentActivityMessageEvent)

        /*  progress_bar.visibility = View.VISIBLE
          val manager = NetworkManager()
          if (manager.isConnectingToInternet(requireContext())) {
              manager.createApiRequest(
                  ApiUtilis.getAPIService(Constants.KEY_DATA_API).getKeyDataByCategory(Utility.getUserID(requireContext()),modeId,"true","true", categoryId, sectionId),
                  object : ServiceListener<KeyDataModel> {
                      override fun getServerResponse(response: KeyDataModel, requestcode: Int) {
                          progress_bar.visibility = View.GONE
                          setData(response)
                      }

                      override fun getError(error: ErrorModel, requestcode: Int) {

                          progress_bar.visibility = View.GONE
                          Utility.displayShortSnackBar(
                              parent_layout,
                              error.getMessage()!!
                          )


                      }
                  })
          } else {
              progress_bar.visibility = View.GONE
              Utility.displayShortSnackBar(
                  parent_layout,
                  resources.getString(R.string.check_internet)
              )

          }*/

    }

    @Subscribe
    fun getMessage(activityFragmentMessage: Events.ActivityFragmentMessage) {
//val messageView = view!!.findViewById<View>(R.id.message) as TextView
//         messageView.text = (getString(R.string.message_received) +
//                 " " + activityFragmentMessage.getMessage())
//
//Toast.makeText(
//    activity,
//    getString(R.string.message_fragment) +
//    " " + activityFragmentMessage.getMessage(),
//Toast.LENGTH_SHORT).show()
    }


    override fun onDestroyView() {
        super.onDestroyView()
        // unregister the registered event.
        GlobalBus.getBus()!!.unregister(this)
    }


//    https://bx3kma191i.execute-api.us-east-1.amazonaws.com/dev/keydata?user_id=NWM5OGUxYzg5YmE2M2VlZjVjNzNjNmJj&mode_id=5c8c96a79c54381add6884db&keydata=true
//    https://efnsx5tq3k.execute-api.us-east-1.amazonaws.com/dev/user/keydata?user_id=NWNhNzExNzY2NjcyODIzYWRlOWNjYzY4&section_id=5c8c95189c54381add6884d5&mode_id=5c8c96a79c54381add6884db&keydata=true
}