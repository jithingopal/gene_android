package com.muhdo.app.fragment.v3.guide.workout

import android.content.Context
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.muhdo.app.R
import com.muhdo.app.databinding.V3LayoutPlanWorkoutChooseEquipmentBinding
import com.muhdo.app.utils.v3.getSelectedRadioButtonText

class ChooseEquipmentFragment : Fragment() {
    lateinit var binding: V3LayoutPlanWorkoutChooseEquipmentBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view =
            inflater.inflate(R.layout.v3_layout_plan_workout_choose_equipment, container, false)
        binding = V3LayoutPlanWorkoutChooseEquipmentBinding.bind(view)
        return view
    }

    fun getSelectedEquipment(): Int? {
        when (binding.rgEquipments.checkedRadioButtonId) {
            -1 -> {
                return null
            }
            R.id.rbGYM -> {
                return 21
            }
            R.id.rbHome -> {
                return 23
            }
            else -> {
                return null
            }
        }

    }


}
