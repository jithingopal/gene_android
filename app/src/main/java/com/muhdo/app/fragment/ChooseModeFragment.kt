package com.muhdo.app.fragment

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import com.muhdo.app.R
import com.muhdo.app.adapter.GenoTypeAdapter
import com.muhdo.app.adapter.ModeSelectionAdapter
import com.muhdo.app.apiModel.keyData.GenoTypeModel
import com.muhdo.app.databinding.ActivityChooseModeBinding
import com.muhdo.app.fragment.v3.ResultsFragment
import com.muhdo.app.model.RecyclerData
import com.muhdo.app.repository.ApiUtilis
import com.muhdo.app.repository.ErrorModel
import com.muhdo.app.repository.NetworkManager
import com.muhdo.app.repository.ServiceListener
import com.muhdo.app.ui.ResultsActivity
import com.muhdo.app.utils.PreferenceConnector
import com.muhdo.app.utils.v3.Constants
import com.muhdo.app.utils.v3.TempUtil
import com.timingsystemkotlin.backuptimingsystem.Utility

class ChooseModeFragment : Fragment() {


    internal lateinit var binding: ActivityChooseModeBinding
    var adapter: GenoTypeAdapter? = null
    private lateinit var modeAdapter: ModeSelectionAdapter
    private lateinit var mSpeedList: ArrayList<RecyclerData>

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.activity_choose_mode, container, false)
        binding = ActivityChooseModeBinding.bind(view)

        binding.genoTypeGrid.layoutManager = GridLayoutManager(requireActivity(), 2)

        Utility.sectionList.clear()
        Utility.modeList.clear()

        getGenotypeModes()
        return view
    }

    private fun getGenotypeModes() {
        binding.progressBar.visibility = View.VISIBLE
        val manager = NetworkManager()
        if (manager.isConnectingToInternet(requireContext())) {
            manager.createApiRequest(
                ApiUtilis.getAPIInstance(activity!!).getGenoType(),
                object : ServiceListener<GenoTypeModel> {
                    override fun getServerResponse(response: GenoTypeModel, requestcode: Int) {
                        binding.progressBar.visibility = View.GONE
                        setList(response)
                    }

                    override fun getError(error: ErrorModel, requestcode: Int) {

                        binding.progressBar.visibility = View.GONE
//                        Utility.displayShortSnackBar(
//                            binding.parentLayout,
//                            error.getMessage()!!
//                        )
                        /*Toast.makeText(activity!!.applicationContext,"server error", Toast.LENGTH_LONG)
                        Toast.makeText(activity!!.applicationContext,"server error", Toast.LENGTH_LONG)
                            .show()*/
                    }
                })
        } else {
            binding.progressBar.visibility = View.GONE
            Toast.makeText(requireContext(), "Please check internet connection!", Toast.LENGTH_LONG)
                .show()
        }
    }

    private fun setList(data: GenoTypeModel) {
        if (activity != null && activity!!.applicationContext != null) {
            adapter = GenoTypeAdapter(
                activity!!.applicationContext,
                this@ChooseModeFragment,
                data.getData()!!
            )
            binding.genoTypeGrid.adapter = adapter
        }

        binding.list.layoutManager = GridLayoutManager(activity, 3)
        mSpeedList = ArrayList()
        mSpeedList.add(RecyclerData("Stress", R.drawable.stress_m))
        mSpeedList.add(RecyclerData("Sleep", R.drawable.sleep_m))
        mSpeedList.add(RecyclerData("Anti-Ageing", R.drawable.anti_ageing_m))
        mSpeedList.add(RecyclerData("Injury Risk", R.drawable.injury_m))
        mSpeedList.add(RecyclerData("Heart Health", R.drawable.heart_health_m))
        mSpeedList.add(RecyclerData("Mental Health", R.drawable.mental_health_m))
        mSpeedList.add(RecyclerData("Addiction", R.drawable.addiction_m))
        mSpeedList.add(RecyclerData("Skin-Ageing", R.drawable.skin_ageing_m))
        mSpeedList.add(RecyclerData("Gut Health", R.drawable.gut_health_m))
        mSpeedList.add(RecyclerData("Skin Health", R.drawable.skin_health_m))
        mSpeedList.add(RecyclerData("Eye Health", R.drawable.eye_health_m))
        mSpeedList.add(RecyclerData("Muscle Health", R.drawable.muscle_health_m))


        try {
            modeAdapter = ModeSelectionAdapter(mSpeedList, requireActivity())
            binding.list.adapter = modeAdapter
        } catch (e: IllegalStateException) {
            e.printStackTrace()
        }
    }

    fun callKeyData(id: String, title: String?) {

        getContext()?.let { it1 ->
            PreferenceConnector.writeString(
                it1,
                PreferenceConnector.MODE_TEXT,
                title.toString()
            )
        }
        getContext()?.let { it1 ->
            PreferenceConnector.writeString(
                it1,
                PreferenceConnector.MODE_ID,
                id
            )
        }
        val i = Intent(activity, ResultsActivity::class.java)
        i.putExtra(Constants.MODE_ID, id)
        i.putExtra(Constants.MODE_TITLE, title)
//        startActivity(i)

        var resultsFragment: ResultsFragment = ResultsFragment()
        resultsFragment.setData(id, title!!)
        replaceFragment(resultsFragment)
        TempUtil.currentFragment = TempUtil.NO_FRAGMENTS_SELECTED


    }

    @SuppressLint("WrongConstant")
    private fun replaceFragment(fragment: Fragment) {
        val transaction = activity!!.supportFragmentManager.beginTransaction()
        transaction.replace(R.id.flContent, fragment)
        transaction.addToBackStack("share")
        transaction.commit()

    }
}