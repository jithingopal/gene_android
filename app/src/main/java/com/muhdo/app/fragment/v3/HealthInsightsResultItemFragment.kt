package com.muhdo.app.fragment.v3


import android.animation.Animator
import android.graphics.*
import android.graphics.drawable.BitmapDrawable
import android.os.Build
import android.os.Bundle
import android.text.Html
import android.util.Base64
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.TextView
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import com.google.gson.JsonObject
import com.muhdo.app.R
import com.muhdo.app.apiModel.healthinsights.ResponseHealthInisghts
import com.muhdo.app.databinding.FragmentHealthInsightsResultItemBinding
import com.muhdo.app.repository.ApiUtilis
import com.muhdo.app.utils.v3.TempUtil
import com.timingsystemkotlin.backuptimingsystem.Utility
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import org.jetbrains.anko.toast


class HealthInsightsResultItemFragment : Fragment() {
    private lateinit var completeResponse: ResponseHealthInisghts
    private lateinit var binding: FragmentHealthInsightsResultItemBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_health_insights_result_item, container, false)
        binding = FragmentHealthInsightsResultItemBinding.bind(view)
        val value: String = arguments?.get("apiRoute").toString()
        getDataForFields(value)
        binding.llStressPressureResults.setOnClickListener {
            if (binding.tvStressPressureResults.text.isNotEmpty()) {
                /*   binding.svStressPressure.post {
                       binding.svStressPressure.smoothScrollBy(0, 250)
                   }*/
                binding.ivStressResults.setImageBitmap(
                    rotateBitmap(
                        (binding.ivStressResults.drawable as BitmapDrawable).bitmap,
                        180
                    )
                )
                binding.tvStressPressureResults.toggleVisibility()
            } else
                context?.toast(getString(R.string.no_data_available))
        }
        binding.llStressPressureRecom.setOnClickListener {
            if (binding.tvStressPressureRecom.text.isNotEmpty()) {
                binding.svStressPressure.post {
                    binding.svStressPressure.smoothScrollBy(250, 0)
                }

                binding.ivStressRecom.setImageBitmap(
                    rotateBitmap(
                        (binding.ivStressRecom.drawable as BitmapDrawable).bitmap,
                        180
                    )
                )
                binding.tvStressPressureRecom.toggleVisibility()
            } else
                context?.toast(getString(R.string.no_data_available))
        }
        return view
    }

    private fun getDataForFields(key: String) {
        binding.progressBar.visibility = View.VISIBLE
        val userId = Base64.decode(
            Utility.getUserID(activity!!.applicationContext),
            Base64.DEFAULT
        )
        val decodeUserId = String(userId, charset("UTF-8"))
        val requestBody = JsonObject()
        requestBody.addProperty("user_id", decodeUserId)
        requestBody.addProperty("type", key)
        ApiUtilis.getAPIInstance(activity!!).getDataForHealthInsights(requestBody)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ success ->
                run {
                    binding.progressBar.visibility = View.GONE
                    completeResponse = success
                    val titles = arrayListOf<String>()
                    completeResponse.data.forEach { data ->
                        titles.add(data.title)
                    }
                    context?.let {

                        val arrayAdapter = ArrayAdapter<String>(
                            it,
                            R.layout.item_spinner_arrow,
                            R.id.txtDropDownLabel,
                            titles
                        )
                        arrayAdapter.setDropDownViewResource(R.layout.item_spinner)
                        binding.spStressPressure.adapter = arrayAdapter
                    }
                }
            }, { error ->
                run {
                    binding.progressBar.visibility = View.GONE
                    Utility.displayShortSnackBar(binding.svStressPressure, error.localizedMessage)
                    TempUtil.log("Error", error.localizedMessage)
                }
            })
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.spStressPressure.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(parent: AdapterView<*>?) {
                    context?.toast("Nothing selected")
                }

                override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    id: Long
                ) {
                    binding.tvStressPressureResults.text = ""
                    binding.tvStressPressureRecom.text = ""
                    if (binding.tvStressPressureResults.isVisible) {
                        binding.ivStressResults.setImageBitmap(
                            rotateBitmap(
                                (binding.ivStressResults.drawable as BitmapDrawable).bitmap,
                                180
                            )
                        )
                        binding.tvStressPressureResults.toggleVisibility()
                    }
                    setIndicatorAndMeter(
                        completeResponse.data[position].indicators,
                        completeResponse.data[position].status
                    )
                    if (binding.tvStressPressureRecom.isVisible) {
                        binding.ivStressRecom.setImageBitmap(
                            rotateBitmap(
                                (binding.ivStressRecom.drawable as BitmapDrawable).bitmap,
                                180
                            )
                        )
                        binding.tvStressPressureRecom.toggleVisibility()
                    }
                    binding.tvContainerTwoContent.text =
                        completeResponse.data[position].genes_of_interest
                    binding.tvContainerThreeTitle.text = completeResponse.data[position].title
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        binding.tvContainerThreeContent.text = Html.fromHtml(
                            completeResponse.data[position].intro,
                            Html.FROM_HTML_MODE_COMPACT
                        )
                    } else {
                        binding.tvContainerThreeContent.text =
                            Html.fromHtml(completeResponse.data[position].intro)
                    }
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        completeResponse.data[position].output?.result?.let {
                            binding.tvStressPressureResults.text = Html.fromHtml(
                                it,
                                Html.FROM_HTML_MODE_COMPACT
                            )
                        }
                        completeResponse.data[position].output?.recommondation?.let {
                            binding.tvStressPressureRecom.text = Html.fromHtml(
                                it,
                                Html.FROM_HTML_MODE_COMPACT
                            )
                        }
                    } else {
                        completeResponse.data[position].output?.result?.let {
                            binding.tvStressPressureResults.text =
                                Html.fromHtml(it)
                        }
                        completeResponse.data[position].output?.recommondation?.let {
                            binding.tvStressPressureRecom.text =
                                Html.fromHtml(it)
                        }
                    }
                }
            }
    }

    private fun setIndicatorAndMeter(indicators: List<String>, value: String) {
        when (indicators.size) {
            2 -> {
                binding.tvSpeedIndicator1.text = indicators[0]
                binding.tvSpeedIndicator5.text = indicators[1]
                if (indicators.indexOf(value) == 0)
                    binding.tsmStress.setSpeedAt(0f)
                else
                    binding.tsmStress.setSpeedAt(10f)
                showAndHideOthers(1, 5)
            }
            3 -> {
                binding.tvSpeedIndicator1.text = indicators[0]
                binding.tvSpeedIndicator3.text = indicators[1]
                binding.tvSpeedIndicator5.text = indicators[2]
                when (indicators.indexOf(value)) {
                    0 -> {
                        binding.tsmStress.setSpeedAt(0f)
                    }
                    1 -> {
                        binding.tsmStress.setSpeedAt(5f)
                    }
                    2 -> {
                        binding.tsmStress.setSpeedAt(10f)
                    }
                }
                showAndHideOthers(1, 3, 5)
            }
            4 -> {
                binding.tvSpeedIndicator1.text = indicators[0]
                binding.tvSpeedIndicator2.text = indicators[1]
                binding.tvSpeedIndicator4.text = indicators[2]
                binding.tvSpeedIndicator5.text = indicators[3]
                when (indicators.indexOf(value)) {
                    0 -> {
                        binding.tsmStress.setSpeedAt(0f)
                    }
                    1 -> {
                        binding.tsmStress.setSpeedAt(2.5f)
                    }
                    2 -> {
                        binding.tsmStress.setSpeedAt(7.5f)
                    }
                    3 -> {
                        binding.tsmStress.setSpeedAt(10f)
                    }
                }
                showAndHideOthers(1, 2, 4, 5)
            }
            5 -> {
                binding.tvSpeedIndicator1.text = indicators[0]
                binding.tvSpeedIndicator2.text = indicators[1]
                binding.tvSpeedIndicator3.text = indicators[2]
                binding.tvSpeedIndicator4.text = indicators[3]
                binding.tvSpeedIndicator5.text = indicators[4]
                when (indicators.indexOf(value)) {
                    0 -> {
                        binding.tsmStress.setSpeedAt(0f)
                    }
                    1 -> {
                        binding.tsmStress.setSpeedAt(2.5f)
                    }
                    2 -> {
                        binding.tsmStress.setSpeedAt(5f)
                    }
                    3 -> {
                        binding.tsmStress.setSpeedAt(7.5f)
                    }
                    4 -> {
                        binding.tsmStress.setSpeedAt(10f)
                    }
                }
                showAndHideOthers(1, 2, 3, 4, 5)
            }
        }
    }

    private fun showAndHideOthers(vararg variables: Int) {
        val completeViewArray = arrayListOf(1, 2, 3, 4, 5)
        completeViewArray.removeAll(variables.toList())
        completeViewArray.forEach { item ->
            val id = resources.getIdentifier("tvSpeedIndicator" + item, "id", context?.packageName)
            //      (indicatorView.plus(item) as TextView).visibility = View.GONE
            view?.findViewById<TextView>(id)?.visibility = View.GONE
            //   binding.tvSpeedIndicator1.visibility = View.GONE
        }
        variables.forEach { item ->
            val id = resources.getIdentifier("tvSpeedIndicator" + item, "id", context?.packageName)
            view?.findViewById<TextView>(id)?.visibility = View.VISIBLE
        }
    }

    private fun rotateBitmap(bitmap: Bitmap, rotationAngleDegree: Int): Bitmap {

        val w = bitmap.width
        val h = bitmap.height

        var newW = w
        var newH = h
        if (rotationAngleDegree == 90 || rotationAngleDegree == 270) {
            newW = h
            newH = w
        }
        val rotatedBitmap = Bitmap.createBitmap(newW, newH, bitmap.config)
        val canvas = Canvas(rotatedBitmap)

        val rect = Rect(0, 0, newW, newH)
        val matrix = Matrix()
        val px = rect.exactCenterX()
        val py = rect.exactCenterY()
        matrix.postTranslate((-bitmap.width / 2).toFloat(), (-bitmap.height / 2).toFloat())
        matrix.postRotate(rotationAngleDegree.toFloat())
        matrix.postTranslate(px, py)
        canvas.drawBitmap(
            bitmap,
            matrix,
            Paint(Paint.ANTI_ALIAS_FLAG or Paint.DITHER_FLAG or Paint.FILTER_BITMAP_FLAG)
        )
        matrix.reset()

        return rotatedBitmap
    }


    fun View.toggleVisibility() {
        val view = this
        when (this.visibility) {
            View.VISIBLE -> {
                view.animate()
                    .translationY(0f)
                    .alpha(0.0f)
                    .setDuration(500)
                    .setListener(object : Animator.AnimatorListener {
                        override fun onAnimationRepeat(animation: Animator?) {

                        }

                        override fun onAnimationCancel(animation: Animator?) {
                        }

                        override fun onAnimationStart(animation: Animator?) {
                        }

                        override fun onAnimationEnd(animation: Animator?) {
                            // view.clearAnimation()
                            view.visibility = View.GONE
                        }
                    })
            }
            View.GONE -> {
                view.animate()
                    .translationY(5f)
                    .alpha(1.0f)
                    .setDuration(500)
                    .setListener(object : Animator.AnimatorListener {
                        override fun onAnimationRepeat(animation: Animator?) {

                        }

                        override fun onAnimationCancel(animation: Animator?) {
                        }

                        override fun onAnimationStart(animation: Animator?) {
                        }

                        override fun onAnimationEnd(animation: Animator?) {
                            //  view.clearAnimation()
                            if (!binding.tvStressPressureRecom.isVisible || binding.tvStressPressureResults.isVisible)
                                binding.svStressPressure.post {
                                    binding.svStressPressure.smoothScrollTo(
                                        0,
                                        (view.y + 50).toInt()
                                    )
                                }
                            view.visibility = View.VISIBLE
                        }
                    })
            }

        }
    }
}
