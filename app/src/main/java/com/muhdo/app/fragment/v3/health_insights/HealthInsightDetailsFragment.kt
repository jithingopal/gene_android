package com.muhdo.app.fragment.v3.health_insights

import android.annotation.SuppressLint
import android.content.Context
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import androidx.viewpager.widget.ViewPager
import com.muhdo.app.R
import com.muhdo.app.apiModel.v3.health_insight.HeaderValue
import com.muhdo.app.databinding.FragmentHealthInsightResultsBinding
import com.muhdo.app.databinding.V3FragmentHealthInsightResultBinding
import com.muhdo.app.fragment.v3.HealthInsightsResultItemFragment
import com.muhdo.app.fragment.v3.ResultChooseModeFragment
import com.muhdo.app.fragment.v3.epigenetics.EpigeneticsDetailsResultFragment
import com.muhdo.app.interfaces.v3.OnChangeInnerFragment
import com.muhdo.app.interfaces.v3.OnHealthInsightsHeaderValuesChanged
import com.muhdo.app.utils.v3.Constants


class HealthInsightDetailsFragment : Fragment(), OnHealthInsightsHeaderValuesChanged,OnChangeInnerFragment {

    lateinit var adapter:ViewPagerAdapter
    private var selectedInnerFragmentPosition:Int=-1
    private var selectedInnerFragment:Fragment?=null
    override fun onFragmentChanged(position: Int, fragment: Fragment) {
        selectedInnerFragment=fragment
        selectedInnerFragmentPosition=position
        setupTabIcons()
        binding.tabLayout.getTabAt(position)!!.select()

//        adapter.replaceFrag(fragment,position)
    }


    private var firstSelectPos = 0
    private var currentSelectedTabPostion = 0
    lateinit var binding: V3FragmentHealthInsightResultBinding
    private val tabLabelMap = createTabLabelMap()
    var mapperHeaderValue = mutableMapOf<Int, HeaderValue>()

    var tab_icons = arrayOf<Int>(
        R.drawable.v3_ic_health_insight_stress,
        R.drawable.v3_ic_health_insight_sleep,
        R.drawable.v3_ic_health_insight_ani_ageing,
        R.drawable.v3_ic_health_insight_injury_risk,
        //    R.drawable.heart_health_m,
        R.drawable.v3_ic_health_insight_gut_health,
        R.drawable.v3_ic_health_insight_mentel_health,
        //   R.drawable.addiction_m,
        //   R.drawable.skin_ageing_m,
        R.drawable.v3_ic_health_insight_skin_health,
        R.drawable.v3_ic_health_insight_eye_health,
        R.drawable.v3_ic_health_insight_muscle_health
    )


    var tab_label =
        arrayOf(
            R.string.stress,
            R.string.sleep,
            R.string.anti_aging,
            R.string.injury_prevention,
            //       R.string.heart_health,
            R.string.gut_health,
            R.string.mental_health,
            //   R.string.addiction,
            //   R.string.skin_aging,
            R.string.skin_health,
            R.string.eye_health,
            R.string.muscle_health
        )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.v3_fragment_health_insight_result, container, false)
        binding = V3FragmentHealthInsightResultBinding.bind(view)
        setupTabIcons()
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        selectTabItem(firstSelectPos)
    }


    private fun selectTabItem(position: Int) {
        binding.tabLayout.getTabAt(position)!!.select()
    }

    fun selectedTabPosition(position: Int) {
        firstSelectPos = position

    }

    private fun createTabLabelMap(): MutableMap<Int, String> {
        val mapper = mutableMapOf<Int, String>()
        mapper[R.string.stress] = Constants.HEALTH_INSIGHTS.STRESS
        mapper[R.string.sleep] = Constants.HEALTH_INSIGHTS.SLEEP
        mapper[R.string.anti_aging] = Constants.HEALTH_INSIGHTS.ANTI_AGEING
        mapper[R.string.injury_prevention] = Constants.HEALTH_INSIGHTS.INJURY_RISK
        //    mapper[R.string.heart_health] = Constants.HEALTH_INSIGHTS.HEART_HEALTH
        mapper[R.string.gut_health] = Constants.HEALTH_INSIGHTS.GUT_HEALTH
        mapper[R.string.mental_health] = Constants.HEALTH_INSIGHTS.MENTAL_HEALTH
        mapper[R.string.skin_health] = Constants.HEALTH_INSIGHTS.SKIN_HEALTH
        mapper[R.string.eye_health] = Constants.HEALTH_INSIGHTS.EYE_HEALTH
        mapper[R.string.muscle_health] = Constants.HEALTH_INSIGHTS.MUSCLE_HEALTH
        return mapper
    }

    private fun updateHeaderValuesMap(position: Int, value: HeaderValue) {
        mapperHeaderValue[position] = value
        if (mapperHeaderValue[currentSelectedTabPostion] != null) {
            updateHeaderInfo(
                getString(tab_label[currentSelectedTabPostion]),
                mapperHeaderValue[currentSelectedTabPostion]!!.infoText,
                mapperHeaderValue[currentSelectedTabPostion]!!.riskStatus,
                mapperHeaderValue[currentSelectedTabPostion]!!.riskIndicators,
                tab_icons[currentSelectedTabPostion]
            )

        }
    }

    private fun updateHeaderInfo(
        title: String?,
        infoText: String?,
        riskStatus: String?,
        riskIndicators: Int?,
        tabIndicaterRes: Int?
    ) {

        if (title != null) binding.tvHeaderTitle.setText(title)
        if (infoText != null) {
            if(infoText!!.length>36){
                binding.tvInfoText.setText(infoText.substring(0,33)+"...")
            }else
            {
                binding.tvInfoText.setText(infoText)
            }
        }
        if (riskStatus != null){
            if(riskStatus!!.length>22) {
                binding.tvRiskStatus.setText((riskStatus.substring(0,18)+"...").toUpperCase())
            }else
            {
                binding.tvRiskStatus.setText(
                    riskStatus.toUpperCase().replace(
                        "\n",
                        " "
                    )
                )
            }
        }
        if (riskIndicators != null) {
            if (riskIndicators<= 0) {
                binding.imgRiskIndicator.visibility = View.INVISIBLE
            } else {
                binding.imgRiskIndicator.visibility = View.VISIBLE
                if (riskIndicators == 5) {
                    binding.imgRiskIndicator.setImageResource(R.drawable.v3_ic_slider_green)

                } else if (riskIndicators == 4) {
                    binding.imgRiskIndicator.setImageResource(R.drawable.v3_ic_slider_half_green)

                } else if (riskIndicators == 3) {
                    binding.imgRiskIndicator.setImageResource(R.drawable.v3_ic_slider_yellow)

                } else if (riskIndicators == 2) {
                    binding.imgRiskIndicator.setImageResource(R.drawable.v3_ic_slider_half_red)

                } else if (riskIndicators == 1) {
                    binding.imgRiskIndicator.setImageResource(R.drawable.v3_ic_slider_red)
                }
            }

        }else{
            binding.imgRiskIndicator.visibility = View.INVISIBLE
        }
        if (tabIndicaterRes != null) {
            binding.imgTabInfo.setImageResource(tabIndicaterRes)
        }
    }

    private fun setupTabIcons() {
        binding.tabLayout.setupWithViewPager(binding.viewPager)
        adapter = ViewPagerAdapter(activity!!.supportFragmentManager)
        binding.viewPager.adapter = adapter
        binding.viewPager.offscreenPageLimit = 0
        var i = 0;
        for ((key, value) in tabLabelMap) {

                val fragment = HealthInsightsDetailsResultsFragment()
                fragment.onChangeInnerFragment=this
                val bundle = Bundle()
                bundle.putString("apiRoute", value)
                bundle.putInt("title", key)
                fragment.arguments = bundle
                fragment.tabPosition = i
                fragment.onHealthInsightsHeaderValuesChanged = this

            if(i==selectedInnerFragmentPosition&&selectedInnerFragment!=null){
                adapter.addFrag(selectedInnerFragment!!, getString(key))
            }
            else{
                adapter.addFrag(fragment, getString(key))
            }
            i++
        }

        binding.viewPager.adapter = adapter

        for (i in 0 until tab_icons.size) {
            val tabView = LayoutInflater.from(activity!!.applicationContext)
                .inflate(R.layout.v3_custom_tab_health_insights_details, null)
            val tabImage = tabView.findViewById(R.id.tab_img) as ImageView
            val tabText = tabView.findViewById(R.id.tabTitle) as TextView
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                tabText.setTextColor(context!!.getColor(R.color.v3_epi_gen_icon_unselected))
            } else {
                tabText.setTextColor(context!!.resources.getColor(R.color.v3_epi_gen_icon_unselected))

            }
            tabImage.setColorFilter(
                ContextCompat.getColor(
                    activity!!.applicationContext,
                    R.color.v3_epi_gen_icon_unselected
                ), android.graphics.PorterDuff.Mode.SRC_IN
            );
            tabImage.setImageResource(tab_icons[i])
            tabText.setText(tab_label[i])
            binding.tabLayout.getTabAt(i)!!.customView = tabView
        }
        val tabImage =
            binding.tabLayout.getTabAt(0)!!.customView!!.findViewById(R.id.tab_img) as ImageView
        val tabText =
            binding.tabLayout.getTabAt(0)!!.customView!!.findViewById(R.id.tabTitle) as TextView
        tabImage.setColorFilter(
            ContextCompat.getColor(
                activity!!.applicationContext,
                R.color.health_insight_colorPrimary
            ), android.graphics.PorterDuff.Mode.SRC_IN
        )
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            tabText.setTextColor(context!!.getColor(R.color.health_insight_colorPrimary))
        }else{
            tabText.setTextColor(context!!.resources.getColor(R.color.health_insight_colorPrimary))
        }
        binding.viewPager.addOnPageChangeListener((object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {

            }

            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {

            }

            override fun onPageSelected(position: Int) {

                for (i in 0 until tab_icons.size) {
                    val tabImage =
                        binding.tabLayout.getTabAt(i)!!.customView!!.findViewById(R.id.tab_img) as ImageView
                    val tabTextView =
                        binding.tabLayout.getTabAt(i)!!.customView!!.findViewById(R.id.tabTitle) as TextView
                    tabImage.setColorFilter(
                        ContextCompat.getColor(
                            activity!!.applicationContext,
                            R.color.v3_epi_gen_icon_unselected
                        ), android.graphics.PorterDuff.Mode.SRC_IN
                    )
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        tabTextView.setTextColor(context!!.getColor(R.color.v3_epi_gen_icon_unselected))
                    }else{
                        tabTextView.setTextColor(context!!.resources.getColor(R.color.v3_epi_gen_icon_unselected))

                    }
//                    setTabSelection(position)
                }

                val tabImage =
                    binding.tabLayout.getTabAt(position)!!.customView!!.findViewById(R.id.tab_img) as ImageView
                val tabText2 =
                    binding.tabLayout.getTabAt(position)!!.customView!!.findViewById(R.id.tabTitle) as TextView
                tabImage.setColorFilter(
                    ContextCompat.getColor(
                        activity!!.applicationContext,
                        R.color.health_insight_colorPrimary
                    ), android.graphics.PorterDuff.Mode.SRC_IN
                )
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    tabText2.setTextColor(context!!.getColor(R.color.health_insight_colorPrimary))
                }else{
                    tabText2.setTextColor(context!!.resources.getColor(R.color.health_insight_colorPrimary))

                }


                binding.nestedScrollView.isSmoothScrollingEnabled = true
                binding.nestedScrollView.fling(0)
                binding.nestedScrollView.smoothScrollTo(0, 0)

                if (mapperHeaderValue[position] != null) {
                    updateHeaderInfo(
                        getString(tab_label[position]),
                        mapperHeaderValue[position]!!.infoText,
                        mapperHeaderValue[position]!!.riskStatus,
                        mapperHeaderValue[position]!!.riskIndicators,
                        tab_icons[position]
                    )
                } else {
                    updateHeaderInfo(
                        getString(tab_label[position]),
                        "",
                        "",
                        null,
                        tab_icons[position]
                    )

                }

                currentSelectedTabPostion = position

            }

        }))


    }



    override fun headerValuesChanged(
        infoText: String?,
        riskStatus: String?,
        riskIndicators: Int?,
        tabPosition: Int
    ) {

        updateHeaderValuesMap(tabPosition, HeaderValue(infoText, riskStatus, riskIndicators))

    }

    inner class ViewPagerAdapter(manager: FragmentManager) :
        FragmentStatePagerAdapter(manager) {
        private val mFragmentList = arrayListOf<Fragment>()
        private val mFragmentTitleList = arrayListOf<String>()

        override fun getItem(position: Int): Fragment {

            return mFragmentList.get(position)
        }

        override fun getCount(): Int {
            return mFragmentList.size
        }

        fun addFrag(fragment: Fragment, title: String) {
            mFragmentList.add(fragment)
            mFragmentTitleList.add(title)
        }
        fun replaceFrag(fragment: Fragment,position: Int) {
            mFragmentList.set(position,fragment  )
            notifyDataSetChanged()
        }

        override fun getPageTitle(position: Int): CharSequence? {
            Log.d("data", "tab pos = " + position)
            return mFragmentTitleList[position]
        }
    }
}
