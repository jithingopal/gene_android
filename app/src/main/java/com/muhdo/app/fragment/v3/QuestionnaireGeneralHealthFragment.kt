package com.muhdo.app.fragment.v3

import android.annotation.SuppressLint
import android.app.Dialog
import android.os.Bundle
import android.text.Html
import android.util.Base64
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.google.gson.Gson
import com.muhdo.app.R
import com.muhdo.app.apiModel.v3.Answer
import com.muhdo.app.apiModel.v3.QuestionnaireAnswer
import com.muhdo.app.databinding.V3FragmentQuestionnaireHolderGeneralHealthBinding
import com.muhdo.app.repository.ApiUtilis
import com.muhdo.app.utils.v3.Constants
import com.muhdo.app.utils.v3.TempUtil
import com.muhdo.app.utils.v3.getSelectedRadioButtonText
import com.muhdo.app.utils.v3.showAlertDialog
import com.timingsystemkotlin.backuptimingsystem.Utility
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.v3_fragment_questionnaire_holder_general_health.view.*
import kotlinx.android.synthetic.main.v3_layout_general_health.*
import kotlinx.android.synthetic.main.v3_layout_general_health_2.*

class QuestionnaireGeneralHealthFragment : Fragment() {
    private var currentPage = 0

    lateinit var dialog: Dialog;
    internal lateinit var binding: V3FragmentQuestionnaireHolderGeneralHealthBinding

    //Questionnaire temp variables

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(
            R.layout.v3_fragment_questionnaire_holder_general_health,
            container,
            false
        )
        binding = V3FragmentQuestionnaireHolderGeneralHealthBinding.bind(view)
        addBottomDots(currentPage)
        setupDialogInfo()
        binding.btnNext.setOnClickListener {
            onNext()
        }
        binding.btnPrevious.setOnClickListener {
            onPrevious()
        }
        binding.layoutGeneralHealth1.layout_general_health_1_bind.findViewById<View>(R.id.btn_questionnaire_info)
            .setOnClickListener {
                onShowInfoDialog()
            }
        binding.layoutGeneralHealth2.layout_general_health_2.findViewById<View>(R.id.btn_questionnaire_info)
            .setOnClickListener {
                onShowInfoDialog()
            }
        return view

    }


    private fun setupDialogInfo(){
        dialog = Dialog(activity)
        val viewDialog = View.inflate(activity, R.layout.v3_dialog_info_questionnaire, null)
        val closeButton = viewDialog.findViewById<ImageView>(R.id.img_close)
        closeButton.setOnClickListener { dialog.dismiss() }
        dialog.setContentView(viewDialog)
        val tvTitle= viewDialog.findViewById<TextView>(R.id.tvTitle)
        val tvSubTitle= viewDialog.findViewById<TextView>(R.id.tvSubTitle)
        val tvDescription= viewDialog.findViewById<TextView>(R.id.tvDescription)
        tvTitle.setText(R.string.info_pane_text_general_health_title)
        tvSubTitle.setText(R.string.info_pane_text_general_health_sub_title)
        tvDescription.setText(R.string.info_pane_text_general_health_description)
        dialog.setCancelable(false)
    }
    private fun onShowInfoDialog() {
        if (!dialog.isShowing)
            dialog.show()
    }

    private fun onNext() {
        if (currentPage == 0) {
            if (rg_ill.checkedRadioButtonId != -1 && rg_ache_pain.checkedRadioButtonId != -1) {
                binding.btnPrevious.visibility = View.VISIBLE
                binding.layoutGeneralHealth1.visibility = View.GONE
                binding.layoutGeneralHealth2.visibility = View.VISIBLE
                currentPage = 1
            } else context?.showAlertDialog(getString(R.string.answer_all_question_message))
        } else if (currentPage == 1) {
            if (rg_week_general_health.checkedRadioButtonId == -1)
                context?.showAlertDialog(getString(R.string.answer_all_question_message))
            else {
                getAllAnswersOfQuestionnaire()
            }
        }
        addBottomDots(currentPage)
    }

    private fun getAllAnswersOfQuestionnaire() {
        val userId = Base64.decode(Utility.getUserID(activity!!.applicationContext), Base64.DEFAULT)
        val decodeUserId = String(userId, charset("UTF-8"))
        val answers = arrayListOf<Answer>()
        val regularIll = Answer(
            question = Constants.GENERAL_HEALTH.REGULARLY_ILL,
            answer = listOf(binding.layoutGeneralHealth1Bind.rgIll.getSelectedRadioButtonText())
        )
        answers.add(regularIll)
        val monthlyPain = Answer(
            question = Constants.GENERAL_HEALTH.MONTHLY_PAIN,
            answer = listOf(binding.layoutGeneralHealth1Bind.rgAchePain.getSelectedRadioButtonText())
        )
        answers.add(monthlyPain)
        val achieveGoal = Answer(
            question = Constants.GENERAL_HEALTH.ACHIEVE_GOAL,
            answer = listOf(
                Constants.getWeekConstantValue(binding.layoutGeneralHealth2Bind.rgWeekGeneralHealth.getSelectedRadioButtonText())
            )
        )
        answers.add(achieveGoal)
        val requestBody = QuestionnaireAnswer(
            user_id = decodeUserId,
            answers = answers,
            category = Constants.QUESTIONNAIRE.GENERAL_HEALTH
        )
        TempUtil.log(
            "General Health answers ", Gson().toJson(requestBody) +
                    binding.layoutGeneralHealth1Bind.rgIll.getSelectedRadioButtonText() + binding.layoutGeneralHealth1Bind.rgAchePain.getSelectedRadioButtonText()
                    + binding.layoutGeneralHealth2Bind.rgWeekGeneralHealth.getSelectedRadioButtonText()
        )
        uploadAnswers(requestBody)
    }

    private fun uploadAnswers(requestBody: QuestionnaireAnswer) {
        binding.progressBar.visibility = View.VISIBLE
        ApiUtilis.getAPIInstance(activity!!).postReviewQuestionnaireAnswers(requestBody)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ success ->
                run {
                    binding.progressBar.visibility = View.GONE
                    Utility.displayShortSnackBar(binding.root, success.message)
                    // Log.d("response from Exercise", success.message)
                    replaceFragment(QuestionnaireHomeFragment())
                    TempUtil.isLifestyleQuestionnaireCompleted = true
                }
            }, { error ->
                run {
                    binding.progressBar.visibility = View.GONE
                    Utility.displayShortSnackBar(binding.root, error.localizedMessage)
                    // Log.d("response from Exercise ", error.localizedMessage)
                }
            })
    }

    @SuppressLint("WrongConstant")
    private fun replaceFragment(fragment: Fragment) {
        val transaction = activity!!.supportFragmentManager.beginTransaction()
        transaction.replace(R.id.flContent, fragment)
        transaction.addToBackStack("share")
        transaction.commit()

    }

    private fun onPrevious() {

        if (currentPage == 0) {

            binding.btnPrevious.visibility = View.GONE
            currentPage = 0

        } else if (currentPage == 1) {
            binding.btnPrevious.visibility = View.GONE

            binding.layoutGeneralHealth1.visibility = View.VISIBLE
            binding.layoutGeneralHealth2.visibility = View.GONE
            currentPage = 0
        }
        addBottomDots(currentPage)

    }

    private fun addBottomDots(currentPage: Int) {


        val dots = arrayOfNulls<TextView>(2)
        binding.layoutDots.removeAllViews()
        for (i in dots.indices) {

            dots[i] = TextView(activity)
            dots[i]!!.text = Html.fromHtml("&#8226;")
            dots[i]!!.textSize = 35f
            dots[i]!!.setTextColor(
                ContextCompat.getColor(
                    activity!!.applicationContext,
                    R.color.secondary_color
                )
            )
            binding.layoutDots.addView(dots[i])
        }

        if (dots.isNotEmpty()) {
            dots[currentPage]!!.setTextColor(
                ContextCompat.getColor(
                    activity!!.applicationContext,
                    R.color.color_dark_blue
                )
            )
        }
    }

}
