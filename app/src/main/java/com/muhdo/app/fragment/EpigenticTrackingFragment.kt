package com.muhdo.app.fragment

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.RelativeLayout
import androidx.fragment.app.Fragment
import com.muhdo.app.R
import com.muhdo.app.databinding.FragmentEpigenticTrackingBinding
import com.muhdo.app.interfaces.ItemClickListener
import com.muhdo.app.model.ResultGridModel
import com.muhdo.app.ui.epigentic.execrise.EpigenticExecriseActivity
import com.muhdo.app.ui.epigentic.food.EpigenticFoodNewActivity
import com.muhdo.app.ui.epigentic.lifestyle.EpigenticLifeStyleActivity
import com.muhdo.app.ui.epigentic.medication.MedicationActivity
import com.muhdo.app.ui.epigentic.supplyments.EpigenticSupplementActivity
import kotlinx.android.synthetic.main.list_results.view.*


class EpigenticTrackingFragment: Fragment() , ItemClickListener {

    companion object {
        lateinit var instance: EpigenticTrackingFragment
            private set
    }
    override fun onClick(position: Int) {
//
        when (position) {
            0->{
                startActivity(Intent(activity, EpigenticFoodNewActivity::class.java))
            }
            1->{
               // startActivity(Intent(activity, EpigenticExecriseActivity::class.java))
                val i = Intent(activity, EpigenticExecriseActivity::class.java)
                i.putExtra("helloString", "")
                this.startActivityForResult(i, 100)
            }
            2->{
                startActivity(Intent(activity, EpigenticSupplementActivity::class.java))
            }
            3->{
                startActivity(Intent(activity, MedicationActivity::class.java))
            }
        }

    }

    var adapter: ResultAdapter? = null
    var thanksLayout: RelativeLayout? = null
    var resultList = ArrayList<ResultGridModel>()
    internal lateinit var binding: FragmentEpigenticTrackingBinding
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_epigentic_tracking, container, false)
        binding = FragmentEpigenticTrackingBinding.bind(view)
        thanksLayout=view.findViewById(R.id.layout_thank);
        setResultGrid()
        setupViews()
        thanksLayoutClose()
        return view
    }

    private fun thanksLayoutClose() {

        binding.layoutClose.setOnClickListener(object :View.OnClickListener{
            override fun onClick(v: View?) {
                binding.layoutThank.visibility=View.GONE
            }

        })

    }


    private fun setResultGrid() {
        // load foods
        resultList.add(ResultGridModel(activity!!.getString(R.string.food), R.drawable.food_img))
        resultList.add(ResultGridModel(activity!!.getString(R.string.exercise), R.drawable.exercise_img))
        resultList.add(
            ResultGridModel(activity!!.getString(R.string.supplyments),
                R.drawable.supplements_img)
        )
        resultList.add(ResultGridModel(activity!!.getString(R.string.medication), R.drawable.medication_img))

        adapter = ResultAdapter(activity!!, resultList, this)

        binding.resultsGrid.adapter = adapter
    }

    private fun setupViews() {
          binding.imgResults.setOnClickListener {
              val i = Intent(activity, EpigenticLifeStyleActivity::class.java)
              this.startActivityForResult(i, 100)
          }
    }



    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(resultCode==100){
          // Toast.makeText(activity, "select"   , Toast.LENGTH_SHORT).show();
            binding.layoutThank.visibility=View.VISIBLE
        }else{
            binding.layoutThank.visibility=View.GONE
        }

    }



    class ResultAdapter(context: Context, private var resultList: ArrayList<ResultGridModel>, private val listener: ItemClickListener) : BaseAdapter() {
        var context: Context? = context

        override fun getCount(): Int {
            return resultList.size
        }

        override fun getItem(position: Int): Any {
            return resultList[position]
        }

        override fun getItemId(position: Int): Long {
            return position.toLong()
        }

        override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
            val food = this.resultList[position]

            val inflator = context!!.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            val foodView = inflator.inflate(R.layout.list_results, null)
            foodView.img_results.setImageResource(food.image!!)
            Log.wtf("data","plan"+food.name)
            /*if(food.name.equals("News Feed")){
                foodView.txt_result_title.setTextColor(Color.parseColor("#000000"))
            }*/
            foodView.txt_result_title.text = food.name!!
            foodView.img_results.setOnClickListener {
                //            when (food.name) {
//                context!!.resources.getString(R.string.result_my_meal) -> {
                listener.onClick(position)
//                }
//            }
            }
            return foodView
        }
    }




}