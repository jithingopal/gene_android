package com.muhdo.app.fragment.v3

import android.app.AlertDialog
import android.os.Bundle
import android.util.Base64
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.viewpager.widget.ViewPager
import com.muhdo.app.R
import com.muhdo.app.adapter.MyPagerAdapter
import com.muhdo.app.apiModel.v3.UserIdReq
import com.muhdo.app.apiModel.v3.workoutplan.workout_overview.DeletePlanResult
import com.muhdo.app.apiModel.workout.WorkoutPlanModel
import com.muhdo.app.databinding.FragmentWorkoutV3Binding
import com.muhdo.app.fragment.FavouriteWorkoutFragment
import com.muhdo.app.fragment.MyPlanFragment
import com.muhdo.app.fragment.v3.guide.workout.ChooseGoalFragment
import com.muhdo.app.fragment.v3.guide.workout.NoWorkoutPlanFoundFragment
import com.muhdo.app.fragment.v3.guide.workout.WorkoutOverviewFragment
import com.muhdo.app.repository.ApiUtilis
import com.muhdo.app.repository.NetworkManager2
import com.muhdo.app.ui.DashboardActivity
import com.muhdo.app.utils.v3.TempUtil
import com.timingsystemkotlin.backuptimingsystem.Utility
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class WorkoutFragment : Fragment() {
    private lateinit var binding: FragmentWorkoutV3Binding
    internal var data: WorkoutPlanModel? = null
    var selectedPostion = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_workout_v3, container, false)
        binding = FragmentWorkoutV3Binding.bind(view)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //  val fragmentAdapter = MyPagerAdapter((context as AppCompatActivity).supportFragmentManager)
        val fragmentAdapter = MyPagerAdapter(childFragmentManager)
        fragmentAdapter.activity = activity!!
        binding.viewpagerMain.adapter = fragmentAdapter
        binding.tabsMain.setupWithViewPager(binding.viewpagerMain)
        binding.viewpagerMain.offscreenPageLimit = 0

        binding.viewpagerMain.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
                when (position) {
                    0 -> {
                        if (Utility.getWorkoutStatus(activity!!.applicationContext)) {
                            MyPlanFragment()
                        } else {
                            NoWorkoutPlanFoundFragment()
                        }

                    }
                    1 -> {
                        WorkoutOverviewFragment()
                    }
                    2 -> {
                        MyProfileFragment()
                    }
                    3 -> {
                        WarmUpCoolDownFragment()
                    }
                    4 -> {
                        InjuryPreventionFragment()
                    }
                    else -> {
                        FavouriteWorkoutFragment()

                    }
                }
            }

            override fun onPageSelected(position: Int) {

            }

            override fun onPageScrollStateChanged(state: Int) {

            }
        })
        binding.indicator.setViewPager(binding.viewpagerMain)
        binding.viewpagerMain.setCurrentItem(selectedPostion)

    }

    private fun displayAlert(message: String?) {
        val dialogBuilder = AlertDialog.Builder(context)
        dialogBuilder.setMessage(message)
            .setCancelable(false)
            .setPositiveButton("Ok") { _, _ ->
                // val i = Intent(requireContext(), DashboardActivity::class.java)
                // startActivity(i)
            }
        val alert = dialogBuilder.create()
        alert.show()
    }


    override fun onPause() {
        super.onPause()
        val dashBoardActivity: DashboardActivity = activity as DashboardActivity
        dashBoardActivity.setResurveyIconVisible(false, View.OnClickListener { })
    }

    override fun onResume() {
        super.onResume()
        val dashBoardActivity: DashboardActivity = activity as DashboardActivity
        dashBoardActivity.setResurveyIconVisible(true, View.OnClickListener { confirmResurvey() })

    }

    private fun confirmResurvey() {
        val dashBoardActivity: DashboardActivity = activity as DashboardActivity
        val dialogBuilder = AlertDialog.Builder(activity)
        dialogBuilder.setMessage(activity?.resources?.getString(R.string.v3_update_workout_survey))
            .setCancelable(false)
            .setPositiveButton(activity?.resources?.getString(R.string.option_yes)) { _, _ ->
                deleteWorkoutPlanFromServer()

            }
            .setNegativeButton(activity?.resources?.getString(R.string.option_no)) { _, _ ->
            }

        val alert = dialogBuilder.create()
        alert.show()
    }

    private fun deleteWorkoutPlanFromServer() {
        val dashBoardActivity: DashboardActivity = activity as DashboardActivity
        val userId = Base64.decode(
            Utility.getUserID(activity!!.applicationContext),
            Base64.DEFAULT
        )
        binding.progressBar.visibility = View.VISIBLE
        var jsonParams: JSONObject = JSONObject()
        jsonParams.put("user_id", Utility.getUserID(activity!!.applicationContext))
        val requestBody = UserIdReq(
            Utility.getUserID(activity!!.applicationContext)
        )
        TempUtil.log("Jithin", jsonParams.toString())
        val manager = NetworkManager2()
        if (manager.isConnectingToInternet(binding.parentLayout.context)) {
            val call =
                ApiUtilis.getAPIInstance(activity!!)
                    .deleteWorkoutPlan(requestBody)
            call.enqueue(object : Callback<DeletePlanResult> {
                override fun onResponse(
                    call: Call<DeletePlanResult>,
                    response: Response<DeletePlanResult>
                ) {
                    binding.progressBar.visibility = View.GONE
                    var resultPojo: DeletePlanResult? = response.body()
                    try {
                        if (resultPojo != null && resultPojo!!.statusCode == 200) {
                            Utility.setWorkoutStatus(activity!!.applicationContext, false)
                            dashBoardActivity.setResurveyIconVisible(
                                false,
                                View.OnClickListener { })
                            dashBoardActivity.replaceFragment(ChooseGoalFragment())
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }

                override fun onFailure(call: Call<DeletePlanResult>, t: Throwable) {
                    // Log error here since request failed
                    binding.progressBar.visibility = View.GONE
                    TempUtil.log("Login", t.toString())

                    Utility.displayShortSnackBar(
                        binding.parentLayout,
                        "Server connection error!!, please try again later."
                    )


                }
            })
        } else {
            binding.progressBar.visibility = View.GONE

            Utility.displayShortSnackBar(
                binding.parentLayout,
                binding.parentLayout.context.resources.getString(R.string.check_internet)
            )

        }

    }

}