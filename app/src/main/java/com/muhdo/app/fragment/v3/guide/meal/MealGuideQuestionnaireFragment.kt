package com.muhdo.app.fragment.v3.guide.meal

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.muhdo.app.R
import com.muhdo.app.apiModel.mealModel.AddMealData
import com.muhdo.app.apiModel.mealModel.AddMealResponse
import com.muhdo.app.apiModel.mealModel.MealPlanGenResponse
import com.muhdo.app.apiModel.v3.MealGuideSurveyRequest
import com.muhdo.app.databinding.V3FragmentWorkoutGuideQuestionnaireBinding
import com.muhdo.app.fragment.v3.guide.meal.choose_meal.ChooseMealFragment
import com.muhdo.app.fragment.v3.guide.workout.ChooseGoalFragment
import com.muhdo.app.fragment.v3.guide.workout.ChooseSubGoalFragment
import com.muhdo.app.repository.ApiUtilis
import com.muhdo.app.repository.ErrorModel
import com.muhdo.app.repository.NetworkManager
import com.muhdo.app.repository.ServiceListener
import com.muhdo.app.utils.PreferenceConnector
import com.muhdo.app.utils.v3.TempUtil
import com.muhdo.app.utils.v3.showAlertDialog
import com.timingsystemkotlin.backuptimingsystem.Utility

class MealGuideQuestionnaireFragment : Fragment() {
    var chooseActivityLevelFragment: ChooseActivityLevelFragment = ChooseActivityLevelFragment()
    var chooseHowManyMealsFragment: ChooseHowManyMealsFragment = ChooseHowManyMealsFragment()
    var chooseSubGoalFragment: ChooseSubGoalFragment = ChooseSubGoalFragment()
    var isNeedToSkipSubGoal = false
    lateinit var binding: V3FragmentWorkoutGuideQuestionnaireBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view =
            inflater.inflate(R.layout.v3_fragment_workout_guide_questionnaire, container, false)
        binding = V3FragmentWorkoutGuideQuestionnaireBinding.bind(view)
        setupViewPager()
        setupButtons()
        return view
    }

    fun setupButtons() {
        binding.btnNext.setOnClickListener {
            when (binding.viewPager.currentItem) {
                0 -> {
                    if (chooseSubGoalFragment.getSelectedSubGoal() != null) {
                        binding.viewPager.setCurrentItem(1)
                    } else {
                        context?.showAlertDialog(getString(R.string.please_choose_sub_goal))
                    }

                }

                1 -> {
                    if (chooseHowManyMealsFragment.getSelectedMealCount() != null) {
                        binding.viewPager.setCurrentItem(2)
                    } else {
                        context?.showAlertDialog(getString(R.string.please_choose_date_and_time_for_workout))
                    }

                }

                2 -> {
                    if (chooseActivityLevelFragment.getActivitylevel() != null) {
                        sendDataToServer()
                    } else {
                        context?.showAlertDialog(getString(R.string.please_choose_equipment1))
                    }


                }

            }

        }
        binding.btnPrevious.setOnClickListener {
            val chooseGoalFragment = ChooseGoalFragment()
            chooseGoalFragment.setGoToFragment(MealGuideQuestionnaireFragment())
            if (isNeedToSkipSubGoal && binding.viewPager.currentItem == 1) {
                replaceFragment(chooseGoalFragment)
            } else if (binding.viewPager.currentItem > 0) {
                try {
                    binding.viewPager.setCurrentItem(binding.viewPager.currentItem - 1)
                } catch (e: java.lang.Exception) {
                    e.printStackTrace()
                }
            } else {
                replaceFragment(chooseGoalFragment)
            }
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)

    }

    fun setupViewPager() {

        val id: String? = PreferenceConnector.readString(
            context!!,
            PreferenceConnector.MODE_ID,
            "5c8c96a79c54381add6884da"
        )
        if (id.equals("5c8c96a79c54381add6884db")) {
            isNeedToSkipSubGoal = false
        } else {
            isNeedToSkipSubGoal = true
        }
        val adapter = ViewPagerAdapter(activity!!.supportFragmentManager)

        adapter.addFrag(chooseSubGoalFragment)
        adapter.addFrag(chooseHowManyMealsFragment)
        adapter.addFrag(chooseActivityLevelFragment)

        binding.viewPager.adapter = adapter
        adapter.notifyDataSetChanged()
        binding.indicator.setViewPager(binding.viewPager)
        val density = resources.displayMetrics.density
        binding.indicator.setRadius(4 * density)
        if (isNeedToSkipSubGoal) {
            binding.viewPager.setCurrentItem(1)
        }
    }

    internal inner class ViewPagerAdapter(manager: FragmentManager) :
        FragmentStatePagerAdapter(manager) {
        private val mFragmentList = arrayListOf<Fragment>()
        private val mFragmentTitleList = arrayListOf<String>()

        override fun getItem(position: Int): Fragment {


            return mFragmentList.get(position)
        }

        override fun getCount(): Int {
            return mFragmentList.size
        }

        fun addFrag(fragment: Fragment/*, title: String*/) {
            mFragmentList.add(fragment)
            mFragmentTitleList.add(""/*title*/)
        }

        override fun getPageTitle(position: Int): CharSequence? {

            return mFragmentTitleList[position]
        }
    }

    fun replaceFragment(fragment: Fragment) {
        val transaction = activity?.supportFragmentManager?.beginTransaction()
        transaction?.replace(R.id.flContent, fragment)
        transaction?.addToBackStack("share")
        transaction?.commit()
    }


    /*-------------------Same code from old workout--------------------------*/


    private fun sendDataToServer() {
        binding.progressBar.visibility = View.VISIBLE
        val manager = NetworkManager()
        var goal: String? = ""
        if (isNeedToSkipSubGoal) {
            val goalId = getContext()?.let { it1 ->
                PreferenceConnector.readString(
                    it1,
                    PreferenceConnector.MODE_ID,
                    ""
                )
            }
            if (goalId.equals("5c8c96a79c54381add6884da"))//fitness
            {
                goal = "D"
            } else if (goalId.equals("5c8c96a79c54381add6884db"))//build muscle
            {
                goal = chooseSubGoalFragment.getSelectedSubGoal()

            } else if (goalId.equals("5c8c96a79c54381add6884dc"))//fat loss
            {
                goal = "B"
            } else if (goalId.equals("5c8c96a79c54381add6884dd"))//Health and wellbeing
            {
                goal = "E"
            }
        } else {
            goal = chooseSubGoalFragment.getSelectedSubGoal()
        }
        val paramsMealGuide = MealGuideSurveyRequest(
            goal,
            chooseHowManyMealsFragment.getSelectedMealCount(),
            chooseActivityLevelFragment.getActivitylevel(),
            Utility.getUserID(activity!!.applicationContext), "meal"
        )
        if (manager.isConnectingToInternet(activity!!.applicationContext)) {
            manager.createApiRequest(

                ApiUtilis.getAPIInstance(activity!!).sendMealSurveyV3(
                    paramsMealGuide
                ),
                object : ServiceListener<AddMealResponse> {
                    override fun getServerResponse(response: AddMealResponse, requestcode: Int) {
                        binding.progressBar.visibility = View.GONE
                        try {
                            if (response != null) {
                                if (response.getCode() == 200) {
                                    if (response.data != null) {
                                        binding.progressBar.visibility = View.VISIBLE
                                        callMealPlanSecondApi(response.data!!)
                                    }

                                }
                            }
                        } catch (e: Exception) {
                            TempUtil.log("data", "" + e.printStackTrace())
                        }
                    }

                    override fun getError(error: ErrorModel, requestcode: Int) {
                        binding.progressBar.visibility = View.GONE
                        Utility.displayShortSnackBar(
                            binding.parentLayout,
                            error.getMessage().toString()
                        )
                    }
                })
        } else {
            binding.progressBar.visibility = View.GONE
            Utility.displayShortSnackBar(
                binding.parentLayout,
                resources.getString(R.string.check_internet)
            )
        }
    }

    private fun callMealPlanSecondApi(

        requestParams: AddMealData

    ) {
        val manager = NetworkManager()
        if (manager.isConnectingToInternet(activity!!.applicationContext)) {
            manager.createApiRequest(
                ApiUtilis.getAPIInstance(activity!!).sendMealPlanResponse(
                    requestParams
                ),
                object : ServiceListener<MealPlanGenResponse> {
                    override fun getServerResponse(
                        response: MealPlanGenResponse,
                        requestcode: Int
                    ) {
                        try {
                            if (response != null) {
                                if (response.getCode() == 200) {
                                    if (response.getData() != null) {
                                        binding.progressBar.visibility = View.GONE
                                        replaceFragment(ChooseMealFragment(0))
                                        /*val i = Intent(activity, ChooseMealActivity::class.java)
                                        startActivity(i)*/
                                        Utility.setMealStatus(requireContext(), true)
                                    }

                                }
                            }
                        } catch (e: Exception) {

                        }
                        binding.progressBar.visibility = View.GONE
                        val mealData = Utility.getMealData(activity!!)

                       /* val i = Intent(activity, ChooseMealActivity::class.java)
                        startActivity(i)*/
                        replaceFragment(ChooseMealFragment(0))

                        Utility.setMealStatus(requireContext(), true)
                    }

                    override fun getError(error: ErrorModel, requestcode: Int) {
                        binding.progressBar.visibility = View.GONE
                        Utility.displayShortSnackBar(
                            binding.parentLayout,
                            error.getMessage().toString()
                        )
                    }
                })
        } else {
            binding.progressBar.visibility = View.GONE
            Utility.displayShortSnackBar(
                binding.parentLayout,
                resources.getString(R.string.check_internet)
            )
        }

    }
    /*------------------- Same code from old workout --------------------------*/
}
