package com.muhdo.app.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ExpandableListAdapter
import androidx.fragment.app.Fragment
import com.google.gson.Gson
import com.muhdo.app.R
import com.muhdo.app.adapter.CustomExpandableListAdapter
import com.muhdo.app.apiModel.mealModel.daysModel.UpdateFavoriteModel
import com.muhdo.app.apiModel.workout.Excerises
import com.muhdo.app.databinding.FragmentWorkoutBinding
import com.muhdo.app.repository.ApiUtilis
import com.muhdo.app.repository.ErrorModel
import com.muhdo.app.repository.NetworkManager
import com.muhdo.app.repository.ServiceListener
import com.muhdo.app.utils.v3.Constants
import com.timingsystemkotlin.backuptimingsystem.Utility
import kotlinx.android.synthetic.main.list_breakfast.view.*
import java.util.*

class WorkoutFragent : Fragment() {
    internal lateinit var binding: FragmentWorkoutBinding
    internal var adapter: ExpandableListAdapter? = null
    internal var data: Excerises? = null
    private var weekDay = ""
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_workout, container, false)
        binding = FragmentWorkoutBinding.bind(view)
        return view
    }


    override fun onResume() {
        super.onResume()
        val gson = Gson()
        val data1 = this.arguments!!.getString(Constants.DAYS_WORKOUT_MESSAGE)
        data = gson.fromJson<Excerises>(data1, Excerises::class.java)
        weekDay = this.arguments!!.getString(Constants.WORKOUT_DAY)!!
        data?.let {
            when (weekDay) {
                requireActivity().resources.getString(R.string.day_monday) -> {
                    adapter = CustomExpandableListAdapter(this, it.get1())
                }
                requireActivity().resources.getString(R.string.day_tuesday) -> {
                    adapter = CustomExpandableListAdapter(this, it.get2())
                }
                requireActivity().resources.getString(R.string.day_wednesday) -> {
                    adapter = CustomExpandableListAdapter(this, it.get3())
                }
                requireActivity().resources.getString(R.string.day_thursday) -> {
                    adapter = CustomExpandableListAdapter(this, it.get4())
                }
                requireActivity().resources.getString(R.string.day_friday) -> {
                    adapter = CustomExpandableListAdapter(this, it.get5())
                }
                requireActivity().resources.getString(R.string.day_saturday) -> {
                    adapter = CustomExpandableListAdapter(this, it.get6())
                }
                requireActivity().resources.getString(R.string.day_sunday) -> {
                    adapter = CustomExpandableListAdapter(this, it.get7())
                }
            }
            binding.list.setAdapter(adapter)
        }

    }


    fun updateFavorite(id: String, view: View, status: Boolean) {
        val manager = NetworkManager()
        val params = HashMap<String, String>()
        params["exercise_id"] = id
        params["is_favourite"] = status.toString()
        params["user_id"] = Utility.getUserID(requireContext())



        if (manager.isConnectingToInternet(activity!!.applicationContext)) {
            manager.createApiRequest(
                ApiUtilis.getAPIInstance(activity!!).updateFavouriteWorkout(params),
                object : ServiceListener<UpdateFavoriteModel> {
                    override fun getServerResponse(
                        response: UpdateFavoriteModel,
                        requestcode: Int
                    ) {
                        view.isClickable = true
                        if (status) {
                            Utility.favouriteIdList.add(id)
                            if (Utility.unFavouriteIdList.contains(id)) {
                                Utility.unFavouriteIdList.remove(id)
                            }
                        } else if (!status) {
                            Utility.unFavouriteIdList.add(id)
                            if (Utility.favouriteIdList.contains(id)) {
                                Utility.favouriteIdList.remove(id)
                            }
                        }

                        binding.list.invalidateViews()
                    }

                    override fun getError(error: ErrorModel, requestcode: Int) {
                        if (status) {
                            view.check_favourite.isChecked = false
                        } else if (!status) {
                            view.check_favourite.isChecked = true
                        }
                        Utility.displayShortSnackBar(
                            binding.parentLayout,
                            error.getMessage().toString()
                        )

                    }
                })
        } else {

            Utility.displayShortSnackBar(
                binding.parentLayout,
                activity!!.resources.getString(R.string.check_internet)
            )
        }
    }

}