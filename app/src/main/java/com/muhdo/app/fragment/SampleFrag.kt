package com.muhdo.app.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import com.muhdo.app.R
import com.muhdo.app.adapter.ResultAdapter
import com.muhdo.app.interfaces.ItemClickListener
import com.muhdo.app.model.ResultGridModel
import kotlinx.android.synthetic.main.fragment_sample.*


class SampleFrag : Fragment(), ItemClickListener {
    override fun onClick(position: Int) {

    }

    var adapter: ResultAdapter? = null
    //    var response: AirIndexData? = null
    private var resultList = ArrayList<ResultGridModel>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_sample, container, false)

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setResultGrid()
    }

    private fun setResultGrid() {

        results_grid.layoutManager = GridLayoutManager(requireActivity(), 2)
        // load foods
        results_grid.layoutManager = GridLayoutManager(requireActivity(), 2)
        resultList.add(ResultGridModel(activity!!.getString(R.string.epigenetic_food), R.drawable.food_epigenetic))
        resultList.add(
            ResultGridModel(
                activity!!.getString(R.string.epigenetic_exercise),
                R.drawable.exercise_epigenetic
            )
        )
        resultList.add(
            ResultGridModel(
                activity!!.getString(R.string.epigenetic_supplements),
                R.drawable.supplements_img
            )
        )
        resultList.add(
            ResultGridModel(
                activity!!.getString(R.string.epigenetic_medication),
                R.drawable.medication_epigenetic
            )
        )
        adapter = ResultAdapter(activity!!, resultList, this)

        //results_grid.adapter = adapter
    }
}