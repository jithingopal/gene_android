package com.muhdo.app.fragment

import android.annotation.SuppressLint
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ExpandableListAdapter
import android.widget.RadioButton
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import androidx.viewpager.widget.ViewPager
import com.google.gson.Gson
import com.muhdo.app.R
import com.muhdo.app.apiModel.workout.WorkoutPlanData
import com.muhdo.app.databinding.FragmentDisplayWorkoutBinding
import com.muhdo.app.utils.v3.Constants

class DisplayWorkoutFragment : Fragment() {
    internal lateinit var binding: FragmentDisplayWorkoutBinding
    internal var adapter: ExpandableListAdapter? = null
    var list: MutableList<String> = arrayListOf()
    internal var data: WorkoutPlanData? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val view = inflater.inflate(R.layout.fragment_display_workout, container, false)
        binding = FragmentDisplayWorkoutBinding.bind(view)

        for (i in 0..6) {
            list.add("off")
        }

        val gson = Gson()
        val data1 = this.arguments!!.getString(Constants.WEEKLY_WORKOUT_MESSAGE)
        data = gson.fromJson<WorkoutPlanData>(data1, WorkoutPlanData::class.java)
        for (i in 0 until data!!.getWeekDays()!!.size) {

            when ((data!!.getWeekDays()!![i]) - 1) {
                0 -> {
                    list[0] = "Mo"
                }
                1 -> {
                    list[1] = "Tu"
                }
                2 -> {
                    list[2] = "We"
                }
                3 -> {
                    list[3] = "Th"
                }
                4 -> {
                    list[4] = "Fr"
                }
                5 -> {
                    list[5] = "Sa"
                }
                6 -> {
                    list[6] = "Su"
                }

            }
        }
        val fragmentAdapter = ViewPagerAdapter(activity!!.supportFragmentManager)
        (binding.viewPager as ViewPager).adapter = fragmentAdapter
        binding.daysTabLayout.setupWithViewPager( binding.viewPager as ViewPager)
        setupViewPager(binding.viewPager as ViewPager)
        setupTabIcons()

        return view
    }

    @SuppressLint("InflateParams")
    private fun setupTabIcons() {

        val tabOne = LayoutInflater.from(activity).inflate(R.layout.custom_tab_workout, null) as RadioButton
        tabOne.text = list[0]
        if (list[0] == "off") {
            tabOne.setTextColor(Color.parseColor("#0097C1"))
            tabOne.isChecked = false
        }

        binding.daysTabLayout.getTabAt(0)!!.customView = tabOne

        val tabTwo = LayoutInflater.from(activity).inflate(R.layout.custom_tab_workout, null) as RadioButton
        tabTwo.text = list[1]
        if (list[1] == "off") {
            tabTwo.setTextColor(Color.parseColor("#0097C1"))
            tabTwo.isChecked = false
        }
        binding.daysTabLayout.getTabAt(1)!!.customView = tabTwo

        val tabThree = LayoutInflater.from(activity).inflate(R.layout.custom_tab_workout, null) as RadioButton
        tabThree.text = list[2]
        if (list[2] == "off") {
            tabThree.setTextColor(Color.parseColor("#0097C1"))
            tabThree.isChecked = false
        }
        binding.daysTabLayout.getTabAt(2)!!.customView = tabThree

        val tabFour = LayoutInflater.from(activity).inflate(R.layout.custom_tab_workout, null) as RadioButton
        tabFour.text = list[3]
        if (list[3] == "off") {
            tabFour.setTextColor(Color.parseColor("#0097C1"))
            tabFour.isChecked = false
        }
        binding.daysTabLayout.getTabAt(3)!!.customView = tabFour

        val tabFive = LayoutInflater.from(activity).inflate(R.layout.custom_tab_workout, null) as RadioButton
        tabFive.text = list[4]
        if (list[4] == "off") {
            tabFive.setTextColor(Color.parseColor("#0097C1"))
            tabFive.isChecked = false
        }
        binding.daysTabLayout.getTabAt(4)!!.customView = tabFive

        val tabSix = LayoutInflater.from(activity).inflate(R.layout.custom_tab_workout, null) as RadioButton
        tabSix.text = list[5]
        if (list[5] == "off") {
            tabSix.setTextColor(Color.parseColor("#0097C1"))
            tabSix.isChecked = false
        }
        binding.daysTabLayout.getTabAt(5)!!.customView = tabSix

        val tabSeven = LayoutInflater.from(activity).inflate(R.layout.custom_tab_workout, null) as RadioButton
        tabSeven.text = list[6]
        if (list[6] == "off") {
            tabSeven.setTextColor(Color.parseColor("#0097C1"))
            tabSeven.isChecked = false
        }
        binding.daysTabLayout.getTabAt(6)!!.customView = tabSeven
        activity!!.resources.displayMetrics
    }

    private fun setupViewPager(viewPager: ViewPager) {
        val adapter = ViewPagerAdapter(activity!!.supportFragmentManager)
        adapter.addFrag(WorkoutFragent(), resources.getString(R.string.results_key_data))
        adapter.addFrag(WorkoutFragent(), resources.getString(R.string.results_sport))
        adapter.addFrag(WorkoutFragent(), resources.getString(R.string.results_diet))
        adapter.addFrag(WorkoutFragent(), resources.getString(R.string.results_key_data))
        adapter.addFrag(WorkoutFragent(), resources.getString(R.string.results_sport))
        adapter.addFrag(WorkoutFragent(), resources.getString(R.string.results_diet))
        adapter.addFrag(WorkoutFragent(), resources.getString(R.string.results_key_data))
        viewPager.adapter = adapter
        (viewPager.adapter!!.instantiateItem(viewPager, viewPager.currentItem) as WorkoutFragent)
    }

    internal inner class ViewPagerAdapter(manager: FragmentManager) : FragmentStatePagerAdapter(manager) {
        private val mFragmentList = arrayListOf<Fragment>()
        private val mFragmentTitleList = arrayListOf<String>()

        override fun getItem(position: Int): Fragment {
            val bundle = Bundle()
            val gson = Gson()
            when (position) {
                0 -> {

                    bundle.putString(Constants.DAYS_WORKOUT_MESSAGE, gson.toJson(data!!.getExcerises()!!))
                    bundle.putString(Constants.WORKOUT_DAY, activity!!.resources.getString(R.string.day_monday))
                }
                1 -> {
                    bundle.putString(Constants.DAYS_WORKOUT_MESSAGE, gson.toJson(data!!.getExcerises()!!))
                    bundle.putString(Constants.WORKOUT_DAY, activity!!.resources.getString(R.string.day_tuesday))

                }
                2 -> {
                    bundle.putString(Constants.DAYS_WORKOUT_MESSAGE, gson.toJson(data!!.getExcerises()!!))
                    bundle.putString(Constants.WORKOUT_DAY, activity!!.resources.getString(R.string.day_wednesday))

                }
                3 -> {
                    bundle.putString(Constants.DAYS_WORKOUT_MESSAGE, gson.toJson(data!!.getExcerises()!!))
                    bundle.putString(Constants.WORKOUT_DAY, activity!!.resources.getString(R.string.day_thursday))

                }
                4 -> {
                    bundle.putString(Constants.DAYS_WORKOUT_MESSAGE, gson.toJson(data!!.getExcerises()!!))
                    bundle.putString(Constants.WORKOUT_DAY, activity!!.resources.getString(R.string.day_friday))

                }
                5 -> {
                    bundle.putString(Constants.DAYS_WORKOUT_MESSAGE, gson.toJson(data!!.getExcerises()!!))
                    bundle.putString(Constants.WORKOUT_DAY, activity!!.resources.getString(R.string.day_saturday))

                }
                6 -> {
                    bundle.putString(Constants.DAYS_WORKOUT_MESSAGE, gson.toJson(data!!.getExcerises()!!))
                    bundle.putString(Constants.WORKOUT_DAY, activity!!.resources.getString(R.string.day_sunday))

                }

            }
            val workoutFragent = WorkoutFragent()
            workoutFragent.arguments = bundle

            return workoutFragent
        }

        override fun getCount(): Int {
            return mFragmentList.size
        }

        fun addFrag(fragment: Fragment, title: String) {
            mFragmentList.add(fragment)
            mFragmentTitleList.add(title)
        }

        override fun getPageTitle(position: Int): CharSequence? {
            return mFragmentTitleList[position]
        }
    }

}