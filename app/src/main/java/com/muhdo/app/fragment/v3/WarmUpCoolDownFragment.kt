package com.muhdo.app.fragment.v3


import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ExpandableListView
import androidx.fragment.app.Fragment
import com.google.gson.JsonObject
import com.muhdo.app.R
import com.muhdo.app.adapter.v3.WarmUpExpandableAdapter
import com.muhdo.app.apiModel.v3.workoutplan.Data
import com.muhdo.app.apiModel.v3.workoutplan.ResponseInjuryPrevention
import com.muhdo.app.repository.ApiUtilis
import com.timingsystemkotlin.backuptimingsystem.Utility
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_warmup_cool_down.*

class WarmUpCoolDownFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_warmup_cool_down, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        fetchWarmUpWorkouts()
    }

    @SuppressLint("CheckResult")
    private fun fetchWarmUpWorkouts() {
        progress_bar.visibility = View.VISIBLE
        val requestBody = JsonObject()
        requestBody.addProperty("type", "WARM_UP")
        ApiUtilis.getAPIInstance(activity!!).getWorkoutInjuryPrevention(requestBody)
            .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
            .subscribe({ success -> parseAndLoadVideoList(success) }, { error -> showError(error) })
    }

    private fun showError(error: Throwable?) {
        try {
            progress_bar.visibility = View.GONE
            error?.localizedMessage?.let { Utility.displayShortSnackBar(elv_foam_rolling, it) }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun parseAndLoadVideoList(it: ResponseInjuryPrevention) {
        progress_bar?.visibility = View.GONE
        val foamRolling = it.data[0].foam_rolling?.toMutableList()
        foamRolling?.add(
            0,
            Data(
                "", getString(R.string.foam_rolling), "", "", "",
                R.drawable.v3_foam_rolling.toString(), null, null
            )
        )
        foamRolling?.add(
            foamRolling.size,
            Data(
                "",
                getString(R.string.flexibility_mobility),
                "",
                "",
                "",
                R.drawable.v3_flexibility_mob.toString(),
                null,
                null
            )
        )
        it.data[0].flexibility?.let { it1 -> foamRolling?.addAll(it1) }
        val adapter =
            context?.let { it1 -> WarmUpExpandableAdapter(it1, foamRolling) }
        elv_foam_rolling.setAdapter(adapter)
        elv_foam_rolling.setGroupIndicator(null)
        elv_foam_rolling.setOnGroupClickListener(object :
            ExpandableListView.OnGroupClickListener {
            override fun onGroupClick(
                parent: ExpandableListView?,
                v: View?,
                groupPosition: Int,
                id: Long
            ): Boolean {
                if (groupPosition == 0 || groupPosition == it.data[0].foam_rolling?.size!! + 1)
                    return true
                return false
            }
        })
    }
}
