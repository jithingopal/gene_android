package com.muhdo.app.fragment.v3.guide.meal

import android.content.Context
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import com.muhdo.app.R
import com.muhdo.app.databinding.V3FragmentChooseGoalFragmentBinding
import com.muhdo.app.databinding.V3LayoutPlanChooseExperianceBinding
import com.muhdo.app.databinding.V3LayoutPlanChooseMealCountBinding
import com.muhdo.app.utils.v3.getSelectedRadioButtonText
import org.jetbrains.anko.toast

class ChooseHowManyMealsFragment : Fragment() {
      lateinit var binding: V3LayoutPlanChooseMealCountBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view =
            inflater.inflate(R.layout.v3_layout_plan_choose_meal_count, container, false)
        binding= V3LayoutPlanChooseMealCountBinding.bind(view)
        binding.slider.setOnDiscreteSliderChangeListener { position: Int ->
            binding.tvMealCount.setText((position+2).toString())
        }
        return view
    }


    fun getSelectedMealCount():Int?{
        return binding.tvMealCount.text.toString().toInt()

    }

}
