package com.muhdo.app.fragment.v3.epigenetics

import android.annotation.SuppressLint
import android.graphics.Color
import android.graphics.DashPathEffect
import android.graphics.Typeface
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import androidx.viewpager.widget.ViewPager
import com.muhdo.app.R
import com.muhdo.app.ui.epigenticResult.model.EpigenticHealth
import com.github.mikephil.charting.components.Legend
import com.github.mikephil.charting.components.LimitLine
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.components.YAxis
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.formatter.IFillFormatter
import com.github.mikephil.charting.formatter.ValueFormatter
import com.github.mikephil.charting.highlight.Highlight
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet
import com.github.mikephil.charting.listener.OnChartValueSelectedListener
import com.github.mikephil.charting.utils.Utils
import com.muhdo.app.apiModel.v3.EpigenBaseReq
import com.muhdo.app.apiModel.v3.epigenetics.EpiSampleInfo
import com.muhdo.app.apiModel.v3.epigenetics.ScoreDataChart
import com.muhdo.app.custom_view.v3.MyMarkerView
import com.muhdo.app.databinding.V3FragmentEpigeneticDetailsBinding
import com.muhdo.app.repository.ApiUtilis
import com.muhdo.app.repository.ErrorModel
import com.muhdo.app.repository.NetworkManager
import com.muhdo.app.repository.ServiceListener
import com.muhdo.app.ui.epigenticResult.model.EpigResultResponse
import com.muhdo.app.utils.v3.TempUtil
import com.muhdo.app.utils.v3.convertDateStringFormat
import com.muhdo.app.utils.v3.showAlertDialog
import com.timingsystemkotlin.backuptimingsystem.Utility
import java.util.*
import kotlin.math.roundToInt


class EpigeneticsDetailsFragment : Fragment(){
    var tab_icons = arrayOf<Int>(
        R.drawable.v3_ic_biological_age,
        R.drawable.v3_ic_eye_health,
        R.drawable.v3_ic_hearing,
        R.drawable.v3_ic_mental_health,
        R.drawable.v3_ic_inflammation
    )

    var selectedKitID=""//position actually starts from one
    var selectedChartPosition=1//position actually starts from one
    var yScoreList: ArrayList<ScoreDataChart>?=null
    internal lateinit var binding: V3FragmentEpigeneticDetailsBinding
    lateinit var epigenticHealthData: List<EpigenticHealth>
    var position = 0
    lateinit var tfRegular: Typeface
    lateinit var tfLight: Typeface
    var sampleInfoList:EpiSampleInfo?=null


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.v3_fragment_epigenetic_details, container, false)
        binding = V3FragmentEpigeneticDetailsBinding.bind(view)
        tfRegular = Typeface.createFromAsset(activity?.getAssets(), "roboto_medium.ttf")
        tfLight = Typeface.createFromAsset(activity?.getAssets(), "roboto_medium.ttf")
        setUpChartView()
        reloadSelectedScoreList(position)
        getEpigenticResultDataForKitID()
        binding.imgViewGoEpigenHome.setOnClickListener {
            replaceFragment(EpigeneticsHomeFragment())
        }
        return view
    }

    var biologicalAge: Float = 0F
    fun setTabSelection(pos: Int) {
        position = pos
    }

    private fun setupTabIcons() {
        val fragmentAdapter = ViewPagerAdapter(activity!!.supportFragmentManager)
        binding.viewPager.adapter = fragmentAdapter
        binding.viewPager.offscreenPageLimit = 0
        val density = resources.displayMetrics.density
        binding.tabLayout.setupWithViewPager(binding.viewPager)
        val adapter = ViewPagerAdapter(activity!!.supportFragmentManager)
        binding.viewPager.adapter = adapter
        for (i in 0 until tab_icons.size) {
            val fragment = EpigeneticsDetailsResultFragment()
            fragment.position = i
            fragment.biologicalAge = this.biologicalAge.toInt()
            if (epigenticHealthData.size != 0) {
                fragment.setEpigeneticHealth(epigenticHealthData.get(i))
            }
            adapter.addFrag(fragment, "pos" + i)
        }
        binding.viewPager.adapter = adapter

        for (i in 0 until tab_icons.size) {
            val tabView = LayoutInflater.from(activity!!.applicationContext)
                .inflate(R.layout.v3_custom_tab_epigen, null)
            val tabImage = tabView.findViewById(R.id.tab_img) as ImageView
            tabImage.setColorFilter(
                ContextCompat.getColor(
                    activity!!.applicationContext,
                    R.color.v3_epi_gen_icon_unselected
                ), android.graphics.PorterDuff.Mode.SRC_IN
            );
            tabImage.setImageResource(tab_icons[i])
            binding.tabLayout.getTabAt(i)!!.customView = tabView
        }
        val tabImage =
            binding.tabLayout.getTabAt(0)!!.customView!!.findViewById(R.id.tab_img) as ImageView
        tabImage.setColorFilter(
            ContextCompat.getColor(
                activity!!.applicationContext,
                R.color.v3_epi_gen_colorPrimary
            ), android.graphics.PorterDuff.Mode.SRC_IN
        )
        binding.viewPager.addOnPageChangeListener((object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {

            }

            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {

            }

            override fun onPageSelected(position: Int) {

                for (i in 0 until tab_icons.size) {
                    val tabImage =
                        binding.tabLayout.getTabAt(i)!!.customView!!.findViewById(R.id.tab_img) as ImageView
                    tabImage.setColorFilter(
                        ContextCompat.getColor(
                            activity!!.applicationContext,
                            R.color.v3_epi_gen_icon_unselected
                        ), android.graphics.PorterDuff.Mode.SRC_IN
                    )
                    setTabSelection(position)
                }

                val tabImage =binding.tabLayout.getTabAt(position)!!.customView!!.findViewById(R.id.tab_img) as ImageView
                tabImage.setColorFilter(
                    ContextCompat.getColor(
                        activity!!.applicationContext,
                        R.color.v3_epi_gen_colorPrimary
                    ), android.graphics.PorterDuff.Mode.SRC_IN
                )


                binding.nestedScrollView.isSmoothScrollingEnabled=true
                binding.nestedScrollView.fling(0)
                binding.nestedScrollView.smoothScrollTo(0,0)
                drawChart(position)

            }

        }))




    }

    private fun getScoreForYAxis(score1:Float,position: Int): Float {
        var score =score1
        /*if (position !=4) {
            score = score!! - biologicalAge
        }*/
        if (score?.toFloat()!! > 10f) {
            score = 10f
        }

        if (score?.toFloat()!! < (-10f)) {
            score = -10f
        }
        /*if (position == 4) {
            var graphScore: Float = 0f
            var inflammationScore =score!!.roundToInt()
            TempUtil.log("Score:",score.toString())
            TempUtil.log("inflammationScore:",inflammationScore.toString())
            when (inflammationScore) {

                in 450..1000->{
                    graphScore=10f
                }

                in 401..449 -> {

                    graphScore = inflammationScore-400f
                    graphScore=graphScore/10
                    graphScore=graphScore+5
                }
                in 376..400 -> {

                    graphScore = inflammationScore-375f
                    graphScore=graphScore/5
                }
                375->{
                    graphScore=0f
                }
                in 350..374 -> {

                    graphScore = inflammationScore-375f
                    graphScore=graphScore/5
                }
                in 301..349 -> {
                    graphScore = inflammationScore-350f
                    graphScore=graphScore/10
                    graphScore=graphScore+(-5)
                }
                in 0..300 -> {
                    graphScore =-10f
                }
                else -> {
                    graphScore = 0f
                }

            }
            Log.e("Graph Score:",graphScore.toString())
            return graphScore
        } else {

            return score
        }*/
        return score
    }

    fun setEpiGeneticSampleList(sampleInfo: EpiSampleInfo) {
        this.sampleInfoList=sampleInfo

    }

    @SuppressLint("WrongConstant")
    private fun replaceFragment(fragment: Fragment) {
        val transaction = activity!!.supportFragmentManager.beginTransaction()
        transaction.replace(R.id.flContent, fragment)
        transaction.addToBackStack("share")
        transaction.commit()

    }

    private fun setUpChartView() {
        run {
            // // Chart Style // //

            // background color
            //chart.setBackgroundColor(getResources().getColor(R.color.bg_chart));
            // background drawable
            /*val drawable = ContextCompat.getDrawable(activity!!, R.drawable.bg_line_chart_epigen)
            binding.chart.setBackground(drawable)*/
            binding.chart.setBackgroundColor(Color.TRANSPARENT)

            // disable description text
            binding.chart.getDescription().setEnabled(false)
            binding.chart.setAutoScaleMinMaxEnabled(true)
            // enable touch gestures
            binding.chart.setTouchEnabled(true)

            // set listeners
//            binding.chart.setOnChartValueSelectedListener(this)
            binding.chart.setHighlightPerTapEnabled(true)
            binding.chart.setOnChartValueSelectedListener(object : OnChartValueSelectedListener {
                override fun onValueSelected(e: Entry, h: Highlight) {
                    if(e.x.toInt()!=selectedChartPosition&&e.x.toInt()!=0)
                    {

                        selectedChartPosition=e.x.toInt()//-1
                         e .setIcon(resources.getDrawable(R.drawable.v3_ic_epi_chart_highlited));


                        getEpigenticResultDataForKitID()
                    }
                }


                override fun onNothingSelected() {
                }
            })
            binding.chart.setDrawGridBackground(false)

            // create marker to display box when values are selected
            val mv = MyMarkerView(activity, R.layout.v3_chart_custom_marker_view)

            // Set the marker to the chart
            mv.chartView = binding.chart
            binding.chart.setMarker(mv)//show label

            // enable scaling and dragging
            binding.chart.setDragEnabled(true)
            binding.chart.setScaleEnabled(false)
//            binding.chart.setScaleXEnabled(true);
//            binding.chart.setScaleYEnabled(true);
            // force pinch zoom along both axis
            binding.chart.setPinchZoom(false)
        }
        val xValues = ArrayList<String>(4)
        xValues.add("0")
        xValues.add("1")
        xValues.add("2")
        xValues.add("3")
        xValues.add("4")
        xValues.add("5")
        val xAxisLabel = arrayOf("Jan", "Feb", "Mar", "Apri", "May", "Jun")
        val xAxis: XAxis
        run {
            // // X-Axis Style // //
            xAxis = binding.chart.getXAxis()
            xAxis.isEnabled = true
            xAxis.axisLineColor = Color.WHITE
            xAxis.gridColor = Color.TRANSPARENT
            xAxis.position = XAxis.XAxisPosition.BOTTOM
            xAxis.axisLineWidth = 1f
            xAxis.textColor = Color.WHITE
            xAxis.setAvoidFirstLastClipping(true)
            xAxis.labelCount = 5
            // vertical grid lines
            //            xAxis.enableGridDashedLine(10f, 10f, 0f);
            //            xAxis.setValueFormatter(new IndexAxisValueFormatter(xAxisLabel));
            var xValueFormatter: ValueFormatter = object : ValueFormatter() {
                override fun getFormattedValue(value: Float): String {
                    // return the string va
                    return value.toInt().toString() + ""
                }
            }
            xAxis.setValueFormatter(xValueFormatter)

        }

        val yAxis: YAxis
        run {
            // // Y-Axis Style // //
            yAxis = binding.chart.getAxisLeft()

            // disable dual axis (only use LEFT axis)
            binding.chart.getAxisRight().setEnabled(false)

            // horizontal grid lines
            yAxis.enableGridDashedLine(10f, 10f, 0f)
            yAxis.gridColor = Color.TRANSPARENT
            yAxis.axisLineColor = Color.WHITE
            yAxis.axisLineWidth = 1f
            yAxis.textColor = Color.WHITE
            yAxis.setDrawTopYLabelEntry(true)
            // axis range
            yAxis.axisMaximum = 10f
            yAxis.axisMinimum = -10f
            yAxis.labelCount = 20
            yAxis.textSize = 14f
            var valueFormatter = object : ValueFormatter() {

                override fun getFormattedValue(value: Float): String {
                    // return the string va
                    when (value.toInt()) {
                        0 -> {
                            if(binding.tabLayout.selectedTabPosition==4){
                               return context!!.resources!!.getString(R.string.score_y_axis)
                            }else{
                                return context!!.resources!!.getString(R.string.age_y_axis)
                            }
                        }
                        1 -> return ""
                        2 -> return ""
                        3 -> return ""
                        4 -> return ""
                        5 -> return "+"
                        6 -> return ""
                        7 -> return ""
                        8 -> return ""
                        9 -> return ""
                        10 -> return ""
                        -1 -> return ""
                        -2 -> return ""
                        -3 -> return ""
                        -4 -> return ""
                        -5 -> return "-"
                        -6 -> return ""
                        -7 -> return ""
                        -8 -> return ""
                        -9 -> return ""
                        -10 -> return ""
                    }
                    return ""
                }
            }
            yAxis.setValueFormatter(valueFormatter)

        }


        run {
            // // Create Limit Lines // //


            val ll1 = LimitLine(0f, " ")
            ll1.lineWidth = 1f
            ll1.lineColor = Color.WHITE
            ll1.enableDashedLine(10f, 10f, 0f)
            ll1.labelPosition = LimitLine.LimitLabelPosition.RIGHT_TOP
            ll1.textSize = 10f
            ll1.typeface = tfRegular
            val ll3 = LimitLine(5f, " ")
            ll3.lineWidth = 1f
            ll3.lineColor = Color.WHITE
            ll3.enableDashedLine(10f, 10f, 0f)
            ll3.labelPosition = LimitLine.LimitLabelPosition.RIGHT_TOP
            ll3.textSize = 10f
            ll3.typeface = tfRegular

            val ll2 = LimitLine(-5f, "")
            ll2.lineWidth = 1f
            ll2.lineColor = Color.WHITE
            ll2.enableDashedLine(10f, 10f, 0f)
            ll2.labelPosition = LimitLine.LimitLabelPosition.RIGHT_BOTTOM
            ll2.textSize = 10f
            ll2.typeface = tfRegular

            // draw limit lines behind data instead of on top
            yAxis.setDrawLimitLinesBehindData(true)
            xAxis.setDrawLimitLinesBehindData(true)

            // add limit lines
            yAxis.addLimitLine(ll1)
            yAxis.addLimitLine(ll2)
            yAxis.addLimitLine(ll3)
        }


        // draw points over time
//        chart.animateX(1000);


        // get the legend (only possible after setting data)
        val l = binding.chart.getLegend()
        // draw legend entries as lines
        l.setForm(Legend.LegendForm.LINE)

    }
    var isLoadingChartForFirstTime=true
    private fun drawChart(selectedTabPosition: Int) {

        val values = ArrayList<Entry>()
        val values2 = ArrayList<Entry>()
        values.add(Entry(0f, 0f ))
        values.add(Entry(1f, 0f ))
        values.add(Entry(2f, 0f ))
        values.add(Entry(3f, 0f ))
        values.add(Entry(4f, 0f ))
        values.add(Entry(5f, 0f ))

        val set1: LineDataSet
        var set2: LineDataSet

        values2.add(Entry(0f, 0f))
        reloadSelectedScoreList(selectedTabPosition)
        var subtractValue=1
        if(isLoadingChartForFirstTime){
            subtractValue=  TempUtil.currentSelectedEpigeneticTab
            isLoadingChartForFirstTime=false
        }
        for((i,scoreItem) in yScoreList!!.withIndex())
        {
            val score=scoreItem.score //getScoreForYAxis(scoreItem.score,selectedTabPosition)
            TempUtil.log("Jithin","scoreItem.score=" +scoreItem.score+" position="+selectedTabPosition+" Score "+score)


            if((selectedChartPosition-subtractValue)==i)
            {
                values2.add(Entry((i.toFloat()+1),score ,  resources.getDrawable(R.drawable.v3_ic_epi_chart_highlited) ))
                TempUtil.markerTextOnChart= convertDateStringFormat(
                    scoreItem.date_of_scanning,
                    "yyyy-mm-dd'T'HH:mm:ss.SSS'Z'",
                    "MMM dd, yyyy h:mm a"
                )
            }else{
                values2.add(Entry((i.toFloat()+1),score))
            }
            if(i==4){
                break
            }

        }

        if (binding.chart.getData() != null && binding.chart.getData().getDataSetCount() > 0) {
            set1 = binding.chart.getData().getDataSetByIndex(0) as LineDataSet
            set1.values = values
            set2 = binding.chart.getData().getDataSetByIndex(1) as LineDataSet
            set2.values = values2
            set2.notifyDataSetChanged()
            set1.notifyDataSetChanged()
            binding.chart.getData().notifyDataChanged()
            binding.chart.notifyDataSetChanged()
        } else {
            // create a dataset and give it a type
            set1 = LineDataSet(values, "")
            set1.setDrawIcons(false)
            set2 = LineDataSet(values2, "")
            set2.setDrawIcons(true)

            // draw dashed line
            //            set1.enableDashedLine(10f, 5f, 0f);

            // black lines and points
            set1.color = Color.TRANSPARENT
            set1.setCircleColor(Color.TRANSPARENT)
            set2.color = Color.WHITE
            set2.setCircleColor(Color.WHITE)
//set2.setDrawHighlightIndicators(true)
            // line thickness and point size
            set2.lineWidth = 1f
            set2.circleRadius = 3f
            // line thickness and point size
            set1.lineWidth = 1f
            set1.circleRadius = 3f

            // draw points as solid circles
            set1.setDrawCircleHole(false)
            set2.setDrawCircleHole(false)

            // customize legend entry
            set1.formLineWidth = 1f
            set1.formLineDashEffect = DashPathEffect(floatArrayOf(10f, 5f), 0f)
            set1.formSize = 15f

            set2.formLineWidth = 1f
            set2.formLineDashEffect = DashPathEffect(floatArrayOf(10f, 5f), 0f)
            set2.formSize = 15f

            // text size of values
            set1.valueTextSize = 9f
            set1.setDrawValues(false)
            set2.valueTextSize = 9f
            set2.setDrawValues(false)

            // draw selection line as dashed
            set1.enableDashedHighlightLine(10f, 5f, 0f)
            set2.enableDashedHighlightLine(10f, 5f, 0f)

            // set the filled area
            set1.setDrawFilled(false)
            set1.fillFormatter =
                IFillFormatter { dataSet, dataProvider ->
                    binding.chart.getAxisLeft().getAxisMinimum()
                }

            set2.setDrawFilled(false)
            set2.fillFormatter =
                IFillFormatter { dataSet, dataProvider ->
                    binding.chart.getAxisLeft().getAxisMinimum()
                }
            //            set1.setFillColor(Color.TRANSPARENT);

            // set color of filled area
            if (Utils.getSDKInt() >= 18) {
                // drawables only supported on api level 18 and above
                val drawable = ContextCompat.getDrawable(activity!!, R.drawable.fade_red)
                set1.fillDrawable = drawable
                set2.fillDrawable = drawable
            } else {
                set1.fillColor = Color.BLACK
                set2.fillColor = Color.BLACK
            }

            val dataSets = ArrayList<ILineDataSet>()
            dataSets.add(set1) // add the data sets
            dataSets.add(set2) // add the data sets

            // create a data object with the data sets
            val data = LineData(dataSets)

            // set data
            binding.chart.setData(data)
        }
        binding.chart.animateY(1250)

    }

    private fun reloadSelectedScoreList(selectedTabPosition: Int) {
        when(selectedTabPosition){
            0->{
                yScoreList=sampleInfoList!!.biological_score
            }
            1->{
                yScoreList=sampleInfoList!!.eye_score
            }
            2->{
                yScoreList=sampleInfoList!!.hearing_score
            }
            3->{
                yScoreList=sampleInfoList!!.memory_score

            }
            4->{
                yScoreList=sampleInfoList!!.inflammation_score
            }


        }
    }


    internal inner class ViewPagerAdapter(manager: FragmentManager) :
        FragmentStatePagerAdapter(manager) {
        private val mFragmentList = arrayListOf<Fragment>()
        private val mFragmentTitleList = arrayListOf<String>()

        override fun getItem(position: Int): Fragment {

            return mFragmentList.get(position)
        }

        override fun getCount(): Int {
            return mFragmentList.size
        }

        fun addFrag(fragment: Fragment, title: String) {
            mFragmentList.add(fragment)
            mFragmentTitleList.add(title)
        }

        override fun getPageTitle(position: Int): CharSequence? {
            Log.d("data", "tab pos = " + position)
            return mFragmentTitleList[position]
        }
    }
    var  isFirstTimeKitID=true
    private fun getEpigenticResultDataForKitID() {
        binding.progressBar.visibility = View.VISIBLE
        val manager = NetworkManager()
        if (manager.isConnectingToInternet(requireContext())) {
            var kitId = Utility.getKitID(activity!!)
            if(yScoreList!=null){
                if (selectedChartPosition!=0)
                {
                    if(isFirstTimeKitID)
                    {
                        kitId=yScoreList!!.get(yScoreList!!.size-1).kit_id
                        selectedChartPosition=yScoreList!!.size//current
                        isFirstTimeKitID=false
                    }else{
                        kitId=yScoreList!!.get(selectedChartPosition-1).kit_id
                    }
                }else{
                    if(selectedKitID.isNotEmpty()) {
                        kitId=selectedKitID
                    } else{
                        return
                    }

                    binding.progressBar.visibility = View.GONE
                }

            }
            val userId= Utility.getUserID(activity!!.applicationContext)
            val epigenReq = EpigenBaseReq(userId,kitId)

            manager.createApiRequest(
                ApiUtilis.getAPIInstance(activity!!).getEpigenticResultDataForSingleKitIdV3(
                    epigenReq
                ),

                object : ServiceListener<EpigResultResponse> {
                    override fun getServerResponse(
                        epigResultObj: EpigResultResponse,
                        requestcode: Int
                    ) {
                        binding.progressBar.visibility = View.GONE
                        try {
                            if (epigResultObj.code == 200) {
                                if (epigResultObj != null) {
                                    epigenticHealthData=epigResultObj.data?.epigenticHealthData!!

                                    setupTabIcons()

                                    if (position == 0) {
                                        binding.viewPager.setCurrentItem(1)
                                    }
                                    binding.viewPager.setCurrentItem(position)
                                } else {
                                }
                            } else if (epigResultObj.code == 401) {
                                TempUtil.log(
                                    "EpiHome",
                                    epigResultObj.code.toString() + " " + epigResultObj.message
                                )
                                context?.showAlertDialog(epigResultObj.message!!)

                            } else {
//                                showDialogBoxWhenAccountSuspended(context,epigResultObj.message.toString())
                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }

                    override fun getError(error: ErrorModel, requestcode: Int) {
                        binding.progressBar.visibility = View.GONE

                        Utility.displayShortSnackBar(
                            binding.parentLayout,
                            error.getMessage()!!
                        )
                    }
                })
        } else {
            binding.progressBar.visibility = View.GONE
            Utility.displayShortSnackBar(
                binding.parentLayout,
                context!!.getString(R.string.check_internet)
            )
        }
    }

}
