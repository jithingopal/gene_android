package com.muhdo.app.fragment.v3

import android.annotation.SuppressLint
import android.os.Bundle
import android.text.Html
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.muhdo.app.R
import com.muhdo.app.databinding.V3FragmentGeneticActionHomeBinding
import com.muhdo.app.ui.DashboardActivity

class GeneticActionPlanHomeFragment : Fragment() {

    internal lateinit var binding: V3FragmentGeneticActionHomeBinding
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.v3_fragment_genetic_action_home, container, false)

        binding = V3FragmentGeneticActionHomeBinding.bind(view)
        binding.btnActionPlanDiet.setOnClickListener {
            var geneticActionPlan = GeneticActionPlanResultFragment()
            geneticActionPlan.selectedTabPosition(0)
            replaceFragment(geneticActionPlan)
        }
        binding.btnVitamins.setOnClickListener {
            var geneticActionPlan = GeneticActionPlanResultFragment()
            geneticActionPlan.selectedTabPosition(1)
            replaceFragment(geneticActionPlan)
        }
        binding.btnPhysical.setOnClickListener {
            var geneticActionPlan = GeneticActionPlanResultFragment()
            geneticActionPlan.selectedTabPosition(2)
            replaceFragment(geneticActionPlan)
        }
        binding.btnHealth.setOnClickListener {
            val geneticActionPlan = GeneticActionPlanResultFragment()
            geneticActionPlan.selectedTabPosition(3)
            replaceFragment(geneticActionPlan)
        }
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        context?.let { (it as DashboardActivity).removeBottomNavColor() }
    }

    @SuppressLint("WrongConstant")
    private fun replaceFragment(fragment: Fragment) {
        val transaction = activity!!.supportFragmentManager.beginTransaction()
        transaction.replace(R.id.flContent, fragment)
        transaction.addToBackStack("share")
        transaction.commit()

    }


}
