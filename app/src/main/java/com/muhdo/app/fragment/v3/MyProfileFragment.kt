package com.muhdo.app.fragment.v3

import android.annotation.SuppressLint
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Base64
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.CompoundButton
import android.widget.RadioGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import com.google.gson.Gson
import com.muhdo.app.R
import com.muhdo.app.adapter.v3.GenoTypeAdapterForMyProfile
import com.muhdo.app.apiModel.keyData.GenoTypeModel
import com.muhdo.app.apiModel.v3.*
import com.muhdo.app.databinding.V3FragmentProfileBinding
import com.muhdo.app.repository.*
import com.muhdo.app.utils.PreferenceConnector
import com.muhdo.app.utils.v3.*
import com.timingsystemkotlin.backuptimingsystem.Utility
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.v3_fragment_profile.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

class MyProfileFragment : Fragment() {

    private val compositeDisposable = CompositeDisposable()
    internal lateinit var binding: V3FragmentProfileBinding
    var adapter: GenoTypeAdapterForMyProfile? = null
    var userData: UserData? = null
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.v3_fragment_profile, container, false)
        binding = V3FragmentProfileBinding.bind(view)

        binding.genoTypeGrid.layoutManager = GridLayoutManager(requireActivity(), 2)
        /*binding.btnReviewQuestionnaire.setOnClickListener {
            replaceFragment(QuestionnaireHomeFragment())
        }*/
        binding.btnEdit.setOnClickListener {
            binding.btnEdit.visibility = View.GONE
            binding.btnSave.visibility = View.VISIBLE
            binding.btnCancel.visibility = View.VISIBLE
            toggleEditableFields(true)
        }
        binding.btnSave.setOnClickListener {
            if (checkValidation()) {
                binding.btnEdit.visibility = View.VISIBLE
                binding.btnSave.visibility = View.GONE
                binding.btnCancel.visibility = View.GONE
                toggleEditableFields(false)
                uploadUserDetails()
            }
        }
        binding.btnCancel.setOnClickListener {

            binding.btnEdit.visibility = View.VISIBLE
            binding.btnSave.visibility = View.GONE
            binding.btnCancel.visibility = View.GONE
            toggleEditableFields(false)
            setUserDataToUI()
        }
        setListeners()
        Utility.sectionList.clear()
        Utility.modeList.clear()
        getGenotypeModes()
        addAppPreferenceGoalCheckedChangedListener()
        return view
    }

    fun getSelectedAppPreferenceGoal(): String {
        val cb1: CheckBox = binding.viewAppPreferences.findViewById(R.id.cbAppPref1) as CheckBox
        val cb2: CheckBox = binding.viewAppPreferences.findViewById(R.id.cbAppPref2) as CheckBox
        val cb3: CheckBox = binding.viewAppPreferences.findViewById(R.id.cbAppPref3) as CheckBox
        val cb4: CheckBox = binding.viewAppPreferences.findViewById(R.id.cbAppPref4) as CheckBox

        if (cb1.isChecked) return cb1.text.toString()
        else if (cb2.isChecked) return cb2.text.toString()
        else if (cb3.isChecked) return cb3.text.toString()
        else if (cb4.isChecked) return cb4.text.toString()
        else return ""
    }

    fun addAppPreferenceGoalCheckedChangedListener() {

        val cb1: CheckBox = binding.viewAppPreferences.findViewById(R.id.cbAppPref1) as CheckBox
        val cb2: CheckBox = binding.viewAppPreferences.findViewById(R.id.cbAppPref2) as CheckBox
        val cb3: CheckBox = binding.viewAppPreferences.findViewById(R.id.cbAppPref3) as CheckBox
        val cb4: CheckBox = binding.viewAppPreferences.findViewById(R.id.cbAppPref4) as CheckBox

        cb1.setOnCheckedChangeListener(appPreferenceAddCheckedChangedListener)
        cb2.setOnCheckedChangeListener(appPreferenceAddCheckedChangedListener)
        cb3.setOnCheckedChangeListener(appPreferenceAddCheckedChangedListener)
        cb4.setOnCheckedChangeListener(appPreferenceAddCheckedChangedListener)

    }

    fun removeAppPreferenceGoalCheckedChangedListener() {

        val cb1: CheckBox = binding.viewAppPreferences.findViewById(R.id.cbAppPref1) as CheckBox
        val cb2: CheckBox = binding.viewAppPreferences.findViewById(R.id.cbAppPref2) as CheckBox
        val cb3: CheckBox = binding.viewAppPreferences.findViewById(R.id.cbAppPref3) as CheckBox
        val cb4: CheckBox = binding.viewAppPreferences.findViewById(R.id.cbAppPref4) as CheckBox

        cb1.setOnCheckedChangeListener(null)
        cb2.setOnCheckedChangeListener(null)
        cb3.setOnCheckedChangeListener(null)
        cb4.setOnCheckedChangeListener(null)

    }

    val appPreferenceAddCheckedChangedListener: CompoundButton.OnCheckedChangeListener =
        CompoundButton.OnCheckedChangeListener { cb, isChecked ->
            val cb1: CheckBox = binding.viewAppPreferences.findViewById(R.id.cbAppPref1) as CheckBox
            val cb2: CheckBox = binding.viewAppPreferences.findViewById(R.id.cbAppPref2) as CheckBox
            val cb3: CheckBox = binding.viewAppPreferences.findViewById(R.id.cbAppPref3) as CheckBox
            val cb4: CheckBox = binding.viewAppPreferences.findViewById(R.id.cbAppPref4) as CheckBox
            removeAppPreferenceGoalCheckedChangedListener()
            when (cb.id) {

                cb1.id -> {
                    cb1.isChecked = true
                    cb2.isChecked = false
                    cb3.isChecked = false
                    cb4.isChecked = false

                }
                cb2.id -> {
                    cb1.isChecked = false
                    cb2.isChecked = true
                    cb3.isChecked = false
                    cb4.isChecked = false
                }
                cb3.id -> {
                    cb1.isChecked = false
                    cb2.isChecked = false
                    cb3.isChecked = true
                    cb4.isChecked = false
                }
                cb4.id -> {
                    cb1.isChecked = false
                    cb2.isChecked = false
                    cb3.isChecked = false
                    cb4.isChecked = true
                }
            }
            addAppPreferenceGoalCheckedChangedListener()
        }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        toggleEditableFields(false)
    }

    private fun setListeners() {
        binding.edtDay.setOnFocusChangeListener { view, hasFocus ->
            if (hasFocus) {

            } else {
                val dayString = binding.edtDay.text.toString()
                if (dayString.isNotEmpty() && dayString != "") {
                    val day: Int = Integer.parseInt(dayString)
                    if (day < 10) {
                        binding.edtDay.setText("0" + day)
                    }
                    if (day == 0) {
                        binding.edtDay.setText("01")
                    }
                    if (day > 31) {
                        binding.edtDay.setText("31")
                    }
                }
            }
            changeDobDividerColor()
        }
        binding.edtMonth.setOnFocusChangeListener { _, hasFocus ->
            if (hasFocus) {

            } else {
                val monthString = binding.edtMonth.text.toString()
                if (monthString.isNotEmpty() && monthString != "") {
                    val month: Int = Integer.parseInt(monthString)
                    if (month < 10) {
                        binding.edtMonth.setText("0" + month)
                    }
                    if (month > 12) {
                        binding.edtMonth.setText("12")
                    }
                    if (month == 0) {
                        binding.edtMonth.setText("01")
                    }

                }
            }
            changeDobDividerColor()
        }

        binding.edtYear.setOnFocusChangeListener { _, hasFocus ->
            if (hasFocus) {

            } else {
                val yearString = binding.edtYear.text.toString()
                if (yearString.isNotEmpty() && yearString != "") {
                    val year: Int = Integer.parseInt(yearString)
                    if (year < 1900) {
                        binding.edtYear.setText("1900")
                    }
                    if (year > Calendar.getInstance().get(Calendar.YEAR)) {
                        binding.edtYear.setText(Calendar.getInstance().get(Calendar.YEAR).toString())
                    }
                }
            }
            changeDobDividerColor()
        }


        binding.edtDay.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if (binding.edtDay.text.toString().length >= 2) {
                    binding.edtMonth.requestFocus()
                }
            }

        })
        binding.edtMonth.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if (binding.edtMonth.text.toString().length >= 2) {
                    binding.edtYear.requestFocus()
                }
            }

        })


    }

    private fun changeDobDividerColor() {
        if (binding.edtDay.text.toString().length >= 2) {
            binding.tvDaySeperator.setTextColor(resources.getColor(R.color.primary_error_color))
        } else {
            binding.tvDaySeperator.setTextColor(resources.getColor(R.color.color_hint))
        }
        if (binding.edtMonth.text.toString().length >= 2) {
            binding.tvMonthSeperator.setTextColor(resources.getColor(R.color.primary_error_color))
        } else {
            binding.tvMonthSeperator.setTextColor(resources.getColor(R.color.color_hint))
        }
    }

    private fun getGenotypeModes() {
        binding.progressBar.visibility = View.VISIBLE
        val manager = NetworkManager()
        if (manager.isConnectingToInternet(requireContext())) {
            manager.createApiRequest(
                ApiUtilis.getAPIInstance(activity!!).getGenoType(),
                object : ServiceListener<GenoTypeModel> {
                    override fun getServerResponse(response: GenoTypeModel, requestcode: Int) {
                        binding.progressBar.visibility = View.GONE
                        setGenotypeList(response)
                        readUserData()
                    }

                    override fun getError(error: ErrorModel, requestcode: Int) {

                        binding.progressBar.visibility = View.GONE
                    }
                })
        } else {
            binding.progressBar.visibility = View.GONE
            Toast.makeText(requireContext(), "Please check internet connection!", Toast.LENGTH_LONG)
                .show()
        }
    }

    private fun setGenotypeList(data: GenoTypeModel) {
        if (activity != null && activity!!.applicationContext != null) {
            adapter =
                GenoTypeAdapterForMyProfile(activity!!, data.getData()!!)
            binding.genoTypeGrid.adapter = adapter
        }
    }

    override fun onResume() {
        super.onResume()
//        changeButtonName()
    }

    private fun toggleEditableFields(isEditable: Boolean) {
        binding.edtHeight.isEnabled = isEditable
        binding.edtWeight.isEnabled = isEditable
        binding.edtDay.isEnabled = isEditable
        binding.edtMonth.isEnabled = isEditable
        binding.edtYear.isEnabled = isEditable

        if (isEditable) {
            binding.overlayAppPref.visibility = View.GONE
            binding.overlayMealGuidePref.visibility = View.GONE
            binding.overlayWorkoutPref.visibility = View.GONE
        } else {
            binding.overlayAppPref.visibility = View.VISIBLE
            binding.overlayMealGuidePref.visibility = View.VISIBLE
            binding.overlayWorkoutPref.visibility = View.VISIBLE
        }

        rb_height_cm.isEnabled = isEditable
        rb_height_ft.isEnabled = isEditable

        rb_weight_kg.isEnabled = isEditable
        rb_weight_lbs.isEnabled = isEditable

        rb_female.isEnabled = isEditable
        rb_male.isEnabled = isEditable

    }

    fun callKeyData(id: String, title: String?) {

        getContext()?.let { it1 ->
            PreferenceConnector.writeString(
                it1,
                PreferenceConnector.MODE_TEXT,
                title.toString()
            )
        }
        getContext()?.let { it1 ->
            PreferenceConnector.writeString(
                it1,
                PreferenceConnector.MODE_ID,
                id
            )
        }
        var title: String? = PreferenceConnector.readString(
            context!!.applicationContext,
            PreferenceConnector.MODE_TEXT,
            "Fitness"
        )
        var id: String? = PreferenceConnector.readString(
            context!!,
            PreferenceConnector.MODE_ID,
            "5c8c96a79c54381add6884da"
        )
    }

    @SuppressLint("CheckResult")
    private fun changeButtonName() {

        binding.progressBar.visibility = View.VISIBLE
        val userId = Base64.decode(Utility.getUserID(activity!!.themedContext), Base64.DEFAULT)
        val decodeUserId = String(userId, charset("UTF-8"))
        ApiUtilis.getAPIInstance(activity!!).getQuestionnaireStatus(userID = decodeUserId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ success ->
                run {
                    binding.progressBar.visibility = View.GONE
                    var isQuestionnaireCompleted = false

                    if (success.data?.get(0)?.status!! &&
                        success.data[1].status &&
                        success.data[2].status &&
                        success.data[3].status
                    ) {
                        isQuestionnaireCompleted = true
                    }
                    if (isQuestionnaireCompleted) {
                        binding.btnReviewQuestionnaire.setText(R.string.update_questionnaire)
                        binding.nestedScrollview.fullScroll(View.FOCUS_UP)

                    } else {
                        binding.btnReviewQuestionnaire.setText(R.string.review_questionnaire)
                        binding.nestedScrollview.fullScroll(View.FOCUS_UP)

                    }
                }
            }, { error ->
                run {
                    binding.progressBar.visibility = View.GONE
                }
            })

    }

    @SuppressLint("WrongConstant")
    private fun replaceFragment(fragment: Fragment) {
        val transaction = activity!!.supportFragmentManager.beginTransaction()
        transaction.replace(R.id.flContent, fragment)
        transaction.addToBackStack("share")
        transaction.commit()

    }

    private fun checkValidation(): Boolean {
        when {
            binding.edtHeight.text.toString().isEmpty() -> {
                binding.edtHeight.error = getString(R.string.enter_height_message)
                return false
            }
            binding.edtWeight.text.toString().isEmpty() -> {
                binding.edtWeight.error = getString(R.string.enter_weight_message)
                return false
            }
            binding.edtDay.text.toString().isEmpty() -> {
                binding.edtDay.error = getString(R.string.enter_date_of_birth)
                return false
            }
            binding.edtMonth.text.toString().isEmpty() -> {
                binding.edtMonth.error = getString(R.string.enter_date_of_birth)
                return false
            }
            binding.edtYear.text.toString().isEmpty() -> {
                binding.edtYear.error = getString(R.string.enter_date_of_birth)
                return false
            }
        }
        return true
    }

    fun setUserDataToUI() {
        if (userData != null) {
            if (userData!!.height_unit.equals("cm", ignoreCase = true)) {
                binding.rbHeightCm.isChecked = true
            } else {
                binding.rbHeightFt.isChecked = true
            }

            if (userData!!.weight_unit.equals("kg", ignoreCase = true)) {
                binding.rbWeightKg.isChecked = true
            } else {
                binding.rbWeightLbs.isChecked = true

            }
            if (userData!!.gender.equals("male", ignoreCase = true)) {
                binding.rbMale.isChecked = true
            } else {
                binding.rbFemale.isChecked = true

            }
            val date = userData!!.birthday!!.split("T")
            val yyyymmdd = date[0].split("-")
            val year = yyyymmdd[0]/*convertDateStringFormat(
                userData!!.birthday,
                "yyyy-mm-dd'T'HH:mm:ss.SSS'Z'",
                "yyyy"
            )*/
            val month = yyyymmdd[1]
            val day = yyyymmdd[2]

            binding.edtHeight.setText(userData!!.height.toString())
            binding.edtWeight.setText(userData!!.weight.toString())
            setUpHeightWeightRadioButtonListener()
            binding.edtDay.setText(day)
            binding.edtMonth.setText(month)
            binding.edtYear.setText(year)

            changeDobDividerColor()
        }
    }

    @SuppressLint("NewApi")
    fun readUserData() {
        context?.let {
            val userId = Base64.decode(
                Utility.getUserID(it),
                Base64.DEFAULT
            )
            val decodeUserId = String(userId, charset("UTF-8"))
            binding.progressBar.visibility = View.VISIBLE
            val getUserCondition = GetUserConditionReq(GetUserReq(decodeUserId))

            val manager = NetworkManager2()
            if (manager.isConnectingToInternet(binding.parentLayout.context)) {
                val call =
                    ApiUtilis.getAPIInstance(activity!!).getUserDetails(
                        getUserCondition
                    )
                call.enqueue(object : Callback<UserDataPojo> {
                    override fun onResponse(
                        call: Call<UserDataPojo>,
                        response: Response<UserDataPojo>
                    ) {
                        binding.progressBar.visibility = View.GONE
                        val userDataPojo: UserDataPojo? = response.body()
                        try {
                            if (userDataPojo != null && userDataPojo.statusCode == 200) {
                                userData = userDataPojo.data?.get(0)
                                setUserDataToUI()

                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }

                    override fun onFailure(call: Call<UserDataPojo>, t: Throwable) {
                        // Log error here since request failed
                        binding.progressBar.visibility = View.GONE
                        TempUtil.log("Login", t.toString())

                        Utility.displayShortSnackBar(
                            binding.parentLayout,
                            "Server connection error!!, please try again later."
                        )

                    }
                })
            } else {
                binding.progressBar.visibility = View.GONE

                Utility.displayShortSnackBar(
                    binding.parentLayout,
                    resources.getString(R.string.check_internet)
                )
            }
        }
    }

    private fun setUpHeightWeightRadioButtonListener() {
        binding.rgHeightMeasure.setOnCheckedChangeListener { radioGroup: RadioGroup, id: Int ->
            var height = 0.0
            if (binding.edtHeight.text.isNotEmpty()) {
                height = binding.edtHeight.text.toString().toDouble()
            }
            if (id == R.id.rb_height_cm) {
                binding.edtHeight.setText(String.format("%.2f", cmToFt(height)))
            } else {
                binding.edtHeight.setText(String.format("%.2f", ftToCm(height)))

            }
        }
        binding.rgWeightMeasure.setOnCheckedChangeListener { _: RadioGroup, id: Int ->
            var weight = 0.0
            if (!binding.edtWeight.text.isEmpty()) {
                weight = binding.edtWeight.text.toString().toDouble()
            }
            if (id == R.id.rb_weight_kg) {
                binding.edtWeight.setText(String.format("%.2f", lbsToKg(weight)))
            } else {
                binding.edtWeight.setText(String.format("%.2f", kgToLbs(weight)))
            }
        }
    }

    private fun uploadUserDetails() {
        binding.progressBar.visibility = View.VISIBLE
        val userId = Base64.decode(Utility.getUserID(activity!!.applicationContext), Base64.DEFAULT)

        val birthday =
            binding.edtYear.text.toString() + "-" + binding.edtMonth.text.toString() + "-" + binding.edtDay.text.toString() /*convertDateStringFormat(
             binding.edtYear.text.toString()+"-"+binding.edtMonth.text.toString()+"-"+binding.edtDay.text.toString(),
              "yyyy-MM-dd",
              "yyyy-mm-dd"
          )*/
        val decodeUserId = String(userId, charset("UTF-8"))
        val requestBody = UpdateUserProfileReq(
            user_id = decodeUserId,
            birthday = birthday, /*binding.edtYear.text.toString() + "-" + binding.edtMonth.text.toString() + "-" + binding.edtDay.text.toString()*/
            gender = binding.rgGender.getSelectedRadioButtonText(),
            weight = binding.edtWeight.text.toString(),
            weight_unit = binding.rgWeightMeasure.getSelectedRadioButtonText(),
            height = binding.edtHeight.text.toString(),
            height_unit = binding.rgHeightMeasure.getSelectedRadioButtonText()
        )
        val gson = Gson()
        TempUtil.log("Request Body", gson.toJson(requestBody))
        compositeDisposable.add(
            ApiUtilis.getAPIInstance(activity!!)
                .postUserprofileDetails(requestBody)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ success: BaseResponse ->
                    run {
                        binding.progressBar.visibility = View.GONE
                        Utility.displayShortSnackBar(
                            binding.parentLayout,
                            success.message
                        )
                    }
                }, { error: Throwable ->
                    run {
                        binding.progressBar.visibility = View.GONE
                        Utility.displayShortSnackBar(
                            binding.parentLayout,
                            error.localizedMessage
                        )
                    }
                })
        )
    }

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.dispose()
    }

}
