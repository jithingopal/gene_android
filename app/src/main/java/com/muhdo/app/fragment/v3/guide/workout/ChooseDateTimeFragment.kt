package com.muhdo.app.fragment.v3.guide.workout

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CompoundButton
import android.widget.TextView
import com.jaygoo.widget.OnRangeChangedListener
import com.jaygoo.widget.RangeSeekBar
import com.muhdo.app.R
import com.muhdo.app.databinding.V3LayoutPlanChooseDateTimeBinding
import com.muhdo.app.utils.v3.TempUtil
import com.timingsystemkotlin.backuptimingsystem.Utility
import kotlinx.android.synthetic.main.activity_recipe.*
import kotlinx.android.synthetic.main.v3_layout_plan_choose_date_time.view.*

class ChooseDateTimeFragment : Fragment() {
    lateinit var binding: V3LayoutPlanChooseDateTimeBinding
    var seekBarLeftValue: Int = 30
    var seekBarRightValue: Int = 45
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view =
            inflater.inflate(R.layout.v3_layout_plan_choose_date_time, container, false)
        binding = V3LayoutPlanChooseDateTimeBinding.bind(view)

        setupDaySelectionCheckBoxes();
        setupRangeSeekBarListener()
        return view
    }

    fun setupRangeSeekBarListener() {
        binding.trainingSeekBar.setOnRangeChangedListener(object :
            OnRangeChangedListener {
            override fun onRangeChanged(
                view: RangeSeekBar,
                leftValue: Float,
                rightValue: Float,
                isFromUser: Boolean
            ) {
                binding.trainingSeekBar.setOnRangeChangedListener(null)
                when (leftValue) {
                    0.0F -> {
                        if (seekBarLeftValue != 30) {
                            binding.trainingSeekBar.setValue(0.00001F, 33.333335F)
                        }
                        seekBarLeftValue = 30
                        seekBarRightValue = 45
                    }
                    33.333336F -> {
                        if (seekBarLeftValue != 45) {
                            binding.trainingSeekBar.setValue(33.333337F, 66.66668F)
                        }
                        seekBarLeftValue = 45
                        seekBarRightValue = 60
                    }
                    66.66667F -> {
                        seekBarLeftValue = 60
                        seekBarRightValue = 65
                    }
                    100.0F -> {
                        seekBarLeftValue = 61
                        seekBarRightValue = 65
                    }
                }

                when (rightValue) {
                    0.0F -> {
                        seekBarRightValue = 30
                        seekBarLeftValue = 30
                    }
                    33.333336F -> {

                        seekBarLeftValue = 30
                        seekBarRightValue = 45

                    }
                    66.66667F -> {
                        if (seekBarRightValue != 60) {
                            binding.trainingSeekBar.setValue(33.333337F, 66.66668F)
                        }

                        seekBarLeftValue = 45
                        seekBarRightValue = 60

                    }
                    100.0F -> {
                        if (seekBarRightValue != 65) {
                            binding.trainingSeekBar.setValue(66.66666F, 99.99999F)
                        }
                        seekBarLeftValue = 61
                        seekBarRightValue = 65
                    }
                }

                setupRangeSeekBarListener()
            }

            override fun onStartTrackingTouch(view: RangeSeekBar, isLeft: Boolean) {

            }

            override fun onStopTrackingTouch(view: RangeSeekBar, isLeft: Boolean) {

            }
        })
    }

    var cbDayAutoListener: CompoundButton.OnCheckedChangeListener =
        CompoundButton.OnCheckedChangeListener({ cb, isChecked ->
            removeAllCheckBoxListeners()
            getSelectedTimeRange()
            if (isChecked) {
                binding.cbDay1.isChecked = false
                binding.cbDay2.isChecked = false
                binding.cbDay3.isChecked = false
                binding.cbDay4.isChecked = false
                binding.cbDay5.isChecked = false
                binding.cbDay6.isChecked = false
                binding.cbDay7.isChecked = false

            }
            setupDaySelectionCheckBoxes()
        })

    fun setupDaySelectionCheckBoxes() {
        binding.cbDayAutoSelection.setOnCheckedChangeListener(cbDayAutoListener)
        binding.cbDay1.setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener({ cb, isChecked ->
            removeAllCheckBoxListeners()
            binding.cbDay1.isChecked = true
            binding.cbDay2.isChecked = false
            binding.cbDay3.isChecked = false
            binding.cbDay4.isChecked = false
            binding.cbDay5.isChecked = false
            binding.cbDay6.isChecked = false
            binding.cbDay7.isChecked = false
            binding.cbDayAutoSelection.isChecked = false
            setupDaySelectionCheckBoxes()

        }))
        binding.cbDay2.setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener({ cb, isChecked ->
            removeAllCheckBoxListeners()
            binding.cbDay1.isChecked = true
            binding.cbDay2.isChecked = true
            binding.cbDay3.isChecked = false
            binding.cbDay4.isChecked = false
            binding.cbDay5.isChecked = false
            binding.cbDay6.isChecked = false
            binding.cbDay7.isChecked = false
            binding.cbDayAutoSelection.isChecked = false
            setupDaySelectionCheckBoxes()

        }))
        binding.cbDay3.setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener({ cb, isChecked ->
            removeAllCheckBoxListeners()
            binding.cbDay1.isChecked = true
            binding.cbDay2.isChecked = true
            binding.cbDay3.isChecked = true
            binding.cbDay4.isChecked = false
            binding.cbDay5.isChecked = false
            binding.cbDay6.isChecked = false
            binding.cbDay7.isChecked = false
            binding.cbDayAutoSelection.isChecked = false
            setupDaySelectionCheckBoxes()

        }))
        binding.cbDay4.setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener({ cb, isChecked ->
            removeAllCheckBoxListeners()
            binding.cbDay1.isChecked = true
            binding.cbDay2.isChecked = true
            binding.cbDay3.isChecked = true
            binding.cbDay4.isChecked = true
            binding.cbDay5.isChecked = false
            binding.cbDay6.isChecked = false
            binding.cbDay7.isChecked = false
            binding.cbDayAutoSelection.isChecked = false
            setupDaySelectionCheckBoxes()

        }))
        binding.cbDay5.setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener({ cb, isChecked ->
            removeAllCheckBoxListeners()
            binding.cbDay1.isChecked = true
            binding.cbDay2.isChecked = true
            binding.cbDay3.isChecked = true
            binding.cbDay4.isChecked = true
            binding.cbDay5.isChecked = true
            binding.cbDay6.isChecked = false
            binding.cbDay7.isChecked = false
            binding.cbDayAutoSelection.isChecked = false
            setupDaySelectionCheckBoxes()

        }))
        binding.cbDay6.setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener({ cb, isChecked ->
            removeAllCheckBoxListeners()
            binding.cbDay1.isChecked = true
            binding.cbDay2.isChecked = true
            binding.cbDay3.isChecked = true
            binding.cbDay4.isChecked = true
            binding.cbDay5.isChecked = true
            binding.cbDay6.isChecked = true
            binding.cbDay7.isChecked = false
            binding.cbDayAutoSelection.isChecked = false
            setupDaySelectionCheckBoxes()

        }))
        binding.cbDay7.setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener({ cb, isChecked ->
            removeAllCheckBoxListeners()
            binding.cbDay1.isChecked = true
            binding.cbDay2.isChecked = true
            binding.cbDay3.isChecked = true
            binding.cbDay4.isChecked = true
            binding.cbDay5.isChecked = true
            binding.cbDay6.isChecked = true
            binding.cbDay7.isChecked = true
            binding.cbDayAutoSelection.isChecked = false
            setupDaySelectionCheckBoxes()

        }))
    }

    private fun removeAllCheckBoxListeners() {
        binding.cbDay1.setOnCheckedChangeListener(null)
        binding.cbDay2.setOnCheckedChangeListener(null)
        binding.cbDay3.setOnCheckedChangeListener(null)
        binding.cbDay4.setOnCheckedChangeListener(null)
        binding.cbDay5.setOnCheckedChangeListener(null)
        binding.cbDay6.setOnCheckedChangeListener(null)
        binding.cbDay7.setOnCheckedChangeListener(null)
    }

    fun getSelectedDays(): Int? {
        if (binding.cbDayAutoSelection.isChecked) {
            return 8
        } else if (binding.cbDay7.isChecked) {
            return 7
        } else if (binding.cbDay6.isChecked) {
            return 6
        } else if (binding.cbDay5.isChecked) {
            return 5
        } else if (binding.cbDay4.isChecked) {
            return 4
        } else if (binding.cbDay3.isChecked) {
            return 3
        } else if (binding.cbDay2.isChecked) {
            return 2
        } else if (binding.cbDay1.isChecked) {
            return 1
        } else {
            return null
        }
    }

    fun getSelectedTimeRange(): Array<Int> {
        TempUtil.log("Jithin ", seekBarLeftValue.toString() + " , " + seekBarRightValue.toString())

        return arrayOf(seekBarLeftValue, seekBarRightValue)
        /*30 -> {
            return arrayOf()

        }
        45 -> {
            return 19
        }
        60 -> {
            return 20
        }
        else -> {
            return 17
        }*/

    }

}
