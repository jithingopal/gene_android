package com.muhdo.app.fragment.v3.guide.workout

import android.content.Context
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.muhdo.app.R
import com.muhdo.app.databinding.V3FragmentChooseGoalFragmentBinding
import com.muhdo.app.databinding.V3LayoutPlanChooseSubGoalBinding
import com.muhdo.app.utils.v3.getSelectedRadioButtonText

class ChooseSubGoalFragment : Fragment() {
    lateinit var binding: V3LayoutPlanChooseSubGoalBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view =
            inflater.inflate(R.layout.v3_layout_plan_choose_sub_goal, container, false)
        binding = V3LayoutPlanChooseSubGoalBinding.bind(view)
        return view
    }

    fun getSelectedSubGoal(): String? {
        when (binding.rgSubGoal.checkedRadioButtonId) {
            -1 -> {
                return null
            }
            R.id.rbTone -> {
                return "B"
            }
            R.id.rbBodyBuilding -> {
                return "A"
            }
            R.id.rbPowerLifter -> {
                return "C"
            }
            else -> {
                return null
            }
        }
    }


}
