package com.muhdo.app.fragment

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Base64
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.ViewPager
import com.google.gson.Gson
import com.muhdo.app.R
import com.muhdo.app.adapter.BannerSlidingLayoutAdapter
import com.muhdo.app.adapter.v3.HomeAdapter
import com.muhdo.app.apiModel.pollution.AirIndexData
import com.muhdo.app.apiModel.pollution.AirIndexModel
import com.muhdo.app.databinding.V3FragmentHomeBinding
import com.muhdo.app.fragment.v3.GeneticActionPlanHomeFragment
import com.muhdo.app.fragment.v3.QuestionnaireHomeFragment
import com.muhdo.app.fragment.v3.health_insights.HealthInsightsFragment
import com.muhdo.app.model.HomeItem
import com.muhdo.app.repository.ApiUtilis
import com.muhdo.app.repository.ErrorModel
import com.muhdo.app.repository.NetworkManager
import com.muhdo.app.repository.ServiceListener
import com.muhdo.app.ui.DashboardActivity
import com.muhdo.app.ui.PollutionActivity
import com.muhdo.app.utils.PreferenceConnector
import com.muhdo.app.utils.PreferenceConnector.BIOLOGICAL_AGE
import com.muhdo.app.utils.v3.Constants
import com.muhdo.app.utils.v3.TempUtil
import com.timingsystemkotlin.backuptimingsystem.Utility
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.v3_fragment_home.*
import org.jetbrains.anko.toast
import java.util.*
import kotlin.collections.set

class HomeFragment : Fragment(), HomeAdapter.HomeAdapterClickListener {

    override fun onItemClick(position: Int) {
        when (position) {
/*            0 -> {
                val i = Intent(activity, DashboardActivity::class.java)
                i.putExtra(Constants.DASHBOARD_ACTION, Constants.DASHBOARD_GENETIC_OVERVIEW)
                startActivity(i)
            }*/
            2 -> {
                binding.progressBar.visibility = View.VISIBLE
                val userId =
                    Base64.decode(Utility.getUserID(activity!!.applicationContext), Base64.DEFAULT)
                val decodeUserId = String(userId, charset("UTF-8"))
                ApiUtilis.getAPIInstance(activity!!).getQuestionnaireStatus(userID = decodeUserId)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({ success ->
                        run {
                            binding.progressBar.visibility = View.GONE
                            var isQuestionnaireCompleted = false

                            if (success.data?.get(0)?.status!! &&
                                success.data.get(1).status &&
                                success.data.get(2).status &&
                                success.data.get(3).status
                            ) {
                                isQuestionnaireCompleted = true
                            }
                            if (isQuestionnaireCompleted) {

                                TempUtil.currentFragment = TempUtil.NO_FRAGMENTS_SELECTED
                                replaceFragment(GeneticActionPlanHomeFragment())

                            } else {
                                val dialog: androidx.appcompat.app.AlertDialog.Builder =
                                    androidx.appcompat.app.AlertDialog.Builder(activity!!.themedContext)
                                dialog.setMessage(R.string.fill_questionnaire_warning_while_generate_action_plan)
                                dialog.setPositiveButton(
                                    R.string.btn_go_to_questionnaire_warning
                                ) { dialog, id ->
                                    dialog.cancel()
                                    TempUtil.currentFragment =
                                        1000// binding.navigationView.selectedItemId
                                    replaceFragment(QuestionnaireHomeFragment())
                                    dialog.cancel()
                                }
                                dialog.setNegativeButton(
                                    R.string.btn_cancel
                                ) { dialog, id ->
                                    dialog.cancel()
                                }
                                dialog.show()
                            }
                        }
                    }, { error ->
                        run {
                            binding.progressBar.visibility = View.GONE
                        }
                    })
            }
            0 -> {
                val i = Intent(activity, DashboardActivity::class.java)
                i.putExtra(Constants.DASHBOARD_ACTION, Constants.DASHBOARD_RESULT)
                startActivity(i)
            }
            1 -> {
                val i = Intent(activity, DashboardActivity::class.java)
                i.putExtra(Constants.DASHBOARD_ACTION, Constants.DASHBOARD_HEALTHINSIGHT)
                startActivity(i)
            }
            3 -> {
                val i = Intent(activity, DashboardActivity::class.java)
                i.putExtra(Constants.DASHBOARD_ACTION, Constants.DASHBOARD_EPIGENETIC_RESULT)
                startActivity(i)
                activity?.finish()
            }
            4 -> {
          /*      val i = Intent(activity, DashboardActivity::class.java)
                i.putExtra(Constants.DASHBOARD_ACTION, Constants.LIFESTYLE_TRACKING)
                startActivity(i)*/
                context?.toast(getString(R.string.comin_soon))
            }
            5 -> {
                val i = Intent(activity, DashboardActivity::class.java)
                i.putExtra(Constants.DASHBOARD_ACTION, Constants.DASHBOARD_MEAL_PLAN)
                startActivity(i)
                activity!!.finish()
            }
            6 -> {
                val i = Intent(activity, DashboardActivity::class.java)
                i.putExtra(Constants.DASHBOARD_ACTION, Constants.DASHBOARD_WORKOUT)
                startActivity(i)
                activity!!.finish()
            }
        }
    }

    @SuppressLint("WrongConstant")
    private fun replaceFragment(fragment: Fragment) {

        val transaction = activity!!.supportFragmentManager.beginTransaction()
        transaction.replace(R.id.flContent, fragment)
        transaction.addToBackStack("share")
        transaction.commit()
    }

    private lateinit var homeItems: MutableList<HomeItem>
    private lateinit var binding: V3FragmentHomeBinding
    var response: AirIndexData? = null
    var positionVal = 0
    var currentPage = 0

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.v3_fragment_home, container, false)
        binding = V3FragmentHomeBinding.bind(view)
        setupViewPager(binding.pollutionBar.bannerSlider, context)
        if (!Utility.getLat(requireContext()).isNullOrEmpty() && !Utility.getLong(requireContext()).isNullOrEmpty()) {
            getPollutionData(
                Utility.getLong(requireContext())!!,
                Utility.getLat(requireContext())!!
            )
        } else {
            Toast.makeText(requireContext(), "Unable to fetch location", Toast.LENGTH_SHORT).show()
        }
        binding.pollutionBar.complianceLayout.setOnClickListener {
            if (response != null) {
                val gson = Gson()
                val i = Intent(requireContext(), PollutionActivity::class.java)
                i.putExtra("PollutionData", gson.toJson(response))
                startActivity(i)
            } else {
                Toast.makeText(requireContext(), "Something went wrong", Toast.LENGTH_LONG).show()
            }
        }

        setupViews()
        activity!!.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)

        val biologicalAge = Integer.parseInt(
            PreferenceConnector.readString(
                activity!!,
                BIOLOGICAL_AGE,
                "0"
            )
        )
        Log.d("data", "age $biologicalAge")
        val sleepIndex =
            context?.let { PreferenceConnector.readString(it, PreferenceConnector.SLEEP_INDEX, "") }
        Log.d("data", "sleep ind$sleepIndex")
        try {
            if (biologicalAge > 0) {
                binding.pollutionBar.biologicalAgeView.visibility = View.VISIBLE
                binding.pollutionBar.biologicalAge.text = biologicalAge.toString()
            } else {
                binding.pollutionBar.complianceLayout.visibility = View.VISIBLE
            }
        } catch (e: Exception) {

        }

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpHomeItems()
    }

    private fun setUpHomeItems() {
        homeItems = createHomeItems()
        rv_home.layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        val adapter = HomeAdapter(homeItems, this)
        rv_home.adapter = adapter
    }

    private fun createHomeItems(): MutableList<HomeItem> {

        val item8 =
            HomeItem(
                R.drawable.ic_new_dna_results,
                getString(R.string.genetic_overview).toUpperCase(Locale.ENGLISH)
            )

        val item1 =
            HomeItem(
                R.drawable.ic_new_genetic_action_plan,
                getString(R.string.genetic_action_plan).toUpperCase(Locale.ENGLISH)
            )

        val item =
            HomeItem(
                R.drawable.ic_new_dna_results,
                getString(R.string.dna_results).toUpperCase(Locale.ENGLISH)
            )

        val item2 =
            HomeItem(
                R.drawable.ic_new_health_insights,
                getString(R.string.health_insights).toUpperCase(Locale.ENGLISH)
            )
        val item3 =
            HomeItem(
                R.drawable.ic_new_epigenetic_results,
                getString(R.string.epigenetic_results).toUpperCase(Locale.ENGLISH)
            )
        val item5 =
            HomeItem(
                R.drawable.ic_new_lifestyle_tracking,
                getString(R.string.lifestyle_tracking).toUpperCase(Locale.ENGLISH)
            )
        val item6 =
            HomeItem(
                R.drawable.ic_new_meal_guide,
                getString(R.string.my_meal_guide).toUpperCase(Locale.ENGLISH)
            )
        val item7 =
            HomeItem(
                R.drawable.ic_new_my_training,
                getString(R.string.v3_my_traing).toUpperCase(Locale.ENGLISH)
            )

        val homeListItems = mutableListOf<HomeItem>()
       // homeListItems.add(item8)
        homeListItems.add(item)
        homeListItems.add(item2)
        homeListItems.add(item1)
        homeListItems.add(item3)
        homeListItems.add(item5)
        homeListItems.add(item6)
        homeListItems.add(item7)
        return homeListItems
    }

    private fun setupViews() {
        binding.cardView.setOnClickListener {
            val i = Intent(activity, DashboardActivity::class.java)
            i.putExtra(Constants.DASHBOARD_ACTION, Constants.DASHBOARD_RESULT)
            startActivity(i)
        }

        binding.cardViewEpigenticResults.setOnClickListener {
            val i = Intent(activity, DashboardActivity::class.java)
            i.putExtra(Constants.DASHBOARD_ACTION, Constants.DASHBOARD_EPIGENETIC_RESULT)
            startActivity(i)
        }
        binding.cardViewGeneticOverview.setOnClickListener {
            val i = Intent(activity, DashboardActivity::class.java)
            i.putExtra(Constants.DASHBOARD_ACTION, Constants.DASHBOARD_GENETIC_OVERVIEW)
            startActivity(i)
        }


        binding.cardViewGeneticActionPlan.setOnClickListener {
            binding.progressBar.visibility = View.VISIBLE
            val userId =
                Base64.decode(Utility.getUserID(activity!!.applicationContext), Base64.DEFAULT)
            val decodeUserId = String(userId, charset("UTF-8"))
            ApiUtilis.getAPIInstance(activity!!).getQuestionnaireStatus(userID = decodeUserId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ success ->
                    run {
                        binding.progressBar.visibility = View.GONE
                        var isQuestionnaireCompleted = false

                        if (success.data?.get(0)?.status!! &&
                            success.data.get(1).status &&
                            success.data.get(2).status &&
                            success.data.get(3).status
                        ) {
                            isQuestionnaireCompleted = true
                        }
                        if (isQuestionnaireCompleted) {

                            TempUtil.currentFragment = TempUtil.NO_FRAGMENTS_SELECTED
                            replaceFragment(GeneticActionPlanHomeFragment())

                        } else {
                            val dialog: androidx.appcompat.app.AlertDialog.Builder =
                                androidx.appcompat.app.AlertDialog.Builder(activity!!.themedContext)
                            dialog.setMessage(R.string.fill_questionnaire_warning_while_generate_action_plan)
                            dialog.setPositiveButton(
                                R.string.btn_go_to_questionnaire_warning
                            ) { dialog, id ->
                                dialog.cancel()
                                TempUtil.currentFragment =
                                    1000// binding.navigationView.selectedItemId
                                replaceFragment(QuestionnaireHomeFragment())
                                dialog.cancel()
                            }
                            dialog.setNegativeButton(
                                R.string.btn_cancel
                            ) { dialog, id ->
                                dialog.cancel()
                            }
                            dialog.show()
                        }
                    }
                }, { error ->
                    run {
                        binding.progressBar.visibility = View.GONE
                    }
                })

        }
        binding.cardViewMealGuide.setOnClickListener {
            val i = Intent(activity, DashboardActivity::class.java)
            i.putExtra(Constants.DASHBOARD_ACTION, Constants.DASHBOARD_MEAL_PLAN)
            startActivity(i)
            activity!!.finish()
        }
        binding.cardViewMyTraining.setOnClickListener {
            val i = Intent(activity, DashboardActivity::class.java)
            i.putExtra(Constants.DASHBOARD_ACTION, Constants.DASHBOARD_WORKOUT)
            startActivity(i)
            activity!!.finish()
        }
        binding.cardViewEpigenticTracking.setOnClickListener {
            activity!!.toast(R.string.comin_soon)
        }
        binding.cardViewHealthInsights.setOnClickListener {
            replaceFragment(HealthInsightsFragment())
            (activity as DashboardActivity).removeBottomNavColor()
            TempUtil.currentFragment = TempUtil.NO_FRAGMENTS_SELECTED
        }
    }


    private fun getPollutionData(long: String, lat: String) {
        binding.progressBar.visibility = View.VISIBLE
        val params = HashMap<String, String>()
        params["latlong"] = "$lat, $long"
        params["user_id"] = Utility.getUserID(requireContext())

        val manager = NetworkManager()
        if (manager.isConnectingToInternet(requireContext())) {
            manager.createApiRequest(
                ApiUtilis.getAPIInstance(activity!!).getPollutionData(params),
                object : ServiceListener<AirIndexModel> {
                    override fun getServerResponse(response: AirIndexModel, requestcode: Int) {
                        binding.progressBar.visibility = View.GONE
                        if (response.getStatusCode() == 200) {
                        } else {
                            if (activity != null) {
                                Toast.makeText(activity, "Something went wrong", Toast.LENGTH_LONG)
                                    .show()
                            }
                        }
                    }

                    override fun getError(error: ErrorModel, requestcode: Int) {
                        if (activity != null && activity!!.applicationContext != null) {
                        }
                        binding.progressBar.visibility = View.GONE
                    }
                })
        } else {
            binding.progressBar.visibility = View.GONE
            Toast.makeText(requireContext(), "Please check internet connection!", Toast.LENGTH_LONG)
                .show()

        }
    }


    private fun setupViewPager(viewPager: ViewPager, context: Context?) {
        val adapter = BannerSlidingLayoutAdapter(childFragmentManager)

        try {
            val biologicalAge =
                Integer.parseInt(PreferenceConnector.readString(activity!!, BIOLOGICAL_AGE, "0"))
            if (biologicalAge > 0) {
                adapter.addFragment(BannerSlideBiologicalAge())
            } else {
                adapter.addFragment(BannerSlidePollutionIndex())
            }
        } catch (e: Exception) {
            adapter.addFragment(BannerSlidePollutionIndex())
        }

        adapter.addFragment(BannerSleepIndexFragment())
        viewPager.adapter = adapter
        binding.indicator.setViewPager(viewPager)
        val density = resources.displayMetrics.density
        //Set circle indicator radius
        binding.indicator.setRadius(5 * density)

        // Pager listener over indicator
        binding.indicator.setOnPageChangeListener(object : ViewPager.OnPageChangeListener {

            override fun onPageSelected(position: Int) {
                currentPage = position
                positionVal = position

            }

            override fun onPageScrolled(pos: Int, arg1: Float, arg2: Int) {

            }

            override fun onPageScrollStateChanged(pos: Int) {

            }
        })


    }


}