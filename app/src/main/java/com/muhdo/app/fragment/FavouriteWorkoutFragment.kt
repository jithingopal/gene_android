package com.muhdo.app.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.muhdo.app.R
import com.muhdo.app.adapter.WorkoutFavouriteAdapter
import com.muhdo.app.apiModel.workout.WorkoutDay
import com.muhdo.app.apiModel.workout.WorkoutFavouriteModel
import com.muhdo.app.databinding.FragmentFavouriteWorkoutBinding
import com.muhdo.app.repository.ApiUtilis
import com.muhdo.app.repository.ErrorModel
import com.muhdo.app.repository.NetworkManager
import com.muhdo.app.repository.ServiceListener
import com.timingsystemkotlin.backuptimingsystem.Utility


class FavouriteWorkoutFragment : Fragment() {
    internal lateinit var binding: FragmentFavouriteWorkoutBinding
    internal var adapter: WorkoutFavouriteAdapter? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_favourite_workout, container, false)
        binding = FragmentFavouriteWorkoutBinding.bind(view)

        return view
    }


    override fun onResume() {
        super.onResume()
        getFavouriteList()
    }

    fun getFavouriteList() {
        binding.progressBar.visibility = View.VISIBLE
        val manager = NetworkManager()
        if (manager.isConnectingToInternet(activity!!.applicationContext)) {


            manager.createApiRequest(
                ApiUtilis.getAPIInstance(activity!!).getFavouriteWorkout(Utility.getUserID(requireContext())),
                object : ServiceListener<WorkoutFavouriteModel> {
                    override fun getServerResponse(response: WorkoutFavouriteModel, requestcode: Int) {
                        setList(response.getData()!!)
                        binding.progressBar.visibility = View.GONE
                    }

                    override fun getError(error: ErrorModel, requestcode: Int) {
                        binding.progressBar.visibility = View.GONE

                    }
                })
        } else {
            binding.progressBar.visibility = View.GONE

        }

    }


    private fun setList(data: List<WorkoutDay>) {
        if (data.isEmpty()) {
            binding.txtMessage.visibility = View.VISIBLE
        }
        else{
            binding.txtMessage.visibility = View.GONE
        }

        adapter = WorkoutFavouriteAdapter(this, data)
        binding.list.setAdapter(adapter)
    }



}