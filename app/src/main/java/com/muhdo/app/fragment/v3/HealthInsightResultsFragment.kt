package com.muhdo.app.fragment.v3

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import androidx.viewpager.widget.ViewPager
import com.muhdo.app.R
import com.muhdo.app.databinding.FragmentHealthInsightResultsBinding
import com.muhdo.app.utils.v3.Constants
import com.muhdo.app.utils.v3.TabUtils
import com.muhdo.app.utils.v3.TempUtil


class HealthInsightResultsFragment : Fragment() {

    private var firstSelectPos = 0
    lateinit var binding: FragmentHealthInsightResultsBinding

    var tab_icons = arrayOf<Int>(
        R.drawable.v3_ic_health_insight_stress,
        R.drawable.v3_ic_health_insight_sleep,
        R.drawable.v3_ic_health_insight_ani_ageing,
        R.drawable.v3_ic_health_insight_injury_risk,
        //    R.drawable.heart_health_m,
        R.drawable.v3_ic_health_insight_mentel_health,
        R.drawable.v3_ic_health_insight_gut_health,
        //   R.drawable.addiction_m,
        //   R.drawable.skin_ageing_m,
        R.drawable.v3_ic_health_insight_skin_health,
        R.drawable.v3_ic_health_insight_eye_health,
        R.drawable.v3_ic_health_insight_muscle_health
    )

    private val tabLabelMap = createTabLabelMap()

    val tab = HashMap<String, String>()

    var tab_label =
        arrayOf(
            R.string.stress,
            R.string.sleep,
            R.string.anti_aging,
            R.string.injury_prevention,
            //       R.string.heart_health,
            R.string.mental_health,
            R.string.gut_health,
            //   R.string.addiction,
            //   R.string.skin_aging,
            R.string.skin_health,
            R.string.eye_health,
            R.string.muscle_health
        )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_health_insight_results, container, false)
        binding = FragmentHealthInsightResultsBinding.bind(view)
        setupTabIcons()
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        selectTabItem(firstSelectPos)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
    }


    override fun onDetach() {
        super.onDetach()
    }

    private fun selectTabItem(position: Int) {
        binding.tabLayout.getTabAt(position)!!.select()
    }

    fun selectedTabPosition(position: Int) {
        firstSelectPos = position
    }

    private fun createTabLabelMap(): MutableMap<Int, String> {
        val mapper = mutableMapOf<Int, String>()
        mapper[R.string.stress] = Constants.HEALTH_INSIGHTS.STRESS
        mapper[R.string.sleep] = Constants.HEALTH_INSIGHTS.SLEEP
        mapper[R.string.anti_aging] = Constants.HEALTH_INSIGHTS.ANTI_AGEING
        mapper[R.string.injury_prevention] = Constants.HEALTH_INSIGHTS.INJURY_RISK
        //    mapper[R.string.heart_health] = Constants.HEALTH_INSIGHTS.HEART_HEALTH
        mapper[R.string.mental_health] = Constants.HEALTH_INSIGHTS.MENTAL_HEALTH
        mapper[R.string.gut_health] = Constants.HEALTH_INSIGHTS.GUT_HEALTH
        mapper[R.string.skin_health] = Constants.HEALTH_INSIGHTS.SKIN_HEALTH
        mapper[R.string.eye_health] = Constants.HEALTH_INSIGHTS.EYE_HEALTH
        mapper[R.string.muscle_health] = Constants.HEALTH_INSIGHTS.MUSCLE_HEALTH
        return mapper
    }

    @SuppressLint("ResourceAsColor", "InflateParams")
    private fun setupTabIcons() {

        val fragmentAdapter = ViewPagerAdapter(activity!!.supportFragmentManager)
        binding.viewPager.adapter = fragmentAdapter
        binding.indicator.setViewPager(binding.viewPager)
        val density = resources.displayMetrics.density
//        binding.indicator.setRadius(5 * density)
        binding.tabLayout.setupWithViewPager(binding.viewPager)
        val adapter = ViewPagerAdapter(activity!!.supportFragmentManager)
        binding.viewPager.offscreenPageLimit = 0
        binding.viewPager.adapter = adapter
        for ((key, value) in tabLabelMap) {
            val fragment = if (value == Constants.HEALTH_INSIGHTS.SLEEP)
                ResultChooseModeFragment() else {
                val fragment = HealthInsightsResultItemFragment()
                val bundle = Bundle()
                bundle.putString("apiRoute", value)
                fragment.arguments = bundle
                fragment
            }
            adapter.addFrag(fragment, getString(key))
        }

        binding.viewPager.adapter = adapter

        for (i in 0 until tab_label.size) {
            val tabView = LayoutInflater.from(activity!!.applicationContext)
                .inflate(R.layout.custom_tab, null)
            tabView.minimumWidth = ((TabUtils.getScreenWidth(activity) / 3.0f).toInt())
            val tabLabel = tabView.findViewById(R.id.tab) as TextView
            val tabImage = tabView.findViewById(R.id.tab_img) as ImageView
            tabImage.setColorFilter(
                ContextCompat.getColor(
                    activity!!.applicationContext,
                    R.color.v3_tint_tab_unselected_color
                ), android.graphics.PorterDuff.Mode.SRC_IN
            );
            tabLabel.text = getString(tab_label[i])
            tabImage.setImageResource(tab_icons[i])
            tabLabel.setTextColor(context!!.resources.getColor(R.color.v3_tint_tab_unselected_color))
            binding.tabLayout.getTabAt(i)!!.customView = tabView
        }
        val tabLabel =
            binding.tabLayout.getTabAt(0)!!.customView!!.findViewById(R.id.tab) as TextView
        val tabImage =
            binding.tabLayout.getTabAt(0)!!.customView!!.findViewById(R.id.tab_img) as ImageView
        tabImage.setColorFilter(
            ContextCompat.getColor(
                activity!!.applicationContext,
                R.color.v3_tint_tab_selected_color
            ), android.graphics.PorterDuff.Mode.SRC_IN
        )
        tabLabel.setTextColor(context!!.resources.getColor(R.color.v3_tint_tab_selected_color))
        binding.viewPager.addOnPageChangeListener((object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {

            }

            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {

            }

            override fun onPageSelected(position: Int) {
                for (i in 0 until tab_label.size) {
                    val tabLabel =
                        binding.tabLayout.getTabAt(i)!!.customView!!.findViewById(R.id.tab) as TextView
                    val tabImage =
                        binding.tabLayout.getTabAt(i)!!.customView!!.findViewById(R.id.tab_img) as ImageView
                    tabImage.setColorFilter(
                        ContextCompat.getColor(
                            activity!!.applicationContext,
                            R.color.v3_tint_tab_unselected_color
                        ), android.graphics.PorterDuff.Mode.SRC_IN
                    )
                    tabLabel.setTextColor(context!!.resources.getColor(R.color.v3_tint_tab_unselected_color))
                }

                val tabLabel =
                    binding.tabLayout.getTabAt(position)!!.customView!!.findViewById(R.id.tab) as TextView
                val tabImage =
                    binding.tabLayout.getTabAt(position)!!.customView!!.findViewById(R.id.tab_img) as ImageView
                tabImage.setColorFilter(
                    ContextCompat.getColor(
                        activity!!.applicationContext,
                        R.color.v3_tint_tab_selected_color
                    ), android.graphics.PorterDuff.Mode.SRC_IN
                )
                tabLabel.setTextColor(context!!.resources.getColor(R.color.v3_tint_tab_selected_color))
            }
        }))
    }

    internal inner class ViewPagerAdapter(manager: FragmentManager) :
        FragmentStatePagerAdapter(manager) {
        private val mFragmentList = arrayListOf<Fragment>()
        private val mFragmentTitleList = arrayListOf<String>()

        override fun getItem(position: Int) = mFragmentList.get(position)

        override fun getCount() = mFragmentList.size

        fun addFrag(fragment: Fragment, title: String) {
            mFragmentList.add(fragment)
            mFragmentTitleList.add(title)
        }

        override fun getPageTitle(position: Int): CharSequence? {
            TempUtil.log("data", "tab pos = $position")
            return mFragmentTitleList[position]
        }
    }
}
