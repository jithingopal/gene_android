package com.muhdo.app.fragment.v3.guide.meal.choose_meal

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import androidx.viewpager.widget.ViewPager
import com.muhdo.app.R
import com.muhdo.app.apiModel.mealModel.ChooseMealModel
import com.muhdo.app.apiModel.mealModel.ChooseMealSuccess
import com.muhdo.app.apiModel.mealModel.Recipe
import com.muhdo.app.apiModel.mealModel.UpdateMealPlanRequest
import com.muhdo.app.databinding.V3FragmentChooseMealBinding
import com.muhdo.app.fragment.v3.MealFragment
import com.muhdo.app.repository.ApiUtilis
import com.muhdo.app.repository.ErrorModel
import com.muhdo.app.repository.NetworkManager
import com.muhdo.app.repository.ServiceListener
import com.muhdo.app.utils.v3.TempUtil
import com.timingsystemkotlin.backuptimingsystem.Utility

class ChooseMealFragment(private var selectedDay: Int = 0) : Fragment() {
    lateinit var binding: V3FragmentChooseMealBinding

    var fragmentMonday: ChooseMealForDayFragment = ChooseMealForDayFragment(0)
    var fragmentTuesday: ChooseMealForDayFragment = ChooseMealForDayFragment(1)
    var fragmentWednesday: ChooseMealForDayFragment = ChooseMealForDayFragment(2)
    var fragmentThursday: ChooseMealForDayFragment = ChooseMealForDayFragment(3)
    var fragmentFriday: ChooseMealForDayFragment = ChooseMealForDayFragment(4)
    var fragmentSaturday: ChooseMealForDayFragment = ChooseMealForDayFragment(5)
    var fragmentSunday: ChooseMealForDayFragment = ChooseMealForDayFragment(6)
    var tab_label =
        arrayOf<Int>(
            R.string.monday,
            R.string.tuesday,
            R.string.wednesday,
            R.string.thursday,
            R.string.friday,
            R.string.saturday,
            R.string.sunday
        )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view =
            inflater.inflate(R.layout.v3_fragment_choose_meal, container, false)
        binding = V3FragmentChooseMealBinding.bind(view)
        getMealList()
        return view
    }

    fun setupViewPager() {

        val adapter = ViewPagerAdapter(activity!!.supportFragmentManager)
        binding.viewPager.offscreenPageLimit = 7
        binding.viewPager.adapter = adapter
        for (i in 0 until tab_label.size) {
            when (i) {

                0 -> {
                    adapter.addFrag(fragmentMonday, getString(tab_label[i]))
                }
                1 -> {
                    adapter.addFrag(fragmentTuesday, getString(tab_label[i]))
                };
                2 -> {
                    adapter.addFrag(fragmentWednesday, getString(tab_label[i]))
                };
                3 -> {
                    adapter.addFrag(fragmentThursday, getString(tab_label[i]))
                };
                4 -> {
                    adapter.addFrag(fragmentFriday, getString(tab_label[i]))
                };
                5 -> {
                    adapter.addFrag(fragmentSaturday, getString(tab_label[i]))
                };
                6 -> {
                    adapter.addFrag(fragmentSunday, getString(tab_label[i]))
                }
            }
        }
        binding.viewPager.adapter = adapter
        binding.viewPager.addOnPageChangeListener(
            object : ViewPager.OnPageChangeListener {

                override fun onPageScrollStateChanged(state: Int) {
                }

                override fun onPageScrolled(
                    position: Int,
                    positionOffset: Float,
                    positionOffsetPixels: Int
                ) {

                }

                override fun onPageSelected(position: Int) {
                    binding.tvDayName.setText(tab_label[position])
                }

            })
        binding.btnContinueToMealPlan.setOnClickListener {
            /* TempUtil.log("Jithin",getAllSelectedMealList().size.toString())
             TempUtil.log("Jithin",Gson().toJson(getAllSelectedMealList()))*/
            var listData: ArrayList<Recipe> = ArrayList<Recipe>(0)
            for (recipe in getAllSelectedMealList()) {
                listData.add(recipe)
            }
            sendMealToServer(listData)
        }
        binding.imgNext.setOnClickListener {

            if (binding.viewPager.currentItem < 6) {
                binding.viewPager.setCurrentItem(binding.viewPager.currentItem + 1)
            }
        }
        binding.imgPrevious.setOnClickListener {
            if (binding.viewPager.currentItem > 0) {
                binding.viewPager.setCurrentItem(binding.viewPager.currentItem - 1)
            }
        }
        binding.indicator.setViewPager(binding.viewPager)
        val density = resources.displayMetrics.density
        binding.indicator.setRadius(4 * density)
        binding.viewPager.setCurrentItem(selectedDay)

    }

    internal inner class ViewPagerAdapter(manager: FragmentManager) :
        FragmentStatePagerAdapter(manager) {
        private val mFragmentList = arrayListOf<Fragment>()
        private val mFragmentTitleList = arrayListOf<String>()

        override fun getItem(position: Int): Fragment {

            return mFragmentList.get(position)
        }

        override fun getCount(): Int {
            return mFragmentList.size
        }

        fun addFrag(fragment: Fragment, title: String) {
            mFragmentList.add(fragment)
            mFragmentTitleList.add(title)
        }

        override fun getPageTitle(position: Int): CharSequence? {
            TempUtil.log("data", "tab pos = " + position)
            return mFragmentTitleList[position]
        }
    }

    private fun getMealList() {
        binding.progressBar.visibility = View.VISIBLE
        val manager = NetworkManager()
        if (manager.isConnectingToInternet(activity!!.applicationContext)) {
            manager.createApiRequest(
                ApiUtilis.getAPIInstance(activity!!).getChoosePlanListV3(
                    Utility.getUserID(activity!!.applicationContext)
                ),
                object : ServiceListener<ChooseMealModel> {
                    override fun getServerResponse(response: ChooseMealModel, requestcode: Int) {
                        setupViewPager()
                        var data: ChooseMealModel? = null
                        data = response
                        fragmentMonday.setParentItems(data.getData()?.getMonday())
                        fragmentTuesday.setParentItems(data.getData()?.getTuesday())
                        fragmentWednesday.setParentItems(data.getData()?.getWednesday())
                        fragmentThursday.setParentItems(data.getData()?.getThursday())
                        fragmentFriday.setParentItems(data.getData()?.getFriday())
                        fragmentSaturday.setParentItems(data.getData()?.getSaturday())
                        fragmentSunday.setParentItems(data.getData()?.getSunday())
                        binding.progressBar.visibility = View.GONE
                    }

                    override fun getError(error: ErrorModel, requestcode: Int) {
                        binding.progressBar.visibility = View.GONE
                        Utility.displayShortSnackBar(
                            binding.parentLayout,
                            error.getMessage().toString()
                        )
                    }
                })
        } else {
            binding.progressBar.visibility = View.GONE
            Utility.displayShortSnackBar(
                binding.parentLayout,
                resources.getString(R.string.check_internet)
            )
        }
    }


    private fun getAllSelectedMealList(): MutableList<Recipe> {
        var listData: MutableList<Recipe> = ArrayList<Recipe>()
        listData.addAll(fragmentMonday.getSelectedMealList())
        listData.addAll(fragmentTuesday.getSelectedMealList())
        listData.addAll(fragmentWednesday.getSelectedMealList())
        listData.addAll(fragmentThursday.getSelectedMealList())
        listData.addAll(fragmentFriday.getSelectedMealList())
        listData.addAll(fragmentSaturday.getSelectedMealList())
        listData.addAll(fragmentSunday.getSelectedMealList())
        return listData
    }

    private fun sendMealToServer(listData: ArrayList<Recipe>) {
        binding.progressBar.visibility = View.VISIBLE
        binding.viewPager.visibility = View.INVISIBLE


//        val listData = ArrayList<Recipe>()
        /*for (i in 0 until Utility.jsonList.length()) {
            val json = JSONObject(Utility.jsonList.get(i).toString())
            val recipe = Recipe(json.getString("recipe_id"), json.getString("day"))
            listData.add(recipe)
        }*/


        val request =
            UpdateMealPlanRequest(listData, Utility.getUserID(activity!!.applicationContext))
        val manager = NetworkManager()
        if (manager.isConnectingToInternet(activity!!.applicationContext)) {
            manager.createApiRequest(
                ApiUtilis.getAPIInstance(activity!!).continueWithMeal(request),
                object : ServiceListener<ChooseMealSuccess> {
                    override fun getServerResponse(response: ChooseMealSuccess, requestcode: Int) {
                        binding.progressBar.visibility = View.GONE
                        binding.viewPager.visibility = View.VISIBLE
                        replaceFragment(MealFragment())
                    }

                    override fun getError(error: ErrorModel, requestcode: Int) {
                        binding.progressBar.visibility = View.GONE
                        binding.viewPager.visibility = View.VISIBLE
                        Utility.displayShortSnackBar(
                            binding.parentLayout,
                            error.getMessage().toString()
                        )
                    }
                })
        } else {
            binding.progressBar.visibility = View.GONE
            binding.viewPager.visibility = View.VISIBLE
            Utility.displayShortSnackBar(
                binding.parentLayout,
                resources.getString(R.string.check_internet)
            )
        }
    }

    fun replaceFragment(fragment: Fragment) {

        val transaction = activity!!.supportFragmentManager.beginTransaction()
        transaction.replace(R.id.flContent, fragment)
        transaction.addToBackStack("share")
        transaction.commit()
    }


}
