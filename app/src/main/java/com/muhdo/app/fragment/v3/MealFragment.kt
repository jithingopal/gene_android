package com.muhdo.app.fragment.v3

import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.util.Base64
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.muhdo.app.R
import com.muhdo.app.adapter.DayMealAdapter
import com.muhdo.app.adapter.RecipeListAdapter
import com.muhdo.app.apiModel.mealModel.MealPlanModel
import com.muhdo.app.apiModel.mealModel.UpdateFavouriteRequest
import com.muhdo.app.apiModel.mealModel.daysModel.UpdateFavoriteModel
import com.muhdo.app.apiModel.v3.UserIdReq
import com.muhdo.app.apiModel.v3.workoutplan.workout_overview.DeletePlanResult
import com.muhdo.app.databinding.FragmentMealV3Binding
import com.muhdo.app.fragment.v3.guide.meal.MealGuideQuestionnaireFragment
import com.muhdo.app.fragment.v3.guide.meal.choose_meal.ChooseMealFragment
import com.muhdo.app.fragment.v3.guide.workout.ChooseGoalFragment
import com.muhdo.app.interfaces.ItemClickListener
import com.muhdo.app.repository.*
import com.muhdo.app.ui.CalorieActivity
import com.muhdo.app.ui.DashboardActivity
import com.muhdo.app.ui.MyFavouriteActivity
import com.muhdo.app.ui.RecipeActivity
import com.muhdo.app.utils.v3.Constants
import com.muhdo.app.utils.v3.DayMealClickListener
import com.muhdo.app.utils.v3.TempUtil
import com.timingsystemkotlin.backuptimingsystem.Utility
import kotlinx.android.synthetic.main.list_breakfast.view.*
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MealFragment : Fragment(),
    ItemClickListener, DayMealClickListener {


    override fun onFavouriteClick(id: String, view: View, status: Boolean) {
        updateFavorite(id, view, status)
    }

    override fun onCardViewClick(recipeID: String) {
        goToRecipe(recipeID)
    }

    override fun onClick(position: Int) {

    }

    private lateinit var binding: FragmentMealV3Binding
    private lateinit var recipeAdapter: RecipeListAdapter
    private lateinit var dayMealAdapter: DayMealAdapter
    private val drawableList = arrayListOf<Int>()
    private val recipeNameList = arrayListOf<String>()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_meal_v3, container, false)
        binding = FragmentMealV3Binding.bind(view)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        getMealList()

        binding.recipeListNew.cardCalorie.setOnClickListener {
            val i = Intent(context, CalorieActivity::class.java)
            startActivity(i)
        }
        binding.recipeListNew.cardFavourite.setOnClickListener {
            val i = Intent(context, MyFavouriteActivity::class.java)
            startActivity(i)
        }

/*
        binding.toolbar.btnNavigation.setOnClickListener {
            val intent = Intent(context, DashboardActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
            (context as Activity).finish()
        }
*/

        binding.txtSeeMoreLunch.setOnClickListener {
            /*val intent = Intent(context, ChooseMealActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
            (context as Activity).finish()*/
            replaceFragment(ChooseMealFragment(1))

        }
        binding.txtSeeMoreBreakfast.setOnClickListener {
            /*val intent = Intent(context, ChooseMealActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
            (context as Activity).finish()*/
            replaceFragment(ChooseMealFragment(0))
        }
        binding.txtSeeMoreDessert.setOnClickListener {
            /*val intent = Intent(context, ChooseMealActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
            (context as Activity).finish()*/
            replaceFragment(ChooseMealFragment(2))

        }
        binding.txtSeeMoreDinner.setOnClickListener {
            /*val intent = Intent(context, ChooseMealActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
            (context as Activity).finish()*/
            replaceFragment(ChooseMealFragment(3))

        }
        binding.txtSeeMoreSnacks.setOnClickListener {
            /*val intent = Intent(context, ChooseMealActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
            (context as Activity).finish()*/
            replaceFragment(ChooseMealFragment(4))

        }
        binding.txtSunday.setOnClickListener {
            /*val intent = Intent(context, ChooseMealActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
            (context as Activity).finish()*/
            replaceFragment(ChooseMealFragment(6))

        }
        binding.txtChangeSaturdayPlan.setOnClickListener {
            /*val intent = Intent(context, ChooseMealActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
            (context as Activity).finish()*/
            replaceFragment(ChooseMealFragment(5))

        }
        /* binding.toolbar.imgResurvey.setOnClickListener {
             confirmResurvey()
         }*/

        setRecipeList()
    }

    fun replaceFragment(fragment: Fragment) {

        val transaction = activity!!.supportFragmentManager.beginTransaction()
        transaction.replace(R.id.flContent, fragment)
        transaction.addToBackStack("share")
        transaction.commit()
    }

    private fun getMealList() {
        binding.progressBar.visibility = View.VISIBLE
        val manager = NetworkManager()
        context?.let {
            if (manager.isConnectingToInternet(it)) {
                manager.createApiRequest(
                    ApiUtilis.getAPIInstance(activity!!).getMealPlanV3(Utility.getUserID(it)),
                    object : ServiceListener<MealPlanModel> {
                        override fun getServerResponse(response: MealPlanModel, requestcode: Int) {
                            binding.progressBar.visibility = View.GONE
                            if (response.getData() != null)
                                setData(response)
                            else {
                                response.getMessage()?.let { it1 ->
                                    Utility.displayShortSnackBar(
                                        binding.parentLayout,
                                        it1
                                    )
                                }
                            }
                        }

                        override fun getError(error: ErrorModel, requestcode: Int) {
                            if (error.getCode() == 302) {
                                val i = Intent(context, DashboardActivity::class.java)
                                i.putExtra(
                                    Constants.DASHBOARD_ACTION,
                                    Constants.DASHBOARD_MEAL_PLAN
                                )
                                startActivity(i)
                                (context as Activity).finish()
                            } else {
                                binding.progressBar.visibility = View.GONE

                                Utility.displayShortSnackBar(
                                    binding.parentLayout,
                                    error.getMessage().toString()
                                )
                            }
                        }
                    })
            } else {
                binding.progressBar.visibility = View.GONE
                Utility.displayShortSnackBar(
                    binding.parentLayout,
                    resources.getString(R.string.check_internet)
                )

            }
        }
    }

    private fun setRecipeList() {

        recipeNameList.add("MY CALORIES")
        recipeNameList.add("MY FAVOURITES")

        drawableList.add(R.drawable.recipes_slider1)
        drawableList.add(R.drawable.recipes_slider2)
        context?.let { recipeAdapter = RecipeListAdapter(it, drawableList, recipeNameList) }
        binding.recipeList.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        binding.recipeList.isNestedScrollingEnabled = true
        binding.recipeList.adapter = recipeAdapter

    }

    private fun setData(data: MealPlanModel) {
        context?.let {
            binding.daysLayout.visibility = View.VISIBLE
            if (data.getData()!!.getMonday() != null) {
                dayMealAdapter = DayMealAdapter(it, data.getData()!!.getMonday()!!, this)
                binding.mondayMealList.layoutManager =
                    LinearLayoutManager(it, LinearLayoutManager.HORIZONTAL, false)
                binding.mondayMealList.isNestedScrollingEnabled = true
                binding.mondayMealList.itemAnimator = DefaultItemAnimator()
                binding.mondayMealList.adapter = dayMealAdapter
            } else {
                binding.txtMonday.visibility = View.GONE
                binding.txtSeeMoreBreakfast.visibility = View.GONE
            }

            if (data.getData()!!.getTuesday() != null) {
                dayMealAdapter = DayMealAdapter(it, data.getData()!!.getTuesday()!!, this)
                binding.tuesdayMealList.layoutManager =
                    LinearLayoutManager(it, LinearLayoutManager.HORIZONTAL, false)
                binding.tuesdayMealList.isNestedScrollingEnabled = true
                binding.tuesdayMealList.itemAnimator = DefaultItemAnimator()
                binding.tuesdayMealList.adapter = dayMealAdapter
            } else {
                binding.txtTuesday.visibility = View.GONE
                binding.txtSeeMoreLunch.visibility = View.GONE
            }

            if (data.getData()!!.getWednesday() != null) {
                dayMealAdapter = DayMealAdapter(it, data.getData()!!.getWednesday()!!, this)
                binding.wednesdayMealList.layoutManager =
                    LinearLayoutManager(it, LinearLayoutManager.HORIZONTAL, false)
                binding.wednesdayMealList.isNestedScrollingEnabled = true
                binding.wednesdayMealList.itemAnimator = DefaultItemAnimator()
                binding.wednesdayMealList.adapter = dayMealAdapter
            } else {
                binding.txtWednesday.visibility = View.GONE
                binding.txtSeeMoreSnacks.visibility = View.GONE
            }

            if (data.getData()!!.getThursday() != null) {
                dayMealAdapter = DayMealAdapter(it, data.getData()!!.getThursday()!!, this)
                binding.thursdayMealList.layoutManager =
                    LinearLayoutManager(it, LinearLayoutManager.HORIZONTAL, false)
                binding.thursdayMealList.isNestedScrollingEnabled = true
                binding.thursdayMealList.itemAnimator = DefaultItemAnimator()
                binding.thursdayMealList.adapter = dayMealAdapter
            } else {
                binding.txtThursday.visibility = View.GONE
                binding.txtSeeMoreDessert.visibility = View.GONE
            }

            if (data.getData()!!.getFriday() != null) {
                dayMealAdapter = DayMealAdapter(it, data.getData()!!.getFriday()!!, this)
                binding.fridayMealList.layoutManager =
                    LinearLayoutManager(it, LinearLayoutManager.HORIZONTAL, false)
                binding.fridayMealList.isNestedScrollingEnabled = true
                binding.fridayMealList.itemAnimator = DefaultItemAnimator()
                binding.fridayMealList.adapter = dayMealAdapter
            } else {
                binding.txtFriday.visibility = View.GONE
                binding.txtSeeMoreDinner.visibility = View.GONE
            }

            if (data.getData()!!.getSaturday() != null) {
                dayMealAdapter = DayMealAdapter(it, data.getData()!!.getSaturday()!!, this)
                binding.saturdayMealList.layoutManager =
                    LinearLayoutManager(it, LinearLayoutManager.HORIZONTAL, false)
                binding.saturdayMealList.isNestedScrollingEnabled = true
                binding.saturdayMealList.itemAnimator = DefaultItemAnimator()
                binding.saturdayMealList.adapter = dayMealAdapter
            } else {
                binding.txtSaturday.visibility = View.GONE
                binding.txtChangeSaturdayPlan.visibility = View.GONE
            }

            if (data.getData()!!.getSunday() != null) {
                dayMealAdapter = DayMealAdapter(it, data.getData()!!.getSunday()!!, this)
                binding.sundayMealList.layoutManager =
                    LinearLayoutManager(it, LinearLayoutManager.HORIZONTAL, false)
                binding.sundayMealList.isNestedScrollingEnabled = true
                binding.sundayMealList.itemAnimator = DefaultItemAnimator()
                binding.sundayMealList.adapter = dayMealAdapter
            } else {
                binding.txtSunday.visibility = View.GONE
                binding.textSunday.visibility = View.GONE
            }
        }
    }

    fun updateFavorite(id: String, view: View, status: Boolean) {
        val manager = NetworkManager()
        context?.let {
            val requestParams =
                UpdateFavouriteRequest(status, id, Utility.getUserID(it))

            if (manager.isConnectingToInternet(it)) {
                manager.createApiRequest(
                    ApiUtilis.getAPIInstance(activity!!).updateFavouriteRecipe(requestParams),
                    object : ServiceListener<UpdateFavoriteModel> {
                        override fun getServerResponse(
                            response: UpdateFavoriteModel,
                            requestcode: Int
                        ) {
                            view.isClickable = true
                            if (status) {
                                Utility.favouriteIdList.add(id)
                                if (Utility.unFavouriteIdList.contains(id)) {
                                    Utility.unFavouriteIdList.remove(id)
                                }
                            } else if (!status) {
                                Utility.unFavouriteIdList.add(id)
                                if (Utility.favouriteIdList.contains(id)) {
                                    Utility.favouriteIdList.remove(id)
                                }
                            }

                            dayMealAdapter.notifyDataSetChanged()
                        }

                        override fun getError(error: ErrorModel, requestcode: Int) {
                            if (status) {
                                view.check_favourite.isChecked = false
                            } else if (!status) {
                                view.check_favourite.isChecked = true
                            }
                            Utility.displayShortSnackBar(
                                binding.parentLayout,
                                error.getMessage().toString()
                            )
                        }
                    })
            }
        }

    }

    fun goToRecipe(recipeID: String) {
        val i = Intent(context, RecipeActivity::class.java)
        i.putExtra(Constants.RECIPE_ID, recipeID)
        startActivity(i)
//        (context as Activity).finish()
    }

    override fun onPause() {
        super.onPause()
        val dashBoardActivity: DashboardActivity = activity as DashboardActivity
        dashBoardActivity.setResurveyIconVisible(false, View.OnClickListener { })
    }

    override fun onResume() {
        super.onResume()
        val dashBoardActivity: DashboardActivity = activity as DashboardActivity
        dashBoardActivity.setResurveyIconVisible(true, View.OnClickListener { confirmResurvey() })

    }

    private fun confirmResurvey() {
        val dialogBuilder = AlertDialog.Builder(activity)
        dialogBuilder.setMessage(R.string.v3_confirm_meal_guide_resurvey)
            .setCancelable(false)
            .setPositiveButton(activity?.resources?.getString(R.string.option_yes)) { _, _ ->
                deleteMealPlanFromServer()
            }
            .setNegativeButton(activity?.resources?.getString(R.string.option_no)) { _, _ ->

            }

        val alert = dialogBuilder.create()
        alert.show()
    }


    private fun deleteMealPlanFromServer() {
        val dashBoardActivity: DashboardActivity = activity as DashboardActivity

        val userId = Base64.decode(
            Utility.getUserID(activity!!.applicationContext),
            Base64.DEFAULT
        )
        binding.progressBar.visibility = View.VISIBLE
        val jsonParams: JSONObject = JSONObject()
        jsonParams.put("user_id", Utility.getUserID(activity!!.applicationContext))
        val requestBody = UserIdReq(
            Utility.getUserID(activity!!.applicationContext)
        )
        TempUtil.log("Jithin", jsonParams.toString())
        val manager = NetworkManager2()
        if (manager.isConnectingToInternet(binding.parentLayout.context)) {
            val call =
                ApiUtilis.getAPIInstance(activity!!)
                    .deleteMealPlan(requestBody)
            call.enqueue(object : Callback<DeletePlanResult> {
                override fun onResponse(
                    call: Call<DeletePlanResult>,
                    response: Response<DeletePlanResult>
                ) {
                    binding.progressBar.visibility = View.GONE
                    var resultPojo: DeletePlanResult? = response.body()
                    try {
                        if (resultPojo != null && resultPojo!!.statusCode == 200) {
                            Utility.setMealStatus(activity!!.applicationContext, false)
                            dashBoardActivity.setResurveyIconVisible(
                                false,
                                View.OnClickListener { })
                            val chooseGoalFragment = ChooseGoalFragment()
                            chooseGoalFragment.setGoToFragment(MealGuideQuestionnaireFragment());
                            dashBoardActivity.replaceFragment(chooseGoalFragment)
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }

                override fun onFailure(call: Call<DeletePlanResult>, t: Throwable) {
                    // Log error here since request failed
                    binding.progressBar.visibility = View.GONE
                    TempUtil.log("Login", t.toString())

                    Utility.displayShortSnackBar(
                        binding.parentLayout,
                        "Server connection error!!, please try again later."
                    )


                }
            })
        } else {
            binding.progressBar.visibility = View.GONE

            Utility.displayShortSnackBar(
                binding.parentLayout,
                binding.parentLayout.context.resources.getString(R.string.check_internet)
            )

        }

    }
}