package com.muhdo.app.fragment.v3

import android.annotation.SuppressLint
import android.app.Dialog
import android.os.Bundle
import android.text.Html
import android.util.Base64
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.muhdo.app.R
import com.muhdo.app.apiModel.v3.Answer
import com.muhdo.app.apiModel.v3.QuestionnaireAnswer
import com.muhdo.app.databinding.V3FragmentQuestionnaireHolderDietBinding
import com.muhdo.app.repository.ApiUtilis
import com.muhdo.app.utils.v3.*
import com.timingsystemkotlin.backuptimingsystem.Utility
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.v3_fragment_questionnaire_holder_diet.view.*
import kotlinx.android.synthetic.main.v3_layout_diet_1.*
import kotlinx.android.synthetic.main.v3_layout_diet_2.*
import kotlinx.android.synthetic.main.v3_layout_diet_3.*

class QuestionnaireDietFragment : Fragment() {
    private var isSkipped: Boolean = false
    private var currentPage = 0

    lateinit var dialog: Dialog;
    internal lateinit var binding: V3FragmentQuestionnaireHolderDietBinding
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view =
            inflater.inflate(R.layout.v3_fragment_questionnaire_holder_diet, container, false)
        binding = V3FragmentQuestionnaireHolderDietBinding.bind(view)
        addBottomDots(currentPage)
        setupDialogInfo()

        binding.btnNext.setOnClickListener {
            onNext()
        }
        binding.btnPrevious.setOnClickListener {
            onPrevious()
        }
        binding.layoutDiet1.layout_diet1_bind.findViewById<View>(R.id.btn_questionnaire_info)
            .setOnClickListener {
                onShowInfoDialog()
            }
        binding.layoutDiet2.layout_diet2_bind.findViewById<View>(R.id.btn_questionnaire_info)
            .setOnClickListener {
                onShowInfoDialog()
            }
        binding.layoutDiet3.layout_diet3_bind.findViewById<View>(R.id.btn_questionnaire_info)
            .setOnClickListener {
                onShowInfoDialog()
            }
        binding.layoutDiet4.layout_diet4_bind.findViewById<View>(R.id.btn_questionnaire_info)
            .setOnClickListener {
                onShowInfoDialog()
            }
        binding.layoutDiet4Bind.tvSkip.setOnClickListener {
            isSkipped = true
            getAllAnswersOfQuestionnaire(isSkipped)
        }
        return view

    }

    @SuppressLint("WrongConstant")
    private fun replaceFragment(fragment: Fragment) {
        val transaction = activity!!.supportFragmentManager.beginTransaction()
        transaction.replace(R.id.flContent, fragment)
        transaction.addToBackStack("share")
        transaction.commit()

    }

    private fun setupDialogInfo() {
        dialog = Dialog(activity)
        val viewDialog = View.inflate(activity, R.layout.v3_dialog_info_questionnaire, null)
        val closeButton = viewDialog.findViewById<ImageView>(R.id.img_close)
        closeButton.setOnClickListener { dialog.dismiss() }
        dialog.setContentView(viewDialog)
        val tvTitle = viewDialog.findViewById<TextView>(R.id.tvTitle)
        val tvSubTitle = viewDialog.findViewById<TextView>(R.id.tvSubTitle)
        val tvDescription = viewDialog.findViewById<TextView>(R.id.tvDescription)
        tvTitle.setText(R.string.info_pane_text_diet_title)
        tvSubTitle.setText(R.string.info_pane_text_diet_sub_title)
        tvDescription.setText(R.string.info_pane_text_diet_description)
        tvSubTitle.visibility = View.VISIBLE
        dialog.setCancelable(false)
    }

    private fun onShowInfoDialog() {
        if (!dialog.isShowing)
            dialog.show()
    }

    private fun onNext() {
        TempUtil.log(
            "clicked",
            "visibility: " + binding.layoutDiet1.visibility.toString() + binding.layoutDiet2.visibility.toString() + binding.layoutDiet3.visibility.toString() + binding.layoutDiet4.visibility.toString()
        )
        if (currentPage == 0) {
            if (rg_healthy_diet.checkedRadioButtonId != -1 && rg_5_times_eat.checkedRadioButtonId != -1 && rg_vitamin_mineral.checkedRadioButtonId != -1) {
                binding.btnPrevious.visibility = View.VISIBLE
                binding.layoutDiet1.visibility = View.GONE
                binding.layoutDiet2.visibility = View.VISIBLE
                binding.layoutDiet3.visibility = View.GONE
                binding.layoutDiet4.visibility = View.GONE
                currentPage = 1
            } else
                context?.showAlertDialog(getString(R.string.answer_all_question_message))
        } else if (currentPage == 1) {
            if (rg_fitness_equip.checkedRadioButtonId != -1 && rg_drink_water.checkedRadioButtonId != -1 && rg_water_more_cup.checkedRadioButtonId != -1) {
                binding.btnPrevious.visibility = View.VISIBLE
                binding.layoutDiet1.visibility = View.GONE
                binding.layoutDiet2.visibility = View.GONE
                binding.layoutDiet3.visibility = View.VISIBLE
                binding.layoutDiet4.visibility = View.GONE
                TempUtil.log("Layout", "inside 2")
                currentPage = 2
            } else context?.showAlertDialog(getString(R.string.answer_all_question_message))
        } else if (currentPage == 2) {
            if (rg_vegan.checkedRadioButtonId != -1 && rg_vegetarian.checkedRadioButtonId != -1 && rg_eat_fish.checkedRadioButtonId != -1) {
                binding.btnPrevious.visibility = View.VISIBLE
                binding.layoutDiet1.visibility = View.GONE
                binding.layoutDiet2.visibility = View.GONE
                binding.layoutDiet3.visibility = View.GONE
                binding.layoutDiet4.visibility = View.VISIBLE
                TempUtil.log("Layout", "inside 3")
                currentPage = 3
            } else context?.showAlertDialog(getString(R.string.answer_all_question_message))
        } else if (currentPage == 3) {
            if (isSkipped || atleastOneCheckBoxSelected(
                    binding.layoutDiet4Bind.cbOption1,
                    binding.layoutDiet4Bind.cbOption2,
                    binding.layoutDiet4Bind.cbOption3,
                    binding.layoutDiet4Bind.cbOption4,
                    binding.layoutDiet4Bind.cbOption5,
                    binding.layoutDiet4Bind.cbOption6,
                    binding.layoutDiet4Bind.cbOption7,
                    binding.layoutDiet4Bind.cbOption8,
                    binding.layoutDiet4Bind.cbOption9
                )
            ) {
                if (atleastOneCheckBoxSelected(
                        binding.layoutDiet4Bind.cbOption1,
                        binding.layoutDiet4Bind.cbOption2,
                        binding.layoutDiet4Bind.cbOption3,
                        binding.layoutDiet4Bind.cbOption4,
                        binding.layoutDiet4Bind.cbOption5,
                        binding.layoutDiet4Bind.cbOption6,
                        binding.layoutDiet4Bind.cbOption7,
                        binding.layoutDiet4Bind.cbOption8,
                        binding.layoutDiet4Bind.cbOption9
                    )
                )
                    isSkipped = false
                getAllAnswersOfQuestionnaire(isSkipped)
            } else {
                context?.showAlertDialog(getString(R.string.answer_or_skip_message))
            }
        }
        addBottomDots(currentPage)
    }

    private fun getAllAnswersOfQuestionnaire(isSkipped: Boolean) {
        val userId = Base64.decode(Utility.getUserID(activity!!.applicationContext), Base64.DEFAULT)
        val decodeUserId = String(userId, charset("UTF-8"))
        TempUtil.log(
            "Diet answers ",
            binding.layoutDiet1Bind.rgHealthyDiet.getSelectedRadioButtonText() + binding.layoutDiet1Bind.rg5TimesEat.getSelectedRadioButtonText()
                    + binding.layoutDiet1Bind.rgVitaminMineral.getSelectedRadioButtonText()
        )
        TempUtil.log(
            "Diet answers 2",
            binding.layoutDiet2Bind.rgFitnessEquip.getSelectedRadioButtonText() + binding.layoutDiet2Bind.rgDrinkWater.getSelectedRadioButtonText()
                    + binding.layoutDiet2Bind.rgWaterMoreCup.getSelectedRadioButtonText()
        )
        TempUtil.log(
            "Diet answers 3",
            binding.layoutDiet3Bind.rgVegan.getSelectedRadioButtonText() + binding.layoutDiet3Bind.rgVegetarian.getSelectedRadioButtonText()
                    + binding.layoutDiet3Bind.rgEatFish.getSelectedRadioButtonText()
        )
        TempUtil.log(
            "Diet answers 4",
            getAllSelectedCheckboxTexts(
                binding.layoutDiet4Bind.cbOption1,
                binding.layoutDiet4Bind.cbOption2,
                binding.layoutDiet4Bind.cbOption3,
                binding.layoutDiet4Bind.cbOption4,
                binding.layoutDiet4Bind.cbOption5,
                binding.layoutDiet4Bind.cbOption6,
                binding.layoutDiet4Bind.cbOption7,
                binding.layoutDiet4Bind.cbOption8,
                binding.layoutDiet4Bind.cbOption9
            ).toString()
        )
        val answers = arrayListOf<Answer>()
        val healthyDiet = Answer(
            question = Constants.DIET.HEALTHY_DIET,
            answer = listOf(binding.layoutDiet1Bind.rgHealthyDiet.getSelectedRadioButtonText())
        )
        answers.add(healthyDiet)
        val eat5times = Answer(
            question = Constants.DIET.EAT_5_VEG_FRUIT,
            answer = listOf(binding.layoutDiet1Bind.rg5TimesEat.getSelectedRadioButtonText())
        )
        answers.add(eat5times)
        val eatDailyVitamin = Answer(
            question = Constants.DIET.TAKE_VITAMIN_DAILY,
            answer = listOf(binding.layoutDiet1Bind.rgVitaminMineral.getSelectedRadioButtonText())
        )
        answers.add(eatDailyVitamin)
        val fitnessSupplements = Answer(
            question = Constants.DIET.FITNESS_SUPPLEMENTS,
            answer = listOf(binding.layoutDiet2Bind.rgFitnessEquip.getSelectedRadioButtonText())
        )
        answers.add(fitnessSupplements)
        val drinkWater = Answer(
            question = Constants.DIET.DRINK_WATER,
            answer = listOf(binding.layoutDiet2Bind.rgDrinkWater.getSelectedRadioButtonText())
        )
        answers.add(drinkWater)
        val drinkCoffee = Answer(
            question = Constants.DIET.DRINK_COFFEE,
            answer = listOf(binding.layoutDiet2Bind.rgWaterMoreCup.getSelectedRadioButtonText())
        )
        answers.add(drinkCoffee)
        val vegan = Answer(
            question = Constants.DIET.VEGAN,
            answer = listOf(binding.layoutDiet3Bind.rgVegan.getSelectedRadioButtonText())
        )
        answers.add(vegan)
        val vegetarian = Answer(
            question = Constants.DIET.VEGETARIAN,
            answer = listOf(binding.layoutDiet3Bind.rgVegetarian.getSelectedRadioButtonText())
        )
        answers.add(vegetarian)
        val eatFish = Answer(
            question = Constants.DIET.EAT_FISH,
            answer = listOf(binding.layoutDiet3Bind.rgEatFish.getSelectedRadioButtonText())
        )
        answers.add(eatFish)
        val omitFoods: Answer
        if (isSkipped) {
            omitFoods = Answer(
                question = Constants.DIET.OMIT_FOOD,
                answer = listOf(Constants.ANSWER.SKIP)
            )
        } else {
            omitFoods = Answer(
                question = Constants.DIET.OMIT_FOOD,
                answer = Constants.createOmitFoodsArray(
                    getAllSelectedCheckboxTexts(
                        binding.layoutDiet4Bind.cbOption1,
                        binding.layoutDiet4Bind.cbOption2,
                        binding.layoutDiet4Bind.cbOption3,
                        binding.layoutDiet4Bind.cbOption4,
                        binding.layoutDiet4Bind.cbOption5,
                        binding.layoutDiet4Bind.cbOption6,
                        binding.layoutDiet4Bind.cbOption7,
                        binding.layoutDiet4Bind.cbOption8,
                        binding.layoutDiet4Bind.cbOption9
                    )
                )
            )
        }
        answers.add(omitFoods)

        val requestBody = QuestionnaireAnswer(
            user_id = decodeUserId,
            answers = answers,
            category = Constants.QUESTIONNAIRE.DIET
        )
        uploadAnswers(requestBody)
    }

    private fun uploadAnswers(requestBody: QuestionnaireAnswer) {
        binding.progressBar.visibility = View.VISIBLE
        ApiUtilis.getAPIInstance(activity!!).postReviewQuestionnaireAnswers(requestBody)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ success ->
                run {
                    binding.progressBar.visibility = View.GONE
                    try {
                        Utility.displayShortSnackBar(binding.root, success.message)
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                    // TempUtil.log("response from Exercise", success.message)
                    replaceFragment(QuestionnaireHomeFragment())
                    TempUtil.isLifestyleQuestionnaireCompleted = true
                }
            }, { error ->
                run {
                    binding.progressBar.visibility = View.GONE
                    Utility.displayShortSnackBar(binding.root, error.localizedMessage)
                    // TempUtil.log("response from Exercise ", error.localizedMessage)
                }
            })
    }


    private fun onPrevious() {

        if (currentPage == 0) {

            binding.btnPrevious.visibility = View.GONE
            currentPage = 0

        } else if (currentPage == 1) {
            binding.btnPrevious.visibility = View.GONE

            binding.layoutDiet1.visibility = View.VISIBLE
            binding.layoutDiet2.visibility = View.GONE
            binding.layoutDiet3.visibility = View.GONE
            binding.layoutDiet4.visibility = View.GONE
            currentPage = 0
        } else if (currentPage == 2) {

            binding.btnPrevious.visibility = View.VISIBLE

            binding.layoutDiet1.visibility = View.GONE
            binding.layoutDiet2.visibility = View.VISIBLE
            binding.layoutDiet3.visibility = View.GONE
            binding.layoutDiet4.visibility = View.GONE
            currentPage = 1
        } else if (currentPage == 3) {
            if (isSkipped)
                isSkipped = false
            binding.btnPrevious.visibility = View.VISIBLE
            binding.layoutDiet1.visibility = View.GONE
            binding.layoutDiet2.visibility = View.GONE
            binding.layoutDiet3.visibility = View.VISIBLE
            binding.layoutDiet4.visibility = View.GONE
            currentPage = 2
        }
        addBottomDots(currentPage)

    }

    private fun addBottomDots(currentPage: Int) {


        val dots = arrayOfNulls<TextView>(4)
        binding.layoutDots.removeAllViews()
        for (i in dots.indices) {

            dots[i] = TextView(activity)
            dots[i]!!.text = Html.fromHtml("&#8226;")
            dots[i]!!.textSize = 35f
            dots[i]!!.setTextColor(
                ContextCompat.getColor(
                    activity!!.applicationContext,
                    R.color.secondary_color
                )
            )
            binding.layoutDots.addView(dots[i])
        }

        if (dots.isNotEmpty()) {
            dots[currentPage]!!.setTextColor(
                ContextCompat.getColor(
                    activity!!.applicationContext,
                    R.color.color_dark_blue
                )
            )
        }
    }
}
