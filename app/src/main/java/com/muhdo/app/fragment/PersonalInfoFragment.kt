package com.muhdo.app.fragment

import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.bruce.pickerview.popwindow.DatePickerPopWin
import com.muhdo.app.R
import com.muhdo.app.databinding.FragmentPersonalInfoBinding
import com.timingsystemkotlin.backuptimingsystem.Utility
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog


class PersonalInfoFragment : Fragment(), DatePickerDialog.OnDateSetListener {

    override fun onDateSet(view: DatePickerDialog?, year: Int, monthOfYear: Int, dayOfMonth: Int) {
 val date = "$dayOfMonth/$monthOfYear/$year"
        binding.edtDob.text = date
    }



    internal lateinit var binding: FragmentPersonalInfoBinding
 override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_personal_info, container, false)
        binding = FragmentPersonalInfoBinding.bind(view)


        binding.edtDob.setOnClickListener {


            val pickerPopWin = DatePickerPopWin.Builder(activity,
                DatePickerPopWin.OnDatePickedListener { _, _, _, dateDesc ->
                    binding.edtDob.text = dateDesc

                }).textConfirm("CONFIRM") //text of confirm button
                .textCancel("CANCEL") //text of cancel button
                .btnTextSize(16) // button text size
                .viewTextSize(25) // pick view text size
                .colorCancel(Color.parseColor("#999999")) //color of cancel button
                .colorConfirm(Color.parseColor("#009900"))//color of confirm button
                .minYear(1990) //min year in loop
                .maxYear(2550) // max year in loop
                .showDayMonthYear(true) // shows like dd mm yyyy (default is false)
                .dateChose("2019-04-10") // date chose when init popwindow
                .build()
            pickerPopWin.showPopWin(activity)

        }


     val mealData = Utility.getMealData(activity!!)
     if (mealData != null) {
         binding.edtHeight.setText(mealData.height)
         binding.edtWeight.setText(mealData.weight)

     }else{
         Toast.makeText(activity, "No Data Found", Toast.LENGTH_LONG).show()
     }


        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val list = arrayOf("Male", "Female", "Other")


        val adapter = ArrayAdapter(activity!!, android.R.layout.simple_spinner_item, list)
   adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding.spinnerSex.adapter = adapter
        binding.spinnerSex.setSelection(0)
        binding.spinnerSex.onItemSelectedListener =
                object : AdapterView.OnItemSelectedListener {
                    override fun onItemSelected(parent: AdapterView<*>, view: View?, position: Int, id: Long) {

                    }

                    override fun onNothingSelected(parent: AdapterView<*>) {
                    }
                }


        val list1 = arrayOf("Kg", "Lbs")
        val arrayAdapter1 = ArrayAdapter(activity!!.applicationContext, android.R.layout.simple_spinner_item, list1)
        arrayAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding.spinnerWeight.adapter = arrayAdapter1

        binding.spinnerWeight.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View?, position: Int, id: Long) {
//                    Toast.makeText(this@MainActivity, getString(R.string.selected_item) + " " + personNames[position], Toast.LENGTH_SHORT).show()
                if(position >= 0){
                    System.out.println("Position==> $position")
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // Code to perform some action when nothing is selected
            }
        }

        val list2 = arrayOf("Cm", "Ft")
        val arrayAdapter2 = ArrayAdapter(activity!!.applicationContext, android.R.layout.simple_spinner_item, list2)
        arrayAdapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding.spinnerHeight.adapter = arrayAdapter2

        binding.spinnerHeight.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                if(p2 >= 0){
                    System.out.println("Position==> $p2")
                }
            }


            override fun onNothingSelected(parent: AdapterView<*>) {
                // Code to perform some action when nothing is selected
            }
        }
    }


}