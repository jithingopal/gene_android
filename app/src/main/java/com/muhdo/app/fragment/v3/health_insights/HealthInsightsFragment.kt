package com.muhdo.app.fragment.v3.health_insights

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.muhdo.app.R
import com.muhdo.app.adapter.ModeSelectionAdapter
import com.muhdo.app.apiModel.keyData.GenoTypeModel
import com.muhdo.app.databinding.V3FragmentHealthInsightBinding
import com.muhdo.app.fragment.v3.ResultsFragment
import com.muhdo.app.model.RecyclerData
import com.muhdo.app.repository.ApiUtilis
import com.muhdo.app.repository.ErrorModel
import com.muhdo.app.repository.NetworkManager
import com.muhdo.app.repository.ServiceListener
import com.muhdo.app.ui.ResultsActivity
import com.muhdo.app.utils.PreferenceConnector
import com.muhdo.app.utils.v3.Constants
import com.timingsystemkotlin.backuptimingsystem.Utility

class HealthInsightsFragment : Fragment() {


    internal lateinit var binding: V3FragmentHealthInsightBinding
    private lateinit var modeAdapter: ModeSelectionAdapter
    private lateinit var mSpeedList: ArrayList<RecyclerData>

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.v3_fragment_health_insight, container, false)
        binding = V3FragmentHealthInsightBinding.bind(view)
        Utility.sectionList.clear()
        Utility.modeList.clear()


        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setList()

    }

    private fun getGenotypeModes() {
        binding.progressBar.visibility = View.VISIBLE
        val manager = NetworkManager()
        if (manager.isConnectingToInternet(requireContext())) {
            manager.createApiRequest(
                ApiUtilis.getAPIInstance(activity!!).getGenoType(),
                object : ServiceListener<GenoTypeModel> {
                    override fun getServerResponse(response: GenoTypeModel, requestcode: Int) {
                        binding.progressBar.visibility = View.GONE
                        setList()
                    }

                    override fun getError(error: ErrorModel, requestcode: Int) {

                        binding.progressBar.visibility = View.GONE
//                        Utility.displayShortSnackBar(
//                            binding.parentLayout,
//                            error.getMessage()!!
//                        )
                        /*Toast.makeText(activity!!.applicationContext,"server error", Toast.LENGTH_LONG)
                        Toast.makeText(activity!!.applicationContext,"server error", Toast.LENGTH_LONG)
                            .show()*/
                    }
                })
        } else {
            binding.progressBar.visibility = View.GONE
            Toast.makeText(requireContext(), "Please check internet connection!", Toast.LENGTH_LONG)
                .show()
        }
    }

    private fun setList() {

        try {
            binding.list.layoutManager = LinearLayoutManager(activity)
            mSpeedList = ArrayList()
            mSpeedList.add(RecyclerData(getString(R.string.stress), R.drawable.v3_ic_health_insight_stress))
            mSpeedList.add(RecyclerData(getString(R.string.sleep), R.drawable.v3_ic_health_insight_sleep))
            mSpeedList.add(RecyclerData(getString(R.string.anti_aging), R.drawable.v3_ic_health_insight_ani_ageing))
            mSpeedList.add(RecyclerData(getString(R.string.injury_risk), R.drawable.v3_ic_health_insight_injury_risk))
            mSpeedList.add(RecyclerData(getString(R.string.gut_health), R.drawable.v3_ic_health_insight_gut_health))
            mSpeedList.add(RecyclerData(getString(R.string.mental_health), R.drawable.v3_ic_health_insight_mentel_health))
            mSpeedList.add(RecyclerData(getString(R.string.skin_health), R.drawable.v3_ic_health_insight_skin_health))
            mSpeedList.add(RecyclerData(getString(R.string.eye_health), R.drawable.v3_ic_health_insight_eye_health))
            mSpeedList.add(RecyclerData(getString(R.string.muscle_health), R.drawable.v3_ic_health_insight_muscle_health))
            mSpeedList.add(RecyclerData(getString(R.string.heart_health), R.drawable.v3_ic_health_insight_heart_health))
            mSpeedList.add(RecyclerData(getString(R.string.addiction), R.drawable.v3_ic_health_insight_addiction))
            mSpeedList.add(RecyclerData(getString(R.string.skin_aging), R.drawable.v3_ic_health_insight_skin_health))
            modeAdapter = ModeSelectionAdapter(mSpeedList, requireActivity())
            binding.list.adapter = modeAdapter
        } catch (e: IllegalStateException) {
            e.printStackTrace()
        }
    }


    @SuppressLint("WrongConstant")
    private fun replaceFragment(fragment: Fragment) {
        val transaction = activity!!.supportFragmentManager.beginTransaction()
        transaction.replace(R.id.flContent, fragment)
        transaction.addToBackStack("share")
        transaction.commit()

    }
}