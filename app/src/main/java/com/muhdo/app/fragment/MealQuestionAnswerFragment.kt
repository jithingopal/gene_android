package com.muhdo.app.fragment

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.muhdo.app.R
import com.muhdo.app.databinding.FragmentMealQestionAnswerBinding
import com.muhdo.app.utils.Events
import com.muhdo.app.utils.GlobalBus
import com.muhdo.app.utils.v3.Constants
import com.timingsystemkotlin.backuptimingsystem.Utility
import org.greenrobot.eventbus.Subscribe


class MealQuestionAnswerFragment : Fragment() {


    var callback: OnDataInputListener? = null
    var activityCount: Int = 0

    interface OnDataInputListener {
        // TODO: Update argument type and name
        fun onDataInput(questionType: String, questionData: String)
    }


    //    private val mCallback: OnDataInputListener = null
    private lateinit var binding: FragmentMealQestionAnswerBinding
    private var isGenderSelected = false
    internal var count = 0


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        GlobalBus.getBus()!!.register(this@MealQuestionAnswerFragment)
        val view = inflater.inflate(R.layout.fragment_meal_qestion_answer, container, false)
        binding = FragmentMealQestionAnswerBinding.bind(view)
        activity!!.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
        val data = this.arguments!!.getString(Constants.MEAL_QUESTION_TYPE)
        showView(data!!)
        validateGoalQuestionnaire()
        validateDietQuestionnaire()
        validateFoodQuestionnaire()
        validateAlcoholQuestionnaire()
        validateMealQuestionnaire()
        validateBMIQuestionnaire()
        validatePersonalQuestionnaire()
        validateActivityQuestionnaire()
        binding.layoutMeals.slider.position = 0
        return view
    }

    private fun showView(data: String) {
        when (data) {
            Constants.MEAL_QUESTION_TYPE_GOAL -> {
                binding.layoutGoal.root.visibility = VISIBLE
            }
            Constants.MEAL_QUESTION_TYPE_DIET -> {
                binding.layoutDiet.root.visibility = VISIBLE
            }
            Constants.MEAL_QUESTION_TYPE_FOOD -> {
                binding.layoutExcludeFood.root.visibility = VISIBLE
            }
            Constants.MEAL_QUESTION_TYPE_ALCOHOL -> {
                binding.layoutAlcohol.root.visibility = VISIBLE
            }
            Constants.MEAL_QUESTION_TYPE_MEALS -> {
                binding.layoutMeals.root.visibility = VISIBLE
            }
            Constants.MEAL_QUESTION_TYPE_BMI -> {
                binding.layoutBmi.btnCm.isChecked = true
                binding.layoutBmi.btnKg.isChecked = true
                binding.layoutBmi.root.visibility = VISIBLE
            }
            Constants.MEAL_QUESTION_TYPE_PERSONAL -> {
                binding.layoutPersonalDetails.root.visibility = VISIBLE
            }
            Constants.MEAL_QUESTION_TYPE_ACTIVITY -> {
                binding.layoutActivity.root.visibility = VISIBLE
            }


        }
    }


    @Subscribe
    fun getPage(page: Int) {

        val sendPageCount = Events.sendPageCount(page)

        GlobalBus.getBus()!!.post(sendPageCount)


    }

    @Subscribe
    fun getMessage(activityFragmentMessage: Events.ActivityFragmentMessage) {
    }


    override fun onDestroyView() {
        super.onDestroyView()
        // unregister the registered event.
        GlobalBus.getBus()!!.unregister(this)
    }


    private fun validateGoalQuestionnaire() {

        binding.layoutGoal.radioGoal.setOnCheckedChangeListener { _, checkedId ->
            when (checkedId) {
                R.id.btn_muscle -> {
                    Utility.listValue[0] = "A"
                    binding.layoutGoal.btnMuscle.isChecked = true
                    binding.layoutGoal.btnFatLoss.isChecked = false
                    binding.layoutGoal.btnWeightGain.isChecked = false
                    binding.layoutGoal.btnSports.isChecked = false
                    binding.layoutGoal.btnHealth.isChecked = false
                    Utility.setGoalDone(activity!!.applicationContext, true)
                    getPage(1)
                }
                R.id.btn_fat_loss -> {

                    Utility.listValue[0] = "B"
                    binding.layoutGoal.btnMuscle.isChecked = false
                    binding.layoutGoal.btnFatLoss.isChecked = true
                    binding.layoutGoal.btnWeightGain.isChecked = false
                    binding.layoutGoal.btnSports.isChecked = false
                    binding.layoutGoal.btnHealth.isChecked = false
                    Utility.setGoalDone(activity!!.applicationContext, true)
                    getPage(1)
                }
                R.id.btn_weight_gain -> {

                    Utility.listValue[0] = "C"
                    binding.layoutGoal.btnMuscle.isChecked = false
                    binding.layoutGoal.btnFatLoss.isChecked = false
                    binding.layoutGoal.btnWeightGain.isChecked = true
                    binding.layoutGoal.btnSports.isChecked = false
                    binding.layoutGoal.btnHealth.isChecked = false
                    Utility.setGoalDone(activity!!.applicationContext, true)
                    getPage(1)
                }
                R.id.btn_sports -> {

                    Utility.listValue[0] = "D"
                    binding.layoutGoal.btnMuscle.isChecked = false
                    binding.layoutGoal.btnFatLoss.isChecked = false
                    binding.layoutGoal.btnWeightGain.isChecked = false
                    binding.layoutGoal.btnSports.isChecked = true
                    binding.layoutGoal.btnHealth.isChecked = false
                    Utility.setGoalDone(activity!!.applicationContext, true)
                    getPage(1)
                }
                R.id.btn_health -> {

                    Utility.listValue[0] = "E"
                    binding.layoutGoal.btnMuscle.isChecked = false
                    binding.layoutGoal.btnFatLoss.isChecked = false
                    binding.layoutGoal.btnWeightGain.isChecked = false
                    binding.layoutGoal.btnSports.isChecked = false
                    binding.layoutGoal.btnHealth.isChecked = true
                    Utility.setGoalDone(activity!!.applicationContext, true)
                    getPage(1)
                }
            }

        }
    }

    private fun validateDietQuestionnaire() {
        binding.layoutDiet.radioVegan.setOnCheckedChangeListener { _, checkedId ->
            when (checkedId) {
                R.id.btn_is_vegan -> {
                    Utility.list[8] = true
                    Utility.setDietDone(activity!!.applicationContext, true)
                }
                R.id.btn_not_vegan -> {
                    Utility.list[8] = false
                    Utility.setDietDone(activity!!.applicationContext, true)
                }
            }

        }

        binding.layoutDiet.radioVeg.setOnCheckedChangeListener { _, checkedId ->
            when (checkedId) {
                R.id.btn_is_veg -> {
                    Utility.list[9] = true
                    Utility.setDietDone1(activity!!.applicationContext, true)
                }
                R.id.btn_not_veg -> {
                    Utility.list[9] = false
                    Utility.setDietDone1(activity!!.applicationContext, true)
                }
            }

        }

        binding.layoutDiet.radioFish.setOnCheckedChangeListener { _, checkedId ->
            when (checkedId) {
                R.id.btn_eat_fish -> {
                    Utility.list[10] = true
                    Utility.setDietDone2(activity!!.applicationContext, true)
                }
                R.id.btn_no_fish -> {
                    Utility.list[10] = false
                    Utility.setDietDone2(activity!!.applicationContext, true)
                }
            }

        }
    }

    private fun validateFoodQuestionnaire() {
        Utility.list[0] = false
        binding.layoutExcludeFood.foodContainsWheat.setOnClickListener {
            if (binding.layoutExcludeFood.foodContainsWheat.currentTextColor == ContextCompat.getColor(
                    activity!!,
                    R.color.color_blue
                )
            ) {
                binding.layoutExcludeFood.foodContainsWheat.setTextColor(
                    ContextCompat.getColor(
                        activity!!,
                        R.color.white
                    )
                )
                binding.layoutExcludeFood.foodContainsWheat.setBackgroundResource(R.drawable.blue_button_bg)
                Utility.setFood(activity!!.applicationContext, true)
                Utility.list[0] = true
            } else {
                binding.layoutExcludeFood.foodContainsWheat.setTextColor(
                    ContextCompat.getColor(
                        activity!!,
                        R.color.color_blue
                    )
                )
                binding.layoutExcludeFood.foodContainsWheat.setBackgroundResource(R.drawable.blue_button_uncheck_bg)
                Utility.setFood(activity!!.applicationContext, false)
                Utility.list[0] = false
            }

        }
        binding.layoutExcludeFood.foodContainsBeef.setOnClickListener {
            if (binding.layoutExcludeFood.foodContainsBeef.currentTextColor == ContextCompat.getColor(
                    activity!!,
                    R.color.color_blue
                )
            ) {
                binding.layoutExcludeFood.foodContainsBeef.setTextColor(
                    ContextCompat.getColor(
                        activity!!,
                        R.color.white
                    )
                )
                binding.layoutExcludeFood.foodContainsBeef.setBackgroundResource(R.drawable.blue_button_bg)
                Utility.setFood(activity!!.applicationContext, true)
                Utility.list[1] = true
            } else {
                binding.layoutExcludeFood.foodContainsBeef.setTextColor(
                    ContextCompat.getColor(
                        activity!!,
                        R.color.color_blue
                    )
                )
                binding.layoutExcludeFood.foodContainsBeef.setBackgroundResource(R.drawable.blue_button_uncheck_bg)
                Utility.list[1] = false
                Utility.setFood(activity!!.applicationContext, false)
            }

        }
        binding.layoutExcludeFood.foodContainsSoy.setOnClickListener {
            if (binding.layoutExcludeFood.foodContainsSoy.currentTextColor == ContextCompat.getColor(
                    activity!!,
                    R.color.color_blue
                )
            ) {
                binding.layoutExcludeFood.foodContainsSoy.setTextColor(
                    ContextCompat.getColor(
                        activity!!,
                        R.color.white
                    )
                )
                binding.layoutExcludeFood.foodContainsSoy.setBackgroundResource(R.drawable.blue_button_bg)
                Utility.setFood(activity!!.applicationContext, true)
                Utility.list[2] = true
            } else {
                binding.layoutExcludeFood.foodContainsSoy.setTextColor(
                    ContextCompat.getColor(
                        activity!!,
                        R.color.color_blue
                    )
                )
                binding.layoutExcludeFood.foodContainsSoy.setBackgroundResource(R.drawable.blue_button_uncheck_bg)
                Utility.setFood(activity!!.applicationContext, false)
                Utility.list[2] = false
            }

        }
        binding.layoutExcludeFood.foodContainsPork.setOnClickListener {
            if (binding.layoutExcludeFood.foodContainsPork.currentTextColor == ContextCompat.getColor(
                    activity!!,
                    R.color.color_blue
                )
            ) {
                binding.layoutExcludeFood.foodContainsPork.setTextColor(
                    ContextCompat.getColor(
                        activity!!,
                        R.color.white
                    )
                )
                binding.layoutExcludeFood.foodContainsPork.setBackgroundResource(R.drawable.blue_button_bg)
                Utility.setFood(activity!!.applicationContext, true)
                Utility.list[3] = true
            } else {
                binding.layoutExcludeFood.foodContainsPork.setTextColor(
                    ContextCompat.getColor(
                        activity!!,
                        R.color.color_blue
                    )
                )
                binding.layoutExcludeFood.foodContainsPork.setBackgroundResource(R.drawable.blue_button_uncheck_bg)
                Utility.setFood(activity!!.applicationContext, false)
                Utility.list[3] = false
            }

        }
        binding.layoutExcludeFood.foodContainsEggs.setOnClickListener {
            if (binding.layoutExcludeFood.foodContainsEggs.currentTextColor == ContextCompat.getColor(
                    activity!!,
                    R.color.color_blue
                )
            ) {
                binding.layoutExcludeFood.foodContainsEggs.setTextColor(
                    ContextCompat.getColor(
                        activity!!,
                        R.color.white
                    )
                )
                binding.layoutExcludeFood.foodContainsEggs.setBackgroundResource(R.drawable.blue_button_bg)
                Utility.setFood(activity!!.applicationContext, true)
                Utility.list[4] = true
            } else {
                binding.layoutExcludeFood.foodContainsEggs.setTextColor(
                    ContextCompat.getColor(
                        activity!!,
                        R.color.color_blue
                    )
                )
                binding.layoutExcludeFood.foodContainsEggs.setBackgroundResource(R.drawable.blue_button_uncheck_bg)
                Utility.setFood(activity!!.applicationContext, false)
                Utility.list[4] = false
            }

        }
        binding.layoutExcludeFood.foodContainsDairy.setOnClickListener {
            if (binding.layoutExcludeFood.foodContainsDairy.currentTextColor == ContextCompat.getColor(
                    activity!!,
                    R.color.color_blue
                )
            ) {
                binding.layoutExcludeFood.foodContainsDairy.setTextColor(
                    ContextCompat.getColor(
                        activity!!,
                        R.color.white
                    )
                )
                binding.layoutExcludeFood.foodContainsDairy.setBackgroundResource(R.drawable.blue_button_bg)
                Utility.setFood(activity!!.applicationContext, true)
                Utility.list[5] = true
            } else {
                binding.layoutExcludeFood.foodContainsDairy.setTextColor(
                    ContextCompat.getColor(
                        activity!!,
                        R.color.color_blue
                    )
                )
                binding.layoutExcludeFood.foodContainsDairy.setBackgroundResource(R.drawable.blue_button_uncheck_bg)
                Utility.setFood(activity!!.applicationContext, false)
                Utility.list[5] = false
            }

        }
        binding.layoutExcludeFood.foodContainsNuts.setOnClickListener {
            if (binding.layoutExcludeFood.foodContainsNuts.currentTextColor == ContextCompat.getColor(
                    activity!!,
                    R.color.color_blue
                )
            ) {
                binding.layoutExcludeFood.foodContainsNuts.setTextColor(
                    ContextCompat.getColor(
                        activity!!,
                        R.color.white
                    )
                )
                binding.layoutExcludeFood.foodContainsNuts.setBackgroundResource(R.drawable.blue_button_bg)
                Utility.setFood(activity!!.applicationContext, true)
                Utility.list[6] = true
            } else {
                binding.layoutExcludeFood.foodContainsNuts.setTextColor(
                    ContextCompat.getColor(
                        activity!!,
                        R.color.color_blue
                    )
                )
                binding.layoutExcludeFood.foodContainsNuts.setBackgroundResource(R.drawable.blue_button_uncheck_bg)
                Utility.setFood(activity!!.applicationContext, false)
                Utility.list[6] = false
            }

        }
        binding.layoutExcludeFood.foodContainsShellFish.setOnClickListener {
            if (binding.layoutExcludeFood.foodContainsShellFish.currentTextColor == ContextCompat.getColor(
                    activity!!,
                    R.color.color_blue
                )
            ) {
                binding.layoutExcludeFood.foodContainsShellFish.setTextColor(
                    ContextCompat.getColor(
                        activity!!,
                        R.color.white
                    )
                )
                binding.layoutExcludeFood.foodContainsShellFish.setBackgroundResource(R.drawable.blue_button_bg)
                Utility.setFood(activity!!.applicationContext, true)
                Utility.list[7] = true
            } else {
                binding.layoutExcludeFood.foodContainsShellFish.setTextColor(
                    ContextCompat.getColor(
                        activity!!,
                        R.color.color_blue
                    )
                )
                binding.layoutExcludeFood.foodContainsShellFish.setBackgroundResource(R.drawable.blue_button_uncheck_bg)
                Utility.setFood(activity!!.applicationContext, false)
                Utility.list[7] = false
            }

        }

    }

    private fun validateAlcoholQuestionnaire() {
        binding.layoutAlcohol.radioAlcohol.setOnCheckedChangeListener { _, checkedId ->
            when (checkedId) {
                R.id.btn_yes -> {
                    Utility.list[11] = true
                    Utility.setAlcohol(activity!!.applicationContext, true)
                }
                R.id.btn_no -> {
                    Utility.list[11] = false
                    Utility.setAlcohol(activity!!.applicationContext, true)
                }
            }

        }
    }

    private fun validateMealQuestionnaire() {


        binding.layoutMeals.slider.position = 0
        binding.layoutMeals.slider.setOnDiscreteSliderChangeListener {
            if (binding.layoutMeals.slider.position >= 0) {


                Utility.listValue[1] = (it + 2).toString()
                binding.layoutMeals.mealText.text = (it + 2).toString()
                Utility.setMeals(activity!!.applicationContext, true)
            }
        }
    }

    private fun validateBMIQuestionnaire() {
        Utility.listValue[2] = "cm"
        Utility.listValue[3] = "kg"
        binding.layoutBmi.btnCm.isChecked = true
        binding.layoutBmi.btnKg.isChecked = true
        formatHeightEdtTxt()
        formatWeightEdtTxt()
        binding.layoutBmi.radioHeightMeasure.setOnCheckedChangeListener { _, checkedId ->
            when (checkedId) {

                R.id.btn_cm -> {
                    Utility.listValue[2] = "cm"
                    binding.layoutBmi.edtHeight.setText(Utility.convertFeetAndInchesToCentimeter(binding.layoutBmi.edtHeight.text.toString()))


                }
                R.id.btn_ft -> {
                    Utility.listValue[2] = "ft"
                    binding.layoutBmi.edtHeight.setText(Utility.centimeterToFeet(binding.layoutBmi.edtHeight.text.toString()))

                }
            }

        }


        binding.layoutBmi.radioWeightMeasure.setOnCheckedChangeListener { _, checkedId ->
            when (checkedId) {
                R.id.btn_kg -> {
                    Utility.listValue[3] = "kg"
                    binding.layoutBmi.edtWeight.setText(Utility.poundTokG(binding.layoutBmi.edtWeight.text.toString()))

                }
                R.id.btn_lbs -> {
                    Utility.listValue[3] = "lbs"
                    binding.layoutBmi.edtWeight.setText(Utility.kGToPound(binding.layoutBmi.edtWeight.text.toString()))

                }
            }

        }

    }


    private fun formatHeightEdtTxt() {
        binding.layoutBmi.edtHeight.addTextChangedListener(object : TextWatcher {


            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}

            @SuppressLint("SetTextI18n")
            override fun afterTextChanged(s: Editable) {

                val height = binding.layoutBmi.edtHeight.text.toString()
                if ((binding.layoutBmi.btnCm.isChecked || binding.layoutBmi.btnFt.isChecked) && height != "0" && height !=
                    "0.0" && !TextUtils.isEmpty(binding.layoutBmi.edtHeight.text.toString())
                ) {
                    Utility.setBMIIndex(activity!!.applicationContext, true)
                    Utility.listValue[4] = binding.layoutBmi.edtHeight.text.toString()
                } else {
                    Utility.setBMIIndex(activity!!.applicationContext, false)
                }
            }
        })
    }


    private fun formatWeightEdtTxt() {
        binding.layoutBmi.edtWeight.addTextChangedListener(object : TextWatcher {


            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}

            @SuppressLint("SetTextI18n")
            override fun afterTextChanged(s: Editable) {
                val weight = binding.layoutBmi.edtWeight.text.toString()
                if (weight != "0" && weight != "0.0" && !TextUtils.isEmpty(weight)) {
                    if (binding.layoutBmi.btnKg.isChecked && binding.layoutBmi.edtWeight.text.toString().toDouble() < 226.0) {
                        Utility.setBMIIndex1(activity!!.applicationContext, true)
                        Utility.listValue[5] = binding.layoutBmi.edtWeight.text.toString()
                    } else if (binding.layoutBmi.btnLbs.isChecked && binding.layoutBmi.edtWeight.text.toString().toDouble() < 500.0) {
                        Utility.setBMIIndex1(activity!!.applicationContext, true)
                        Utility.listValue.set(index = 5, element = binding.layoutBmi.edtWeight.text.toString())
                    }
                } else {
                    Utility.setBMIIndex1(activity!!.applicationContext, false)
                }
            }
        })
    }

    private fun formatEdtTxt() {
        binding.layoutPersonalDetails.edtDob.addTextChangedListener(object : TextWatcher {


            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}

            @SuppressLint("SetTextI18n")
            override fun afterTextChanged(s: Editable) {
                if (count <= binding.layoutPersonalDetails.edtDob.text.toString().length && (binding.layoutPersonalDetails.edtDob.text.toString().length == 2 || binding.layoutPersonalDetails.edtDob.text.toString().length == 5)) {
                    binding.layoutPersonalDetails.edtDob.setText(binding.layoutPersonalDetails.edtDob.text.toString() + "/")
                    val pos = binding.layoutPersonalDetails.edtDob.text.length
                    binding.layoutPersonalDetails.edtDob.setSelection(pos)
                } else if (count >= binding.layoutPersonalDetails.edtDob.text.toString().length && (binding.layoutPersonalDetails.edtDob.text.toString().length == 2 || binding.layoutPersonalDetails.edtDob.text.toString().length == 5)) {
                    binding.layoutPersonalDetails.edtDob.setText(
                        binding.layoutPersonalDetails.edtDob.text.toString().substring(
                            0,
                            binding.layoutPersonalDetails.edtDob.text.toString().length - 1
                        )
                    )
                    val pos = binding.layoutPersonalDetails.edtDob.text.length
                    binding.layoutPersonalDetails.edtDob.setSelection(pos)
                }
                count = binding.layoutPersonalDetails.edtDob.text.toString().length
                val date = binding.layoutPersonalDetails.edtDob.text.toString()



                if (binding.layoutPersonalDetails.edtDob.text.length == 10) {
                    if ((date.substring(0, 2).toInt() < 32 && date.substring(3, 5).toInt() < 13 && date.substring(
                            6,
                            10
                        ).toInt() < 2015)
                    )
                        Utility.setPersonalDetails(activity!!.applicationContext, true)
                    Utility.listValue[10] = date
                }

                if (binding.layoutPersonalDetails.edtDob.text.length == 10 && activity != null) {
                    val imm = activity!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager?
                    imm!!.hideSoftInputFromWindow(binding.layoutPersonalDetails.edtDob.windowToken, 0)
                }

            }
        })
    }


    private fun validatePersonalQuestionnaire() {
        formatEdtTxt()
        binding.layoutPersonalDetails.radioSex.setOnCheckedChangeListener { _, checkedId ->
            when (checkedId) {
                R.id.btn_female -> {
                    Utility.listValue[11] = "female"
                    isGenderSelected = true
                    Utility.setGender(activity!!.applicationContext, true)
                }
                R.id.btn_male -> {
                    Utility.listValue[11] = "male"
                    isGenderSelected = true
                    Utility.setGender(activity!!.applicationContext, true)
                }
            }

        }

    }


    @SuppressLint("ResourceType")
    private fun validateActivityQuestionnaire() {
        binding.layoutActivity.llPlus.setOnClickListener {
            when (activityCount) {
                0 -> {
                    binding.layoutActivity.activityLevel1.isChecked = true
                    binding.layoutActivity.activityLevel2.isChecked = false
                    binding.layoutActivity.activityLevel3.isChecked = false
                    binding.layoutActivity.activityLevel4.isChecked = false
                    binding.layoutActivity.activityLevel5.isChecked = false
                    binding.layoutActivity.activityLevel6.isChecked = false
                    binding.layoutActivity.activityLevel7.isChecked = false
                    activityCount = 1
                    Utility.listValue[6] = "23"
                    Utility.setActivity(activity!!.applicationContext, true)
                    binding.layoutActivity.activityTitle.text = activity!!.resources.getString(R.string.activity_text_1)
                }
                1 -> {
                    binding.layoutActivity.llMinus.visibility = VISIBLE
                    activityCount = 2

                    binding.layoutActivity.activityLevel1.isChecked = true
                    binding.layoutActivity.activityLevel2.isChecked = true
                    binding.layoutActivity.activityLevel3.isChecked = false
                    binding.layoutActivity.activityLevel4.isChecked = false
                    binding.layoutActivity.activityLevel5.isChecked = false
                    binding.layoutActivity.activityLevel6.isChecked = false
                    binding.layoutActivity.activityLevel7.isChecked = false
                    binding.layoutActivity.activityTitle.text = activity!!.resources.getString(R.string.activity_text_2)
                    Utility.listValue[6] = "24"
                    Utility.setActivity(activity!!.applicationContext, true)
                }
                2 -> {
                    Utility.listValue[6] = "25"
                    Utility.setActivity(activity!!.applicationContext, true)
                    activityCount = 3
                    binding.layoutActivity.activityLevel1.isChecked = true
                    binding.layoutActivity.activityLevel2.isChecked = true
                    binding.layoutActivity.activityLevel3.isChecked = true
                    binding.layoutActivity.activityLevel4.isChecked = false
                    binding.layoutActivity.activityLevel5.isChecked = false
                    binding.layoutActivity.activityLevel6.isChecked = false
                    binding.layoutActivity.activityLevel7.isChecked = false
                    binding.layoutActivity.activityTitle.text = activity!!.resources.getString(R.string.activity_text_3)

                }
                3 -> {
                    Utility.listValue[6] = "26"
                    Utility.setActivity(activity!!.applicationContext, true)
                    activityCount = 4
                    binding.layoutActivity.activityLevel1.isChecked = true
                    binding.layoutActivity.activityLevel2.isChecked = true
                    binding.layoutActivity.activityLevel3.isChecked = true
                    binding.layoutActivity.activityLevel4.isChecked = true
                    binding.layoutActivity.activityLevel5.isChecked = false
                    binding.layoutActivity.activityLevel6.isChecked = false
                    binding.layoutActivity.activityLevel7.isChecked = false
                    binding.layoutActivity.activityTitle.text = activity!!.resources.getString(R.string.activity_text_4)

                }
                4 -> {
                    Utility.listValue[6] = "27"
                    Utility.setActivity(activity!!.applicationContext, true)
                    activityCount = 5
                    binding.layoutActivity.activityLevel1.isChecked = true
                    binding.layoutActivity.activityLevel2.isChecked = true
                    binding.layoutActivity.activityLevel3.isChecked = true
                    binding.layoutActivity.activityLevel4.isChecked = true
                    binding.layoutActivity.activityLevel5.isChecked = true
                    binding.layoutActivity.activityLevel6.isChecked = false
                    binding.layoutActivity.activityLevel7.isChecked = false
                    binding.layoutActivity.activityTitle.text = activity!!.resources.getString(R.string.activity_text_5)

                }
                5 -> {
                    Utility.listValue[6] = "28"
                    Utility.setActivity(activity!!.applicationContext, true)
                    activityCount = 6
                    binding.layoutActivity.activityLevel1.isChecked = true
                    binding.layoutActivity.activityLevel2.isChecked = true
                    binding.layoutActivity.activityLevel3.isChecked = true
                    binding.layoutActivity.activityLevel4.isChecked = true
                    binding.layoutActivity.activityLevel5.isChecked = true
                    binding.layoutActivity.activityLevel6.isChecked = true
                    binding.layoutActivity.activityLevel7.isChecked = false
                    binding.layoutActivity.activityTitle.text = activity!!.resources.getString(R.string.activity_text_6)


                }
                6 -> {
                    activityCount = 7
                    binding.layoutActivity.activityLevel1.isChecked = true
                    binding.layoutActivity.activityLevel2.isChecked = true
                    binding.layoutActivity.activityLevel3.isChecked = true
                    binding.layoutActivity.activityLevel4.isChecked = true
                    binding.layoutActivity.activityLevel5.isChecked = true
                    binding.layoutActivity.activityLevel6.isChecked = true
                    binding.layoutActivity.activityLevel7.isChecked = true
                    Utility.listValue[6] = "29"
                    Utility.setActivity(activity!!.applicationContext, true)
                    binding.layoutActivity.activityTitle.text = activity!!.resources.getString(R.string.activity_text_7)
                }
            }
        }
        binding.layoutActivity.llMinus.setOnClickListener {
            when (activityCount) {
                2 -> {
                    binding.layoutActivity.activityLevel1.isChecked = true
                    binding.layoutActivity.activityLevel2.isChecked = false
                    binding.layoutActivity.activityLevel3.isChecked = false
                    binding.layoutActivity.activityLevel4.isChecked = false
                    binding.layoutActivity.activityLevel5.isChecked = false
                    binding.layoutActivity.activityLevel6.isChecked = false
                    binding.layoutActivity.activityLevel7.isChecked = false
                    activityCount = 1
                    Utility.listValue[6] = "23"
                    Utility.setActivity(activity!!.applicationContext, true)
                    binding.layoutActivity.activityTitle.text = activity!!.resources.getString(R.string.activity_text_1)
                }
                3 -> {
                    activityCount = 2

                    binding.layoutActivity.activityLevel1.isChecked = true
                    binding.layoutActivity.activityLevel2.isChecked = true
                    binding.layoutActivity.activityLevel3.isChecked = false
                    binding.layoutActivity.activityLevel4.isChecked = false
                    binding.layoutActivity.activityLevel5.isChecked = false
                    binding.layoutActivity.activityLevel6.isChecked = false
                    binding.layoutActivity.activityLevel7.isChecked = false
                    binding.layoutActivity.activityTitle.text = activity!!.resources.getString(R.string.activity_text_2)
                    Utility.listValue[6] = "24"
                    Utility.setActivity(activity!!.applicationContext, true)
                }
                4 -> {
                    Utility.listValue[6] = "25"
                    Utility.setActivity(activity!!.applicationContext, true)
                    activityCount = 3
                    binding.layoutActivity.activityLevel1.isChecked = true
                    binding.layoutActivity.activityLevel2.isChecked = true
                    binding.layoutActivity.activityLevel3.isChecked = true
                    binding.layoutActivity.activityLevel4.isChecked = false
                    binding.layoutActivity.activityLevel5.isChecked = false
                    binding.layoutActivity.activityLevel6.isChecked = false
                    binding.layoutActivity.activityLevel7.isChecked = false
                    binding.layoutActivity.activityTitle.text = activity!!.resources.getString(R.string.activity_text_3)

                }
                5 -> {
                    Utility.listValue[6] = "26"
                    Utility.setActivity(activity!!.applicationContext, true)
                    activityCount = 4
                    binding.layoutActivity.activityLevel1.isChecked = true
                    binding.layoutActivity.activityLevel2.isChecked = true
                    binding.layoutActivity.activityLevel3.isChecked = true
                    binding.layoutActivity.activityLevel4.isChecked = true
                    binding.layoutActivity.activityLevel5.isChecked = false
                    binding.layoutActivity.activityLevel6.isChecked = false
                    binding.layoutActivity.activityLevel7.isChecked = false
                    binding.layoutActivity.activityTitle.text = activity!!.resources.getString(R.string.activity_text_4)

                }
                6 -> {
                    Utility.listValue[6] = "27"
                    Utility.setActivity(activity!!.applicationContext, true)
                    activityCount = 5
                    binding.layoutActivity.activityLevel1.isChecked = true
                    binding.layoutActivity.activityLevel2.isChecked = true
                    binding.layoutActivity.activityLevel3.isChecked = true
                    binding.layoutActivity.activityLevel4.isChecked = true
                    binding.layoutActivity.activityLevel5.isChecked = true
                    binding.layoutActivity.activityLevel6.isChecked = false
                    binding.layoutActivity.activityLevel7.isChecked = false
                    binding.layoutActivity.activityTitle.text = activity!!.resources.getString(R.string.activity_text_5)

                }
                7 -> {
                    Utility.listValue[6] = "28"
                    Utility.setActivity(activity!!.applicationContext, true)
                    activityCount = 6
                    binding.layoutActivity.activityLevel1.isChecked = true
                    binding.layoutActivity.activityLevel2.isChecked = true
                    binding.layoutActivity.activityLevel3.isChecked = true
                    binding.layoutActivity.activityLevel4.isChecked = true
                    binding.layoutActivity.activityLevel5.isChecked = true
                    binding.layoutActivity.activityLevel6.isChecked = true
                    binding.layoutActivity.activityLevel7.isChecked = false
                    binding.layoutActivity.activityTitle.text = activity!!.resources.getString(R.string.activity_text_6)


                }

            }
        }

        binding.layoutActivity.activityLevel1.setOnClickListener {

            binding.layoutActivity.activityLevel1.isChecked = true
            binding.layoutActivity.activityLevel2.isChecked = false
            binding.layoutActivity.activityLevel3.isChecked = false
            binding.layoutActivity.activityLevel4.isChecked = false
            binding.layoutActivity.activityLevel5.isChecked = false
            binding.layoutActivity.activityLevel6.isChecked = false
            binding.layoutActivity.activityLevel7.isChecked = false
            activityCount = 1
            Utility.listValue[6] = "23"
            Utility.setActivity(activity!!.applicationContext, true)

            binding.layoutActivity.activityTitle.text = activity!!.resources.getString(R.string.activity_text_1)

        }
        binding.layoutActivity.activityLevel2.setOnClickListener {

            activityCount = 2

            binding.layoutActivity.activityLevel1.isChecked = true
            binding.layoutActivity.activityLevel2.isChecked = true
            binding.layoutActivity.activityLevel3.isChecked = false
            binding.layoutActivity.activityLevel4.isChecked = false
            binding.layoutActivity.activityLevel5.isChecked = false
            binding.layoutActivity.activityLevel6.isChecked = false
            binding.layoutActivity.activityLevel7.isChecked = false
            binding.layoutActivity.activityTitle.text = activity!!.resources.getString(R.string.activity_text_2)
            Utility.listValue[6] = "24"
            Utility.setActivity(activity!!.applicationContext, true)

        }
        binding.layoutActivity.activityLevel3.setOnClickListener {
            //            resetActivity()
            Utility.listValue[6] = "25"
            Utility.setActivity(activity!!.applicationContext, true)
            activityCount = 3
            binding.layoutActivity.activityLevel1.isChecked = true
            binding.layoutActivity.activityLevel2.isChecked = true
            binding.layoutActivity.activityLevel3.isChecked = true
            binding.layoutActivity.activityLevel4.isChecked = false
            binding.layoutActivity.activityLevel5.isChecked = false
            binding.layoutActivity.activityLevel6.isChecked = false
            binding.layoutActivity.activityLevel7.isChecked = false
            binding.layoutActivity.activityTitle.text = activity!!.resources.getString(R.string.activity_text_3)
        }
        binding.layoutActivity.activityLevel4.setOnClickListener {

            Utility.listValue[6] = "26"
            Utility.setActivity(activity!!.applicationContext, true)
            activityCount = 4
            binding.layoutActivity.activityLevel1.isChecked = true
            binding.layoutActivity.activityLevel2.isChecked = true
            binding.layoutActivity.activityLevel3.isChecked = true
            binding.layoutActivity.activityLevel4.isChecked = true
            binding.layoutActivity.activityLevel5.isChecked = false
            binding.layoutActivity.activityLevel6.isChecked = false
            binding.layoutActivity.activityLevel7.isChecked = false
            binding.layoutActivity.activityTitle.text = activity!!.resources.getString(R.string.activity_text_4)
        }
        binding.layoutActivity.activityLevel5.setOnClickListener {

            Utility.listValue[6] = "27"
            Utility.setActivity(activity!!.applicationContext, true)
            activityCount = 5
            binding.layoutActivity.activityLevel1.isChecked = true
            binding.layoutActivity.activityLevel2.isChecked = true
            binding.layoutActivity.activityLevel3.isChecked = true
            binding.layoutActivity.activityLevel4.isChecked = true
            binding.layoutActivity.activityLevel5.isChecked = true
            binding.layoutActivity.activityLevel6.isChecked = false
            binding.layoutActivity.activityLevel7.isChecked = false
            binding.layoutActivity.activityTitle.text = activity!!.resources.getString(R.string.activity_text_5)
        }
        binding.layoutActivity.activityLevel6.setOnClickListener {

            Utility.listValue[6] = "28"
            Utility.setActivity(activity!!.applicationContext, true)
            activityCount = 6
            binding.layoutActivity.activityLevel1.isChecked = true
            binding.layoutActivity.activityLevel2.isChecked = true
            binding.layoutActivity.activityLevel3.isChecked = true
            binding.layoutActivity.activityLevel4.isChecked = true
            binding.layoutActivity.activityLevel5.isChecked = true
            binding.layoutActivity.activityLevel6.isChecked = true
            binding.layoutActivity.activityLevel7.isChecked = false
            binding.layoutActivity.activityTitle.text = activity!!.resources.getString(R.string.activity_text_6)
        }
        binding.layoutActivity.activityLevel7.setOnClickListener {

            activityCount = 7
            binding.layoutActivity.activityLevel1.isChecked = true
            binding.layoutActivity.activityLevel2.isChecked = true
            binding.layoutActivity.activityLevel3.isChecked = true
            binding.layoutActivity.activityLevel4.isChecked = true
            binding.layoutActivity.activityLevel5.isChecked = true
            binding.layoutActivity.activityLevel6.isChecked = true
            binding.layoutActivity.activityLevel7.isChecked = true
            Utility.listValue[6] = "29"
            Utility.setActivity(activity!!.applicationContext, true)
            binding.layoutActivity.activityTitle.text = activity!!.resources.getString(R.string.activity_text_7)
        }
    }

}
