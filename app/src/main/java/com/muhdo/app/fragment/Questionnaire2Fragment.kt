package com.muhdo.app.fragment

import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.text.Html
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.muhdo.app.R
import com.muhdo.app.apiModel.mealModel.*
import com.muhdo.app.data.MealData
import com.muhdo.app.databinding.FragmentQuestionAnswerBinding
import com.muhdo.app.repository.ApiUtilis
import com.muhdo.app.repository.ErrorModel
import com.muhdo.app.repository.NetworkManager
import com.muhdo.app.repository.ServiceListener
import com.muhdo.app.ui.ChooseMealActivity
import com.muhdo.app.utils.Events
import com.muhdo.app.utils.GlobalBus
import com.muhdo.app.utils.v3.Constants
import com.timingsystemkotlin.backuptimingsystem.Utility
import org.greenrobot.eventbus.Subscribe


class Questionnaire2Fragment : Fragment(), MealQuestionAnswerFragment.OnDataInputListener {
    override fun onDataInput(questionType: String, questionData: String) {

        when (questionType) {
            Constants.MEAL_QUESTION_TYPE_GOAL -> {
                if (questionData != "") {
                    userInput = true
                }
            }
        }
    }

    internal lateinit var binding: FragmentQuestionAnswerBinding
    private var currentPage = 0
    private var userInput = false
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        GlobalBus.getBus()!!.register(this@Questionnaire2Fragment)
        val view = inflater.inflate(R.layout.fragment_question_answer, container, false)
        binding = FragmentQuestionAnswerBinding.bind(view)
        addBottomDots(currentPage)
        resetData()
        init()


        binding.btncontinue.setOnClickListener {
            when (currentPage) {
                0 -> {
                    if (Utility.isGoalDone(activity!!.applicationContext)) {
                        currentPage++
                        addBottomDots(currentPage)

                    } else {
                        showAlert("Please select your goal")
                    }
                }
                1 -> {
                    if (Utility.isDietDone(activity!!.applicationContext) && Utility.isDietDone1(
                            activity!!.applicationContext
                        ) && Utility.isDietDone2(
                            activity!!.applicationContext
                        )
                    ) {
                        currentPage++
                        addBottomDots(currentPage)

                    } else {
                        showAlert("Please select all diet options")
                    }

                }
                2 -> {
                    currentPage++
                    addBottomDots(currentPage)
                }
                3 -> {
                    if (Utility.isAlcohol(activity!!.applicationContext)) {
                        currentPage++
                        addBottomDots(currentPage)
                        Utility.list

                    } else {
                        showAlert("Please select alcohol consumption")
                    }

                }
                4 -> {
                    if (Utility.isMeals(activity!!.applicationContext)) {
                        currentPage++
                        addBottomDots(currentPage)

                    } else {
                        Utility.setMeals(activity!!.applicationContext, true)
                        Utility.listValue[1] = "2"
                        currentPage++
                        addBottomDots(currentPage)
                    }

                }
                5 -> {
                    if (Utility.isBMIIndex(activity!!.applicationContext) && Utility.isBMIIndex1(
                            activity!!.applicationContext
                        )
                    ) {
                        currentPage++
                        addBottomDots(currentPage)

                    } else {
                        showAlert("Please enter your height and weight")
                    }

                }
                6 -> {
                    if (Utility.isPersonalDetails(activity!!.applicationContext) &&
                        Utility.isGender(activity!!.applicationContext)
                    ) {
                        currentPage++

                        addBottomDots(currentPage)


                    } else {
                        if (!Utility.isPersonalDetails(activity!!.applicationContext)) {
                            showAlert("Please enter correct date of birth")
                        } else
                            if (!Utility.isGender(activity!!.applicationContext)) {
                                showAlert("Please select your sex")
                            }
                    }

                }
                7 -> {
                    if (Utility.isActivity(activity!!.applicationContext)) {
                        sendDataToServer()
                        Utility.list
                    } else {
                        showAlert("Please select activity level")
                    }

                }


            }

            binding.imgClose.setOnClickListener {
                activity!!.fragmentManager.popBackStack()
            }

        }
        return view

    }

    override fun onDestroyView() {
        super.onDestroyView()
        // unregister the registered event.
        GlobalBus.getBus()!!.unregister(this)
    }

    @Subscribe
    fun getMessage(sendPageCount: Events.sendPageCount) {
        currentPage = sendPageCount.page
        addBottomDots(currentPage)
    }


    private fun init() {
        for (i in 0..11) {
            Utility.list.add(false)
            Utility.listValue.add("")
        }
    }

    private fun resetData() {
        Utility.setGoalDone(activity!!.applicationContext, false)
        Utility.setDietDone(activity!!.applicationContext, false)
        Utility.setDietDone1(activity!!.applicationContext, false)
        Utility.setDietDone2(activity!!.applicationContext, false)
        Utility.setFood(activity!!.applicationContext, false)
        Utility.setAlcohol(activity!!.applicationContext, false)
        Utility.setActivity(activity!!.applicationContext, false)
        Utility.setMeals(activity!!.applicationContext, false)
        Utility.setBMIIndex(activity!!.applicationContext, false)
        Utility.setBMIIndex1(activity!!.applicationContext, false)
        Utility.setPersonalDetails(activity!!.applicationContext, false)
        Utility.setGender(activity!!.applicationContext, false)
    }

    private fun addBottomDots(currentPage: Int) {

        when (currentPage) {
            1 -> {
                binding.btncontinue.visibility = VISIBLE
            }
        }


        val dots = arrayOfNulls<TextView>(8)
        binding.layoutDots.removeAllViews()
        for (i in dots.indices) {

            dots[i] = TextView(activity)
            dots[i]!!.text = Html.fromHtml("&#8226;")
            dots[i]!!.textSize = 35f
            dots[i]!!.setTextColor(
                ContextCompat.getColor(
                    activity!!.applicationContext,
                    R.color.secondary_color
                )
            )
            binding.layoutDots.addView(dots[i])
        }

        if (dots.isNotEmpty()) {
            dots[currentPage]!!.setTextColor(
                ContextCompat.getColor(
                    activity!!.applicationContext,
                    R.color.color_dark_blue
                )
            )
        }
        loadFragment(currentPage)
    }

    private fun loadFragment(position: Int) {
        val transaction = childFragmentManager.beginTransaction()
        val bundle = Bundle()
        when (position) {
            0 -> {
                bundle.putString(Constants.MEAL_QUESTION_TYPE, Constants.MEAL_QUESTION_TYPE_GOAL)
            }
            1 -> {
                bundle.putString(Constants.MEAL_QUESTION_TYPE, Constants.MEAL_QUESTION_TYPE_DIET)
            }
            2 -> {
                bundle.putString(Constants.MEAL_QUESTION_TYPE, Constants.MEAL_QUESTION_TYPE_FOOD)
            }
            3 -> {
                bundle.putString(Constants.MEAL_QUESTION_TYPE, Constants.MEAL_QUESTION_TYPE_ALCOHOL)
            }
            4 -> {
                bundle.putString(Constants.MEAL_QUESTION_TYPE, Constants.MEAL_QUESTION_TYPE_MEALS)
            }
            5 -> {
                bundle.putString(Constants.MEAL_QUESTION_TYPE, Constants.MEAL_QUESTION_TYPE_BMI)
            }
            6 -> {
                bundle.putString(
                    Constants.MEAL_QUESTION_TYPE,
                    Constants.MEAL_QUESTION_TYPE_PERSONAL
                )
            }
            7 -> {
                bundle.putString(
                    Constants.MEAL_QUESTION_TYPE,
                    Constants.MEAL_QUESTION_TYPE_ACTIVITY
                )
            }
        }
        val fragInfo = MealQuestionAnswerFragment()
        fragInfo.arguments = bundle
        transaction.replace(R.id.flContent, fragInfo)
        transaction.commit()
    }


    private fun showAlert(msg: String) {
        val builder = AlertDialog.Builder(activity)
        builder.setMessage(msg)

        builder.setPositiveButton("OK") { _, _ ->

        }

        val alert = builder.create()
        alert.show()
    }


    private fun sendDataToServer() {
        binding.progressBar.visibility = View.VISIBLE
        binding.flContent.visibility = View.GONE
        val manager = NetworkManager()
        var height: Int
        var weight: Int
        val omit = Omit(
            Utility.list[1], Utility.list[4], Utility.list[5],
            Utility.list[0], Utility.list[3], Utility.list[6],
            Utility.list[2], Utility.list[7]
        )



        height = if (Utility.listValue[4].contains(".")) {
            // full file name
            val parts = Utility.listValue[4].split("\\.".toRegex()).dropLastWhile { it.isEmpty() }
                .toTypedArray() // String array, each element is text between dots

            val beforeFirstDot = parts[0]
            Integer.parseInt(beforeFirstDot)

        } else {
            Integer.parseInt(Utility.listValue[4])
        }
        weight = if (Utility.listValue[5].contains(".")) {
            // full file name
            val parts = Utility.listValue[5].split("\\.".toRegex()).dropLastWhile { it.isEmpty() }
                .toTypedArray() // String array, each element is text between dots

            val beforeFirstDot = parts[0]
            Integer.parseInt(beforeFirstDot)
        } else {
            Integer.parseInt(Utility.listValue[5])
        }

        if (height <= 0) {
            height = 25
        }

        if (weight <= 0) {
            weight = 25
        }


        val requestParams = MealRequest(
            Utility.listValue[0],
            Utility.list[9],
            Utility.list[8],
            Utility.list[10],
            omit, Utility.list[11], Utility.listValue[1].toInt(),
            height,
            Utility.listValue[2],
            weight,
            Utility.listValue[3],
            Utility.listValue[6].toInt(),
            Utility.getUserID(activity!!.applicationContext),
            "meal_plan",
            Utility.formatDate(Utility.listValue[10]),
            Utility.listValue[11]
        )
        val data = MealData(
            1, Utility.listValue[0],
            Utility.list[9].toString(),
            Utility.list[8].toString(),
            Utility.list[10].toString(),
            height.toString(), weight.toString(),
            Utility.listValue[3],
            Utility.listValue[2],
            Utility.list[11].toString(),
            Utility.listValue[6],
            Utility.listValue[1]
        )
        Log.e("Questionnaire2", " request params " + requestParams.toString())
        Log.e("Questionnaire2", " Meal data params " + data.toString())
        if (manager.isConnectingToInternet(activity!!.applicationContext)) {
            manager.createApiRequest(
                ApiUtilis.getAPIInstance(activity!!).sendMealSurvey(requestParams),
                object : ServiceListener<AddMealResponse> {
                    override fun getServerResponse(response: AddMealResponse, requestcode: Int) {
                        binding.progressBar.visibility = View.GONE
                      try{
                          if(response!=null){
                              if(response.getCode()==200){
                                  if(response.data!=null){
                                      binding.progressBar.visibility = View.VISIBLE
                                      callMealPlanSecondApi(response.data!!,data)
                                  }
                              }
                          }
                      }catch (e:Exception){
                           Log.d("data",""+e.printStackTrace())
                      }
                    }

                    override fun getError(error: ErrorModel, requestcode: Int) {
                        binding.progressBar.visibility = View.GONE
                        binding.flContent.visibility = View.VISIBLE
                        Utility.displayShortSnackBar(
                            binding.parentLayout,
                            error.getMessage().toString()
                        )
                    }
                })
        } else {
            binding.progressBar.visibility = View.GONE
            binding.flContent.visibility = View.VISIBLE
            Utility.displayShortSnackBar(
                binding.parentLayout,
                resources.getString(R.string.check_internet)
            )
        }
    }

    private fun callMealPlanSecondApi(

        requestParams: AddMealData,
        data: MealData
    ) {
        val manager = NetworkManager()
        if (manager.isConnectingToInternet(activity!!.applicationContext)) {
            manager.createApiRequest(
                ApiUtilis.getAPIInstance(activity!!).sendMealPlanResponse(requestParams),
                object : ServiceListener<MealPlanGenResponse> {
                    override fun getServerResponse(
                        response: MealPlanGenResponse,
                        requestcode: Int
                    ) {
                        try {
                            if (response != null) {
                                if (response.getCode() == 200) {
                                    if (response.getData() != null) {
                                        binding.progressBar.visibility = View.GONE
                                        binding.flContent.visibility = View.VISIBLE
                                        val mealData = Utility.getMealData(activity!!)
                                        if (mealData != null) {
                                            Utility.deleteMealInfo(
                                                activity!!.applicationContext,
                                                data
                                            )
                                            Utility.saveMealInfo(
                                                activity!!.applicationContext,
                                                data
                                            )

                                        } else {
                                            Utility.saveMealInfo(
                                                activity!!.applicationContext,
                                                data
                                            )
                                        }
                                        val i = Intent(activity, ChooseMealActivity::class.java)
                                        startActivity(i)
                                        Utility.setMealStatus(requireContext(), true)
                                    }

                                }
                            }
                        } catch (e: Exception) {

                        }
                        binding.progressBar.visibility = View.GONE
                        binding.flContent.visibility = View.VISIBLE
                        val mealData = Utility.getMealData(activity!!)
                        if (mealData != null) {
                            Utility.deleteMealInfo(activity!!.applicationContext, data)
                            Utility.saveMealInfo(activity!!.applicationContext, data)

                        } else {
                            Utility.saveMealInfo(activity!!.applicationContext, data)
                        }
                        val i = Intent(activity, ChooseMealActivity::class.java)
                        startActivity(i)
                        Utility.setMealStatus(requireContext(), true)
                    }

                    override fun getError(error: ErrorModel, requestcode: Int) {
                        binding.progressBar.visibility = View.GONE
                        binding.flContent.visibility = View.VISIBLE
                        Utility.displayShortSnackBar(
                            binding.parentLayout,
                            error.getMessage().toString()
                        )
                    }
                })
        } else {
            binding.progressBar.visibility = View.GONE
            binding.flContent.visibility = View.VISIBLE
            Utility.displayShortSnackBar(
                binding.parentLayout,
                resources.getString(R.string.check_internet)
            )
        }
    }
}