package com.muhdo.app.fragment.v3.account

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import androidx.viewpager.widget.ViewPager
import com.muhdo.app.R
import com.muhdo.app.databinding.V3FragmentPersonalInfoBinding
import com.muhdo.app.fragment.v3.MyAccountFragment
import com.muhdo.app.fragment.v3.MyProfileFragment
import com.muhdo.app.fragment.v3.QuestionnaireHomeFragment
import com.muhdo.app.utils.v3.TempUtil


class PersonalInfoFragment : Fragment() {
    lateinit var binding: V3FragmentPersonalInfoBinding
    var firstSelectPos = 0
    var tab_label =
        arrayOf(
            R.string.v3_tab_profile_and_preferences,
            R.string.v3_tab_questionnaire,
            R.string.v3_tab_kit_id_information,
            R.string.v3_tab_account_details
        )

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        val view =
            inflater.inflate(R.layout.v3_fragment_personal_info, container, false)
        binding = V3FragmentPersonalInfoBinding.bind(view)
        setupTabIcons()
        selectTabItem(firstSelectPos)

        return view
    }

    @SuppressLint("WrongConstant")
    private fun replaceFragment(fragment: Fragment) {
        val transaction = childFragmentManager.beginTransaction()
        transaction.replace(R.id.flContent, fragment)
        transaction.addToBackStack("share")
        transaction.commit()

    }

    private fun selectTabItem(position: Int) {
        binding.tabLayout.getTabAt(position)!!.select();
    }

    public fun selectedTabPosition(position: Int) {
        firstSelectPos = position
    }

    @SuppressLint("ResourceAsColor", "InflateParams")
    private fun setupTabIcons() {
        val fragmentAdapter = ViewPagerAdapter(childFragmentManager)
        binding.viewPager.adapter = fragmentAdapter
        binding.indicator.setViewPager(binding.viewPager)
        binding.viewPager.offscreenPageLimit = 0
        binding.tabLayout.setupWithViewPager(binding.viewPager)
        val adapter = ViewPagerAdapter(childFragmentManager)
        binding.viewPager.adapter = adapter
        adapter.addFrag(MyProfileFragment(), getString(tab_label[0]))
        adapter.addFrag(QuestionnaireHomeFragment(), getString(tab_label[1]))
        adapter.addFrag(KitIdInfoFragment(), getString(tab_label[2]))
        adapter.addFrag(MyAccountFragment(), getString(tab_label[3]))
        binding.viewPager.adapter = adapter

        for (i in 0 until tab_label.size) {
            val tabView = LayoutInflater.from(activity!!.applicationContext)
                .inflate(R.layout.custom_tab_personal_info, null)
            val tabLabel = tabView.findViewById(R.id.tab) as TextView
            tabLabel.text = getString(tab_label[i])
            tabLabel.setTextColor(context!!.getColor(R.color.colorPrimary))
            binding.tabLayout.getTabAt(i)!!.customView = tabView
        }
        val tabLabel =
            binding.tabLayout.getTabAt(0)!!.customView!!.findViewById(R.id.tab) as TextView
        tabLabel.setTextColor(context!!.resources.getColor(R.color.primary_error_color))
        binding.viewPager.addOnPageChangeListener((object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {

            }

            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
            }


            override fun onPageSelected(position: Int) {
                for (i in 0 until tab_label.size) {
                    val tabLabel =
                        binding.tabLayout.getTabAt(i)!!.customView!!.findViewById(R.id.tab) as TextView
                    tabLabel.setTextColor(context!!.resources.getColor(R.color.colorPrimary))
                }

                val tabLabel =
                    binding.tabLayout.getTabAt(position)!!.customView!!.findViewById(R.id.tab) as TextView
                tabLabel.setTextColor(context!!.resources.getColor(R.color.primary_error_color))
            }
        }))
    }

    internal inner class ViewPagerAdapter(manager: FragmentManager) :
        FragmentStatePagerAdapter(manager) {
        private val mFragmentList = arrayListOf<Fragment>()
        private val mFragmentTitleList = arrayListOf<String>()

        override fun getItem(position: Int): Fragment {

            return mFragmentList.get(position)
        }

        override fun getCount(): Int {
            return mFragmentList.size
        }

        fun addFrag(fragment: Fragment, title: String) {
            mFragmentList.add(fragment)
            mFragmentTitleList.add(title)
        }

        override fun getPageTitle(position: Int): CharSequence? {
            TempUtil.log("data", "tab pos = $position")
            return mFragmentTitleList[position]
        }
    }

}