package com.muhdo.app.fragment.v3.health_insights

import android.os.Build
import android.os.Bundle
import android.text.Html
import android.util.Base64
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.JsonObject
import com.muhdo.app.R
import com.muhdo.app.adapter.CustomSpinnerAdapter
import com.muhdo.app.adapter.v3.HealthInsightSpinnerAdapter
import com.muhdo.app.adapter.v3.HealthInsightsOverviewRecyAdpter
import com.muhdo.app.apiModel.healthinsights.Data
import com.muhdo.app.apiModel.healthinsights.ResponseHealthInisghts
import com.muhdo.app.databinding.V3FragmentHealthInsightDetailsResult2Binding
import com.muhdo.app.databinding.V3FragmentHealthInsightDetailsResultBinding
import com.muhdo.app.interfaces.v3.OnHealthInsightsHeaderValuesChanged
import com.muhdo.app.repository.ApiUtilis
import com.muhdo.app.utils.v3.TempUtil
import com.timingsystemkotlin.backuptimingsystem.Utility
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class HealthInsightsDetailsResults2Fragment : Fragment() {

  var onHealthInsightsHeaderValuesChanged:OnHealthInsightsHeaderValuesChanged?=null
  var tabPosition:Int?=0
    internal lateinit var binding: V3FragmentHealthInsightDetailsResult2Binding
    public lateinit var list: List<Data>
    public var selectedPostion = 0

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view =
            inflater.inflate(R.layout.v3_fragment_health_insight_details_result_2, container, false)
        binding = V3FragmentHealthInsightDetailsResult2Binding.bind(view)
        var spinnerItem = ArrayList<String>(0)
        for (value in list) {
            spinnerItem.add(value.title)
        }
        val spinnerAdapter =
            HealthInsightSpinnerAdapter(activity!!.applicationContext!!, spinnerItem)

        binding.spinner.adapter =
            spinnerAdapter//ArrayAdapter<String>(activity,R.layout.v3_item_dropdown_health_insight,spinnerItem)
        binding.spinner.setSelection(selectedPostion)
        binding.spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {}

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, pos: Int, p3: Long) {
                initData(pos)
                if (onHealthInsightsHeaderValuesChanged != null) {
                    onHealthInsightsHeaderValuesChanged!!.headerValuesChanged(
                        list.get(pos).title,
                        list.get(pos).status,
                        list.get(pos).output!!.indicator,
                        tabPosition!!
                    )
                }
            }

        }


        initData(selectedPostion)
        return view
    }

    var isResultExpanded = false
    var isRecommendationExpanded = false


    private fun initData(position: Int) {
        var data = list.get(position)
        isRecommendationExpanded = false
        isResultExpanded = false
        binding.tvGenesOfInterest.setText(data.genes_of_interest)
        binding.tvTitle.setText(data.title)
        binding.tvIntro.setText(Html.fromHtml(data.intro))
        binding.tvViewMoreResutls.setOnClickListener {
            var resultText: String =
                Html.fromHtml(data.output!!.result).toString()
            if (isResultExpanded) {
                binding.layoutResult.setBackgroundResource(R.drawable.v3_bg_health_insights_recommendation_highlated)
                binding.tvViewMoreResutls.setTextColor(context!!.getColor(R.color.v3_epi_gen_icon_unselected) )
                binding.tvViewMoreResutls.setText(R.string.v3_view_less)
            } else {
                binding.layoutResult.setBackgroundResource(R.drawable.v3_bg_epigen_recommendation_normal)

                binding.tvViewMoreResutls.setTextColor(context!!.getColor(R.color.health_insight_colorPrimaryDark) )
                binding.tvViewMoreResutls.setText(R.string.v3_view_more)
                if (resultText.length > 100) {
                    resultText = resultText.substring(0, 99) + "..."
                }
            }
            isResultExpanded = !isResultExpanded
            binding.tvResults.setText(Html.fromHtml(resultText))

        }
        binding.tvViewMoreRecommendations.setOnClickListener {
            var recommendationText: String =
                Html.fromHtml(data.output!!.recommondation).toString()

            if (isRecommendationExpanded) {
                binding.layoutRecomendations.setBackgroundResource(R.drawable.v3_bg_health_insights_recommendation_highlated)

                binding.tvViewMoreRecommendations.setText(R.string.v3_view_less)
                binding.tvViewMoreRecommendations.setTextColor(context!!.getColor(R.color.v3_epi_gen_icon_unselected) )
            } else {

                binding.tvViewMoreRecommendations.setText(R.string.v3_view_more)
                binding.tvViewMoreRecommendations.setTextColor(context!!.getColor(R.color.health_insight_colorPrimaryDark) )
                binding.layoutRecomendations.setBackgroundResource(R.drawable.v3_bg_epigen_recommendation_normal)
                if (recommendationText.length > 100) {
                    recommendationText = recommendationText.substring(0, 99) + "..."
                }
            }
            isRecommendationExpanded = !isRecommendationExpanded
            binding.tvRecommendations.setText(Html.fromHtml(recommendationText))

        }
        var recommendationText: String =
            Html.fromHtml(data.output!!.recommondation).toString()
        if (recommendationText.length > 100) {
            recommendationText = recommendationText.substring(0, 99) + "..."
        }
        var resultText: String =
            Html.fromHtml(data.output!!.result).toString()
        if (resultText.length > 100) {
            resultText = resultText.substring(0, 99) + "..."
        }

        binding.tvResults.setText(Html.fromHtml(resultText))
        binding.tvRecommendations.setText(Html.fromHtml(recommendationText))
        binding.tvViewMoreRecommendations.setText(R.string.v3_view_more)
        binding.tvViewMoreResutls.setText(R.string.v3_view_more)
        binding.tvRecommendations.setText(Html.fromHtml(recommendationText))
        binding.layoutResult.setBackgroundResource(R.drawable.v3_bg_epigen_recommendation_normal)
        binding.layoutRecomendations.setBackgroundResource(R.drawable.v3_bg_epigen_recommendation_normal)
        binding.tvViewMoreResutls.setTextColor(context!!.getColor(R.color.health_insight_colorPrimaryDark) )
        binding.tvViewMoreRecommendations.setTextColor(context!!.getColor(R.color.health_insight_colorPrimaryDark) )


    }


}