package com.muhdo.app.fragment.v3

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.annotation.SuppressLint
import android.app.Dialog
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnticipateInterpolator
import android.view.animation.OvershootInterpolator
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import androidx.viewpager.widget.ViewPager
import com.muhdo.app.R
import com.muhdo.app.databinding.V3FragmentGeneticOverviewResultBinding
import com.muhdo.app.interfaces.v3.OnOverviewLoadListener
import com.muhdo.app.utils.AnimatorUtils
import com.muhdo.app.utils.v3.TabUtils
import com.muhdo.app.utils.v3.TempUtil


class GeneticOverviewResultFragment : Fragment() {
    lateinit var binding: V3FragmentGeneticOverviewResultBinding
    lateinit var dialog: Dialog;

    var tab_icons = arrayOf<Int>(
        R.drawable.v3_ic_diet_weight_concerns,
        R.drawable.v3_ic_vitamin_deficiencies,
        R.drawable.v3_ic_physical_health_risk,
        R.drawable.v3_ic_physical_health_gifts,
        R.drawable.v3_ic_health_warnings,
        R.drawable.v3_ic_sleep_issues
    )
    var firstSelectPos = 0
    var showArc = false
    var tab_label =
        arrayOf<Int>(
            R.string.diet_weight_concerns,
            R.string.vitamin_deficiencies,
            R.string.physical_health_risks,
            R.string.physical_health_gifts,
            R.string.health_warnings,
            R.string.sleep_issues
        )

    public fun selectedTabPosition(position: Int) {
        firstSelectPos = position
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        val view =
            inflater.inflate(R.layout.v3_fragment_genetic_overview_result, container, false)
        binding = V3FragmentGeneticOverviewResultBinding.bind(view)


        setupTabIcons()

        binding.fab.setOnClickListener {
            if (!showArc) {
                showMenu()
                showArc = true
            } else {
                showArc = false
                hideMenu()
            }

        }

        binding.arcLayout.getChildAt(0).setOnClickListener {

            showArc = false
            selectTabItem(0)
            hideMenu()
//            binding.fab.setImageResource(R.drawable.v3_ic_diet_weight_concerns)
        }
        binding.arcLayout.getChildAt(1).setOnClickListener {

            showArc = false
            selectTabItem(1)
            hideMenu()
//            binding.fab.setImageResource(R.drawable.v3_ic_vitamin_deficiencies)
        }
        binding.arcLayout.getChildAt(2).setOnClickListener {

            showArc = false
            selectTabItem(2)
            hideMenu()
//            binding.fab.setImageResource(R.drawable.v3_ic_physical_health_risk)
        }
        binding.arcLayout.getChildAt(3).setOnClickListener {

            showArc = false
            selectTabItem(3)
            hideMenu()
//            binding.fab.setImageResource(R.drawable.v3_ic_physical_health_gifts)
        }
        binding.arcLayout.getChildAt(4).setOnClickListener {

            showArc = false
            selectTabItem(4)
            hideMenu()
//            binding.fab.setImageResource(R.drawable.v3_ic_health_warnings)
        }
        binding.arcLayout.getChildAt(5).setOnClickListener {

            showArc = false
            selectTabItem(5)
            hideMenu()
//            binding.fab.setImageResource(R.drawable.v3_ic_sleep_issues)
        }

        binding.btnInfoPane.setOnClickListener {
            showInfoDialog()
        }

        selectTabItem(firstSelectPos)
        initDialog()
        return view
    }
    private fun showInfoDialog() {
        if (dialog != null) {
            val tvTitle = dialog.findViewById<TextView>(R.id.tvTitle)
            val tvSubTitle = dialog.findViewById<TextView>(R.id.tvSubTitle)
            val tvDescription = dialog.findViewById<TextView>(R.id.tvDescription)
            var title:String=getString(tab_label[binding.tabLayout.selectedTabPosition]).replace("\n"," ")
            var subTitle:String=getString(tab_label[binding.tabLayout.selectedTabPosition]).replace("\n"," ")
            var descriptionPart:String=" "+getString(tab_label[binding.tabLayout.selectedTabPosition]).replace("\n"," ")+" "

            tvTitle.setText(title)
            tvSubTitle.setText(subTitle)
            tvDescription.setText(
                context?.getText(R.string.info_pane_overview_description_part1).toString() + descriptionPart + context?.getText(
                    R.string.info_pane_overview_description_part2
                ).toString()
            )
            dialog.show()
        }
    }

    private fun initDialog() {
        dialog = Dialog(activity)
        val viewDialog =
            View.inflate(activity, R.layout.v3_dialog_info_genetic_overview_result, null)
        val closeButton = viewDialog.findViewById<ImageView>(R.id.img_close)
        closeButton.setOnClickListener { dialog.dismiss() }
        dialog.setContentView(viewDialog)
        val tvTitle = viewDialog.findViewById<TextView>(R.id.tvTitle)
        val tvSubTitle = viewDialog.findViewById<TextView>(R.id.tvSubTitle)
        val tvDescription = viewDialog.findViewById<TextView>(R.id.tvDescription)
        tvSubTitle.visibility = View.GONE
        dialog.setCancelable(false)
    }


    @SuppressLint("WrongConstant")
    private fun replaceFragment(fragment: Fragment) {
        val transaction = activity!!.supportFragmentManager.beginTransaction()
        transaction.replace(R.id.flContent, fragment)
        transaction.addToBackStack("share")
        transaction.commit()

    }

    private fun selectTabItem(position: Int) {
        binding.tabLayout.getTabAt(position)!!.select();
    }

    @RequiresApi(Build.VERSION_CODES.HONEYCOMB)
    private fun createShowItemAnimator(item: View): Animator {

        val dx = binding.fab.getX() - item.x
        val dy = binding.fab.getY() - item.y

        item.rotation = 0f
        item.translationX = dx
        item.translationY = dy

        return ObjectAnimator.ofPropertyValuesHolder(
            item,
            AnimatorUtils.rotation(0f, 720f),
            AnimatorUtils.translationX(dx, 0f),
            AnimatorUtils.translationY(dy, 0f)
        )
    }

    @RequiresApi(Build.VERSION_CODES.HONEYCOMB)
    private fun createHideItemAnimator(item: View): Animator {
        val dx = binding.fab.x - item.x
        val dy = binding.fab.y - item.y

        val anim = ObjectAnimator.ofPropertyValuesHolder(
            item,
            AnimatorUtils.rotation(720f, 0f),
            AnimatorUtils.translationX(0f, dx),
            AnimatorUtils.translationY(0f, dy)
        )

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            anim.addListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator) {
                    super.onAnimationEnd(animation)
                    item.translationX = 0f
                    item.translationY = 0f
                }
            })
        }

        return anim
    }

    @RequiresApi(Build.VERSION_CODES.HONEYCOMB)
    private fun showMenu() {
        binding.menuLayout.setVisibility(View.VISIBLE)

        binding.shadowArc.visibility = View.VISIBLE
        binding.shadowTab.visibility = View.VISIBLE
        binding.shadowIndicator.visibility = View.VISIBLE

        val animList = java.util.ArrayList<Animator>()

        var i = 0
        val len = binding.arcLayout.getChildCount()
        while (i < len) {
            animList.add(createShowItemAnimator(binding.arcLayout.getChildAt(i)))
            i++
        }

        val animSet = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            AnimatorSet()
        } else {
            TODO("VERSION.SDK_INT < HONEYCOMB")
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            animSet.duration = 400
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            animSet.interpolator = OvershootInterpolator()
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            animSet.playTogether(animList)
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            animSet.start()
        }
    }

    @RequiresApi(Build.VERSION_CODES.HONEYCOMB)
    private fun hideMenu() {

        val animList = java.util.ArrayList<Animator>()

        for (i in binding.arcLayout.getChildCount() - 1 downTo 0) {
            animList.add(createHideItemAnimator(binding.arcLayout.getChildAt(i)))
        }

        val animSet = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            AnimatorSet()
        } else {
            TODO("VERSION.SDK_INT < HONEYCOMB")
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            animSet.duration = 400
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.DONUT) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                animSet.interpolator = AnticipateInterpolator()
            }
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            animSet.playTogether(animList)
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            animSet.addListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator) {
                    super.onAnimationEnd(animation)
                    binding.menuLayout.setVisibility(View.INVISIBLE)
                }
            })
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            animSet.start()
        }
        binding.shadowArc.visibility = View.GONE
        binding.shadowTab.visibility = View.GONE
        binding.shadowIndicator.visibility = View.GONE
    }

    @SuppressLint("ResourceAsColor", "InflateParams")
    private fun setupTabIcons() {

        val fragmentAdapter = ViewPagerAdapter(activity!!.supportFragmentManager)
        binding.viewPager.adapter = fragmentAdapter

        binding.indicator.setViewPager(binding.viewPager)

        val density = resources.displayMetrics.density
//        binding.indicator.setRadius(5 * density)
        binding.tabLayout.setupWithViewPager(binding.viewPager)
        val adapter = ViewPagerAdapter(activity!!.supportFragmentManager)
        binding.viewPager.offscreenPageLimit = 0
        binding.viewPager.adapter = adapter
        for (i in 0 until tab_label.size) {
            val fragment = GeneticOverviewResultItemFragment()
            fragment.setOverViewType(com.muhdo.app.utils.v3.Constants.GENETIC_OVERVIEW_TYPES[i])
            fragment.setOnLoadListener(object : OnOverviewLoadListener {
                override fun onLoadingCompleted(isLoaded: Boolean) {
                    binding.viewPager.offscreenPageLimit = 6
                }
            })
            adapter.addFrag(fragment, getString(tab_label[i]))
        }
        binding.viewPager.adapter = adapter

        for (i in 0 until tab_label.size) {
            val tabView = LayoutInflater.from(activity!!.applicationContext)
                .inflate(R.layout.custom_tab, null)
            tabView.minimumWidth = ((TabUtils.getScreenWidth(activity) / 3.0f).toInt())
            val tabLabel = tabView.findViewById(R.id.tab) as TextView
            val tabImage = tabView.findViewById(R.id.tab_img) as ImageView
            tabImage.setColorFilter(
                ContextCompat.getColor(
                    activity!!.applicationContext,
                    R.color.v3_tint_tab_unselected_color
                ), android.graphics.PorterDuff.Mode.SRC_IN
            );
            tabLabel.text = getString(tab_label[i])
            tabImage.setImageResource(tab_icons[i])
            tabLabel.setTextColor(context!!.resources.getColor(R.color.v3_tint_tab_unselected_color))
            binding.tabLayout.getTabAt(i)!!.customView = tabView
        }
        val tabLabel =
            binding.tabLayout.getTabAt(0)!!.customView!!.findViewById(R.id.tab) as TextView
        val tabImage =
            binding.tabLayout.getTabAt(0)!!.customView!!.findViewById(R.id.tab_img) as ImageView
        tabImage.setColorFilter(
            ContextCompat.getColor(
                activity!!.applicationContext,
                R.color.v3_tint_tab_selected_color
            ), android.graphics.PorterDuff.Mode.SRC_IN
        )
        tabLabel.setTextColor(context!!.resources.getColor(R.color.v3_tint_tab_selected_color))
        binding.viewPager.addOnPageChangeListener((object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {

            }

            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {

            }

            override fun onPageSelected(position: Int) {
                for (i in 0 until tab_label.size) {
                    val tabLabel =
                        binding.tabLayout.getTabAt(i)!!.customView!!.findViewById(R.id.tab) as TextView
                    val tabImage =
                        binding.tabLayout.getTabAt(i)!!.customView!!.findViewById(R.id.tab_img) as ImageView
                    tabImage.setColorFilter(
                        ContextCompat.getColor(
                            activity!!.applicationContext,
                            R.color.v3_tint_tab_unselected_color
                        ), android.graphics.PorterDuff.Mode.SRC_IN
                    )
                    tabLabel.setTextColor(context!!.resources.getColor(R.color.v3_tint_tab_unselected_color))
                }

                val tabLabel =
                    binding.tabLayout.getTabAt(position)!!.customView!!.findViewById(R.id.tab) as TextView
                val tabImage =
                    binding.tabLayout.getTabAt(position)!!.customView!!.findViewById(R.id.tab_img) as ImageView
                tabImage.setColorFilter(
                    ContextCompat.getColor(
                        activity!!.applicationContext,
                        R.color.v3_tint_tab_selected_color
                    ), android.graphics.PorterDuff.Mode.SRC_IN
                )
                tabLabel.setTextColor(context!!.resources.getColor(R.color.v3_tint_tab_selected_color))
            }


        }))
    }

    internal inner class ViewPagerAdapter(manager: FragmentManager) :
        FragmentStatePagerAdapter(manager) {
        private val mFragmentList = arrayListOf<Fragment>()
        private val mFragmentTitleList = arrayListOf<String>()

        override fun getItem(position: Int): Fragment {

            /* when (position) {
                 0 -> {

                     return GeneticOverviewResultItemFragment()
                 }
                 else -> {

                     return GeneticOverviewResultItemFragment()
                 }
             }*/
            return mFragmentList.get(position)
        }

        override fun getCount(): Int {
            return mFragmentList.size
        }

        fun addFrag(fragment: Fragment, title: String) {
            mFragmentList.add(fragment)
            mFragmentTitleList.add(title)
        }

        override fun getPageTitle(position: Int): CharSequence? {
            TempUtil.log("data", "tab pos = " + position)
            return mFragmentTitleList[position]
        }
    }

}