package com.muhdo.app.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.muhdo.app.R
import com.muhdo.app.adapter.IngredientAdapter
import com.muhdo.app.databinding.FragmentIngredientsBinding
import org.json.JSONArray

class IngredientsFragment : Fragment() {
    internal lateinit var binding: FragmentIngredientsBinding
   lateinit var adapter: IngredientAdapter
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_ingredients, container, false)
        binding = FragmentIngredientsBinding.bind(view)
        val data = this.arguments!!.getString("recipeInfo")
        val jsonArray = JSONArray(data)
        setData(jsonArray)
        return view
    }

    private fun setData(data: JSONArray){
        adapter = IngredientAdapter(activity!!.applicationContext, data)
        binding.ingredientsList.layoutManager =
                LinearLayoutManager(activity!!.applicationContext)
        binding.ingredientsList.isNestedScrollingEnabled = true
        binding.ingredientsList.itemAnimator = DefaultItemAnimator()
        binding.ingredientsList.adapter = adapter
    }
}