package com.muhdo.app.fragment.v3.dna


import android.os.Build
import android.os.Bundle
import android.text.Html
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.muhdo.app.R
import com.muhdo.app.adapter.v3.HealthInsightSpinnerAdapter
import com.muhdo.app.apiModel.keyData.GenericProfileData
import com.muhdo.app.apiModel.keyData.GenericProfileModel
import com.muhdo.app.apiModel.keyData.SectionByCategoryData
import com.muhdo.app.apiModel.keyData.SectionByCategoryModel
import com.muhdo.app.callback.KeyDataClickListener
import com.muhdo.app.repository.ApiUtilis
import com.muhdo.app.repository.ErrorModel
import com.muhdo.app.repository.NetworkManager
import com.muhdo.app.repository.ServiceListener
import com.timingsystemkotlin.backuptimingsystem.Utility
import kotlinx.android.synthetic.main.custom_tab.*
import kotlinx.android.synthetic.main.fragment_dna_result_text.*
import org.json.JSONObject

class DnaResultTextFragment(private val keyDataClickListener: KeyDataClickListener) : Fragment() {
    private var apiInProgress: Boolean = false
    private lateinit var completeResponse: SectionByCategoryData
    lateinit var spinnerAdapter: HealthInsightSpinnerAdapter
    lateinit var list: List<GenericProfileData>
    var selectedPostion = 0
    var categoryID = ""
    var sectionID = ""
    var modeID = ""
    var data1: SectionByCategoryData? = null;
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_dna_result_text, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        var spinnerItem = ArrayList<String>(0)
        list = ArrayList(0)
        for (value in list) {
            spinnerItem.add(value.getTitle()!!)
        }
        spinnerAdapter = HealthInsightSpinnerAdapter(activity!!.applicationContext!!, spinnerItem)

        spinner.adapter =
            spinnerAdapter//ArrayAdapter<String>(activity,R.layout.v3_item_dropdown_health_insight,spinnerItem)

        spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {}

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, pos: Int, p3: Long) {
                categoryID = list[pos].getId()!!
                getKeyData(
                    categoryID,
                    sectionID,
                    Utility.modeID_V3!!
                )
            }
        }
        getGenericProfile(sectionID)
    }

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser);

        if (isVisibleToUser && isResumed()) { // fragment is visible and have created
            getGenericProfile(sectionID)
            if (data1 != null) {
                data1!!.getActiveIndicator()?.let {
                    keyDataClickListener.dataChangeFromSpinner(
                        data1!!.getTitle(),
                        it.getTitle(),
                        it.getDegree()
                    )
                }
            }
        }
    }

    private fun getKeyData(categoryId: String, genoTypeId: String, modeID: String) {
        progress_bar.visibility = View.VISIBLE
        apiInProgress = true
        val manager = NetworkManager()
        if (manager.isConnectingToInternet(requireContext())) {

            // System.out.println("genoTypeId****--> $genoTypeId  modeID******--> $modeID categoryId==>   $categoryId")
            //val userId = Base64.decode(Utility.getUserID(requireContext()), Base64.DEFAULT)
            //val decodeUserId = String(userId, charset("UTF-8"))

            manager.createApiRequest(
                ApiUtilis.getAPIInstance(activity!!).getKeyDataByCategory(
                    Utility.getUserID(requireContext()),
                    modeID,
                    "false",
                    "true",
                    categoryId,
                    genoTypeId
                ),
                object : ServiceListener<SectionByCategoryModel> {
                    override fun getServerResponse(
                        response: SectionByCategoryModel,
                        requestcode: Int
                    ) {
                        if (progress_bar != null)
                            progress_bar.visibility = View.GONE
                        //dismissProgressDialog()
                        if (response.getCode() == 200 && response.getMessage() == "Sections for user found") {

                            try {
                                setData(response.getData()!!)
                            } catch (e: Exception) {
                                apiInProgress = false
                                e.printStackTrace()
                            }
                        }

                    }

                    override fun getError(error: ErrorModel, requestcode: Int) {
                        //dismissProgressDialog()
                        apiInProgress = false
                        progress_bar.visibility = View.GONE

                    }
                })
        } else {
            progress_bar.visibility = View.GONE
            //    dismissProgressDialog()
            Utility.displayShortSnackBar(
                parent_layout,
                resources.getString(R.string.check_internet)
            )

        }
    }

    public fun setSectionCategoryMode(sectionID: String, categoryId: String, modeID: String) {
        this.sectionID = sectionID
        this.categoryID = categoryId
        this.modeID = modeID
    }

    private fun setData(data: SectionByCategoryData) {
        this.data1 = data
        val degree = 10 * data.getActiveIndicator()!!.getDegree()!!.toInt() / 18
        // Log.d("data","degree--- "+data.getActiveIndicator()!!.getDegree()+" "+degree)
        if (data.getDescription() != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                tv_description.visibility = View.VISIBLE
                tv_description.text =
                    Html.fromHtml(data.getDescription(), Html.FROM_HTML_MODE_COMPACT)
            } else {
                tv_description.visibility = View.VISIBLE
                tv_description.text = Html.fromHtml(data.getDescription())
            }
        } else {
            tv_description.visibility = View.GONE
        }

        if (data.getIndicatorDescription() != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                tv_description.visibility = View.VISIBLE
                tv_Message.text = Html.fromHtml(
                    data.getIndicatorDescription(),
                    Html.FROM_HTML_MODE_COMPACT
                )
            } else {
                tv_description.visibility = View.VISIBLE
                tv_Message.text = Html.fromHtml(data.getIndicatorDescription())
            }
        } else {
            tv_Message.visibility = View.GONE
        }
        if (data.getInterests() != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                tv_gene_of_interest_content.visibility = View.VISIBLE
                tv_gene_of_interest_content.text = Html.fromHtml(
                    data.getInterests(),
                    Html.FROM_HTML_MODE_COMPACT
                );
            } else {
                tv_gene_of_interest_content.visibility = View.VISIBLE
                tv_gene_of_interest_content.text =
                    Html.fromHtml(data.getDescription())
            }
        } else {
            tv_gene_of_interest_content.visibility = View.GONE
        }
        if (data.getMessage() != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                tv_Message.visibility = View.VISIBLE
                tv_Message.text = Html.fromHtml(data.getMessage(), Html.FROM_HTML_MODE_COMPACT)
            } else {
                tv_Message.visibility = View.VISIBLE
                tv_Message.text = Html.fromHtml(data.getMessage())
            }
        } else {
            tv_Message.visibility = View.GONE
        }
        if (data.getTitle() != null) {
            tv_sub_title.text = data.getTitle()
        } else {
            tv_sub_title.visibility = View.GONE
        }
        Log.e(
            "Update Header:",
            "status: " + data.getActiveIndicator()!!.getTitle() + " degree: " + data.getActiveIndicator()!!.getDegree() + " name: " + data.getTitle()
        )
        this.completeResponse = data
        apiInProgress = false
        if (isVisible && userVisibleHint) {
            data.getActiveIndicator()?.let {
                keyDataClickListener.dataChangeFromSpinner(
                    data.getTitle(),
                    it.getTitle(),
                    it.getDegree()
                )
            }
        }
    }

    private fun getGenericProfile(genoTypeId: String) {

        val manager = NetworkManager()
        if (manager.isConnectingToInternet(requireContext())) {
            manager.createApiRequest(
                ApiUtilis.getAPIInstance(activity!!).getGenericProfile(
                    genoTypeId
                ),
                object : ServiceListener<GenericProfileModel> {
                    override fun getServerResponse(
                        response: GenericProfileModel,
                        requestcode: Int
                    ) {

                        try {
                            progress_bar.visibility = View.GONE
                            Log.d("data", "getGenericProfile()call " + genoTypeId)
                            setSpinnerData(response.getData()!!)
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }

                    override fun getError(error: ErrorModel, requestcode: Int) {

                        progress_bar.visibility = View.GONE
                        val json = JSONObject(error.getMessage()!!)
                        if (json.has("message") && activity != null && activity!!.applicationContext != null) {
                            Toast.makeText(
                                activity!!.applicationContext,
                                json.getString("message"),
                                Toast.LENGTH_LONG
                            )
                                .show()
                        }
                    }
                })
        } else {
            progress_bar.visibility = View.GONE
            Utility.displayShortSnackBar(
                parent_layout,
                resources.getString(R.string.check_internet)
            )

        }
    }

    private fun setSpinnerData(list: List<GenericProfileData>) {
        this.list = list
        var spinnerItem = ArrayList<String>(0)
        for ((i, value) in list.withIndex()) {
            spinnerItem.add(value.getTitle()!!)
            if (value.getId().equals(Utility.selectedKeydataCategoryID_V3)) {
                selectedPostion = i
            }
        }
        spinnerAdapter.setListItems(spinnerItem)
        spinner.setSelection(selectedPostion)
    }

    fun updateHeaderFields(position: Int) {
        //  if (isVisible && userVisibleHint) {
        if (::completeResponse.isInitialized && sectionID == Utility.sectionList[position - 1])
            completeResponse.getActiveIndicator()?.let {
                keyDataClickListener.dataChangeFromSpinner(
                    completeResponse.getTitle(),
                    it.getTitle(),
                    it.getDegree()
                )
                //     }
            }
    }


}
