package com.muhdo.app.fragment.v3.guide.workout

import android.content.DialogInterface
import android.os.Bundle
import android.util.Base64
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.muhdo.app.R
import com.muhdo.app.databinding.V3FragmentNoWorkoutPlanBinding
import com.muhdo.app.fragment.v3.QuestionnaireHomeFragment
import com.muhdo.app.fragment.v3.WorkoutFragment
import com.muhdo.app.repository.ApiUtilis
import com.muhdo.app.ui.DashboardActivity
import com.muhdo.app.utils.v3.TempUtil
import com.timingsystemkotlin.backuptimingsystem.Utility
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class NoWorkoutPlanFoundFragment : Fragment() {
    lateinit var binding: V3FragmentNoWorkoutPlanBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view =
            inflater.inflate(R.layout.v3_fragment_no_workout_plan, container, false)
        binding = V3FragmentNoWorkoutPlanBinding.bind(view)
        binding.tvCreateWorkoutPlan.setOnClickListener{
            fetchQuestionnaireStatusAndGoToWorkoutPlan()
        }
        return view
    }

    private fun replaceFragment(fragment: Fragment) {
        val transaction = activity?.supportFragmentManager?.beginTransaction()
        transaction?.replace(R.id.flContent, fragment)
        transaction?.addToBackStack("share")
        transaction?.commit()
    }

    private fun fetchQuestionnaireStatusAndGoToWorkoutPlan() {

        binding.progressBar.visibility = View.VISIBLE
        val userId = Base64.decode(Utility.getUserID(activity!!.themedContext), Base64.DEFAULT)
        val decodeUserId = String(userId, charset("UTF-8"))
        ApiUtilis.getAPIInstance(activity!!).getQuestionnaireStatus(userID = decodeUserId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ success ->
                run {
                    binding.progressBar.visibility = View.GONE
                    var isQuestionnaireCompleted: Boolean = false;

                    if (success.data?.get(0)?.status!! &&
                        success.data?.get(1)?.status!! &&
                        success.data?.get(2)?.status!! &&
                        success.data?.get(3)?.status!!
                    ) {
                        isQuestionnaireCompleted = true
                    }
                    if (isQuestionnaireCompleted) {

                        if (Utility.getWorkoutStatus(activity!!.applicationContext)) {
                            replaceFragment(WorkoutFragment())
                        } else {
                            replaceFragment(ChooseGoalFragment())
                        }
                    } else {
                        var dialog: androidx.appcompat.app.AlertDialog.Builder =
                            androidx.appcompat.app.AlertDialog.Builder(activity!!.themedContext)
                        dialog.setMessage(R.string.fill_questionnaire_warning_before_create_workout_plan)
                        dialog.setPositiveButton(
                            R.string.btn_go_to_questionnaire_warning,
                            DialogInterface.OnClickListener { dialog, id ->
                                dialog.cancel()
                                TempUtil.currentFragment =
                                    1000// binding.navigationView.selectedItemId
                                replaceFragment(QuestionnaireHomeFragment())
                                (activity as DashboardActivity).removeBottomNavColor()
                                dialog.cancel()
                            })
                        dialog.setNegativeButton(
                            R.string.btn_cancel,
                            DialogInterface.OnClickListener { dialog, id ->
                                dialog.cancel()
                            })
                        dialog.show()

                    }
                }
            }, { error ->
                run {
                    binding.progressBar.visibility = View.INVISIBLE
                }
            })
    }

}
