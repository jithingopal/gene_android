package com.muhdo.app.fragment.v3


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.google.gson.JsonObject
import com.muhdo.app.R
import com.muhdo.app.adapter.v3.InjuryPreventionVideoAdapter
import com.muhdo.app.apiModel.v3.workoutplan.ResponseInjuryPrevention
import com.muhdo.app.databinding.FragmentInjuryPreventionBinding
import com.muhdo.app.repository.ApiUtilis
import com.timingsystemkotlin.backuptimingsystem.Utility
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_injury_prevention.*

class InjuryPreventionFragment : Fragment() {

    internal lateinit var binding: FragmentInjuryPreventionBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_injury_prevention, container, false)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        fetchInjuryPreventionWorkouts()
    }

    private fun fetchInjuryPreventionWorkouts() {
        progress_bar.visibility = View.VISIBLE
        val requestBody = JsonObject()
        requestBody.addProperty("type", "INJURY")
        ApiUtilis.getAPIInstance(activity!!).getWorkoutInjuryPrevention(requestBody)
            .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
            .subscribe({ success -> parseAndLoadVideoList(success) }, { error -> showError(error) })
    }

    private fun parseAndLoadVideoList(it: ResponseInjuryPrevention) {
        context?.let { context ->

            if(progress_bar!=null)
            {
                progress_bar.visibility = View.GONE
            }

            try{val adapter = InjuryPreventionVideoAdapter(context, it.data)
            elv_injury_prevention.setAdapter(adapter)}catch (e:Exception){e.printStackTrace()}
        }
    }

    private fun showError(error: Throwable?) {
        if(progress_bar!=null)
       progress_bar.visibility = View.GONE
        error?.localizedMessage?.let { Utility.displayShortSnackBar(elv_injury_prevention, it) }
    }

}
