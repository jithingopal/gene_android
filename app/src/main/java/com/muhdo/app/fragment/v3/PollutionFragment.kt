package com.muhdo.app.fragment.v3


import android.animation.Animator
import android.app.Dialog
import android.graphics.*
import android.graphics.drawable.BitmapDrawable
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import com.muhdo.app.R
import kotlinx.android.synthetic.main.fragment_pollution.*
import org.jetbrains.anko.toast


class PollutionFragment : Fragment() {
    lateinit var dialog: Dialog
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setupDialogInfo()
        return inflater.inflate(R.layout.fragment_pollution, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        iv_info.setOnClickListener {
            dialog.show()
        }
        ll_stress_pressure_results.setOnClickListener {
            if (tv_aqi_suggestions.text.isNotEmpty()) {
                iv_stress_results.setImageBitmap(
                    rotateBitmap(
                        (iv_stress_results.drawable as BitmapDrawable).bitmap,
                        180
                    )
                )
                tv_aqi_suggestions.toggleVisibility()
            } else
                context?.toast(getString(R.string.no_data_available))
        }
        ll_stress_pressure_recom.setOnClickListener {
            if (tv_aqi_recommendations.text.isNotEmpty()) {
                sv_stress_pressure.post {
                    sv_stress_pressure.smoothScrollBy(250, 0)
                }

                iv_stress_recom.setImageBitmap(
                    rotateBitmap(
                        (iv_stress_recom.drawable as BitmapDrawable).bitmap,
                        180
                    )
                )
                tv_aqi_recommendations.toggleVisibility()
            } else
                context?.toast(getString(R.string.no_data_available))
        }
    }

    private fun rotateBitmap(bitmap: Bitmap, rotationAngleDegree: Int): Bitmap {

        val w = bitmap.width
        val h = bitmap.height

        var newW = w
        var newH = h
        if (rotationAngleDegree == 90 || rotationAngleDegree == 270) {
            newW = h
            newH = w
        }
        val rotatedBitmap = Bitmap.createBitmap(newW, newH, bitmap.config)
        val canvas = Canvas(rotatedBitmap)

        val rect = Rect(0, 0, newW, newH)
        val matrix = Matrix()
        val px = rect.exactCenterX()
        val py = rect.exactCenterY()
        matrix.postTranslate((-bitmap.width / 2).toFloat(), (-bitmap.height / 2).toFloat())
        matrix.postRotate(rotationAngleDegree.toFloat())
        matrix.postTranslate(px, py)
        canvas.drawBitmap(
            bitmap,
            matrix,
            Paint(Paint.ANTI_ALIAS_FLAG or Paint.DITHER_FLAG or Paint.FILTER_BITMAP_FLAG)
        )
        matrix.reset()

        return rotatedBitmap
    }


    private fun View.toggleVisibility() {
        val view = this
        when (this.visibility) {
            View.VISIBLE -> {
                view.animate()
                    .translationY(0f)
                    .alpha(0.0f)
                    .setDuration(500)
                    .setListener(object : Animator.AnimatorListener {
                        override fun onAnimationRepeat(animation: Animator?) {

                        }

                        override fun onAnimationCancel(animation: Animator?) {
                        }

                        override fun onAnimationStart(animation: Animator?) {
                        }

                        override fun onAnimationEnd(animation: Animator?) {
                            // view.clearAnimation()
                            view.visibility = View.GONE
                        }
                    })
            }
            View.GONE -> {
                view.animate()
                    .translationY(5f)
                    .alpha(1.0f)
                    .setDuration(500)
                    .setListener(object : Animator.AnimatorListener {
                        override fun onAnimationRepeat(animation: Animator?) {

                        }

                        override fun onAnimationCancel(animation: Animator?) {
                        }

                        override fun onAnimationStart(animation: Animator?) {
                        }

                        override fun onAnimationEnd(animation: Animator?) {
                            //  view.clearAnimation()
                            if (!tv_aqi_recommendations.isVisible || tv_aqi_suggestions.isVisible)
                                sv_stress_pressure.post {
                                    sv_stress_pressure.smoothScrollTo(
                                        0,
                                        (view.y + 50).toInt()
                                    )
                                }
                            view.visibility = View.VISIBLE
                        }
                    })
            }

        }
    }

    override fun onResume() {
        super.onResume()
        val arguments = arguments
        progress_bar.visibility = View.VISIBLE
        Handler().postDelayed(Runnable {
            arguments?.let {
              try{  progress_bar.visibility = View.GONE
                tv_daily_message.text = it.getString("daily_message")
                tv_tv_health_implications_content.text = it.getString("health_implications")
                tv_aqi_suggestions.text = it.getString("suggestions")
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    tv_aqi_recommendations.text = Html.fromHtml(
                        it.getString("recommendations"),
                        Html.FROM_HTML_MODE_COMPACT
                    )
                } else {
                    tv_aqi_recommendations.text =
                        Html.fromHtml(it.getString("recommendations"))
                }
                dcp_air_pollution.setProgress(
                    it.getInt("airIndexValue"),
                    it.getFloat("percentage"),
                    it.getString("category")!!
                )
            }catch (e:Exception){
                  e.printStackTrace()
              }
            }
        }, 2000)
    }

    private fun setupDialogInfo() {
        dialog = Dialog(activity)
        val viewDialog = View.inflate(activity, R.layout.v3_dialog_info_questionnaire, null)
        val closeButton = viewDialog.findViewById<ImageView>(R.id.img_close)
        closeButton.setOnClickListener { dialog.dismiss() }
        dialog.setContentView(viewDialog)
        val tvTitle = viewDialog.findViewById<TextView>(R.id.tvTitle)
        val tvSubTitle = viewDialog.findViewById<TextView>(R.id.tvSubTitle)
        val tvDescription = viewDialog.findViewById<TextView>(R.id.tvDescription)
        tvTitle.setText(R.string.pollution_title)
        //  tvSubTitle.setText(R.string.info_pane_text_diet_sub_title)
        tvDescription.setText(R.string.polluton_info_description)
        tvSubTitle.visibility = View.VISIBLE
        dialog.setCancelable(false)
    }
}
