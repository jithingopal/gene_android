package com.muhdo.app.fragment.v3.dna

import android.animation.Animator
import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import androidx.viewpager.widget.ViewPager
import com.google.android.material.tabs.TabLayout
import com.muhdo.app.R
import com.muhdo.app.apiModel.keyData.Indicator
import com.muhdo.app.callback.KeyDataClickListener
import com.muhdo.app.interfaces.v3.OnSwipeTouchListener
import com.muhdo.app.utils.v3.Constants.SWIPE_RIGHT_TO_LEFT
import com.muhdo.app.utils.v3.TempUtil
import com.timingsystemkotlin.backuptimingsystem.Utility
import kotlinx.android.synthetic.main.fragment_dna_result.*
import kotlinx.android.synthetic.main.v3_fragment_health_insight_result.tabLayout
import kotlinx.android.synthetic.main.v3_fragment_health_insight_result.view_pager

class DnaResultFragment : Fragment(), KeyDataClickListener {
    override fun dataChangeFromSpinner(title: String?, status: String?, percent: Int?) {
        tvInfoText.text = title
        tvRiskStatus.text = status
        percent?.let { setHeaderProgress(it) }
    }

    override fun onHeaderItemChange(indicator: Indicator) {
        TempUtil.log(
            "TempName",
            "Category ${indicator.getCategoryId()} Sectionid ${indicator.getSectionId()} modeid ${indicator.getModeId()}"
        )
        Utility.selectedKeydataSectionID_V3 = indicator.getSectionId()
        Utility.selectedKeydataCategoryID_V3 = indicator.getCategoryId()
        Utility.modeID_V3 = indicator.getModeId()
        for (key in Utility.sectionList.keys) {
            if (Utility.sectionList[key] == indicator.getSectionId()) {
                view_pager.setCurrentItem(key + 1, true)
                if (indicator.getCategoryId() != null) {
                    Utility.categoryID = indicator.getCategoryId()!!
                }
            }
        }
        tvInfoText.text = indicator.getTitle()
        tvRiskStatus.text = indicator.getResult()
        indicator.getPercent()?.let { setHeaderProgress(it) }
    }

    private fun setHeaderProgress(degreeValue: Int) {
        imgIndicator.visibility = View.VISIBLE

        if (degreeValue in 109..144) {
            imgIndicator.setImageResource(R.drawable.v3_ic_slider_half_red)
        } else if (degreeValue in 73..108) {
            imgIndicator.setImageResource(R.drawable.v3_ic_slider_yellow)
        } else if (degreeValue in 37..72) {
            imgIndicator.setImageResource(R.drawable.v3_ic_slider_half_green)
        } else if (degreeValue in 0..36) {
            imgIndicator.setImageResource(R.drawable.v3_ic_slider_green)
        } else {
            imgIndicator.setImageResource(R.drawable.v3_ic_slider_red)
        }
    }

    override fun OnItemClickListener(categoryId: String?, sectionId: String?, modeId: String?) {
        TempUtil.log(
            "TempName",
            "Category $categoryId Sectionid $sectionId modeid $modeId"
        )
    }

    private lateinit var dnaResultTextFragment: DnaResultTextFragment
    private lateinit var dnaResultListFragment: DnaResultListFragment
    override fun onAttach(context: Context) {
        super.onAttach(context)
        //   tabSelectedListener = context as TabSelectedListener
    }

    private val swipeTouchListener = object : OnSwipeTouchListener(activity) {
        override fun onSwipeLeft() {
            super.onSwipeLeft()
            if (tab_dna_header.selectedTabPosition < 3)
                tab_dna_header.getTabAt(tab_dna_header.selectedTabPosition + 1)?.select()
        }

        override fun onSwipeRight() {
            super.onSwipeRight()
            if (tab_dna_header.selectedTabPosition > 0)
                tab_dna_header.getTabAt(tab_dna_header.selectedTabPosition - 1)?.select()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_dna_result, container, false)
        createSectionsList()
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dnaResultListFragment = DnaResultListFragment(this)
        setupTabIcons()
        addHeaderTabs()
        tab_dna_header.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(p0: TabLayout.Tab?) {

            }

            override fun onTabUnselected(p0: TabLayout.Tab?) {
            }

            override fun onTabSelected(tab: TabLayout.Tab?) {
                //find the visible fragment then populate the result accordingly
                if (tab != null) {
                    setHeaderTextCallApi(position = tab.position)
                }
                imgIndicator.visibility = View.INVISIBLE
                val selectedTabPos = tabLayout.selectedTabPosition
                setupTabIcons()
                tabLayout.getTabAt(selectedTabPos)!!.select()
                /* when (tabLayout.selectedTabPosition) {
                     0 -> {
                         tab?.position?.let { dnaResultListFragment.headerTabSwiped(it) }
                     }
                     else -> {
                         setupTabIcons()
                         tabLayout.getTabAt(selectedTabPos)!!.select()
                     }

                 }*/
            }

        })
        sv_dna_overview_parent.setOnTouchListener(swipeTouchListener)
        // layoutHeader.setOnTouchListener(swipeTouchListener)
    }

    fun setFilpAnimationToQuickView(swipe_to_from: Int) {
        var firstRotation = 0f
        var secondRotation = 90f
        var thirdRotation = 270f
        var fourthRotation = 360f
        if (swipe_to_from == SWIPE_RIGHT_TO_LEFT) {
            firstRotation = 360f
            // secondRotation = 270f;
            thirdRotation = 90f
            fourthRotation = 0f
        }
        layoutQuickView.animate().rotationY(360f)
            .setListener(object : Animator.AnimatorListener {
                override fun onAnimationRepeat(p0: Animator?) {

                }

                override fun onAnimationEnd(p0: Animator?) {
                }

                override fun onAnimationCancel(p0: Animator?) {
                }

                override fun onAnimationStart(p0: Animator?) {
                }

            })

    }

    private var tabIcons = arrayOf(
        R.drawable.ic_dna_keydata_circle,
        R.drawable.ic_dna_sport_circle,
        R.drawable.ic_dna_diet_circle,
        R.drawable.ic_dna_micronutrients_circle,
        R.drawable.ic_dna_health_circle/*,
        R.drawable.v3_ic_mental_health*/
    )

    private var tabLabel =
        arrayOf(
            R.string.results_key_data,
            R.string.dna_health,
            R.string.diet,
            R.string.dna_physical,
            R.string.dna_vitamins/*,
            R.string.dna_psychology*/
        )

    private fun setupTabIcons() {
        val fragmentAdapter = ViewPagerAdapter(activity!!.supportFragmentManager)
        view_pager.adapter = fragmentAdapter
        view_pager.offscreenPageLimit = 0
        tabLayout.setupWithViewPager(view_pager)
        val adapter = ViewPagerAdapter(activity!!.supportFragmentManager)
        for (i in 0 until tabLabel.size) {
            if (i == 0)
                adapter.addFrag(dnaResultListFragment, getString(tabLabel[i]))
            else {
                dnaResultTextFragment = DnaResultTextFragment(this)
                val fragment = dnaResultTextFragment
                fragment.setSectionCategoryMode(Utility.sectionList[i - 1]!!, "", "")
                adapter.addFrag(fragment, getString(tabLabel[i]))
            }
        }
        view_pager.adapter = adapter
        for (i in 0 until tabIcons.size) {
            val tabView = LayoutInflater.from(activity!!.applicationContext)
                .inflate(R.layout.v3_custom_tab_health_insights_details, null)
            val tabImg = tabView.findViewById(R.id.tab_img) as ImageView
            val tabText = tabView.findViewById(R.id.tabTitle) as TextView
            tabText.setTextColor(context!!.getColor(R.color.v3_epi_gen_icon_unselected))
            tabImg.setColorFilter(
                ContextCompat.getColor(
                    activity!!.applicationContext,
                    R.color.v3_epi_gen_icon_unselected
                ), android.graphics.PorterDuff.Mode.SRC_IN
            )
            tabImg.setImageResource(tabIcons[i])
            tabText.setText(tabLabel[i])
            tabLayout.getTabAt(i)!!.customView = tabView
        }
        val tabImage =
            tabLayout.getTabAt(0)!!.customView!!.findViewById(R.id.tab_img) as ImageView
        val tabText2 =
            tabLayout.getTabAt(0)!!.customView!!.findViewById(R.id.tabTitle) as TextView
        tabImage.setColorFilter(
            ContextCompat.getColor(
                activity!!.applicationContext,
                R.color.health_insight_colorPrimary
            ), android.graphics.PorterDuff.Mode.SRC_IN
        )
        tabText2.setTextColor(context!!.getColor(R.color.health_insight_colorPrimary))

        view_pager.addOnPageChangeListener((object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {
            }

            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {

            }

            override fun onPageSelected(position: Int) {
                //  tabSelectedListener.onTabSelected(position)
                //   dnaResultTextFragment.updateHeaderFields(position)
                Log.d("Page Selected", "page $position")
                if (position == 0) {
                    setHeaderTextCallApi(tab_dna_header.selectedTabPosition)
                }

                for (i in 0 until tabIcons.size) {
                    val tabImage =
                        tabLayout.getTabAt(i)!!.customView!!.findViewById(R.id.tab_img) as ImageView
                    val tabTextView =
                        tabLayout.getTabAt(i)!!.customView!!.findViewById(R.id.tabTitle) as TextView
                    tabImage.setColorFilter(
                        ContextCompat.getColor(
                            activity!!.applicationContext,
                            R.color.v3_epi_gen_icon_unselected
                        ), android.graphics.PorterDuff.Mode.SRC_IN
                    )
                    tabTextView.setTextColor(context!!.getColor(R.color.v3_epi_gen_icon_unselected))
//                    setTabSelection(position)
                }

                val tabImage =
                    tabLayout.getTabAt(position)!!.customView!!.findViewById(R.id.tab_img) as ImageView
                val tabText2 =
                    tabLayout.getTabAt(position)!!.customView!!.findViewById(R.id.tabTitle) as TextView
                tabImage.setColorFilter(
                    ContextCompat.getColor(
                        activity!!.applicationContext,
                        R.color.health_insight_colorPrimary
                    ), android.graphics.PorterDuff.Mode.SRC_IN
                )
                tabText2.setTextColor(context!!.getColor(R.color.health_insight_colorPrimary))

//                nestedScrollView.isSmoothScrollingEnabled = true
//                nestedScrollView.fling(0)
//                nestedScrollView.smoothScrollTo(0, 0)

            }

        }))


    }

    internal inner class ViewPagerAdapter(manager: FragmentManager) :
        FragmentStatePagerAdapter(manager) {
        private val mFragmentList = arrayListOf<Fragment>()
        private val mFragmentTitleList = arrayListOf<String>()

        override fun getItem(position: Int): Fragment {

            return mFragmentList.get(position)
        }

        override fun getCount(): Int {
            return mFragmentList.size
        }

        fun addFrag(fragment: Fragment, title: String) {
            mFragmentList.add(fragment)
            mFragmentTitleList.add(title)
        }

        override fun getPageTitle(position: Int): CharSequence? {
            Log.d("data", "tab pos = $position")
            return mFragmentTitleList[position]
        }
    }

    private fun addHeaderTabs() {
        val tabHeaderLabel = arrayOf(
            R.string.v3_health_and_wellbeing,
            R.string.v3_weight_loss,
            R.string.v3_fitness_and_endurance,
            R.string.v3_muscle_building
        )
        for (i in tabHeaderLabel.indices) {
            tab_dna_header.addTab(tab_dna_header.newTab().setText(tabHeaderLabel[i]), i)
        }
    }

    private fun createSectionsList() {
        Utility.sectionList[0] = "5c8c95189c54381add6884d8"
        Utility.sectionList[1] = "5c8c95189c54381add6884d6"
        Utility.sectionList[2] = "5c8c95189c54381add6884d5"
        Utility.sectionList[3] = "5c8c95189c54381add6884d7"
        Utility.sectionList[4] = "5c8c95189c54381add6884d9"
    }


    private fun setHeaderTextCallApi(position: Int) {
        tvRiskStatus.text = ""
        imgIndicator.visibility = View.INVISIBLE
        when (position) {
            0 -> {
                tvInfoText.text = getString(R.string.v3_health_and_wellbeing)
                imgInfoIcon.setImageResource(R.drawable.ic_dna_header_health_well)
                // "Health & Wellbeing"
                val modeId = "5c8c96a79c54381add6884dd"
                Utility.modeID_V3 = modeId
            }
            1 -> {
                tvInfoText.text = getString(R.string.v3_weight_loss)
                imgInfoIcon.setImageResource(R.drawable.ic_dna_header_weight_loss)
                // "Fat Loss"
                val modeId = "5c8c96a79c54381add6884dc"
                Utility.modeID_V3 = modeId
            }
            2 -> {
                tvInfoText.text = getString(R.string.v3_fitness_and_endurance)
                imgInfoIcon.setImageResource(R.drawable.ic_dna_header_fit_endur)
                //"Fitness & Endurance"
                val modeId = "5c8c96a79c54381add6884da"
                Utility.modeID_V3 = modeId
            }
            3 -> {
                tvInfoText.text = getString(R.string.v3_muscle_building)
                imgInfoIcon.setImageResource(R.drawable.ic_dna_header_muscle_build)
                // "Build Muscle"
                val modeId = "5c8c96a79c54381add6884db"
                Utility.modeID_V3 = modeId
            }

        }
    }


}