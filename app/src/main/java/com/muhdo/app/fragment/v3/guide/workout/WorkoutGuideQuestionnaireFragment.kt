package com.muhdo.app.fragment.v3.guide.workout

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.muhdo.app.R
import com.muhdo.app.apiModel.mealModel.daysModel.UpdateFavoriteModel
import com.muhdo.app.apiModel.v3.WorkoutTrainingRequest
import com.muhdo.app.databinding.V3FragmentWorkoutGuideQuestionnaireBinding
import com.muhdo.app.fragment.v3.WorkoutFragment
import com.muhdo.app.repository.ApiUtilis
import com.muhdo.app.repository.ErrorModel
import com.muhdo.app.repository.NetworkManager
import com.muhdo.app.repository.ServiceListener
import com.muhdo.app.ui.workout.WorkoutLoading
import com.muhdo.app.utils.PreferenceConnector
import com.muhdo.app.utils.v3.TempUtil
import com.muhdo.app.utils.v3.showAlertDialog
import com.timingsystemkotlin.backuptimingsystem.Utility

class WorkoutGuideQuestionnaireFragment : Fragment() {
    private var chooseSubGoalFragment: ChooseSubGoalFragment = ChooseSubGoalFragment()
    private var chooseExperianceFragment: ChooseExperianceFragment = ChooseExperianceFragment()
    private var chooseEquipmentFragment: ChooseEquipmentFragment = ChooseEquipmentFragment()
    private var chooseDateTimeFragment: ChooseDateTimeFragment = ChooseDateTimeFragment()
    private var isNeedToSkipSubGoal = false
    lateinit var binding: V3FragmentWorkoutGuideQuestionnaireBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    val TAG = "Jithin"

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view =
            inflater.inflate(R.layout.v3_fragment_workout_guide_questionnaire, container, false)
        binding = V3FragmentWorkoutGuideQuestionnaireBinding.bind(view)
        setupViewPager()
        setupButtons()
        return view
    }

    fun setupButtons() {
        binding.btnNext.setOnClickListener {
            when (binding.viewPager.currentItem) {
                0 -> {
                    if (chooseSubGoalFragment.getSelectedSubGoal() != null) {
                        binding.viewPager.setCurrentItem(1)
                    } else {
                        context?.showAlertDialog(getString(R.string.please_choose_sub_goal))
                    }

                }

                1 -> {
                    if (chooseDateTimeFragment.getSelectedDays() != null) {
                        binding.viewPager.setCurrentItem(2)
                    } else {
                        context?.showAlertDialog(getString(R.string.please_choose_date_and_time_for_workout))
                    }

                }

                2 -> {
                    if (chooseEquipmentFragment.getSelectedEquipment() != null) {
                        binding.viewPager.setCurrentItem(3)
                    } else {
                        context?.showAlertDialog(getString(R.string.please_choose_equipment1))
                    }


                }

                3 -> {

                    if (chooseExperianceFragment.getSelectedExperiance() != null) {

                        TempUtil.log("Workout", "Calling fun")

                        sendDataToServer()
                    } else {
                        context?.showAlertDialog(getString(R.string.please_choose_your_experiance_in_workout))
                    }


                }


            }

        }
        binding.btnPrevious.setOnClickListener {
            val chooseGoalFragment = ChooseGoalFragment()
            chooseGoalFragment.setGoToFragment(WorkoutGuideQuestionnaireFragment())
            if (isNeedToSkipSubGoal && binding.viewPager.currentItem == 1) {
                replaceFragment(chooseGoalFragment)
            } else if (binding.viewPager.currentItem > 0) {
                try {
                    binding.viewPager.setCurrentItem(binding.viewPager.currentItem - 1)
                } catch (e: java.lang.Exception) {
                    e.printStackTrace()
                }
            } else {
                replaceFragment(chooseGoalFragment)
            }
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)

    }

    fun setupViewPager() {

        val id: String? = PreferenceConnector.readString(
            context!!,
            PreferenceConnector.MODE_ID,
            "5c8c96a79c54381add6884da"
        )
        if (id.equals("5c8c96a79c54381add6884db")) {
            isNeedToSkipSubGoal = false
        } else {
            isNeedToSkipSubGoal = true
        }
        val adapter = ViewPagerAdapter(activity!!.supportFragmentManager)

        adapter.addFrag(chooseSubGoalFragment)
        adapter.addFrag(chooseDateTimeFragment)
        adapter.addFrag(chooseEquipmentFragment)
        adapter.addFrag(chooseExperianceFragment)
        binding.viewPager.adapter = adapter
        adapter.notifyDataSetChanged()
        binding.indicator.setViewPager(binding.viewPager)
        val density = resources.displayMetrics.density
        binding.indicator.setRadius(4 * density)
        if (isNeedToSkipSubGoal) {
            binding.viewPager.setCurrentItem(1)
        }
    }

    internal inner class ViewPagerAdapter(manager: FragmentManager) :
        FragmentStatePagerAdapter(manager) {
        private val mFragmentList = arrayListOf<Fragment>()
        private val mFragmentTitleList = arrayListOf<String>()

        override fun getItem(position: Int): Fragment {


            return mFragmentList[position]
        }

        override fun getCount(): Int {
            return mFragmentList.size
        }

        fun addFrag(fragment: Fragment/*, title: String*/) {
            mFragmentList.add(fragment)
            mFragmentTitleList.add(""/*title*/)
        }

        override fun getPageTitle(position: Int): CharSequence? {

            return mFragmentTitleList[position]
        }
    }

    fun replaceFragment(fragment: Fragment) {
        val transaction = activity?.supportFragmentManager?.beginTransaction()
        transaction?.replace(R.id.flContent, fragment)
        transaction?.addToBackStack("share")
        transaction?.commit()
    }


    /*-------------------Same code from old workout--------------------------*/

    private fun sendDataToServer() {
        TempUtil.log("Workout", "Sending Data to server")
        binding.progressBar.visibility = View.VISIBLE
        val manager = NetworkManager()
        var goal: String? = ""
        if (isNeedToSkipSubGoal) {
            val goalId = getContext()?.let { it1 ->
                PreferenceConnector.readString(
                    it1,
                    PreferenceConnector.MODE_ID,
                    ""
                )
            }
            if (goalId.equals("5c8c96a79c54381add6884da"))//fitness
            {
                goal = "E"
            } else if (goalId.equals("5c8c96a79c54381add6884db"))//build muscle
            {
                goal = chooseSubGoalFragment.getSelectedSubGoal()

            } else if (goalId.equals("5c8c96a79c54381add6884dc"))//fat loss
            {
                goal = "B"
            } else if (goalId.equals("5c8c96a79c54381add6884dd"))//Health and wellbeing
            {
                goal = "C"
            }
        } else {
            goal = chooseSubGoalFragment.getSelectedSubGoal()
        }
        val workoutTrainingRequest = WorkoutTrainingRequest(
            goal,
            chooseDateTimeFragment.getSelectedTimeRange(),
            chooseDateTimeFragment.getSelectedDays(),
            chooseEquipmentFragment.getSelectedEquipment(),
            chooseExperianceFragment.getSelectedExperiance(),
            Utility.getUserID(activity!!.applicationContext)
        )
        if (manager.isConnectingToInternet(activity!!.applicationContext)) {
            manager.createApiRequest(
                ApiUtilis.getAPIInstance(activity!!).sendWorkOutSurveyV3(
                    workoutTrainingRequest
                ),
                object : ServiceListener<UpdateFavoriteModel> {
                    override fun getServerResponse(
                        response: UpdateFavoriteModel,
                        requestcode: Int
                    ) {
                        binding.progressBar.visibility = View.GONE
//                        binding.flContent.visibility = View.VISIBLE
                        if (response.getCode() == 200) {
                            TempUtil.log(TAG, response.getMessage())
                            TempUtil.log(TAG, response.getData()?.getMessage())
                            if (response.getData()!!.getMessage().equals("yes")) {
                                Utility.diseaseList.clear()
                                Utility.setWorkoutStatus(requireContext(), true)
                                displayAlert(response.getMessage())

                            } else {
                                Utility.diseaseList.clear()
                                Utility.setWorkoutStatus(requireContext(), true)
                                replaceFragment(WorkoutFragment())

                            }

                        }

                    }

                    override fun getError(error: ErrorModel, requestcode: Int) {
                        binding.progressBar.visibility = View.GONE
                        Utility.displayShortSnackBar(
                            binding.parentLayout,
                            error.getMessage().toString()
                        )
                    }
                })
        } else {
            binding.progressBar.visibility = View.GONE
            Utility.displayShortSnackBar(
                binding.parentLayout,
                resources.getString(R.string.check_internet)
            )
        }
    }


    private fun displayAlert(message: String?) {
        val dialogBuilder = AlertDialog.Builder(requireContext())
        dialogBuilder.setMessage(message)
            .setCancelable(false)
            .setPositiveButton(R.string.btn_ok) { _, _ ->
                /*val intent = Intent(activity, WorkoutLoading::class.java)
                startActivity(intent)*/
                replaceFragment(WorkoutFragment())
            }


        val alert = dialogBuilder.create()
        alert.show()
    }
    /*-------------------Same code from old workout--------------------------*/
}
