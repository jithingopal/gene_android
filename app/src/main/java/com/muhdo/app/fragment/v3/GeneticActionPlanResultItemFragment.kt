package com.muhdo.app.fragment.v3

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.util.Base64
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.muhdo.app.R
import com.muhdo.app.adapter.v3.GeneticActionPlanResultReycAdapter
import com.muhdo.app.apiModel.v3.GeneticActionPlanReq
import com.muhdo.app.apiModel.v3.GeneticActionPlanResultData
import com.muhdo.app.apiModel.v3.GeneticActionPlanResultPojo
import com.muhdo.app.databinding.V3FragmentGeneticPlanResultItemBinding
import com.muhdo.app.repository.ApiUtilis
import com.muhdo.app.repository.NetworkManager2
import com.muhdo.app.ui.DashboardActivity
import com.muhdo.app.utils.PreferenceConnector
import com.muhdo.app.utils.v3.Constants
import com.muhdo.app.utils.v3.TempUtil
import com.timingsystemkotlin.backuptimingsystem.Utility
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class GeneticActionPlanResultItemFragment : Fragment() {

    internal lateinit var binding: V3FragmentGeneticPlanResultItemBinding

    var adapter: GeneticActionPlanResultReycAdapter? = null
    var data: MutableList<GeneticActionPlanResultData>? = null
    private var ACTION_PLAN_TYPE: String = "DIET"

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.v3_fragment_genetic_plan_result_item, container, false)
        binding = V3FragmentGeneticPlanResultItemBinding.bind(view)
        setTitleDescription()
        binding.recyclerGeneticActionPlanResult.layoutManager =
            LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        updateListAdapter()
        setTitleDescription()
        readActionPlanResultFromServer()

        binding.btnViewDnaResults.setOnClickListener {
            val title: String? = PreferenceConnector.readString(
                activity!!.applicationContext,
                PreferenceConnector.MODE_TEXT,
                "Weight Loss"
            )
            val id: String? = PreferenceConnector.readString(
                activity!!.applicationContext,
                PreferenceConnector.MODE_ID,
                "5c8c96a79c54381add6884da"
            )

            val i = Intent(activity, DashboardActivity::class.java)
            i.putExtra("id", id)
            i.putExtra("title", title)
            i.putExtra(
                Constants.DASHBOARD_ACTION,
                Constants.DASHBOARD_RESULT
            )
            startActivity(i)

/*            val resultsFragment = ResultsFragment()
            resultsFragment.setData(id!!, title!!)
            replaceFragment(resultsFragment)*/

        }
        binding.btnGeneticOverview.setOnClickListener {

            /*val i = Intent(activity, DashboardActivity::class.java)
            i.putExtra(
                Constants.DASHBOARD_ACTION,
                Constants.DASHBOARD_GENETIC_OVERVIEW
            )
            startActivity(i)*/
                  replaceFragment(GeneticOverviewHomeFragment())
        }

        return view
    }

    @SuppressLint("WrongConstant")
    private fun replaceFragment(fragment: Fragment) {
        val transaction = activity!!.supportFragmentManager.beginTransaction()
        transaction.replace(R.id.flContent, fragment)
        transaction.addToBackStack("share")
        transaction.commit()

    }

    fun setActionPlanViewType(type: String) {
        ACTION_PLAN_TYPE = type
    }


    private fun readActionPlanResultFromServer() {

        val userId = Base64.decode(
            Utility.getUserID(activity!!.applicationContext),
            Base64.DEFAULT
        )
        val decodeUserId = String(userId, charset("UTF-8"))
        binding.progressBar.visibility = View.VISIBLE
        val requestBody = GeneticActionPlanReq(
            decodeUserId, ACTION_PLAN_TYPE
        )
        val manager = NetworkManager2()
        if (manager.isConnectingToInternet(binding.parentLayout.context)) {
            val call =
                ApiUtilis.getAPIInstance(activity!!)
                    .postGeneticActionPlanList(requestBody)
            call.enqueue(object : Callback<GeneticActionPlanResultPojo> {
                override fun onResponse(
                    call: Call<GeneticActionPlanResultPojo>,
                    response: Response<GeneticActionPlanResultPojo>
                ) {
                    binding.progressBar.visibility = View.GONE
                    val resultPojo: GeneticActionPlanResultPojo? = response.body()
                    try {
                        if (resultPojo != null && resultPojo.statusCode == 200) {
                            data = resultPojo.data
                            updateListAdapter()

                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }

                override fun onFailure(call: Call<GeneticActionPlanResultPojo>, t: Throwable) {
                    // Log error here since request failed
                    binding.progressBar.visibility = View.GONE
                    TempUtil.log("Login", t.toString())

                    Utility.displayShortSnackBar(
                        binding.parentLayout,
                        "Server connection error!!, please try again later."
                    )


                }
            })
        } else {
            binding.progressBar.visibility = View.GONE

            Utility.displayShortSnackBar(
                binding.parentLayout,
                binding.parentLayout.context.resources.getString(R.string.check_internet)
            )
        }

    }

    private fun updateListAdapter() {
        if (data != null) {
            setNoItemInfoText()
            adapter =
                GeneticActionPlanResultReycAdapter(activity!!.applicationContext, this, data!!)
            binding.recyclerGeneticActionPlanResult.addItemDecoration(
                DividerItemDecoration(
                    binding.recyclerGeneticActionPlanResult.getContext(),
                    DividerItemDecoration.VERTICAL
                )
            )

            binding.recyclerGeneticActionPlanResult.adapter = adapter
            binding.recyclerGeneticActionPlanResult.adapter?.notifyDataSetChanged()
        }
    }


    private fun setNoItemInfoText() {
        if (data != null) {
            if (data!!.size <= 0) {
                binding.tvNoListItemInfo.visibility = View.VISIBLE
                when {
                    ACTION_PLAN_TYPE.equals(Constants.ACTION_PLAN_TYPES[0]) -> binding.tvNoListItemInfo.setText(
                        R.string.no_item_text_for_diet_action_plan
                    )
                    ACTION_PLAN_TYPE.equals(Constants.ACTION_PLAN_TYPES[1]) -> binding.tvNoListItemInfo.setText(
                        R.string.no_item_text_for_vitamin_action_plan
                    )
                    ACTION_PLAN_TYPE.equals(Constants.ACTION_PLAN_TYPES[2]) -> binding.tvNoListItemInfo.setText(
                        R.string.no_item_text_for_physical_action_plan
                    )
                    ACTION_PLAN_TYPE.equals(Constants.ACTION_PLAN_TYPES[3]) -> binding.tvNoListItemInfo.setText(
                        R.string.no_item_text_for_health_action_plan
                    )
                }
            } else {
                binding.tvNoListItemInfo.visibility = View.GONE
            }
        }
    }

    private fun setTitleDescription() {
        when {
            ACTION_PLAN_TYPE.equals(Constants.ACTION_PLAN_TYPES[0]) -> binding.tvTitleDescription.setText(
                R.string.genetic_action_plan_result_item_main_description_diet
            )
            ACTION_PLAN_TYPE.equals(Constants.ACTION_PLAN_TYPES[1]) -> binding.tvTitleDescription.setText(
                R.string.genetic_action_plan_result_item_main_description_vitamin
            )
            ACTION_PLAN_TYPE.equals(Constants.ACTION_PLAN_TYPES[2]) -> binding.tvTitleDescription.setText(
                R.string.genetic_action_plan_result_item_main_description_physical
            )
            ACTION_PLAN_TYPE.equals(Constants.ACTION_PLAN_TYPES[3]) -> binding.tvTitleDescription.setText(
                R.string.genetic_action_plan_result_item_main_description_health
            )
        }
    }
}
