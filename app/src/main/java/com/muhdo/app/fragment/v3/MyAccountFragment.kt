package com.muhdo.app.fragment.v3

import android.annotation.SuppressLint
import android.os.Bundle
import android.text.Editable
import android.text.Selection
import android.text.TextWatcher
import android.util.Base64
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.fragment.app.Fragment
import com.google.gson.Gson
import com.muhdo.app.R
import com.muhdo.app.apiModel.login.CountryData
import com.muhdo.app.apiModel.login.CountryModel
import com.muhdo.app.apiModel.v3.*
import com.muhdo.app.databinding.V3FragmentAccountBinding
import com.muhdo.app.fragment.v3.account.dialog_fragment.EditPasswordDialogFragment
import com.muhdo.app.repository.*
import com.muhdo.app.utils.v3.ErrorRemoverTextWatcher
import com.muhdo.app.utils.v3.TempUtil
import com.timingsystemkotlin.backuptimingsystem.Utility
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*
import java.util.regex.Pattern

class MyAccountFragment : Fragment() {

    private var selectedPosition: Int = 0
    private val countryResponseList = arrayListOf<CountryData>()
    private lateinit var phoneCodeTextWatcher: TextWatcher
    private lateinit var userData: UserData
    private var selectedCountryCode: String = ""
    internal lateinit var binding: V3FragmentAccountBinding
    private val compositeDisposable = CompositeDisposable()
    private var countryCode = "0"
    private var currentCountryCodeId: String? = ""
    private var currentPhno: String? = ""
    private var firstTimeCountrySelectionFromServer: Boolean = false
    private val countryList: MutableList<String> = ArrayList()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.v3_fragment_account, container, false)
        binding = V3FragmentAccountBinding.bind(view)
        toggleEditableFields(false)
        binding.btnEdit.setOnClickListener {
            toggleEditableFields(true)
            binding.edtPhone.addTextChangedListener(phoneCodeTextWatcher)
            binding.btnEdit.visibility = View.GONE
            binding.btnSave.visibility = View.VISIBLE
            binding.btnCancel.visibility = View.VISIBLE
        }
        binding.btnChangePassword.setOnClickListener{
            val newFragment = EditPasswordDialogFragment.newInstance()
            newFragment.enterTransition=R.anim.dialog_enter_from_top
            newFragment.show(childFragmentManager, "Dialog")
        }
        binding.btnSave.setOnClickListener {
            if (checkValidation()) {
                binding.btnEdit.visibility = View.VISIBLE
                binding.btnSave.visibility = View.GONE
                binding.btnCancel.visibility = View.GONE
                binding.edtPhone.removeTextChangedListener(phoneCodeTextWatcher)
                /*   context?.toast(
                       countryResponseList[binding.spinnerCountry.selectedItemPosition].getId().plus(" = id ").plus(
                           countryResponseList[binding.spinnerCountry.selectedItemPosition-1 ].getCountryId()
                       ).plus(" ")
                           .plus(countryResponseList[binding.spinnerCountry.selectedItemPosition-1 ].getName())
                   )*/
                toggleEditableFields(false)
                uploadUserDetails()
            }
        }
        binding.btnCancel.setOnClickListener {
            binding.btnEdit.visibility = View.VISIBLE
            binding.btnSave.visibility = View.GONE
            binding.btnCancel.visibility = View.GONE
            binding.edtPhone.removeTextChangedListener(phoneCodeTextWatcher)
            toggleEditableFields(false)
            bindFieldsWithData()
        }

        phoneCodeTextWatcher = object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                if (!s.toString().startsWith(selectedCountryCode)) {
                    binding.edtPhone.setText(selectedCountryCode)
                    Selection.setSelection(binding.edtPhone.text, binding.edtPhone.length())
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }
        }


        val userId = Base64.decode(
            Utility.getUserID(activity!!.applicationContext),
            Base64.DEFAULT
        )

        val decodeUserId = String(userId, charset("UTF-8"))
        readUserData(decodeUserId)
        setErrorClearWatcher()
        return view
    }

    private fun uploadUserDetails() {
        binding.progressBar.visibility = View.VISIBLE
        val userId = Base64.decode(Utility.getUserID(activity!!.applicationContext), Base64.DEFAULT)
        val decodeUserId = String(userId, charset("UTF-8"))
        val requestBody = UpdateUserData(
            user_id = decodeUserId,
            first_name = binding.edtFirstName.text.toString().trim(),
            last_name = binding.edtLastName.text.toString().trim(),
            //  email = binding.edtEmail.text.toString().trim(),
            //  country = countryResponseList[binding.spinnerCountry.selectedItemPosition-1].getId(),
            //  phone = binding.edtPhone.text.toString(),
            country_region = binding.edtCountryRegion.text.toString().trim(),
            address_lane1 = binding.edtStreet.text.toString().trim(),
            city = binding.edtCity.text.toString().trim(),
            zipcode = binding.edtPostcode.text.toString().trim()
        )

        val gson = Gson()
        TempUtil.log("Request Body", gson.toJson(requestBody))
        compositeDisposable.add(
            ApiUtilis.getAPIInstance(activity!!)
                .postUserDetails(requestBody)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ success: BaseResponse ->
                    run {
                        binding.progressBar.visibility = View.GONE
                        Utility.displayShortSnackBar(
                            binding.parentLayout,
                            success.message
                        )
                    }
                }, { error: Throwable ->
                    run {
                        binding.progressBar.visibility = View.GONE
                        Utility.displayShortSnackBar(
                            binding.parentLayout,
                            error.localizedMessage
                        )
                    }
                })
        )
    }


    private fun getAllLocation() {
        val manager = NetworkManager()
        manager.createApiRequest(
            ApiUtilis.getAPIInstance(activity!!).getAllCountry(),
            object :
                ServiceListener<CountryModel> {
                override fun getServerResponse(response: CountryModel, requestcode: Int) {

                    try {
                        setSpinnerData(response)
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }

                override fun getError(error: ErrorModel, requestcode: Int) {
                    Utility.displayShortSnackBar(
                        binding.parentLayout,
                        error.getMessage().toString()
                    )
                }
            })
    }

    private fun setLanguageSpinnerData() {
        try {
            val languageList: MutableList<String> = ArrayList()

            languageList.add(getString(R.string.please_select_language))
            languageList.add(getString(R.string.english))
            val selectedPosition = 1


            // Create an ArrayAdapter using a simple spinner layout and languages array
            val adapter = ArrayAdapter(
                activity?.getApplicationContext(),
                android.R.layout.simple_spinner_item,
                languageList
            )
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            // Set Adapter to Spinner
            binding.spinnerLanguage.adapter = adapter
            binding.spinnerLanguage.setSelection(selectedPosition)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun setSpinnerData(response: CountryModel) {
        countryList.add(getString(R.string.please_select_country))
        response.getData()?.takeIf { it.isNotEmpty() }
            ?.apply { countryResponseList.addAll(response.getData()!!) }
/*        val countryData = CountryData()
        countryData.setName("Please select country")
        countryResponseList.add(0, countryData)*/
        for (i in 0 until response.getData()!!.size) {
            countryList.add(response.getData()!![i].getName().toString())
            if (currentCountryCodeId.equals(response.getData()!![i].getId().toString()))
                selectedPosition = i + 1
        }
        // Create an ArrayAdapter using a simple spinner layout and languages array
        val adapter = ArrayAdapter(activity, android.R.layout.simple_spinner_item, countryList)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        // Set Adapter to Spinner
        binding.spinnerCountry.adapter = adapter
        binding.spinnerCountry.onItemSelectedListener = null
        bindFieldsWithData()
//        binding.spinnerCountry.setSelection(selectedPosition)
        binding.spinnerCountry.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {
                @SuppressLint("SetTextI18n")
                override fun onItemSelected(
                    parent: AdapterView<*>,
                    view: View,
                    position: Int,
                    id: Long
                ) {

                    if (position != 0 && !firstTimeCountrySelectionFromServer) {
                        if (countryList.size > position) {

                            Utility.countryID =
                                response.getData()!![position - 1].getId().toString()
                            val code =
                                response.getData()!![position - 1].getPhoneCode()!!.toString()
                            selectedCountryCode = "+".plus(code)
                            binding.edtPhone.setText(selectedCountryCode)

                            val l = binding.edtPhone.text.length
                            binding.edtPhone.setSelection(l)
                        }
                    } else {
                        firstTimeCountrySelectionFromServer = false
                    }
                }

                override fun onNothingSelected(parent: AdapterView<*>) {
                }
            }
    }

    private fun setErrorClearWatcher() {
        binding.edtFirstName.also { it.addTextChangedListener(ErrorRemoverTextWatcher(it)) }
        binding.edtLastName.also { it.addTextChangedListener(ErrorRemoverTextWatcher(it)) }
        //   binding.edtEmail.also { it.addTextChangedListener(ErrorRemoverTextWatcher(it)) }
        binding.edtCountryRegion.also { it.addTextChangedListener(ErrorRemoverTextWatcher(it)) }
        binding.edtStreet.also { it.addTextChangedListener(ErrorRemoverTextWatcher(it)) }
        binding.edtCity.also { it.addTextChangedListener(ErrorRemoverTextWatcher(it)) }
        binding.edtPostcode.also { it.addTextChangedListener(ErrorRemoverTextWatcher(it)) }
    }

    private fun toggleEditableFields(isEditable: Boolean) {
        binding.edtFirstName.isEnabled = isEditable
        binding.edtLastName.isEnabled = isEditable
        // binding.edtEmail.isEnabled = isEditable
        //   binding.edtPhone.isEnabled = isEditable
        binding.edtCountryRegion.isEnabled = isEditable
        binding.edtStreet.isEnabled = isEditable
        binding.edtPostcode.isEnabled = isEditable
        binding.edtDob.isEnabled = isEditable
        binding.edtCity.isEnabled = isEditable
        binding.spinnerCountry.isEnabled = false
        binding.spinnerLanguage.isEnabled = isEditable
    }

    private fun checkValidation(): Boolean {
        var isValid = true
        when {
            binding.edtFirstName.text.isEmpty() -> {
                TempUtil.log("Error", "Error in Registraion first name")
                binding.edtFirstName.error =
                    getString(R.string.please_enter_first_name)
                binding.edtFirstName.requestFocus()
                isValid = false
            }

            binding.edtLastName.text.isEmpty() -> {
                binding.edtLastName.error =
                    getString(R.string.please_enter_last_name)
                binding.edtLastName.requestFocus()
                isValid = false
            }

            /*        binding.edtEmail.text.isEmpty()||!isValidEmailId(binding.edtEmail.text.toString()) -> {
                        binding.edtEmail.error = getString(R.string.please_enter_email)
                        binding.edtEmail.requestFocus()
                        isValid = false
                    }*/

            /*     binding.edtPhone.text.isEmpty() || binding.edtPhone.text.length < 10
                         || !binding.edtPhone.text.contains("+") -> {
                     binding.edtPhone.error = getString(R.string.please_enter_phone)
                     binding.edtPhone.requestFocus()
                     isValid = false
                 }*/
            binding.edtCountryRegion.text.isEmpty() -> {
                binding.edtCountryRegion.error =
                    getString(R.string.please_enter_country_name)
                binding.edtCountryRegion.requestFocus()
                isValid = false
            }

            binding.edtStreet.text.isEmpty() -> {
                binding.edtStreet.error = getString(R.string.please_enter_street)
                binding.edtStreet.requestFocus()
                isValid = false
            }

            binding.edtCity.text.isEmpty() -> {
                binding.edtCity.error = getString(R.string.please_enter_city)
                binding.edtCity.requestFocus()
                isValid = false
            }
            binding.edtPostcode.text.isEmpty() -> {
                binding.edtPostcode.error = getString(R.string.please_enter_postcode)
                binding.edtPostcode.requestFocus()
                isValid = false
            }

            /*  binding.spinnerCountry.selectedItem.equals("") || binding.spinnerCountry.selectedItem.equals(
                  "Please select country"
              ) -> {
                  Toast.makeText(context, "Please select country", Toast.LENGTH_LONG).show()
                  binding.spinnerCountry.requestFocus()
                  isValid = false
              }*/
        }
        return isValid
    }

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.dispose()
    }


    @SuppressLint("NewApi")
    public fun readUserData(userId: String) {


        binding.progressBar.visibility = View.VISIBLE
//        val paramBody =            RequestBody.create("application/json; charset=utf-8".toMediaTypeOrNull(), params)
        val getUserCondition: GetUserConditionReq = GetUserConditionReq(GetUserReq(userId))
        val manager = NetworkManager2()
        if (manager.isConnectingToInternet(binding.parentLayout.context)) {
            val call =
                ApiUtilis.getAPIInstance(activity!!)
                    .getUserDetails(
                        getUserCondition
                    )
            call.enqueue(object : Callback<UserDataPojo> {
                override fun onResponse(
                    call: Call<UserDataPojo>,
                    response: Response<UserDataPojo>
                ) {
                    try {
                        binding.progressBar.visibility = View.GONE
                        val userDataPojo: UserDataPojo? = response.body()
                        if (userDataPojo != null && userDataPojo.statusCode == 200) {
                            if (userDataPojo.data != null && userDataPojo.data?.get(0) != null) {
                                userData = userDataPojo.data!![0]
                                currentCountryCodeId = userData.country
                            }
                        }
                        setLanguageSpinnerData()
                        getAllLocation()
                    } catch (e: java.lang.Exception) {
                        e.printStackTrace()
                    }
                }

                override fun onFailure(call: Call<UserDataPojo>, t: Throwable) {
                    // Log error here since request failed
                    binding.progressBar.visibility = View.GONE
                    TempUtil.log("Login", t.toString())

                    Utility.displayShortSnackBar(
                        binding.parentLayout,
                        getString(R.string.server_connection_error)
                    )


                }
            })
        } else {
            binding.progressBar.visibility = View.GONE


            try {
                Utility.displayShortSnackBar(
                    binding.parentLayout,
                    binding.parentLayout.context.resources.getString(R.string.check_internet)
                )
            } catch (e: Exception) {
                e.printStackTrace()
            }

        }

    }

    private fun bindFieldsWithData() {
        if (::userData.isInitialized) {
            binding.edtFirstName.setText(userData.first_name)
            binding.edtLastName.setText(userData.last_name)
            binding.edtEmail.setText(userData.email)
            binding.edtKitId.setText(userData.kit_id)
            binding.edtPhone.setText(userData.phone)
            firstTimeCountrySelectionFromServer = true
            binding.edtCountryRegion.setText(userData.country_region)
            binding.edtStreet.setText(userData.address_lane1)
            binding.edtCity.setText(userData.city)
            binding.edtPostcode.setText(userData.zipcode)
            binding.spinnerCountry.setSelection(selectedPosition)
            /*  Observable.fromIterable(countryResponseList.withIndex()).subscribeOn(Schedulers.io())
                  .observeOn(Schedulers.io()).takeUntil( {(index,item) ->
                          item.getId() == currentCountryCodeId
              }).subscribe{(index,item) -> run{
                      Log.d("items and index", " ${item} and ${index}")
                  }}  */
            /*        Observable.fromIterable(countryResponseList.withIndex()).subscribeOn(Schedulers.io())
                        .observeOn(Schedulers.io()).forEachWhile { (index, item) ->
                            if (item.getId() == currentCountryCodeId) {
                                binding.spinnerCountry.setSelection(index + 1)
                            }
                            item.getId() == currentCountryCodeId
                        }
        */
        }
    }

    private fun isValidEmailId(email: String): Boolean {

        return Pattern.compile(
            "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                    + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                    + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                    + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                    + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                    + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$"
        ).matcher(email).matches()
    }
}