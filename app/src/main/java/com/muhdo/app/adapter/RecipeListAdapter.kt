package com.muhdo.app.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.muhdo.app.R
import com.muhdo.app.databinding.ListRecipeBinding
import com.muhdo.app.ui.CalorieActivity
import com.muhdo.app.ui.MyFavouriteActivity

class RecipeListAdapter(internal var context: Context, private val list: List<Int>, private val nameList: List<String>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        val binding =
            DataBindingUtil.inflate<ListRecipeBinding>(
            LayoutInflater.from(parent.context),
            R.layout.list_recipe,
            parent,
            false
        )
        return RecipeListAdapter.ViewHolder(binding)

    }


    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val viewHolder = holder as ViewHolder
        val model = list[position]
        viewHolder.binding!!.imgRecipe.setImageResource(model)
        viewHolder.binding!!.recipeName.text = nameList[position]
        viewHolder.binding!!.imgRecipe.setOnClickListener {
         if(viewHolder.binding!!.recipeName.text == "MY FAVOURITES"){
             val i = Intent(context, MyFavouriteActivity::class.java)
             context.startActivity(i)
         }else{
             val i = Intent(context, CalorieActivity::class.java)
             context.startActivity(i)
         }

        }

    }

    class ViewHolder(binding: ListRecipeBinding) : RecyclerView.ViewHolder(binding.root) {
        var binding: ListRecipeBinding? = null

        init {
            this.binding = DataBindingUtil.bind(binding.root)
        }
    }
}
