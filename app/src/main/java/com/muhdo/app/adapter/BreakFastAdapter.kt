package com.muhdo.app.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.muhdo.app.R
import com.muhdo.app.databinding.ListBreakfastBinding
import com.muhdo.app.model.RecipeModel

class BreakFastAdapter(internal var context: Context, private val list: List<RecipeModel>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        val binding = DataBindingUtil.inflate<ListBreakfastBinding>(
            LayoutInflater.from(parent.context),
            R.layout.list_breakfast,
            parent,
            false
        )
        return BreakFastAdapter.ViewHolder(binding)

    }


    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        val viewHolder = holder as ViewHolder
        val model = list[position]
        viewHolder.binding!!.txtRecipeName.text = model.name
        viewHolder.binding!!.imgRecipe.setImageResource(model.image!!)
        viewHolder.binding!!.cardView.setOnClickListener {

        }
    }

    class ViewHolder(binding: ListBreakfastBinding) : RecyclerView.ViewHolder(binding.root) {
        var binding: ListBreakfastBinding? = null

        init {
            this.binding = DataBindingUtil.bind(binding.root)
        }
    }
}
