package com.muhdo.app.adapter.v3

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.muhdo.app.R
import android.widget.CheckBox
import androidx.core.content.ContextCompat
import com.bumptech.glide.Glide
import com.muhdo.app.apiModel.mealModel.Recipe
import com.muhdo.app.apiModel.mealModel.daysModel.Monday
import com.muhdo.app.interfaces.v3.OnMealChoosedInChildListener
import com.timingsystemkotlin.backuptimingsystem.Utility


class ChooseMealPlanChildRecyAdpter(
    internal var context: FragmentActivity,
    private var list: List<Monday>,
    private val dayPostion: Int,
    private val mealPostion: Int,
    val onMealChoosedInChildListener: OnMealChoosedInChildListener
) : RecyclerView.Adapter<ChooseMealPlanChildRecyAdpter.Holder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        val itemview = LayoutInflater.from(context)
            .inflate(R.layout.item_choose_meal_for_day_child, parent, false)
        return Holder(itemview)
    }


    var selected_position = 0
    override fun getItemCount(): Int {
        listData = ArrayList<Recipe>(list.size)
        return list.size
    }

    var listData: ArrayList<Recipe> = ArrayList<Recipe>()


    override fun onBindViewHolder(holder: Holder, position: Int) {
        if (this.list.isNotEmpty()) {
            val model = list[position]
            holder.txtRecipeName.text = model.getName()
            Glide
                .with(context)
                .load("https://s3.amazonaws.com/" + model.getBucket() + "/" + model.getImage())
                .centerCrop()
                .placeholder(R.drawable.diet_1)
                .into(holder.imgRecipe)

            if (model.getIsFavourite() == true || Utility.favouriteIdList.contains(model.getId())) {
                holder.cbFavourite.isChecked = true
            }
            if (Utility.unFavouriteIdList.contains(model.getId())) {
                holder.cbFavourite.isChecked = false
            }
            /*if (selected_position==0&&model?.getIsSelected()!!) {
                holder.cardView.setCardBackgroundColor(ContextCompat.getColor(context, R.color.primary_error_color))
                holder.txtRecipeName.setTextColor(ContextCompat.getColor(context, R.color.white))
            } else {
                holder.cardView.setCardBackgroundColor(ContextCompat.getColor(context, R.color.white))
                holder.txtRecipeName.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary))
            }*/
            var day:String=""
            when(dayPostion){
                0->{day=context.getString(R.string.monday)}
                1->{day=context.getString(R.string.tuesday)}
                2->{day=context.getString(R.string.wednesday)}
                3->{day=context.getString(R.string.thursday)}
                4->{day=context.getString(R.string.friday)}
                5->{day=context.getString(R.string.saturday)}
                6->{day=context.getString(R.string.sunday)}
            }
            val recipe: Recipe = Recipe(list.get(selected_position).getId(), day)
            if (selected_position == position) {
                holder.cardView.setCardBackgroundColor(
                    ContextCompat.getColor(
                        context,
                        R.color.primary_error_color
                    )
                )
                holder.txtRecipeName.setTextColor(ContextCompat.getColor(context, R.color.white))
                onMealChoosedInChildListener?.onMealChoosed(recipe, mealPostion)
            } else {
                holder.cardView.setCardBackgroundColor(
                    ContextCompat.getColor(
                        context,
                        R.color.white
                    )
                )
                holder.txtRecipeName.setTextColor(
                    ContextCompat.getColor(
                        context,
                        R.color.colorPrimary
                    )
                )


            }
            holder.cardView.setOnClickListener {
                if (onMealChoosedInChildListener == null) {
                    onMealChoosedInChildListener?.onMealChoosed(recipe, mealPostion)
                }
                selected_position = position
                notifyDataSetChanged()
            }


        }
    }

    inner class Holder(view: View) : RecyclerView.ViewHolder(view) {
        internal var txtRecipeName: TextView = view.findViewById(R.id.txt_recipe_name)
        internal var imgRecipe: ImageView = view.findViewById(R.id.img_recipe)
        internal var cbFavourite: CheckBox = view.findViewById(R.id.check_favourite)
        internal var cardView: CardView = view.findViewById(R.id.card_view)
    }

}