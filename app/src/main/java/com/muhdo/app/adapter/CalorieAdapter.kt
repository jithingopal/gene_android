package com.muhdo.app.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.muhdo.app.R
import com.muhdo.app.apiModel.mealModel.CalorieDatum
import com.muhdo.app.databinding.ItemCalorieBinding

class CalorieAdapter(
    internal var context: Context,
    private val list: List<CalorieDatum>
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        val binding = DataBindingUtil.inflate<ItemCalorieBinding>(
            LayoutInflater.from(parent.context),
            R.layout.item_calorie,
            parent,
            false
        )
        return CalorieAdapter.ViewHolder(binding)

    }


    override fun getItemCount(): Int {
        return list.size
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        val viewHolder = holder as ViewHolder
        viewHolder.binding!!.mealName.text = list[position].getType() + ": "
        viewHolder.binding!!.mealCalorie.text = "" + list[position].getKcal() + " kcal"
    }

    class ViewHolder(binding: ItemCalorieBinding) : RecyclerView.ViewHolder(binding.root) {
        var binding: ItemCalorieBinding? = null

        init {
            this.binding = DataBindingUtil.bind(binding.root)
        }
    }
}
