package com.muhdo.app.adapter.v3

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.RecyclerView
import com.muhdo.app.R
import com.muhdo.app.apiModel.healthinsights.Data
import com.muhdo.app.apiModel.v3.epigenetics.EpiSampleInfo
import com.muhdo.app.apiModel.v3.epigenetics.ScoreDataHome
import com.muhdo.app.fragment.v3.epigenetics.EpigeneticsDetailsFragment
import com.muhdo.app.fragment.v3.health_insights.HealthInsightsDetailsResults2Fragment
import com.muhdo.app.interfaces.v3.OnChangeInnerFragment
import com.muhdo.app.interfaces.v3.OnHealthInsightsHeaderValuesChanged
import com.muhdo.app.utils.v3.ItemAnimation
import org.jetbrains.anko.toast

class HealthInsightsOverviewRecyAdpter(
    internal var context: FragmentActivity,
    private var list: List<Data>,
    private var onHealthInsightsHeaderValuesChanged: OnHealthInsightsHeaderValuesChanged?,
    private val tabPosition: Int,
    private val childFragmentManager: FragmentManager,
    private val onChangeInnerFragment: OnChangeInnerFragment
) : RecyclerView.Adapter<HealthInsightsOverviewRecyAdpter.Holder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        val itemview = LayoutInflater.from(context.applicationContext)
            .inflate(R.layout.v3_item_health_insight_overview, parent, false)
        return Holder(itemview)
    }


    fun updateList(list: List<Data>) {
        this.list = list
        lastPosition = -1

        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        if (this.list.isNotEmpty()) {

            holder.tvTitle.setText(
                list.get(position).title.toString()
            )
            holder.tvStatus.setText(
                context.getString(R.string.status) + " " + list.get(position).status.toString().replace(
                    "\n",
                    " "
                ).toUpperCase()
            )
            holder.rootLayout.setOnClickListener {
                var fragment = HealthInsightsDetailsResults2Fragment();
                fragment.list = list
                fragment.selectedPostion = position
                fragment.onHealthInsightsHeaderValuesChanged = onHealthInsightsHeaderValuesChanged!!
                fragment.tabPosition = tabPosition
                if (onHealthInsightsHeaderValuesChanged != null) {
                    onHealthInsightsHeaderValuesChanged!!.headerValuesChanged(
                        list.get(position).title,
                        list.get(position).status,
                        list.get(position).output!!.indicator,
                        tabPosition
                    )
                }
                if (onChangeInnerFragment != null) {
                    onChangeInnerFragment.onFragmentChanged(tabPosition, fragment)
                }

            }

            if (list.get(position).output!!.indicator == 5) {
                holder.imgIndicator.setImageResource(R.drawable.v3_ic_slider_green)

            } else if (list.get(position).output!!.indicator == 4) {
                holder.imgIndicator.setImageResource(R.drawable.v3_ic_slider_half_green)

            } else if (list.get(position).output!!.indicator == 3) {
                holder.imgIndicator.setImageResource(R.drawable.v3_ic_slider_yellow)

            } else if (list.get(position).output!!.indicator == 2) {
                holder.imgIndicator.setImageResource(R.drawable.v3_ic_slider_half_red)

            } else if (list.get(position).output!!.indicator == 1) {
                holder.imgIndicator.setImageResource(R.drawable.v3_ic_slider_red)
            }




            setAnimation(holder.itemView, position)
        }
    }

    private fun replaceFragment(fragment: Fragment) {
        val transaction = childFragmentManager.beginTransaction()
        transaction.replace(R.id.linearLayoutContainer, fragment)
        transaction.addToBackStack("share")
        transaction.commit()

    }

    inner class Holder(view: View) : RecyclerView.ViewHolder(view) {
        internal var tvTitle: TextView = view.findViewById(R.id.tvTitle)
        internal var tvStatus: TextView = view.findViewById(R.id.tvStatus)
        internal var tvScore: TextView = view.findViewById(R.id.tvScore)
        internal var imgIndicator: ImageView = view.findViewById(R.id.imgIndicator)
        internal var rootLayout: View = view.findViewById(R.id.root_layout)
    }

    private var lastPosition = -1
    private var on_attach = true

    private fun setAnimation(view: View, position: Int) {
        if (position > lastPosition) {
            ItemAnimation.animate(view, if (on_attach) position else -1, ItemAnimation.BOTTOM_UP)
            lastPosition = position
        }
    }
}