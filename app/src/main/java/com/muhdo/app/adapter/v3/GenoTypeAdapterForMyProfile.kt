package com.muhdo.app.adapter.v3

import android.app.Activity
import android.content.Context
import android.content.DialogInterface
import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.muhdo.app.R
import com.muhdo.app.apiModel.keyData.GenoTypeData
import com.muhdo.app.fragment.v3.MyProfileFragment
import com.muhdo.app.fragment.v3.QuestionnaireHomeFragment
import com.muhdo.app.interfaces.v3.OnGenoTypeItemSelectedListener
import com.muhdo.app.utils.PreferenceConnector


class GenoTypeAdapterForMyProfile(
    internal var context: Activity,
    private var list: MutableList<GenoTypeData>
) : RecyclerView.Adapter<GenoTypeAdapterForMyProfile.Holder>() {

    var onGenoTypeItemSelectedListener: OnGenoTypeItemSelectedListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        val itemview = LayoutInflater.from(context).inflate(R.layout.list_genotype, parent, false)
        return Holder(itemview)
    }

    fun setGenoTypeItemSelectListener(onGenoTypeItemSelectedListener: OnGenoTypeItemSelectedListener) {
        this.onGenoTypeItemSelectedListener = onGenoTypeItemSelectedListener
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {

        if (this.list.isNotEmpty()) {
            Glide
                .with(context)
                .load(list[position].getIcon())
                .centerInside()
                .into(holder.imgResults)

            holder.txtResultTitle.text = list[position].getTitle()
            var id: String? = PreferenceConnector.readString(
                context!!,
                PreferenceConnector.MODE_ID,
                "5c8c96a79c54381add6884da"
            )

            if (id!!.equals(list[position].getId()!!)) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    holder.relativeLayout.setBackground(context.getDrawable(R.drawable.v3_bg_selected_result_item))
                } else {
                    holder.relativeLayout.setBackground(context.resources.getDrawable(R.drawable.v3_bg_selected_result_item))

                }
            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    holder.relativeLayout.setBackgroundColor(context.getColor(R.color.white))
                } else {
                    holder.relativeLayout.setBackgroundColor(context.resources.getColor(R.color.white))

                }
            }

            holder.imgResults.isClickable = false;

            holder.relativeLayout.setOnClickListener {


                if (id!!.equals(list[position].getId()!!)) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        holder.relativeLayout.setBackground(context.getDrawable(R.drawable.v3_bg_selected_result_item))
                    } else {
                        holder.relativeLayout.setBackground(context.resources.getDrawable(R.drawable.v3_bg_selected_result_item))
                    }
                    context.let { it1 ->
                        PreferenceConnector.writeString(
                            it1,
                            PreferenceConnector.MODE_TEXT,
                            list[position].getTitle()!!
                        )
                    }
                    context.let { it1 ->
                        PreferenceConnector.writeString(
                            it1,
                            PreferenceConnector.MODE_ID,
                            list[position].getId()!!
                        )
                    }
                    notifyDataSetChanged()
                    if (onGenoTypeItemSelectedListener != null) {
                        onGenoTypeItemSelectedListener?.onItemSelected(
                            list[position].getId(),
                            list[position].getTitle()
                        )
                    }
                } else {
                    var dialog: androidx.appcompat.app.AlertDialog.Builder =
                        androidx.appcompat.app.AlertDialog.Builder(context!!)
                    var dialgoMessage: String =
                        context.getText(R.string.choose_goal_confirmation_warning_part_1).toString() + " " + list[position].getTitle() + " " + context.getText(
                            R.string.choose_goal_confirmation_warning_part_2
                        ).toString()
                    dialog.setMessage(dialgoMessage)
                    dialog.setPositiveButton(
                        R.string.btn_go_to_choose_goal_warning,
                        DialogInterface.OnClickListener { dialog, id ->
                            dialog.cancel()
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                holder.relativeLayout.setBackground(context.getDrawable(R.drawable.v3_bg_selected_result_item))
                            } else {
                                holder.relativeLayout.setBackground(context.resources.getDrawable(R.drawable.v3_bg_selected_result_item))

                            }
                            context.let { it1 ->
                                PreferenceConnector.writeString(
                                    it1,
                                    PreferenceConnector.MODE_TEXT,
                                    list[position].getTitle()!!
                                )
                            }
                            context.let { it1 ->
                                PreferenceConnector.writeString(
                                    it1,
                                    PreferenceConnector.MODE_ID,
                                    list[position].getId()!!
                                )
                            }
                            notifyDataSetChanged()
                            if (onGenoTypeItemSelectedListener != null) {
                                onGenoTypeItemSelectedListener?.onItemSelected(
                                    list[position].getId(),
                                    list[position].getTitle()
                                )
                            }
                        })
                    dialog.setNegativeButton(
                        R.string.btn_cancel,
                        DialogInterface.OnClickListener { dialog, id ->
                            dialog.cancel()
                        })
                    dialog.show()
                }

            }
        }
    }

    inner class Holder(view: View) : RecyclerView.ViewHolder(view) {
        internal var imgResults: ImageView = view.findViewById(R.id.img_results)

        internal var txtResultTitle: TextView = view.findViewById(R.id.txt_result_title)
        internal var relativeLayout: RelativeLayout = view.findViewById(R.id.relative_layout)
        internal var cardView: View = view.findViewById(R.id.card_view)
    }
}





