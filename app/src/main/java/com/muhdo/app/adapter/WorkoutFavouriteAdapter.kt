package com.muhdo.app.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseExpandableListAdapter
import android.widget.CheckBox
import android.widget.TextView
import com.ct7ct7ct7.androidvimeoplayer.view.VimeoPlayerView
import com.muhdo.app.R
import com.muhdo.app.apiModel.workout.WorkoutDay
import com.muhdo.app.fragment.FavouriteWorkoutFragment
import com.timingsystemkotlin.backuptimingsystem.Utility


class WorkoutFavouriteAdapter internal constructor(
    private val context: FavouriteWorkoutFragment,
    private val list: List<WorkoutDay>?
) : BaseExpandableListAdapter() {
    override fun getChild(listPosition: Int, expandedListPosition: Int): Any {
        return this.list!![expandedListPosition]
    }

    override fun getChildId(listPosition: Int, expandedListPosition: Int): Long {
        return expandedListPosition.toLong()
    }

    override fun getChildView(
        listPosition: Int,
        expandedListPosition: Int,
        isLastChild: Boolean,
        convertView: View?,
        parent: ViewGroup
    ): View {
        var convertView = convertView

        if (convertView == null) {
            val layoutInflater = context.activity!!.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            convertView = layoutInflater.inflate(R.layout.workout_child_layout, null)
        }

        val videoView = convertView!!.findViewById<VimeoPlayerView>(R.id.vimeoPlayer)

        context.lifecycle.addObserver(videoView)

        val link = list!![listPosition].getVideoLink()!!
        if (link != "" && link.contains("player.vimeo.com/video/")) {
            val link1 = link.replace("https://player.vimeo.com/video/", "")
            System.out.println("link1====>  $link1")
            videoView.initialize(link1.toInt())
        }


        val checkFavourite = convertView.findViewById<CheckBox>(R.id.check_favourite)

        val id = list[listPosition].getExercises()!!.getId()!!

        if (list[listPosition].getExercises()!!.getIsFavourite()!!
            || Utility.favouriteIdList.contains(id)
        ) {
            checkFavourite.isChecked = true
        }
        if (Utility.unFavouriteIdList.contains(id)) {
            checkFavourite.isChecked = false
        }
        val expandedListTextView = convertView.findViewById<TextView>(R.id.txt_recipe_name)
        val workoutText: String
        if (list[listPosition].getEquipmentWeight() == 0 && (list[listPosition].getRest() == 0 ||
                    list[listPosition].getRest() == 0.0)
        ) {
            workoutText = "" + list[listPosition].getSets() + " Sets | " +
                    list[listPosition].getRepetition() + " Reps | "
        } else if (list[listPosition].getEquipmentWeight() == 0) {
            workoutText = "" + list[listPosition].getSets() + " Sets | " +
                    list[listPosition].getRepetition() + " Reps | " +
                    list[listPosition].getRest() + " Rest"
        } else if ((list[listPosition].getRest() == 0 ||
                    list[listPosition].getRest() == 0.0)
        ) {
            workoutText = "" + list[listPosition].getSets() + " Sets | " +
                    list[listPosition].getRepetition() + " Reps | " +
                    list[listPosition].getEquipmentWeight() + " Kg | "

        } else {
            workoutText = "" + list[listPosition].getSets() + " Sets | " +
                    list[listPosition].getRepetition() + " Reps | " +
                    list[listPosition].getEquipmentWeight() + " Kg | " +
                    list[listPosition].getRest() + " Rest"
        }

        expandedListTextView.text = workoutText
        return convertView
    }

    override fun getChildrenCount(listPosition: Int): Int {
        return 1
    }

    override fun getGroup(listPosition: Int): Any {
        return this.list!![listPosition]
    }

    override fun getGroupCount(): Int {
        return this.list!!.size
    }

    override fun getGroupId(listPosition: Int): Long {
        return listPosition.toLong()
    }

    @SuppressLint("SetTextI18n")
    override fun getGroupView(listPosition: Int, isExpanded: Boolean, convertView: View?, parent: ViewGroup): View {
        var convertView = convertView
        if (convertView == null) {
            val layoutInflater = context.activity!!.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            convertView = layoutInflater.inflate(R.layout.workout_parent_layout, null)
        }


        val listTitleTextView = convertView!!.findViewById<TextView>(R.id.txt_workout_name)
        listTitleTextView.text = list!![listPosition].getExercises()!!.getExerciseName()
        val txtWorkoutSteps = convertView.findViewById<TextView>(R.id.txt_workout_steps)
        txtWorkoutSteps.text = "" + list[listPosition].getSets() + "x" + list[listPosition].getRepetition()
        return convertView
    }

    override fun hasStableIds(): Boolean {
        return false
    }

    override fun isChildSelectable(listPosition: Int, expandedListPosition: Int): Boolean {
        return true
    }
}