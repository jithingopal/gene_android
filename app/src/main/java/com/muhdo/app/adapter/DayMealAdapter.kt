package com.muhdo.app.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.muhdo.app.R
import com.muhdo.app.apiModel.mealModel.daysModel.Monday
import com.muhdo.app.databinding.ListBreakfastBinding
import com.muhdo.app.ui.MealActivity
import com.muhdo.app.utils.v3.DayMealClickListener
import com.timingsystemkotlin.backuptimingsystem.Utility

class DayMealAdapter(
    internal var context: Context,
    private val list: List<Monday>,
    private val clickListener: DayMealClickListener
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        val binding = DataBindingUtil.inflate<ListBreakfastBinding>(
            LayoutInflater.from(parent.context),
            R.layout.list_breakfast,
            parent,
            false
        )
        return DayMealAdapter.ViewHolder(binding)

    }


    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        val viewHolder = holder as ViewHolder
        val model = list[position]
        viewHolder.binding!!.txtRecipeName.text = model.getName()
        Glide
            .with(context)
            .load("https://s3.amazonaws.com/" + model.getBucket() + "/" + model.getImage())
            .centerCrop()
            .placeholder(R.drawable.diet_1)
            .into(viewHolder.binding!!.imgRecipe)

        if (model.getIsFavourite() == true || Utility.favouriteIdList.contains(model.getId())) {
            viewHolder.binding!!.checkFavourite.isChecked = true
        }
        if (Utility.unFavouriteIdList.contains(model.getId())) {
            viewHolder.binding!!.checkFavourite.isChecked = false
        }

        viewHolder.binding!!.cardView.setOnClickListener {
            clickListener.onCardViewClick(model.getId()!!)
            /*       if (context is MealActivity) {
                       (context as MealActivity).goToRecipe(model.getId()!!)
                   }*/
        }
        viewHolder.binding!!.checkFavourite.setOnClickListener {
            val sts: Boolean
            viewHolder.binding!!.checkFavourite.isClickable = false
            if (!Utility.unFavouriteIdList.contains(model.getId())) {
                if (model.getIsFavourite() == true || Utility.favouriteIdList.contains(model.getId())) {
                    sts = false
                    viewHolder.binding!!.checkFavourite.isChecked = false
                } else {
                    sts = true
                    viewHolder.binding!!.checkFavourite.isChecked = true
                }
            } else {

                if (Utility.favouriteIdList.contains(model.getId())) {
                    sts = false
                    viewHolder.binding!!.checkFavourite.isChecked = false
                } else {
                    sts = true
                    viewHolder.binding!!.checkFavourite.isChecked = true
                }

            }

            clickListener.onFavouriteClick(
                model.getId().toString(),
                viewHolder.binding!!.checkFavourite,
                sts
            )
        /*    if (context is MealActivity) {
                (context as MealActivity).updateFavorite(
                    model.getId().toString(),
                    viewHolder.binding!!.checkFavourite,
                    sts
                )
            }*/
        }

    }

    class ViewHolder(binding: ListBreakfastBinding) : RecyclerView.ViewHolder(binding.root) {
        var binding: ListBreakfastBinding? = null

        init {
            this.binding = DataBindingUtil.bind(binding.root)
        }
    }
}
