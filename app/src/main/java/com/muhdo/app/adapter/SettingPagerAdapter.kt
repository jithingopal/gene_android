package com.muhdo.app.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.muhdo.app.fragment.PasswordFragment
import com.muhdo.app.fragment.PersonalInfoFragment
import com.muhdo.app.fragment.PreferencesFragment

class SettingPagerAdapter(fm: FragmentManager) : FragmentStatePagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {


        return when (position) {
            0 -> {
                PersonalInfoFragment()

            }
            1 -> {
                PreferencesFragment()

            }
            else -> {
                PasswordFragment()
            }
        }
    }

    override fun getCount(): Int {
        return 3
    }

    override fun getPageTitle(position: Int): CharSequence {
        return when (position) {
            0 -> "Personal Information"
            1 -> "Diet & Workout Preferences"
            else -> "Password & Preferences"
        }
    }
}