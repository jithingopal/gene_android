package com.muhdo.app.adapter;

import android.annotation.SuppressLint;
import android.graphics.PorterDuff;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.muhdo.app.R;
import com.muhdo.app.fragment.v3.HealthInsightResultsFragment;
import com.muhdo.app.fragment.v3.health_insights.HealthInsightDetailsFragment;
import com.muhdo.app.model.RecyclerData;

import java.util.ArrayList;

public class ModeSelectionAdapter extends RecyclerView.Adapter<ModeSelectionAdapter.MyViewHolder> {
    private ArrayList<RecyclerData> mList;
    public FragmentActivity mContext;

    /**
     * View holder class
     */
    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView header, tv_coming_soon;
        public ImageView image,imgNext;
        View relativeLayout;


        MyViewHolder(View view) {
            super(view);
            header = view.findViewById(R.id.txt_result_title);
            image = view.findViewById(R.id.img_results);
            imgNext = view.findViewById(R.id.img_next);
            tv_coming_soon = view.findViewById(R.id.tv_coming_soon);
            relativeLayout = view.findViewById(R.id.relative_layout);
        }
    }

    public ModeSelectionAdapter(ArrayList<RecyclerData> mList, FragmentActivity mContext) {
        this.mList = mList;
        this.mContext = mContext;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        RecyclerData recyclerData = mList.get(position);
        holder.header.setText(recyclerData.getHeader());
        holder.image.setImageResource(recyclerData.getImage());
        holder.image.setAlpha(.9f);
        if (position == 11 || position == 10 || position == 9) {
            holder.tv_coming_soon.setVisibility(View.VISIBLE);
            holder.imgNext.setVisibility(View.GONE);
        } else {
            holder.tv_coming_soon.setVisibility(View.GONE);
            holder.imgNext.setVisibility(View.VISIBLE);
        }
        /*if (position == 0 || position == 2 || position == 4 || position == 6 || position == 8 || position == 10) {
            holder.image.setColorFilter(ContextCompat.getColor(mContext, R.color.tint_health_insight_item1), PorterDuff.Mode.SRC_IN);
        } else {
            holder.image.setColorFilter(ContextCompat.getColor(mContext, R.color.tint_health_insight_item2), android.graphics.PorterDuff.Mode.SRC_IN);
        }*/
        holder.relativeLayout.setOnClickListener(v -> {
            if (position == 11 || position ==10 || position == 9) {
                // Message to be added
            } else {
                Log.d("data", "title adapter : " + mList.get(position));
                HealthInsightDetailsFragment fragment = new HealthInsightDetailsFragment();
                fragment.selectedTabPosition(position);
                replaceFragment(fragment);
            }


        });
    }


    @SuppressLint("WrongConstant")
    private void replaceFragment(Fragment fragment) {
        FragmentTransaction transaction = mContext.getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.flContent, fragment);
        transaction.addToBackStack("share");
        transaction.commit();

    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_mode_section, parent, false);
        return new MyViewHolder(v);
    }
}
