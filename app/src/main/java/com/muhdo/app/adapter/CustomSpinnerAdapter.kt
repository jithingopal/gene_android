package com.muhdo.app.adapter

import android.content.Context
import android.content.res.ColorStateList
import android.graphics.Color
import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.muhdo.app.R
import com.muhdo.app.utils.v3.TempUtil


class CustomSpinnerAdapter(val context: Context, private var listItemsTxt: MutableList<String>) :
    BaseAdapter() {


    private val mInflater: LayoutInflater = LayoutInflater.from(context)

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val view: View
        val vh: ItemRowHolder
        if (convertView == null) {
            view = mInflater.inflate(R.layout.item_spinner, parent, false)
            vh = ItemRowHolder(view)
            view?.tag = vh
        } else {
            view = convertView
            vh = view.tag as ItemRowHolder
        }
        vh.label.text = listItemsTxt[position]
        vh.label.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.drop_dwon, 0)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            vh.label.compoundDrawableTintList = ColorStateList.valueOf(Color.WHITE)
            vh.label.compoundDrawablePadding = 10
        }
        /* if (position == TempUtil.getLastSelectedSpinnerPosition()) {
             view.visibility = View.GONE
         } else {
             view.visibility = View.VISIBLE

         }*/
        return view
    }

    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val view: View
        val vh: ItemRowHolder
        if (convertView == null) {
            view = mInflater.inflate(R.layout.item_spinner, parent, false)
            vh = ItemRowHolder(view)
            view?.tag = vh
        } else {
            view = convertView
            vh = view.tag as ItemRowHolder
        }
        vh.label.text = listItemsTxt[position]
       /* parent?.let {
            val drawable = ContextCompat.getDrawable(it.context, R.drawable.drop_dwon)
            if (position == TempUtil.getLastSelectedSpinnerPosition()) {
                vh.label.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.drop_dwon, 0)
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    vh.label.compoundDrawableTintList = ColorStateList.valueOf(Color.WHITE)
                    vh.label.compoundDrawablePadding = 10
                }
            }else{
                vh.label.setCompoundDrawablesWithIntrinsicBounds(0, 0,0, 0)
            }
        }*/
        return view
    }

    override fun getItem(position: Int): Any? {

        return null

    }

    override fun getItemId(position: Int): Long {

        return 0

    }

    override fun getCount(): Int {
        return listItemsTxt.size
    }

    private class ItemRowHolder(row: View?) {

        val label: TextView = row?.findViewById(R.id.txtDropDownLabel) as TextView
    }
}