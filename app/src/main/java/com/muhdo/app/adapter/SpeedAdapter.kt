package com.muhdo.app.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.muhdo.app.R
import com.muhdo.app.apiModel.keyData.Indicator
import com.muhdo.app.fragment.KeyDataFragment
import com.timingsystemkotlin.backuptimingsystem.Utility
import kotlinx.android.synthetic.main.item_speedview.view.*

class SpeedAdapter(
    private val context: FragmentActivity?,
    var list: List<Indicator>, private val fragment: KeyDataFragment
) :
    RecyclerView.Adapter<SpeedAdapter.BindHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BindHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.item_speedview, parent, false)
        return BindHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: BindHolder, position: Int) {
        holder.bindItems(list[position])
    }

    inner class BindHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindItems(speed: Indicator) {
            itemView.text_header.text = speed.getTitle()
            val text = Utility.toTitleCase(speed.getResult()!!)
            itemView.text_footer.text = text

            Log.d("data","speed"+speed.getPercent()!!.toFloat());
         /*   if(speed.getPercent()!!.toFloat()>40.0){
                itemView.speedView.highSpeedColor=Color.RED
                itemView.speedView.mediumSpeedColor=Color.BLACK
                itemView.speedView.lowSpeedColor=Color.BLUE

            }else{
                itemView.speedView.highSpeedColor=Color.RED
                itemView.speedView.mediumSpeedColor=Color.BLACK
                itemView.speedView.lowSpeedColor=Color.BLUE
            }*/
            itemView.speedView.speedTo(speed.getPercent()!!.toFloat(), 0)
            Log.d("data","adapter categoryId = "+speed.getCategoryId())
            Log.d("data","adapter sectionId = "+speed.getSectionId())
            Log.d("data","adapter modeId = "+speed.getModeId())

            itemView.setOnClickListener {
                fragment.getCategoryData(speed.getCategoryId()!!, speed.getSectionId()!!, speed.getModeId()!!)
            }
        }
    }


}