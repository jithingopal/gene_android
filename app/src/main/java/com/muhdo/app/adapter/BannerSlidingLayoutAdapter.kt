package com.muhdo.app.adapter

import androidx.fragment.app.FragmentPagerAdapter
import java.util.ArrayList

class BannerSlidingLayoutAdapter(manager: androidx.fragment.app.FragmentManager) : FragmentPagerAdapter(manager) {

    private val mFragmentList = ArrayList<androidx.fragment.app.Fragment>()

    override fun getItem(position: Int): androidx.fragment.app.Fragment {

        return mFragmentList[position]
    }


    override fun getCount(): Int {
        return mFragmentList.size
    }

    fun addFragment(fragment: androidx.fragment.app.Fragment) {
        mFragmentList.add(fragment)
    }


}