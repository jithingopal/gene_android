package com.muhdo.app.adapter.v3

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.muhdo.app.R
import com.muhdo.app.apiModel.v3.epigenetics.EpiSampleInfo
import com.muhdo.app.apiModel.v3.epigenetics.ScoreDataHome
import com.muhdo.app.fragment.v3.epigenetics.EpigeneticsDetailsFragment
import com.muhdo.app.utils.v3.ItemAnimation
import org.jetbrains.anko.toast

class EpigeneticOverviewRecyAdpter(
    internal var context: FragmentActivity,
    private var list: List<ScoreDataHome>,private var sampleInfoList: EpiSampleInfo?
) : RecyclerView.Adapter<EpigeneticOverviewRecyAdpter.Holder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        val itemview = LayoutInflater.from(context.applicationContext)
            .inflate(R.layout.v3_item_epi_genetic_overview, parent, false)
        return Holder(itemview)
    }

    private var biologicalAge: Float = 0F

    fun updatetList(list: List<ScoreDataHome>, biologicalAge: Float, sampleInfo: EpiSampleInfo?) {
        this.list = list
        lastPosition = -1
        this.biologicalAge = biologicalAge
        this.sampleInfoList = sampleInfo

        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        if (this.list.isNotEmpty()) {
            /*if (position != 0 && position != 4) {
                holder.tvTitle.setText(
                    list.get(position).trackingTitle?.replace(
                        "_Score",
                        " Age"
                    )?.replace("_", " ")
                )
                val score:Float?=list.get(position).data?.scoreData?.score?.toFloat()
                holder.tvScore.setText((score?.plus(biologicalAge)).toString())
            }else{
                  }*/
            holder.tvTitle.setText(
                list.get(position).title?.replace("_", " ")
            )
            holder.tvScore.setText(list.get(position).score.toString())


            /* epigeneticsDetailsFragment.setEnterTransition(android.R.anim.bounce_interpolator Slide(Gravity.BOTTOM));
             epigeneticsDetailsFragment.setExitTransition( Slide(Gravity.RIGHT))*/;
            holder.rootLayout.setOnClickListener {
                val epigeneticsDetailsFragment = EpigeneticsDetailsFragment()
                epigeneticsDetailsFragment.biologicalAge = biologicalAge
                if (sampleInfoList != null) {
                    epigeneticsDetailsFragment.setEpiGeneticSampleList(sampleInfoList!!)
                }else{
                    context.toast(R.string.v3_no_chart_info_found)
                    return@setOnClickListener
                }
                epigeneticsDetailsFragment.setTabSelection(position)
                replaceFragment(epigeneticsDetailsFragment)
            }
            val sliderColor: String? = list.get(position).color

            if (!sliderColor!!.isEmpty()) {
                if (sliderColor.equals("Yellow")) {
                    holder.imgIndicator.setImageResource(R.drawable.v3_ic_slider_yellow)
                    holder.tvStatus.setText(
                        context.resources.getString(R.string.status) + " " + context.resources.getString(
                            R.string.normal
                        )
                    )
                } else if (sliderColor.equals("Red")) {
                    holder.imgIndicator.setImageResource(R.drawable.v3_ic_slider_red)
                    holder.tvStatus.setText(
                        context.resources.getString(R.string.status) + " " + context.resources.getString(
                            R.string.unhealthy
                        )
                    )
                } else if (sliderColor.equals("Green")) {
                    holder.imgIndicator.setImageResource(R.drawable.v3_ic_slider_green)
                    holder.tvStatus.setText(
                        context.resources.getString(R.string.status) + " " + context.resources.getString(
                            R.string.v3_healthy
                        )
                    )
                }
            }

            when (position) {
                0 -> {
                    holder.imgIcon.setImageResource(R.drawable.v3_ic_biological_age)
                }
                1 -> {
                    holder.imgIcon.setImageResource((R.drawable.v3_ic_eye_health))

                }
                2 -> {
                    holder.imgIcon.setImageResource((R.drawable.v3_ic_hearing))

                }
                3 -> {
                    holder.imgIcon.setImageResource((R.drawable.v3_ic_mental_health))

                }
                4 -> {
                    holder.imgIcon.setImageResource((R.drawable.v3_ic_inflammation))

                }
            }

            setAnimation(holder.itemView, position)
        }
    }

    private fun replaceFragment(fragment: Fragment) {
        val transaction = context.supportFragmentManager.beginTransaction()
        transaction.replace(R.id.flContent, fragment)
        transaction.addToBackStack("share")

        transaction.commit()

    }

    inner class Holder(view: View) : RecyclerView.ViewHolder(view) {
        internal var tvTitle: TextView = view.findViewById(R.id.tvTitle)
        internal var tvStatus: TextView = view.findViewById(R.id.tvStatus)
        internal var tvScore: TextView = view.findViewById(R.id.tvScore)
        internal var imgIcon: ImageView = view.findViewById(R.id.imgIcon)
        internal var imgIndicator: ImageView = view.findViewById(R.id.imgIndicator)
        internal var rootLayout: View = view.findViewById(R.id.root_layout)
    }

    private var lastPosition = -1
    private var on_attach = true

    private fun setAnimation(view: View, position: Int) {
        if (position > lastPosition) {
            ItemAnimation.animate(view, if (on_attach) position else -1, ItemAnimation.BOTTOM_UP)
            lastPosition = position
        }
    }
}