package com.muhdo.app.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.muhdo.app.R
import com.muhdo.app.databinding.ItemIngredientBinding
import org.json.JSONArray
import org.json.JSONObject

class IngredientAdapter(
    internal var context: Context,
    private val list: JSONArray
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        val binding = DataBindingUtil.inflate<ItemIngredientBinding>(
            LayoutInflater.from(parent.context),
            R.layout.item_ingredient,
            parent,
            false
        )
        return IngredientAdapter.ViewHolder(binding)

    }


    override fun getItemCount(): Int {
        return list.length()
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        val viewHolder = holder as ViewHolder
        val json = JSONObject(list.get(position).toString())
        viewHolder.binding!!.txtIngredient.text = json.getString("measurement") + " " +
                json.getString("ingredient")

    }

    class ViewHolder(binding: ItemIngredientBinding) : RecyclerView.ViewHolder(binding.root) {
        var binding: ItemIngredientBinding? = null

        init {
            this.binding = DataBindingUtil.bind(binding.root)
        }
    }
}
