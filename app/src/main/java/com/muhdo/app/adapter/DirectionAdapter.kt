package com.muhdo.app.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.muhdo.app.R
import com.muhdo.app.databinding.ListDirectionBinding
import org.json.JSONArray
import org.json.JSONObject

class DirectionAdapter(internal var context: Context, private val list: JSONArray) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        val binding = DataBindingUtil.inflate<ListDirectionBinding>(
            LayoutInflater.from(parent.context),
            R.layout.list_direction,
            parent,
            false
        )
        return DirectionAdapter.ViewHolder(binding)

    }


    override fun getItemCount(): Int {
        return list.length()
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val viewHolder = holder as ViewHolder
        viewHolder.binding!!.txtDirection.text = (position + 1).toString()
        val json = JSONObject(list.get(position).toString())
        viewHolder.binding!!.txtDirectionContain.text = json.getString("value")
    }

    class ViewHolder(binding: ListDirectionBinding) : RecyclerView.ViewHolder(binding.root) {
        var binding: ListDirectionBinding? = null

        init {
            this.binding = DataBindingUtil.bind(binding.root)
        }
    }
}
