package com.muhdo.app.adapter

import android.content.Context
import android.graphics.Color
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.muhdo.app.R
import com.muhdo.app.interfaces.ItemClickListener
import com.muhdo.app.model.ResultGridModel
import kotlinx.android.synthetic.main.list_results.view.*
import org.jetbrains.anko.toast


class ResultAdapter(context: Context, private var resultList: ArrayList<ResultGridModel>, private val listener: ItemClickListener) : BaseAdapter() {
    var context: Context? = context

    override fun getCount(): Int {
        return resultList.size
    }

    override fun getItem(position: Int): Any {
        return resultList[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val food = this.resultList[position]

        val inflator = context!!.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val foodView = inflator.inflate(R.layout.list_results, null)

        foodView.img_results.setImageResource(food.image!!)
        Log.wtf("data", "plan" + food.name)
        /*if (food.name.equals(context!!.getString(R.string.health_insights))) {
            foodView.txt_result_title.setTextColor(Color.parseColor("#000000"))
        }*/
        foodView.txt_result_title.text = food.name!!
        if(position==2){
            foodView.findViewById<View>(R.id.viewOverlay).visibility= View.VISIBLE
            foodView.img_results.setOnClickListener {
                context?.toast("This feature is not available right now")
            }
        }else
        {
            foodView.img_results.setOnClickListener {
                //            when (food.name) {
//                context!!.resources.getString(R.string.result_my_meal) -> {
                listener.onClick(position)
//                }
//            }
            }
        }

        return foodView
    }

    class ResultAdapter(
        context: Context,
        private var resultList: ArrayList<ResultGridModel>,
        private val listener: ItemClickListener
    ) : RecyclerView.Adapter<ResultAdapter.Holder>() {
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
            val itemView = LayoutInflater.from(context).inflate(R.layout.list_results, parent, false)
            return Holder(itemView)
        }

        override fun getItemCount(): Int {
            return resultList.size
        }

        override fun onBindViewHolder(holder: Holder, position: Int) {
            val food = this.resultList[position]
            holder.imgResults.setImageResource(food.image!!)
            holder.txtResultTitle.text = food.name!!
            if (food.name!! == "News Feed") {
                holder.txtResultTitle.setTextColor(Color.parseColor("#003552"))
            }
            holder.imgResults.setOnClickListener {

                listener.onClick(position)
            }
        }


        inner class Holder(view: View) : RecyclerView.ViewHolder(view) {
            internal var imgResults: ImageView = view.findViewById(R.id.img_results)
            internal var txtResultTitle: TextView = view.findViewById(R.id.txt_result_title)
        }


        var context: Context? = context


    }
}