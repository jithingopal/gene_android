package com.muhdo.app.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.muhdo.app.R
import com.muhdo.app.databinding.ListBreakfastBinding
import com.muhdo.app.fragment.MealFragment
import com.timingsystemkotlin.backuptimingsystem.Utility
import org.json.JSONArray
import org.json.JSONObject

class Meal3Adapter(
    internal var context: Context,
    private val list: JSONArray,
    private var mealFragment: MealFragment,
    var activityPosition: Int,
    private var weekDay:String
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    var selectedPosition = -1
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        val binding = DataBindingUtil.inflate<ListBreakfastBinding>(
            LayoutInflater.from(parent.context),
            R.layout.list_breakfast,
            parent,
            false
        )
        return Meal3Adapter.ViewHolder(binding)

    }


    override fun getItemCount(): Int {
        return list.length()
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        val viewHolder = holder as ViewHolder
       val json = JSONObject(list.get(position).toString())
        viewHolder.binding!!.txtRecipeName.text = json.getString("name")
        if (json.getBoolean("is_favourite") || Utility.favouriteIdList.contains(json.getString("_id"))) {
            viewHolder.binding!!.checkFavourite.isChecked = true
        }
        if(Utility.unFavouriteIdList.contains(json.getString("_id"))){
            viewHolder.binding!!.checkFavourite.isChecked = false
        }
        if(json.has("bucket"))
        {
              Glide
            .with(context)
            .load("https://s3.amazonaws.com/" + json.getString("bucket") + "/" + json.getString("image"))
            .centerCrop()
            .placeholder(R.drawable.diet_1)
            .into(viewHolder.binding!!.imgRecipe)

        }   else{
            Glide
                .with(context)
                .load("https://s3.amazonaws.com/" + "dev-recipe-images" + "/" + json.getString("image"))
                .centerCrop()
                .placeholder(R.drawable.diet_1)
                .into(viewHolder.binding!!.imgRecipe)
        }
        if (selectedPosition == -1 && Utility.idList.contains(json.getString("_id"))) {
            viewHolder.binding!!.cardView.setCardBackgroundColor(ContextCompat.getColor(context, R.color.primary_error_color))
            viewHolder.binding!!.txtRecipeName.setTextColor(ContextCompat.getColor(context, R.color.white))
        } else {
            viewHolder.binding!!.cardView.setCardBackgroundColor(ContextCompat.getColor(context, R.color.white))
            viewHolder.binding!!.txtRecipeName.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary))
        }


        if (position == activityPosition) {
            viewHolder.binding!!.cardView.setCardBackgroundColor(ContextCompat.getColor(context, R.color.primary_error_color))
            viewHolder.binding!!.txtRecipeName.setTextColor(ContextCompat.getColor(context, R.color.white))
        }
        viewHolder.binding!!.recipeLayout.setOnClickListener {
            selectedPosition = position

            if (selectedPosition == position) {

                    mealFragment.getRecipeData3(json.getString("_id"), position, weekDay, "Meal3")

                viewHolder.binding!!.cardView.setCardBackgroundColor(ContextCompat.getColor(context, R.color.primary_error_color))
                viewHolder.binding!!.txtRecipeName.setTextColor(ContextCompat.getColor(context, R.color.white))
            }
        }

        viewHolder.binding!!.checkFavourite.setOnClickListener {
            val sts: Boolean
            viewHolder.binding!!.checkFavourite.isClickable = false
            if(!Utility.unFavouriteIdList.contains(json.getString("_id"))){
                if(json.getBoolean("is_favourite") || Utility.favouriteIdList.contains(json.getString("_id"))){
                    sts = false
                    viewHolder.binding!!.checkFavourite.isChecked = false
                }else{
                    sts = true
                    viewHolder.binding!!.checkFavourite.isChecked = true
                }
            }
            else{

                if(Utility.favouriteIdList.contains(json.getString("_id"))){
                    sts = false
                    viewHolder.binding!!.checkFavourite.isChecked = false
                }else{
                    sts = true
                    viewHolder.binding!!.checkFavourite.isChecked = true
                }
            }

            mealFragment.updateFavorite(json.getString("_id"),viewHolder.binding!!.checkFavourite, sts)
        }

    }

    class ViewHolder(binding: ListBreakfastBinding) : RecyclerView.ViewHolder(binding.root) {
        var binding: ListBreakfastBinding? = null

        init {
            this.binding = DataBindingUtil.bind(binding.root)
        }
    }
}
