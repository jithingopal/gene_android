package com.muhdo.app.adapter.v3

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.muhdo.app.R
import com.muhdo.app.apiModel.mealModel.Recipe
import com.muhdo.app.apiModel.mealModel.daysModel.Monday
import com.muhdo.app.interfaces.v3.OnMealChoosedInChildListener
import com.muhdo.app.interfaces.v3.OnMealChoosedInParentListener
import com.muhdo.app.utils.v3.TempUtil

class ChooseMealPlanParentRecyAdpter(
    internal var context: FragmentActivity,
    private var list: List<List<Monday>>?,
    private val dayPosition: Int
) : RecyclerView.Adapter<ChooseMealPlanParentRecyAdpter.Holder>(), OnMealChoosedInChildListener {

    var listData: MutableList<Recipe> = ArrayList<Recipe>(0)
    override fun onMealChoosed(recipe: Recipe, mealPosition: Int) {
        TempUtil.log("ChooseMealPlanParent", "mealPosition" + mealPosition+"\n Recipe Name:"+recipe.getRecipeId()+"\n Day:"+recipe.getDay())


       if ((listData.size) > mealPosition) {
           listData.removeAt (mealPosition)
           listData.add (mealPosition, recipe)
        } else {
            listData.add(recipe)
        }
        TempUtil.log(
            "ChooseMealPlanParent",
            "listData Size: " + listData.size + "\n Recipe:" + recipe.getRecipeId()
        )

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        val itemview = LayoutInflater.from(context)
            .inflate(R.layout.item_choose_meal_for_day_parent, parent, false)
        return Holder(itemview)
    }


    override fun getItemCount(): Int {
        if (list == null) {
            return 0
        } else {
            return list!!.size
        }
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        if (this.list != null && this.list!!.isNotEmpty()) {
            holder.recyViewChild.layoutManager =
                LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
          holder.tvMealTitle.setText(context.getString(R.string.meal)+" "+(position+1))
            if (list?.get(position) != null) {
                var mAdapter: ChooseMealPlanChildRecyAdpter =
                    ChooseMealPlanChildRecyAdpter(
                        context!!,
                        list?.get(position)!!,
                        dayPosition,position,
                        this
                    )
//                mAdapter.onMealChoosedInChildListener=this
                /*holder.recyViewChild.addItemDecoration(
                    DividerItemDecoration(
                        holder.recyViewChild.getContext(),
                        DividerItemDecoration.VERTICAL
                    )
                );*/

                holder.recyViewChild.adapter = mAdapter
                holder.recyViewChild.adapter?.notifyDataSetChanged()
            }
        }
    }

    inner class Holder(view: View) : RecyclerView.ViewHolder(view) {
        internal var recyViewChild: RecyclerView =
            view.findViewById(R.id.recycler_meal_plan_for_day_child)


        internal var tvMealTitle:TextView = view.findViewById(R.id.tvMealTitle)
        internal var view: View = view.findViewById(R.id.root_layout)
    }

    fun getSelectedRecipe(): MutableList<Recipe> {
        TempUtil.log("ChooseMealPlanParent", "getSelectedRecipe size " + listData?.size)
        return listData
    }
}