package com.muhdo.app.adapter.v3

import android.app.Activity
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.muhdo.app.R
import com.muhdo.app.apiModel.v3.GeneticOverviewResultData
import com.muhdo.app.apiModel.v3.workoutplan.workout_overview.WorkoutDataValues
import com.muhdo.app.apiModel.v3.workoutplan.workout_overview.WorkoutOverviewData
import android.R.attr.translationY
import android.animation.Animator
import android.animation.AnimatorListenerAdapter


class WorkoutOverviewChildRecyAdpter(
    internal var context: FragmentActivity,
    private var list: MutableList<WorkoutDataValues>
) : RecyclerView.Adapter<WorkoutOverviewChildRecyAdpter.Holder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        val itemview = LayoutInflater.from(context)
            .inflate(R.layout.item_workout_overview_child, parent, false)
        return Holder(itemview)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        if (this.list.isNotEmpty()) {
            holder.tvTitle.setText(list.get(position).title)
            holder.tvDescription.setText(list.get(position).description)
            if (list.get(position).subTitle != null && (list.get(position).subTitle!!.size > 0)) {

                for ((index, subtitle)  in list.get(position).subTitle!!.withIndex()) {
                    if(index!=0){
                        holder.tvSubtitle.append("\n" )
                    }
                    holder.tvSubtitle.append(subtitle )

                }
            } else {
                holder.tvSubtitle.visibility = View.GONE
            }
            if (list.get(position).content != null && (list.get(position).content!!.size > 0)) {

                for ((index,content)  in list.get(position).content!!.withIndex()) {
                    if(index!=0){
                        holder.tvContent.append("\n" )
                    }
                    holder.tvContent.append(content + "\n")

                }
            } else {
                holder.tvContent.visibility = View.GONE
            }

            holder.tvTitle.setOnClickListener {
                if (holder.popup.visibility != View.VISIBLE) {
                    holder.popup.visibility = View.VISIBLE
                    holder.tvTitle.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_up_arrow, 0);
                }else{
                    holder.popup.visibility = View.GONE
                    holder.tvTitle.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_down_arrow, 0);
                }
            }

        }
    }

    inner class Holder(view: View) : RecyclerView.ViewHolder(view) {
        internal var tvTitle: TextView = view.findViewById(R.id.tv_title)
        internal var tvDescription: TextView = view.findViewById(R.id.tv_description)
        internal var tvSubtitle: TextView = view.findViewById(R.id.tv_SubTitle)
        internal var tvContent: TextView = view.findViewById(R.id.tv_content)
        internal var popup: CardView = view.findViewById(R.id.card_view_popup)


        internal var view: View = view.findViewById(R.id.root_layout)
    }

}