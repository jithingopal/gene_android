package com.muhdo.app.adapter

import android.app.Activity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.muhdo.app.AppApplication
import com.muhdo.app.R
import com.muhdo.app.databinding.V3FragmentNoWorkoutPlanBinding
import com.muhdo.app.fragment.FavouriteWorkoutFragment
import com.muhdo.app.fragment.MyPlanFragment
import com.muhdo.app.fragment.v3.InjuryPreventionFragment
import com.muhdo.app.fragment.v3.MyProfileFragment
import com.muhdo.app.fragment.v3.WarmUpCoolDownFragment
import com.muhdo.app.fragment.v3.guide.workout.NoWorkoutPlanFoundFragment
import com.muhdo.app.fragment.v3.guide.workout.WorkoutOverviewFragment
import com.timingsystemkotlin.backuptimingsystem.Utility

class MyPagerAdapter(fm: FragmentManager) : FragmentStatePagerAdapter(fm) {
    lateinit  var activity:FragmentActivity
    var tab_label =
        arrayOf<Int>(
            R.string.v3_my_plan, R.string.v3_overview, R.string.v3_my_profile,
            R.string.v3_my_exercise_menu_warmup,
            R.string.v3_my_exercise_menu_injury_prevention
        )

    override fun getItem(position: Int): Fragment {


        return when (position) {
            0 -> {
                if (Utility.getWorkoutStatus(activity.applicationContext) ){
                    MyPlanFragment()
                }else
                {
                      NoWorkoutPlanFoundFragment()
                }

            }
            1 -> {
                WorkoutOverviewFragment()
            } 2 -> {
                MyProfileFragment()
            } 3 -> {
                WarmUpCoolDownFragment()
            } 4 -> {
                InjuryPreventionFragment()
            }else->{
                FavouriteWorkoutFragment()

            }
        }
    }

    override fun getCount(): Int {
        return 5
    }

    override fun getPageTitle(position: Int): CharSequence {
        return when (position) {
            0 -> "My Plan"
            1 -> "Overview"
            2 -> "My Profile"
            3 -> "Warm Up/Cool Down"
            4 -> "Injury Prevention"
            else -> "Favourites"
        }
    }
}