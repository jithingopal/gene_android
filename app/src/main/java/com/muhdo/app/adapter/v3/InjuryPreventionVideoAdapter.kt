package com.muhdo.app.adapter.v3

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseExpandableListAdapter
import android.widget.CheckBox
import android.widget.TextView
import androidx.appcompat.widget.AppCompatCheckBox
import com.ct7ct7ct7.androidvimeoplayer.view.VimeoPlayerView
import com.muhdo.app.R
import com.muhdo.app.apiModel.v3.workoutplan.Data
import com.muhdo.app.utils.v3.TempUtil

class InjuryPreventionVideoAdapter
internal constructor(
    private val context: Context,
    private var list: List<Data>?
) : BaseExpandableListAdapter() {
    override fun getChild(listPosition: Int, expandedListPosition: Int): Any {
        return this.list!![listPosition]
    }

    override fun getChildId(listPosition: Int, expandedListPosition: Int): Long {
        return expandedListPosition.toLong()
    }

    override fun getChildView(
        listPosition: Int,
        expandedListPosition: Int,
        isLastChild: Boolean,
        convertView: View?,
        parent: ViewGroup
    ): View {
        var convertView = convertView
        if (convertView == null) {
            val layoutInflater =
                context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            convertView = layoutInflater.inflate(R.layout.workout_child_layout, null)
        }
        val videoView = convertView!!.findViewById<VimeoPlayerView>(R.id.vimeoPlayer)
        //  (context as Activity).lifecycle.addObserver(videoView)

        convertView.findViewById<AppCompatCheckBox>(R.id.check_favourite).visibility =
            View.INVISIBLE

        val link = list!![listPosition].video_link
        if (link != "" && link.contains("player.vimeo.com/video/") || link.contains("vimeo.com/")) {
            var link1 = link.replace("https://player.vimeo.com/video/", "")
            link1 = link1.replace("https://vimeo.com/", "")
            System.out.println("link1====>  $link1")
            videoView.initialize(link1.toInt(), "https://vimeo.com/")
            // videoView.initialize(link1.toInt())
        }
        //   videoView.initialize(link.toInt())
        val checkFavourite = convertView.findViewById<CheckBox>(R.id.check_favourite)
        val workoutInstruction = convertView.findViewById<TextView>(R.id.txt_workout_instruction)
        //    workoutInstruction.text = list!![listPosition].exercise_description
        workoutInstruction.text =
            list!![listPosition].exercise_description.trim().replace(Regex("(\\s)+"), " ")


        checkFavourite.setOnClickListener {
            val sts: Boolean
            checkFavourite.isClickable = false

        }

        val expandedListTextView = convertView.findViewById<TextView>(R.id.txt_recipe_name)
        var workoutText = ""
        list?.let {
            if (it[listPosition].sets.isNotEmpty()) {
                if (it[listPosition].sets != "0" || it[listPosition].sets != "0.0") {
                    workoutText = workoutText.plus(it[listPosition].sets + " Sets | ")
                }
            }
            if (it[listPosition].repetition.isNotEmpty()) {
                if (it[listPosition].repetition != "0" || it[listPosition].repetition != "0.0") {
                    if (it[listPosition].repetition.contains(
                            "secs",
                            true
                        ) || it[listPosition].repetition.contains("per", true)
                    ) {
                   //     workoutText = workoutText.replace("reps", "")
                        workoutText = workoutText.plus(list!![listPosition].repetition.replace("reps", "") + " | ")
                    } else {
                      //  workoutText = workoutText.replace("reps", "")
                        workoutText =
                            workoutText.plus(list!![listPosition].repetition.replace("reps", "") + " Reps | ")
                    }
                }
            }
            if (it[listPosition].rest.isNotEmpty()) {
                if (it[listPosition].rest != "0" || it[listPosition].rest != "0.0") {
                    workoutText = workoutText.plus(list!![listPosition].rest + " Rest")
                }
            }
        }
        /*   if ((list!![listPosition].rest == "0" ||
                       list!![listPosition].rest == "0.0")
           ) {
               workoutText = "" + list!![listPosition].sets + " Sets | " +
                       list!![listPosition].repetition + " Reps | "
           } *//*else if (list!![listPosition].getEquipmentWeight() == 0) {
            workoutText = "" + list!![listPosition].sets + " Sets | " +
                    list!![listPosition].repetition + " Reps | " +
                    list!![listPosition].rest + " Rest"
        }*//* else if ((list!![listPosition].rest == "0" ||
                    list!![listPosition].rest == "0.0")
        ) {
            workoutText = "" + list!![listPosition].sets + " Sets | " +
                    list!![listPosition].repetition + " Reps | "
            //  list!![listPosition].getEquipmentWeight() + " Kg | "

        } else {
            workoutText = "" + list!![listPosition].sets + " Sets | " +
                    list!![listPosition].repetition + " Reps | " +
                    //   list!![listPosition].getEquipmentWeight() + " Kg | " +
                    list!![listPosition].rest + " Rest"
        }*/

        expandedListTextView.text = workoutText
        return convertView
    }

    override fun getChildrenCount(listPosition: Int): Int {
        return 1
    }

    override fun getGroup(listPosition: Int): Any {
        return this.list!![listPosition]
    }

    override fun getGroupCount(): Int {
        return this.list!!.size
    }

    override fun getGroupId(listPosition: Int): Long {
        return listPosition.toLong()
    }

    @SuppressLint("SetTextI18n")
    override fun getGroupView(
        listPosition: Int,
        isExpanded: Boolean,
        convertView: View?,
        parent: ViewGroup
    ): View {
        var convertView = convertView
        if (convertView == null) {
            val layoutInflater =
                context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            convertView = layoutInflater.inflate(R.layout.workout_parent_layout, null)
        }
        val listTitleTextView = convertView!!.findViewById<TextView>(R.id.txt_workout_name)
        listTitleTextView.text = list!![listPosition].exercise_name
        val txtWorkoutSteps = convertView.findViewById<TextView>(R.id.txt_workout_steps)
        txtWorkoutSteps.text =
            "" + list!![listPosition].sets + "x" + list!![listPosition].repetition
        return convertView
    }

    override fun hasStableIds(): Boolean {
        return false
    }

    override fun isChildSelectable(listPosition: Int, expandedListPosition: Int): Boolean {

        TempUtil.log("data", "expan " + listPosition + " " + expandedListPosition)
        return true
    }
}