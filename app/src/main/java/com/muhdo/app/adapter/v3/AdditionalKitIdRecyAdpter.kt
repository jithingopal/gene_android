package com.muhdo.app.adapter.v3

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.muhdo.app.R
import com.muhdo.app.apiModel.v3.KitID
import com.muhdo.app.utils.v3.convertDateStringFormat

/*@author jithingopal@emvigotech.com*/

/*This class shows the data inside AdditionalKitIDListDialogFragment which is a popup dialog from KitIdInfoFragment*/
class AdditionalKitIdRecyAdpter(
    internal var context: FragmentActivity,
    private var list: MutableList<KitID>
) : RecyclerView.Adapter<AdditionalKitIdRecyAdpter.Holder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        val itemview = LayoutInflater.from(context)
            .inflate(R.layout.v3_item_additional_kit_id, parent, false)
        return Holder(itemview)
    }


    override fun getItemCount(): Int {
        if (list == null) {
            return 0
        } else {
            return list!!.size
        }
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        if (this.list != null && this.list!!.isNotEmpty()) {
            holder.tvKitID.setText(list[position].kit_id)
            //show UTC time format in another time format
            holder.tvKitDate.setText(
                convertDateStringFormat(
                    list[position].date_of_scanning,
                    "yyyy-mm-dd'T'HH:mm:ss.SSS'Z'",
                    "MMM dd, yyyy h:mm a"
                )
            )
            holder.tvKitName.setText(context.getString(R.string.v3_sample)+" "+(position+1))

        }
    }

    inner class Holder(view: View) : RecyclerView.ViewHolder(view) {
        internal var tvKitID: TextView = view.findViewById(R.id.tvKitID)
        internal var tvKitName: TextView = view.findViewById(R.id.tvKitName)
        internal var tvKitDate: TextView = view.findViewById(R.id.tvKitDate)
        internal var view: View = view.findViewById(R.id.root_layout)
    }


}