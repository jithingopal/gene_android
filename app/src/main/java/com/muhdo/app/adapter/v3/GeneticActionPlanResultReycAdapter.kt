package com.muhdo.app.adapter.v3

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.muhdo.app.R
import com.muhdo.app.apiModel.v3.GeneticActionPlanResultData


class GeneticActionPlanResultReycAdapter(
    internal var context: Context,
    private var fragment: Fragment,
    private var list: MutableList<GeneticActionPlanResultData>
) : RecyclerView.Adapter<GeneticActionPlanResultReycAdapter.Holder>() {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): GeneticActionPlanResultReycAdapter.Holder {
        val itemview = LayoutInflater.from(context)
            .inflate(R.layout.item_genetic_action_plan_result, parent, false)

        return Holder(itemview)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(
        holder: GeneticActionPlanResultReycAdapter.Holder,
        position: Int
    ) {
        if (this.list.isNotEmpty()) {
            holder.tvTitle.setText(list.get(position).title)
            holder.tvDescription.setText(list.get(position).description)
            if (list.get(position).value == 0) {
                holder.imgIndicator.setImageResource(R.mipmap.slider_green)
            } else if (list.get(position).value == 1) {
                holder.imgIndicator.setImageResource(R.mipmap.slider_yellow)
            } else if (list.get(position).value == 2) {
                holder.imgIndicator.setImageResource(R.mipmap.slider_red)
            }
        }
    }


    inner class Holder(view: View) : RecyclerView.ViewHolder(view) {
        internal var tvTitle: TextView = view.findViewById(R.id.tv_title)
        internal var tvDescription: TextView = view.findViewById(R.id.tv_description)
        internal var imgIndicator: ImageView = view.findViewById(R.id.img_indicator)
    }


}