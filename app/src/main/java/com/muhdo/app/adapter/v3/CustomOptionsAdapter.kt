package com.muhdo.app.adapter.v3

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.muhdo.app.R
import com.muhdo.app.apiModel.v3.HomeOptions
import kotlinx.android.synthetic.main.item_home_options.view.*

class CustomOptionsAdapter(
    private val optionsList: List<HomeOptions>,
    val clickListener: CustomOptionsClickListener
) :
    RecyclerView.Adapter<CustomOptionsAdapter.OptionViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OptionViewHolder {
        return OptionViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_home_options,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return optionsList.size
    }

    override fun onBindViewHolder(holder: OptionViewHolder, position: Int) {
        holder.bindViews(optionsList[position].imageRes, optionsList[position].title)
    }

    inner class OptionViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindViews(imageRes: Int, title: String) {
            itemView.tv_home_option.text = title
           itemView.iv_option_icon.setImageResource(imageRes)
            itemView.setOnClickListener {
                clickListener.onItemClick(adapterPosition, title)
            }
        }

    }

    interface CustomOptionsClickListener {

        fun onItemClick(position: Int, label: String)
    }

}