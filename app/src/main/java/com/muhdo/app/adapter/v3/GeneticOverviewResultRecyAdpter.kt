package com.muhdo.app.adapter.v3

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.muhdo.app.R
import com.muhdo.app.apiModel.v3.GeneticOverviewResultData

class GeneticOverviewResultRecyAdpter(
    internal var context: Context,
    private var fragment: Fragment,
    private var list: MutableList<GeneticOverviewResultData>
) : RecyclerView.Adapter<GeneticOverviewResultRecyAdpter.Holder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        val itemview = LayoutInflater.from(context).inflate(R.layout.item_genetic_overview_result, parent, false)
        return Holder(itemview)
    }

    override fun getItemCount(): Int {
      return  list.size
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        if (this.list.isNotEmpty()) {
            holder.tvTitle.setText( list.get(position).title)
            holder.tvDescription.setText(list.get(position).content)
        }
    }

    inner class Holder(view: View) : RecyclerView.ViewHolder(view) {
        internal var tvTitle: TextView = view.findViewById(R.id.tv_title)
        internal var tvDescription: TextView = view.findViewById(R.id.tv_description)
        internal var view: View = view.findViewById(R.id.root_layout)
    }

}