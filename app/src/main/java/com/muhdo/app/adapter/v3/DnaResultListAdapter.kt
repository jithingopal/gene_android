package com.muhdo.app.adapter.v3

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.muhdo.app.R
import com.muhdo.app.apiModel.keyData.Indicator
import com.muhdo.app.callback.KeyDataClickListener
import kotlinx.android.synthetic.main.item_dna_overview.view.*

class DnaResultListAdapter(
    private val clickListener: KeyDataClickListener
) :
    RecyclerView.Adapter<DnaResultListAdapter.CustomViewHolder>() {
    private var list = arrayListOf<Indicator>()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolder {
        return CustomViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_dna_overview,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {
        holder.bindView(list[position])
    }

    fun setData(list: List<Indicator>) {
        this.list.clear()
        this.list.addAll(list)
        notifyDataSetChanged()
    }

    inner class CustomViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindView(indicator: Indicator) {
          //  if (adapterPosition == 0)
        //        itemView.cl_dna_results_parent.setBackgroundResource(R.drawable.v3_bg_epigenetic_overview_selected)

            itemView.tv_overview_title.text = indicator.getTitle()
            itemView.setOnClickListener {
                itemView.cl_dna_results_parent.setBackgroundResource(R.drawable.v3_bg_epigenetic_overview_selected)
                clickListener.onHeaderItemChange(
                    list[adapterPosition]
                    /*   categoryId = list[adapterPosition].getCategoryId(),
                       modeId = list[adapterPosition].getModeId(),
                       sectionId = list[adapterPosition].getSectionId()*/
                )
            }
            itemView.tv_overview_status.text =
                itemView.context.resources.getString(R.string.status).plus(" ")
                    .plus(indicator.getResult())
            val degreeValue = indicator.getPercent()!!

            itemView.tv_overview_status.text =
                itemView.context.resources.getString(R.string.status).plus(" ")
                    .plus(indicator.getResult())
            if (degreeValue > 108 && degreeValue <= 144) {
                itemView.iv_overview_indicator.setImageResource(R.drawable.v3_ic_slider_half_red)

            } else if (degreeValue > 72 && degreeValue <= 108) {
                itemView.iv_overview_indicator.setImageResource(R.drawable.v3_ic_slider_yellow)

            } else if (degreeValue > 36 && degreeValue <= 72) {
                itemView.iv_overview_indicator.setImageResource(R.drawable.v3_ic_slider_half_green)

            } else if (degreeValue >= 0 && degreeValue <= 36) {
                itemView.iv_overview_indicator.setImageResource(R.drawable.v3_ic_slider_green)

            } else {
                itemView.iv_overview_indicator.setImageResource(R.drawable.v3_ic_slider_red)
            }

        }
    }

}