package com.muhdo.app.adapter.v3

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.muhdo.app.R
import com.muhdo.app.model.HomeItem
import kotlinx.android.synthetic.main.item_new_home.view.*

class HomeAdapter(
    private val homeItems: List<HomeItem>,
    private val clickListener: HomeAdapterClickListener
) :
    RecyclerView.Adapter<HomeAdapter.CustomViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolder {
        return CustomViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_new_home, parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return homeItems.size
    }

    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {
        holder.bindView(homeItems[position])
    }


    inner class CustomViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindView(homeItem: HomeItem) {
            itemView.tv_home_label.text = homeItem.label
            itemView.iv_home_icon.setImageResource(homeItem.imageUrl)
            itemView.setOnClickListener {
                clickListener.onItemClick(adapterPosition)
            }
        }
    }


    interface HomeAdapterClickListener {

        fun onItemClick(position: Int)
    }
}