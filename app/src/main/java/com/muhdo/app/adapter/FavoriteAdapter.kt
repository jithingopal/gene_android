package com.muhdo.app.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.muhdo.app.R
import com.muhdo.app.apiModel.mealModel.FavoriteListModelData
import com.muhdo.app.databinding.ListBreakfastBinding
import com.muhdo.app.ui.MyFavouriteActivity

class FavoriteAdapter(internal var context: Context, private val list: List<FavoriteListModelData>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        val binding = DataBindingUtil.inflate<ListBreakfastBinding>(
            LayoutInflater.from(parent.context),
            R.layout.list_breakfast,
            parent,
            false
        )

        return FavoriteAdapter.ViewHolder(binding)

    }


    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        val viewHolder = holder as ViewHolder
        val model = list[position]
        viewHolder.binding!!.txtRecipeName.text = model.getRecipeId()!!.getName()
        Glide
            .with(context)
            .load("https://s3.amazonaws.com/" + model.getRecipeId()!!.getBucket() + "/" + model.getRecipeId()!!.getImage())
            .centerCrop()
            .placeholder(R.drawable.diet_1)
            .into(viewHolder.binding!!.imgRecipe)
        if (model.getIsFavourite()!!) {
            viewHolder.binding!!.checkFavourite.setButtonDrawable(R.drawable.heart_like)
        } else {
            viewHolder.binding!!.checkFavourite.setButtonDrawable(R.drawable.heart_unlike)
        }
        viewHolder.binding!!.cardView.setOnClickListener {
            if (context is MyFavouriteActivity) {
                (context as MyFavouriteActivity).goToRecipe(model.getRecipeId()!!.getId()!!)
            } else {
                viewHolder.binding!!.cardView.setCardBackgroundColor(ContextCompat.getColor(context, R.color.primary_error_color))
                viewHolder.binding!!.cardView.setCardBackgroundColor(ContextCompat.getColor(context, R.color.primary_error_color))
            }
        }

    }

    class ViewHolder(binding: ListBreakfastBinding) : RecyclerView.ViewHolder(binding.root) {
        var binding: ListBreakfastBinding? = null

        init {
            this.binding = DataBindingUtil.bind(binding.root)
        }
    }

}
