package com.muhdo.app.adapter.v3

import android.app.Activity
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.muhdo.app.R
import com.muhdo.app.apiModel.v3.GeneticOverviewResultData
import com.muhdo.app.apiModel.v3.workoutplan.workout_overview.WorkoutOverviewData

class WorkoutOverviewParentRecyAdpter(
    internal var context: FragmentActivity,
    private var list: MutableList<WorkoutOverviewData>
) : RecyclerView.Adapter<WorkoutOverviewParentRecyAdpter.Holder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        val itemview = LayoutInflater.from(context).inflate(R.layout.item_workout_overview_parent, parent, false)
        return Holder(itemview)
    }

    override fun getItemCount(): Int {
      return  list.size
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        if (this.list.isNotEmpty()) {
            holder.tvTitle.setText( list.get(position).title)
            holder.tvDescription.setText(list.get(position).description)
            holder.recyViewChild.layoutManager =
                LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            if(list.get(position).values!=null) {
                var mAdapter: WorkoutOverviewChildRecyAdpter =
                    WorkoutOverviewChildRecyAdpter(context!!, list.get(position).values!!)
                /*holder.recyViewChild.addItemDecoration(
                    DividerItemDecoration(
                        holder.recyViewChild.getContext(),
                        DividerItemDecoration.VERTICAL
                    )
                );*/

                holder.recyViewChild.adapter = mAdapter
                holder.recyViewChild.adapter?.notifyDataSetChanged()
            }
        }
    }

    inner class Holder(view: View) : RecyclerView.ViewHolder(view) {
        internal var tvTitle: TextView = view.findViewById(R.id.tv_title)
        internal var tvDescription: TextView = view.findViewById(R.id.tv_description)
        internal var recyViewChild: RecyclerView = view.findViewById(R.id.recycler_workout_overview_child)



        internal var view: View = view.findViewById(R.id.root_layout)
    }

}