package com.muhdo.app.ui.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.PictureDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import androidx.recyclerview.widget.RecyclerView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.model.StreamEncoder;
//// import com.caverock.androidsvg.SVG;
//// import com.caverock.androidsvg.SVGParser;

import com.larvalabs.svgandroid.SVG;
import com.larvalabs.svgandroid.SVGParser;
import com.muhdo.app.R;
import com.muhdo.app.apiModel.result.SleepScore;
import com.muhdo.app.ui.ResultChooseMode;
import com.muhdo.app.utils.SvgDecoder;
import com.muhdo.app.utils.SvgDrawableTranscoder;
import com.muhdo.app.utils.SvgSoftwareLayerSetter;
import com.pixplicity.sharp.Sharp;
import okhttp3.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/*
import com.bumptech.glide.RequestBuilder;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.model.StreamEncoder;
// import com.bumptech.glide.load.resource.file.FileToStreamDecoder;
*/

public class FoodDetailAdapter1 extends RecyclerView.Adapter<FoodDetailAdapter1.MyViewHolder>{
    private List<SleepScore.Foods> foodDetailList1;
    public Activity mContext;


    //// private RequestBuilder<Uri, InputStream, SVG, PictureDrawable> requestBuilder;


    public FoodDetailAdapter1(@Nullable ArrayList<SleepScore.Foods> foodList1, @NotNull Activity mContext) {
        this.foodDetailList1 = foodList1;
        this.mContext =   mContext;
    }

    /**
     * View holder class
     */
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView imageViewFood;
        public TextView textViewFoodName, textViewFoodQuantity,textViewFoodQuantityGram;
        public MyViewHolder(View view) {
            super(view);
            imageViewFood = (ImageView) view.findViewById(R.id.imageView_Foods);
            textViewFoodName = (TextView) view.findViewById(R.id.textView_FoodName);
            textViewFoodQuantity = (TextView) view.findViewById(R.id.textView_FoodQuantity);
            textViewFoodQuantityGram = (TextView) view.findViewById(R.id.textView_FoodQuantityGram);
        }
    }

    @Override
    public void onBindViewHolder(final FoodDetailAdapter1.MyViewHolder holder, final int position) {
        SleepScore.Foods foods = foodDetailList1.get(position);
        holder.textViewFoodName.setText(foods.getFoodsName());
        holder.textViewFoodQuantity.setText(foods.getFoodsQtyUnit1());
        holder.textViewFoodQuantityGram.setText(foods.getFoodsQtyUnit2());
        Log.d("data",""+foods.getFoodsImg());

        //new HttpImageRequestTask(holder.imageViewFood, "url").execute();

        Glide
                .with(mContext)
                .load(foods.getFoodsImg())
                .centerCrop()
                .placeholder(R.drawable.diet_1)
                .into(holder.imageViewFood);


      /*  GlideToVectorYou
                .init()
                .with(this)
                .withListener(new GlideToVectorYouListener() {
                    @Override
                    public void onLoadFailed() {
                        Toast.makeText(context, "Load failed", Toast.LENGTH_SHORT).show()
                    }

                    @Override
                    public void onResourceReady() {
                        Toast.makeText(context, "Image ready", Toast.LENGTH_SHORT).show()
                    }
                })
                .setPlaceHolder(placeholderLoading, placeholderError)
                .load(IMAGE_URL, imageview);*/

            /*
            GenericRequestBuilder<Uri,InputStream, SVG, PictureDrawable>
                requestBuilder = Glide.with(mContext)
                .using(Glide.buildStreamModelLoader(Uri.class, mContext), InputStream.class)
                .from(Uri.class)
                .as(SVG.class)
                .transcode(new SvgDrawableTranscoder(), PictureDrawable.class)
                .sourceEncoder(new StreamEncoder())
                .cacheDecoder(new FileToStreamDecoder<SVG>(new SvgDecoder()))
                .decoder(new SvgDecoder())
                .placeholder(R.drawable.svg_image_view_placeholder)
                .error(R.drawable.error_image)
                .listener(new SvgSoftwareLayerSetter<Uri>());



            Uri uri = Uri.parse(svgImageUrl);
            requestBuilder
                    .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                    .load(uri)
                    .into(imageView);
            */


            /*
        requestBuilder = Glide.with(mActivity)
                .using(Glide.buildStreamModelLoader(Uri.class, mActivity), InputStream.class)
                .from(Uri.class)
                .as(SVG.class)
                .transcode(new SvgDrawableTranscoder(), PictureDrawable.class)
                .sourceEncoder(new StreamEncoder())
                .cacheDecoder(new FileToStreamDecoder<SVG>(new SvgDecoder()))
                .decoder(new SvgDecoder())
                .placeholder(R.drawable.ic_facebook)
                .error(R.drawable.ic_web)
                .animate(android.R.anim.fade_in)
                .listener(new SvgSoftwareLayerSetter<Uri>());
        */

        /*Picasso.with(ctx)
                .load("http://blog.concretesolutions.com.br/wp-content/uploads/2015/04/Android1.png")
                .into(getTarget(url));*/
        //FatchSvg.fetchSvg(mContext, foods.getFoodsImg(), holder.imageViewFood);

    }

    @Override
    public int getItemCount() {
        return foodDetailList1.size();
    }

    @Override
    public FoodDetailAdapter1.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_food_detail, parent, false);
        return new FoodDetailAdapter1.MyViewHolder(v);
    }

    /*
    public static class FatchSvg {
        private static OkHttpClient httpClient;

        public static void fetchSvg(Context context, String url, final ImageView target) {
            if (httpClient == null) {
                // Use cache for performance and basic offline capability
                httpClient = new OkHttpClient.Builder()
                        .cache(new Cache(context.getCacheDir(), 5 * 1024 * 1014))
                        .build();
            }

            Request request = new Request.Builder().url(url).build();
            httpClient.newCall(request).enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                  //  target.setImageDrawable(R.mipmap.ic_launcher);
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    InputStream stream = response.body().byteStream();
                    Sharp.loadInputStream(stream).into(target);
                    stream.close();
                }
            });
        }
    }
    */

    private class HttpImageRequestTask extends AsyncTask<Void, Void, Drawable> {

        ImageView iv = null;
        String url = null;

        public HttpImageRequestTask(ImageView iv, String url){
            this.iv = iv;
            this.url = url;
        }


        @Override
        protected Drawable doInBackground(Void... params) {
            try {

                final URL url = new URL("http://upload.wikimedia.org/wikipedia/commons/e/e8/Svg_example3.svg");
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                InputStream inputStream = urlConnection.getInputStream();
                SVG svg = SVGParser.getSVGFromInputStream(inputStream);
                Drawable drawable = svg.createPictureDrawable();
                return drawable;
            } catch (Exception e) {
                Log.e("MainActivity", e.getMessage(), e);
            }

            return null;
        }

        @Override
        protected void onPostExecute(Drawable drawable) {
            // Update the view
            iv.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
            iv.setImageDrawable(drawable);
        }
    }


}

