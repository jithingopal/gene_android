package com.muhdo.app.ui.login.v3

import android.app.Dialog
import android.content.DialogInterface
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.RadioButton
import android.widget.RadioGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import com.facebook.appevents.AppEventsLogger
import com.muhdo.app.R
import com.muhdo.app.databinding.ActivitySignUpPrecheckBinding
import com.muhdo.app.ui.login.SignUpVideo

class SignUpPrecheckActivity : AppCompatActivity() {

    lateinit var  dialog :Dialog;
    lateinit var binding: ActivitySignUpPrecheckBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        AppEventsLogger.activateApp(this)
        dialog= Dialog(this)
        val view = View.inflate(this, R.layout.dialog_signup_precheck, null)
        val closeButton = view.findViewById<ImageView>(R.id.img_close)
        closeButton.setOnClickListener { dialog.dismiss() }
        dialog.setContentView(view)
        dialog.setCancelable(false)
        binding = DataBindingUtil.setContentView(this@SignUpPrecheckActivity, R.layout.activity_sign_up_precheck)

        binding.radioYesOrNo.setOnCheckedChangeListener(RadioGroup.OnCheckedChangeListener{group,checkedId->
           if(checkedId==R.id.btn_yes){
               val i = Intent(applicationContext, SignUpVideo::class.java)
               startActivity(i)
               finish()
           }else{


               if(!dialog.isShowing)
               dialog.show()

           }
        })
        binding.btnNo.setOnClickListener { view->
            if(!dialog.isShowing)
                dialog.show()
        }
        binding.btnBack.setOnClickListener{view->
            finish()
        }
        binding.llBack.setOnClickListener{
            view->finish()
        }

    }
}
