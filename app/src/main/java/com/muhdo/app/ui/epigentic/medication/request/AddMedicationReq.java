package com.muhdo.app.ui.epigentic.medication.request;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AddMedicationReq {

    @SerializedName("user_id")
    @Expose
    private String userId;

    @SerializedName("medication_id")
    @Expose
    private String medicationId;

    @SerializedName("drugs")
    @Expose
    private String drugsName;

    @SerializedName("dosage")
    @Expose
    private String dosage;

    @SerializedName("quantity")
    @Expose
    private String quantity;


    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getMedicationId() {
        return medicationId;
    }

    public void setMedicationId(String medicationId) {
        this.medicationId = medicationId;
    }

    public String getDrugsName() {
        return drugsName;
    }

    public void setDrugsName(String drugsName) {
        this.drugsName = drugsName;
    }

    public String getDosage() {
        return dosage;
    }

    public void setDosage(String dosage) {
        this.dosage = dosage;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }


}
