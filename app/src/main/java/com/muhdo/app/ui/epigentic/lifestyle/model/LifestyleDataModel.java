package com.muhdo.app.ui.epigentic.lifestyle.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LifestyleDataModel {

    @SerializedName("_id")
    @Expose
    private String id;

    @SerializedName("user_id")
    @Expose
    private String userId;

    @SerializedName("sleep")
    @Expose
    private String sleepVal;

    @SerializedName("cravings")
    @Expose
    private String drugsName;

    @SerializedName("energy")
    @Expose
    private String energyVal;

    @SerializedName("party")
    @Expose
    private String partyVal;

    @SerializedName("general_health")
    @Expose
    private String generalHealthVal;

    @SerializedName("mental_health")
    @Expose
    private String mentalHealthVal;

    @SerializedName("social")
    @Expose
    private String socialVal;

    @SerializedName("motivation")
    @Expose
    private String motivationVal;

    @SerializedName("anxiety")
    @Expose
    private String anxietyVal;


    @SerializedName("steps")
    @Expose
    private String stepsVal;


    @SerializedName("created_at")
    @Expose
    private String createdAtVal;


    @SerializedName("updated_at")
    @Expose
    private String updatedAtVal;


    @SerializedName("__v")
    @Expose
    private String vVal;


}
