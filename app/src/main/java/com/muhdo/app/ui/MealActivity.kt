package com.muhdo.app.ui

import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.muhdo.app.R
import com.muhdo.app.adapter.DayMealAdapter
import com.muhdo.app.adapter.RecipeListAdapter
import com.muhdo.app.apiModel.mealModel.MealPlanModel
import com.muhdo.app.apiModel.mealModel.UpdateFavouriteRequest
import com.muhdo.app.apiModel.mealModel.daysModel.UpdateFavoriteModel
import com.muhdo.app.databinding.ActivityMealBinding
import com.muhdo.app.interfaces.ItemClickListener
import com.muhdo.app.repository.ApiUtilis
import com.muhdo.app.repository.ErrorModel
import com.muhdo.app.repository.NetworkManager
import com.muhdo.app.repository.ServiceListener
import com.muhdo.app.utils.v3.Constants
import com.muhdo.app.utils.v3.DayMealClickListener
import com.timingsystemkotlin.backuptimingsystem.Utility
import kotlinx.android.synthetic.main.list_breakfast.view.*


class MealActivity : AppCompatActivity(),
    ItemClickListener, DayMealClickListener {


    override fun onFavouriteClick(id: String, view: View, status: Boolean) {
        updateFavorite(id, view, status)
    }

    override fun onCardViewClick(recipeID: String) {
        goToRecipe(recipeID)
    }

    override fun onClick(position: Int) {

    }

    private lateinit var binding: ActivityMealBinding
    private lateinit var recipeAdapter: RecipeListAdapter
    private lateinit var dayMealAdapter: DayMealAdapter
    private val drawableList = arrayListOf<Int>()
    private val recipeNameList = arrayListOf<String>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this@MealActivity, R.layout.activity_meal)
        getMealList()

        binding.recipeListNew.cardCalorie.setOnClickListener {
            val i = Intent(this@MealActivity, CalorieActivity::class.java)
            startActivity(i)
        }
        binding.recipeListNew.cardFavourite.setOnClickListener {
            val i = Intent(this@MealActivity, MyFavouriteActivity::class.java)
            startActivity(i)

        }

        binding.toolbar.btnNavigation.setOnClickListener {
            val intent = Intent(this, DashboardActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
            finish()
        }

        binding.txtSeeMoreLunch.setOnClickListener {
            val intent = Intent(this, ChooseMealActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
            finish()
        }
        binding.txtSeeMoreBreakfast.setOnClickListener {
            val intent = Intent(this, ChooseMealActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
            finish()
        }
        binding.txtSeeMoreDessert.setOnClickListener {
            val intent = Intent(this, ChooseMealActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
            finish()
        }
        binding.txtSeeMoreDinner.setOnClickListener {
            val intent = Intent(this, ChooseMealActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
            finish()
        }
        binding.txtSeeMoreSnacks.setOnClickListener {
            val intent = Intent(this, ChooseMealActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
            finish()
        }
        binding.txtSunday.setOnClickListener {
            val intent = Intent(this, ChooseMealActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
            finish()
        }
        binding.txtChangeSaturdayPlan.setOnClickListener {
            val intent = Intent(this, ChooseMealActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
            finish()
        }
        binding.toolbar.imgResurvey.setOnClickListener {
            confirmResurvey()
        }

        setRecipeList()
    }


    private fun confirmResurvey() {
        val dialogBuilder = AlertDialog.Builder(this@MealActivity)
        dialogBuilder.setMessage("Are you sure you want to update your meal plan?")
            .setCancelable(false)
            .setPositiveButton("Yes") { _, _ ->
                Utility.setMealStatus(this@MealActivity, false)
                val i = Intent(applicationContext, DashboardActivity::class.java)
                i.putExtra(Constants.DASHBOARD_ACTION, Constants.DASHBOARD_MEAL_PLAN)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
                startActivity(i)
                finish()
            }
            .setNegativeButton("No") { _, _ ->

            }

        val alert = dialogBuilder.create()
        alert.show()
    }

    private fun setRecipeList() {

        recipeNameList.add("MY CALORIES")
        recipeNameList.add("MY FAVOURITES")

        drawableList.add(R.drawable.recipes_slider1)
        drawableList.add(R.drawable.recipes_slider2)
        recipeAdapter = RecipeListAdapter(this@MealActivity, drawableList, recipeNameList)
        binding.recipeList.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        binding.recipeList.isNestedScrollingEnabled = true
        binding.recipeList.adapter = recipeAdapter

    }


    override fun onBackPressed() {
        super.onBackPressed()
        val i = Intent(this@MealActivity, DashboardActivity::class.java)
        i.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(i)
    }


    private fun getMealList() {
        binding.progressBar.visibility = View.VISIBLE
        val manager = NetworkManager()
        if (manager.isConnectingToInternet(applicationContext)) {
            manager.createApiRequest(
                ApiUtilis.getAPIInstance(applicationContext).getMealPlanV3(
                    Utility.getUserID(
                        applicationContext
                    )
                ),
                object : ServiceListener<MealPlanModel> {
                    override fun getServerResponse(response: MealPlanModel, requestcode: Int) {
                        binding.progressBar.visibility = View.GONE
                        setData(response)
                    }

                    override fun getError(error: ErrorModel, requestcode: Int) {
                        if (error.getCode() == 302) {
                            val i = Intent(this@MealActivity, DashboardActivity::class.java)
                            i.putExtra(Constants.DASHBOARD_ACTION, Constants.DASHBOARD_MEAL_PLAN)
                            startActivity(i)
                            finish()

                        } else {
                            binding.progressBar.visibility = View.GONE

                            Utility.displayShortSnackBar(
                                binding.parentLayout,
                                error.getMessage().toString()
                            )
                        }
                    }
                })
        } else {
            binding.progressBar.visibility = View.GONE
            Utility.displayShortSnackBar(
                binding.parentLayout,
                resources.getString(R.string.check_internet)
            )

        }

    }


    private fun goToRecipe(recipeID: String) {
        val i = Intent(applicationContext, RecipeActivity::class.java)
        i.putExtra(Constants.RECIPE_ID, recipeID)
        startActivity(i)
        finish()
    }

    private fun setData(data: MealPlanModel) {
        binding.daysLayout.visibility = VISIBLE
        if (data.getData()!!.getMonday() != null) {
            dayMealAdapter = DayMealAdapter(this@MealActivity, data.getData()!!.getMonday()!!, this)
            binding.mondayMealList.layoutManager =
                LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
            binding.mondayMealList.isNestedScrollingEnabled = true
            binding.mondayMealList.itemAnimator = DefaultItemAnimator()
            binding.mondayMealList.adapter = dayMealAdapter
        } else {
            binding.txtMonday.visibility = GONE
            binding.txtSeeMoreBreakfast.visibility = GONE
        }

        if (data.getData()!!.getThursday() != null) {
            dayMealAdapter = DayMealAdapter(this@MealActivity, data.getData()!!.getTuesday()!!,this)
            binding.tuesdayMealList.layoutManager =
                LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
            binding.tuesdayMealList.isNestedScrollingEnabled = true
            binding.tuesdayMealList.itemAnimator = DefaultItemAnimator()
            binding.tuesdayMealList.adapter = dayMealAdapter
        } else {
            binding.txtTuesday.visibility = GONE
            binding.txtSeeMoreLunch.visibility = GONE
        }

        if (data.getData()!!.getWednesday() != null) {
            dayMealAdapter = DayMealAdapter(this@MealActivity, data.getData()!!.getWednesday()!!,this)
            binding.wednesdayMealList.layoutManager =
                LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
            binding.wednesdayMealList.isNestedScrollingEnabled = true
            binding.wednesdayMealList.itemAnimator = DefaultItemAnimator()
            binding.wednesdayMealList.adapter = dayMealAdapter
        } else {
            binding.txtWednesday.visibility = GONE
            binding.txtSeeMoreSnacks.visibility = GONE
        }

        if (data.getData()!!.getThursday() != null) {
            dayMealAdapter = DayMealAdapter(this@MealActivity, data.getData()!!.getThursday()!!,this)
            binding.thursdayMealList.layoutManager =
                LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
            binding.thursdayMealList.isNestedScrollingEnabled = true
            binding.thursdayMealList.itemAnimator = DefaultItemAnimator()
            binding.thursdayMealList.adapter = dayMealAdapter
        } else {
            binding.txtThursday.visibility = GONE
            binding.txtSeeMoreDessert.visibility = GONE
        }

        if (data.getData()!!.getFriday() != null) {
            dayMealAdapter = DayMealAdapter(this@MealActivity, data.getData()!!.getFriday()!!,this)
            binding.fridayMealList.layoutManager =
                LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
            binding.fridayMealList.isNestedScrollingEnabled = true
            binding.fridayMealList.itemAnimator = DefaultItemAnimator()
            binding.fridayMealList.adapter = dayMealAdapter
        } else {
            binding.txtFriday.visibility = GONE
            binding.txtSeeMoreDinner.visibility = GONE
        }

        if (data.getData()!!.getSaturday() != null) {
            dayMealAdapter = DayMealAdapter(this@MealActivity, data.getData()!!.getSaturday()!!,this)
            binding.saturdayMealList.layoutManager =
                LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
            binding.saturdayMealList.isNestedScrollingEnabled = true
            binding.saturdayMealList.itemAnimator = DefaultItemAnimator()
            binding.saturdayMealList.adapter = dayMealAdapter
        } else {
            binding.txtSaturday.visibility = GONE
            binding.txtChangeSaturdayPlan.visibility = GONE
        }


        if (data.getData()!!.getSunday() != null) {
            dayMealAdapter = DayMealAdapter(this@MealActivity, data.getData()!!.getSunday()!!,this)
            binding.sundayMealList.layoutManager =
                LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
            binding.sundayMealList.isNestedScrollingEnabled = true
            binding.sundayMealList.itemAnimator = DefaultItemAnimator()
            binding.sundayMealList.adapter = dayMealAdapter
        } else {
            binding.txtSunday.visibility = GONE
            binding.textSunday.visibility = GONE
        }


    }


    fun updateFavorite(id: String, view: View, status: Boolean) {
        val manager = NetworkManager()

        val requestParams =
            UpdateFavouriteRequest(status, id, Utility.getUserID(applicationContext))

        if (manager.isConnectingToInternet(applicationContext)) {
            manager.createApiRequest(
                ApiUtilis.getAPIInstance(applicationContext).updateFavouriteRecipe(requestParams),
                object : ServiceListener<UpdateFavoriteModel> {
                    override fun getServerResponse(
                        response: UpdateFavoriteModel,
                        requestcode: Int
                    ) {
                        view.isClickable = true
                        if (status) {
                            Utility.favouriteIdList.add(id)
                            if (Utility.unFavouriteIdList.contains(id)) {
                                Utility.unFavouriteIdList.remove(id)
                            }
                        } else if (!status) {
                            Utility.unFavouriteIdList.add(id)
                            if (Utility.favouriteIdList.contains(id)) {
                                Utility.favouriteIdList.remove(id)
                            }
                        }

                        dayMealAdapter.notifyDataSetChanged()
                    }

                    override fun getError(error: ErrorModel, requestcode: Int) {
                        if (status) {
                            view.check_favourite.isChecked = false
                        } else if (!status) {
                            view.check_favourite.isChecked = true
                        }
                        Utility.displayShortSnackBar(
                            binding.parentLayout,
                            error.getMessage().toString()
                        )
                    }
                })
        } else {


        }
    }

}