package com.muhdo.app.ui.userrawdata

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

 class UserRawDataModel {

     @SerializedName("_id")
     @Expose
     private var id: String? = null

     @SerializedName("gender")
     @Expose
     private var gender: String? = null

     @SerializedName("first_name")
     @Expose
     private var firstName: String? = null

     @SerializedName("last_name")
     @Expose
     private var lastName: String? = null

     @SerializedName("kit_id")
     @Expose
     private var kitId: String? = null

     @SerializedName("kit_activated")
     @Expose
     private var kitActivated: Boolean? = null

     @SerializedName("file_parsed")
     @Expose
     private var fileParsed: String? = null

     @SerializedName("phone")
     @Expose
     private var phone: String? = null

     @SerializedName("address_lane1")
     @Expose
     private var addressLane1: String? = null

     @SerializedName("address_lane2")
     @Expose
     private var addressLane2: String? = null


     @SerializedName("city")
     @Expose
     private var city: String? = null


     @SerializedName("zipcode")
     @Expose
     private var zipCode: String? = null

     @SerializedName("state")
     @Expose
     private var state: String? = null

     @SerializedName("country")
     @Expose
     private var country: String? = null


     @SerializedName("weight")
     @Expose
     private var weight: String? = null


     @SerializedName("weight_unit")
     @Expose
     private var weightUnit: String? = null

     @SerializedName("height")
     @Expose
     private var height: String? = null

     @SerializedName("height_unit")
     @Expose
     private var heightUnit: String? = null

     @SerializedName("timezone")
     @Expose
     private var timeZone: String? = null

     @SerializedName("timezone_offset")
     @Expose
     private var timezoneOffset: String? = null


     @SerializedName("confirm_token")
     @Expose
     private var confirmToken: String? = null

     @SerializedName("status")
     @Expose
     private var status: String? = null

     @SerializedName("subscribed")
     @Expose
     private var subscribed: String? = null

     @SerializedName("subscribed_type")
     @Expose
     private var subscribedType: String? = null

     @SerializedName("last_login")
     @Expose
     private var lastLogin: String? = null

     @SerializedName("email")
     @Expose
     private var email: String? = null

     @SerializedName("enterpriser")
     @Expose
     private var enterpriser: String? = null

     @SerializedName("key_data_generated")
     @Expose
     private var keyDataGenerated: Boolean? = null

     fun getCode(): Boolean? {
         return keyDataGenerated
     }

     fun setCode(keyDataGenerated: Boolean?) {
         this.keyDataGenerated = keyDataGenerated
     }

 }


