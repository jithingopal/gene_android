package com.muhdo.app.ui.login

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.facebook.FacebookSdk
import com.facebook.appevents.AppEventsLogger
import com.muhdo.app.R
import com.muhdo.app.apiModel.login.LostPasswordResponse
import com.muhdo.app.databinding.ActivityLostPasswordBinding
import com.muhdo.app.repository.ApiUtilis
import com.muhdo.app.repository.NetworkManager2
import com.muhdo.app.ui.ResetPasswordActivity
import com.timingsystemkotlin.backuptimingsystem.Utility
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

class LostPasswordActivity : AppCompatActivity() {
    lateinit var binding: ActivityLostPasswordBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        FacebookSdk.sdkInitialize(applicationContext)
        AppEventsLogger.activateApp(this)
        binding = DataBindingUtil.setContentView(
            this@LostPasswordActivity,
            R.layout.activity_lost_password
        )
        binding.edtUsername.setSelection(binding.edtUsername.text.length)

        binding.btnBack.setOnClickListener {
            val i = Intent(applicationContext, LoginActivity::class.java)
            startActivity(i)
            finish()
        }

        binding.btnReset.setOnClickListener {
            if (binding.edtUsername.text.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(
                    binding.edtUsername.text
                ).matches()
            ) {
                binding.edtUsername.error = getString(R.string.please_enter_email)
                binding.edtUsername.requestFocus()
            } else {
                resetPassword()
            }
        }

    }


    override fun onBackPressed() {
        super.onBackPressed()
        val i = Intent(applicationContext, LoginActivity::class.java)
        startActivity(i)
        finish()
    }

    @SuppressLint("NewApi")
    private fun resetPassword() {
        binding.progressBar.visibility = View.VISIBLE
        val manager = NetworkManager2()
        if (manager.isConnectingToInternet(applicationContext)) {
            val params = HashMap<String, String>()
            params["email"] = binding.edtUsername.text.toString()
            ApiUtilis.getAPIInstance(applicationContext).lostPassword(params)
                .enqueue(object : Callback<LostPasswordResponse> {
                    override fun onResponse(
                        call: Call<LostPasswordResponse>,
                        response: Response<LostPasswordResponse>
                    ) {
                        binding.progressBar.visibility = View.GONE
                        if (response.body() == null) {
                            Utility.displayShortSnackBar(
                                binding.parentLayout,
                                "Bad request. Expected Username/client id combination not found."
                            )
                        } else {
                            if (response.body() != null && response.body()!!.getStatusCode() == 200) {
                                displayAlert()
                            } else if (response.body() != null) {
                                Utility.displayShortSnackBar(
                                    binding.parentLayout,
                                    "Bad request. Expected Username/client id combination not found."
                                )
                            }
                        }
                    }

                    override fun onFailure(call: Call<LostPasswordResponse>, t: Throwable) {
                        // Log error here since request failed
                        binding.progressBar.visibility = View.GONE
                        Log.e("Login", t.toString())
                        Utility.displayShortSnackBar(
                            binding.parentLayout,
                            "Server connection error!!, please try again later."
                        )
                    }
                })
        } else {
            binding.progressBar.visibility = View.GONE

            Utility.displayShortSnackBar(
                binding.parentLayout,
                resources.getString(R.string.check_internet)
            )

        }

    }


    private fun displayAlert() {
        val dialogBuilder = AlertDialog.Builder(this@LostPasswordActivity)
        dialogBuilder.setMessage("Your request has been processed. Please check your email for verification code")
            .setCancelable(false)
            .setPositiveButton("Yes") { _, _ ->
                val i = Intent(applicationContext, ResetPasswordActivity::class.java)
                i.putExtra("EmailID", binding.edtUsername.text.toString())
                startActivity(i)
                finish()
            }


        val alert = dialogBuilder.create()
        alert.show()
    }

}
