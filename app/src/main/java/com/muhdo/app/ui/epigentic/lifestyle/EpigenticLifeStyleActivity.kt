package com.muhdo.app.ui.epigentic.lifestyle

import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.Context
import android.os.Bundle
import android.text.Html
import android.util.Base64
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import com.muhdo.app.R
import com.muhdo.app.databinding.ActivityEpigenticLifeStyleBinding
import com.muhdo.app.fragment.MealQuestionAnswerFragment
import com.muhdo.app.repository.ApiUtilis
import com.muhdo.app.repository.ErrorModel
import com.muhdo.app.repository.NetworkManager
import com.muhdo.app.repository.ServiceListener
import com.muhdo.app.ui.epigentic.lifestyle.response.LifestyleResponseModel
import com.muhdo.app.utils.v3.Constants
import com.timingsystemkotlin.backuptimingsystem.Utility
import java.util.*

class EpigenticLifeStyleActivity : AppCompatActivity() {
    private var progressDialog: ProgressDialog? = null
    private var currentPage = 0
    var requestCode = 100;
    private var userInput = false
    lateinit var binding: ActivityEpigenticLifeStyleBinding
    val params = HashMap<String, String>()
    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(
            this@EpigenticLifeStyleActivity,
            R.layout.activity_epigentic_life_style
        )
        binding.btnBack.setOnClickListener {
            finish()
        }
        Dialog(this)

        resetData()
        init()
        val data = Constants.LIFESTYLE_QUESTION_TYPE_SLEEP
        showView(data)
        validateSleepQuestionnaire()
        validateCravingsQuestionnaire()
        validateEnergyQuestionnaire()
        validatePartyQuestionnaire()
        validateGenHealthQuestionnaire()
        validateMentalHealthQuestionnaire()
        validateSocialQuestionnaire()
        validateMotivationQuestionnaire()
        validateAnxityQuestionnaire()
        validateStepsQuestionnaire()
        addBottomDots(currentPage)

        val userId = Base64.decode(Utility.getUserID(this), Base64.DEFAULT)
        val decodeUserId = String(userId, charset("UTF-8"))
        params["user_id"] = Utility.getUserID(this)
        skipLyout()

        sendLifestyleDataToServer()


    }

    private fun skipLyout() {

        binding.textviewSkip.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                Log.d("data", "click a")

                if (currentPage == 0) {
                    params["sleep"] = "0"
                    val data = Constants.LIFESTYLE_QUESTION_TYPE_CRAVINGS
                    binding.layoutLifestyleSleep.root.visibility = View.GONE
                    showView(data)
                    currentPage++
                    addBottomDots(currentPage)
                } else if (currentPage == 1) {
                    params["cravings"] = "0"
                    val data = Constants.LIFESTYLE_QUESTION_TYPE_ENERGY
                    binding.layoutLifestyleCravings.root.visibility = View.GONE
                    showView(data)
                    currentPage++
                    addBottomDots(currentPage)
                } else if (currentPage == 2) {
                    params["energy"] = "0"
                    val data = Constants.LIFESTYLE_QUESTION_TYPE_PARTY
                    binding.layoutLifestyleEnergy.root.visibility = View.GONE
                    showView(data)
                    currentPage++
                    addBottomDots(currentPage)
                } else if (currentPage == 3) {
                    params["party"] = "0"
                    val data = Constants.LIFESTYLE_QUESTION_TYPE_GEN_HEALTH
                    binding.layoutLifestyleParty.root.visibility = View.GONE
                    showView(data)
                    currentPage++
                    addBottomDots(currentPage)
                } else if (currentPage == 4) {
                    params["general_health"] = "0"
                    val data = Constants.LIFESTYLE_QUESTION_TYPE_MENTAL_HEALTH
                    binding.layoutLifestyleGenHealth.root.visibility = View.GONE
                    showView(data)
                    currentPage++
                    addBottomDots(currentPage)
                } else if (currentPage == 5) {
                    params["mental_health"] = "0"
                    val data = Constants.LIFESTYLE_QUESTION_TYPE_SOCIAL
                    binding.layoutLifestyleMentalHealth.root.visibility = View.GONE
                    showView(data)
                    currentPage++
                    addBottomDots(currentPage)
                } else if (currentPage == 6) {
                    params["social"] = "0"
                    val data = Constants.LIFESTYLE_QUESTION_TYPE_MOTIVATION
                    binding.layoutLifestyleSocial.root.visibility = View.GONE
                    showView(data)
                    currentPage++
                    addBottomDots(currentPage)
                } else if (currentPage == 7) {
                    params["motivation"] = "0"
                    val data = Constants.LIFESTYLE_QUESTION_TYPE_ANXIETY
                    binding.layoutLifestyleMotivation.root.visibility = View.GONE
                    showView(data)
                    currentPage++
                    addBottomDots(currentPage)
                } else if (currentPage == 8) {
                    binding.btncontinue.visibility = View.VISIBLE
                    params["anxiety"] = "0"
                    val data = Constants.LIFESTYLE_QUESTION_TYPE_STEPS
                    binding.layoutLifestyleAnxity.root.visibility = View.GONE
                    showView(data)
                    currentPage++
                    addBottomDots(currentPage)
                } else if (currentPage == 9) {
                    binding.btncontinue.visibility = View.VISIBLE
                    params["steps"] = "0"
                    /*val data = Constants.LIFESTYLE_QUESTION_TYPE_STEPS
                    binding.layoutLifestyleAnxity.root.visibility = View.GONE
                    showView(data)*/
                }


            }
        })

    }

    private fun sendLifestyleDataToServer() {
        binding.btncontinue.setOnClickListener {
            //binding.progressBar.visibility = View.VISIBLE
            showProgressDialog()
            val manager = NetworkManager()
            if (manager.isConnectingToInternet(applicationContext)) {
                manager.createApiRequest(
                    ApiUtilis.getAPIService(Constants.LIFESTYLE_API).addLifestyle(params),
                    object : ServiceListener<LifestyleResponseModel> {
                        override fun getServerResponse(
                            response: LifestyleResponseModel,
                            requestcode: Int
                        ) {
                            //binding.progressBar.visibility = View.GONE
                            dismissProgressDialog()
                            if (response.getCode() == 200 && response.getData() != null) {
                                //Log.d("data",""+response.getCode());
                                setResult(100)
                                finish()
                            } else {
                                Utility.displayShortSnackBar(
                                    binding.parentLayout,
                                    response.getMessage().toString()
                                )
                            }

                        }

                        override fun getError(error: ErrorModel, requestcode: Int) {
                            dismissProgressDialog()
                            //binding.progressBar.visibility = View.GONE
                            Utility.displayShortSnackBar(
                                binding.parentLayout,
                                error.getMessage().toString()
                            )
                        }
                    })
            } else {
                dismissProgressDialog()
                // binding.progressBar.visibility = View.GONE
                Utility.displayShortSnackBar(
                    binding.parentLayout,
                    resources.getString(R.string.check_internet)
                )
            }
        }

    }

    private fun validateSleepQuestionnaire() {
        binding.layoutLifestyleSleep.optionA.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                binding.layoutLifestyleSleep.view1.background =
                    ActivityCompat.getDrawable(applicationContext, R.drawable.change_border_bg)
                Log.d("data", "click a")
                currentPage++
                addBottomDots(currentPage)
                params["sleep"] = "1"
                //Utility.listValue.set(1, "A")
                val data = Constants.LIFESTYLE_QUESTION_TYPE_CRAVINGS
                binding.layoutLifestyleSleep.root.visibility = View.GONE
                overridePendingTransition(R.anim.enter, R.anim.exit);
                showView(data)
            }
        })

        binding.layoutLifestyleSleep.optionB.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                binding.layoutLifestyleSleep.view2.background =
                    ActivityCompat.getDrawable(applicationContext, R.drawable.change_border_bg)

                Log.d("data", "click a")
                currentPage++
                addBottomDots(currentPage)
                params["sleep"] = "2"
                // Utility.listValue.set(1, "B")
                val data = Constants.LIFESTYLE_QUESTION_TYPE_CRAVINGS
                binding.layoutLifestyleSleep.root.visibility = View.GONE
                overridePendingTransition(R.anim.enter, R.anim.exit);
                showView(data)
            }
        })

        binding.layoutLifestyleSleep.optionC.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                binding.layoutLifestyleSleep.view3.background =
                    ActivityCompat.getDrawable(applicationContext, R.drawable.change_border_bg)

                Log.d("data", "click a")
                currentPage++
                addBottomDots(currentPage)
                params["sleep"] = "3"
                //Utility.listValue.set(1, "C")
                val data = Constants.LIFESTYLE_QUESTION_TYPE_CRAVINGS
                binding.layoutLifestyleSleep.root.visibility = View.GONE
                overridePendingTransition(R.anim.enter, R.anim.exit);
                showView(data)
            }
        })


        binding.layoutLifestyleSleep.optionD.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                binding.layoutLifestyleSleep.view4.background =
                    ActivityCompat.getDrawable(applicationContext, R.drawable.change_border_bg)

                Log.d("data", "click a")
                currentPage++
                addBottomDots(currentPage)
                params["sleep"] = "4"
                //Utility.listValue.set(1, "D")
                val data = Constants.LIFESTYLE_QUESTION_TYPE_CRAVINGS
                binding.layoutLifestyleSleep.root.visibility = View.GONE
                overridePendingTransition(R.anim.enter, R.anim.exit);
                showView(data)
            }
        })

    }


    private fun validateCravingsQuestionnaire() {
        binding.layoutLifestyleCravings.optionA.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                binding.layoutLifestyleCravings.view1.background =
                    ActivityCompat.getDrawable(applicationContext, R.drawable.change_border_bg)

                Log.d("data", "click a")
                currentPage++
                addBottomDots(currentPage)
                params["cravings"] = "5"
                //Utility.listValue.set(2, "A")
                val data = Constants.LIFESTYLE_QUESTION_TYPE_ENERGY
                binding.layoutLifestyleCravings.root.visibility = View.GONE
                showView(data)
            }
        })

        binding.layoutLifestyleCravings.optionB.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                Log.d("data", "click a")
                binding.layoutLifestyleCravings.view1.background =
                    ActivityCompat.getDrawable(applicationContext, R.drawable.change_border_bg)

                currentPage++
                addBottomDots(currentPage)
                params["cravings"] = "6"
                // Utility.listValue.set(2, "B")
                val data = Constants.LIFESTYLE_QUESTION_TYPE_ENERGY
                binding.layoutLifestyleCravings.root.visibility = View.GONE
                showView(data)
            }
        })

        binding.layoutLifestyleCravings.optionC.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                Log.d("data", "click a")
                binding.layoutLifestyleCravings.view1.background =
                    ActivityCompat.getDrawable(applicationContext, R.drawable.change_border_bg)

                currentPage++
                addBottomDots(currentPage)
                params["cravings"] = "7"
                // Utility.listValue.set(2, "C")
                val data = Constants.LIFESTYLE_QUESTION_TYPE_ENERGY
                binding.layoutLifestyleCravings.root.visibility = View.GONE
                showView(data)
            }
        })


        binding.layoutLifestyleCravings.optionD.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                Log.d("data", "click a")
                binding.layoutLifestyleCravings.view1.background =
                    ActivityCompat.getDrawable(applicationContext, R.drawable.change_border_bg)

                currentPage++
                addBottomDots(currentPage)
                params["cravings"] = "8"
                //Utility.listValue.set(2, "D")
                val data = Constants.LIFESTYLE_QUESTION_TYPE_ENERGY
                binding.layoutLifestyleCravings.root.visibility = View.GONE
                showView(data)
            }
        })

    }


    private fun validateEnergyQuestionnaire() {
        binding.layoutLifestyleEnergy.optionA.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                Log.d("data", "click a")
                binding.layoutLifestyleEnergy.view1.background =
                    ActivityCompat.getDrawable(applicationContext, R.drawable.change_border_bg)
                currentPage++
                addBottomDots(currentPage)
                params["energy"] = "9"
                //Utility.listValue.set(3, "A")
                val data = Constants.LIFESTYLE_QUESTION_TYPE_PARTY
                binding.layoutLifestyleEnergy.root.visibility = View.GONE
                showView(data)
            }
        })

        binding.layoutLifestyleEnergy.optionB.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                Log.d("data", "click a")
                binding.layoutLifestyleEnergy.view1.background =
                    ActivityCompat.getDrawable(applicationContext, R.drawable.change_border_bg)

                currentPage++
                addBottomDots(currentPage)
                params["energy"] = "10"
                //Utility.listValue.set(3, "B")
                val data = Constants.LIFESTYLE_QUESTION_TYPE_PARTY
                binding.layoutLifestyleEnergy.root.visibility = View.GONE
                showView(data)
            }
        })

        binding.layoutLifestyleEnergy.optionC.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                Log.d("data", "click a")
                binding.layoutLifestyleEnergy.view1.background =
                    ActivityCompat.getDrawable(applicationContext, R.drawable.change_border_bg)

                currentPage++
                addBottomDots(currentPage)
                params["energy"] = "11"
                //Utility.listValue.set(3, "C")
                val data = Constants.LIFESTYLE_QUESTION_TYPE_PARTY
                binding.layoutLifestyleEnergy.root.visibility = View.GONE
                showView(data)
            }
        })


        binding.layoutLifestyleEnergy.optionD.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                Log.d("data", "click a")
                binding.layoutLifestyleEnergy.view1.background =
                    ActivityCompat.getDrawable(applicationContext, R.drawable.change_border_bg)

                currentPage++
                addBottomDots(currentPage)
                params["energy"] = "12"
                //Utility.listValue.set(3, "D")
                val data = Constants.LIFESTYLE_QUESTION_TYPE_PARTY
                binding.layoutLifestyleEnergy.root.visibility = View.GONE
                showView(data)
            }
        })

    }


    private fun validatePartyQuestionnaire() {
        binding.layoutLifestyleParty.optionA.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                Log.d("data", "click a")
                binding.layoutLifestyleParty.view1.background =
                    ActivityCompat.getDrawable(applicationContext, R.drawable.change_border_bg)

                currentPage++
                addBottomDots(currentPage)
                params["party"] = "13"
                //Utility.listValue.set(4, "A")
                val data = Constants.LIFESTYLE_QUESTION_TYPE_GEN_HEALTH
                binding.layoutLifestyleParty.root.visibility = View.GONE
                showView(data)
            }
        })

        binding.layoutLifestyleParty.optionB.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                Log.d("data", "click a")
                binding.layoutLifestyleParty.view1.background =
                    ActivityCompat.getDrawable(applicationContext, R.drawable.change_border_bg)

                currentPage++
                addBottomDots(currentPage)
                params["party"] = "14"
                //Utility.listValue.set(4, "B")
                val data = Constants.LIFESTYLE_QUESTION_TYPE_GEN_HEALTH
                binding.layoutLifestyleParty.root.visibility = View.GONE
                showView(data)
            }
        })

        binding.layoutLifestyleParty.optionC.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                Log.d("data", "click a")
                binding.layoutLifestyleParty.view1.background =
                    ActivityCompat.getDrawable(applicationContext, R.drawable.change_border_bg)

                currentPage++
                addBottomDots(currentPage)
                params["party"] = "15"
                //Utility.listValue.set(4, "C")
                val data = Constants.LIFESTYLE_QUESTION_TYPE_GEN_HEALTH
                binding.layoutLifestyleParty.root.visibility = View.GONE
                showView(data)
            }
        })


        binding.layoutLifestyleParty.optionD.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                Log.d("data", "click a")
                binding.layoutLifestyleParty.view1.background =
                    ActivityCompat.getDrawable(applicationContext, R.drawable.change_border_bg)

                currentPage++
                addBottomDots(currentPage)
                params["party"] = "16"
                //Utility.listValue.set(4, "D")
                val data = Constants.LIFESTYLE_QUESTION_TYPE_GEN_HEALTH
                binding.layoutLifestyleParty.root.visibility = View.GONE
                showView(data)
            }
        })

    }

    private fun validateGenHealthQuestionnaire() {
        binding.layoutLifestyleGenHealth.optionA.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                Log.d("data", "click a")
                binding.layoutLifestyleGenHealth.view1.background =
                    ActivityCompat.getDrawable(applicationContext, R.drawable.change_border_bg)

                currentPage++
                addBottomDots(currentPage)
                params["general_health"] = "18"
                //Utility.listValue.set(5, "A")
                val data = Constants.LIFESTYLE_QUESTION_TYPE_MENTAL_HEALTH
                binding.layoutLifestyleGenHealth.root.visibility = View.GONE
                showView(data)
            }
        })

        binding.layoutLifestyleGenHealth.optionB.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                Log.d("data", "click a")
                binding.layoutLifestyleGenHealth.view1.background =
                    ActivityCompat.getDrawable(applicationContext, R.drawable.change_border_bg)

                currentPage++
                addBottomDots(currentPage)
                params["general_health"] = "19"
                //Utility.listValue.set(5, "B")
                val data = Constants.LIFESTYLE_QUESTION_TYPE_MENTAL_HEALTH
                binding.layoutLifestyleGenHealth.root.visibility = View.GONE
                showView(data)
            }
        })

        binding.layoutLifestyleGenHealth.optionC.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                Log.d("data", "click a")
                binding.layoutLifestyleGenHealth.view1.background =
                    ActivityCompat.getDrawable(applicationContext, R.drawable.change_border_bg)

                currentPage++
                addBottomDots(currentPage)
                params["general_health"] = "20"
                //Utility.listValue.set(5, "C")
                val data = Constants.LIFESTYLE_QUESTION_TYPE_MENTAL_HEALTH
                binding.layoutLifestyleGenHealth.root.visibility = View.GONE
                showView(data)
            }
        })


        /*binding.layoutLifestyleGenHealth.optionD.setOnClickListener(object: View.OnClickListener {
            override fun onClick(v: View?) {
                Log.d("data","click a")
                currentPage++
                addBottomDots(currentPage)
                Utility.listValue.set(5, "D")
                val data = Constants.LIFESTYLE_QUESTION_TYPE_MENTAL_HEALTH
                binding.layoutLifestyleGenHealth.root.visibility = View.GONE
                showView(data)
            }
        })*/

    }


    private fun validateMentalHealthQuestionnaire() {
        binding.layoutLifestyleMentalHealth.optionA.setOnClickListener(object :
            View.OnClickListener {
            override fun onClick(v: View?) {
                Log.d("data", "click a")
                binding.layoutLifestyleMentalHealth.view1.background =
                    ActivityCompat.getDrawable(applicationContext, R.drawable.change_border_bg)

                currentPage++
                addBottomDots(currentPage)
                params["mental_health"] = "21"
                // Utility.listValue.set(6, "A")
                val data = Constants.LIFESTYLE_QUESTION_TYPE_SOCIAL
                binding.layoutLifestyleMentalHealth.root.visibility = View.GONE
                showView(data)
            }
        })

        binding.layoutLifestyleMentalHealth.optionB.setOnClickListener(object :
            View.OnClickListener {
            override fun onClick(v: View?) {
                Log.d("data", "click a")
                binding.layoutLifestyleMentalHealth.view1.background =
                    ActivityCompat.getDrawable(applicationContext, R.drawable.change_border_bg)

                currentPage++
                addBottomDots(currentPage)
                params["mental_health"] = "22"
                //Utility.listValue.set(6, "B")
                val data = Constants.LIFESTYLE_QUESTION_TYPE_SOCIAL
                binding.layoutLifestyleMentalHealth.root.visibility = View.GONE
                showView(data)
            }
        })

        binding.layoutLifestyleMentalHealth.optionC.setOnClickListener(object :
            View.OnClickListener {
            override fun onClick(v: View?) {
                Log.d("data", "click a")
                binding.layoutLifestyleMentalHealth.view1.background =
                    ActivityCompat.getDrawable(applicationContext, R.drawable.change_border_bg)

                currentPage++
                addBottomDots(currentPage)
                params["mental_health"] = "23"
                //Utility.listValue.set(6, "C")
                val data = Constants.LIFESTYLE_QUESTION_TYPE_SOCIAL
                binding.layoutLifestyleMentalHealth.root.visibility = View.GONE
                showView(data)
            }
        })


        binding.layoutLifestyleMentalHealth.optionD.setOnClickListener(object :
            View.OnClickListener {
            override fun onClick(v: View?) {
                Log.d("data", "click a")
                binding.layoutLifestyleMentalHealth.view1.background =
                    ActivityCompat.getDrawable(applicationContext, R.drawable.change_border_bg)

                currentPage++
                addBottomDots(currentPage)
                params["mental_health"] = "24"
                //Utility.listValue.set(6, "D")
                val data = Constants.LIFESTYLE_QUESTION_TYPE_SOCIAL
                binding.layoutLifestyleMentalHealth.root.visibility = View.GONE
                showView(data)
            }
        })

    }


    private fun validateSocialQuestionnaire() {
        binding.layoutLifestyleSocial.optionA.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                Log.d("data", "click a")
                binding.layoutLifestyleSocial.view1.background =
                    ActivityCompat.getDrawable(applicationContext, R.drawable.change_border_bg)

                currentPage++
                addBottomDots(currentPage)
                params["social"] = "25"
                //Utility.listValue.set(7, "A")
                val data = Constants.LIFESTYLE_QUESTION_TYPE_MOTIVATION
                binding.layoutLifestyleSocial.root.visibility = View.GONE
                showView(data)
            }
        })

        binding.layoutLifestyleSocial.optionB.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                Log.d("data", "click a")
                binding.layoutLifestyleSocial.view1.background =
                    ActivityCompat.getDrawable(applicationContext, R.drawable.change_border_bg)

                currentPage++
                addBottomDots(currentPage)
                params["social"] = "26"
                //Utility.listValue.set(7, "B")
                val data = Constants.LIFESTYLE_QUESTION_TYPE_MOTIVATION
                binding.layoutLifestyleSocial.root.visibility = View.GONE
                showView(data)
            }
        })

        binding.layoutLifestyleSocial.optionC.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                Log.d("data", "click a")
                binding.layoutLifestyleSocial.view1.background =
                    ActivityCompat.getDrawable(applicationContext, R.drawable.change_border_bg)

                currentPage++
                addBottomDots(currentPage)
                params["social"] = "27"
                // Utility.listValue.set(7, "C")
                val data = Constants.LIFESTYLE_QUESTION_TYPE_MOTIVATION
                binding.layoutLifestyleSocial.root.visibility = View.GONE
                showView(data)
            }
        })


        binding.layoutLifestyleSocial.optionD.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                Log.d("data", "click a")
                binding.layoutLifestyleSocial.view1.background =
                    ActivityCompat.getDrawable(applicationContext, R.drawable.change_border_bg)

                currentPage++
                addBottomDots(currentPage)
                params["social"] = "28"
                //Utility.listValue.set(7, "D")
                val data = Constants.LIFESTYLE_QUESTION_TYPE_MOTIVATION
                binding.layoutLifestyleSocial.root.visibility = View.GONE
                showView(data)
            }
        })

    }


    private fun validateMotivationQuestionnaire() {
        binding.layoutLifestyleMotivation.optionA.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                Log.d("data", "click a")
                binding.layoutLifestyleMotivation.view1.background =
                    ActivityCompat.getDrawable(applicationContext, R.drawable.change_border_bg)

                currentPage++
                addBottomDots(currentPage)
                params["motivation"] = "31"
                // Utility.listValue.set(8, "A")
                val data = Constants.LIFESTYLE_QUESTION_TYPE_ANXIETY
                binding.layoutLifestyleMotivation.root.visibility = View.GONE
                showView(data)
            }
        })

        binding.layoutLifestyleMotivation.optionB.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                Log.d("data", "click a")
                binding.layoutLifestyleMotivation.view1.background =
                    ActivityCompat.getDrawable(applicationContext, R.drawable.change_border_bg)

                currentPage++
                addBottomDots(currentPage)
                params["motivation"] = "32"
                //Utility.listValue.set(8, "B")
                val data = Constants.LIFESTYLE_QUESTION_TYPE_ANXIETY
                binding.layoutLifestyleMotivation.root.visibility = View.GONE
                showView(data)
            }
        })

        binding.layoutLifestyleMotivation.optionC.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                Log.d("data", "click a")
                binding.layoutLifestyleMotivation.view1.background =
                    ActivityCompat.getDrawable(applicationContext, R.drawable.change_border_bg)

                currentPage++
                addBottomDots(currentPage)
                params["motivation"] = "33"
                //Utility.listValue.set(8, "C")
                val data = Constants.LIFESTYLE_QUESTION_TYPE_ANXIETY
                binding.layoutLifestyleMotivation.root.visibility = View.GONE
                showView(data)
            }
        })


        binding.layoutLifestyleMotivation.optionD.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                Log.d("data", "click a")
                binding.layoutLifestyleMotivation.view1.background =
                    ActivityCompat.getDrawable(applicationContext, R.drawable.change_border_bg)

                currentPage++
                addBottomDots(currentPage)
                params["motivation"] = "34"
                //Utility.listValue.set(8, "D")
                val data = Constants.LIFESTYLE_QUESTION_TYPE_ANXIETY
                binding.layoutLifestyleMotivation.root.visibility = View.GONE
                showView(data)
            }
        })

    }


    private fun validateAnxityQuestionnaire() {
        binding.layoutLifestyleAnxity.optionA.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                Log.d("data", "click a")
                binding.layoutLifestyleAnxity.view1.background =
                    ActivityCompat.getDrawable(applicationContext, R.drawable.change_border_bg)

                binding.btncontinue.visibility = View.VISIBLE
                currentPage++
                addBottomDots(currentPage)
                params["anxiety"] = "35"
                //  Utility.listValue.set(9, "A")
                val data = Constants.LIFESTYLE_QUESTION_TYPE_STEPS
                binding.layoutLifestyleAnxity.root.visibility = View.GONE
                showView(data)
            }
        })

        binding.layoutLifestyleAnxity.optionB.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                binding.btncontinue.visibility = View.VISIBLE
                Log.d("data", "click a")
                binding.layoutLifestyleAnxity.view1.background =
                    ActivityCompat.getDrawable(applicationContext, R.drawable.change_border_bg)

                currentPage++
                addBottomDots(currentPage)
                params["anxiety"] = "36"
                //Utility.listValue.set(9, "B")
                val data = Constants.LIFESTYLE_QUESTION_TYPE_STEPS
                binding.layoutLifestyleAnxity.root.visibility = View.GONE
                showView(data)
            }
        })

        binding.layoutLifestyleAnxity.optionC.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                Log.d("data", "click a")
                binding.layoutLifestyleAnxity.view1.background =
                    ActivityCompat.getDrawable(applicationContext, R.drawable.change_border_bg)

                binding.btncontinue.visibility = View.VISIBLE
                currentPage++
                addBottomDots(currentPage)
                params["anxiety"] = "37"
                //Utility.listValue.set(9, "C")
                val data = Constants.LIFESTYLE_QUESTION_TYPE_STEPS
                binding.layoutLifestyleAnxity.root.visibility = View.GONE
                showView(data)
            }
        })


        binding.layoutLifestyleAnxity.optionD.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                Log.d("data", "click a")
                binding.layoutLifestyleAnxity.view1.background =
                    ActivityCompat.getDrawable(applicationContext, R.drawable.change_border_bg)

                binding.btncontinue.visibility = View.VISIBLE
                currentPage++
                addBottomDots(currentPage)
                params["anxiety"] = "38"
                //Utility.listValue.set(9, "D")
                val data = Constants.LIFESTYLE_QUESTION_TYPE_STEPS
                binding.layoutLifestyleAnxity.root.visibility = View.GONE
                showView(data)
            }
        })

    }


    private fun validateStepsQuestionnaire() {

        //binding.btncontinue.visibility = View.VISIBLE
        binding.layoutLifestyleSteps.optionA.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                binding.layoutLifestyleSteps.view1.background =
                    ActivityCompat.getDrawable(applicationContext, R.drawable.change_border_bg)
                binding.layoutLifestyleSteps.view2.background =
                    ActivityCompat.getDrawable(applicationContext, R.drawable.facebook_button_bg)
                binding.layoutLifestyleSteps.view3.background =
                    ActivityCompat.getDrawable(applicationContext, R.drawable.facebook_button_bg)
                binding.layoutLifestyleSteps.view4.background =
                    ActivityCompat.getDrawable(applicationContext, R.drawable.facebook_button_bg)

                Log.d("data", "click a")
                // currentPage++
                // addBottomDots(currentPage)
                params["steps"] = "39"

            }
        })

        binding.layoutLifestyleSteps.optionB.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                binding.layoutLifestyleSteps.view1.background =
                    ActivityCompat.getDrawable(applicationContext, R.drawable.facebook_button_bg)
                binding.layoutLifestyleSteps.view2.background =
                    ActivityCompat.getDrawable(applicationContext, R.drawable.change_border_bg)
                binding.layoutLifestyleSteps.view3.background =
                    ActivityCompat.getDrawable(applicationContext, R.drawable.facebook_button_bg)
                binding.layoutLifestyleSteps.view4.background =
                    ActivityCompat.getDrawable(applicationContext, R.drawable.facebook_button_bg)

                Log.d("data", "click a")
                //  currentPage++
                //  addBottomDots(currentPage)
                params["steps"] = "40"

            }
        })

        binding.layoutLifestyleSteps.optionC.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                binding.layoutLifestyleSteps.view3.background =
                    ActivityCompat.getDrawable(applicationContext, R.drawable.change_border_bg)
                binding.layoutLifestyleSteps.view1.background =
                    ActivityCompat.getDrawable(applicationContext, R.drawable.facebook_button_bg)
                binding.layoutLifestyleSteps.view2.background =
                    ActivityCompat.getDrawable(applicationContext, R.drawable.facebook_button_bg)
                binding.layoutLifestyleSteps.view4.background =
                    ActivityCompat.getDrawable(applicationContext, R.drawable.facebook_button_bg)

                Log.d("data", "click a")
                //currentPage++
                //addBottomDots(currentPage)
                params["steps"] = "41"

            }
        })


        binding.layoutLifestyleSteps.optionD.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                binding.layoutLifestyleSteps.view4.background =
                    ActivityCompat.getDrawable(applicationContext, R.drawable.change_border_bg)
                binding.layoutLifestyleSteps.view2.background =
                    ActivityCompat.getDrawable(applicationContext, R.drawable.facebook_button_bg)
                binding.layoutLifestyleSteps.view3.background =
                    ActivityCompat.getDrawable(applicationContext, R.drawable.facebook_button_bg)
                binding.layoutLifestyleSteps.view1.background =
                    ActivityCompat.getDrawable(applicationContext, R.drawable.facebook_button_bg)

                Log.d("data", "click a")
                // currentPage++
                // addBottomDots(currentPage)
                params["steps"] = "42"

            }
        })

    }


    private fun showView(data: String) {
        when (data) {


            Constants.LIFESTYLE_QUESTION_TYPE_SLEEP -> {
                overridePendingTransition(R.anim.enter, R.anim.exit);
                binding.layoutLifestyleSleep.root.visibility = View.VISIBLE
            }
            Constants.LIFESTYLE_QUESTION_TYPE_CRAVINGS -> {
                overridePendingTransition(R.anim.enter, R.anim.exit);
                binding.layoutLifestyleCravings.root.visibility = View.VISIBLE
            }
            Constants.LIFESTYLE_QUESTION_TYPE_ENERGY -> {
                binding.layoutLifestyleEnergy.root.visibility = View.VISIBLE
            }
            Constants.LIFESTYLE_QUESTION_TYPE_PARTY -> {
                binding.layoutLifestyleParty.root.visibility = View.VISIBLE
            }
            Constants.LIFESTYLE_QUESTION_TYPE_GEN_HEALTH -> {
                binding.layoutLifestyleGenHealth.root.visibility = View.VISIBLE
            }

            Constants.LIFESTYLE_QUESTION_TYPE_MENTAL_HEALTH -> {
                binding.layoutLifestyleMentalHealth.root.visibility = View.VISIBLE
            }
            Constants.LIFESTYLE_QUESTION_TYPE_SOCIAL -> {
                binding.layoutLifestyleSocial.root.visibility = View.VISIBLE
            }

            Constants.LIFESTYLE_QUESTION_TYPE_MOTIVATION -> {
                binding.layoutLifestyleMotivation.root.visibility = View.VISIBLE
            }

            Constants.LIFESTYLE_QUESTION_TYPE_ANXIETY -> {
                binding.layoutLifestyleAnxity.root.visibility = View.VISIBLE
            }

            Constants.LIFESTYLE_QUESTION_TYPE_STEPS -> {
                //binding.btncontinue.visibility = View.VISIBLE
                binding.layoutLifestyleSteps.root.visibility = View.VISIBLE

            }


        }
    }

    private fun showAlert(msg: String) {
        val builder = AlertDialog.Builder(this)
        builder.setMessage(msg)

        builder.setPositiveButton("OK") { _, _ ->

        }

        val alert = builder.create()
        alert.show()
    }


    fun addBottomDots(currentPage: Int) {
        val dots = arrayOfNulls<TextView>(10)
        binding.layoutDots.removeAllViews()
        for (i in dots.indices) {

            dots[i] = TextView(this)
            dots[i]!!.text = Html.fromHtml("&#8226;")
            dots[i]!!.textSize = 35f
            dots[i]!!.setTextColor(
                ContextCompat.getColor(
                    this!!.applicationContext,
                    R.color.secondary_color
                )
            )
            binding.layoutDots.addView(dots[i])
        }

//        .getColor(context, R.color.color_name)
        if (dots.isNotEmpty()) {
            dots[currentPage]!!.setTextColor(
                ContextCompat.getColor(
                    this!!.applicationContext,
                    R.color.color_dark_blue
                )
            )
        }
        if (currentPage == 10) {
            binding.btncontinue.visibility = View.VISIBLE

        }

        //loadFragment(currentPage)
    }

    private fun loadFragment(position: Int) {
        val transaction = supportFragmentManager.beginTransaction()
        //  val transaction = activity!!.supportFragmentManager.beginTransaction()
        val bundle = Bundle()
        when (position) {
            0 -> {
                bundle.putString(Constants.MEAL_QUESTION_TYPE, Constants.MEAL_QUESTION_TYPE_GOAL)
            }
            1 -> {
                bundle.putString(Constants.MEAL_QUESTION_TYPE, Constants.MEAL_QUESTION_TYPE_DIET)
            }
            2 -> {
                bundle.putString(Constants.MEAL_QUESTION_TYPE, Constants.MEAL_QUESTION_TYPE_FOOD)
            }
            3 -> {
                bundle.putString(Constants.MEAL_QUESTION_TYPE, Constants.MEAL_QUESTION_TYPE_ALCOHOL)
            }
            4 -> {
                bundle.putString(Constants.MEAL_QUESTION_TYPE, Constants.MEAL_QUESTION_TYPE_MEALS)
            }
            5 -> {
                bundle.putString(Constants.MEAL_QUESTION_TYPE, Constants.MEAL_QUESTION_TYPE_BMI)
            }
            6 -> {
                bundle.putString(
                    Constants.MEAL_QUESTION_TYPE,
                    Constants.MEAL_QUESTION_TYPE_PERSONAL
                )
            }
            7 -> {
                bundle.putString(
                    Constants.MEAL_QUESTION_TYPE,
                    Constants.MEAL_QUESTION_TYPE_ACTIVITY
                )
            }
        }
        val fragInfo = MealQuestionAnswerFragment()
        fragInfo.arguments = bundle
        transaction.replace(R.id.flContent, fragInfo)
        transaction.commit()
    }

    private fun init() {
        for (i in 0..11) {
            Utility.list.add(false)
            Utility.listValue.add("")
        }
    }

    private fun resetData() {
        Utility.setGoalDone(this!!.applicationContext, false)
        Utility.setDietDone(this!!.applicationContext, false)
        Utility.setDietDone1(this!!.applicationContext, false)
        Utility.setDietDone2(this!!.applicationContext, false)
        Utility.setFood(this!!.applicationContext, false)
        Utility.setAlcohol(this!!.applicationContext, false)
        Utility.setActivity(this!!.applicationContext, false)
        Utility.setMeals(this!!.applicationContext, false)
        Utility.setBMIIndex(this!!.applicationContext, false)
        Utility.setBMIIndex1(this!!.applicationContext, false)
        Utility.setPersonalDetails(this!!.applicationContext, false)
    }


    fun Dialog(context: Context) {
        try {
            progressDialog = ProgressDialog(context)
            progressDialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
            progressDialog!!.setProgressStyle(ProgressDialog.STYLE_SPINNER)
            progressDialog!!.setMessage(getString(R.string.please_wait))
            progressDialog!!.setCancelable(false)
        } catch (ignore: Exception) {
        }

    }

    fun showProgressDialog() {
        try {
            if (progressDialog != null) {
                progressDialog!!.show()
            }
        } catch (ignore: Exception) {
        }

    }

    fun dismissProgressDialog() {
        try {
            if (progressDialog != null) {
                progressDialog!!.dismiss()
            }
        } catch (ignore: Exception) {
        }

    }
}
