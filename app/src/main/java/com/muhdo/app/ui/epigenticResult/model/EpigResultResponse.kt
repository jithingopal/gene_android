package com.muhdo.app.ui.epigenticResult.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.muhdo.app.apiModel.keyData.KeyData
import com.muhdo.app.apiModel.result.SleepScore
import java.io.Serializable


class EpigResultResponse() : Serializable {

    @SerializedName("statusCode")
    @Expose
    var code: Int? = null

    @SerializedName("message")
    @Expose
    var message: String? = null

    @SerializedName("data")
    @Expose
    var data: Data? = null

}

class Data() :  Serializable    {

    @SerializedName("biological_age_data")
    @Expose
     var biologicalAge: BiologicalAge? = null

    @SerializedName("epigenetic_data")
    @Expose
     var epigenticHealthData: List<EpigenticHealth>? = null


}

class EpigenticHealth()  : Serializable  {

    @SerializedName("title")
    @Expose
     var trackingTitle: String? = null

    @SerializedName("Data")
    @Expose
     var data: EpigenticHealthData? = null

    constructor(parcel: Parcel) : this() {
        trackingTitle = parcel.readString()
    }



}

class EpigenticHealthData {

    @SerializedName("info")
    @Expose
     var info: String? = null

    @SerializedName("introduction")
    @Expose
     var introduction: String? = null

    @SerializedName("outcome")
    @Expose
     var outcome: String? = null

    @SerializedName("recommendations")
    @Expose
     var recommendations: String? = null

    @SerializedName("recommendations_info")
    @Expose
    open  var recommendationInfoList: List<RecommendationsInfo>? = null


    @SerializedName("score_data")
    @Expose
    var scoreData: ScoreData? = null
    @SerializedName("img")
    @Expose
    var img: String? = null
}

class ScoreData {

    @SerializedName("score")
    @Expose
     var score: String? = null

    @SerializedName("color")
    @Expose
     var color: String? = null


}

class RecommendationsInfo {
    @SerializedName("title")
    @Expose
     var title: String? = null

    @SerializedName("text")
    @Expose
    var text: String? = null
    @SerializedName("img")
    @Expose
    var img: String? = null
    @SerializedName("selected_img")
    @Expose
    var selected_img: String? = null

}

class BiologicalAge {

    @SerializedName("score")
    @Expose
     var score: String? = null

    @SerializedName("color")
    @Expose
     var color: String? = null


}
