package com.muhdo.app.ui.login

import android.Manifest
import android.annotation.SuppressLint
import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationManager
import android.os.Bundle
import android.provider.Settings
import android.util.Base64
import android.util.Log
import android.view.View
import android.view.Window
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.databinding.DataBindingUtil
import androidx.multidex.MultiDex
import com.facebook.FacebookSdk
import com.facebook.appevents.AppEventsLogger
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.muhdo.app.R
import com.muhdo.app.apiModel.login.KitRegistrationResponse
import com.muhdo.app.apiModel.login.VerifyOtpModel
import com.muhdo.app.databinding.ActivityVerifyOtpBinding
import com.muhdo.app.repository.*
import com.muhdo.app.ui.DashboardActivity
import com.muhdo.app.ui.ThankyouRegistrationActivity
import com.muhdo.app.ui.userrawdata.RawDataResponse
import com.muhdo.app.utils.PreferenceConnector
import com.muhdo.app.utils.PreferenceConnector.BIOLOGICAL_AGE
import com.muhdo.app.utils.PreferenceConnector.KIT_ID
import com.muhdo.app.utils.PreferenceConnector.SLEEP_INDEX
import com.timingsystemkotlin.backuptimingsystem.Utility
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class VerifyOtpActivity : AppCompatActivity(),
    GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,
    com.google.android.gms.location.LocationListener {

    override fun onConnected(p0: Bundle?) {
        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {

            return
        }


        startLocationUpdates()

        val fusedLocationProviderClient:
                FusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this)
        fusedLocationProviderClient.getLastLocation()
            .addOnSuccessListener {
                it?.let { location ->
                    if (Utility.getLat(applicationContext).isNullOrEmpty() && Utility.getLong(
                            applicationContext
                        ).isNullOrEmpty()
                    ) {
                        Utility.saveLatLong(
                            applicationContext,
                            location.latitude.toString(),
                            location.longitude.toString()
                        )
//                    Toast.makeText(this, "fetch location", Toast.LENGTH_SHORT).show()
                    }
                }


//                Toast.makeText(this@LoginActivity, "Locatiopn ==> " + it.latitude, Toast.LENGTH_LONG).show()
            }
//            .addOnSuccessListener(this, OnSuccessListener<Location> { location ->
//                // Got last known location. In some rare situations this can be null.
//                if (location != null) {
//                    // Logic to handle location object
//                    mLocation = location;
////                    txt_latitude.setText("" + mLocation.latitude)
////                    txt_longitude.setText("" + mLocation.longitude)
//                }
//            })
    }

    override fun onConnectionSuspended(p0: Int) {
//        Log.i(TAG, "Connection Suspended");
        mGoogleApiClient.connect()
    }

    override fun onConnectionFailed(p0: ConnectionResult) {

    }

    override fun onLocationChanged(p0: Location?) {
        if (Utility.getLat(applicationContext).isNullOrEmpty() && Utility.getLong(applicationContext).isNullOrEmpty()) {
            if (p0!!.longitude.toString() != null && p0!!.latitude.toString() != null) {
                Utility.saveLatLong(
                    applicationContext,
                    p0.latitude.toString(),
                    p0.longitude.toString()
                )
//                Toast.makeText(this, "fetch location", Toast.LENGTH_SHORT).show()
            } else {
                Toast.makeText(this, "Unable to fetch location", Toast.LENGTH_SHORT).show()
            }
        }
    }


    lateinit var binding: ActivityVerifyOtpBinding
    private lateinit var mGoogleApiClient: GoogleApiClient
    private var mLocationManager: LocationManager? = null
    private var mLocationRequest: LocationRequest? = null
    private val UPDATE_INTERVAL = (2 * 1000).toLong()  /* 10 secs */
    private val FASTEST_INTERVAL: Long = 2000 /* 2 sec */
    lateinit var locationManager: LocationManager
    open var progressDialog: ProgressDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        FacebookSdk.sdkInitialize(applicationContext)
        AppEventsLogger.activateApp(this)
        Dialog(this@VerifyOtpActivity)
        binding =
            DataBindingUtil.setContentView(this@VerifyOtpActivity, R.layout.activity_verify_otp)
//        binding.edtUsername.setSelection(binding.edtUsername.text.length)


        MultiDex.install(this)

        mGoogleApiClient = GoogleApiClient.Builder(this)
            .addConnectionCallbacks(this)
            .addOnConnectionFailedListener(this)
            .addApi(LocationServices.API)
            .build()

        mLocationManager = this.getSystemService(Context.LOCATION_SERVICE) as LocationManager

        checkLocation()

        binding.btnBack.setOnClickListener {
            //            val i = Intent(applicationContext, LoginActivity::class.java)
//            startActivity(i)
            finish()
        }

        binding.btnSend.setOnClickListener {
            if (binding.edtOtp.text.isEmpty()) {
                binding.edtOtp.error = getString(R.string.please_enter_code)
                binding.edtOtp.requestFocus()
            } else {
                val imm =
                    applicationContext!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager?
                imm!!.hideSoftInputFromWindow(binding.edtOtp.getWindowToken(), 0)

                sendOTP()
            }
        }

    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }


    private fun isLocationEnabled(): Boolean {
        locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(
            LocationManager.NETWORK_PROVIDER
        )
    }


    protected fun startLocationUpdates() {

        // Create the location request
        mLocationRequest = LocationRequest.create()
            .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
            .setInterval(UPDATE_INTERVAL)
            .setFastestInterval(FASTEST_INTERVAL)
        // Request location updates
        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            return
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(
            mGoogleApiClient,
            mLocationRequest, this
        )
    }

    private fun checkLocation(): Boolean {
        if (!isLocationEnabled())
            showAlert()
        return isLocationEnabled()
    }


    private fun showAlert() {
        val dialog = AlertDialog.Builder(this)
        dialog.setTitle("Enable Location")
            .setMessage("Your Locations Settings is set to 'Off'.\nPlease Enable Location to " + "use this app")
            .setPositiveButton(
                "Location Settings",
                { paramDialogInterface, paramInt ->
                    val myIntent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                    startActivity(myIntent)
                })
            .setNegativeButton(
                "Cancel",
                { paramDialogInterface, paramInt -> })
        dialog.show()
    }


    @SuppressLint("NewApi")
    private fun sendOTP() {

        binding.progressBar.visibility = View.VISIBLE

//        getKitRegistrationStatus()//to skip otp

        val manager = NetworkManager2()
        if (manager.isConnectingToInternet(applicationContext)) {
            ApiUtilis.getAPIInstance(applicationContext)
                .verifyOtp(Utility.getUserID(applicationContext), binding.edtOtp.text.toString())
                .enqueue(object : Callback<VerifyOtpModel> {
                    override fun onResponse(
                        call: Call<VerifyOtpModel>,
                        response: Response<VerifyOtpModel>
                    ) {
                        if (response.body() == null) {
                            binding.progressBar.visibility = View.GONE
                            Utility.displayShortSnackBar(
                                binding.parentLayout,
                                "Bad request."
                            )
                        } else {
                            if (response.body() != null && response.body()!!.getStatusCode() == 200) {
                                getKitRegistrationStatus()

                                //code commented k
                                /*Utility.setMealStatus(this@VerifyOtpActivity, json.getBoolean("meal_generated"))
                                Utility.setWorkoutStatus(this@VerifyOtpActivity, json.getBoolean("workout_generated"))
                                val i = Intent(applicationContext, ScannerActivity::class.java)
                                startActivity(i)
                                finish()
                                */

                                /* if(response.body()!!.getStatusCode() == 200){
                                     // Check user raw data status
                                     getUserRawData()
                                 }else{
                                     // val i = Intent(applicationContext, ThankyouRegistrationActivity::class.java)
                                     // startActivity(i)
                                     val i = Intent(applicationContext, ScannerActivity::class.java)
                                     startActivity(i)
                                     finish()

                                 }*/

                            } else if (response.body() != null) {
                                binding.progressBar.visibility = View.GONE
                                Utility.displayShortSnackBar(
                                    binding.parentLayout,
                                    response.body()!!.getData().toString()
                                )
                            }
                        }
                    }

                    override fun onFailure(call: Call<VerifyOtpModel>, t: Throwable) {
                        // Log error here since request failed
                        binding.progressBar.visibility = View.GONE
                        Log.e("Login", t.toString())
                        Utility.displayShortSnackBar(
                            binding.parentLayout,
                            "Server connection error!!, please try again later."
                        )
                    }
                })
        } else {
            binding.progressBar.visibility = View.GONE

            Utility.displayShortSnackBar(
                binding.parentLayout,
                resources.getString(R.string.check_internet)
            )

        }

    }

    private fun getUserRawData() {
        //binding!!.progressBar.visibility = View.VISIBLE
        val manager = NetworkManager()
        Log.wtf("data", "userid" + Utility.getUserID(this))
        val datasd = Base64.decode(Utility.getUserID(this), Base64.DEFAULT)
        val decodeUserId = String(datasd, charset("UTF-8"))

        if (manager.isConnectingToInternet(this)) {
            manager.createApiRequest(
                ApiUtilis.getAPIInstance(applicationContext).getUserRawData(decodeUserId),
                object : ServiceListener<RawDataResponse> {
                    override fun getServerResponse(response: RawDataResponse, requestcode: Int) {
                        binding.progressBar.visibility = View.GONE
                        Log.wtf("data", "code" + response.getCode() + " " + response.getData())

                        try {
                            if (response.getCode() == 200) {

                                Log.wtf(
                                    "data",
                                    "biological_age---  " + response.getData()!!.biologicalAge
                                )
                                Log.wtf(
                                    "data",
                                    "sleep index---  " + response.getData()!!.sleepIndex
                                )

                                if (!response.getData()!!.biologicalAge.isNullOrEmpty()) {
                                    PreferenceConnector.writeString(
                                        applicationContext,
                                        BIOLOGICAL_AGE,
                                        response.getData()!!.biologicalAge.toString()
                                    )
                                }
                                if (!response.getData()!!.sleepIndex.isNullOrEmpty()) {
                                    Log.wtf("data", "inside sleep")
                                    PreferenceConnector.writeString(
                                        applicationContext,
                                        SLEEP_INDEX,
                                        response.getData()!!.sleepIndex.toString()
                                    )
                                }

                                if (!response.getData()!!.kitId.isNullOrEmpty()) {
                                    PreferenceConnector.writeString(
                                        applicationContext,
                                        KIT_ID,
                                        response.getData()!!.kitId.toString()
                                    )
                                }
                                val i = Intent(applicationContext, DashboardActivity::class.java)
                                startActivity(i)
                                finish()

                            } else {
                                /*Utility.displayShortSnackBar(
                                    binding.parentLayout,
                                    response.getMessage().toString()
                                )*/
                                finish()
                                val i = Intent(
                                    applicationContext,
                                    ThankyouRegistrationActivity::class.java
                                )
                                startActivity(i)
                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }

                    }

                    override fun getError(error: ErrorModel, requestcode: Int) {
                        binding.progressBar.visibility = View.GONE

                        /*
                        Utility.displayShortSnackBar(
                            binding.parentLayout,
                            error.getMessage().toString()
                        )
                        */
                        finish()
                        val i = Intent(applicationContext, ThankyouRegistrationActivity::class.java)
                        startActivity(i)


                    }
                })
        } else {
            // binding.progressBar.visibility = View.GONE
            binding.progressBar.visibility = View.GONE
            Utility.displayShortSnackBar(
                binding.parentLayout,
                resources.getString(R.string.check_internet)
            )
        }
    }


    override fun onStart() {
        super.onStart()
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect()
        }
    }

    override fun onStop() {
        super.onStop()
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect()
        }
    }

    private fun getKitRegistrationStatus() {
        //binding!!.progressBar.visibility = View.VISIBLE
        val manager = NetworkManager2()
        if (manager.isConnectingToInternet(applicationContext)) {
            ApiUtilis.getAPIInstance(applicationContext).getKitStatus(Utility.getUserID(applicationContext))
                .enqueue(object : Callback<JsonObject> {
                    override fun onResponse(
                        call: Call<JsonObject>,
                        response: Response<JsonObject>
                    ) {

                        val js: JsonObject? = response.body()
                        val jsonResult = JSONObject(js.toString())
                        Log.e("Jithin2", js.toString())
                        try {
                            val gson = Gson()
                            if (jsonResult.getInt("statusCode") == 200) {
                                val kitRegistration: KitRegistrationResponse =
                                    gson.fromJson(js, KitRegistrationResponse::class.java)


                                if (kitRegistration == null) {
                                    binding.progressBar.visibility = View.GONE
                                    Utility.displayShortSnackBar(
                                        binding.parentLayout,
                                        "Bad request. Expected Username/client id combination not found."
                                    )
                                } else {
                                    if (kitRegistration != null && kitRegistration!!.getStatusCode() == 200) {
                                        if (kitRegistration!!.data!!.status == true) {
                                            //displayAlert()
                                            // Check user raw data status
                                            getUserRawData()

                                        } else {
                                            val i =
                                                Intent(
                                                    applicationContext,
                                                    ScannerActivity::class.java
                                                )
                                            startActivity(i)
                                            finish()
                                        }

                                } else if (response.body() != null) {
                                    binding.progressBar.visibility = View.GONE
                                    val i = Intent(applicationContext, ScannerActivity::class.java)
                                    startActivity(i)
                                    finish()
                                    //Utility.displayShortSnackBar(binding.parentLayout, response.body()!!.getData().toString())
                                }
                            }
                        } else if (jsonResult.getInt("statusCode") == 404) {
                            val i = Intent(applicationContext, ScannerActivity::class.java)
                            startActivity(i)
                            finish()

                            } else {

                                Utility.displayShortSnackBar(
                                    binding.parentLayout,
                                    JSONObject(jsonResult.getString("body")).getString("message").toString()
                                )

                            }

                        } catch (e: java.lang.Exception) {
                            e.printStackTrace()
                        }


                    }

                    override fun onFailure(call: Call<JsonObject>, t: Throwable) {
                        binding.progressBar.visibility = View.GONE
                        Log.e("Login", t.toString())
                        Utility.displayShortSnackBar(
                            binding.parentLayout,
                            "Server connection error!!, please try again later."
                        )
                    }
                })
        } else {
            binding.progressBar.visibility = View.GONE

            Utility.displayShortSnackBar(
                binding.parentLayout,
                resources.getString(R.string.check_internet)
            )

        }
    }

    private fun displayAlert() {
        val dialogBuilder = AlertDialog.Builder(this@VerifyOtpActivity)
        dialogBuilder.setMessage("Your account has been activated, please wait for 5 minutes to get user data")
            .setCancelable(false)
            .setPositiveButton("Ok") { _, _ ->
                val i = Intent(this@VerifyOtpActivity, DashboardActivity::class.java)
                startActivity(i)
            }


        val alert = dialogBuilder.create()
        alert.show()
    }


    fun Dialog(context: VerifyOtpActivity) {
        try {
            progressDialog = ProgressDialog(context)
            progressDialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
            progressDialog!!.setProgressStyle(ProgressDialog.STYLE_SPINNER)
            progressDialog!!.setMessage(getString(R.string.please_wait))
            progressDialog!!.setCancelable(false)
        } catch (ignore: Exception) {
        }

    }

    fun showProgressDialog() {
        try {
            if (progressDialog != null) {
                progressDialog!!.show()
            }
        } catch (ignore: Exception) {
        }

    }

    fun dismissProgressDialog() {
        try {
            if (progressDialog != null) {
                progressDialog!!.dismiss()
            }
        } catch (ignore: Exception) {
        }

    }
}