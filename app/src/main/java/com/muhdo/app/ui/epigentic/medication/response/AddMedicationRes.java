package com.muhdo.app.ui.epigentic.medication.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.muhdo.app.ui.epigentic.medication.model.MedicationInfo;

public class AddMedicationRes {


    @SerializedName("code")
    @Expose
    private String statusCode;

    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("data")
    @Expose
    private MedicationInfo medicationInfo;

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public MedicationInfo getMedicationInfo() {
        return medicationInfo;
    }

    public void setMedicationInfo(MedicationInfo medicationInfo) {
        this.medicationInfo = medicationInfo;
    }
}
