package com.muhdo.app.ui.epigentic.medication.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.muhdo.app.ui.epigentic.medication.model.MedicationType
import java.io.Serializable

class MedicationTypeResponse : Serializable {
    @SerializedName("code")
    @Expose
    private var code: Int? = null
    @SerializedName("message")
    @Expose
    private var message: String? = null
    @SerializedName("data")
    @Expose
    private var data: List<MedicationType>? = null

    fun getCode(): Int? {
        return code
    }

    fun setCode(code: Int?) {
        this.code = code
    }

    fun getMessage(): String? {
        return message
    }

    fun setMessage(message: String) {
        this.message = message
    }

    fun getData(): List<MedicationType>? {
        return data
    }

    fun setData(data: List<MedicationType>) {
        this.data = data
    }
}