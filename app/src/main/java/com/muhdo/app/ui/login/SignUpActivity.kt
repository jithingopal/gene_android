package com.muhdo.app.ui.login

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.app.DatePickerDialog
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.telephony.TelephonyManager
import android.text.*
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.inputmethod.InputMethodManager
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.databinding.DataBindingUtil
import com.facebook.*
import com.facebook.appevents.AppEventsLogger
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.android.gms.auth.GoogleAuthUtil
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.Task
import com.google.i18n.phonenumbers.PhoneNumberUtil
import com.muhdo.app.R
import com.muhdo.app.apiModel.login.CountryModel
import com.muhdo.app.apiModel.login.VerifyOtpModel
import com.muhdo.app.databinding.ActivitySignUpBinding
import com.muhdo.app.repository.ApiUtilis
import com.muhdo.app.repository.ErrorModel
import com.muhdo.app.repository.NetworkManager
import com.muhdo.app.repository.ServiceListener
import com.muhdo.app.ui.signUp.SignUpViewModel
import com.muhdo.app.ui.v3.BaseActivityV3
import com.timingsystemkotlin.backuptimingsystem.Utility
import kotlinx.android.synthetic.main.dialog_i_agree.view.*
import org.jetbrains.anko.doAsync
import java.text.SimpleDateFormat
import java.util.*

class SignUpActivity : BaseActivityV3() {


    lateinit var binding: ActivitySignUpBinding
    private lateinit var signUpViewModel: SignUpViewModel
    private val countryList: MutableList<String> = ArrayList()
    private var callbackManager = CallbackManager.Factory.create()!!
    private lateinit var mGoogleSignInClient: GoogleSignInClient
    private var countryCode = "0"
    private var cal = Calendar.getInstance()!!
    private var status = true


    @RequiresApi(Build.VERSION_CODES.N)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        FacebookSdk.sdkInitialize(applicationContext)
        AppEventsLogger.activateApp(this)

        signUpViewModel = SignUpViewModel()
        binding = DataBindingUtil.setContentView(this@SignUpActivity, R.layout.activity_sign_up)
        binding.viewModel = signUpViewModel
        getCountryCode()
        init()
        initParameters()
        initViews()
        getAllLocation()

        // create an OnDateSetListener
        val dateSetListener1 = DatePickerDialog.OnDateSetListener { p0, p1, p2, p3 ->
            p0!!.maxDate = System.currentTimeMillis()
            cal.set(Calendar.YEAR, p1)
            cal.set(Calendar.MONTH, p2)
            cal.set(Calendar.DAY_OF_MONTH, p3)
            updateDateInView()
        }


        binding.edtPassword.addTextChangedListener(object : TextWatcher {


            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}

            @SuppressLint("SetTextI18n")
            override fun afterTextChanged(s: Editable) {
                if (android.util.Patterns.EMAIL_ADDRESS.matcher(binding.edtEmail.text).matches() && status) {
                    status = false
                    val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager?
                    imm!!.hideSoftInputFromWindow(binding.edtPassword.windowToken, 0)

                    validateEmail()

                } else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(binding.edtEmail.text).matches()) {
                    binding.edtEmail.error = getString(R.string.please_enter_email)
                    binding.edtEmail.requestFocus()
                    status = true
                }
            }
        })

        binding.edtDob.setOnClickListener {
            DatePickerDialog(
                this@SignUpActivity,
                dateSetListener1,
                // set DatePickerDialog to point to today's date when it loads up
                cal.get(Calendar.YEAR),
                cal.get(Calendar.MONTH),
                cal.get(Calendar.DAY_OF_MONTH)
            ).show()
            //                dateSetListener1.setMaxDate(System.currentTimeMillis());
        }

        binding.btnSignIn.setOnClickListener {
            val i = Intent(applicationContext, LoginActivity::class.java)
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(i)
            finish()
        }

        binding.btnGoogle.setOnClickListener {
            //            binding.signInButton.performClick()
            signIn()
        }

        binding.edtDay.setOnFocusChangeListener { view, hasFocus ->
            if (hasFocus) {

            } else {
                var dayString: String = binding.edtDay.text.toString()
                if (dayString != null && !dayString.isEmpty() && !dayString.equals("")) {
                    var day: Int = Integer.parseInt(dayString);
                    if (day < 10) {
                        binding.edtDay.setText("0" + day)
                    }
                    if (day == 0) {

                        binding.edtDay.setText("01")

                    }
                    if (day > 31) {

                        binding.edtDay.setText("31")

                    }
                }
            }
        }
        binding.edtMonth.setOnFocusChangeListener { view, hasFocus ->
            if (hasFocus) {

            } else {
                var monthString: String = binding.edtMonth.text.toString()
                if (monthString != null && !monthString.isEmpty() && !monthString.equals("")) {
                    var month: Int = Integer.parseInt(monthString);
                    if (month < 10) {
                        binding.edtMonth.setText("0" + month)
                    }
                    if (month > 12) {
                        binding.edtMonth.setText("12")
                    }
                    if (month == 0) {
                        binding.edtMonth.setText("01")
                    }

                }
            }
        }

        binding.edtYear.setOnFocusChangeListener { view, hasFocus ->
            if (hasFocus) {

            } else {
                var yearString: String = binding.edtYear.text.toString()
                if (yearString != null && !yearString.isEmpty() && !yearString.equals("")) {
                    var year: Int = Integer.parseInt(yearString);
                    if (year < 1900) {
                        binding.edtYear.setText("1900")
                    }
                    if (year > Calendar.getInstance().get(Calendar.YEAR)) {
                        binding.edtYear.setText(Calendar.getInstance().get(Calendar.YEAR).toString() + "")
                    }

                }
            }
        }

        binding.txtPrivacyPolicy.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                val mDialogView = LayoutInflater.from(this).inflate(R.layout.dialog_i_agree, null)
                val mBuilder = AlertDialog.Builder(this)
                    .setView(mDialogView)
                mBuilder.setCancelable(false)
                val mAlertDialog = mBuilder.show()
                mDialogView.i_agree_dialog_text.setText("You are accepting Muhdo's ")
                mDialogView.i_agree_dialog_text.setClickable(true)
                mDialogView.i_agree_dialog_text.setMovementMethod(LinkMovementMethod.getInstance())
                val textTermCondition =
                    "<a href='https://muhdo.com/terms-conditions/' >Terms and Conditions</a>"
                /*link color #1f7fff*/
                val textPrivacyPolicy =
                    "<a href='https://muhdo.com/privacy-policy/'>Privacy Policy</a>";
                mDialogView.i_agree_dialog_text.append(Html.fromHtml(textPrivacyPolicy))
                mDialogView.i_agree_dialog_text.append(" under the General Data Protection Regulation (GDPR).")
                mDialogView.btn_i_agree.setOnClickListener {
                    mAlertDialog.dismiss()
                }
                mDialogView.btn_close.setOnClickListener {
                    mAlertDialog.dismiss()
                    binding.txtPrivacyPolicy.setChecked(false)
                }
            }
        }
        val textTermCondition =
            "I've read and accepted the " + "<a font color='#0000ff' href='https://muhdo.com/terms-conditions/' >Terms & Conditions</a> and <a href='https://muhdo.com/privacy-policy/'>Privacy Policy.</a>"
        binding.textViewPrivacyPolicy.setMovementMethod(LinkMovementMethod.getInstance());
        binding.textViewPrivacyPolicy.setText(Html.fromHtml(textTermCondition))

        /*binding.txtPrivacyPolicy.makeLinks(
            Pair("privacy policy", View.OnClickListener {
                *//*val uri =
                    Uri.parse("https://muhdo.com/") // missing 'http://' will cause crashed
                val intent = Intent(Intent.ACTION_VIEW, uri)
                startActivity(intent)*//*
            })
        )*/

    }


    private fun validateEmail() {

        binding.progressBar.visibility = VISIBLE


        val params = HashMap<String, String>()
        params["email"] = binding.edtEmail.text.toString()

        val manager = NetworkManager()
        if (manager.isConnectingToInternet(applicationContext)) {
            manager.createApiRequest(
                ApiUtilis.getAPIInstance(applicationContext).validateEmail(params),
                object : ServiceListener<VerifyOtpModel> {
                    override fun getServerResponse(response: VerifyOtpModel, requestcode: Int) {
                        binding.progressBar.visibility = GONE

                        binding.edtEmail.setText("")
                        binding.edtPassword.setText("")
                        status = true
                        displayAlert()
                    }

                    override fun getError(error: ErrorModel, requestcode: Int) {
                        binding.progressBar.visibility = GONE
                        status = false


                    }
                })
        } else {
            binding.progressBar.visibility = GONE
            Utility.displayShortSnackBar(
                binding.parentLayout,
                resources.getString(R.string.check_internet)
            )
            status = false


        }
    }


    private fun displayAlert() {
        val dialogBuilder = AlertDialog.Builder(this@SignUpActivity)
        dialogBuilder.setMessage("Email is already registered with Muhdo")
            .setCancelable(false)
            .setPositiveButton("Yes") { _, _ ->
                binding.edtEmail.requestFocus()
            }
        val alert = dialogBuilder.create()
        alert.show()
    }


    private fun updateDateInView() {
        val myFormat = "MM/dd/yyyy" // mention the format you need
        val sdf = SimpleDateFormat(myFormat, Locale.US)

        binding.edtDob.setText(sdf.format(cal.time))
    }


    private fun TextView.makeLinks(vararg links: Pair<String, View.OnClickListener>) {
        val spannableString = SpannableString(this.text)
        for (link in links) {
            val clickableSpan = object : ClickableSpan() {
                override fun onClick(view: View) {
                    Selection.setSelection((view as TextView).text as Spannable, 0)
                    view.invalidate()
                    link.second.onClick(view)
                }
            }
            val startIndexOfLink = this.text.toString().indexOf(link.first)
            spannableString.setSpan(
                clickableSpan, startIndexOfLink, startIndexOfLink + link.first.length,
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
            )
        }
        this.movementMethod =
            LinkMovementMethod.getInstance() // without LinkMovementMethod, link can not click
        this.setText(spannableString, TextView.BufferType.SPANNABLE)
    }


    private fun getCountryCode() {
        val tm = this.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
        val countryCodeValue = tm.networkCountryIso.toUpperCase()
        countryCode =
            PhoneNumberUtil.getInstance().getCountryCodeForRegion(countryCodeValue).toString()
    }

    private fun initParameters() {
        callbackManager = CallbackManager.Factory.create()

        LoginManager.getInstance().registerCallback(callbackManager,
            object : FacebookCallback<LoginResult> {
                override fun onSuccess(loginResult: LoginResult) {
                    val request = GraphRequest.newMeRequest(
                        loginResult.accessToken
                    ) { `object`, response ->
                        Log.v("LoginActivity", response.toString())
                        signUpViewModel.fbLogin(
                            `object`,
                            this@SignUpActivity,
                            binding,
                            loginResult.accessToken.token.toString()
                        )
                    }
                    val parameters = Bundle()
                    parameters.putString("fields", "id,name,email,first_name,last_name")
                    request.parameters = parameters
                    request.executeAsync()
                }

                override fun onCancel() {
                    // App code
                }

                override fun onError(exception: FacebookException) {
                    // App code
                }
            })
    }


    private fun initViews() {
        binding.btnFacebook.setOnClickListener {
            val accessToken = AccessToken.getCurrentAccessToken()
            if (accessToken != null && !accessToken.isExpired) {
                LoginManager.getInstance().logOut()

                LoginManager.getInstance().logInWithReadPermissions(
                    this,
                    Arrays.asList("public_profile", "email")
                )
            } else {
                LoginManager.getInstance().logInWithReadPermissions(
                    this,
                    Arrays.asList("public_profile", "email")
                )
            }
        }
    }

    private fun init() {
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestEmail()//request email id
            .build()

        mGoogleSignInClient = GoogleSignIn.getClient(this, gso)
    }

    private fun signIn() {
        binding.progressBar.visibility = VISIBLE
        val signInIntent = mGoogleSignInClient.signInIntent
        startActivityForResult(signInIntent, 16)
    }

    private fun handleSignInResult(completedTask: Task<GoogleSignInAccount>) {
        try {
            binding.progressBar.visibility = GONE
            val account = completedTask.getResult(ApiException::class.java)
            val scopes = "oauth2:profile email"
            doAsync {
                val token = GoogleAuthUtil.getToken(applicationContext, account!!.email, scopes)
                signUpViewModel.socialLogin(account, this@SignUpActivity, binding, token)
            }
        } catch (e: ApiException) {
            com.timingsystemkotlin.backuptimingsystem.Utility.displayShortSnackBar(
                binding.parentLayout,
                "Something went wrong " + e.statusCode.toString()
            )
            Log.w("Login", "signInResult:failed code=" + e.statusCode)
        }
    }


    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 16) {
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            handleSignInResult(task)

        } else {
            try {
                callbackManager!!.onActivityResult(requestCode, resultCode, data)
            } catch (e: Exception) {
                /* TODO: handle exception */
                Toast.makeText(applicationContext, "ERROR: " + e.message, Toast.LENGTH_LONG).show()
                println("Facebook error : $e")
            }
        }
    }

    private fun getAllLocation() {
        val manager = NetworkManager()
        manager.createApiRequest(
            ApiUtilis.getAPIInstance(applicationContext).getAllCountry(),
            object :
                ServiceListener<CountryModel> {
                override fun getServerResponse(response: CountryModel, requestcode: Int) {

                    setSpinnerData(response)
                }

                override fun getError(error: ErrorModel, requestcode: Int) {
                    Utility.displayShortSnackBar(
                        binding.parentLayout,
                        error.getMessage().toString()
                    )
                }
            })
    }

    private fun setSpinnerData(response: CountryModel) {
        countryList.add("Please select country")
        var selectedPosition = 0
        for (i in 0 until response.getData()!!.size) {
            countryList.add(response.getData()!![i].getName().toString())
            if (countryCode != "0" && countryCode == response.getData()!![i].getPhoneCode().toString()) {
                selectedPosition = i + 1
            }
        }

        // Create an ArrayAdapter using a simple spinner layout and languages array
        val adapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, countryList)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        // Set Adapter to Spinner
        binding.spinnerCountry.adapter = adapter
        binding.spinnerCountry.setSelection(selectedPosition)
        binding.spinnerCountry.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {
                @SuppressLint("SetTextI18n")
                override fun onItemSelected(
                    parent: AdapterView<*>,
                    view: View,
                    position: Int,
                    id: Long
                ) {

                    if (position != 0) {
                        System.out.println("countryList.get(position)   " + countryList[position])
                        if (countryList.size > position) {
                            if (countryList.get(position) == "United Kingdom") {
                                binding.layoutUkStreet.visibility = VISIBLE
                            } else {
                                binding.layoutUkStreet.visibility = GONE
                            }

                            Utility.countryID =
                                response.getData()!![position - 1].getId().toString()
                            val code =
                                response.getData()!![position - 1].getPhoneCode()!!.toString()
                            binding.edtPhone.setText("+" + code)

                            val l = binding.edtPhone.text.length
                            binding.edtPhone.setSelection(l)
                        }
                    }
                }

                override fun onNothingSelected(parent: AdapterView<*>) {
                }
            }
    }
}