package com.muhdo.app.ui.epigentic.lifestyle.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.muhdo.app.ui.epigentic.lifestyle.model.LifestyleDataModel

class LifestyleResponseModel {

    @SerializedName("code")
    @Expose
    private var code: Int? = null
    @SerializedName("message")
    @Expose
    private var message: String? = null
    @SerializedName("data")
    @Expose
    private var data: LifestyleDataModel? = null


    fun getCode(): Int? {
        return code
    }

    fun setCode(code: Int?) {
        this.code = code
    }

    fun getMessage(): String? {
        return message
    }

    fun setMessage(message: String) {
        this.message = message
    }

    fun getData():LifestyleDataModel? {
        return data
    }

    fun setData(data: LifestyleDataModel) {
        this.data = data
    }


}