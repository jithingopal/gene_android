package com.muhdo.app.ui

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager
import com.muhdo.app.R
import com.muhdo.app.databinding.ActivityWalkThroughBinding
import com.muhdo.app.ui.login.LoginActivity
import com.muhdo.app.ui.login.SignUpVideo
import com.muhdo.app.ui.v3.BaseActivityV3
import com.muhdo.app.utils.v3.Constants
import com.timingsystemkotlin.backuptimingsystem.Utility

class WalkThroughActivity : BaseActivityV3(), View.OnClickListener {

    private var myViewPagerAdapter: MyViewPagerAdapter? = null
    private var layouts: IntArray? =
        intArrayOf(R.layout.slide_screen1, R.layout.slide_screen2, R.layout.slide_screen3)
    lateinit var binding: ActivityWalkThroughBinding
    private var dots = arrayOfNulls<TextView>(3)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding =
            DataBindingUtil.setContentView(this@WalkThroughActivity, R.layout.activity_walk_through)


        if (!Utility.isFirstTimeLaunch(applicationContext)) {
            launchHomeScreen("")
            finish()
        }

        setOnClick()

        if (Build.VERSION.SDK_INT >= 21) {
            window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_STABLE or
                    View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
        }

        addBottomDots(0)

        // making notification bar transparent
        changeStatusBarColor()

        myViewPagerAdapter = MyViewPagerAdapter()
        binding.viewPager.adapter = myViewPagerAdapter
        binding.viewPager.addOnPageChangeListener(viewPagerPageChangeListener)

    }

    private fun setOnClick() {
        binding.btnLogin.setOnClickListener(this)
        binding.btnNext.setOnClickListener(this)
        binding.btnSignUp.setOnClickListener(this)
        binding.btnSkip.setOnClickListener(this)
    }

    private fun addBottomDots(currentPage: Int) {
        dots = arrayOfNulls(layouts!!.size)

        val colorsActive = resources.getIntArray(R.array.array_dot_active)
        val colorsInactive = resources.getIntArray(R.array.array_dot_inactive)

        binding.layoutDots.removeAllViews()
        for (i in dots.indices) {
            dots[i] = TextView(this)
            dots[i]!!.text = Html.fromHtml("&#8226;")
            dots[i]!!.textSize = 35f
            dots[i]!!.setTextColor(colorsInactive[currentPage])
            binding.layoutDots.addView(dots[i])
        }

        if (dots.isNotEmpty())
            dots[currentPage]!!.setTextColor(colorsActive[currentPage])
    }

    private fun getItem(i: Int): Int {
        return binding.viewPager.currentItem + i
    }

    private fun launchHomeScreen(action: String) {
        Utility.setFirstTimeLaunch(applicationContext, false)
        if (Utility.getUserStatus(applicationContext) && Utility.isUserRemembered(applicationContext)) {
            val i = Intent(applicationContext, DashboardActivity::class.java)
            startActivity(i)
            finish()
        } else {
            val i = Intent(applicationContext, LoginActivity::class.java)
            i.putExtra(Constants.USER_ACTION, action)
            startActivity(i)
            finish()
        }

    }

    //  viewpager change listener
    private var viewPagerPageChangeListener: ViewPager.OnPageChangeListener =
        object : ViewPager.OnPageChangeListener {

            override fun onPageSelected(position: Int) {
                addBottomDots(position)

                // changing the next button text 'NEXT' / 'GOT IT'

                when (position) {
                    0 -> {
                        binding.txtNotice.text = resources.getString(R.string.walk_through_1)
                    }
                    1 -> {
                        binding.txtNotice.text = resources.getString(R.string.walk_through_2)
                    }
                    2 -> {
                        binding.txtNotice.text = ""
                        /* val param = binding.txtNotice.layoutParams as RelativeLayout.LayoutParams
                         param.setMargins(0,100,0,0)
                         binding.txtNotice.layoutParams = param

                         binding.txtNotice.text = resources.getString(R.string.walk_through_3)*/
                    }

                }
                if (position == layouts!!.size - 1) {
                    // last page. make button text to GOT IT

                    binding.loginLayout.visibility = View.VISIBLE
                    binding.btnNext.visibility = View.GONE
                    binding.btnSkip.visibility = View.GONE
                } else {
                    // still pages are left
                    binding.btnNext.text = getString(R.string.next)
                    binding.loginLayout.visibility = View.GONE
                    binding.btnSkip.visibility = View.VISIBLE
                    binding.btnNext.visibility = View.VISIBLE
                }
            }

            override fun onPageScrolled(arg0: Int, arg1: Float, arg2: Int) {}

            override fun onPageScrollStateChanged(arg0: Int) {}
        }

    /**
     * Making notification bar transparent
     */
    private fun changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val window = window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            window.statusBarColor = Color.TRANSPARENT
        }
    }


    /**
     * View pager adapter
     */
    inner class MyViewPagerAdapter : PagerAdapter() {
        private var layoutInflater: LayoutInflater? = null

        override fun instantiateItem(container: ViewGroup, position: Int): Any {
            layoutInflater = getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

            val view = layoutInflater!!.inflate(layouts!![position], container, false)
            container.addView(view)

            return view
        }

        override fun getCount(): Int {
            return layouts!!.size
        }

        override fun isViewFromObject(view: View, obj: Any): Boolean {
            return view === obj
        }

        override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
            val view = `object` as View
            container.removeView(view)
        }
    }


    override fun onClick(p0: View?) {
        when (p0!!.id) {
            R.id.btn_login -> {
                binding.btnLogin.setBackgroundResource(R.drawable.button_bg)
                Utility.setFirstTimeLaunch(applicationContext, false)
                val i = Intent(applicationContext, LoginActivity::class.java)
                startActivity(i)
                finish()
            }

            R.id.btn_sign_up -> {
                binding.btnSignUp.setBackgroundResource(R.drawable.button_bg)
                Utility.setFirstTimeLaunch(applicationContext, false)
                val i = Intent(applicationContext, SignUpVideo::class.java)
                startActivity(i)
                //finish()
            }

            R.id.btn_skip -> {
                launchHomeScreen("")
            }

            R.id.btn_next -> {
                // checking for last page
                // if last page home screen will be launched
                val current = getItem(+1)
                if (current < layouts!!.size) {
                    // move to next screen
                    binding.viewPager.currentItem = current
                } else {
                    launchHomeScreen("")
                }
            }

        }

    }
}