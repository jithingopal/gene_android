package com.muhdo.app.ui.signUp

import android.app.AlertDialog
import android.content.Intent
import android.os.Build
import android.util.Base64
import android.util.Log
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModel
import com.muhdo.app.R
import com.muhdo.app.apiModel.login.KitResponse
import com.muhdo.app.databinding.ActivityKitRegistrationBinding
import com.muhdo.app.repository.ApiUtilis
import com.muhdo.app.repository.ErrorModel
import com.muhdo.app.repository.NetworkManager
import com.muhdo.app.repository.ServiceListener
import com.muhdo.app.ui.DashboardActivity
import com.muhdo.app.ui.ThankyouRegistrationActivity
import com.muhdo.app.ui.userrawdata.RawDataResponse
import com.muhdo.app.utils.v3.TempUtil
import com.timingsystemkotlin.backuptimingsystem.Utility
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.*

class KitViewModel : ViewModel() {

    fun onClick(view: View) {
        val binding = DataBindingUtil.findBinding<ActivityKitRegistrationBinding>(view)
        if (checkValidation(view)) {
            Log.e("Error", "Error in Login")

            registerKit(view)
        } else {
            Utility.displayShortSnackBar(binding!!.parentLayout, "Please select gender")
        }
    }


    fun onCheckChanged(binding: ActivityKitRegistrationBinding) {

    }

    private fun checkValidation(view: View): Boolean {

        val binding = DataBindingUtil.findBinding<ActivityKitRegistrationBinding>(view)
        var isValid = true
        when {

            binding!!.edtKitId.text.isEmpty() -> {
                binding.edtKitId.error = view.context.getString(R.string.please_enter_kit_id)
                binding.edtKitId.requestFocus()
                isValid = false
            }
        }
        return isValid
    }


    private fun registerKit(view: View) {
        val binding = DataBindingUtil.findBinding<ActivityKitRegistrationBinding>(view)
        binding!!.progressBar.visibility = View.VISIBLE
        val manager = NetworkManager()

        if (manager.isConnectingToInternet(view.context)) {
            val params = HashMap<String, String>()
            Log.e("Jithin", "Gender:" + TempUtil.getLastSignedUserGender())

            params["id"] = binding.edtKitId.text.toString().toUpperCase()
            params["gender"] = TempUtil.getLastSignedUserGender()
            params["user_id"] = Utility.getUserID(view.context)

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                val current = LocalDateTime.now()
                val formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
                var currentDate: String = current.format(formatter)
                Log.wtf("data", "date oreo" + currentDate)
                params["date"] = currentDate

            } else {
                var date = Date();
                val formatter = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
                val currentDate: String = formatter.format(date)
                Log.wtf("data", "date" + currentDate)
                params["date"] = currentDate
            }
            //params["date"] = "2019-03-29T06:16:14.940Z"

            manager.createApiRequest(
                ApiUtilis.getAPIInstance(view.context).sendKit(params),
                object : ServiceListener<KitResponse> {
                    override fun getServerResponse(response: KitResponse, requestcode: Int) {
                        if (response.getCode() == 200) {
                            displayAlert(view)
                            // val i = Intent(view.context, ThankyouRegistrationActivity::class.java)
                            //view.context.startActivity(i)
                            /*val i = Intent(view.context, DashboardActivity::class.java)
                            view.context.startActivity(i)
                            Utility.saveUserStatus(view.context, true)*/
                        }

                    }

                    override fun getError(error: ErrorModel, requestcode: Int) {
                        binding.progressBar.visibility = View.GONE
                        val json = JSONObject(error.getMessage())
                        if (error.getCode() == 400 && json.has("message") && json.getString("message") == "Bad request. Expected Already activated") {

                            val i = Intent(view.context, DashboardActivity::class.java)
                            view.context.startActivity(i)
                            Utility.saveUserStatus(view.context, true)
                        } else {
                            Utility.displayShortSnackBar(
                                binding.parentLayout,
                                error.getMessage().toString()
                            )
                        }

                    }
                })
        } else {
            binding.progressBar.visibility = View.GONE
            Utility.displayShortSnackBar(
                binding.parentLayout,
                view.context.resources.getString(R.string.check_internet)
            )

        }

    }


    private fun displayAlert(view: View) {
        val dialogBuilder = AlertDialog.Builder(view.context)
        dialogBuilder.setMessage("Kit ID successfully registered")
            .setCancelable(false)
            .setPositiveButton("Ok") { _, _ ->
                // val i = Intent(view.context, DashboardActivity::class.java)
                // view.context.startActivity(i)
                getUserRawData(view)
            }


        val alert = dialogBuilder.create()
        alert.show()
    }


    private fun getUserRawData(view: View) {
        val binding = DataBindingUtil.findBinding<ActivityKitRegistrationBinding>(view)
        binding!!.progressBar.visibility = View.VISIBLE

        val manager = NetworkManager()
        Log.wtf("data", "userid" + Utility.getUserID(view.context));
        val datasd = Base64.decode(Utility.getUserID(view.context), Base64.DEFAULT)
        val decodeUserId = String(datasd, charset("UTF-8"))

        if (manager.isConnectingToInternet(view.context)) {
            manager.createApiRequest(
                ApiUtilis.getAPIInstance(view.context).getUserRawData(decodeUserId),
                object : ServiceListener<RawDataResponse> {
                    override fun getServerResponse(response: RawDataResponse, requestcode: Int) {
                        binding.progressBar.visibility = View.GONE
                        if (response.getCode() == 200) {
                            val i = Intent(view.context, DashboardActivity::class.java)
                            view.context.startActivity(i)
                        } else {
                            /*  Utility.displayShortSnackBar(
                                binding.parentLayout,
                                response.getMessage().toString()
                            )*/
                            val i = Intent(view.context, ThankyouRegistrationActivity::class.java)
                            view.context.startActivity(i)
                        }
                    }

                    override fun getError(error: ErrorModel, requestcode: Int) {
                        // binding.progressBar.visibility = View.GONE
                        binding.progressBar.visibility = View.GONE
                        /*
                        Utility.displayShortSnackBar(
                            binding.parentLayout,
                            error.getMessage().toString()
                        )
                        */
                        val i = Intent(view.context, ThankyouRegistrationActivity::class.java)
                        view.context.startActivity(i)
                    }
                })
        } else {
            // binding.progressBar.visibility = View.GONE
            binding.progressBar.visibility = View.GONE
            Utility.displayShortSnackBar(
                binding.parentLayout,
                view.context.resources.getString(R.string.check_internet)
            )
        }
    }

/*
    @SuppressLint("NewApi")
    private fun registerKit(view: View) {

        val binding = DataBindingUtil.findBinding<ActivityKitRegistrationBinding>(view)
        binding!!.progressBar.visibility = View.VISIBLE


        val manager = NetworkManager2()
        if (manager.isConnectingToInternet(view.context)) {
            val params = HashMap<String, String>()

            params["id"] = binding!!.edtKitId.text.toString()
            params["gender"] = genderValue
            params["user_id"] = Utility.getUserID(view.context)
            params["date"] = "2019-03-29T06:16:14.940Z"
            val apiService = ApiClientNew.getClient()!!.create(ApiInterface::class.java)
            val call = apiService.registerKit(params)
            call.enqueue(object : Callback<KitResponse> {
                override fun onResponse(call: Call<KitResponse>, response: Response<KitResponse>) {
                    binding.progressBar.visibility = View.GONE
                    if (response.body() != null && response.body()!!.getCode() == 200) {
                        val i = Intent(view.context, DashboardActivity::class.java)
                        view.context.startActivity(i)
                        Utility.saveUserStatus(view.context, true)
                    } else if (response.body() != null && response!!.body()!!.getMessage().equals("Bad request. Expected Already activated")) {

                        val i = Intent(view.context, DashboardActivity::class.java)
                        view.context.startActivity(i)
                        Utility.saveUserStatus(view.context, true)
                    }
                    else{
                        Utility.displayShortSnackBar(binding.parentLayout, response.body()!!.getData().toString())
                    }
                }

                override fun onFailure(call: Call<KitResponse>, t: Throwable) {
                    // Log error here since request failed
                    binding.progressBar.visibility = View.GONE
                    Log.e("Login", t.toString())
                    Utility.displayShortSnackBar(
                        binding.parentLayout,
                        "Server connection error!!, please try again later."
                    )
                }
            })
        } else {
            binding.progressBar.visibility = View.GONE

            Utility.displayShortSnackBar(
                binding.parentLayout,
                view.context.resources.getString(R.string.check_internet)
            )

        }

    }
*/
}