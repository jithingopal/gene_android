package com.muhdo.app.ui

import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.muhdo.app.R
import com.muhdo.app.databinding.ActivityThankyouRegistrationBinding
import com.muhdo.app.ui.login.LoginActivity

class ThankyouRegistrationActivity  : AppCompatActivity(){
    lateinit var binding: ActivityThankyouRegistrationBinding
    private var progressDialog: ProgressDialog? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this@ThankyouRegistrationActivity, R.layout.activity_thankyou_registration)

        binding.textViewLogout.setOnClickListener(object :View.OnClickListener{
            override fun onClick(v: View?) {
                finish()
                val i = Intent(applicationContext, LoginActivity::class.java)
                startActivity(i)

            }

        })

    }

}