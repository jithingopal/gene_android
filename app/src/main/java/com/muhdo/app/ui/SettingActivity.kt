package com.muhdo.app.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.muhdo.app.R
import com.muhdo.app.adapter.SettingPagerAdapter
import com.muhdo.app.databinding.ActivitySettingBinding

class SettingActivity : AppCompatActivity() {
    lateinit var binding: ActivitySettingBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this@SettingActivity, R.layout.activity_setting)
        val fragmentAdapter = SettingPagerAdapter(supportFragmentManager)
        binding.viewpagerMain.adapter = fragmentAdapter
        binding.tabsMain.setupWithViewPager(binding.viewpagerMain)
        binding.viewpagerMain.offscreenPageLimit = 0

        binding.toolbar.btnNavigation.setOnClickListener {
            finish()
        }

    }
}