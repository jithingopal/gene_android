package com.muhdo.app.ui

import android.Manifest
import android.annotation.SuppressLint
import android.annotation.TargetApi
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.content.res.ColorStateList
import android.content.res.Resources
import android.graphics.Color
import android.graphics.Typeface
import android.graphics.drawable.ColorDrawable
import android.location.Location
import android.location.LocationManager
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.text.Spannable
import android.text.SpannableString
import android.util.Base64
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.animation.AnimationUtils
import android.widget.PopupWindow
import android.widget.RelativeLayout
import android.widget.Toast
import androidx.cardview.widget.CardView
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.view.GravityCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.multidex.MultiDex
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.crashlytics.android.Crashlytics
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.navigation.NavigationView
import com.muhdo.app.BuildConfig
import com.muhdo.app.R
import com.muhdo.app.adapter.v3.CustomOptionsAdapter
import com.muhdo.app.apiModel.mealModel.MealPlanModel
import com.muhdo.app.apiModel.v3.HomeOptions
import com.muhdo.app.callback.TabSelectedListener
import com.muhdo.app.databinding.ActivityDashboardBinding
import com.muhdo.app.fragment.EpigenticTrackingFragment
import com.muhdo.app.fragment.HomeFragment
import com.muhdo.app.fragment.v3.*
import com.muhdo.app.fragment.v3.account.PersonalInfoFragment
import com.muhdo.app.fragment.v3.dna.DnaResultFragment
import com.muhdo.app.fragment.v3.epigenetics.EpigeneticsHomeFragment
import com.muhdo.app.fragment.v3.guide.meal.MealGuideQuestionnaireFragment
import com.muhdo.app.fragment.v3.guide.workout.ChooseGoalFragment
import com.muhdo.app.fragment.v3.guide.workout.MyExerciseFragment
import com.muhdo.app.fragment.v3.health_insights.HealthInsightsFragment
import com.muhdo.app.interfaces.IFragmentCallback
import com.muhdo.app.repository.ApiUtilis
import com.muhdo.app.repository.ErrorModel
import com.muhdo.app.repository.NetworkManager
import com.muhdo.app.repository.ServiceListener
import com.muhdo.app.ui.login.LoginActivity
import com.muhdo.app.ui.v3.BaseActivityV3
import com.muhdo.app.utils.CustomTypefaceSpan
import com.muhdo.app.utils.PreferenceConnector
import com.muhdo.app.utils.v3.Constants
import com.muhdo.app.utils.v3.TempUtil
import com.muhdo.app.utils.v3.TempUtil.currentFragment
import com.timingsystemkotlin.backuptimingsystem.Utility
import io.fabric.sdk.android.Fabric
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_dashboard.*
import org.jetbrains.anko.toast
import java.util.*


class DashboardActivity : BaseActivityV3(), TabSelectedListener,
    NavigationView.OnNavigationItemSelectedListener, IFragmentCallback,
    GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,
    com.google.android.gms.location.LocationListener,
    CustomOptionsAdapter.CustomOptionsClickListener {
    override fun onTabSelected(position: Int) {
    }

    override fun onItemClick(position: Int, label: String) {
        when (label) {
            getString(R.string.genetic_overview) -> {
                binding.navigationView.selectedItemId = R.id.bottom_nav_home
                currentFragment =
                    TempUtil.NO_FRAGMENTS_SELECTED//binding.navigationView.selectedItemId
                replaceFragment(GeneticOverviewHomeFragment())
            }
            getString(R.string.genetic_action_plan) -> {
                replaceFragment(GeneticActionPlanHomeFragment())
            }
            getString(R.string.dna_results) -> {
                replaceFragment(ResultsFragment())
            }
            getString(R.string.health_insights) -> {
                replaceFragment(HealthInsightsFragment())
            }
            getString(R.string.epigenetic_results) -> {
                replaceFragment(EpigeneticsHomeFragment())
            }
            getString(R.string.more_life_style_tracking) -> {
                //   replaceFragment(EpigenticTrackingFragment())
                toast(R.string.comin_soon)
            }
            getString(R.string.my_meal_guide) -> {
                fetchQuestionnaireStatusAndGoTomealPlan()
            }
            getString(R.string.v3_plan_my_exercise) -> {
                // if (Utility.getWorkoutStatus(this@DashboardActivity)) {
                //   replaceFragment(WorkoutFragment())
                // } else {
                replaceFragment(MyExerciseFragment())
                //   }
            }
        }
        moreOptionDialog.dismiss()
    }

    override fun onNavigationItemSelected(menuItem: MenuItem): Boolean {
        addBottomNavColor()
        when (menuItem.itemId) {
            R.id.nav_home -> {
                binding.navigationView.visibility = View.VISIBLE
                binding.navigationView.selectedItemId = R.id.bottom_nav_home
                currentFragment = binding.navigationView.selectedItemId
                replaceFragment(HomeFragment())
            }
            R.id.nav_feedback -> {
                val i = Intent(applicationContext, TalkToExpertActivity::class.java)
                i.putExtra(TempUtil.EXTRA_FEED_BACK, true)
                startActivity(i)
            }

            R.id.nav_talk_to_experts -> {
                val i = Intent(applicationContext, TalkToExpertActivity::class.java)
                startActivity(i)
            }

            R.id.nav_logout -> {
                currentFragment = R.id.nav_logout
                confirmLogOut()
            }

            R.id.nav_profile -> {

                binding.navigationView.selectedItemId = R.id.bottom_nav_home
                currentFragment = TempUtil.NO_FRAGMENTS_SELECTED
                removeBottomNavColor()
                val personalInfoFragment = PersonalInfoFragment()
                personalInfoFragment.firstSelectPos = 0
                replaceFragment(personalInfoFragment)
            }
            R.id.nav_account_info -> {

                binding.navigationView.selectedItemId = R.id.bottom_nav_home
                currentFragment = TempUtil.NO_FRAGMENTS_SELECTED
                removeBottomNavColor()
                val personalInfoFragment = PersonalInfoFragment()
                personalInfoFragment.firstSelectPos = 3
                replaceFragment(personalInfoFragment)
            }
            R.id.nav_questionnaire -> {

                binding.navigationView.selectedItemId = R.id.bottom_nav_home
                currentFragment = TempUtil.NO_FRAGMENTS_SELECTED
                removeBottomNavColor()
                val personalInfoFragment = PersonalInfoFragment()
                personalInfoFragment.firstSelectPos = 1
                replaceFragment(personalInfoFragment)
            }
            R.id.nav_kit_info -> {

                binding.navigationView.selectedItemId = R.id.bottom_nav_home
                currentFragment = TempUtil.NO_FRAGMENTS_SELECTED
                removeBottomNavColor()
                val personalInfoFragment = PersonalInfoFragment()
                personalInfoFragment.firstSelectPos = 2
                replaceFragment(personalInfoFragment)
            }


        }
        binding.drawerLayout.closeDrawer(GravityCompat.START)
        return true

    }

    fun setResurveyIconVisible(isVisible: Boolean, onClickListener: View.OnClickListener) {
        if (isVisible) {
            binding.toolbar.imgResurvey.visibility = View.VISIBLE
        } else {
            binding.toolbar.imgResurvey.visibility = View.INVISIBLE
        }
        binding.toolbar.imgResurvey.setOnClickListener(onClickListener)

    }

    fun addBottomNavColor() {
        binding.navigationView.getMenu().getItem(0).setChecked(true)
        val colorList = ColorStateList(
            arrayOf(
                intArrayOf(-android.R.attr.state_selected),  // Disabled
                intArrayOf(android.R.attr.state_selected)    // Enabled
            ),
            intArrayOf(
                Color.parseColor("#707070"),     // The color for the Disabled state
                Color.parseColor("#148CB9")        // The color for the Enabled state
            )
        )
        binding.navigationView.setItemTextColor(colorList)
        binding.navigationView.setItemIconTintList(colorList)
    }


    fun removeBottomNavColor() {
        binding.navigationView.getMenu().getItem(1).setChecked(false)
        val colorList = ColorStateList(
            arrayOf(
                intArrayOf(-android.R.attr.state_enabled),  // Disabled
                intArrayOf(android.R.attr.state_enabled)    // Enabled
            ),
            intArrayOf(
                Color.parseColor("#707070"),     // The color for the Disabled state
                Color.parseColor("#707070")        // The color for the Enabled state
            )
        )
        binding.navigationView.setItemTextColor(colorList)
        binding.navigationView.setItemIconTintList(colorList)
    }

    private lateinit var moreOptionDialog: PopupWindow
    lateinit var binding: ActivityDashboardBinding
    private var selectedFragment: Fragment? = null
    private val LOCATION_PERMISSIONS = 123
    private val TAG = "MainActivity"
    private var mGoogleApiClient: GoogleApiClient? = null
    private var mLocationManager: LocationManager? = null
    private var mLocationRequest: LocationRequest? = null
    private val UPDATE_INTERVAL = (2 * 1000).toLong()  /* 10 secs */
    private val FASTEST_INTERVAL: Long = 2000 /* 2 sec */

    lateinit var locationManager: LocationManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Fabric.with(this, Crashlytics())
        binding =
            DataBindingUtil.setContentView(this@DashboardActivity, R.layout.activity_dashboard)
        binding.nvView.setNavigationItemSelectedListener(this)
        binding.navigationView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
        binding.toolbar.btnNavigation.setOnClickListener {
            binding.drawerLayout.openDrawer(GravityCompat.START)
        }
        binding.tvVersionName.setText("Muhdo " + BuildConfig.VERSION_NAME.replace("_", "."))
        binding.nvView.itemIconTintList = null
        Utility.saveLoginStatus(applicationContext, true)

        MultiDex.install(this)

        mGoogleApiClient = GoogleApiClient.Builder(this)
            .addConnectionCallbacks(this)
            .addOnConnectionFailedListener(this)
            .addApi(LocationServices.API)
            .build()

        mLocationManager = this.getSystemService(Context.LOCATION_SERVICE) as LocationManager

        checkLocation()

        checkAndRequestPermissions()

        val m = binding.nvView.menu
        for (i in 0 until m.size()) {
            val mi = m.getItem(i)

            //for applying RecipeIngredient font to subMenu ...
            val subMenu = mi.subMenu
            if (subMenu != null && subMenu.size() > 0) {
                for (j in 0 until subMenu.size()) {
                    val subMenuItem = subMenu.getItem(j)
                    applyFontToMenuItem(subMenuItem)
                }
            }

            //the method we have create in activity
            applyFontToMenuItem(mi)
        }

        binding.navigationView.selectedItemId = R.id.bottom_nav_home
        currentFragment = binding.navigationView.selectedItemId
        when (intent.getStringExtra(Constants.DASHBOARD_ACTION)) {
            Constants.DASHBOARD_RESULT -> {
                binding.navigationView.selectedItemId = R.id.bottom_nav_results
                currentFragment = binding.navigationView.selectedItemId
//                replaceFragment(ChooseModeFragment())
                val title: String? = PreferenceConnector.readString(
                    applicationContext,
                    PreferenceConnector.MODE_TEXT,
                    "Weight Loss"
                )
                val id: String? = PreferenceConnector.readString(
                    applicationContext,
                    PreferenceConnector.MODE_ID,
                    "5c8c96a79c54381add6884dc"
                )
                val resultsFragment = ResultsFragment()
                TempUtil.log("title", "Title=" + title)
                TempUtil.log("ID", "id=" + id)
                val getId = intent.getStringExtra("id")
                val getTitle = intent.getStringExtra("title")
                if (getId != null && getTitle != null) {
                    resultsFragment.setData(getId, getTitle)
                } else
                    resultsFragment.setData(id!!, title!!)
//                    replaceFragment(resultsFragment)
                selectedFragment = resultsFragment
            }
            Constants.DASHBOARD_EPIGENETIC_RESULT -> {
                binding.navigationView.selectedItemId = R.id.bottom_nav_epigentic
                currentFragment = binding.navigationView.selectedItemId
                replaceFragment(EpigeneticsHomeFragment())
            }
            Constants.DASHBOARD_HEALTHINSIGHT -> {
                binding.navigationView.selectedItemId = R.id.bottom_nav_insights
                currentFragment = binding.navigationView.selectedItemId
                replaceFragment(HealthInsightsFragment())
            }
            Constants.LIFESTYLE_TRACKING -> {
                /*val dnaResultFragment =
                    DnaResultFragment()
                binding.navigationView.selectedItemId = R.id.bottom_nav_results
                currentFragment = binding.navigationView.selectedItemId
                replaceFragment(dnaResultFragment)
                selectedFragment = dnaResultFragment*/
                toast(R.string.comin_soon)
            }
            Constants.DASHBOARD_MEAL_PLAN -> {
                binding.navigationView.selectedItemId = R.id.bottom_nav_meal
                currentFragment = binding.navigationView.selectedItemId
                fetchQuestionnaireStatusAndGoTomealPlan()
            }
            Constants.DASHBOARD_WORKOUT -> {
                //    binding.navigationView.selectedItemId = R.id.bottom_nav_more
                currentFragment = TempUtil.NO_FRAGMENTS_SELECTED
                removeBottomNavColor()
                Log.wtf("data", "workout status" + Utility.getWorkoutStatus(this@DashboardActivity))
                if (Utility.getWorkoutStatus(this@DashboardActivity)) {
                    replaceFragment(MyExerciseFragment())
                } else {
                    replaceFragment(MyExerciseFragment())
                }

            }
            Constants.DASHBOARD_WORKOUT_MY_PLAN -> {
                binding.navigationView.selectedItemId = R.id.bottom_nav_workout
                currentFragment = binding.navigationView.selectedItemId
                Log.wtf("data", "workout status" + Utility.getWorkoutStatus(this@DashboardActivity))
                if (Utility.getWorkoutStatus(this@DashboardActivity)) {
                    TempUtil.log("WorkoutError", "Replacing workout fragment")

                    replaceFragment(WorkoutFragment())

                } else {
                    replaceFragment(MyExerciseFragment())
                }

            }
            Constants.DASHBOARD_EPIGENETIC -> {
                binding.navigationView.selectedItemId = R.id.bottom_nav_epigentic
                currentFragment = binding.navigationView.selectedItemId
                replaceFragment(EpigenticTrackingFragment())
            }
            Constants.DASHBOARD_GENETIC_OVERVIEW -> {
//                currentFragment = binding.navigationView.selectedItemId
                replaceFragment(GeneticOverviewHomeFragment())

            }
        }

    }

    private fun checkLocation(): Boolean {
        if (!isLocationEnabled())
            showAlert()
        return isLocationEnabled()
    }


    private fun checkAndRequestPermissions(): Boolean {

        val locationPermission = ContextCompat.checkSelfPermission(
            this@DashboardActivity,
            Manifest.permission.ACCESS_FINE_LOCATION
        )
        val listPermissionsNeeded = ArrayList<String>()

        if (locationPermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.ACCESS_FINE_LOCATION)
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(
                this@DashboardActivity,
                listPermissionsNeeded.toTypedArray(),
                LOCATION_PERMISSIONS
            )
            return false
        }
        return true
    }


    @TargetApi(Build.VERSION_CODES.M)
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        when (requestCode) {
            LOCATION_PERMISSIONS -> {

                val neverAskagainList = ArrayList<String>()

                if (grantResults.isNotEmpty()) {
                    for (i in grantResults.indices) {
                        if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                            if (!shouldShowRequestPermissionRationale(permissions[i])) {
                                if (permissions[i].equals(
                                        Manifest.permission.ACCESS_FINE_LOCATION,
                                        ignoreCase = true
                                    )
                                ) {
                                    neverAskagainList.add("Location")
                                }
                            }
                        }
                    }

                    if (neverAskagainList.size > 0) {
                        val snackBarString = StringBuilder()
                        for (perm in neverAskagainList) {
                            if (snackBarString.isNotEmpty()) {
                                snackBarString.append(", $perm")
                            } else {
                                snackBarString.append(perm)
                            }
                        }
//                        Utility.displayShortSnackBar(null, "Location" + " access Denied. Please Enable it from Settings")
                    } else {
                        checkAndRequestPermissions()
                    }
                }
            }
            else -> super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }

    }


    override fun onConnected(p0: Bundle?) {

        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {

            return
        }


        startLocationUpdates()

        val fusedLocationProviderClient:
                FusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this)
        fusedLocationProviderClient.getLastLocation()
            .addOnSuccessListener {

                if (Utility.getLat(applicationContext).isNullOrEmpty() && Utility.getLong(
                        applicationContext
                    ).isNullOrEmpty()
                ) {
                    Utility.saveLatLong(
                        applicationContext,
                        it.latitude.toString(),
                        it.longitude.toString()
                    )
//                    Toast.makeText(this, "fetch location", Toast.LENGTH_SHORT).show()
                }

//                Toast.makeText(this@LoginActivity, "Locatiopn ==> " + it.latitude, Toast.LENGTH_LONG).show()
            }
    }


    private fun isLocationEnabled(): Boolean {
        locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(
            LocationManager.NETWORK_PROVIDER
        )
    }

    private fun showAlert() {
        val dialog = AlertDialog.Builder(this)
        dialog.setTitle(R.string.v3_enable_location)
            .setMessage(R.string.v3_enable_location_message)
            .setPositiveButton(
                R.string.v3_location_settings
            ) { paramDialogInterface, paramInt ->
                val myIntent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                startActivity(myIntent)
            }
            .setNegativeButton(
                R.string.btn_cancel
            ) { paramDialogInterface, paramInt -> }
        dialog.show()
    }

    protected fun startLocationUpdates() {

        // Create the location request
        mLocationRequest = LocationRequest.create()
            .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
            .setInterval(UPDATE_INTERVAL)
            .setFastestInterval(FASTEST_INTERVAL)
        // Request location updates
        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            return
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(
            mGoogleApiClient,
            mLocationRequest, this
        )
    }

    override fun onStart() {
        super.onStart()
        mGoogleApiClient?.connect()
    }

    override fun onStop() {
        super.onStop()

        if (mGoogleApiClient?.isConnected!!) {
            mGoogleApiClient?.disconnect()
        }
    }

    override fun onConnectionSuspended(p0: Int) {

        Log.i(TAG, "Connection Suspended")
        mGoogleApiClient?.connect()
    }

    override fun onConnectionFailed(connectionResult: ConnectionResult) {
        Log.i(TAG, "Connection failed. Error: " + connectionResult.getErrorCode())
    }

    override fun onLocationChanged(p0: Location?) {

        if (Utility.getLat(applicationContext).isNullOrEmpty() && Utility.getLong(applicationContext).isNullOrEmpty()) {
            if (p0?.longitude != null && p0?.latitude != null) {
                Utility.saveLatLong(
                    applicationContext,
                    p0.latitude.toString(),
                    p0.longitude.toString()
                )
            } else {
                Toast.makeText(this, R.string.v3_unable_to_fetch_location, Toast.LENGTH_SHORT)
                    .show()
            }
        }
    }


    override fun onQuickAccessTabClicked(
        fragment: Fragment,
        nav_id: Int,
        string: String
    ) {
        goToFragment(fragment, nav_id, string)

    }


    private fun goToFragment(selectedFragment: Fragment?, item: Int, string: String) {
        if (selectedFragment != null) {
            val arguments = Bundle()
            arguments.putString("shareData", string)
            selectedFragment.arguments = arguments
            val transaction = supportFragmentManager.beginTransaction()
            transaction.replace(R.id.flContent, selectedFragment)
            transaction.addToBackStack("share")
            transaction.commit()
            currentFragment = item
        }
    }

    override fun onResume() {
        super.onResume()
        if (currentFragment == R.id.bottom_nav_home) {
            replaceFragment(HomeFragment())
        }
    }

    private val mOnNavigationItemSelectedListener =
        BottomNavigationView.OnNavigationItemSelectedListener { item ->
            addBottomNavColor()

            when (item.itemId) {
                R.id.bottom_nav_home -> {
//                    currentFragment = item.itemId
                    selectedFragment = HomeFragment()
                }

                R.id.bottom_nav_epigentic -> {
                    selectedFragment = EpigeneticsHomeFragment()
                }
                R.id.bottom_nav_results -> {
                    selectedFragment = DnaResultFragment()
                }
                R.id.bottom_nav_insights -> {
                    selectedFragment = HealthInsightsFragment()
                }
                R.id.bottom_nav_more -> {
                    createMoreOptionsAndShow()
                }
                else -> {
                    selectedFragment = HomeFragment()
                }
            }

            if (selectedFragment != null && currentFragment != item.itemId && item.itemId != R.id.bottom_nav_more) {
                val transaction = supportFragmentManager.beginTransaction()
                transaction.replace(R.id.flContent, selectedFragment!!)
                transaction.commit()
                if (selectedFragment is HomeFragment)
                    currentFragment = R.id.bottom_nav_home
                else
                    currentFragment = item.itemId
            }
            return@OnNavigationItemSelectedListener true
        }


    private fun applyFontToMenuItem(mi: MenuItem) {
        val font = Typeface.createFromAsset(assets, "roboto_regular.ttf")
        val mNewTitle = SpannableString(mi.title)
        mNewTitle.setSpan(
            CustomTypefaceSpan("", font),
            0,
            mNewTitle.length,
            Spannable.SPAN_INCLUSIVE_INCLUSIVE
        )
        mi.title = mNewTitle
        if (mi.title.toString().equals("Log Out")) {
            Log.d("data", "menu" + mi.title)
            val font = Typeface.createFromAsset(assets, "roboto_medium.ttf")
            val mNewTitle = SpannableString(mi.title)
            mNewTitle.setSpan(
                CustomTypefaceSpan("", font),
                0,
                mNewTitle.length,
                Spannable.SPAN_INCLUSIVE_INCLUSIVE
            )
            mi.title = mNewTitle
        }
    }

    @SuppressLint("WrongConstant")
    fun replaceFragment(fragment: Fragment) {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.flContent, fragment)
        transaction.addToBackStack("share")
        transaction.commit()
    }

    override fun onBackPressed() {
        if (binding.drawerLayout.isDrawerOpen(GravityCompat.START)) {
            binding.drawerLayout.closeDrawer(GravityCompat.START)
            return
        }
        if (currentFragment == R.id.bottom_nav_home) {
            val intent = Intent(Intent.ACTION_MAIN)
            intent.addCategory(Intent.CATEGORY_HOME)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
        } else {
            selectedFragment = HomeFragment()
            currentFragment = R.id.bottom_nav_home
            replaceFragment(selectedFragment as HomeFragment)
            binding.navigationView.getMenu().getItem(0).isChecked = true
        }
    }

    private fun confirmLogOut() {
        val dialogBuilder = AlertDialog.Builder(this@DashboardActivity)
        dialogBuilder.setMessage(R.string.v3_are_you_sure_logout)
            .setCancelable(false)
            .setPositiveButton(R.string.option_yes) { _, _ ->

                PreferenceConnector.getEditor(this@DashboardActivity).clear().apply()
                Utility.setMealStatus(this, false)
                Utility.setWorkoutStatus(this, false)
                Utility.saveUserStatus(applicationContext, false)
                Utility.setKitID(applicationContext, "")
                Utility.saveLoginStatus(applicationContext, false)
                finish()
                val i = Intent(applicationContext, LoginActivity::class.java)
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP) // To clean up all activities
                startActivity(i)
            }
            .setNegativeButton(R.string.option_no) { dialog, _ ->
                dialog.dismiss()
            }
        val alert = dialogBuilder.create()
        alert.show()
    }

    private fun createMoreOptionsAndShow() {
        val dialogView =
            LayoutInflater.from(this@DashboardActivity).inflate(R.layout.more_options_home, null)
        moreOptionDialog = PopupWindow(
            dialogView,
            RelativeLayout.LayoutParams.MATCH_PARENT,
            RelativeLayout.LayoutParams.MATCH_PARENT
        )
        moreOptionDialog.contentView = dialogView
        moreOptionDialog.setBackgroundDrawable(
            ColorDrawable(Color.TRANSPARENT)
        )
        val cardView = dialogView.findViewById<CardView>(R.id.card_view)
        val listView = dialogView.findViewById<RecyclerView>(R.id.rv_more_options)
        val rlMoreOptionsContainer =
            dialogView.findViewById<RelativeLayout>(R.id.rl_more_options_container)
        rlMoreOptionsContainer.setOnClickListener {
            moreOptionDialog.dismiss()
        }
        val adapter = CustomOptionsAdapter(createMoreOptionsMenuList(), this)
        listView.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        listView.adapter = adapter
        moreOptionDialog.isOutsideTouchable = true
        moreOptionDialog.isFocusable = true
        //  moreOptionDialog.showAtLocation(navigationView, Gravity.NO_GRAVITY, width, height)
        moreOptionDialog.showAtLocation(navigationView, Gravity.NO_GRAVITY, 0, 0)
        cardView.setAnimation(AnimationUtils.loadAnimation(this, R.anim.dialog_more_options));
    }

    private fun createMoreOptionsMenuList(): List<HomeOptions> {
         val options1 = HomeOptions(
            R.drawable.v3_ic_genetic_action_plan,
            getString(R.string.genetic_action_plan)
        )
        val options2 =
            HomeOptions(
                R.drawable.v3_ic_epigenetic_tracking,
                getString(R.string.more_life_style_tracking)
            )
        val options3 = HomeOptions(R.drawable.meal_off, getString(R.string.my_meal_guide))
        val options4 = HomeOptions(R.drawable.workout_off, getString(R.string.v3_plan_my_exercise))
        val optionsList = arrayListOf<HomeOptions>()
        optionsList.add(options1)
        optionsList.add(options2)
        optionsList.add(options3)
        optionsList.add(options4)
        return optionsList
    }

    fun dpToPx(dp: Int): Int {
        val density = Resources.getSystem().displayMetrics.density
        return Math.round(dp * density)
    }

    @SuppressLint("CheckResult")
    private fun fetchQuestionnaireStatusAndGoTomealPlan() {

        binding.progressBar.visibility = View.VISIBLE
        val userId = Base64.decode(Utility.getUserID(themedContext), Base64.DEFAULT)
        val decodeUserId = String(userId, charset("UTF-8"))
        ApiUtilis.getAPIInstance(applicationContext).getQuestionnaireStatus(userID = decodeUserId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ success ->
                run {
                    binding.progressBar.visibility = View.GONE
                    var isQuestionnaireCompleted: Boolean = false;

                    if (success.data?.get(0)?.status!! &&
                        success.data[1].status &&
                        success.data[2].status &&
                        success.data[3].status
                    ) {
                        isQuestionnaireCompleted = true
                    }
                    if (isQuestionnaireCompleted) {

                        binding.navigationView.menu.getItem(4).isChecked = true

                        currentFragment = TempUtil.NO_FRAGMENTS_SELECTED
                        if (Utility.getMealStatus(this@DashboardActivity)) {
                            replaceFragment(MealFragment())
                        } else {
                            val chooseGoalFragment = ChooseGoalFragment()
                            chooseGoalFragment.setGoToFragment(MealGuideQuestionnaireFragment());
                            replaceFragment(chooseGoalFragment)
//
                        }
//                        goToMealPlan()
                    } else {
                        val dialog: androidx.appcompat.app.AlertDialog.Builder =
                            androidx.appcompat.app.AlertDialog.Builder(themedContext)
                        dialog.setMessage(R.string.fill_questionnaire_warning_before_create_meal_plan)
                        dialog.setPositiveButton(
                            R.string.btn_go_to_questionnaire_warning
                        ) { dialog, id ->
                            dialog.cancel()
                            currentFragment = TempUtil.NO_FRAGMENTS_SELECTED
                            replaceFragment(QuestionnaireHomeFragment())
                            removeBottomNavColor()
                            dialog.cancel()
                        }
                        dialog.setNegativeButton(
                            R.string.btn_cancel
                        ) { dialog, id ->
                            dialog.cancel()
                            navigationView.getMenu().getItem(0).setChecked(true);
                        }
                        dialog.show()
                    }
                }
            }, {
                run {
                    binding.progressBar.visibility = View.GONE
                }
            })
    }

    //this function is for check if there is any previous meal plan already available
    private fun goToMealPlan() {
        binding.progressBar.visibility = View.VISIBLE
        val manager = NetworkManager()
        if (manager.isConnectingToInternet(this)) {
            manager.createApiRequest(
                ApiUtilis.getAPIInstance(applicationContext).getMealPlanV3(
                    Utility.getUserID(this)
                ),
                object : ServiceListener<MealPlanModel> {
                    override fun getServerResponse(response: MealPlanModel, requestcode: Int) {
                        binding.progressBar.visibility = View.GONE
                        try {
                            if (response.getData()?.getMonday()!!.size > 0) {
                                replaceFragment(MealFragment())
                            } else {
                                val chooseGoalFragment = ChooseGoalFragment()
                                chooseGoalFragment.setGoToFragment(
                                    MealGuideQuestionnaireFragment()
                                )
                                replaceFragment(chooseGoalFragment)
                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }

                    override fun getError(error: ErrorModel, requestcode: Int) {
                        binding.progressBar.visibility = View.GONE
                        if (Utility.getMealStatus(this@DashboardActivity)) {
                            replaceFragment(MealFragment())
                        } else {
                            val chooseGoalFragment = ChooseGoalFragment()
                            chooseGoalFragment.setGoToFragment(MealGuideQuestionnaireFragment())
                            replaceFragment(chooseGoalFragment)
                        }
                    }
                })
        } else {
            binding.progressBar.visibility = View.GONE
            Utility.displayShortSnackBar(
                binding.parentLayout,
                resources.getString(R.string.check_internet)
            )
        }
    }

}