package com.muhdo.app.ui.epigentic.execrise.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.muhdo.app.ui.epigentic.execrise.model.ExerciseInfo

class ExecriseResponseModel {

    @SerializedName("code")
    @Expose
    private var code: Int? = null
    @SerializedName("message")
    @Expose
    private var message: String? = null
    @SerializedName("data")
    private var data: ExerciseInfo? = null



    fun getCode(): Int? {
        return code
    }

    fun setCode(code: Int?) {
        this.code = code
    }

    fun getMessage(): String? {
        return message
    }

    fun setMessage(message: String) {
        this.message = message
    }


    fun getData():ExerciseInfo? {
        return data
    }

    fun setData(data: ExerciseInfo) {
        this.data = data
    }

}