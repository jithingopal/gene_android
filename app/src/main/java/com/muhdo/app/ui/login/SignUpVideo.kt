package com.muhdo.app.ui.login

import android.app.Activity
import android.content.Intent
import android.content.pm.ActivityInfo
import android.content.res.Configuration
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.DocumentsContract.EXTRA_ORIENTATION
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.databinding.DataBindingUtil
import com.ct7ct7ct7.androidvimeoplayer.listeners.VimeoPlayerReadyListener
import com.ct7ct7ct7.androidvimeoplayer.listeners.VimeoPlayerStateListener
import com.ct7ct7ct7.androidvimeoplayer.model.PlayerState
import com.ct7ct7ct7.androidvimeoplayer.model.TextTrack
import com.ct7ct7ct7.androidvimeoplayer.view.VimeoPlayerActivity
import com.ct7ct7ct7.androidvimeoplayer.view.VimeoPlayerView
import com.muhdo.app.R
import com.muhdo.app.databinding.ActivitySignUpVideoBinding

import com.muhdo.app.ui.signUp.SignUpVideoFullScreenActivity
import com.muhdo.app.ui.v3.BaseActivityV3
import kotlinx.android.synthetic.main.activity_sign_up_video.*
import kotlinx.android.synthetic.main.workout_child_layout.*




class SignUpVideo : BaseActivityV3() {
    var context=this
    var REQUEST_CODE = 1234
    val REQUEST_ORIENTATION_AUTO = "REQUEST_ORIENTATION_AUTO"
    val REQUEST_ORIENTATION_PORTRAIT = "REQUEST_ORIENTATION_PORTRAIT"
    val REQUEST_ORIENTATION_LANDSCAPE = "REQUEST_ORIENTATION_LANDSCAPE"
    private var orientation = null;
    lateinit var binding: ActivitySignUpVideoBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_sign_up_video)
        binding = DataBindingUtil.setContentView(this@SignUpVideo, R.layout.activity_sign_up_video)
        context.lifecycle.addObserver(binding.vimeoPlayerView)
        val link1 = "https://vimeo.com/341308382"
        binding.vimeoPlayerView.initialize(341308382,link1)
        if (REQUEST_ORIENTATION_PORTRAIT == orientation) {
            requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        } else if (REQUEST_ORIENTATION_LANDSCAPE == orientation) {
            requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
        }

        binding.vimeoPlayerView.addStateListener(object : VimeoPlayerStateListener {
            override fun onPlaying(duration: Float) {
                Log.d("data","play video")
               binding.vimeoPlayerView.visibility=View.VISIBLE
                binding.btnContinue.visibility=View.GONE
                //videoView.play()
            }

            override fun onPaused(seconds: Float) {
                Log.d("data","pause video")
                binding.btnContinue.visibility=View.VISIBLE
            }

            override fun onEnded(duration: Float) {
                binding.btnContinue.visibility=View.VISIBLE
            }
        })
      binding.imageViewPlayButton.setOnClickListener(object : View.OnClickListener{
            override fun onClick(v: View?) {
                binding.vimeoPlayerView.visibility=View.VISIBLE
                var requestOrientation = VimeoPlayerActivity.REQUEST_ORIENTATION_LANDSCAPE
//                startActivityForResult(VimeoPlayerActivity.createIntent(context, requestOrientation, binding.vimeoPlayerView), REQUEST_CODE)
                binding.imageViewPlayButton.visibility=View.GONE
                binding.vimeoPlayerView.play()
            }
        })
        binding.vimeoPlayerView.setFullscreenVisibility(true)
        binding.vimeoPlayerView.setFullscreenClickListener(object:View.OnClickListener{
            override fun onClick(v: View?) {
                //// videoView.setFullscreenVisibility(true);
               //  requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
                //// setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

                // commented by Rakesh
                //define the orientation
                startActivityForResult(VimeoPlayerActivity.createIntent(context,
                    requestedOrientation.toString(), binding.vimeoPlayerView), REQUEST_CODE)
            }
        })

     binding.btnContinue.setOnClickListener(object : View.OnClickListener{
         override fun onClick(v: View?) {
             val i = Intent(applicationContext, SignUpActivity::class.java)
             startActivity(i)
             finish()
         }

     })
       binding.llBack.setOnClickListener(object : View.OnClickListener{
            override fun onClick(v: View?) {
                finish()
            }

        })
    }

/*
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        Log.d("data", "onActivityResult")

        if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_CODE) {
            var playAt = data!!.getFloatExtra(VimeoPlayerActivity.RESULT_STATE_VIDEO_PLAY_AT, 0f)
            vimeoPlayer.seekTo(playAt)

            var playerState = PlayerState.valueOf(data!!.getStringExtra(VimeoPlayerActivity.RESULT_STATE_PLAYER_STATE))
            when (playerState) {
                PlayerState.PLAYING -> vimeoPlayer.play()
                PlayerState.PAUSED -> vimeoPlayer.pause()
            }
        }

    }
*/

    override fun onBackPressed() {
        // super.onBackPressed()
    }

    override fun onStart() {
        super.onStart()
        binding.vimeoPlayerView.play()
    }


    override fun onStop() {
        super.onStop()
        Log.d("data","onStop")
        binding.vimeoPlayerView.play()
    }

    override fun onPause() {
        super.onPause()
        Log.d("data","onPause")
        binding.vimeoPlayerView.play()
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d("data","onDestroy")
        binding.vimeoPlayerView.play()

    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        val orientation = resources.configuration.orientation
        if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
            // In landscape
            Log.d("data","land")
        }
        else {
            Log.d("data","port")
            // In portrait
        }

        /*if (REQUEST_ORIENTATION_AUTO == orientation) {
            //vimeoPlayerView.reset();
            binding.vimeoPlayerView.play()
        }*/
    }

}
