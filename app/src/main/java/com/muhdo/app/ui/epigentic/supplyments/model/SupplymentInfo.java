package com.muhdo.app.ui.epigentic.supplyments.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class SupplymentInfo  implements Serializable, Comparable<SupplymentInfo>  {

    @SerializedName("_id")
    @Expose
    private String id;

    @SerializedName("user_id")
    @Expose
    private String userId;

    @SerializedName("supplement_id")
    @Expose
    private String supplementId;

    @SerializedName("category")
    @Expose
    private String category;

    @SerializedName("micronutrient_group")
    @Expose
    private String micronutrientGroup;

    @SerializedName("ingredient")
    @Expose
    private String ingredientName;

    @SerializedName("dosage")
    @Expose
    private String dosage;

    @SerializedName("quantity")
    @Expose
    private String quantity;

    @SerializedName("created_at")
    @Expose
    private String createdDate;

    @SerializedName("updated_at")
    @Expose
    private String updateDate;

    @SerializedName("__v")
    @Expose
    private String v;

    @SerializedName("date_group")
    @Expose
    private String dateGroup;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getSupplementId() {
        return supplementId;
    }

    public void setSupplementId(String supplementId) {
        this.supplementId = supplementId;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getMicronutrientGroup() {
        return micronutrientGroup;
    }

    public void setMicronutrientGroup(String micronutrientGroup) {
        this.micronutrientGroup = micronutrientGroup;
    }

    public String getIngredientName() {
        return ingredientName;
    }

    public void setIngredientName(String ingredientName) {
        this.ingredientName = ingredientName;
    }

    public String getDosage() {
        return dosage;
    }

    public void setDosage(String dosage) {
        this.dosage = dosage;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }

    public String getV() {
        return v;
    }

    public void setV(String v) {
        this.v = v;
    }

    public String getDateGroup() {
        return dateGroup;
    }

    public void setDateGroup(String dateGroup) {
        this.dateGroup = dateGroup;
    }

    @Override
    public int compareTo(SupplymentInfo o) {
        return this.getIngredientName().compareTo(o.ingredientName);
    }
}
