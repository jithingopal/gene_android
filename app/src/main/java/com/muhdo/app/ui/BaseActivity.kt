package com.muhdo.app.ui

import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.muhdo.app.AppApplication
import com.muhdo.app.R
import com.muhdo.app.apiModel.v3.firebase.ForceUpdateEvent
import com.muhdo.app.helper.dagger.AppHelper
import com.muhdo.app.utils.PreferenceConnector
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import javax.inject.Inject

open class BaseActivity : AppCompatActivity() {

    @Inject
    lateinit var appHelper: AppHelper

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        (applicationContext as AppApplication).appComponent.inject(this)
    }


}