package com.muhdo.app.ui.adapter;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import androidx.recyclerview.widget.RecyclerView;
import com.bumptech.glide.Glide;
import com.muhdo.app.R;
import com.muhdo.app.apiModel.result.SleepScore;
import com.muhdo.app.ui.ResultChooseMode;
import com.pixplicity.sharp.Sharp;
import okhttp3.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class FoodDetailAdapter2 extends RecyclerView.Adapter<FoodDetailAdapter2.MyViewHolder>{
    private List<SleepScore.RecipeDetail> foodDetailList1;
    public Activity mContext;
    public FoodDetailAdapter2(@Nullable ArrayList<SleepScore.RecipeDetail> foodList1, @NotNull Activity mContext) {
        this.foodDetailList1 = foodList1;
        this.mContext = mContext;
    }

    /**
     * View holder class
     */
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView imageViewFood;
        public TextView textViewFoodName, textViewFoodQuantity,textViewFoodQuantityGram;
        public MyViewHolder(View view) {
            super(view);
            imageViewFood = (ImageView) view.findViewById(R.id.imageView_Foods);
            textViewFoodName = (TextView) view.findViewById(R.id.textView_FoodName);
            textViewFoodQuantity = (TextView) view.findViewById(R.id.textView_FoodQuantity);
            textViewFoodQuantityGram = (TextView) view.findViewById(R.id.textView_FoodQuantityGram);
        }
    }

    @Override
    public void onBindViewHolder(final FoodDetailAdapter2.MyViewHolder holder, final int position) {
        SleepScore.RecipeDetail foods = foodDetailList1.get(position);
        Log.d("data","recipe "+foods.getRecipeDetailName());
        holder.textViewFoodName.setText(foods.getRecipeDetailName());
        Glide
                .with(mContext)
                .load("https://s3.amazonaws.com/dev-recipe-images/"+foods.getRecipeDetailImage())
                .centerCrop()
                .placeholder(R.drawable.diet_1)
                .into(holder.imageViewFood);

    }

    @Override
    public int getItemCount() {
        return foodDetailList1.size();
    }

    @Override
    public FoodDetailAdapter2.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_recipe_detail, parent, false);
        return new FoodDetailAdapter2.MyViewHolder(v);
    }



}

