package com.muhdo.app.ui.epigentic.food.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FoodCategoryResponse {

    @SerializedName("code")
    @Expose
    private String statusCode;

    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("data")
    @Expose
    private FoodCategoryResponseData data;

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public FoodCategoryResponseData getData() {
        return data;
    }
    public void setData(FoodCategoryResponseData data) {
        this.data = data;
    }
}
