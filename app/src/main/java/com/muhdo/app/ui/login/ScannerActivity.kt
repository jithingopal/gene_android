package com.muhdo.app.ui.login

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Handler
import android.util.Base64
import android.util.Log
import android.view.View
import android.view.View.VISIBLE
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat.checkSelfPermission
import androidx.databinding.DataBindingUtil
import com.facebook.FacebookSdk
import com.facebook.appevents.AppEventsLogger
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.google.zxing.Result
import com.muhdo.app.R
import com.muhdo.app.apiModel.login.KitRegistrationResponse
import com.muhdo.app.databinding.ActivityScannerBinding
import com.muhdo.app.repository.*
import com.muhdo.app.ui.DashboardActivity
import com.muhdo.app.ui.ThankyouRegistrationActivity
import com.muhdo.app.ui.userrawdata.RawDataResponse
import com.muhdo.app.utils.PreferenceConnector
import com.muhdo.app.utils.v3.Constants
import com.timingsystemkotlin.backuptimingsystem.Utility
import me.dm7.barcodescanner.zxing.ZXingScannerView
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ScannerActivity : AppCompatActivity(), ZXingScannerView.ResultHandler {
    lateinit var binding: ActivityScannerBinding
    private val FLASH_STATE = "FLASH_STATE"

    private var mScannerView: ZXingScannerView? = null
    private var mFlash: Boolean = false

    @Suppress("DEPRECATED_IDENTITY_EQUALS")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        FacebookSdk.sdkInitialize(applicationContext)
        AppEventsLogger.activateApp(this)
        binding = DataBindingUtil.setContentView(this@ScannerActivity, R.layout.activity_scanner)


        if (checkSelfPermission(
                this,
                Manifest.permission.CAMERA
            ) !== PackageManager.PERMISSION_GRANTED
        ) {
            run {
                Toast.makeText(
                    this,
                    "Please grant camera permission to scan the kit",
                    Toast.LENGTH_SHORT
                )
                    .show()
                val intent = Intent(Intent.ACTION_MAIN)
                intent.addCategory(Intent.CATEGORY_HOME)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(intent)
            }
        }

        getKitRegistrationStatus()
        mScannerView = ZXingScannerView(this)
        binding.contentFrame.addView(mScannerView)
        binding.btnFacebook.setOnClickListener {
            val i = Intent(applicationContext, KitRegistrationActivity::class.java)
            i.putExtra(Constants.KIT_ID_VALUE, "")
            startActivity(i)
        }
    }


    override fun onResume() {
        super.onResume()
        mScannerView!!.setResultHandler(this)
        mScannerView!!.startCamera()
        mScannerView!!.flash = mFlash
    }

    override fun onPause() {
        super.onPause()
        mScannerView!!.stopCamera()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putBoolean(FLASH_STATE, mFlash)
    }

    override fun handleResult(rawResult: Result) {
        val i = Intent(applicationContext, KitRegistrationActivity::class.java)
        i.putExtra(Constants.KIT_ID_VALUE, rawResult.text.toString())
        startActivity(i)


        // Note:
        // * Wait 2 seconds to resume the preview.
        // * On older devices continuously stopping and resuming camera preview can result in freezing the app.
        // * I don't know why this is the case but I don't have the time to figure out.
        val handler = Handler()
        handler.postDelayed({ mScannerView!!.resumeCameraPreview(this@ScannerActivity) }, 2000)
    }

    fun toggleFlash(v: View) {
        mFlash = !mFlash
        mScannerView!!.flash = mFlash
        if (mFlash) {
            binding.btnToggleFlash.setText(R.string.v3_btn_flash_off)
        } else {
            binding.btnToggleFlash.setText(R.string.v3_btn_flash_on)
        }

    }

    private fun getKitRegistrationStatus() {
        //binding!!.progressBar.visibility = View.VISIBLE
        val manager = NetworkManager2()
        if (manager.isConnectingToInternet(applicationContext)) {
            ApiUtilis.getAPIInstance(applicationContext).getKitStatus(Utility.getUserID(applicationContext))
                .enqueue(object : Callback<JsonObject> {
                    override fun onResponse(
                        call: Call<JsonObject>,
                        response: Response<JsonObject>
                    ) {

                        var js: JsonObject? = response.body()
                        var jsonResult: JSONObject = JSONObject(js.toString())
                        Log.e("Jithin2", js.toString());
                        try {
                            val gson = Gson()
                            if (jsonResult.getInt("statusCode") == 200) {
                                var kitRegistration: KitRegistrationResponse =
                                    gson.fromJson(js, KitRegistrationResponse::class.java)


                                if (kitRegistration == null) {
                                    binding.progressBar.visibility = View.GONE
                                    Utility.displayShortSnackBar(
                                        binding.parentLayout,
                                        "Bad request. Expected Username/client id combination not found."
                                    )
                                } else {
                                    if (kitRegistration != null && kitRegistration!!.getStatusCode() == 200) {
                                        if (kitRegistration!!.data!!.status == true) {
                                            //displayAlert()
                                            // Check user raw data status
                                            getUserRawData()

                                        } else {
                                            val i = Intent(
                                                applicationContext,
                                                ScannerActivity::class.java
                                            )
                                            startActivity(i)
                                            finish()
                                        }

                                    } else if (response.body() != null) {
                                        binding.progressBar.visibility = View.GONE
                                        val i =
                                            Intent(applicationContext, ScannerActivity::class.java)
                                        startActivity(i)
                                        finish()
                                        //Utility.displayShortSnackBar(binding.parentLayout, response.body()!!.getData().toString())
                                    }
                                }
                            } else {

                                Utility.displayShortSnackBar(
                                    binding.parentLayout,
                                    JSONObject(jsonResult.getString("body")).getString("message").toString()
                                )

                            }

                        } catch (e: java.lang.Exception) {
                            e.printStackTrace()
                        }


                    }

                    override fun onFailure(call: Call<JsonObject>, t: Throwable) {
                        binding.progressBar.visibility = View.GONE
                        Log.e("Login", t.toString())
                        Utility.displayShortSnackBar(
                            binding.parentLayout,
                            "Server connection error!!, please try again later."
                        )
                    }
                })
        } else {
            binding.progressBar.visibility = View.GONE

            Utility.displayShortSnackBar(
                binding.parentLayout,
                resources.getString(R.string.check_internet)
            )

        }
    }

    /*private fun getKitRegistrationStatus() {

        val manager = NetworkManager2()
        if (manager.isConnectingToInternet(applicationContext)) {

            val apiService = ApiClientNew.getClient()!!.create(ApiInterface::class.java)
            val call = apiService.getKitStatus(Utility.getUserID(applicationContext))
            call.enqueue(object : Callback<KitRegistrationResponse> {
                override fun onResponse(call: Call<KitRegistrationResponse>, response: Response<KitRegistrationResponse>) {
                    binding.progressBar.visibility = View.GONE
                    if (response.body() == null) {
                        Utility.displayShortSnackBar(
                            binding.parentLayout,
                            "Bad request. Expected Username/client id combination not found."
                        )
                    } else {
                        if (response.body() != null && response.body()!!.getStatusCode() == 200) {
                            if (response.body()!!.data!!.status==true) {
                                Utility.saveUserStatus(applicationContext, true)
*//*                                val i = Intent(this@ScannerActivity, ThankyouRegistrationActivity::class.java)
                                startActivity(i)
*//*                                // val i = Intent(this@ScannerActivity, DashboardActivity::class.java)
                                //startActivity(i)
                                getUserRawData()
                            }

                        }

                        else if (response.body()!!.getStatusCode() == 404) {
                            binding.layoutScanner.visibility = VISIBLE
                        } else {
                            binding.layoutScanner.visibility = VISIBLE
                            //Utility.displayShortSnackBar(binding.parentLayout, response.body()!!.getData().toString())
                        }
                    }
                }

                override fun onFailure(call: Call<KitRegistrationResponse>, t: Throwable) {
                    binding.progressBar.visibility = View.GONE
                    Log.e("Login", t.toString())
                    Utility.displayShortSnackBar(
                        binding.parentLayout,
                        "Server connection error!!, please try again later."
                    )
                }
            })
        } else {
            binding.progressBar.visibility = View.GONE

            Utility.displayShortSnackBar(
                binding.parentLayout,
                resources.getString(R.string.check_internet)
            )

        }
    }*/

    override fun onBackPressed() {
        super.onBackPressed()
        val i = Intent(applicationContext, LoginActivity::class.java)
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(i)
        finish()
    }

    private fun getUserRawData() {
        binding.progressBar.visibility = VISIBLE
        val manager = NetworkManager()
        //val params = HashMap<String, String>()
        //params["user_id"] = Utility.getUserID(this)
        // var userId= URLDecoder.decode(Utility.getUserID(this), "UTF-8")

        val datasd = Base64.decode(Utility.getUserID(this), Base64.DEFAULT)
        val decodeUserId = String(datasd, charset("UTF-8"))
        if (manager.isConnectingToInternet(this)) {
            manager.createApiRequest(
                ApiUtilis.getAPIInstance(applicationContext).getUserRawData(decodeUserId),
                object : ServiceListener<RawDataResponse> {
                    override fun getServerResponse(response: RawDataResponse, requestcode: Int) {
                        binding.progressBar.visibility = View.GONE

                        try {
                            if (response.getCode() == 200) {
                                /*val sharedPreference =  getSharedPreferences("dna", Context.MODE_PRIVATE)
                                val editor = sharedPreference.edit()
                                editor.putString("biological_age",response.getData()!!.biologicalAge)
                                editor.apply()*/

                                if (response.getData()!!.kitId.toString() != "") {
                                    PreferenceConnector.writeString(
                                        applicationContext,
                                        PreferenceConnector.KIT_ID,
                                        response.getData()!!.kitId.toString()
                                    )

                                }

                                if (response.getData()!!.biologicalAge.toString() != "") {
                                    PreferenceConnector.writeString(
                                        applicationContext,
                                        PreferenceConnector.BIOLOGICAL_AGE,
                                        response.getData()!!.biologicalAge
                                    )
                                } else if (response.getData()!!.sleepIndex.toString() != "") {
                                    PreferenceConnector.writeString(
                                        applicationContext,
                                        PreferenceConnector.SLEEP_INDEX,
                                        response.getData()!!.sleepIndex
                                    )
                                }

                                val i = Intent(applicationContext, DashboardActivity::class.java)
                                startActivity(i)
                            } else {
                                /*Utility.displayShortSnackBar(
                                    binding.parentLayout,
                                    response.getMessage().toString()
                                )*/
                                finish()
                                val i = Intent(
                                    applicationContext,
                                    ThankyouRegistrationActivity::class.java
                                )
                                startActivity(i)
                            }
                        } catch (e: Exception) {

                        }

                    }

                    override fun getError(error: ErrorModel, requestcode: Int) {
                        binding.progressBar.visibility = View.GONE

                        /*
                        Utility.displayShortSnackBar(
                            binding.parentLayout,
                            error.getMessage().toString()
                        )
                        */
                        finish()
                        val i = Intent(applicationContext, ThankyouRegistrationActivity::class.java)
                        startActivity(i)


                    }
                })
        } else {
            binding.progressBar.visibility = View.GONE
            Utility.displayShortSnackBar(
                binding.parentLayout,
                resources.getString(R.string.check_internet)
            )
        }
    }

}