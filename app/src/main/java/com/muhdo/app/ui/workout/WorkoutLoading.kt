package com.muhdo.app.ui.workout

import android.app.AlertDialog
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.muhdo.app.R
import com.muhdo.app.databinding.WorkoutLoadingBinding
import com.muhdo.app.ui.DashboardActivity
import com.muhdo.app.utils.v3.Constants

class WorkoutLoading : AppCompatActivity(){

    lateinit var binding: WorkoutLoadingBinding
    private lateinit var handler: Handler
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            val w = window // in Activity's onCreate() for instance
            w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS)
        }

/*
        val showAlert = getIntent().getExtras().get("show_alert")
        val showAlertMsg = getIntent().getExtras().get("show_alert_msg")

        if(showAlert.equals("yes")){
            displayAlert(showAlertMsg as String?)
        }else{
             handler = Handler()
               handler.postDelayed({
               startActivity(Intent(this@WorkoutLoading, WorkOutActivity::class.java))
                finish()

        }, 5000)
        }
*/
        binding = DataBindingUtil.setContentView(this@WorkoutLoading, R.layout.workout_loading)
        handler = Handler()
        handler.postDelayed({
            // for autoLogin user

            val i = Intent(applicationContext, DashboardActivity::class.java)
            i.putExtra(Constants.DASHBOARD_ACTION, Constants.DASHBOARD_WORKOUT_MY_PLAN)
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(i)
            finish()

        }, 5000)

    }






    private fun displayAlert(message: String?) {
        val dialogBuilder = AlertDialog.Builder(this)
        dialogBuilder.setMessage(message)
            .setCancelable(false)
            .setPositiveButton("Ok") { _, _ ->
                handler = Handler()
                handler.postDelayed({
                     startActivity(Intent(this@WorkoutLoading, WorkOutActivity::class.java))
                     finish()

                }, 5000)


            }


        val alert = dialogBuilder.create()
        alert.show()
    }
    }