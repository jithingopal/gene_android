package com.muhdo.app.ui.epigentic.supplyments.adapter;


import android.content.Context;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.*;
import androidx.recyclerview.widget.RecyclerView;
import com.muhdo.app.R;
import com.muhdo.app.ui.epigentic.supplyments.EpigenticSupplementActivity;
import com.muhdo.app.ui.epigentic.supplyments.model.SupplymentInfo;

import java.util.ArrayList;
import java.util.List;


public class SupplementTypeAdapter extends RecyclerView.Adapter<SupplementTypeAdapter.MyViewHolder> implements Filterable {

    private List<SupplymentInfo> supplymentInfoList;
    private List<SupplymentInfo> supplymentInfoListFiltered;
    private supplementInfoListListener listener;
    public EpigenticSupplementActivity mContext;
    String quantity;

    /**
     * View holder class
     */
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView textViewIngredientName, textViewUnit;
        public EditText unitValue;
        public LinearLayout addSupplement;


        public MyViewHolder(View view) {
            super(view);
            textViewIngredientName = (TextView) view.findViewById(R.id.textview_supplement_type);
            textViewUnit = (TextView) view.findViewById(R.id.textview_unit);
            unitValue = (EditText) view.findViewById(R.id.editText_unit_value);
            addSupplement = (LinearLayout) view.findViewById(R.id.layout_add_supplyment);

        }
    }

    public SupplementTypeAdapter(List supplymentInfoList, EpigenticSupplementActivity mContext) {
        this.supplymentInfoList = supplymentInfoList;
        this.supplymentInfoListFiltered = supplymentInfoList;
        this.mContext = (EpigenticSupplementActivity) mContext;
    }


    // Provide listener on every view
    public void setListener(supplementInfoListListener listener) {
        this.listener = listener;
    }


    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        SupplymentInfo supplymentInfo = supplymentInfoListFiltered.get(position);
        holder.textViewIngredientName.setText(supplymentInfo.getIngredientName());
        holder.textViewUnit.setText(supplymentInfo.getDosage());
        /*holder.unitValue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                 quantity=holder.unitValue.getText().toString();

            }
        });*/

        holder.addSupplement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    String quantity = holder.unitValue.getText().toString();
                    //Toast.makeText(mContext, "select  " + value, Toast.LENGTH_SHORT).show();
                    if(quantity.equals("") || quantity.isEmpty()){
                        Toast.makeText(mContext, "Please enter value", Toast.LENGTH_SHORT).show();

                    }else{
                        if(Integer.parseInt(quantity)>0){
                            new Handler().postDelayed(runnable, 1000 );
                            holder.unitValue.setText("");
                            listener.addSupplement(supplymentInfoListFiltered.get(position), quantity, position);
                        }else{
                            Toast.makeText(mContext, "Invalid value", Toast.LENGTH_SHORT).show();
                        }

                    }


                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return supplymentInfoListFiltered.size();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.supplement_spinner_row, parent, false);
        return new MyViewHolder(v);
    }


    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString().trim();
                if (charString.isEmpty()) {
                    supplymentInfoListFiltered = supplymentInfoList;
                } else {
                    List<SupplymentInfo> filteredList = new ArrayList<>();
                    for (SupplymentInfo row : supplymentInfoList) {
                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getIngredientName().trim().toLowerCase().contains(charString.toString().toLowerCase())) {
                            filteredList.add(row);
                        }
                    }

                    supplymentInfoListFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = supplymentInfoListFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                supplymentInfoListFiltered = (ArrayList<SupplymentInfo>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }


    public interface supplementInfoListListener {
        void addSupplement(SupplymentInfo obj, String quantity, int position);

    }



    Runnable runnable = new Runnable()
    {
        @Override
        public void run()
        {
            InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm != null) {
                mContext.getWindow().setSoftInputMode(
                        WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
                );
            }

        }
    };

}
