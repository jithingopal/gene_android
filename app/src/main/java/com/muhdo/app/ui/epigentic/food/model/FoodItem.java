package com.muhdo.app.ui.epigentic.food.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class FoodItem implements Serializable, Comparable<FoodItem> {

    @SerializedName("_id")
    @Expose
    private String id;

    @SerializedName("meal_time")
    @Expose
    private String mealTime;

    @SerializedName("food_group")
    @Expose
    private String foodGroup;

    @SerializedName("food_group_name")
    @Expose
    private String foodGroupName;

    @SerializedName("food_name")
    @Expose
    private String foodName;

    @SerializedName("value_type")
    @Expose
    private String valueType;

    @SerializedName("measurement")
    @Expose
    private String measurement;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMealTime() {
        return mealTime;
    }

    public void setMealTime(String mealTime) {
        this.mealTime = mealTime;
    }

    public String getFoodGroup() {
        return foodGroup;
    }

    public void setFoodGroup(String foodGroup) {
        this.foodGroup = foodGroup;
    }

    public String getFoodGroupName() {
        return foodGroupName;
    }

    public void setFoodGroupName(String foodGroupName) {
        this.foodGroupName = foodGroupName;
    }

    public String getFoodName() {
        return foodName;
    }

    public void setFoodName(String foodName) {
        this.foodName = foodName;
    }

    public String getValueType() {
        return valueType;
    }

    public void setValueType(String valueType) {
        this.valueType = valueType;
    }

    public String getMeasurement() {
        return measurement;
    }

    public void setMeasurement(String measurement) {
        this.measurement = measurement;
    }

    @Override
    public int compareTo(FoodItem o) {
        return this.getFoodName().compareTo(o.getFoodName());
    }
}
