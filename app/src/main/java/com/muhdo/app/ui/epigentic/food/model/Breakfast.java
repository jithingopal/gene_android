package com.muhdo.app.ui.epigentic.food.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Breakfast {

    @SerializedName("MEAT")
    @Expose
    private List<FoodItem> meatItemList;

    @SerializedName("FISH")
    @Expose
    private List<FoodItem> fishItemList;

    @SerializedName("DAIRY")
    @Expose
    private List<FoodItem> dairyItemList;

    @SerializedName("CEREAL_BREADS")
    @Expose
    private List<FoodItem> cerealBreadsItemList;

    @SerializedName("VEGETABLES")
    @Expose
    private List<FoodItem> vegetablesItemList;

    @SerializedName("FRUIT_NUTS")
    @Expose
    private List<FoodItem> fruitNutsItemList;

    @SerializedName("OTHERS")
    @Expose
    private List<FoodItem> otherItemList;

    public List<FoodItem> getMeatItemList() {
        return meatItemList;
    }

    public void setMeatItemList(List<FoodItem> meatItemList) {
        this.meatItemList = meatItemList;
    }

    public List<FoodItem> getFishItemList() {
        return fishItemList;
    }

    public void setFishItemList(List<FoodItem> fishItemList) {
        this.fishItemList = fishItemList;
    }

    public List<FoodItem> getDairyItemList() {
        return dairyItemList;
    }

    public void setDairyItemList(List<FoodItem> dairyItemList) {
        this.dairyItemList = dairyItemList;
    }

    public List<FoodItem> getCerealBreadsItemList() {
        return cerealBreadsItemList;
    }

    public void setCerealBreadsItemList(List<FoodItem> cerealBreadsItemList) {
        this.cerealBreadsItemList = cerealBreadsItemList;
    }

    public List<FoodItem> getVegetablesItemList() {
        return vegetablesItemList;
    }

    public void setVegetablesItemList(List<FoodItem> vegetablesItemList) {
        this.vegetablesItemList = vegetablesItemList;
    }

    public List<FoodItem> getFruitNutsItemList() {
        return fruitNutsItemList;
    }

    public void setFruitNutsItemList(List<FoodItem> fruitNutsItemList) {
        this.fruitNutsItemList = fruitNutsItemList;
    }

    public List<FoodItem> getOtherItemList() {
        return otherItemList;
    }

    public void setOtherItemList(List<FoodItem> otherItemList) {
        this.otherItemList = otherItemList;
    }
}
