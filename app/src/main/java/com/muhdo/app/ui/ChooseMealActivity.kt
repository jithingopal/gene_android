package com.muhdo.app.ui

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.text.Html
import android.util.Log
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import com.google.gson.Gson
import com.muhdo.app.R
import com.muhdo.app.apiModel.mealModel.ChooseMealModel
import com.muhdo.app.apiModel.mealModel.ChooseMealSuccess
import com.muhdo.app.apiModel.mealModel.Recipe
import com.muhdo.app.apiModel.mealModel.UpdateMealPlanRequest
import com.muhdo.app.databinding.ActivityChooseMealBinding
import com.muhdo.app.fragment.MealFragment
import com.muhdo.app.repository.ApiUtilis
import com.muhdo.app.repository.ErrorModel
import com.muhdo.app.repository.NetworkManager
import com.muhdo.app.repository.ServiceListener
import com.muhdo.app.utils.v3.Constants
import com.timingsystemkotlin.backuptimingsystem.Utility
import org.json.JSONArray
import org.json.JSONObject


class ChooseMealActivity : AppCompatActivity(), View.OnClickListener {
    object CONSTANTS {
  var selectedDay:String="Selected_Day"
    }

    override fun onClick(p0: View?) {
        when (p0!!.id) {
            R.id.btn_continue_plan -> {
                Log.d("Message ", "Total Meal :" + Utility.jsonList.length())
                Log.d("Message ", "Seleced Meal :" + noOfMeals)
                if (Utility.jsonList.length() == noOfMeals) {
                    sendMealToServer()
                } else {
                    Utility.displayShortSnackBar(binding.parentLayout, "Please select all meals")
                }

            }
            R.id.img_previous -> {
                if (dayCount > 0) {
                    dayCount--
                    addBottomDots(dayCount)
                }
            }
            R.id.img_next -> {
                if (dayCount < 6) {
                    dayCount++
                    addBottomDots(dayCount)
                }
            }

        }
    }

    lateinit var binding: ActivityChooseMealBinding
    private var dayCount: Int = 0
    private var data: ChooseMealModel? = null
    private var differenceCount: Int = 0
    private var noOfMeals: Int = 0
    private var selectedDayPosition=0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this@ChooseMealActivity, R.layout.activity_choose_meal)
        getMealList()
        if (intent.extras != null) {
            selectedDayPosition = intent.extras.getInt(CONSTANTS.selectedDay, 0)
        }
        Utility.jsonList = JSONArray()
        Utility.idList.clear()
        binding.imgNext.setOnClickListener(this)
        binding.imgPrevious.setOnClickListener(this)
        binding.btnContinuePlan.setOnClickListener(this)
        binding.toolbar.btnNavigation.setOnClickListener {
            val intent = Intent(this, DashboardActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
            finish()
        }
    }


    private fun getMealList() {
        binding.progressBar.visibility = VISIBLE
        val manager = NetworkManager()
        if (manager.isConnectingToInternet(applicationContext)) {
            manager.createApiRequest(
                ApiUtilis.getAPIInstance(applicationContext).getChoosePlanListV3(Utility.getUserID(applicationContext)),
                object : ServiceListener<ChooseMealModel> {
                    override fun getServerResponse(response: ChooseMealModel, requestcode: Int) {
                        data = response
                        addBottomDots(dayCount)
                        binding.progressBar.visibility = GONE
                    }

                    override fun getError(error: ErrorModel, requestcode: Int) {
                        binding.progressBar.visibility = GONE
                        Utility.displayShortSnackBar(binding.parentLayout, error.getMessage().toString())
                    }
                })
        } else {
            binding.progressBar.visibility = GONE
            Utility.displayShortSnackBar(binding.parentLayout, resources.getString(R.string.check_internet))
        }
    }

    private fun sendMealToServer() {
        binding.progressBar.visibility = VISIBLE
        binding.flContent.visibility = GONE


        val listData = ArrayList<Recipe>()
        for (i in 0 until Utility.jsonList.length()) {
            val json = JSONObject(Utility.jsonList.get(i).toString())
            val recipe = Recipe(json.getString("recipe_id"), json.getString("day"))
            listData.add(recipe)
        }


        val request = UpdateMealPlanRequest(listData, Utility.getUserID(applicationContext))
        val manager = NetworkManager()
        if (manager.isConnectingToInternet(applicationContext)) {
            manager.createApiRequest(
                ApiUtilis.getAPIInstance(applicationContext).continueWithMeal(request),
                object : ServiceListener<ChooseMealSuccess> {
                    override fun getServerResponse(response: ChooseMealSuccess, requestcode: Int) {
                        binding.progressBar.visibility = GONE
                        binding.flContent.visibility = VISIBLE
                        val i = Intent(applicationContext, Sample::class.java)
                        startActivity(i)

                    }

                    override fun getError(error: ErrorModel, requestcode: Int) {
                        binding.progressBar.visibility = GONE
                        binding.flContent.visibility = VISIBLE
                        Utility.displayShortSnackBar(binding.parentLayout, error.getMessage().toString())
                    }
                })
        } else {
            binding.progressBar.visibility = GONE
            binding.flContent.visibility = VISIBLE
            Utility.displayShortSnackBar(binding.parentLayout, resources.getString(R.string.check_internet))
        }
    }

    @SuppressLint("ResourceType")
    private fun passData(response: ChooseMealModel, position: Int) {
        noOfMeals = response.getData()!!.getTotalMeals()!! * 7

        val transaction = supportFragmentManager.beginTransaction()
        if (dayCount > 0) {
            transaction.setCustomAnimations(R.anim.exit, R.anim.enter)
        }

        val bundle = Bundle()
//        val myMessage = response.getData().toString()
        val gson = Gson()
        when (position) {
            0 -> {
                if (response.getData()!!.getMonday() != null) {
                    bundle.putString(Constants.ACTIVITY_FRAGMENT_MESSAGE, gson.toJson(response.getData()!!.getMonday()))
                } else {
                    bundle.putString(Constants.ACTIVITY_FRAGMENT_MESSAGE, "")
                }
                bundle.putString(Constants.ACTIVITY_FRAGMENT_WEEK, resources.getString(R.string.monday))
            }
            1 -> {
                if (response.getData()!!.getTuesday() != null) {
                    bundle.putString(
                        Constants.ACTIVITY_FRAGMENT_MESSAGE, gson.toJson(response.getData()!!.getTuesday())
                    )
                } else {
                    bundle.putString(Constants.ACTIVITY_FRAGMENT_MESSAGE, "")
                }
                bundle.putString(Constants.ACTIVITY_FRAGMENT_WEEK, resources.getString(R.string.tuesday))
            }
            2 -> {
                if (response.getData()!!.getWednesday() != null) {
                    bundle.putString(
                        Constants.ACTIVITY_FRAGMENT_MESSAGE, gson.toJson(response.getData()!!.getWednesday())
                    )
                } else {
                    bundle.putString(Constants.ACTIVITY_FRAGMENT_MESSAGE, "")
                }
                bundle.putString(Constants.ACTIVITY_FRAGMENT_WEEK, resources.getString(R.string.wednesday))
            }
            3 -> {
                if (response.getData()!!.getThursday() != null) {
                    bundle.putString(
                        Constants.ACTIVITY_FRAGMENT_MESSAGE, gson.toJson(response.getData()!!.getThursday())
                    )
                } else {
                    bundle.putString(Constants.ACTIVITY_FRAGMENT_MESSAGE, "")
                }
                bundle.putString(Constants.ACTIVITY_FRAGMENT_WEEK, resources.getString(R.string.thursday))
            }
            4 -> {
                if (response.getData()!!.getFriday() != null) {
                    bundle.putString(Constants.ACTIVITY_FRAGMENT_MESSAGE, gson.toJson(response.getData()!!.getFriday()))
                } else {
                    bundle.putString(Constants.ACTIVITY_FRAGMENT_MESSAGE, "")
                }
                bundle.putString(Constants.ACTIVITY_FRAGMENT_WEEK, resources.getString(R.string.friday))
            }
            5 -> {
                if (response.getData()!!.getSaturday() != null) {
                    bundle.putString(
                        Constants.ACTIVITY_FRAGMENT_MESSAGE, gson.toJson(response.getData()!!.getSaturday())
                    )
                } else {
                    bundle.putString(Constants.ACTIVITY_FRAGMENT_MESSAGE, "")
                }
                bundle.putString(Constants.ACTIVITY_FRAGMENT_WEEK, resources.getString(R.string.saturday))
            }
            6 -> {
                if (response.getData()!!.getSunday() != null) {
                    bundle.putString(Constants.ACTIVITY_FRAGMENT_MESSAGE, gson.toJson(response.getData()!!.getSunday()))
                } else {
                    bundle.putString(Constants.ACTIVITY_FRAGMENT_MESSAGE, "")
                }
                bundle.putString(Constants.ACTIVITY_FRAGMENT_WEEK, resources.getString(R.string.sunday))
            }
        }

        val fragInfo = MealFragment()
        fragInfo.arguments = bundle
        transaction.replace(R.id.flContent, fragInfo)
        transaction.commit()

    }

    private fun addBottomDots(currentPage: Int) {
        differenceCount = currentPage - differenceCount
        when (currentPage) {
            0 -> {
                binding.txtWeekday.text = resources.getString(R.string.day_monday)
                binding.imgPrevious.visibility = GONE
                binding.imgNext.visibility = VISIBLE
            }
            1 -> {
                binding.txtWeekday.text = resources.getString(R.string.day_tuesday)
                binding.imgPrevious.visibility = VISIBLE
                binding.imgNext.visibility = VISIBLE
            }
            2 -> {
                binding.txtWeekday.text = resources.getString(R.string.day_wednesday)
                binding.imgPrevious.visibility = VISIBLE
                binding.imgNext.visibility = VISIBLE
            }
            3 -> {
                binding.txtWeekday.text = resources.getString(R.string.day_thursday)
                binding.imgPrevious.visibility = VISIBLE
                binding.imgNext.visibility = VISIBLE
            }
            4 -> {
                binding.txtWeekday.text = resources.getString(R.string.day_friday)
                binding.imgPrevious.visibility = VISIBLE
                binding.imgNext.visibility = VISIBLE
            }
            5 -> {
                binding.txtWeekday.text = resources.getString(R.string.day_saturday)
                binding.imgPrevious.visibility = VISIBLE
                binding.imgNext.visibility = VISIBLE
            }
            6 -> {
                binding.txtWeekday.text = resources.getString(R.string.day_sunday)
                binding.imgPrevious.visibility = VISIBLE
                binding.imgNext.visibility = GONE
            }
        }

        passData(data!!, currentPage)

        val dots = arrayOfNulls<TextView>(7)
        binding.layoutDots.removeAllViews()
        for (i in dots.indices) {
            dots[i] = TextView(this)
            dots[i]!!.text = Html.fromHtml("&#8226;")
            dots[i]!!.textSize = 35f
            dots[i]!!.setTextColor(ContextCompat.getColor(applicationContext, R.color.secondary_color))
            binding.layoutDots.addView(dots[i])
        }
        if (dots.isNotEmpty())
            dots[currentPage]!!.setTextColor(ContextCompat.getColor(applicationContext, R.color.color_dark_blue))
    }

    override fun onBackPressed() {
        super.onBackPressed()
        val intent = Intent(this, DashboardActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(intent)
        finish()
    }

}