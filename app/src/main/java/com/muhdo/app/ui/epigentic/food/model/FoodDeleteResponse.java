package com.muhdo.app.ui.epigentic.food.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FoodDeleteResponse {

    @SerializedName("code")
    @Expose
    private String statusCode;

    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("data")
    @Expose
    private FoodInfoModel foodInfoModel;

    public FoodInfoModel getFoodInfoModel() {
        return foodInfoModel;
    }

    public void setFoodInfoModel(FoodInfoModel foodInfoModel) {
        this.foodInfoModel = foodInfoModel;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


}
