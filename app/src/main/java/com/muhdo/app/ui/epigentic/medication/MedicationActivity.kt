package com.muhdo.app.ui.epigentic.medication

import android.app.ProgressDialog
import android.content.Context
import android.os.Bundle
import android.util.Base64
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.mindorks.placeholderview.ExpandablePlaceHolderView
import com.mindorks.placeholderview.annotations.Layout
import com.mindorks.placeholderview.annotations.Resolve
import com.mindorks.placeholderview.annotations.expand.ChildPosition
import com.mindorks.placeholderview.annotations.expand.ParentPosition
import com.muhdo.app.R
import com.muhdo.app.databinding.ActivityMedicationBinding
import com.muhdo.app.repository.ApiUtilis
import com.muhdo.app.repository.ErrorModel
import com.muhdo.app.repository.NetworkManager
import com.muhdo.app.repository.ServiceListener
import com.muhdo.app.ui.epigentic.medication.adapter.MedicationTypeAdapter
import com.muhdo.app.ui.epigentic.medication.model.MedicationInfo
import com.muhdo.app.ui.epigentic.medication.model.MedicationType
import com.muhdo.app.ui.epigentic.medication.response.AddMedicationRes
import com.muhdo.app.ui.epigentic.medication.response.MedicationListRosponse
import com.muhdo.app.ui.epigentic.medication.response.MedicationTypeResponse
import com.muhdo.app.ui.epigentic.medication.view.MedicationHeadingView
import com.muhdo.app.utils.v3.Constants
import com.timingsystemkotlin.backuptimingsystem.Utility
import java.util.*


open class MedicationActivity : AppCompatActivity() {
    open var progressDialog: ProgressDialog? = null
    var mExpandableView: ExpandablePlaceHolderView? = null
    lateinit var progressBar: FrameLayout
   // var txtSearch: AutoCompleteTextView? = null
    var mList: List<MedicationType>? = null
    var medicationAdapter: MedicationTypeAdapter? = null
    public var obj: Context? = null
   // private var mOnScrollListener: ExpandablePlaceHolderView.OnScrollListener? = null
     var show = false
   open lateinit var binding: ActivityMedicationBinding
    var drugsId="0"

    companion object {
        private val TAG = this::class.java.simpleName
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this@MedicationActivity, R.layout.activity_medication)
        Dialog(this@MedicationActivity)
        binding.btnBack.setOnClickListener {
            finish()
            /*if(show==true){
                binding.layoutMedication2.visibility=View.GONE;
                binding.layoutShowList.visibility=View.GONE;
                binding.layoutMedication1.visibility=View.VISIBLE;
                binding.layoutAddMedication.visibility=View.VISIBLE

                show=false;
            }else{
                finish()
            }*/
        }
        mExpandableView = findViewById(R.id.expandable_view_medication) as ExpandablePlaceHolderView
        progressBar=findViewById(R.id.progress_bar) as FrameLayout
        clickExccriseOption()
        clickShowList()
        showProgressDialog()
       // binding.progressBar.visibility = View.VISIBLE
        callMedicationService(this@MedicationActivity)
        // search button
        getAllMedication()
        // click on button continue send medication info on server
        sendMedicationInfoOnServer();
        setAllMedicationInAutoCompleteTextView(this)


    }



    private fun sendMedicationInfoOnServer() {
        var readioText="mg";
        binding.radioGroupMgUgMl.setOnCheckedChangeListener(
            RadioGroup.OnCheckedChangeListener { group, checkedId ->
                val radio: RadioButton = findViewById(checkedId)
               readioText= radio.text.toString();
            })

        binding.btncontinue.setOnClickListener {

            var type=binding!!.edittextMedicationType.text.toString()
            var dosage=binding!!.edittextDosage.text.toString()

            if(type.isEmpty() || dosage.isEmpty()){
                Toast.makeText(applicationContext,"Please enter value",Toast.LENGTH_SHORT).show()


          }else{

            val userId = Base64.decode(Utility.getUserID(this), Base64.DEFAULT)
            val decodeUserId = String(userId, charset("UTF-8"))
            val manager = NetworkManager()
            val params = HashMap<String, String>()
            params["user_id"] = decodeUserId
            params["medication_id"] = drugsId
            params["drugs"] =binding!!.edittextMedicationType.text.toString()
            params["dosage"] =readioText
            params["quantity"] =binding!!.edittextDosage.text.toString()
            showProgressDialog()
            //binding.progressBar.visibility = View.VISIBLE
            if (manager.isConnectingToInternet(applicationContext)) {
                manager.createApiRequest(
                    ApiUtilis.getAPIService(Constants.EPIGENTIC_API).addMedication(params),
                    object : ServiceListener<AddMedicationRes> {
                        override fun getServerResponse(response: AddMedicationRes, requestcode: Int) {

                           if(response.statusCode.equals("200")){
                               callMedicationService(this@MedicationActivity)
                           }else{
                               Utility.displayShortSnackBar(
                                   binding.parentLayout,
                                   response.getMessage().toString()
                               )
                           }

                        }

                        override fun getError(error: ErrorModel, requestcode: Int) {
                           // binding.progressBar.visibility = View.GONE
                            dismissProgressDialog()
                            Utility.displayShortSnackBar(
                                binding.parentLayout,
                                error.getMessage().toString()
                            )
                        }
                    })
            } else {
               // binding.progressBar.visibility = View.GONE
                dismissProgressDialog()
                Utility.displayShortSnackBar(
                    binding.parentLayout,
                    resources.getString(R.string.check_internet)
                )
            }
            }
        }
    }

    private fun setAllMedicationInAutoCompleteTextView(medicationActivity: MedicationActivity) {

        // Set an item click listener for auto complete text view
        binding.autocompleteView.onItemClickListener = AdapterView.OnItemClickListener{
                parent,view,position,id->
            // Display the clicked item using toast
            var drugsName= mList!!.get(position).drugsName;
            var dosageUnit=mList!!.get(position).dosage;
            drugsId= mList!!.get(position).drugsId
          //  Toast.makeText(applicationContext,"Selected : $v",Toast.LENGTH_SHORT).show()
            binding.edittextMedicationType.setText(drugsName.toString())

            if(dosageUnit.equals("mg")){
                binding.radioBtnMg.isChecked=true
                binding.radioBtnUg.isChecked=false
                binding.radioBtnMl.isChecked=false
            }
            else if(dosageUnit.equals("µg")){
                binding.radioBtnUg.isChecked=true
                binding.radioBtnMg.isChecked=false
                binding.radioBtnMl.isChecked=false
            }

            else if(dosageUnit.equals("ml")){
                binding.radioBtnMl.isChecked=true
                binding.radioBtnUg.isChecked=false
                binding.radioBtnMg.isChecked=false
            }
        }
    }

     fun clickExccriseOption() {
        binding.layoutAddMedication.setOnClickListener(object: View.OnClickListener {
            override fun onClick(v: View?) {
                Log.d("data","click a")
                show=true
               // Toast.makeText(applicationContext,"hello",Toast.LENGTH_SHORT).show();
                binding.layoutMedication2.visibility=View.VISIBLE;
                binding.layoutShowList.visibility=View.VISIBLE;
                binding.layoutMedication1.visibility=View.GONE
                binding.layoutAddMedication.visibility=View.GONE

            }
        })
    }

    private fun clickShowList() {
        binding.layoutShowList.setOnClickListener(object: View.OnClickListener {
            override fun onClick(v: View?) {
                Log.d("data","click a")
                show=true
              //  Toast.makeText(applicationContext,"hello", Toast.LENGTH_SHORT).show();
                binding.layoutMedication2.visibility=View.GONE;
                binding.layoutShowList.visibility=View.GONE;
                binding.layoutMedication1.visibility=View.VISIBLE
                binding.layoutAddMedication.visibility=View.VISIBLE
            }
        })
    }

    private fun getAllMedication() {
        val manager = NetworkManager()
        if (manager.isConnectingToInternet(applicationContext)) {
            manager.createApiRequest(
                ApiUtilis.getAPIService(Constants.EPIGENTIC_API).getAllMedicationType(),
                object : ServiceListener<MedicationTypeResponse> {
                    override fun getServerResponse(response: MedicationTypeResponse, requestcode: Int) {
                        //  binding.progressBar.visibility = View.GONE
                        try {
                             Log.d("data","medicatin type"+ response.getData()!!.size);
                            mList = response.getData()
                            medicationAdapter = MedicationTypeAdapter(this@MedicationActivity,R.layout.activity_medication, R.id.textview_item_name, mList!!)
                            binding.autocompleteView!!.setThreshold(1);
                            binding.autocompleteView!!.setAdapter(medicationAdapter);
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }

                    override fun getError(error: ErrorModel, requestcode: Int) {
                        //binding.progressBar.visibility = View.GONE
                        Utility.displayShortSnackBar(
                            binding.parentLayout,
                            error.getMessage().toString()
                        )
                    }
                })
        } else {
            //binding.progressBar.visibility = View.GONE
            Utility.displayShortSnackBar(
                binding.parentLayout,
                resources.getString(R.string.check_internet)
            )

        }
    }
    fun callMedicationService(mContext: MedicationActivity) {
        //showProgressDialog()

        val userId = Base64.decode(Utility.getUserID(this), Base64.DEFAULT)
        val decodeUserId = String(userId, charset("UTF-8"))
        val manager = NetworkManager()
        if (manager.isConnectingToInternet(this@MedicationActivity)) {
            manager.createApiRequest(
                ApiUtilis.getAPIService(Constants.EPIGENTIC_API).getMedicationList(decodeUserId),
                object : ServiceListener<MedicationListRosponse> {
                    override fun getServerResponse(response: MedicationListRosponse, requestcode: Int) {
                       // binding.progressBar.visibility = View.GONE
                        mContext. dismissProgressDialog()
                        mExpandableView?.removeAllViews();
                        try{
                            if(response.statusCode.equals("200") && response.dataGroupModelList.size>0){

                                Collections.reverse(response.dataGroupModelList);
                                if(response.dataGroupModelList!=null){
                                    for (dataGroup in response.dataGroupModelList) {
                                        mExpandableView!!.addView(MedicationHeadingView(this@MedicationActivity, dataGroup.date_group))
                                        for (info in dataGroup.getInfoList()) {
                                            mExpandableView!!.addView(MedicationItemView(this@MedicationActivity, info))
                                        }
                                    }
                                }

                                show=true
                                //  Toast.makeText(applicationContext,"hello", Toast.LENGTH_SHORT).show();
                                binding.layoutMedication2.visibility=View.GONE;
                                binding.layoutShowList.visibility=View.GONE;
                                binding.layoutMedication1.visibility=View.VISIBLE
                                binding.layoutAddMedication.visibility=View.VISIBLE
                            }else{
                                Utility.displayShortSnackBar(
                                    binding.parentLayout,
                                    resources.getString(R.string.no_data)
                                )
                            }
                        }catch (e:Exception){

                        }


                    }
                    override fun getError(error: ErrorModel, requestcode: Int) {
                        //binding.progressBar.visibility = View.GONE
                        mContext. dismissProgressDialog()
                        Utility.displayShortSnackBar(
                            binding.parentLayout,
                            error.getMessage().toString()
                        )
                    }
                })
        } else {
            mContext. dismissProgressDialog()
            //binding.progressBar.visibility = View.GONE
            Utility.displayShortSnackBar(
                binding.parentLayout,
                resources.getString(R.string.check_internet)
            )
        }
    }

    @Layout(R.layout.row_medication_heading_item)
    class MedicationItemView(
        private val mContext: MedicationActivity,
        private val mInfo: MedicationInfo

    ): MedicationActivity() {

        override var progressDialog: ProgressDialog? = null
        @ParentPosition
        private val mParentPosition: Int = 0

        @ChildPosition
        private val mChildPosition: Int = 0

        @com.mindorks.placeholderview.annotations.View(R.id.textview_medication_type)
        private val medicationTypeVal: TextView? = null

        @com.mindorks.placeholderview.annotations.View(R.id.textview_medication_unit)
        private val medicationUnit: TextView? = null

        @com.mindorks.placeholderview.annotations.View(R.id.textview_unit_value)
        private val medicationUnitVal: TextView? = null

        @com.mindorks.placeholderview.annotations.View(R.id.view_delete)
        private val viewDelete: LinearLayout? = null

        @Resolve
        private fun onResolved() {
            medicationTypeVal!!.setText(mInfo.drugsName)
            medicationUnit!!.setText(mInfo.dosage)
            medicationUnitVal!!.setText(mInfo.quantity)

            viewDelete?.setOnClickListener(object: android.view.View.OnClickListener {
                override fun onClick(v: android.view.View?) {
                    Log.d("data","click a")
                    callDeleteService(mContext);
                   // mContext.callMedicationService()
                }

                private fun callDeleteService(mContext: MedicationActivity) {
                    mContext.showProgressDialog()
                    //binding.progressBar.visibility = View.VISIBLE
                    val manager = NetworkManager()
                    if (manager.isConnectingToInternet(mContext)) {
                        manager.createApiRequest(
                            ApiUtilis.getAPIService(Constants.EPIGENTIC_API).deleteMedication(mInfo.id),
                            object : ServiceListener<AddMedicationRes> {
                                override fun getServerResponse(response: AddMedicationRes, requestcode: Int) {
                                   if(response.statusCode.equals("200")){
                                       mContext.callMedicationService(mContext)
                                   }else{
                                       Utility.displayShortSnackBar(
                                           binding.parentLayout,
                                           response.getMessage().toString()
                                       )
                                   }

                                }

                                override fun getError(error: ErrorModel, requestcode: Int) {
                                    //binding.progressBar.visibility = View.GONE
                                    mContext.dismissProgressDialog()
                                    Utility.displayShortSnackBar(
                                        binding.parentLayout,
                                        error.getMessage().toString()
                                    )
                                }
                            })
                    } else {
                       mContext.dismissProgressDialog()
                       // binding.progressBar.visibility = View.GONE

                        Utility.displayShortSnackBar(
                            binding.parentLayout,
                            resources.getString(R.string.check_internet)
                        )
                    }


                }
            })
        }
    }


    fun Dialog(context: MedicationActivity) {
        try {
            progressDialog = ProgressDialog(context)
            progressDialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
            progressDialog!!.setProgressStyle(ProgressDialog.STYLE_SPINNER)
            progressDialog!!.setMessage(getString(R.string.please_wait))
            progressDialog!!.setCancelable(false)
        } catch (ignore: Exception) {
        }

    }

    fun showProgressDialog() {
        try {
            if (progressDialog != null) {
                progressDialog!!.show()
            }
        } catch (ignore: Exception) {
        }

    }

    fun dismissProgressDialog() {
        try {
            if (progressDialog != null) {
                progressDialog!!.dismiss()
            }
        } catch (ignore: Exception) {
        }

    }
}
