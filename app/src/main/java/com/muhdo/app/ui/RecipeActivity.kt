package com.muhdo.app.ui

import android.annotation.SuppressLint
import android.graphics.Color
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.google.gson.Gson
import com.muhdo.app.R
import com.muhdo.app.apiModel.mealModel.RecipeDescriptionModel
import com.muhdo.app.databinding.ActivityRecipeBinding
import com.muhdo.app.fragment.DirectionFragment
import com.muhdo.app.fragment.IngredientsFragment
import com.muhdo.app.repository.ApiUtilis
import com.muhdo.app.repository.ErrorModel
import com.muhdo.app.repository.NetworkManager
import com.muhdo.app.repository.ServiceListener
import com.muhdo.app.utils.v3.Constants
import com.timingsystemkotlin.backuptimingsystem.Utility
import kotlinx.android.synthetic.main.activity_recipe.*


class RecipeActivity : AppCompatActivity() {
    lateinit var binding: ActivityRecipeBinding
    private var data: RecipeDescriptionModel? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
        binding = DataBindingUtil.setContentView(this@RecipeActivity, R.layout.activity_recipe)
        getRecipeData(intent.getStringExtra(Constants.RECIPE_ID))
        binding.radioGroup.setOnCheckedChangeListener { _, checkedId ->
            // find which radio button is selected
            if (checkedId == R.id.btn_ingredients) {
                binding.btnIngredients.setTextColor(Color.parseColor("#ffffff"))
                binding.btnIngredients.setBackgroundResource(R.drawable.blue_button_bg)
                binding.btnDirections.setBackgroundResource(R.drawable.blue_button_uncheck_bg)
                binding.btnDirections.setTextColor(Color.parseColor("#1595C5"))
                replaceFragment(IngredientsFragment(), 0)

            } else if (checkedId == R.id.btn_directions) {
                binding.btnDirections.setBackgroundResource(R.drawable.blue_button_bg)
                binding.btnDirections.setTextColor(Color.parseColor("#ffffff"))
                binding.btnIngredients.setTextColor(Color.parseColor("#1595C5"))
                binding.btnIngredients.setBackgroundResource(R.drawable.blue_button_uncheck_bg)
                replaceFragment(DirectionFragment(), 1)

            }
        }

        binding.imgBack.setOnClickListener {
            onBackPressed()
        }


    }


    private fun getRecipeData(recipeID: String) {
        binding.progressBar.visibility = View.VISIBLE
        val manager = NetworkManager()
        if (manager.isConnectingToInternet(applicationContext)) {
            manager.createApiRequest(
                ApiUtilis.getAPIInstance(applicationContext).getRecipeDescription(recipeID),
                object : ServiceListener<RecipeDescriptionModel> {
                    override fun getServerResponse(
                        response: RecipeDescriptionModel,
                        requestcode: Int
                    ) {
                        binding.progressBar.visibility = View.GONE
                        data = response
                        setData(data!!)
                    }

                    override fun getError(error: ErrorModel, requestcode: Int) {
                        System.out.println("ERROR")
                        binding.progressBar.visibility = View.GONE
                        Utility.displayShortSnackBar(
                            binding.parentLayout,
                            error.getMessage().toString()
                        )
                    }
                })
        } else {
            binding.progressBar.visibility = View.GONE
            Utility.displayShortSnackBar(
                binding.parentLayout,
                resources.getString(R.string.check_internet)
            )

        }

    }

    @SuppressLint("SetTextI18n")
    private fun setData(data: RecipeDescriptionModel) {

        binding.btnIngredients.setTextColor(Color.parseColor("#ffffff"))
        binding.btnIngredients.setBackgroundResource(R.drawable.blue_button_bg)

        Glide
            .with(applicationContext)
            .load("https://s3.amazonaws.com/" + data.getData()!!.getBucket() + "/" + data.getData()!!.getImage())
            .centerCrop()
            .placeholder(R.drawable.img_pasta)
            .into(binding.imgRecipe)
//        replaceFragment(IngredientsFragment())

        binding.txtRecipeName.text = data.getData()!!.getName()
        binding.txtRecipeTime.text = data.getData()!!.getTime()
        binding.txtRecipeContain.text = "" + data.getData()!!.getCalories() + " Calories | " +
                data.getData()!!.getProtein() + " Protein | " +
                data.getData()!!.getCarbs() + " Carbs | " +
                data.getData()!!.getSugars() + " Sugars | " +
                data.getData()!!.getFat() + " Fat | " +
                data.getData()!!.getSaturatedFat() + " Saturated Fat | " +
                data.getData()!!.getFibre() + " Fibre | " +
                data.getData()!!.getSodium() + " Sodium | "

        btn_ingredients.isChecked = true
        replaceFragment(IngredientsFragment(), 0)

    }

    override fun onBackPressed() {
        super.onBackPressed()
    }


    @SuppressLint("WrongConstant")
    private fun replaceFragment(fragment: Fragment, fragmentIndex: Int) {

        val transaction = supportFragmentManager.beginTransaction()
        val bundle = Bundle()
        val gson = Gson()
        if (fragmentIndex == 0) {
            bundle.putString("recipeInfo", gson.toJson(data!!.getData()!!.getRecipeIngredients()))
        } else if (fragmentIndex == 1) {
            bundle.putString("recipeInfo", gson.toJson(data!!.getData()!!.getPreparationSteps()))
        }
        fragment.arguments = bundle
        transaction.replace(R.id.fragment_container, fragment)
        transaction.commit()
    }
}