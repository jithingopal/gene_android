package com.muhdo.app.ui.epigentic.execrise.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ExerciseInfo {

    @SerializedName("_id")
    @Expose
    private String id;

    @SerializedName("user_id")
    @Expose
    private String userId;

    @SerializedName("exercise_type")
    @Expose
    private String exerciseType;

    @SerializedName("training_time")
    @Expose
    private String trainingTime;

    @SerializedName("intense")
    @Expose
    private String intense;


    @SerializedName("created_at")
    @Expose
    private String createdDate;

    @SerializedName("updated_at")
    @Expose
    private String updateDate;

    @SerializedName("__v")
    @Expose
    private String v;

    @SerializedName("date_group")
    @Expose
    private String dateGroup;


    @SerializedName("kit_id")
    @Expose
    private String kitId;

    @SerializedName("biological_age")
    @Expose
    private String biologicalAge;

    @SerializedName("sleep_index")
    @Expose
    private String sleepIndex;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getExerciseType() {
        return exerciseType;
    }

    public void setExerciseType(String exerciseType) {
        this.exerciseType = exerciseType;
    }

    public String getTrainingTime() {
        return trainingTime;
    }

    public void setTrainingTime(String trainingTime) {
        this.trainingTime = trainingTime;
    }

    public String getIntense() {
        return intense;
    }

    public void setIntense(String intense) {
        this.intense = intense;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }

    public String getV() {
        return v;
    }

    public void setV(String v) {
        this.v = v;
    }

    public String getDateGroup() {
        return dateGroup;
    }

    public void setDateGroup(String dateGroup) {
        this.dateGroup = dateGroup;
    }

    public String getBiologicalAge() {
        return biologicalAge;
    }

    public void setBiologicalAge(String biologicalAge) {
        this.biologicalAge = biologicalAge;
    }

    public String getSleepIndex() {
        return sleepIndex;
    }

    public void setSleepIndex(String sleepIndex) {
        this.sleepIndex = sleepIndex;
    }

    public String getKitId() {
        return kitId;
    }

    public void setKitId(String kitId) {
        this.kitId = kitId;
    }
}
