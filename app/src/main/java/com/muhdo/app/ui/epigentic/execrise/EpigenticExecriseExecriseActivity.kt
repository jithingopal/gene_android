package com.muhdo.app.ui.epigentic.execrise

import android.annotation.SuppressLint
import android.app.ProgressDialog
import android.content.Context
import android.os.Build
import android.os.Bundle
import android.util.Base64
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.google.gson.GsonBuilder
import com.mindorks.placeholderview.ExpandablePlaceHolderView
import com.mindorks.placeholderview.annotations.Layout
import com.mindorks.placeholderview.annotations.Resolve
import com.mindorks.placeholderview.annotations.expand.ChildPosition
import com.mindorks.placeholderview.annotations.expand.ParentPosition
import com.muhdo.app.R
import com.muhdo.app.databinding.ActivityEpigenticExecriseExecriseLayoutBinding
import com.muhdo.app.repository.ApiUtilis
import com.muhdo.app.repository.ErrorModel
import com.muhdo.app.repository.NetworkManager
import com.muhdo.app.repository.ServiceListener
import com.muhdo.app.ui.epigentic.execrise.model.ExerciseInfo
import com.muhdo.app.ui.epigentic.execrise.response.ExecriseResponseModel
import com.muhdo.app.ui.epigentic.execrise.response.ExerciseListResponse

import com.muhdo.app.ui.epigentic.medication.response.Feed

import com.muhdo.app.ui.epigentic.supplyments.view.HeadingView
import com.muhdo.app.utils.v3.Constants
import com.timingsystemkotlin.backuptimingsystem.Utility
import kotlinx.android.synthetic.main.activity_epigentic_execrise_execrise_layout.view.*
import org.json.JSONArray
import java.io.IOException
import java.io.InputStream
import java.util.*

class EpigenticExecriseExecriseActivity : AppCompatActivity() {
    private var progressDialog: ProgressDialog? = null
    private var mExpandableView: ExpandablePlaceHolderView? = null
    private var mContext: Context? = null
    //private var mOnScrollListener: ExpandablePlaceHolderView.OnScrollListener? = null
    var show = false
    var execriseType = "GYM"
    var trainingTime = "0,30"


    open lateinit var binding: ActivityEpigenticExecriseExecriseLayoutBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(
            this@EpigenticExecriseExecriseActivity,
            R.layout.activity_epigentic_execrise_execrise_layout
        )
        Dialog(this)
        mExpandableView = findViewById(R.id.expandable_view_execrise) as ExpandablePlaceHolderView
        binding.btnBack.setOnClickListener {
            finish()
            /* if(show==true){
                 binding.layoutExecrise2.visibility= View.GONE
                 binding.layoutAddExecrise.visibility=View.GONE
                 binding.layoutExecrise1.visibility= View.VISIBLE
                 binding.layoutNotAddExecrise.visibility= View.VISIBLE
                 show=false;
             }else{
                 finish()
             }*/
        }
        // clickExccriseOption()
        clickAddExecriseView()
        clickNotAddExecriseView()
        // loadFeeds(this)
        clickOnLayoutGym()
        clickOnLayoutRun()
        clickOnLayoutBike()
        clickOnLayoutSwim()
        clickOnLayoutYoga()
        clickOnLayoutPilates()
        clickOnLayoutTeam()
        clickOnLayoutOther()
        showProgressDialog()
        clickOnDoneBtn()
        seekbarProgressChange()
        sendExecriseDataOnServer(this@EpigenticExecriseExecriseActivity)
        callExerciseService(this@EpigenticExecriseExecriseActivity)

        /*for (feed in this!!.loadFeeds(this)!!) {
           mExpandableView!!.addView(HeadingView(this, feed.getHeading()))
           for (info in feed.getInfoList()) {
               mExpandableView!!.addView(ExcrisetemView(this, info))
           }
       }*/
    }

    private fun clickOnDoneBtn() {
        binding.layoutBtnDone.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                finish()
            }

        })
    }

    private fun seekbarProgressChange() {
        // Set a SeekBar change listener
        binding.seekBarBn.setProgress(60)
        binding.seekBarBn.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {

            override fun onProgressChanged(seekBar: SeekBar, progress: Int, b: Boolean) {
                // Display the current progress of SeekBar
                if (progress <= 30) {
                    trainingTime = "0,30"
                    binding.textViewSeekbarProgress.text = "0 MIN - 30 MIN"
                    binding.seekBarBn.setProgress(25)
                } else if (progress < 60) {
                    trainingTime = "30,60"
                    binding.textViewSeekbarProgress.text = "30 MIN - 60 MIN"
                } else if (progress < 90) {
                    trainingTime = "60,90"
                    binding.textViewSeekbarProgress.text = "60 MIN - 90 MIN"
                } else if (progress < 120) {
                    trainingTime = "90,120"
                    binding.textViewSeekbarProgress.text = "90 MIN - 120 MIN"
                } else if (progress < 150) {
                    trainingTime = "120,150"
                    binding.textViewSeekbarProgress.text = "120 MIN - 150 MIN"
                    binding.seekBarBn.setProgress(125)
                }

            }

            override fun onStartTrackingTouch(seekBar: SeekBar) {
                // Do something


            }

            override fun onStopTrackingTouch(seekBar: SeekBar) {

            }
        })

    }

    private fun clickAddExecriseView() {
        binding.layoutAddExecrise.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                Log.d("data", "click a")
                show = true
                binding.layoutExecrise2.visibility = View.GONE
                binding.layoutAddExecrise.visibility = View.GONE
                binding.layoutLoggedExcriseView.visibility = View.GONE

                binding.scrollHorizontal.visibility = View.VISIBLE
                binding.layoutExecrise1.visibility = View.VISIBLE
                binding.layoutNotAddExecrise.visibility = View.VISIBLE
                binding.layoutGymRun.visibility = View.VISIBLE

            }
        })
    }

    private fun clickNotAddExecriseView() {
        binding.layoutNotAddExecrise.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                Log.d("data", "click a")
                show = false
                // Toast.makeText(applicationContext,"hello", Toast.LENGTH_SHORT).show();
                binding.layoutExecrise2.visibility = View.VISIBLE
                binding.layoutAddExecrise.visibility = View.VISIBLE
                binding.scrollHorizontal.visibility = View.VISIBLE
                binding.layoutLoggedExcriseView.visibility = View.VISIBLE
                binding.layoutExecrise1.visibility = View.GONE
                binding.layoutNotAddExecrise.visibility = View.GONE
                binding.layoutGymRun.visibility = View.GONE


            }
        })
    }

    private fun loadJSONFromAsset(context: Context, jsonFileName: String): String? {
        var json: String? = null
        var `is`: InputStream? = null
        try {
            val manager = context.assets
            Log.d("data", "path $jsonFileName")
            `is` = manager.open(jsonFileName)
            val size = `is`!!.available()
            val buffer = ByteArray(size)
            `is`.read(buffer)
            `is`.close()
            json = String(buffer)
        } catch (ex: IOException) {
            ex.printStackTrace()
            return null
        }

        return json
    }

    fun loadFeeds(context: Context): List<Feed>? {
        try {
            val builder = GsonBuilder()
            val gson = builder.create()
            val array = JSONArray(loadJSONFromAsset(context, "news.json"))
            val feedList = ArrayList<Feed>()
            for (i in 0 until array.length()) {
                val feed = gson.fromJson<Feed>(array.getString(i), Feed::class.java!!)
                feedList.add(feed)
            }
            return feedList
        } catch (e: Exception) {
            Log.d("data", "seedGames parseException $e")
            e.printStackTrace()
            return null
        }

    }

    private fun clickOnLayoutGym() {
        binding.layoutGym.setOnClickListener(object : View.OnClickListener {
            @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
            override fun onClick(v: View?) {
                execriseType = "GYM"
                binding.layoutGym.imageView_gym.setBackgroundTintList(
                    getResources().getColorStateList(
                        R.color.white
                    )
                );
                binding.layoutRun.imageView_run.setBackgroundTintList(
                    getResources().getColorStateList(
                        R.color._execrise_img_defult_color
                    )
                );
                binding.layoutBike.imageView_bike.setBackgroundTintList(
                    getResources().getColorStateList(
                        R.color._execrise_img_defult_color
                    )
                );
                binding.layoutSwim.imageView_swim.setBackgroundTintList(
                    getResources().getColorStateList(
                        R.color._execrise_img_defult_color
                    )
                );
                binding.layoutYoga.imageView_yoga.setBackgroundTintList(
                    getResources().getColorStateList(
                        R.color._execrise_img_defult_color
                    )
                );
                binding.layoutPilates.imageView_pilates.setBackgroundTintList(
                    getResources().getColorStateList(
                        R.color._execrise_img_defult_color
                    )
                );
                binding.layoutTeam.imageView_team.setBackgroundTintList(
                    getResources().getColorStateList(
                        R.color._execrise_img_defult_color
                    )
                );
                binding.layoutOther.imageView_other.setBackgroundTintList(
                    getResources().getColorStateList(
                        R.color._execrise_img_defult_color
                    )
                );

                binding.layoutGym.textView_gym.setTextColor(resources.getColor(R.color.white))
                binding.layoutRun.textView_run.setTextColor(resources.getColor(R.color._execrise_img_defult_color))
                binding.layoutBike.textView_bike.setTextColor(resources.getColor(R.color._execrise_img_defult_color))
                binding.layoutSwim.textView_swim.setTextColor(resources.getColor(R.color._execrise_img_defult_color))
                binding.layoutYoga.textView_yoga.setTextColor(resources.getColor(R.color._execrise_img_defult_color))
                binding.layoutPilates.textView_pilates.setTextColor(resources.getColor(R.color._execrise_img_defult_color))
                binding.layoutTeam.textView_team.setTextColor(resources.getColor(R.color._execrise_img_defult_color))
                binding.layoutOther.textView_other.setTextColor(resources.getColor(R.color._execrise_img_defult_color))


            }
        })
    }

    private fun clickOnLayoutRun() {
        binding.layoutRun.setOnClickListener(object : View.OnClickListener {
            @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
            override fun onClick(v: View?) {
                execriseType = "RUN"
                binding.layoutGym.imageView_gym.setBackgroundTintList(
                    getResources().getColorStateList(
                        R.color._execrise_img_defult_color
                    )
                );
                binding.layoutRun.imageView_run.setBackgroundTintList(
                    getResources().getColorStateList(
                        R.color.white
                    )
                );
                binding.layoutBike.imageView_bike.setBackgroundTintList(
                    getResources().getColorStateList(
                        R.color._execrise_img_defult_color
                    )
                );
                binding.layoutSwim.imageView_swim.setBackgroundTintList(
                    getResources().getColorStateList(
                        R.color._execrise_img_defult_color
                    )
                );
                binding.layoutYoga.imageView_yoga.setBackgroundTintList(
                    getResources().getColorStateList(
                        R.color._execrise_img_defult_color
                    )
                );
                binding.layoutPilates.imageView_pilates.setBackgroundTintList(
                    getResources().getColorStateList(
                        R.color._execrise_img_defult_color
                    )
                );
                binding.layoutTeam.imageView_team.setBackgroundTintList(
                    getResources().getColorStateList(
                        R.color._execrise_img_defult_color
                    )
                );
                binding.layoutOther.imageView_other.setBackgroundTintList(
                    getResources().getColorStateList(
                        R.color._execrise_img_defult_color
                    )
                );

                binding.layoutGym.textView_gym.setTextColor(resources.getColor(R.color._execrise_img_defult_color))
                binding.layoutRun.textView_run.setTextColor(resources.getColor(R.color.white))
                binding.layoutBike.textView_bike.setTextColor(resources.getColor(R.color._execrise_img_defult_color))
                binding.layoutSwim.textView_swim.setTextColor(resources.getColor(R.color._execrise_img_defult_color))
                binding.layoutYoga.textView_yoga.setTextColor(resources.getColor(R.color._execrise_img_defult_color))
                binding.layoutPilates.textView_pilates.setTextColor(resources.getColor(R.color._execrise_img_defult_color))
                binding.layoutTeam.textView_team.setTextColor(resources.getColor(R.color._execrise_img_defult_color))
                binding.layoutOther.textView_other.setTextColor(resources.getColor(R.color._execrise_img_defult_color))

            }
        })
    }

    private fun clickOnLayoutBike() {
        binding.layoutBike.setOnClickListener(object : View.OnClickListener {
            @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
            override fun onClick(v: View?) {
                execriseType = "BIKE"
                binding.layoutGym.imageView_gym.setBackgroundTintList(
                    getResources().getColorStateList(
                        R.color._execrise_img_defult_color
                    )
                );
                binding.layoutRun.imageView_run.setBackgroundTintList(
                    getResources().getColorStateList(
                        R.color._execrise_img_defult_color
                    )
                );
                binding.layoutBike.imageView_bike.setBackgroundTintList(
                    getResources().getColorStateList(
                        R.color.white
                    )
                );
                binding.layoutSwim.imageView_swim.setBackgroundTintList(
                    getResources().getColorStateList(
                        R.color._execrise_img_defult_color
                    )
                );
                binding.layoutYoga.imageView_yoga.setBackgroundTintList(
                    getResources().getColorStateList(
                        R.color._execrise_img_defult_color
                    )
                );
                binding.layoutPilates.imageView_pilates.setBackgroundTintList(
                    getResources().getColorStateList(
                        R.color._execrise_img_defult_color
                    )
                );
                binding.layoutTeam.imageView_team.setBackgroundTintList(
                    getResources().getColorStateList(
                        R.color._execrise_img_defult_color
                    )
                );
                binding.layoutOther.imageView_other.setBackgroundTintList(
                    getResources().getColorStateList(
                        R.color._execrise_img_defult_color
                    )
                );

                binding.layoutGym.textView_gym.setTextColor(resources.getColor(R.color._execrise_img_defult_color))
                binding.layoutRun.textView_run.setTextColor(resources.getColor(R.color._execrise_img_defult_color))
                binding.layoutBike.textView_bike.setTextColor(resources.getColor(R.color.white))
                binding.layoutSwim.textView_swim.setTextColor(resources.getColor(R.color._execrise_img_defult_color))
                binding.layoutYoga.textView_yoga.setTextColor(resources.getColor(R.color._execrise_img_defult_color))
                binding.layoutPilates.textView_pilates.setTextColor(resources.getColor(R.color._execrise_img_defult_color))
                binding.layoutTeam.textView_team.setTextColor(resources.getColor(R.color._execrise_img_defult_color))
                binding.layoutOther.textView_other.setTextColor(resources.getColor(R.color._execrise_img_defult_color))

            }
        })
    }

    private fun clickOnLayoutSwim() {
        binding.layoutSwim.setOnClickListener(object : View.OnClickListener {
            @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
            override fun onClick(v: View?) {
                execriseType = "SWIM"
                binding.layoutGym.imageView_gym.setBackgroundTintList(
                    getResources().getColorStateList(
                        R.color._execrise_img_defult_color
                    )
                );
                binding.layoutRun.imageView_run.setBackgroundTintList(
                    getResources().getColorStateList(
                        R.color._execrise_img_defult_color
                    )
                );
                binding.layoutBike.imageView_bike.setBackgroundTintList(
                    getResources().getColorStateList(
                        R.color._execrise_img_defult_color
                    )
                );
                binding.layoutSwim.imageView_swim.setBackgroundTintList(
                    getResources().getColorStateList(
                        R.color.white
                    )
                );
                binding.layoutYoga.imageView_yoga.setBackgroundTintList(
                    getResources().getColorStateList(
                        R.color._execrise_img_defult_color
                    )
                );
                binding.layoutPilates.imageView_pilates.setBackgroundTintList(
                    getResources().getColorStateList(
                        R.color._execrise_img_defult_color
                    )
                );
                binding.layoutTeam.imageView_team.setBackgroundTintList(
                    getResources().getColorStateList(
                        R.color._execrise_img_defult_color
                    )
                );
                binding.layoutOther.imageView_other.setBackgroundTintList(
                    getResources().getColorStateList(
                        R.color._execrise_img_defult_color
                    )
                );

                binding.layoutGym.textView_gym.setTextColor(resources.getColor(R.color._execrise_img_defult_color))
                binding.layoutRun.textView_run.setTextColor(resources.getColor(R.color._execrise_img_defult_color))
                binding.layoutBike.textView_bike.setTextColor(resources.getColor(R.color._execrise_img_defult_color))
                binding.layoutSwim.textView_swim.setTextColor(resources.getColor(R.color.white))
                binding.layoutYoga.textView_yoga.setTextColor(resources.getColor(R.color._execrise_img_defult_color))
                binding.layoutPilates.textView_pilates.setTextColor(resources.getColor(R.color._execrise_img_defult_color))
                binding.layoutTeam.textView_team.setTextColor(resources.getColor(R.color._execrise_img_defult_color))
                binding.layoutOther.textView_other.setTextColor(resources.getColor(R.color._execrise_img_defult_color))
            }
        })
    }

    private fun clickOnLayoutYoga() {
        binding.layoutYoga.setOnClickListener(object : View.OnClickListener {
            @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
            override fun onClick(v: View?) {
                execriseType = "YOGA"
                binding.layoutGym.imageView_gym.setBackgroundTintList(
                    getResources().getColorStateList(
                        R.color._execrise_img_defult_color
                    )
                );
                binding.layoutRun.imageView_run.setBackgroundTintList(
                    getResources().getColorStateList(
                        R.color._execrise_img_defult_color
                    )
                );
                binding.layoutBike.imageView_bike.setBackgroundTintList(
                    getResources().getColorStateList(
                        R.color._execrise_img_defult_color
                    )
                );
                binding.layoutSwim.imageView_swim.setBackgroundTintList(
                    getResources().getColorStateList(
                        R.color._execrise_img_defult_color
                    )
                );
                binding.layoutYoga.imageView_yoga.setBackgroundTintList(
                    getResources().getColorStateList(
                        R.color.white
                    )
                );
                binding.layoutPilates.imageView_pilates.setBackgroundTintList(
                    getResources().getColorStateList(
                        R.color._execrise_img_defult_color
                    )
                );
                binding.layoutTeam.imageView_team.setBackgroundTintList(
                    getResources().getColorStateList(
                        R.color._execrise_img_defult_color
                    )
                );
                binding.layoutOther.imageView_other.setBackgroundTintList(
                    getResources().getColorStateList(
                        R.color._execrise_img_defult_color
                    )
                );

                binding.layoutGym.textView_gym.setTextColor(resources.getColor(R.color._execrise_img_defult_color))
                binding.layoutRun.textView_run.setTextColor(resources.getColor(R.color._execrise_img_defult_color))
                binding.layoutBike.textView_bike.setTextColor(resources.getColor(R.color._execrise_img_defult_color))
                binding.layoutSwim.textView_swim.setTextColor(resources.getColor(R.color._execrise_img_defult_color))
                binding.layoutYoga.textView_yoga.setTextColor(resources.getColor(R.color.white))
                binding.layoutPilates.textView_pilates.setTextColor(resources.getColor(R.color._execrise_img_defult_color))
                binding.layoutTeam.textView_team.setTextColor(resources.getColor(R.color._execrise_img_defult_color))
                binding.layoutOther.textView_other.setTextColor(resources.getColor(R.color._execrise_img_defult_color))
            }
        })
    }

    private fun clickOnLayoutPilates() {
        binding.layoutPilates.setOnClickListener(object : View.OnClickListener {
            @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
            override fun onClick(v: View?) {
                execriseType = "PILATES"
                binding.layoutGym.imageView_gym.setBackgroundTintList(
                    getResources().getColorStateList(
                        R.color._execrise_img_defult_color
                    )
                );
                binding.layoutRun.imageView_run.setBackgroundTintList(
                    getResources().getColorStateList(
                        R.color._execrise_img_defult_color
                    )
                );
                binding.layoutBike.imageView_bike.setBackgroundTintList(
                    getResources().getColorStateList(
                        R.color._execrise_img_defult_color
                    )
                );
                binding.layoutSwim.imageView_swim.setBackgroundTintList(
                    getResources().getColorStateList(
                        R.color._execrise_img_defult_color
                    )
                );
                binding.layoutYoga.imageView_yoga.setBackgroundTintList(
                    getResources().getColorStateList(
                        R.color._execrise_img_defult_color
                    )
                );
                binding.layoutPilates.imageView_pilates.setBackgroundTintList(
                    getResources().getColorStateList(
                        R.color.white
                    )
                );
                binding.layoutTeam.imageView_team.setBackgroundTintList(
                    getResources().getColorStateList(
                        R.color._execrise_img_defult_color
                    )
                );
                binding.layoutOther.imageView_other.setBackgroundTintList(
                    getResources().getColorStateList(
                        R.color._execrise_img_defult_color
                    )
                );

                binding.layoutGym.textView_gym.setTextColor(resources.getColor(R.color._execrise_img_defult_color))
                binding.layoutRun.textView_run.setTextColor(resources.getColor(R.color._execrise_img_defult_color))
                binding.layoutBike.textView_bike.setTextColor(resources.getColor(R.color._execrise_img_defult_color))
                binding.layoutSwim.textView_swim.setTextColor(resources.getColor(R.color._execrise_img_defult_color))
                binding.layoutYoga.textView_yoga.setTextColor(resources.getColor(R.color._execrise_img_defult_color))
                binding.layoutPilates.textView_pilates.setTextColor(resources.getColor(R.color.white))
                binding.layoutTeam.textView_team.setTextColor(resources.getColor(R.color._execrise_img_defult_color))
                binding.layoutOther.textView_other.setTextColor(resources.getColor(R.color._execrise_img_defult_color))
            }
        })
    }

    private fun clickOnLayoutTeam() {
        binding.layoutTeam.setOnClickListener(object : View.OnClickListener {
            @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
            override fun onClick(v: View?) {
                execriseType = "TEAM"
                binding.layoutGym.imageView_gym.setBackgroundTintList(
                    getResources().getColorStateList(
                        R.color._execrise_img_defult_color
                    )
                );
                binding.layoutRun.imageView_run.setBackgroundTintList(
                    getResources().getColorStateList(
                        R.color._execrise_img_defult_color
                    )
                );
                binding.layoutBike.imageView_bike.setBackgroundTintList(
                    getResources().getColorStateList(
                        R.color._execrise_img_defult_color
                    )
                );
                binding.layoutSwim.imageView_swim.setBackgroundTintList(
                    getResources().getColorStateList(
                        R.color._execrise_img_defult_color
                    )
                );
                binding.layoutYoga.imageView_yoga.setBackgroundTintList(
                    getResources().getColorStateList(
                        R.color._execrise_img_defult_color
                    )
                );
                binding.layoutPilates.imageView_pilates.setBackgroundTintList(
                    getResources().getColorStateList(
                        R.color._execrise_img_defult_color
                    )
                );
                binding.layoutTeam.imageView_team.setBackgroundTintList(
                    getResources().getColorStateList(
                        R.color.white
                    )
                );
                binding.layoutOther.imageView_other.setBackgroundTintList(
                    getResources().getColorStateList(
                        R.color._execrise_img_defult_color
                    )
                );

                binding.layoutGym.textView_gym.setTextColor(resources.getColor(R.color._execrise_img_defult_color))
                binding.layoutRun.textView_run.setTextColor(resources.getColor(R.color._execrise_img_defult_color))
                binding.layoutBike.textView_bike.setTextColor(resources.getColor(R.color._execrise_img_defult_color))
                binding.layoutSwim.textView_swim.setTextColor(resources.getColor(R.color._execrise_img_defult_color))
                binding.layoutYoga.textView_yoga.setTextColor(resources.getColor(R.color._execrise_img_defult_color))
                binding.layoutPilates.textView_pilates.setTextColor(resources.getColor(R.color._execrise_img_defult_color))
                binding.layoutTeam.textView_team.setTextColor(resources.getColor(R.color.white))
                binding.layoutOther.textView_other.setTextColor(resources.getColor(R.color._execrise_img_defult_color))
            }
        })
    }

    private fun clickOnLayoutOther() {
        binding.layoutOther.setOnClickListener(object : View.OnClickListener {
            @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
            override fun onClick(v: View?) {
                execriseType = "OTHER"
                binding.layoutGym.imageView_gym.setBackgroundTintList(
                    getResources().getColorStateList(
                        R.color._execrise_img_defult_color
                    )
                );
                binding.layoutRun.imageView_run.setBackgroundTintList(
                    getResources().getColorStateList(
                        R.color._execrise_img_defult_color
                    )
                );
                binding.layoutBike.imageView_bike.setBackgroundTintList(
                    getResources().getColorStateList(
                        R.color._execrise_img_defult_color
                    )
                );
                binding.layoutSwim.imageView_swim.setBackgroundTintList(
                    getResources().getColorStateList(
                        R.color._execrise_img_defult_color
                    )
                );
                binding.layoutYoga.imageView_yoga.setBackgroundTintList(
                    getResources().getColorStateList(
                        R.color._execrise_img_defult_color
                    )
                );
                binding.layoutPilates.imageView_pilates.setBackgroundTintList(
                    getResources().getColorStateList(
                        R.color._execrise_img_defult_color
                    )
                );
                binding.layoutTeam.imageView_team.setBackgroundTintList(
                    getResources().getColorStateList(
                        R.color._execrise_img_defult_color
                    )
                );
                binding.layoutOther.imageView_other.setBackgroundTintList(
                    getResources().getColorStateList(
                        R.color.white
                    )
                );

                binding.layoutGym.textView_gym.setTextColor(resources.getColor(R.color._execrise_img_defult_color))
                binding.layoutRun.textView_run.setTextColor(resources.getColor(R.color._execrise_img_defult_color))
                binding.layoutBike.textView_bike.setTextColor(resources.getColor(R.color._execrise_img_defult_color))
                binding.layoutSwim.textView_swim.setTextColor(resources.getColor(R.color._execrise_img_defult_color))
                binding.layoutYoga.textView_yoga.setTextColor(resources.getColor(R.color._execrise_img_defult_color))
                binding.layoutPilates.textView_pilates.setTextColor(resources.getColor(R.color._execrise_img_defult_color))
                binding.layoutTeam.textView_team.setTextColor(resources.getColor(R.color._execrise_img_defult_color))
                binding.layoutOther.textView_other.setTextColor(resources.getColor(R.color.white))
            }
        })
    }

    private fun sendExecriseDataOnServer(mContext: EpigenticExecriseExecriseActivity) {
        var readioIntenseVal = "LOW";
        binding.radioBtnLow.setOnClickListener(View.OnClickListener {
            readioIntenseVal = "LOW"
            binding.radioBtnLow.isChecked = true
            binding.radioBtnMedium.isChecked = false
            binding.radioBtnHigh.isChecked = false
            binding.radioBtnVeryHigh.isChecked = false
            Log.d("data", "send data" + readioIntenseVal);
            //  Toast.makeText(applicationContext,"hello", Toast.LENGTH_SHORT).show();
        })


        binding.radioBtnMedium.setOnClickListener(View.OnClickListener {
            readioIntenseVal = "MEDIUM"
            binding.radioBtnMedium.isChecked = true
            binding.radioBtnHigh.isChecked = false
            binding.radioBtnVeryHigh.isChecked = false
            binding.radioBtnLow.isChecked = false
            //   Toast.makeText(applicationContext,"MEDIUM", Toast.LENGTH_SHORT).show();
        })

        binding.radioBtnHigh.setOnClickListener(View.OnClickListener {
            readioIntenseVal = "HIGH"
            binding.radioBtnHigh.isChecked = true
            binding.radioBtnMedium.isChecked = false
            binding.radioBtnLow.isChecked = false
            binding.radioBtnVeryHigh.isChecked = false
            // Toast.makeText(applicationContext,"HIGH", Toast.LENGTH_SHORT).show();

        })

        binding.radioBtnVeryHigh.setOnClickListener(View.OnClickListener {
            readioIntenseVal = "VERY HIGH"
            binding.radioBtnVeryHigh.isChecked = true
            binding.radioBtnHigh.isChecked = false
            binding.radioBtnMedium.isChecked = false
            binding.radioBtnLow.isChecked = false
        })


        binding.layoutSend.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                //Toast.makeText(applicationContext,"send", Toast.LENGTH_SHORT).show();
                mContext.showProgressDialog()
                val manager = NetworkManager()
                val params = HashMap<String, String>()
                val userId = Base64.decode(
                    Utility.getUserID(this@EpigenticExecriseExecriseActivity),
                    Base64.DEFAULT
                )
                val decodeUserId = String(userId, charset("UTF-8"))

                params["user_id"] = decodeUserId
                params["exercise_type"] = execriseType
                params["training_time"] = trainingTime
                params["intense"] = readioIntenseVal
                Log.d("data", "send data" + readioIntenseVal);
                //   Toast.makeText(applicationContext,"send", Toast.LENGTH_SHORT).show();

                if (manager.isConnectingToInternet(applicationContext)) {
                    manager.createApiRequest(
                        ApiUtilis.getAPIService(Constants.EXECRISE_API).sendExecriseData(params),
                        object : ServiceListener<ExecriseResponseModel> {
                            override fun getServerResponse(
                                response: ExecriseResponseModel,
                                requestcode: Int
                            ) {
                                //  binding.progressBar.visibility = View.GONE
                                // setData(response)

                                Log.d("data", "" + response.getCode())
                                if (response.getCode() == 200) {
                                    callExerciseService(this@EpigenticExecriseExecriseActivity)
                                    binding.layoutExecrise2.visibility = View.VISIBLE
                                    binding.layoutAddExecrise.visibility = View.VISIBLE
                                    binding.scrollHorizontal.visibility = View.VISIBLE
                                    binding.layoutLoggedExcriseView.visibility = View.VISIBLE
                                    binding.layoutExecrise1.visibility = View.GONE
                                    binding.layoutNotAddExecrise.visibility = View.GONE
                                    binding.layoutGymRun.visibility = View.GONE


                                } else {
                                    Utility.displayShortSnackBar(
                                        binding.parentLayout,
                                        response.getMessage().toString()
                                    )
                                }
                            }

                            override fun getError(error: ErrorModel, requestcode: Int) {
                                mContext.dismissProgressDialog()
                                //binding.progressBar.visibility = View.GONE
                                Utility.displayShortSnackBar(
                                    binding.parentLayout,
                                    error.getMessage().toString()
                                )
                            }
                        })
                } else {
                    //binding.progressBar.visibility = View.GONE
                    mContext.dismissProgressDialog()
                    Utility.displayShortSnackBar(
                        binding.parentLayout,
                        resources.getString(R.string.check_internet)
                    )
                }
            }

        })

        /* binding.layoutSendExecriseData.setOnClickListener {
             mContext.showProgressDialog()
             val manager = NetworkManager()
             val params = HashMap<String, String>()
             params["user_id"] = "1"
             params["exercise_type"] = execriseType
             params["training_time"] ="0,30"
             params["intense"] =readioIntenseVal
             Log.d("data","send data"+readioIntenseVal);
             Toast.makeText(applicationContext,"send", Toast.LENGTH_SHORT).show();


           *//*  if (manager.isConnectingToInternet(applicationContext)) {
                manager.createApiRequest(
                    ApiUtilis.getAPIService(Constants.EXECRISE_API).sendExecriseData(params),
                    object : ServiceListener<ExecriseResponseModel> {
                        override fun getServerResponse(response: ExecriseResponseModel, requestcode: Int) {
                            //  binding.progressBar.visibility = View.GONE
                            // setData(response)

                            Log.d("data",""+response.getCode());
                            if(response.getCode()==200){
                                callExerciseService(this@EpigenticExecriseExecriseActivity)
                            }
                        }

                        override fun getError(error: ErrorModel, requestcode: Int) {
                            mContext.dismissProgressDialog()
                            //binding.progressBar.visibility = View.GONE
                            Utility.displayShortSnackBar(
                                binding.parentLayout,
                                error.getMessage().toString()
                            )
                        }
                    })
            } else {
                //binding.progressBar.visibility = View.GONE
                mContext.dismissProgressDialog()
                Utility.displayShortSnackBar(
                    binding.parentLayout,
                    resources.getString(R.string.check_internet)
                )
            }*//*
        }*/
    }


    //Get user exercise data from server
    fun callExerciseService(mContext: EpigenticExecriseExecriseActivity) {
        // mContext.showProgressDialog()

        val userId = Base64.decode(Utility.getUserID(this), Base64.DEFAULT)
        val decodeUserId = String(userId, charset("UTF-8"))
        val manager = NetworkManager()
        if (manager.isConnectingToInternet(mContext)) {
            manager.createApiRequest(
                ApiUtilis.getAPIService(Constants.EXECRISE_API).getExerciseData(decodeUserId),
                object : ServiceListener<ExerciseListResponse> {
                    override fun getServerResponse(
                        response: ExerciseListResponse,
                        requestcode: Int
                    ) {
                        // dismissProgressDialog()
                        mContext.dismissProgressDialog()
                        //Log.d("data",""+response.dateGroupModelList.get(0).date_group);
                        mExpandableView?.removeAllViews()

                        try {

                            if (response.statusCode.equals("200") && response.dateGroupModelList.size > 0) {
                                Collections.reverse(response.dateGroupModelList)
                                if (response.dateGroupModelList != null) {
                                    for (dateGroup in response.dateGroupModelList) {
                                        mExpandableView!!.addView(
                                            HeadingView(
                                                mContext,
                                                dateGroup.date_group
                                            )
                                        )
                                        for (info in dateGroup.getInfoList()) {
                                            mExpandableView!!.addView(
                                                ExcriseItemView(
                                                    mContext,
                                                    info
                                                )
                                            )
                                        }
                                    }
                                }
                            } else {
                                Utility.displayShortSnackBar(
                                    binding.parentLayout,
                                    resources.getString(R.string.no_data)
                                )
                            }
                        } catch (e: Exception) {

                        }


                    }

                    override fun getError(error: ErrorModel, requestcode: Int) {
                        //binding.progressBar.visibility = View.GONE
                        // dismissProgressDialog()
                        mContext.dismissProgressDialog()
                        Utility.displayShortSnackBar(
                            binding.parentLayout,
                            error.getMessage().toString()
                        )
                    }
                })
        } else {
            mContext.dismissProgressDialog()
            //dismissProgressDialog()
            //binding.progressBar.visibility = View.GONE
            Utility.displayShortSnackBar(
                binding.parentLayout,
                resources.getString(R.string.check_internet)
            )

        }
    }


    @Layout(R.layout.row_excrise_heading_item)
    class ExcriseItemView(
        private val mContext: EpigenticExecriseExecriseActivity,
        private val mInfo: ExerciseInfo
    ) {

        @ParentPosition
        private val mParentPosition: Int = 0

        @ChildPosition
        private val mChildPosition: Int = 0

        @com.mindorks.placeholderview.annotations.View(R.id.textview_excrise_type)
        private val titleTxt: TextView? = null

        @com.mindorks.placeholderview.annotations.View(R.id.view_intense_duration)
        private val viewIntenseDuration: LinearLayout? = null

        @com.mindorks.placeholderview.annotations.View(R.id.layout_delete_exercise_heading_item)
        private val layoutDelete: LinearLayout? = null

        @com.mindorks.placeholderview.annotations.View(R.id.spinner_exercise_intensity)
        private val spinnerExecirseIntensity: Spinner? = null

        @com.mindorks.placeholderview.annotations.View(R.id.spinner_exercise_duration)
        private val spinnerExecirseDuration: Spinner? = null

        var data = arrayOf("LOW", "MEDIUM", "HIGH", "VERY HIGH")
        var durationData = arrayOf("0-30", "30-60", "60-90", "90-120", "120+")


        @Resolve
        private fun onResolved() {
            // methodSpinnerExecirseIntensity()

            if (mInfo.exerciseType.equals("REST") || mInfo.exerciseType.equals("INJURED")) {
                viewIntenseDuration!!.visibility = View.GONE

            } else {
                viewIntenseDuration!!.visibility = View.VISIBLE
            }
            titleTxt!!.text = mInfo.exerciseType
            layoutDelete?.setOnClickListener(object : android.view.View.OnClickListener {
                override fun onClick(v: android.view.View?) {
                    Log.d("data", "click a")
                    //  Toast.makeText(mContext,""+mInfo.id, Toast.LENGTH_SHORT).show()
                    callDeleteService(mContext);
                }

                fun callDeleteService(mContext: EpigenticExecriseExecriseActivity) {
                    mContext.showProgressDialog()
                    val manager = NetworkManager()
                    if (manager.isConnectingToInternet(mContext)) {
                        manager.createApiRequest(
                            ApiUtilis.getAPIService(Constants.EXECRISE_API).deleteExercise(mInfo.id),
                            object : ServiceListener<ExecriseResponseModel> {
                                override fun getServerResponse(
                                    response: ExecriseResponseModel,
                                    requestcode: Int
                                ) {
                                    if (response.getCode() == 200) {
                                        mContext.callExerciseService(mContext)
                                    } else {
                                        Utility.displayShortSnackBar(
                                            mContext.binding.parentLayout,
                                            response.getMessage().toString()
                                        )
                                    }


                                }

                                override fun getError(error: ErrorModel, requestcode: Int) {
                                    //binding.progressBar.visibility = View.GONE
                                    mContext.dismissProgressDialog()
                                    Utility.displayShortSnackBar(
                                        mContext.binding.parentLayout,
                                        error.getMessage().toString()
                                    )
                                }
                            })
                    } else {
                        mContext.dismissProgressDialog()
                        // binding.progressBar.visibility = View.GONE

                        Utility.displayShortSnackBar(
                            mContext.binding.parentLayout,
                            mContext.resources.getString(R.string.check_internet)
                        )
                    }
                }

            })

            methodSpinnerExecirseIntensity(mInfo)
            //  methodSpinnerExecirseDuration(mInfo)
        }


        @SuppressLint("ResourceType")
        private fun methodSpinnerExecirseIntensity(mInfo: ExerciseInfo) {
            // var isSpinnerInitial = true
            var intanse = ""
            var duration = ""

            var defaultSelPosIntenseSpiner = 0;
            var defaultSelPosDurationSpiner = 0;

            defaultSelPosIntenseSpiner = data.indexOf(mInfo.intense);
            var selTraingTimeVal = mInfo.trainingTime.replace(",", "-");
            defaultSelPosDurationSpiner = durationData.indexOf(selTraingTimeVal);

            if (defaultSelPosIntenseSpiner < 0) defaultSelPosIntenseSpiner = 0
            if (defaultSelPosDurationSpiner < 0) defaultSelPosDurationSpiner = 0

            var spinnerAdapter: CustomDropDownAdapter = CustomDropDownAdapter(mContext!!, data)

            spinnerExecirseIntensity?.adapter = spinnerAdapter
            spinnerExecirseIntensity?.onItemSelectedListener =
                object : AdapterView.OnItemSelectedListener {
                    override fun onItemSelected(
                        parent: AdapterView<*>?,
                        view: android.view.View?,
                        position: Int,
                        id: Long
                    ) {
                        var spinnerValIntense = data[position]
                        var text = spinnerExecirseIntensity?.getSelectedItem().toString();
                        intanse = data[position]

                        Log.wtf(
                            "data", " " + spinnerValIntense + " " + mInfo.trainingTime
                                    + ":in=" + intanse + ": pos=" + position + " id=" + id
                        );
                        // programmatically set the drop down : no need to call web service
                        if (defaultSelPosIntenseSpiner == position) {
                            Log.wtf("data", "intanse No need to call API");
                        }
                        // user chnage the drop down call the webservice
                        else {
                            Log.wtf("data", "intanse Yes call API");
                            updateExercisedata(duration, intanse, mInfo)
                        }
                    }

                    override fun onNothingSelected(parent: AdapterView<*>) {
                    }
                }

            // set default value

            spinnerExecirseIntensity?.setSelection(defaultSelPosIntenseSpiner);
            // ---------------- Duration spiner change event
            var spinnerAdapterDuration: CustomDropDownAdapterDuration =
                CustomDropDownAdapterDuration(mContext!!, durationData)
            spinnerExecirseDuration?.adapter = spinnerAdapterDuration
            spinnerExecirseDuration?.onItemSelectedListener =
                object : AdapterView.OnItemSelectedListener {
                    override fun onItemSelected(
                        parent: AdapterView<*>?,
                        view: android.view.View?,
                        position: Int,
                        id: Long
                    ) {
                        var spinnerValDuration = durationData[position]
                        Log.d("data", " " + spinnerValDuration + " " + mInfo.intense)
                        duration = durationData[position]
                        Log.wtf(
                            "data", " " + spinnerValDuration + " " + mInfo.trainingTime
                                    + ":in=" + intanse + ": pos=" + position
                        );
                        // programmatically set the drop down : no need to call web service
                        if (defaultSelPosDurationSpiner == position) {
                            Log.wtf("data", "duration No need to call API");
                        }
                        // user chnage the drop down call the webservice
                        else {
                            Log.wtf(
                                "data",
                                "duration  Yes call API" + defaultSelPosDurationSpiner + " == " + position
                            );
                            updateExercisedata(duration, intanse, mInfo)
                        }

                    }

                    override fun onNothingSelected(parent: AdapterView<*>) {
                    }

                }
            Log.wtf(
                "data",
                "trainingTime===" + mInfo.trainingTime + " : " + defaultSelPosDurationSpiner
            );
            spinnerExecirseDuration?.setSelection(defaultSelPosDurationSpiner);
        }

        private fun updateExercisedata(
            duration: String,
            intanse: String,
            mInfo: ExerciseInfo
        ) {
            // Toast.makeText(mContext,"start tracking"+duration+" "+intanse,Toast.LENGTH_SHORT).show()
            var durationTime = ""
            if (duration.equals("0-30")) {
                durationTime = "0,30"
            } else if (duration.equals("30-60")) {
                durationTime = "30,60"
            } else if (duration.equals("60-90")) {
                durationTime = "60,90"
            } else if (duration.equals("90-120")) {
                durationTime = "90,120"
            } else if (duration.equals("120-150")) {
                durationTime = "120,150"
            }


            mContext.showProgressDialog()
            val manager = NetworkManager()
            val params = HashMap<String, String>()
            params["_id"] = mInfo.id
            params["training_time"] = durationTime
            params["intense"] = intanse
            //   Toast.makeText(applicationContext,"send", Toast.LENGTH_SHORT).show();

            if (manager.isConnectingToInternet(mContext)) {
                manager.createApiRequest(
                    ApiUtilis.getAPIService(Constants.EXECRISE_API).updateExecriseData(params),
                    object : ServiceListener<ExecriseResponseModel> {
                        override fun getServerResponse(
                            response: ExecriseResponseModel,
                            requestcode: Int
                        ) {
                            //  binding.progressBar.visibility = View.GONE
                            // setData(response)

                            Log.d("data", "" + response.getCode());
                            if (response.getCode() == 200) {
                                mContext.callExerciseService(mContext)
                            }
                        }

                        override fun getError(error: ErrorModel, requestcode: Int) {
                            mContext.dismissProgressDialog()
                            //binding.progressBar.visibility = View.GONE
                            /*Utility.displayShortSnackBar(
                                binding.parentLayout,
                                error.getMessage().toString()
                            )*/
                        }
                    })
            } else {
                //binding.progressBar.visibility = View.GONE
                mContext.dismissProgressDialog()
                /* Utility.displayShortSnackBar(
                     binding.parentLayout,
                     resources.getString(R.string.check_internet)
                 )*/
            }
        }


        @SuppressLint("ResourceType")
        private fun methodSpinnerExecirseDuration(mInfo: ExerciseInfo) {
            var spinnerAdapter: CustomDropDownAdapterDuration =
                CustomDropDownAdapterDuration(mContext!!, durationData)
            spinnerExecirseDuration?.adapter = spinnerAdapter
            spinnerExecirseDuration?.onItemSelectedListener =
                object : AdapterView.OnItemSelectedListener {
                    override fun onItemSelected(
                        parent: AdapterView<*>?,
                        view: android.view.View?,
                        position: Int,
                        id: Long
                    ) {
                        var spinnerValDuration = durationData[position]
                        var text = spinnerExecirseDuration?.getSelectedItem().toString();
                        Log.d("data", " " + spinnerValDuration + " " + mInfo.intense)

                    }

                    override fun onNothingSelected(parent: AdapterView<*>) {
                    }

                }
            spinnerExecirseDuration?.setSelection(data.indexOf(mInfo.trainingTime));
        }
    }


    class CustomDropDownAdapter(
        val context: Context,
        var listItemsTxt: Array<String>
    ) : BaseAdapter() {
        val mInflater: LayoutInflater = LayoutInflater.from(context)
        override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
            val view: View
            val vh: ItemRowHolder
            if (convertView == null) {
                view = mInflater.inflate(R.layout.row_exercise_spinner_intensity, parent, false)
                vh = ItemRowHolder(view)
                view?.tag = vh
            } else {
                view = convertView
                vh = view.tag as ItemRowHolder
            }

            // setting adapter item height programatically.

            vh.label.text = listItemsTxt.get(position)
            return view
        }

        override fun getItem(position: Int): Any? {
            return null
        }

        override fun getItemId(position: Int): Long {
            return 0

        }

        override fun getCount(): Int {
            return listItemsTxt.size
        }

        private class ItemRowHolder(row: View?) {

            val label: TextView

            init {
                this.label = row?.findViewById(R.id.textview_intensity) as TextView
            }
        }


    }


    class CustomDropDownAdapterDuration(val context: Context, var listItemsTxt: Array<String>) :
        BaseAdapter() {
        val mInflater: LayoutInflater = LayoutInflater.from(context)
        override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
            val view: View
            val vh: ItemRowHolder
            if (convertView == null) {
                view = mInflater.inflate(R.layout.row_exercise_spinner_intensity, parent, false)
                vh = ItemRowHolder(view)
                view?.tag = vh
            } else {
                view = convertView
                vh = view.tag as ItemRowHolder
            }

/*
            // setting adapter item height programatically.
            val params = view.layoutParams
            params.height = 60
            view.layoutParams = params
*/
            vh.label.text = listItemsTxt.get(position)
            return view
        }

        override fun getItem(position: Int): Any? {
            return null
        }

        override fun getItemId(position: Int): Long {
            return 0

        }

        override fun getCount(): Int {
            return listItemsTxt.size
        }

        private class ItemRowHolder(row: View?) {

            val label: TextView

            init {
                this.label = row?.findViewById(R.id.textview_intensity) as TextView
            }
        }
    }

    fun Dialog(context: Context) {
        try {
            progressDialog = ProgressDialog(context)
            progressDialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
            progressDialog!!.setProgressStyle(ProgressDialog.STYLE_SPINNER)
            progressDialog!!.setMessage(getString(R.string.please_wait))
            progressDialog!!.setCancelable(false)
        } catch (ignore: Exception) {
        }

    }

    fun showProgressDialog() {
        try {
            if (progressDialog != null) {
                progressDialog!!.show()
            }
        } catch (ignore: Exception) {
        }

    }

    fun dismissProgressDialog() {
        try {
            if (progressDialog != null) {
                progressDialog!!.dismiss()
            }
        } catch (ignore: Exception) {
        }

    }

}