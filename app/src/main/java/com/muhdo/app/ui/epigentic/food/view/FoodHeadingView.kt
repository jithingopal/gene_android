package com.muhdo.app.ui.epigentic.food.view

import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.mindorks.placeholderview.annotations.Layout
import com.mindorks.placeholderview.annotations.Resolve
import com.mindorks.placeholderview.annotations.View
import com.mindorks.placeholderview.annotations.expand.*
import com.muhdo.app.R
import com.muhdo.app.ui.epigentic.food.EpigenticFoodNewActivity

@Parent
@SingleTop
@Layout(R.layout.row_medication_heading)
class FoodHeadingView(private val mContext: EpigenticFoodNewActivity, private val mHeading: String) {

    @View(R.id.headingTxt)
    private val headingTxt: TextView? = null

    @View(R.id.toggleIcon)
    private val toggleIcon: ImageView? = null

    @Toggle(R.id.toggleView)
    private val toggleView: LinearLayout? = null

    @ParentPosition
    private val mParentPosition: Int = 0

    @Resolve
    private fun onResolved() {
        toggleIcon!!.setImageDrawable(mContext.resources.getDrawable(R.drawable.ic_black_arrow))
        headingTxt!!.text = mHeading
        mContext.cate=""

    }

    @Expand
    private fun onExpand() {
        mContext.cate=""
        toggleIcon!!.setImageDrawable(mContext.resources.getDrawable(R.drawable.ic_up_black_arrow))
    }

    @Collapse
    private fun onCollapse() {
        mContext.cate=""
        toggleIcon!!.setImageDrawable(mContext.resources.getDrawable(R.drawable.ic_black_arrow))
    }
}
