package com.muhdo.app.ui

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.View.GONE
import android.view.View.VISIBLE
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.github.mikephil.charting.data.PieData
import com.github.mikephil.charting.data.PieDataSet
import com.github.mikephil.charting.data.PieEntry
import com.github.mikephil.charting.formatter.PercentFormatter
import com.muhdo.app.R
import com.muhdo.app.adapter.CalorieAdapter
import com.muhdo.app.apiModel.mealModel.CalorieModel
import com.muhdo.app.databinding.ActivityCalorieBinding
import com.muhdo.app.repository.ApiUtilis
import com.muhdo.app.repository.ErrorModel
import com.muhdo.app.repository.NetworkManager
import com.muhdo.app.repository.ServiceListener
import com.timingsystemkotlin.backuptimingsystem.Utility
import kotlinx.android.synthetic.main.fragment_keydata.*

class CalorieActivity : AppCompatActivity() {

    lateinit var binding: ActivityCalorieBinding
    lateinit var adapter: CalorieAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this@CalorieActivity, R.layout.activity_calorie)
        getCalorieResponce()

        binding.btnBack.setOnClickListener {
            /*val i = Intent(this@CalorieActivity, MealActivity::class.java)

            i.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(i)*/
            onBackPressed()
        }

        binding.btnChooseMealPlan.setOnClickListener {
            val intent = Intent(this, ChooseMealActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
            finish()
        }

    }

    private fun getCalorieResponce() {
        binding.progressBar.visibility = VISIBLE
        val manager = NetworkManager()
        if (manager.isConnectingToInternet(applicationContext)) {
            manager.createApiRequest(
                ApiUtilis.getAPIInstance(applicationContext).getCalorieResponse(
                    Utility.getUserID(
                        applicationContext
                    )
                ),
                object : ServiceListener<CalorieModel> {
                    @SuppressLint("ObsoleteSdkInt")
                    override fun getServerResponse(response: CalorieModel, requestcode: Int) {
                        binding.progressBar.visibility = GONE
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                            setData(response)
                        }
                    }

                    override fun getError(error: ErrorModel, requestcode: Int) {
                        binding.progressBar.visibility = GONE
                        Utility.displayShortSnackBar(
                            binding.parentLayout,
                            error.getMessage().toString()
                        )
                    }
                })
        } else {
            binding.progressBar.visibility = GONE
            Utility.displayShortSnackBar(
                binding.parentLayout,
                resources.getString(R.string.check_internet)
            )

        }

    }

    private fun calculateTotalCalorie(response: CalorieModel): Double {
        var totalCount = 0.0
        for (i in 0 until response.getData()!!.size) {
            totalCount += response.getData()!![i].getKcal()!!
        }

        return totalCount
    }

    @RequiresApi(Build.VERSION_CODES.HONEYCOMB)
    private fun setPieChart(response: CalorieModel) {


        val yvalues = ArrayList<PieEntry>()
        for (i in 0 until response.getData()!!.size) {
            yvalues.add(
                PieEntry(
                    response.getData()!![i].getPercent()!!.toFloat(),
                    response.getData()!![i].getType()!!
                )
            )


        }

        pieChart.setUsePercentValues(true)

        val dataSet = PieDataSet(yvalues, "")


        dataSet.colors = mutableListOf(
            ContextCompat.getColor(applicationContext!!, R.color.color_pie_1),
            ContextCompat.getColor(applicationContext!!, R.color.color_pie_2),
            ContextCompat.getColor(applicationContext!!, R.color.color_pie_3),
            ContextCompat.getColor(applicationContext!!, R.color.color_pie_4),
            ContextCompat.getColor(applicationContext!!, R.color.color_pie_5),
            ContextCompat.getColor(applicationContext!!, R.color.color_pie_6)
        )

        pieChart.setEntryLabelColor(
            ContextCompat.getColor(
                applicationContext,
                R.color.colorPrimary
            )
        )


        pieChart.legend.isWordWrapEnabled = true
        val legend = pieChart.legend
        legend.textSize = 14f

        val data = PieData(dataSet)
        data.setValueFormatter(PercentFormatter(binding.pieChart))
        //dataSet.setColors(ColorTemplate.COLORFUL_COLORS)
        pieChart.isDrawHoleEnabled = false
        data.setValueTextSize(13f)
        pieChart.animateXY(1400, 1400)
        pieChart.description = null
        pieChart.data = data
        pieChart.isRotationEnabled = false

    }


    @RequiresApi(Build.VERSION_CODES.HONEYCOMB)
    private fun setData(response: CalorieModel) {
        binding.txtTotalCalorie.text = calculateTotalCalorie(response).toString()
        setPieChart(response)

        adapter = CalorieAdapter(applicationContext, response.getData()!!)
        binding.calorieList.layoutManager =
            LinearLayoutManager(applicationContext)
        binding.calorieList.isNestedScrollingEnabled = true
        binding.calorieList.itemAnimator = DefaultItemAnimator()
        binding.calorieList.adapter = adapter

    }

    override fun onBackPressed() {
        super.onBackPressed()
        /* val i = Intent(this@CalorieActivity, MealActivity::class.java)
         i.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
         startActivity(i)*/
    }
}