package com.muhdo.app.ui.epigentic.food.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.List;

public class DateGroupModel {

    @SerializedName("date_group")
    @Expose
    String date_group;

    @SerializedName("data")
    @Expose
    private List<FoodInfoModel> foodInfoList;

    public String getDate_group() {
        return date_group;
    }

    public void setDate_group(String date_group) {
        this.date_group = date_group;
    }

    public List<FoodInfoModel> getInfoList() {
        return foodInfoList;
    }

    public void setInfoList(List<FoodInfoModel> infoList) {
        this.foodInfoList = foodInfoList;
    }
}
