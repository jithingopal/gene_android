package com.muhdo.app.ui

import android.os.Build
import android.os.Bundle
import android.text.Html
import android.util.Base64
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil.setContentView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.mindorks.placeholderview.Utils.dpToPx
import com.muhdo.app.R
import com.muhdo.app.adapter.CustomSpinnerAdapter
import com.muhdo.app.apiModel.result.SleepBarChartResponse
import com.muhdo.app.apiModel.result.SleepCalculatorModelResponse
import com.muhdo.app.apiModel.result.SleepScore
import com.muhdo.app.databinding.ActivityResultChooseModeBinding
import com.muhdo.app.repository.ApiUtilis
import com.muhdo.app.repository.ErrorModel
import com.muhdo.app.repository.NetworkManager
import com.muhdo.app.repository.ServiceListener
import com.muhdo.app.ui.adapter.FoodDetailAdapter1
import com.muhdo.app.ui.adapter.FoodDetailAdapter2
import com.timingsystemkotlin.backuptimingsystem.Utility
import java.util.*


class ResultChooseMode : AppCompatActivity() {
    internal lateinit var binding: ActivityResultChooseModeBinding
    private val modeList: MutableList<String> = ArrayList()
    private var sleepType: String = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = setContentView(this@ResultChooseMode, R.layout.activity_result_choose_mode)
        getIntentData()
        getBarChartData()
        getModeList()

        binding.btnBack.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                finish()
            }

        })


        binding.imgBackToTop.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                binding.scrollView.fullScroll(View.FOCUS_UP)
            }

        })
    }

    private fun getBarChartData() {
        binding.progressBar.visibility = View.VISIBLE
        val manager = NetworkManager()
        val userId = Base64.decode(Utility.getUserID(this), Base64.DEFAULT)
        val decodeUserId = String(userId, charset("UTF-8"))
        if (manager.isConnectingToInternet(applicationContext)) {
            manager.createApiRequest(
                ApiUtilis.getAPIInstance(applicationContext).getBarChartDataForSleepIndex(
                    decodeUserId
                ),
                object : ServiceListener<SleepBarChartResponse> {
                    override fun getServerResponse(
                        sleepBarChartResponseObj: SleepBarChartResponse,
                        requestcode: Int
                    ) {
                        // binding.progressBar.visibility = View.GONE
                        try {
                            if (sleepBarChartResponseObj.getCode() == 200) {
                                if (!sleepBarChartResponseObj.sleepBarChartList!!.isEmpty()) {
                                    setListDataInBarChart(sleepBarChartResponseObj)
                                }
                            }
                        } catch (e: Exception) {

                        }
                    }

                    override fun getError(error: ErrorModel, requestcode: Int) {
                        // binding.progressBar.visibility = View.GONE
                        Utility.displayShortSnackBar(
                            binding.parentLayout,
                            error.getMessage().toString()
                        )
                    }
                })
        } else {
            //binding.progressBar.visibility = View.GONE
            Utility.displayShortSnackBar(
                binding.parentLayout,
                resources.getString(R.string.check_internet)
            )
        }

    }

    private fun setListDataInBarChart(sleepBarChartResponseObj: SleepBarChartResponse) {
        Log.d("data", "barchart list size " + sleepBarChartResponseObj.sleepBarChartList!!.size)
        if (sleepBarChartResponseObj.sleepBarChartList!![0].barChartDay.equals("SUN", false)) {
            if (sleepBarChartResponseObj.sleepBarChartList!![0].barChartSleepHours.equals(
                    "0",
                    false
                )
            ) {
                binding.rlBarChartSun.setBackgroundResource(R.drawable.bar_chart_bg_white)
                binding.textViewSunSleepDay.setTextColor(resources.getColor(R.color.color_dark_yellow))
                binding.textViewSunSleepHours.setTextColor(resources.getColor(R.color.color_dark_yellow))
            } else {

                val sleepHours =
                    sleepBarChartResponseObj.sleepBarChartList!![0].barChartSleepHours!!.toDouble();
                val height = 220 * sleepHours / 10
                binding.textViewSunSleepHours.text = sleepHours.toString() + "\nHOURS"
                val layoutParams = binding.rlBarChartTue.layoutParams as LinearLayout.LayoutParams
                layoutParams.height = dpToPx(height.toFloat())
                binding.rlBarChartSun.setLayoutParams(layoutParams)
                //binding.rlBarChartSun.getLayoutParams().height =4*height.toInt()
                binding.rlBarChartSun.setBackgroundResource(R.drawable.bar_chart_bg)
                binding.textViewSunSleepDay.setTextColor(resources.getColor(R.color.white))
                binding.textViewSunSleepHours.setTextColor(resources.getColor(R.color.white))
                binding.rlBarChartSun.requestLayout()

            }
        }

        if (sleepBarChartResponseObj.sleepBarChartList!![1].barChartDay.equals("MON", false)) {
            if (sleepBarChartResponseObj.sleepBarChartList!![1].barChartSleepHours.equals(
                    "0",
                    false
                )
            ) {
                binding.rlBarChartMon.setBackgroundResource(R.drawable.bar_chart_bg_white)
                binding.textViewMonSleepDay.setTextColor(resources.getColor(R.color.color_dark_yellow))
                binding.textViewMonSleepHours.setTextColor(resources.getColor(R.color.color_dark_yellow))
            } else {

                val sleepHours =
                    sleepBarChartResponseObj.sleepBarChartList!!.get(1).barChartSleepHours!!.toDouble();
                val height = 220 * sleepHours / 10
                binding.textViewMonSleepHours.text = sleepHours.toString() + "\nHOURS"
                val layoutParams =
                    binding.rlBarChartTue.getLayoutParams() as LinearLayout.LayoutParams
                layoutParams.height = dpToPx(height.toFloat())
                binding.rlBarChartMon.setLayoutParams(layoutParams)

                // binding.rlBarChartMon.getLayoutParams().height = 4*height.toInt()
                binding.rlBarChartMon.setBackgroundResource(R.drawable.bar_chart_bg)
                binding.textViewMonSleepDay.setTextColor(resources.getColor(R.color.white))
                binding.textViewMonSleepHours.setTextColor(resources.getColor(R.color.white))
                binding.rlBarChartMon.requestLayout()

            }
        }

        if (sleepBarChartResponseObj.sleepBarChartList!![2].barChartDay.equals("TUE", false)) {
            if (sleepBarChartResponseObj.sleepBarChartList!![2].barChartSleepHours.equals(
                    "0",
                    false
                )
            ) {
                binding.rlBarChartTue.setBackgroundResource(R.drawable.bar_chart_bg_white)
                binding.textViewTueSleepDay.setTextColor(resources.getColor(R.color.color_dark_yellow))
                binding.textViewTueSleepHours.setTextColor(resources.getColor(R.color.color_dark_yellow))
            } else {

                val sleepHours =
                    sleepBarChartResponseObj.sleepBarChartList!![2].barChartSleepHours!!.toDouble();
                val height = 220 * sleepHours / 10
                Log.d("data", "height = $height $sleepHours")
                binding.textViewTueSleepHours.text = sleepHours.toString() + "\nHOURS"

                val layoutParams =
                    binding.rlBarChartTue.getLayoutParams() as LinearLayout.LayoutParams
                layoutParams.height = dpToPx(height.toFloat())
                binding.rlBarChartTue.setLayoutParams(layoutParams)

                //binding.rlBarChartTue.getLayoutParams().height =4*height.toInt()
                //Log.d("data","height = "+2*height.toInt())
                binding.rlBarChartTue.setBackgroundResource(R.drawable.bar_chart_bg)
                binding.textViewTueSleepDay.setTextColor(resources.getColor(R.color.white))
                binding.textViewTueSleepHours.setTextColor(resources.getColor(R.color.white))
                binding.rlBarChartTue.requestLayout()


            }
        }

        if (sleepBarChartResponseObj.sleepBarChartList!![3].barChartDay.equals("WED", true)) {
            if (sleepBarChartResponseObj.sleepBarChartList!![3].barChartSleepHours.equals(
                    "0",
                    true
                )
            ) {
                binding.rlBarChartWed.setBackgroundResource(R.drawable.bar_chart_bg_white)
                binding.textViewWedSleepDay.setTextColor(resources.getColor(R.color.color_dark_yellow))
                binding.textViewWedSleepHours.setTextColor(resources.getColor(R.color.color_dark_yellow))
            } else {

                val sleepHours =
                    sleepBarChartResponseObj.sleepBarChartList!![3].barChartSleepHours!!.toDouble();
                val height = 220 * sleepHours / 10
                Log.d("data", "height =-- $height")
                binding.textViewWedSleepHours.text = sleepHours.toString() + "\nHOURS"

                val layoutParams =
                    binding.rlBarChartTue.getLayoutParams() as LinearLayout.LayoutParams
                layoutParams.height = dpToPx(height.toFloat())
                binding.rlBarChartWed.setLayoutParams(layoutParams)
                //binding.rlBarChartWed.getLayoutParams().height = 4*height.toInt()
                //Log.d("data","height =-- "+height.toInt()*2)
                binding.rlBarChartWed.setBackgroundResource(R.drawable.bar_chart_bg)
                binding.textViewWedSleepDay.setTextColor(resources.getColor(R.color.white))
                binding.textViewWedSleepHours.setTextColor(resources.getColor(R.color.white))
                binding.rlBarChartWed.requestLayout()

            }
        }

        if (sleepBarChartResponseObj.sleepBarChartList!![4].barChartDay.equals("THU", false)) {
            if (sleepBarChartResponseObj.sleepBarChartList!![4].barChartSleepHours.equals(
                    "0",
                    false
                )
            ) {
                binding.rlBarChartThur.setBackgroundResource(R.drawable.bar_chart_bg_white)
                binding.textViewThrSleepDay.setTextColor(resources.getColor(R.color.color_dark_yellow))
                binding.textViewThrSleepHours.setTextColor(resources.getColor(R.color.color_dark_yellow))
            } else {
                val sleepHours =
                    sleepBarChartResponseObj.sleepBarChartList!![4].barChartSleepHours!!.toDouble();
                val height = 220 * sleepHours / 10
                binding.textViewThrSleepHours.text = sleepHours.toString() + "\nHOURS"
                val layoutParams = binding.rlBarChartTue.layoutParams as LinearLayout.LayoutParams
                layoutParams.height = dpToPx(height.toFloat())
                binding.rlBarChartThur.layoutParams = layoutParams

                //binding.rlBarChartThur.getLayoutParams().height = 4*height.toInt()
                binding.rlBarChartThur.setBackgroundResource(R.drawable.bar_chart_bg)
                binding.textViewThrSleepDay.setTextColor(resources.getColor(R.color.white))
                binding.textViewThrSleepHours.setTextColor(resources.getColor(R.color.white))
                binding.rlBarChartThur.requestLayout()

            }
        }

        if (sleepBarChartResponseObj.sleepBarChartList!![5].barChartDay.equals("FRI", false)) {
            if (sleepBarChartResponseObj.sleepBarChartList!![5].barChartSleepHours.equals(
                    "0",
                    false
                )
            ) {
                binding.rlBarChartFri.setBackgroundResource(R.drawable.bar_chart_bg_white)
                binding.textViewFriSleepDay.setTextColor(resources.getColor(R.color.color_dark_yellow))
                binding.textViewFriSleepHours.setTextColor(resources.getColor(R.color.color_dark_yellow))
            } else {
                val sleepHours =
                    sleepBarChartResponseObj.sleepBarChartList!![5].barChartSleepHours!!.toDouble();
                val height = 220 * sleepHours / 10
                binding.textViewFriSleepHours.text = sleepHours.toString() + "\nHOURS"
                val layoutParams =
                    binding.rlBarChartTue.getLayoutParams() as LinearLayout.LayoutParams
                layoutParams.height = dpToPx(height.toFloat()).toInt()
                binding.rlBarChartFri.setLayoutParams(layoutParams)
                //binding.rlBarChartFri.getLayoutParams().height = 4*height.toInt()
                binding.rlBarChartFri.setBackgroundResource(R.drawable.bar_chart_bg)
                binding.textViewFriSleepDay.setTextColor(resources.getColor(R.color.white))
                binding.textViewFriSleepHours.setTextColor(resources.getColor(R.color.white))
                binding.rlBarChartFri.requestLayout()
            }
        }

        if (sleepBarChartResponseObj.sleepBarChartList!![6].barChartDay.equals("SAT", false)) {
            if (sleepBarChartResponseObj.sleepBarChartList!![6].barChartSleepHours.equals(
                    "0",
                    false
                )
            ) {
                binding.rlBarChartSat.setBackgroundResource(R.drawable.bar_chart_bg_white)
                binding.textViewSatSleepDay.setTextColor(resources.getColor(R.color.color_dark_yellow))
                binding.textViewSatSleepHours.setTextColor(resources.getColor(R.color.color_dark_yellow))
            } else {

                val sleepHours =
                    sleepBarChartResponseObj.sleepBarChartList!![6].barChartSleepHours!!.toDouble();
                val height = 220 * sleepHours / 10
                binding.textViewSatSleepHours.text = sleepHours.toString() + "\nHOURS"
                val layoutParams =
                    binding.rlBarChartTue.getLayoutParams() as LinearLayout.LayoutParams
                layoutParams.height = dpToPx(height.toFloat()).toInt()
                binding.rlBarChartSat.setLayoutParams(layoutParams)
                //binding.rlBarChartSat.getLayoutParams().height =4*height.toInt()
                binding.rlBarChartSat.setBackgroundResource(R.drawable.bar_chart_bg)
                binding.textViewSatSleepDay.setTextColor(
                    ContextCompat.getColor(
                        this,
                        R.color.white
                    )
                )
                binding.textViewSatSleepHours.setTextColor(
                    ContextCompat.getColor(
                        this,
                        R.color.white
                    )
                )
                binding.rlBarChartSat.requestLayout()
            }
        }
    }

    private fun getIntentData() {
        val intent = getIntent()
        val title: String? = intent.getStringExtra("title")
//        binding.textViewTitle.text=title
        val pos = intent.getStringExtra("position");
        val position = Integer.parseInt(pos)

        Log.d("data", "result $title $position")
/*
        if (position == 0) {
            val image: Int = intent.getIntExtra("image", R.drawable.stress_m)
//            binding.modeImage.setImageResource(image)
        } else if (position == 1) {
            val image: Int = intent.getIntExtra("image", R.drawable.sleep_m)
//            binding.modeImage.setImageResource(image)
        } else if (position == 2) {
            val image: Int = intent.getIntExtra("image", R.drawable.anti_ageing_m)
//            binding.modeImage.setImageResource(image)
        } else if (position == 3) {
            val image: Int = intent.getIntExtra("image", R.drawable.injury_m)
//            binding.modeImage.setImageResource(image)
        } else if (position == 4) {
            val image: Int = intent.getIntExtra("image", R.drawable.heart_health_m)
//            binding.modeImage.setImageResource(image)
        } else if (position == 5) {
            val image: Int = intent.getIntExtra("image", R.drawable.mental_health_m)
//            binding.modeImage.setImageResource(image)
        } else if (position == 6) {
            val image: Int = intent.getIntExtra("image", R.drawable.addiction_m)
//            binding.modeImage.setImageResource(image)
        } else if (position == 7) {
            val image: Int = intent.getIntExtra("image", R.drawable.skin_ageing_m)
//            binding.modeImage.setImageResource(image)
        } else if (position == 8) {
            val image: Int = intent.getIntExtra("image", R.drawable.gut_health_m)
//            binding.modeImage.setImageResource(image)
        } else if (position == 9) {
            val image: Int = intent.getIntExtra("image", R.drawable.skin_health_m)
//            binding.modeImage.setImageResource(image)
        } else if (position == 10) {
            val image: Int = intent.getIntExtra("image", R.drawable.eye_health_m)
//            binding.modeImage.setImageResource(image)
        } else if (position == 11) {
            val image: Int = intent.getIntExtra("image", R.drawable.muscle_health_m)
//            binding.modeImage.setImageResource(image)
        }
*/

    }

    private fun getModeList() {
        // binding.progressBar.visibility = View.VISIBLE
        val manager = NetworkManager()
        val userId = Base64.decode(Utility.getUserID(this), Base64.DEFAULT)
        val decodeUserId = String(userId, charset("UTF-8"))
        if (manager.isConnectingToInternet(applicationContext)) {
            manager.createApiRequest(
                ApiUtilis.getAPIInstance(applicationContext).getSleepAlgoCalculatesScore(
                    decodeUserId
                ),
                object : ServiceListener<SleepCalculatorModelResponse> {
                    override fun getServerResponse(
                        response: SleepCalculatorModelResponse,
                        requestcode: Int
                    ) {
                        binding.progressBar.visibility = View.GONE
                        setSpinnerData(response.getData()!!)
                        //setData(response.getData()!!.get(0))


                    }

                    override fun getError(error: ErrorModel, requestcode: Int) {
                        binding.progressBar.visibility = View.GONE
                        Utility.displayShortSnackBar(
                            binding.parentLayout,
                            error.getMessage().toString()
                        )
                    }
                })
        } else {
            binding.progressBar.visibility = View.GONE
            Utility.displayShortSnackBar(
                binding.parentLayout,
                resources.getString(R.string.check_internet)
            )
        }
    }

    private fun setData(sleepScoreObj: SleepScore, position: Int) {
        val degree = 10 * sleepScoreObj.getDegree()!!.toInt() / 18
        var foodDetailAdapter1: FoodDetailAdapter1?
        val foodDetailAdapter2: FoodDetailAdapter2?
        if (sleepScoreObj.getGenesOfInterest() != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                binding.txtGenesOfInterest.text =
                    ("Genes of interest: " + Html.fromHtml(
                        sleepScoreObj.getGenesOfInterest(),
                        Html.FROM_HTML_MODE_COMPACT
                    ))
            } else {
                binding.txtGenesOfInterest.text =
                    ("Genes of interest: " + Html.fromHtml(sleepScoreObj.getGenesOfInterest()))
            }
        } else {
            binding.txtGenesOfInterest.visibility = View.GONE
        }
        if (sleepScoreObj.getAspectIntro() != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                binding.txtAspectIntro.text = Html.fromHtml(
                    sleepScoreObj.getAspectIntro(),
                    Html.FROM_HTML_MODE_COMPACT
                )
            } else {
                binding.txtAspectIntro.text = Html.fromHtml(sleepScoreObj.getAspectIntro())
            }
        } else {
            binding.txtAspectIntro.visibility = View.GONE
        }
        if (sleepScoreObj.getResult() != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                binding.txtResult.text =
                    Html.fromHtml(sleepScoreObj.getResult(), Html.FROM_HTML_MODE_COMPACT)
            } else {
                binding.txtResult.text = Html.fromHtml(sleepScoreObj.getDescription())
            }
        } else {
            binding.txtResult.visibility = View.GONE
        }
        if (sleepScoreObj.getRecommendation() != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                binding.txtRecommendation.text =
                    Html.fromHtml(sleepScoreObj.getRecommendation(), Html.FROM_HTML_MODE_COMPACT)
            } else {
                binding.txtRecommendation.text = Html.fromHtml(sleepScoreObj.getRecommendation())
            }
        } else {
            binding.txtRecommendation.visibility = View.GONE
        }

        binding.speedView.speedTo(degree.toFloat(), 0)
        /*val handler = Handler()
        handler.postDelayed({
            // for autoLogin user
            binding.scrollView.fullScroll(View.FOCUS_UP)
        }, 3000)*/

        try {


            if (sleepScoreObj.foodDetail!!.isNotEmpty()) {
                binding.llFoodDetail.visibility = View.VISIBLE
                // list of food details
                if (sleepScoreObj != null && !sleepScoreObj.foodDetail!!.isEmpty() && sleepScoreObj.foodDetail != null) {
                    // loop

                    binding.rvFood1.layoutManager =
                        LinearLayoutManager(this, RecyclerView.HORIZONTAL, false)
                    binding.textViewFood1Title.text = sleepScoreObj.foodDetail!![0].foodDetailTitle
                    val foodList1: ArrayList<SleepScore.Foods>? =
                        sleepScoreObj.foodDetail!![0].foodsList as ArrayList<SleepScore.Foods>?
                    Log.d("Data---", "foodList1 size= " + foodList1!!.size)
                    foodDetailAdapter1 = FoodDetailAdapter1(foodList1, this@ResultChooseMode)
                    binding.rvFood1.adapter = foodDetailAdapter1


                    binding.rvFood2.layoutManager =
                        LinearLayoutManager(this, RecyclerView.HORIZONTAL, false)
                    binding.textViewFood2Title.text = sleepScoreObj.foodDetail!![1].foodDetailTitle
                    val foodList2: ArrayList<SleepScore.Foods>? =
                        sleepScoreObj.foodDetail!![1].foodsList as ArrayList<SleepScore.Foods>?
                    Log.d("Data---", "foodList2 size= " + foodList2!!.size)
                    foodDetailAdapter1 = FoodDetailAdapter1(foodList2, this@ResultChooseMode)
                    binding.rvFood2.adapter = foodDetailAdapter1


                    binding.rvFood3.layoutManager =
                        LinearLayoutManager(this, RecyclerView.HORIZONTAL, false)
                    binding.textViewFood3Title.text = sleepScoreObj.foodDetail!![2].foodDetailTitle
                    val foodList3 =
                        sleepScoreObj.foodDetail!![2].foodsList as ArrayList<SleepScore.Foods>?
                    Log.d("Data---", "foodList3 size= " + foodList3!!.size)
                    foodDetailAdapter1 = FoodDetailAdapter1(foodList3, this@ResultChooseMode)
                    binding.rvFood3.adapter = foodDetailAdapter1

                    binding.rvFood4.layoutManager =
                        LinearLayoutManager(this, RecyclerView.HORIZONTAL, false)
                    binding.textViewFood4Title.text = sleepScoreObj.foodDetail!![3].foodDetailTitle
                    val foodList4 =
                        sleepScoreObj.foodDetail!![3].foodsList as ArrayList<SleepScore.Foods>?
                    Log.d("Data---", "foodList3 size= " + foodList4!!.size)
                    foodDetailAdapter1 = FoodDetailAdapter1(foodList4, this@ResultChooseMode)
                    binding.rvFood4.adapter = foodDetailAdapter1


                    binding.rvFood5.layoutManager =
                        LinearLayoutManager(this, RecyclerView.HORIZONTAL, false)
                    binding.textViewFood5Title.text = sleepScoreObj.foodDetail!![4].foodDetailTitle
                    val foodList5 =
                        sleepScoreObj.foodDetail!![4].foodsList as ArrayList<SleepScore.Foods>?
                    foodDetailAdapter1 = FoodDetailAdapter1(foodList5, this@ResultChooseMode)
                    Log.d("Data---", "foodList5 size= " + foodList5!!.size)
                    binding.rvFood5.adapter = foodDetailAdapter1

                    // receipe --------------------- loop


                    // }
                }
            } else {
                binding.llFoodDetail.visibility = View.GONE

            }

            if (!sleepScoreObj.recipeDetail!!.isEmpty()) {

                binding.textViewRecomm.visibility = View.VISIBLE
                binding.llRecipeDetail.visibility = View.VISIBLE

                binding.rvFood6.layoutManager =
                    LinearLayoutManager(this, RecyclerView.HORIZONTAL, false)
                val receipeList: ArrayList<SleepScore.RecipeDetail>? =
                    sleepScoreObj.recipeDetail as ArrayList<SleepScore.RecipeDetail>?
                Log.d("data", "recipe" + receipeList!!.size);
                foodDetailAdapter2 = FoodDetailAdapter2(receipeList, this@ResultChooseMode)
                binding.rvFood6.adapter = foodDetailAdapter2
            } else {
                binding.textViewRecomm.visibility = View.GONE
                binding.llRecipeDetail.visibility = View.GONE
            }
        } catch (e: Exception) {

        }
    }

    private fun setSpinnerData(responseDataArr: List<SleepScore>) {
        var selectedPosition = 0
        for (i in 0 until responseDataArr.size) {
            modeList.add(responseDataArr[i].getSleepTitle().toString())
            if (sleepType != "" && responseDataArr[i].getSleepType() == sleepType) {
                selectedPosition = i
                Utility.categoryStatus = false
            }
        }
        if (modeList.size > 0 && applicationContext != null) {
            val spinnerAdapter = CustomSpinnerAdapter(applicationContext!!, modeList)
            binding.spinner.adapter = spinnerAdapter
            binding.spinner.setSelection(selectedPosition)
            binding.spinner.onItemSelectedListener =
                object : AdapterView.OnItemSelectedListener {
                    override fun onItemSelected(
                        parent: AdapterView<*>,
                        view: View,
                        position: Int,
                        id: Long
                    ) {
                        setData(responseDataArr[position], position)
                        val spinnerVal = modeList[position]
                        updateSpeedViewValue(spinnerVal)
                    }

                    override fun onNothingSelected(parent: AdapterView<*>) {
                    }
                }
        }
    }


    private fun updateSpeedViewValue(spinnerVal: String) {
        if (spinnerVal.equals("Night Owl")) {
            binding.textView1Speed.visibility = View.VISIBLE
            binding.textView2Speed.visibility = View.GONE
            binding.textView3Speed.visibility = View.GONE
            binding.textView4Speed.visibility = View.VISIBLE
            binding.textView5Speed.visibility = View.GONE
            binding.textView6Speed.visibility = View.GONE
            binding.textView7Speed.visibility = View.VISIBLE
            binding.textView1Speed.text = "Not\nLikely"
            binding.textView4Speed.text = "Likely"
            binding.textView7Speed.text = "Highly\nLikely"
        } else if (spinnerVal.equals("Sleep Duration")) {
            binding.textView1Speed.visibility = View.VISIBLE
            binding.textView2Speed.visibility = View.GONE
            binding.textView3Speed.visibility = View.GONE
            binding.textView4Speed.visibility = View.VISIBLE
            binding.textView5Speed.visibility = View.GONE
            binding.textView6Speed.visibility = View.GONE
            binding.textView7Speed.visibility = View.VISIBLE
            binding.textView1Speed.text = "Shorter\nSleep"
            binding.textView4Speed.text = "Normal\nSleep"
            binding.textView7Speed.text = "Longer\nSleep"
        } else if (spinnerVal.equals("Fragmented Sleep")) {
            binding.textView1Speed.visibility = View.VISIBLE
            binding.textView2Speed.visibility = View.GONE
            binding.textView3Speed.visibility = View.GONE
            binding.textView4Speed.visibility = View.VISIBLE
            binding.textView5Speed.visibility = View.GONE
            binding.textView6Speed.visibility = View.GONE
            binding.textView7Speed.visibility = View.VISIBLE
            binding.textView1Speed.text = "Not\nLikely"
            binding.textView4Speed.text = "Likely"
            binding.textView7Speed.text = "Highly\nLikely"
        } else if (spinnerVal.equals("Narcolepsy")) {
            binding.textView1Speed.visibility = View.VISIBLE
            binding.textView2Speed.visibility = View.GONE
            binding.textView3Speed.visibility = View.GONE
            binding.textView4Speed.visibility = View.VISIBLE
            binding.textView5Speed.visibility = View.GONE
            binding.textView6Speed.visibility = View.GONE
            binding.textView7Speed.visibility = View.VISIBLE
            binding.textView1Speed.text = "Not\nLikely"
            binding.textView4Speed.text = "Likely"
            binding.textView7Speed.text = "Highly\nLikely"
        } else if (spinnerVal.equals("Stress & Sleep")) {
            binding.textView1Speed.visibility = View.VISIBLE
            binding.textView2Speed.visibility = View.GONE
            binding.textView3Speed.visibility = View.GONE
            binding.textView4Speed.visibility = View.VISIBLE
            binding.textView5Speed.visibility = View.GONE
            binding.textView6Speed.visibility = View.GONE
            binding.textView7Speed.visibility = View.VISIBLE
            binding.textView1Speed.text = "Not\nLikely"
            binding.textView4Speed.text = "Likely"
            binding.textView7Speed.text = "Highly\nLikely"

        } else if (spinnerVal.equals("Caffeine & Sleep")) {
            binding.textView1Speed.visibility = View.VISIBLE
            binding.textView2Speed.visibility = View.GONE
            binding.textView3Speed.visibility = View.GONE
            binding.textView4Speed.visibility = View.VISIBLE
            binding.textView5Speed.visibility = View.GONE
            binding.textView6Speed.visibility = View.GONE
            binding.textView7Speed.visibility = View.VISIBLE
            binding.textView1Speed.text = "Not\nLikely"
            binding.textView4Speed.text = "Likely"
            binding.textView7Speed.text = "Highly\nLikely"
        }

    }
}