package com.muhdo.app.ui.epigentic.food.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.muhdo.app.ui.epigentic.food.model.DateGroupModel;


import java.util.List;

public class FoodListResponse {

    @SerializedName("code")
    @Expose
    private String statusCode;

    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("data")
    @Expose
    private List<DateGroupModel> dateGroupModelList;

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DateGroupModel> getDateGroupModelList() {
        return dateGroupModelList;
    }

    public void setDateGroupModelList(List<DateGroupModel> dateGroupModelList) {
        this.dateGroupModelList = dateGroupModelList;
    }

}
