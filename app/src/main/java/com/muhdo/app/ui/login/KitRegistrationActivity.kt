package com.muhdo.app.ui.login

import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.facebook.FacebookSdk
import com.facebook.appevents.AppEventsLogger
import com.muhdo.app.R
import com.muhdo.app.databinding.ActivityKitRegistrationBinding
import com.muhdo.app.ui.DashboardActivity
import com.muhdo.app.ui.signUp.KitViewModel
import com.muhdo.app.utils.v3.Constants

class KitRegistrationActivity : Activity() {
    lateinit var binding: ActivityKitRegistrationBinding
    private lateinit var kitViewModel: KitViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        FacebookSdk.sdkInitialize(applicationContext)
        AppEventsLogger.activateApp(this)
        binding = DataBindingUtil.setContentView(
            this@KitRegistrationActivity,
            R.layout.activity_kit_registration
        )
        kitViewModel = KitViewModel()
        kitViewModel.onCheckChanged(binding)
        binding.viewModel = kitViewModel
        binding.btnBack.setOnClickListener {
            onBackPressed()
        }
//        binding.btnReset.setOnClickListener {
////            getKitRegistrationStatus()
//        }

        if (intent.getStringExtra(Constants.KIT_ID_VALUE) != "") {
            binding.edtKitId.setText(intent.getStringExtra(Constants.KIT_ID_VALUE).toUpperCase())
            binding.edtKitId.isClickable = false
            binding.edtKitId.isEnabled = false
            binding.edtKitId.isFocusable = false
            binding.edtKitId.isCursorVisible = false
//            binding.edtKitId.isInEditMode = false
        }
    }


    override fun onBackPressed() {
        /* val i = Intent(applicationContext, LoginActivity::class.java)
         i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
         startActivity(i)
         finish()*/
        val i = Intent(this, ScannerActivity::class.java)
        finish()
        startActivity(i)
    }

    private fun goBackToRegistration() {
        onBackPressed()
    }
    /* private fun getKitRegistrationStatus() {

         binding!!.progressBar.visibility = View.VISIBLE


         val manager = NetworkManager2()
         if (manager.isConnectingToInternet(applicationContext)) {

             val apiService = ApiClientNew.getClient()!!.create(ApiInterface::class.java)
             val call = apiService.getKitStatus(Utility.getUserID(applicationContext))
             call.enqueue(object : Callback<LoginModule> {
                 override fun onResponse(call: Call<LoginModule>, response: Response<LoginModule>) {
                     binding.progressBar.visibility = View.GONE
                     if (response.body() == null) {
                         Utility.displayShortSnackBar(
                             binding.parentLayout,
                             "Bad request. Expected Username/client id combination not found."
                         )
                     } else {
                         if (response.body() != null && response.body()!!.getStatusCode() == 200) {
                             val json = JSONObject(response.body()!!.getData().toString())
                             if (json.getBoolean("found")) {
                                 displayAlert()

                             } else {

                             }

                         } else if (response.body() != null) {
                             Utility.displayShortSnackBar(binding.parentLayout, response.body()!!.getData().toString())
                         }
                     }
                 }

                 override fun onFailure(call: Call<LoginModule>, t: Throwable) {
                     binding.progressBar.visibility = View.GONE
                     Log.e("Login", t.toString())
                     Utility.displayShortSnackBar(
                         binding.parentLayout,
                         "Server connection error!!, please try again later."
                     )
                 }
             })
         } else {
             binding.progressBar.visibility = View.GONE

             Utility.displayShortSnackBar(
                 binding.parentLayout,
                 resources.getString(R.string.check_internet)
             )
         }
     }*/


    private fun displayAlert() {
        val dialogBuilder = AlertDialog.Builder(this@KitRegistrationActivity)
        dialogBuilder.setMessage("Your account has been activated, please wait for 5 minutes to get user data")
            .setCancelable(false)
            .setPositiveButton("Ok") { _, _ ->
                val i = Intent(this@KitRegistrationActivity, DashboardActivity::class.java)
                startActivity(i)
            }


        val alert = dialogBuilder.create()
        alert.show()
    }

}