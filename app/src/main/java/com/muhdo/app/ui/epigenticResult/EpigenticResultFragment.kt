package com.muhdo.app.fragment

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.google.gson.Gson
import com.muhdo.app.R
import com.muhdo.app.adapter.GenoTypeAdapter
import com.muhdo.app.databinding.EpigenticResultFragmentBinding
import com.muhdo.app.model.RecyclerData
import com.muhdo.app.repository.ApiUtilis
import com.muhdo.app.repository.ErrorModel
import com.muhdo.app.repository.NetworkManager
import com.muhdo.app.repository.ServiceListener
import com.muhdo.app.ui.epigenticResult.EpigenticHealthTrackingDetailActivity
import com.muhdo.app.ui.epigenticResult.model.EpigResultResponse
import com.muhdo.app.utils.PreferenceConnector


class EpigenticResultFragment : Fragment() {


    internal lateinit var binding: EpigenticResultFragmentBinding
    var adapter: GenoTypeAdapter? = null
    private lateinit var mSpeedList: ArrayList<RecyclerData>

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.epigentic_result_fragment, container, false)
        binding = EpigenticResultFragmentBinding.bind(view)
        getEpigenticResultData()
        return view
    }

    private fun getEpigenticResultData() {
        val kitId =
            PreferenceConnector.readString(activity!!, PreferenceConnector.KIT_ID, "0").toString()
        //val kitId="DE1520SR"
        binding.progressBar.visibility = View.VISIBLE
        val manager = NetworkManager()
        if (manager.isConnectingToInternet(requireContext())) {
            manager.createApiRequest(
                ApiUtilis.getAPIInstance(activity!!).getEpigenticResultData(kitId),
                object : ServiceListener<EpigResultResponse> {
                    override fun getServerResponse(
                        epigResultObj: EpigResultResponse,
                        requestcode: Int
                    ) {
                        binding.progressBar.visibility = View.GONE
                        try {
                            if (epigResultObj.code == 200) {
                                if (epigResultObj != null) {
                                    setResultData(epigResultObj)
                                } else {
                                }
                            } else {
                                showDialogBoxWhenAccountSuspended(
                                    context,
                                    epigResultObj.message.toString()
                                )
                            }
                        } catch (e: Exception) {

                        }
                    }

                    override fun getError(error: ErrorModel, requestcode: Int) {
                        binding.progressBar.visibility = View.GONE

//                        Utility.displayShortSnackBar(
//                            binding.parentLayout,
//                            error.getMessage()!!
//                        )
                        /*Toast.makeText(activity!!.applicationContext,"server error", Toast.LENGTH_LONG)
                        Toast.makeText(activity!!.applicationContext,"server error", Toast.LENGTH_LONG)
                            .show()*/
                    }
                })
        } else {
            binding.progressBar.visibility = View.GONE
            Toast.makeText(requireContext(), "Please check internet connection!", Toast.LENGTH_LONG)
                .show()
        }
    }


    private fun showDialogBoxWhenAccountSuspended(context: Context?, msg: String) {
        val builder = AlertDialog.Builder(context, R.style.AlertDialog)
        builder.setTitle("Muhdo")
        builder.setMessage(msg)
        //builder.setPositiveButton("OK", DialogInterface.OnClickListener(function = x))
        builder.setPositiveButton("Ok")
        { dialog, which ->
            dialog.dismiss()
        }
        builder.show()
    }

    private fun setResultData(epigResultObj: EpigResultResponse) {

        val biologicalAge = epigResultObj.data!!.biologicalAge!!.score.toString()
        val biologicalAgeColor = epigResultObj.data!!.biologicalAge!!.color.toString()

        if (!biologicalAge.isEmpty()) {
            binding.txtBiologicalAgeValue.text = biologicalAge
        }
        if (!biologicalAgeColor.isEmpty()) {
            if (biologicalAgeColor.equals("Yellow")) {
                binding.llDial.setBackgroundResource(R.mipmap.epigenetic_dial_yellow)
            } else if (biologicalAgeColor.equals("Red")) {
                binding.llDial.setBackgroundResource(R.mipmap.epigenetic_dial_red)
            } else if (biologicalAgeColor.equals("Green")) {
                binding.llDial.setBackgroundResource(R.mipmap.epigenetic_dial_green)
            }
        }

        try {
            val sliderAge =
                epigResultObj.data!!.epigenticHealthData!!.get(0).data!!.scoreData!!.color
            val sliderEyeHealth =
                epigResultObj.data!!.epigenticHealthData!!.get(1).data!!.scoreData!!.color
            val sliderHearing =
                epigResultObj.data!!.epigenticHealthData!!.get(2).data!!.scoreData!!.color
            val sliderMemory =
                epigResultObj.data!!.epigenticHealthData!!.get(3).data!!.scoreData!!.color
            val sliderInflammation =
                epigResultObj.data!!.epigenticHealthData!!.get(4).data!!.scoreData!!.color

            val objAge = epigResultObj.data!!.epigenticHealthData!!.get(0);
            val objEyeHealth = epigResultObj.data!!.epigenticHealthData!!.get(1);
            val objHearing = epigResultObj.data!!.epigenticHealthData!!.get(2);
            val objMemory = epigResultObj.data!!.epigenticHealthData!!.get(3);
            val objInflammation = epigResultObj.data!!.epigenticHealthData!!.get(4);

            //Log.d("data","obj eye--- "+ objEyeHealth.data!!.recommendationInfoList!!.get(3).text)


            val gsonAge = Gson()
            val objectAge = gsonAge.toJson(objAge)

            val gsonEye = Gson()
            val objectEye = gsonEye.toJson(objEyeHealth)

            val gsonHearing = Gson()
            val objectHearing = gsonHearing.toJson(objHearing)

            val gsonMemory = Gson()
            val objectMemory = gsonMemory.toJson(objMemory)

            val gsonInflammation = Gson()
            val objectInflammation = gsonInflammation.toJson(objInflammation)


            if (!sliderAge!!.isEmpty()) {

            }
            if (!sliderEyeHealth!!.isEmpty()) {
                if (sliderEyeHealth.equals("Yellow")) {
                    binding.imageViewSliderEye.setBackgroundResource(R.mipmap.slider_yellow)
                } else if (sliderEyeHealth.equals("Red")) {
                    binding.imageViewSliderEye.setBackgroundResource(R.mipmap.slider_red)
                } else if (sliderEyeHealth.equals("Green")) {
                    binding.imageViewSliderEye.setBackgroundResource(R.mipmap.slider_green)
                }
            }


            if (!sliderHearing!!.isEmpty()) {
                if (sliderHearing.equals("Yellow")) {
                    binding.imageViewSliderHearing.setBackgroundResource(R.mipmap.slider_yellow)
                } else if (sliderHearing.equals("Red")) {
                    binding.imageViewSliderHearing.setBackgroundResource(R.mipmap.slider_red)
                } else if (sliderHearing.equals("Green")) {
                    binding.imageViewSliderHearing.setBackgroundResource(R.mipmap.slider_green)
                }
            }

            if (!sliderMemory!!.isEmpty()) {
                if (sliderMemory.equals("Yellow")) {
                    binding.imageViewSliderMemory.setBackgroundResource(R.mipmap.slider_yellow)
                } else if (sliderMemory.equals("Red")) {
                    binding.imageViewSliderMemory.setBackgroundResource(R.mipmap.slider_red)
                } else if (sliderMemory.equals("Green")) {
                    binding.imageViewSliderMemory.setBackgroundResource(R.mipmap.slider_green)
                }
            }

            if (!sliderInflammation!!.isEmpty()) {
                if (sliderInflammation.equals("Yellow")) {
                    binding.imageViewSliderInflammation.setBackgroundResource(R.mipmap.slider_yellow)
                } else if (sliderInflammation.equals("Red")) {
                    binding.imageViewSliderInflammation.setBackgroundResource(R.mipmap.slider_red)
                } else if (sliderInflammation.equals("Green")) {
                    binding.imageViewSliderInflammation.setBackgroundResource(R.mipmap.slider_green)
                }
            }



            binding.cardViewEyeHealth.setOnClickListener(object : View.OnClickListener {
                override fun onClick(v: View?) {
                    val bundle = Bundle()
                    val i =
                        Intent(requireContext(), EpigenticHealthTrackingDetailActivity::class.java)
                    i.putExtra("track_key", "track_eye_data")
                    i.putExtra("object", objectEye)
                    i.putExtra("count", 1)
                    i.putExtra("objectAge", objectAge)
                    i.putExtra("objectEye", objectEye)
                    i.putExtra("objectHearing", objectHearing)
                    i.putExtra("objectMemory", objectMemory)
                    i.putExtra("objectInfl", objectInflammation)
                    startActivity(i)

                }
            })

            binding.cardViewHearing.setOnClickListener(object : View.OnClickListener {
                override fun onClick(v: View?) {
                    val i =
                        Intent(requireContext(), EpigenticHealthTrackingDetailActivity::class.java)
                    i.putExtra("track_key", "track_hearing_data")
                    i.putExtra("object", objectHearing)
                    i.putExtra("count", 2)
                    i.putExtra("objectAge", objectAge)
                    i.putExtra("objectEye", objectEye)
                    i.putExtra("objectHearing", objectHearing)
                    i.putExtra("objectMemory", objectMemory)
                    i.putExtra("objectInfl", objectInflammation)
                    startActivity(i)

                }
            })

            binding.cardViewMemory.setOnClickListener(object : View.OnClickListener {
                override fun onClick(v: View?) {
                    val i =
                        Intent(requireContext(), EpigenticHealthTrackingDetailActivity::class.java)
                    i.putExtra("track_key", "track_memory_data")
                    i.putExtra("object", objectMemory)
                    i.putExtra("count", 3)
                    i.putExtra("objectAge", objectAge)
                    i.putExtra("objectEye", objectEye)
                    i.putExtra("objectHearing", objectHearing)
                    i.putExtra("objectMemory", objectMemory)
                    i.putExtra("objectInfl", objectInflammation)
                    startActivity(i)

                }
            })
            binding.cardViewInflamation.setOnClickListener(object : View.OnClickListener {
                override fun onClick(v: View?) {
                    val i =
                        Intent(requireContext(), EpigenticHealthTrackingDetailActivity::class.java)
                    i.putExtra("track_key", "track_inflammation_data")
                    i.putExtra("object", objectInflammation)
                    i.putExtra("objectAge", objectAge)
                    i.putExtra("objectEye", objectEye)
                    i.putExtra("objectHearing", objectHearing)
                    i.putExtra("objectMemory", objectMemory)
                    i.putExtra("objectInfl", objectInflammation)
                    i.putExtra("count", 4)
                    startActivity(i)

                }
            })

            binding.llDial.setOnClickListener(object : View.OnClickListener {
                override fun onClick(v: View?) {
                    val i =
                        Intent(requireContext(), EpigenticHealthTrackingDetailActivity::class.java)
                    i.putExtra("track_key", "track_age_data")
                    i.putExtra("object", objectAge)
                    i.putExtra("objectAge", objectAge)
                    i.putExtra("objectEye", objectEye)
                    i.putExtra("objectHearing", objectHearing)
                    i.putExtra("objectMemory", objectMemory)
                    i.putExtra("objectInfl", objectInflammation)

                    i.putExtra("count", 0)
                    startActivity(i)

                }
            })


        } catch (e: Exception) {

        }


    }
}