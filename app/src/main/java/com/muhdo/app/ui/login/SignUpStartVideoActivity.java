package com.muhdo.app.ui.login;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import com.ct7ct7ct7.androidvimeoplayer.listeners.VimeoPlayerStateListener;
import com.ct7ct7ct7.androidvimeoplayer.view.VimeoPlayerView;
import com.muhdo.app.R;

public class SignUpStartVideoActivity extends AppCompatActivity {

    private VimeoPlayerView vimeoPlayerView;
    private int videoId;
    private float startAt;
    private float endAt;
    private float aspectRatio;
    private String orientation;
    private Button btnContinue;
    private ImageView customButtonPlay;
    private LinearLayout llBack;
    private Context context=this;
    public static final String REQUEST_ORIENTATION_AUTO = "REQUEST_ORIENTATION_AUTO";
    public static final String REQUEST_ORIENTATION_PORTRAIT = "REQUEST_ORIENTATION_PORTRAIT";
    public static final String REQUEST_ORIENTATION_LANDSCAPE = "REQUEST_ORIENTATION_LANDSCAPE";
    public static final String RESULT_STATE_VIDEO_ID = "RESULT_STATE_VIDEO_ID";
    public static final String RESULT_STATE_VIDEO_PLAY_AT = "RESULT_STATE_VIDEO_PLAY_AT";
    public static final String RESULT_STATE_PLAYER_STATE = "RESULT_STATE_PLAYER_STATE";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up_start_video);
        vimeoPlayerView = (VimeoPlayerView)findViewById(R.id.vimeoPlayerView);
        btnContinue = (Button)findViewById(R.id.btn_Continue);
        customButtonPlay = (ImageView)findViewById(R.id.imageView_PlayButton);
        llBack = (LinearLayout)findViewById(R.id.llBack);

        String link1 = "https://vimeo.com/341308382";
        vimeoPlayerView.initialize(341308382,link1);
        videoId=vimeoPlayerView.getVideoId();
        vimeoPlayerView.addStateListener(new VimeoPlayerStateListener() {
          @Override
          public void onPlaying(float duration) {
              Log.d("data","play video");
              vimeoPlayerView.setVisibility(View.VISIBLE);
              btnContinue.setVisibility(View.GONE);
          }

          @Override
          public void onPaused(float seconds) {
              btnContinue.setVisibility(View.VISIBLE);

          }

          @Override
          public void onEnded(float duration) {
              btnContinue.setVisibility(View.VISIBLE);
          }
      });


        customButtonPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vimeoPlayerView.setVisibility(View.VISIBLE);
                customButtonPlay.setVisibility(View.GONE);
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
            }
        });

    }

    @Override
    public void onBackPressed() {

        if (REQUEST_ORIENTATION_LANDSCAPE.equals(orientation)) {
            // setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            orientation = REQUEST_ORIENTATION_PORTRAIT;
        }else if (REQUEST_ORIENTATION_PORTRAIT.equals(orientation)) {
            Intent intent = new Intent();
            intent.putExtra(RESULT_STATE_VIDEO_ID, videoId);
            intent.putExtra(RESULT_STATE_VIDEO_PLAY_AT, vimeoPlayerView.getCurrentTimeSeconds());
            intent.putExtra(RESULT_STATE_PLAYER_STATE, vimeoPlayerView.getPlayerState().name());
            setResult(Activity.RESULT_OK, intent);
            finish();
        }

        // original code
        /*
        Intent intent = new Intent();
        intent.putExtra(RESULT_STATE_VIDEO_ID, videoId);
        intent.putExtra(RESULT_STATE_VIDEO_PLAY_AT, vimeoPlayerView.getCurrentTimeSeconds());
        intent.putExtra(RESULT_STATE_PLAYER_STATE, vimeoPlayerView.getPlayerState().name());
        setResult(Activity.RESULT_OK, intent);
        finish();
        */


    }

}
