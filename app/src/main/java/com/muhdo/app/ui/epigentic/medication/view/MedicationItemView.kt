package com.muhdo.app.ui.epigentic.medication.view

import android.content.Context
import android.util.Log
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import com.mindorks.placeholderview.annotations.Layout
import com.mindorks.placeholderview.annotations.Resolve
import com.mindorks.placeholderview.annotations.View
import com.mindorks.placeholderview.annotations.expand.ChildPosition
import com.mindorks.placeholderview.annotations.expand.ParentPosition
import com.muhdo.app.R
import com.muhdo.app.ui.epigentic.medication.MedicationActivity
import com.muhdo.app.ui.epigentic.medication.model.MedicationInfo

@Layout(R.layout.row_medication_heading_item)
class MedicationItemView(
    private val mContext: Context,
    private val mInfo: MedicationInfo,
    mContext1: MedicationActivity?
) {

    @ParentPosition
    private val mParentPosition: Int = 0

    @ChildPosition
    private val mChildPosition: Int = 0

    @View(R.id.textview_medication_type)
    private val medicationTypeVal: TextView? = null

    @View(R.id.textview_medication_unit)
    private val medicationUnit: TextView? = null

    @View(R.id.textview_unit_value)
    private val medicationUnitVal: TextView? = null

    @View(R.id.view_delete)
    private val viewDelete: LinearLayout? = null

    @Resolve
    private fun onResolved() {
        medicationTypeVal!!.setText(mInfo.drugsName)
        medicationUnit!!.setText(mInfo.dosage)
        medicationUnitVal!!.setText(mInfo.quantity)

        viewDelete?.setOnClickListener(object: android.view.View.OnClickListener {
            override fun onClick(v: android.view.View?) {
                Log.d("data","click a")
                Toast.makeText(mContext,"delete"+ mInfo.id,Toast.LENGTH_SHORT).show();


            }
        })






    }


}
