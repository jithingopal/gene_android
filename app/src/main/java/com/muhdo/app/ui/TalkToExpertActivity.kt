package com.muhdo.app.ui

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.muhdo.app.R
import com.muhdo.app.databinding.ActivityTalkToExpertBinding
import com.muhdo.app.utils.v3.TempUtil


class TalkToExpertActivity : AppCompatActivity() {
    lateinit var binding: ActivityTalkToExpertBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(
            this@TalkToExpertActivity,
            R.layout.activity_talk_to_expert
        )

        if (intent.extras != null) {
            if (intent.extras.getBoolean(TempUtil.EXTRA_FEED_BACK, false)) {
                binding.tvTitle.setText(R.string.v3_provide_feed_back)
            }
        }
        binding.btnBack.setOnClickListener {
            val intent = Intent(this, DashboardActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
            finish()
        }

        binding.btnSend.setOnClickListener {
            if (binding.edtMsg.text.isEmpty()) {
                binding.edtMsg.error = "Please enter message!!"
                binding.edtMsg.requestFocus()
            } else {
                val intent = Intent(Intent.ACTION_SEND)
                val recipients = arrayOf("info@muhdo.com")
                intent.putExtra(Intent.EXTRA_EMAIL, recipients)
                intent.putExtra(Intent.EXTRA_SUBJECT, "Talk to our experts")
                intent.putExtra(Intent.EXTRA_TEXT, binding.edtMsg.text)
                intent.type = "text/html"
                startActivity(Intent.createChooser(intent, "Send mail"))
            }
        }
    }
}