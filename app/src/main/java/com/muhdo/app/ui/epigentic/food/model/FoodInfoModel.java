package com.muhdo.app.ui.epigentic.food.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class FoodInfoModel implements Serializable, Comparable<FoodInfoModel>  {

    @SerializedName("_id")
    @Expose
    private String id;

    @SerializedName("user_id")
    @Expose
    private String userId;

    @SerializedName("created_at")
    @Expose
    private String createdDate;

    @SerializedName("updated_at")
    @Expose
    private String updateDate;

    @SerializedName("__v")
    @Expose
    private String v;

    @SerializedName("date_group")
    @Expose
    private String dateGroup;

    @SerializedName("nutrition_id")
    @Expose
    private String nutritionId;

    @SerializedName("user_value")
    @Expose
    private String userValue;

    @SerializedName("meal_time")
    @Expose
    private String mealTime;

    @SerializedName("food_group")
    @Expose
    private String foodGroup;

    @SerializedName("food_group_name")
    @Expose
    private String foodGroupName;

    @SerializedName("food_name")
    @Expose
    private String foodName;

    @SerializedName("value_type")
    @Expose
    private String valueType;

    @SerializedName("measurement")
    @Expose
    private String measurement;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNutritionId() {
        return nutritionId;
    }

    public void setNutritionId(String nutritionId) {
        this.nutritionId = nutritionId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserValue() {
        return userValue;
    }

    public void setUserValue(String userValue) {
        this.userValue = userValue;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }

    public String getV() {
        return v;
    }

    public void setV(String v) {
        this.v = v;
    }

    public String getDateGroup() {
        return dateGroup;
    }

    public void setDateGroup(String dateGroup) {
        this.dateGroup = dateGroup;
    }

    public String getMealTime() {
        return mealTime;
    }

    public void setMealTime(String mealTime) {
        this.mealTime = mealTime;
    }

    public String getFoodGroup() {
        return foodGroup;
    }

    public void setFoodGroup(String foodGroup) {
        this.foodGroup = foodGroup;
    }

    public String getFoodGroupName() {
        return foodGroupName;
    }

    public void setFoodGroupName(String foodGroupName) {
        this.foodGroupName = foodGroupName;
    }

    public String getFoodName() {
        return foodName;
    }

    public void setFoodName(String foodName) {
        this.foodName = foodName;
    }

    public String getValueType() {
        return valueType;
    }

    public void setValueType(String valueType) {
        this.valueType = valueType;
    }

    public String getMeasurement() {
        return measurement;
    }

    public void setMeasurement(String measurement) {
        this.measurement = measurement;
    }

    @Override
    public int compareTo(FoodInfoModel o) {
        return this.getMealTime().compareTo(o.mealTime);
    }
}
