package com.muhdo.app.ui.v3;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.LimitLine;
import com.github.mikephil.charting.components.MarkerView;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.formatter.IFillFormatter;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.dataprovider.LineDataProvider;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.Utils;
import com.muhdo.app.R;
import com.muhdo.app.custom_view.v3.MyMarkerView;
import com.muhdo.app.utils.v3.TempUtil;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class SampleChartActivity extends AppCompatActivity implements
        OnChartValueSelectedListener {

    private LineChart chart;
    protected Typeface tfRegular;
    protected Typeface tfLight;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.v3_line_chart);
        tfRegular = Typeface.createFromAsset(getAssets(), "roboto_medium.ttf");
        tfLight = Typeface.createFromAsset(getAssets(), "roboto_medium.ttf");

        setTitle("LineChartActivity1");


        {   // // Chart Style // //
            chart = findViewById(R.id.chart1);

            // background color
            //chart.setBackgroundColor(getResources().getColor(R.color.bg_chart));
            // background drawable
            Drawable drawable = ContextCompat.getDrawable(this, R.drawable.bg_line_chart_epigen);

            chart.setBackground(drawable);

            // disable description text
            chart.getDescription().setEnabled(false);
            chart.setAutoScaleMinMaxEnabled(true);
            // enable touch gestures
            chart.setTouchEnabled(false);

            // set listeners
            chart.setOnChartValueSelectedListener(this);
            chart.setDrawGridBackground(false);

            // create marker to display box when values are selected
            MyMarkerView mv = new MyMarkerView(this, R.layout.v3_chart_custom_marker_view);

            // Set the marker to the chart
            mv.setChartView(chart);
            chart.setMarker(mv);

            // enable scaling and dragging
            chart.setDragEnabled(true);
            chart.setScaleEnabled(false);
            // chart.setScaleXEnabled(true);
            // chart.setScaleYEnabled(true);

            // force pinch zoom along both axis
            chart.setPinchZoom(false);
        }
        ArrayList<String> xValues = new ArrayList<String>(4);
        xValues.add("0");
        xValues.add("1");
        xValues.add("2");
        xValues.add("3");
        xValues.add("4");
        xValues.add("5");
        String[] xAxisLabel = {"Jan", "Feb", "Mar", "Apri", "May", "Jun"};
        XAxis xAxis;
        {   // // X-Axis Style // //
            xAxis = chart.getXAxis();
            xAxis.setEnabled(true);
            xAxis.setAxisLineColor(Color.WHITE);
            xAxis.setGridColor(Color.TRANSPARENT);
            xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
            xAxis.setAxisLineWidth(1f);
            xAxis.setTextColor(Color.WHITE);
            xAxis.setAvoidFirstLastClipping(true);
            xAxis.setLabelCount(5);
            // vertical grid lines
//            xAxis.enableGridDashedLine(10f, 10f, 0f);
//            xAxis.setValueFormatter(new IndexAxisValueFormatter(xAxisLabel));
            xAxis.setValueFormatter(new ValueFormatter() {
                @Override
                public String getFormattedValue(float value) {
                    // return the string va
                    return (int) value + "";
                }
            });
        }

        YAxis yAxis;
        {   // // Y-Axis Style // //
            yAxis = chart.getAxisLeft();

            // disable dual axis (only use LEFT axis)
            chart.getAxisRight().setEnabled(false);

            // horizontal grid lines
            yAxis.enableGridDashedLine(10f, 10f, 0f);
            yAxis.setGridColor(Color.TRANSPARENT);
            yAxis.setAxisLineColor(Color.WHITE);
            yAxis.setAxisLineWidth(1f);
            yAxis.setTextColor(Color.WHITE);
            yAxis.setDrawTopYLabelEntry(true);
            // axis range
            yAxis.setAxisMaximum(10f);
            yAxis.setAxisMinimum(-10f);
            yAxis.setLabelCount(20);
            yAxis.setTextSize(16);
            yAxis.setValueFormatter(new ValueFormatter() {

                @Override
                public String getFormattedValue(float value) {
                    // return the string va
                    switch ((int) value) {
                        case 0:
                            return "Age";
                        case 1:
                            return "";
                        case 2:
                            return "";
                        case 3:
                            return "";
                        case 4:
                            return "";
                        case 5:
                            return "+";
                        case 6:
                            return "";
                        case 7:
                            return "";
                        case 8:
                            return "";
                        case 9:
                            return "";
                        case 10:
                            return "";
                        case -1:
                            return "";
                        case -2:
                            return "";
                        case -3:
                            return "";
                        case -4:
                            return "";
                        case -5:
                            return "-";
                        case -6:
                            return "";
                        case -7:
                            return "";
                        case -8:
                            return "";
                        case -9:
                            return "";
                        case -10:
                            return "";
                    }
                    return "";
                }
            });
        }


        {   // // Create Limit Lines // //


            LimitLine ll1 = new LimitLine(0f, " ");
            ll1.setLineWidth(1f);
            ll1.setLineColor(Color.WHITE);
            ll1.enableDashedLine(10f, 10f, 0f);
            ll1.setLabelPosition(LimitLine.LimitLabelPosition.RIGHT_TOP);
            ll1.setTextSize(10f);
            ll1.setTypeface(tfRegular);
            LimitLine ll3 = new LimitLine(5f, " ");
            ll3.setLineWidth(1f);
            ll3.setLineColor(Color.WHITE);
            ll3.enableDashedLine(10f, 10f, 0f);
            ll3.setLabelPosition(LimitLine.LimitLabelPosition.RIGHT_TOP);
            ll3.setTextSize(10f);
            ll3.setTypeface(tfRegular);

            LimitLine ll2 = new LimitLine(-5f, "");
            ll2.setLineWidth(1f);
            ll2.setLineColor(Color.WHITE);
            ll2.enableDashedLine(10f, 10f, 0f);
            ll2.setLabelPosition(LimitLine.LimitLabelPosition.RIGHT_BOTTOM);
            ll2.setTextSize(10f);
            ll2.setTypeface(tfRegular);

            // draw limit lines behind data instead of on top
            yAxis.setDrawLimitLinesBehindData(true);
            xAxis.setDrawLimitLinesBehindData(true);

            // add limit lines
            yAxis.addLimitLine(ll1);
            yAxis.addLimitLine(ll2);
            yAxis.addLimitLine(ll3);
        }

        setData(3, 180);
        // draw points over time
//        chart.animateX(1000);
        chart.animateY(1250);

        // get the legend (only possible after setting data)
        Legend l = chart.getLegend();

        // draw legend entries as lines
        l.setForm(Legend.LegendForm.LINE);

    }

    private void setData(int count, float range) {

        ArrayList<Entry> values = new ArrayList<>();
        values.add(new Entry(0, 0, getResources().getDrawable(R.drawable.ic_blue_dot)));
        values.add(new Entry(1, 7, getResources().getDrawable(R.drawable.ic_blue_dot)));
        values.add(new Entry(2, 3.5f, getResources().getDrawable(R.drawable.ic_blue_dot)));
        values.add(new Entry(3, -2, getResources().getDrawable(R.drawable.ic_blue_dot)));
        values.add(new Entry(4, -10, getResources().getDrawable(R.drawable.ic_blue_dot)));
        values.add(new Entry(5, 10, getResources().getDrawable(R.drawable.ic_blue_dot)));
        ArrayList<Entry> values2 = new ArrayList<>();
        values2.add(new Entry(0, 0, getResources().getDrawable(R.drawable.ic_blue_dot)));
        values2.add(new Entry(1, -7, getResources().getDrawable(R.drawable.ic_blue_dot)));

        LineDataSet set1;
        LineDataSet set2;


        if (chart.getData() != null &&
                chart.getData().getDataSetCount() > 0) {
            set1 = (LineDataSet) chart.getData().getDataSetByIndex(0);
            set1.setValues(values);
            set2 = (LineDataSet) chart.getData().getDataSetByIndex(1);
            set2.setValues(values2);
            set2.notifyDataSetChanged();
            set1.notifyDataSetChanged();
            chart.getData().notifyDataChanged();
            chart.notifyDataSetChanged();
        } else {
            // create a dataset and give it a type
            set1 = new LineDataSet(values, "");

            set1.setDrawIcons(false);
            set2 = new LineDataSet(values2, "");

            set2.setDrawIcons(false);

            // draw dashed line
//            set1.enableDashedLine(10f, 5f, 0f);

            // black lines and points
            set1.setColor(Color.WHITE);
            set1.setCircleColor(Color.WHITE);
            set2.setColor(Color.GREEN);
            set2.setCircleColor(Color.GREEN);

            // line thickness and point size
            set1.setLineWidth(1f);
            set1.setCircleRadius(3f);

            set2.setLineWidth(1f);
            set2.setCircleRadius(3f);

            // draw points as solid circles
            set1.setDrawCircleHole(false);

            // customize legend entry
            set1.setFormLineWidth(1f);
            set1.setFormLineDashEffect(new DashPathEffect(new float[]{10f, 5f}, 0f));
            set1.setFormSize(15.f);

            // text size of values
            set1.setValueTextSize(9f);
            set1.setDrawValues(false);

            // draw selection line as dashed
            set1.enableDashedHighlightLine(10f, 5f, 0f);

            // set the filled area
            set1.setDrawFilled(false);
            set1.setFillFormatter(new IFillFormatter() {
                @Override
                public float getFillLinePosition(ILineDataSet dataSet, LineDataProvider dataProvider) {
                    return chart.getAxisLeft().getAxisMinimum();
                }
            });
//            set1.setFillColor(Color.TRANSPARENT);

            // set color of filled area
            if (Utils.getSDKInt() >= 18) {
                // drawables only supported on api level 18 and above
                Drawable drawable = ContextCompat.getDrawable(this, R.drawable.fade_red);
                set1.setFillDrawable(drawable);
            } else {
                set1.setFillColor(Color.BLACK);
            }

            ArrayList<ILineDataSet> dataSets = new ArrayList<>();
            dataSets.add(set1); // add the data sets
            dataSets.add(set2); // add the data sets

            // create a data object with the data sets
            LineData data = new LineData(dataSets);

            // set data
            chart.setData(data);
        }
    }


    @Override
    public void onValueSelected(Entry e, Highlight h) {
        TempUtil.log("Entry selected", e.toString());
        TempUtil.log("LOW HIGH", "low: " + chart.getLowestVisibleX() + ", high: " + chart.getHighestVisibleX());
        TempUtil.log("MIN MAX", "xMin: " + chart.getXChartMin() + ", xMax: " + chart.getXChartMax() + ", yMin: " + chart.getYChartMin() + ", yMax: " + chart.getYChartMax());
    }

    @Override
    public void onNothingSelected() {
        TempUtil.log("Nothing selected", "Nothing selected.");
    }


}
