package com.muhdo.app.ui.epigentic.execrise.view

import android.content.Context
import android.util.Log
import android.widget.*
import com.mindorks.placeholderview.annotations.Layout
import com.mindorks.placeholderview.annotations.Resolve
import com.mindorks.placeholderview.annotations.View
import com.mindorks.placeholderview.annotations.expand.ChildPosition
import com.mindorks.placeholderview.annotations.expand.ParentPosition
import com.muhdo.app.R
import com.muhdo.app.ui.epigentic.medication.model.Info
import android.widget.Spinner
import android.annotation.SuppressLint
import android.widget.ArrayAdapter


@Layout(R.layout.row_excrise_heading_item)
class ExcrisetemView(private val mContext: Context, private val mInfo: Info) {

    @ParentPosition
    private val mParentPosition: Int = 0

    @ChildPosition
    private val mChildPosition: Int = 0

    @View(R.id.textview_excrise_type)
    private val titleTxt: TextView? = null

    @View(R.id.layout_delete_exercise_heading_item)
    private val layoutDelete: LinearLayout? = null

    @View(R.id.spinner_exercise_intensity)
    private val spinnerExecirseIntensity: Spinner? = null

    @View(R.id.spinner_exercise_duration)
    private val spinnerExecirseDuration: Spinner? = null


    var data = arrayOf("LOW", "MEDIUM", "HIGH", "VERY HIGH")
    var durationData = arrayOf("0-30", "30-60", "60-90", "90-120","120+")



    @Resolve
    private fun onResolved() {
        titleTxt!!.text = mInfo.title


        layoutDelete?.setOnClickListener(object: android.view.View.OnClickListener {
            override fun onClick(v: android.view.View?) {
                Log.d("data","click a")
               // Toast.makeText(mContext,""+mInfo.time, Toast.LENGTH_SHORT).show()
            }
        })


        methodSpinnerExecirseIntensity()
        methodSpinnerExecirseDuration()

    }

    @SuppressLint("ResourceType")
    private fun methodSpinnerExecirseIntensity() {

        val adapter = ArrayAdapter<String>(mContext, R.layout.row_exercise_spinner_intensity, R.id.textview_intensity, data)
        spinnerExecirseIntensity?.setAdapter(adapter)

         /* val adapter = ArrayAdapter<String>(mContext,R.layout.row_exercise_spinner_intensity, data)
         adapter.setDropDownViewResource(R.id.textview_intensity)
        spinnerExecirseIntensity?.adapter = adapter
        spinnerExecirseIntensity?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>?, view: android.view.View?, position: Int, id: Long) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

           override fun onNothingSelected(parent: AdapterView<*>) {


            }
        }*/



    }


    @SuppressLint("ResourceType")
    private fun methodSpinnerExecirseDuration() {

        val adapter = ArrayAdapter<String>(mContext, R.layout.row_exercise_spinner_intensity, R.id.textview_intensity, durationData)
        spinnerExecirseDuration?.setAdapter(adapter)

        /* val adapter = ArrayAdapter<String>(mContext,R.layout.row_exercise_spinner_intensity, data)
        adapter.setDropDownViewResource(R.id.textview_intensity)
       spinnerExecirseIntensity?.adapter = adapter
       spinnerExecirseIntensity?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
           override fun onItemSelected(parent: AdapterView<*>?, view: android.view.View?, position: Int, id: Long) {
               TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
           }

          override fun onNothingSelected(parent: AdapterView<*>) {


           }
       }*/



    }


}


