package com.muhdo.app.ui.v3

import android.app.Dialog
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.muhdo.app.R
import com.muhdo.app.apiModel.v3.firebase.ForceUpdateEvent
import com.muhdo.app.utils.PreferenceConnector
import com.muhdo.app.utils.v3.TempUtil
import com.timingsystemkotlin.backuptimingsystem.Utility
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode


open class BaseActivityV3 : AppCompatActivity() {

  var dialog: Dialog?=null;
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setupDialogInfo()
    }
    override fun onResume() {
        super.onResume()
        showForceUpdateDialog()
    }
    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onEvent(event: ForceUpdateEvent) {
        TempUtil.log("AppApplication","Triggering Event on Dashboard")
        showForceUpdateDialog()
    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        super.onStop()

        EventBus.getDefault().unregister(this)
    }
    fun showForceUpdateDialog() {

        var isForceUpdateEnabled: Boolean? = PreferenceConnector.readBoolean(
            applicationContext,
            PreferenceConnector.FORCE_UPDATE_STATUS,
            false
        )
        if (isForceUpdateEnabled!!) {
            dialog?.show()
        } else {
            if (dialog != null && dialog!!.isShowing) {
                dialog?.dismiss()
            }
        }
    }

    private fun setupDialogInfo() {
        dialog = Dialog(this)
        val viewDialog = View.inflate(this, R.layout.v3_dialog_force_update, null)
        dialog?.setContentView(viewDialog)
        val closeButton = viewDialog.findViewById<ImageView>(R.id.img_close)
        val tvTitle = viewDialog.findViewById<TextView>(R.id.tvTitle)
        val tvMessage = viewDialog.findViewById<TextView>(R.id.tvMessage)
        val tvPlaystoreLink = viewDialog.findViewById<TextView>(R.id.tvPlayStoreLink)
        tvTitle.setText(R.string.dialog_force_update_title)
        tvMessage.setText(R.string.dialog_force_update_message)
        tvPlaystoreLink.setText(R.string.btn_update_app)

        var playstoreLink: String? = PreferenceConnector.readString(
            applicationContext,
            PreferenceConnector.PLAY_STORE_URL,
            "https://play.google.com/store/apps/details?id=com.muhdo.app"
        )

        tvPlaystoreLink.setOnClickListener{

            val url = playstoreLink
            val i = Intent(Intent.ACTION_VIEW)
            i.data = Uri.parse(url)
            startActivity(i)
            var forceLogoutStatus: Boolean? = PreferenceConnector.readBoolean(
                applicationContext,
                PreferenceConnector.FORCE_LOGOUT_STATUS,
                false
            )
            if(forceLogoutStatus!!){
                TempUtil.log("forceLogoutStatus","From Dialog logging out")
                Utility.setMealStatus(this, false)
                Utility.setWorkoutStatus(this, false)
                Utility.saveUserStatus(applicationContext, false)
                Utility.setKitID(applicationContext,"")
                Utility.saveLoginStatus(applicationContext, false)
                finish()
            }
        }
        closeButton.setOnClickListener {
            //close button is invisible for now
        }
        dialog?.setCancelable(false)
    }


}