package com.muhdo.app.ui.epigentic.medication.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.muhdo.app.ui.epigentic.medication.model.DataGroupModel;

import java.util.List;

public class MedicationListRosponse {

    @SerializedName("code")
    @Expose
    private String statusCode;

    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("data")
    @Expose
    private List<DataGroupModel> dataGroupModelList;

    public List<DataGroupModel> getDataGroupModelList() {
        return dataGroupModelList;
    }

    public void setDataGroupModelList(List<DataGroupModel> dataGroupModelList) {
        this.dataGroupModelList = dataGroupModelList;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
