package com.muhdo.app.ui.signUp

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.util.Log
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModel
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.muhdo.app.R
import com.muhdo.app.apiModel.login.socialLogin.Auth
import com.muhdo.app.apiModel.login.socialLogin.SocialLoginRequest
import com.muhdo.app.apiModel.login.socialLogin.UserData
import com.muhdo.app.apiModel.signupSignIn.LoginModule
import com.muhdo.app.apiModel.signupSignIn.SignUpModule
import com.muhdo.app.apiModel.signupSignIn.SignUpRequestModule
import com.muhdo.app.databinding.ActivityLoginBinding
import com.muhdo.app.databinding.ActivitySignUpBinding
import com.muhdo.app.repository.ApiUtilis
import com.muhdo.app.repository.NetworkManager
import com.muhdo.app.repository.NetworkManager2
import com.muhdo.app.ui.login.LoginActivity
import com.muhdo.app.ui.login.ScannerActivity
import com.muhdo.app.ui.login.VerifyOtpActivity
import com.muhdo.app.utils.PreferenceConnector
import com.muhdo.app.utils.v3.TempUtil
import com.timingsystemkotlin.backuptimingsystem.Utility
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*


class SignUpViewModel : ViewModel() {

    fun onLoginClick(view: View) {
        if (checkLoginValidation(view)) {
            Log.e("Error", "Error in Login")
            hideKeyboard(view)
            loginUser(view)
        }
    }

    fun OnRegistrationClick(view: View) {
        if (checkValidation(view)) {
            hideKeyboard(view)
            registrationUser(view)
            Log.e("Error", "Error in Registraion")
        }
    }


    fun socialLogin(
        data: GoogleSignInAccount,
        context: Context,
        binding: ActivityLoginBinding,
        token: String
    ) {
        Utility.displayShortSnackBar(
            binding.parentLayout as View,
            "Getting response from server, please wait!!"
        )
        val auth = Auth(
            context.resources.getString(R.string.google_client_id),
            token
        )
        val userData = UserData(
            data.givenName, data.familyName, data.displayName.toString(),
            data.photoUrl.toString(), data.photoUrl.toString(), data.id, data.email, ""
        )
        val socialLoginRequest = SocialLoginRequest(auth, userData, "google")

        val manager = NetworkManager()
        if (manager.isConnectingToInternet(context)) {
            val call = ApiUtilis.getAPIInstance(context).socialLogin(socialLoginRequest)
            call.enqueue(object : Callback<LoginModule> {
                override fun onResponse(call: Call<LoginModule>, response: Response<LoginModule>) {

                    if (response.body() != null && response.body()!!.getStatusCode() == 200) {
                        //Utility.saveLoginStatus(context, true)
                        goToKitRegistration2(response.body()!!.getData().toString(), context)
                    } else if (response.body() != null) {
                        Utility.displayShortSnackBar(
                            binding.parentLayout,
                            response.body()!!.getData().toString()
                        )
                    }
                }

                override fun onFailure(call: Call<LoginModule>, t: Throwable) {
                    Utility.displayShortSnackBar(
                        binding.parentLayout,
                        context.resources.getString(R.string.server_error)
                    )
                    // Log error here since request failed
                    Log.e("Login", t.toString())
                }
            })
        } else {

            Utility.displayShortSnackBar(
                binding.parentLayout,
                context.resources.getString(R.string.check_internet)
            )

        }
    }


    fun fbLogin(
        data: JSONObject,
        context: Context,
        binding: ActivityLoginBinding,
        token: String
    ) {

        if (data.has("first_name") && data.has("id") && data.has("email") && data.has("last_name") && data.has(
                "name"
            )
        ) {
            val auth = Auth(
                context.resources.getString(R.string.facebook_app_id),
                token
            )

            val photo = "https://graph.facebook.com/" + data.getString("id") + "/picture?type=large"
            val userData = UserData(
                data.getString("first_name"), data.getString("last_name"), data.getString("name"),
                photo,
                photo, data.getString("id"), data.getString("email"), ""
            )


            val socialLoginRequest = SocialLoginRequest(auth, userData, "facebook")
            binding.progressBar.visibility = VISIBLE

            val manager = NetworkManager()
            if (manager.isConnectingToInternet(context)) {
                val call = ApiUtilis.getAPIInstance(context).socialLogin(socialLoginRequest)
                call.enqueue(object : Callback<LoginModule> {
                    override fun onResponse(
                        call: Call<LoginModule>,
                        response: Response<LoginModule>
                    ) {
                        binding.progressBar.visibility = GONE
                        if (response.body() != null && response.body()!!.getStatusCode() == 200) {
                            //Utility.saveLoginStatus(context, true)
                            goToKitRegistration2(response.body()!!.getData().toString(), context)
                        } else if (response.body() != null) {
                            Utility.displayShortSnackBar(
                                binding.parentLayout,
                                response.body()!!.getData().toString()
                            )
                        }
                    }

                    override fun onFailure(call: Call<LoginModule>, t: Throwable) {
                        binding.progressBar.visibility = GONE
                        Utility.displayShortSnackBar(
                            binding.parentLayout,
                            context.resources.getString(R.string.server_error)
                        )
                        // Log error here since request failed
                        Log.e("Login", t.toString())
                    }
                })
            } else {
                binding.progressBar.visibility = GONE

                Utility.displayShortSnackBar(
                    binding.parentLayout,
                    context.resources.getString(R.string.check_internet)
                )

            }

        } else {
            Utility.displayShortSnackBar(
                binding.parentLayout,
                "Unable to fetch user data, please try again later"
            )
        }
    }


    fun socialLogin(
        data: GoogleSignInAccount,
        context: Context,
        binding: ActivitySignUpBinding,
        token: String
    ) {

        Utility.displayShortSnackBar(
            binding.parentLayout,
            "Getting response from server, please wait!!"
        )
//        binding.progressBar.visibility = VISIBLE
        val auth = Auth(
            context.resources.getString(R.string.google_client_id),
            token
        )
        val userData = UserData(
            data.givenName, data.familyName, data.displayName.toString(),
            data.photoUrl.toString(), data.photoUrl.toString(), data.id, data.email, ""
        )
        val socialLoginRequest = SocialLoginRequest(auth, userData, "google")


        val manager = NetworkManager()
        if (manager.isConnectingToInternet(context)) {
            ApiUtilis.getAPIInstance(context).socialLogin(socialLoginRequest)
                .enqueue(object : Callback<LoginModule> {
                    override fun onResponse(
                        call: Call<LoginModule>,
                        response: Response<LoginModule>
                    ) {
//                    binding.progressBar.visibility = GONE
                        if (response.body() != null && response.body()!!.getStatusCode() == 200) {
                            // Utility.saveLoginStatus(context, true)
                            goToKitRegistration2(response.body()!!.getData().toString(), context)
                        } else if (response.body() != null) {
                            Utility.displayShortSnackBar(
                                binding.parentLayout,
                                response.body()!!.getData().toString()
                            )
                        }
                    }

                    override fun onFailure(call: Call<LoginModule>, t: Throwable) {
//                    binding.progressBar.visibility = GONE
                        Utility.displayShortSnackBar(
                            binding.parentLayout,
                            context.resources.getString(R.string.server_error)
                        )
                        // Log error here since request failed
                        Log.e("Login", t.toString())
                    }
                })
        } else {
//            binding!!.progressBar.visibility = GONE

            Utility.displayShortSnackBar(
                binding.parentLayout,
                context.resources.getString(R.string.check_internet)
            )

        }
    }


    fun fbLogin(
        data: JSONObject,
        context: Context,
        binding: ActivitySignUpBinding,
        token: String
    ) {
        if (data.has("first_name") && data.has("id") && data.has("email") && data.has("last_name") && data.has(
                "name"
            )
        ) {

            val auth = Auth(
                context.resources.getString(R.string.facebook_app_id),
                token
            )
            val photo = "https://graph.facebook.com/" + data.getString("id") + "/picture?type=large"
            val userData = UserData(
                data.getString("first_name"), data.getString("last_name"), data.getString("name"),
                photo,
                photo, data.getString("id"), data.getString("email"), ""
            )


            val socialLoginRequest = SocialLoginRequest(auth, userData, "facebook")
            binding.progressBar.visibility = VISIBLE

            val manager = NetworkManager()
            if (manager.isConnectingToInternet(context)) {
                ApiUtilis.getAPIInstance(context).socialLogin(socialLoginRequest)
                    .enqueue(object : Callback<LoginModule> {
                        override fun onResponse(
                            call: Call<LoginModule>,
                            response: Response<LoginModule>
                        ) {
                            binding.progressBar.visibility = GONE
                            if (response.body() != null && response.body()!!.getStatusCode() == 200) {
                                //Utility.saveLoginStatus(context, true)
                                goToKitRegistration2(
                                    response.body()!!.getData().toString(),
                                    context
                                )
                            } else if (response.body() != null) {
                                Utility.displayShortSnackBar(
                                    binding.parentLayout,
                                    response.body()!!.getData().toString()
                                )
                            }
                        }

                        override fun onFailure(call: Call<LoginModule>, t: Throwable) {
                            binding.progressBar.visibility = GONE
                            Utility.displayShortSnackBar(
                                binding.parentLayout,
                                context.resources.getString(R.string.server_error)
                            )
                            // Log error here since request failed
                            Log.e("Login", t.toString())
                        }
                    })
            } else {
                binding.progressBar.visibility = GONE
                Utility.displayShortSnackBar(
                    binding.parentLayout,
                    context.resources.getString(R.string.check_internet)
                )
            }
        } else {
            Utility.displayShortSnackBar(
                binding.parentLayout,
                "Unable to fetch user data, please try again later"
            )
        }
    }

    @SuppressLint("NewApi")
    private fun registrationUser(view: View) {
        val binding = DataBindingUtil.findBinding<ActivitySignUpBinding>(view)
        binding!!.progressBar.visibility = VISIBLE

//        var user = ""
        val parts =
            binding.edtEmail.text.toString().split("\\@".toRegex()).dropLastWhile { it.isEmpty() }
                .toTypedArray() // String array, each element is text between dots
        val userName = parts[0]
//        if (userName.length < 6) {
//            user = userName + "12"
//        } else {
//            user = userName
//        }
        val street = binding.edtStreet.text.toString() + binding.edtUkStreet.text.toString()

        val height = binding.edtHeight.text.toString()
        val weight = binding.edtWeight.text.toString()
        var heightUnit = "cm"
        var weightUnit = "lbs"
        var gender = "male"
        var newsletter = "0"
        val dob =
            binding.edtYear.text.toString() + "-" + binding.edtMonth.text.toString() + "-" + binding.edtDay.text.toString()
        val countryRegion = binding.edtCountry.text.toString()
        if (binding.btnFeet.isChecked) {
            heightUnit = "ft"
        }

        if (binding.btnKg.isChecked) {
            weightUnit = "kg"
        }

        if (binding.btnFemale.isChecked) {
            gender = "female"
        }
        if (binding.cbUpdatesAndOffers.isChecked) {
            newsletter = "1"
        }
        val requestParams = SignUpRequestModule(
            binding.edtEmail.text.toString().toLowerCase(),
            userName.toLowerCase(),
            Utility.encodeString(binding.edtPassword.text.toString()),
            binding.edtPhone.text.toString(),
            gender,
            binding.edtFirstName.text.toString(),
            binding.edtLastName.text.toString(),
            street,
            binding.edtCity.text.toString(),
            binding.edtPostcode.text.toString(),
            "5c7e70f3ebc82935c7241eff",
            "true",
            "5c6a602ffabb1930861944dd",
            Utility.countryID,
            binding.txtPrivacyPolicy.isChecked,
            newsletter,
            height, weight, heightUnit, weightUnit, countryRegion, dob
        )
        val manager = NetworkManager2()
        if (manager.isConnectingToInternet(view.context)) {
            ApiUtilis.getAPIInstance(view.context).signUp(requestParams)
                .enqueue(object : Callback<SignUpModule> {
                    override fun onResponse(
                        call: Call<SignUpModule>,
                        response: Response<SignUpModule>
                    ) {
                        binding.progressBar.visibility = GONE
                        if (response.body() != null && response.body()!!.getStatusCode() == 200) {
                            val i = Intent(view.context, LoginActivity::class.java)
                            i.putExtra("DisplayAlert", "Display")
                            view.context.startActivity(i)
                        } else if (response.body() != null) {
                            try {
                                Utility.displayShortSnackBar(
                                    binding.parentLayout,
                                    JSONObject(response.body()!!.getBody().toString()).getString("message").toString()
                                )
                            } catch (e: Exception) {
                                e.printStackTrace()
                            }
                        }
                    }

                    override fun onFailure(call: Call<SignUpModule>, t: Throwable) {
                        binding.progressBar.visibility = GONE
                        Utility.displayShortSnackBar(
                            binding.parentLayout,
                            "Server connection error!!, please try again later."
                        )
                        // Log error here since request failed

                        Log.e("Login", t.toString())
                    }
                })

        } else {
            binding.progressBar.visibility = GONE

            Utility.displayShortSnackBar(
                binding.parentLayout,
                view.context.resources.getString(R.string.check_internet)
            )

        }
    }

    @SuppressLint("NewApi")
    private fun loginUser(view: View) {
        val binding = DataBindingUtil.findBinding<ActivityLoginBinding>(view)
        binding!!.progressBar.visibility = VISIBLE
        val manager = NetworkManager2()
        if (manager.isConnectingToInternet(view.context)) {
            val params = HashMap<String, String>()
            params["email"] = binding.edtUsername.text.toString().toLowerCase()
            params["password"] = Utility.encodeString(binding!!.edtPassword.text.toString())
            params["login_via"] = "mobile"
            val call = ApiUtilis.getAPIInstance(view.context).login(params)
            call.enqueue(object : Callback<JsonObject> {
                override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>) {
                    binding.progressBar.visibility = GONE
                    val js: JsonObject? = response.body()
                    val jsonResult = JSONObject(js.toString())
                    Log.e("Jithin2", js.toString())
                    try {
                        val gson = Gson()
                        if (jsonResult.getInt("statusCode") == 200) {
                            TempUtil.setLastSignedUserGender(
                                jsonResult.getJSONObject("data").getJSONObject(
                                    "user_data"
                                ).getString("gender")
                            )
                            Log.e("Jithin", "Gender:" + TempUtil.getLastSignedUserGender())
                            val loginModule: LoginModule? =
                                gson.fromJson(js, LoginModule::class.java)
                            if (loginModule != null && loginModule.getStatusCode() == 200) {
                                val userId = loginModule.getData()!!.userId.toString()
                                val suspended = loginModule.getData()!!.user!!.suspended
                                if (suspended == true) {
                                    showDialogBoxWhenAccountSuspended(view)
                                    return

                                } else {
                                    Log.d("data", " " + userId + " " + suspended)
                                    //Utility.saveLoginStatus(view.context, true)
                                    PreferenceConnector.writeString(
                                        view.context,
                                        PreferenceConnector.EMAIL,
                                        binding.edtUsername.text.toString()
                                    )
                                    PreferenceConnector.writeString(
                                        view.context,
                                        PreferenceConnector.PASSWORD,
                                        binding.edtPassword.text.toString()
                                    )
                                    goToKitRegistration(userId, view.context)
                                }


                            }
                        } else {

                            Utility.displayShortSnackBar(
                                binding.parentLayout,
                                JSONObject(jsonResult.getString("body")).getString("message").toString()
                            )

                        }


                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }

                override fun onFailure(call: Call<JsonObject>, t: Throwable) {
                    // Log error here since request failed
                    binding.progressBar.visibility = GONE
                    Log.e("Login", t.toString())

                    Utility.displayShortSnackBar(
                        binding.parentLayout,
                        "Server connection error!!, please try again later."
                    )


                }
            })
        } else {
            binding.progressBar.visibility = GONE

            Utility.displayShortSnackBar(
                binding.parentLayout,
                view.context.resources.getString(R.string.check_internet)
            )

        }

    }

    private fun showDialogBoxWhenAccountSuspended(view: View) {
        val builder = AlertDialog.Builder(view.context, R.style.AlertDialog)
        builder.setTitle("Muhdo")
        builder.setMessage(R.string.susspended_msg)
        //builder.setPositiveButton("OK", DialogInterface.OnClickListener(function = x))
        builder.setPositiveButton("Ok")
        { dialog, which ->
            dialog.dismiss()
        }
        builder.show()
    }


    private fun goToKitRegistration(userId: String, context: Context) {
        Utility.saveUserID(context, userId)
        val i = Intent(context, VerifyOtpActivity::class.java)
        //val i = Intent(context, DashboardActivity::class.java)
        context.startActivity(i)
//        context.applicationContext.finish()


    }

    private fun goToKitRegistration2(data: String, context: Context) {
        val json = JSONObject(data)
        Utility.saveUserID(context, json.getString("user_id"))
        val i = Intent(context, ScannerActivity::class.java)
        context.startActivity(i)


    }

    private fun checkValidation(view: View): Boolean {

        val binding = DataBindingUtil.findBinding<ActivitySignUpBinding>(view)
        var isValid = true
        when {
            binding!!.edtFirstName.text!!.isEmpty() -> {
                Log.e("Error", "Error in Registraion first name")
                binding.edtFirstName.error =
                    view.context.getString(R.string.please_enter_first_name)
                binding.edtFirstName.requestFocus()
                isValid = false
            }
            binding.edtLastName.text.isEmpty() -> {
                binding.edtLastName.error =
                    view.context.getString(R.string.please_enter_last_name)
                binding.edtLastName.requestFocus()
                isValid = false
            }

            binding.edtEmail.text.isEmpty() -> {
                binding.edtEmail.error = view.context.getString(R.string.please_enter_email)
                binding.edtEmail.requestFocus()
                isValid = false
            }
            binding.edtHeight.text.isEmpty() -> {
                binding.edtHeight.error =
                    view.context.getString(R.string.please_enter_your_height)
                binding.edtHeight.requestFocus()
                isValid = false
            }
            binding.edtWeight.text.isEmpty() -> {
                binding.edtWeight.error =
                    view.context.getString(R.string.please_enter_your_weight)
                binding.edtWeight.requestFocus()
                isValid = false
            }
            binding.edtCountry.text.isEmpty() -> {
                binding.edtCountry.error =
                    view.context.getString(R.string.please_enter_country_name)
                binding.edtCountry.requestFocus()
                isValid = false
            }
            binding.edtDay.text.isEmpty() -> {
                binding.edtDay.error = view.context.getString(R.string.please_enter_valid_dob)
                binding.edtDay.requestFocus()
                isValid = false
            }
            binding.edtMonth.text.isEmpty() -> {
                binding.edtMonth.error = view.context.getString(R.string.please_enter_valid_dob)
                binding.edtMonth.requestFocus()
                isValid = false
            }
            binding.edtYear.text.isEmpty() -> {
                binding.edtYear.error = view.context.getString(R.string.please_enter_valid_dob)
                binding.edtYear.requestFocus()
                isValid = false
            }
            !android.util.Patterns.EMAIL_ADDRESS.matcher(binding.edtEmail.text).matches() -> {
                binding.edtEmail.error = view.context.getString(R.string.email_validation)
                binding.edtEmail.requestFocus()
                isValid = false
            }

//            binding!!.edtDob.text.isEmpty() -> {
//                binding!!.edtDob.error = "Please enter correct date of birth"
//                binding!!.edtDob.requestFocus()
//                isValid = false
//            }

            binding.edtPassword.text.isEmpty() || binding.edtPassword.text.length < 8 || !Utility.isPasswordValid(
                binding.edtPassword.text
            ) -> {
                binding.edtPassword.error = view.context.getString(R.string.please_enter_password)
                binding.edtPassword.requestFocus()
                isValid = false
            }
            binding.edtVerifyPassword.text.isEmpty() || binding.edtPassword.text.length < 8 || !Utility.isPasswordValid(
                binding.edtPassword.text
            ) -> {
                binding.edtVerifyPassword.error =
                    view.context.getString(R.string.please_enter_password)
                binding.edtVerifyPassword.requestFocus()
                isValid = false
            }

            !binding.edtPassword.text.toString().equals(binding.edtVerifyPassword.text.toString()) -> {
                binding.edtVerifyPassword.error =
                    view.context.getString(R.string.please_verify_password)
                binding.edtVerifyPassword.requestFocus()
                isValid = false
            }

            binding.spinnerCountry.selectedItem.equals("") || binding.spinnerCountry.selectedItem.equals(
                "Please select country"
            ) -> {
                Toast.makeText(view.context, "Please select country", Toast.LENGTH_LONG).show()
                binding.spinnerCountry.requestFocus()
                isValid = false
            }

            binding.edtPhone.text.isEmpty() || binding.edtPhone.text.length < 10
                    || !binding.edtPhone.text.contains("+") -> {
                binding.edtPhone.error = view.context.getString(R.string.please_enter_phone)
                binding.edtPhone.requestFocus()
                isValid = false
            }
            binding.edtStreet.text.isEmpty() -> {
                binding.edtStreet.error = view.context.getString(R.string.please_enter_street)
                binding.edtStreet.requestFocus()
                isValid = false
            }

            binding.edtCity.text.isEmpty() -> {
                binding.edtCity.error = view.context.getString(R.string.please_enter_city)
                binding.edtCity.requestFocus()
                isValid = false
            }
            binding.edtPostcode.text.isEmpty() -> {
                binding.edtPostcode.error = view.context.getString(R.string.please_enter_postcode)
                binding.edtPostcode.requestFocus()
                isValid = false
            }
            binding.txtPrivacyPolicy.isChecked().not() -> {
                binding.txtPrivacyPolicy.error = view.context.getString(R.string.please_check)
                binding.txtPrivacyPolicy.requestFocus()
                isValid = false
            }
        }
        return isValid
    }

    private fun checkLoginValidation(view: View): Boolean {

        val binding = DataBindingUtil.findBinding<ActivityLoginBinding>(view)
        var isValid = true
        when {

            binding!!.edtUsername.text.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(
                binding.edtUsername.text
            ).matches() -> {
                binding.edtUsername.error = view.context.getString(R.string.please_enter_email)
                binding.edtUsername.requestFocus()
                isValid = false
            }

            binding.edtPassword.text.isEmpty() || binding.edtPassword.text.length < 8 || !Utility.isPasswordValid(
                binding.edtPassword.text
            ) -> {
                binding.edtPassword.error = view.context.getString(R.string.please_enter_password)
                binding.edtPassword.requestFocus()
                isValid = false
            }

        }
        return isValid
    }

    fun hideKeyboard(view1: View) {
        val imm =
            view1.context!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager?
        imm!!.hideSoftInputFromWindow(view1.windowToken, 0)

    }
}