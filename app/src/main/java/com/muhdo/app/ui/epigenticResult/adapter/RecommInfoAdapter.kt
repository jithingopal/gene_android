package com.muhdo.app.ui.epigenticResult.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.muhdo.app.R
import com.muhdo.app.ui.epigenticResult.EpigenticHealthTrackingDetailActivity
import com.muhdo.app.ui.epigenticResult.model.RecommendationsInfo

class RecommInfoAdapter(
    internal var context: Context,
    private var chooseModeFragment: EpigenticHealthTrackingDetailActivity,
    private var recommendationInfoList: List<RecommendationsInfo>?) : RecyclerView.Adapter<RecommInfoAdapter.Holder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        val itemview = LayoutInflater.from(context).inflate(R.layout.list_genotype, parent, false)
        return Holder(itemview)
    }

    override fun getItemCount(): Int {
        return recommendationInfoList!!.size
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {

      /*  if (this.list.isNotEmpty()) {
            Glide
                .with(context)
                .load(list[position].getIcon())
                .centerInside()
                .into(holder.imgResults)

            holder.txtResultTitle.text = list[position].getTitle()


            holder.imgResults.setOnClickListener {
                chooseModeFragment.callKeyData(list[position].getId()!!)
            }

            holder.relativeLayout.setOnClickListener {
                chooseModeFragment.callKeyData(list[position].getId()!!)
            }
        }*/
    }

    inner class Holder(view: View) : RecyclerView.ViewHolder(view) {
        internal var imgResults: ImageView = view.findViewById(R.id.img_results)
        internal var txtResultTitle: TextView = view.findViewById(R.id.txt_result_title)
        internal var relativeLayout: RelativeLayout = view.findViewById(R.id.relative_layout)
    }
}