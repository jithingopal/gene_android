package com.muhdo.app.ui.epigentic.execrise

import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Base64
import android.util.Log
import android.view.View
import android.view.Window
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.databinding.DataBindingUtil
import com.muhdo.app.R
import com.muhdo.app.databinding.ActivityEpigenticExecriseBinding
import com.muhdo.app.repository.ApiUtilis
import com.muhdo.app.repository.ErrorModel
import com.muhdo.app.repository.NetworkManager
import com.muhdo.app.repository.ServiceListener
import com.muhdo.app.ui.epigentic.execrise.response.ExecriseResponseModel
import com.muhdo.app.utils.v3.Constants
import com.timingsystemkotlin.backuptimingsystem.Utility
import java.util.*

class EpigenticExecriseActivity  : AppCompatActivity(){
    lateinit var binding: ActivityEpigenticExecriseBinding
    private var progressDialog: ProgressDialog? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this@EpigenticExecriseActivity, R.layout.activity_epigentic_execrise)
        Dialog(this)
        binding.btnBack.setOnClickListener {
            finish()
        }

        clickExccriseOption()
        clickRestOption()
        clickInguredOption()
        clickButtonContiune()

    }

    private fun clickButtonContiune() {

            binding.btncontinue.setOnClickListener(object: View.OnClickListener {
                override fun onClick(v: View?) {
                    Log.d("data", "click a")
                   finish()
                }
            })

    }

    private fun clickExccriseOption() {
        binding.optionExecrise.setOnClickListener(object: View.OnClickListener {
            override fun onClick(v: View?) {
                Log.d("data","click a")
                binding.view1.background = ActivityCompat.getDrawable(applicationContext, R.drawable.change_border_bg)
                binding.view2.background = ActivityCompat.getDrawable(applicationContext, R.drawable.facebook_button_bg)
                binding.view3.background = ActivityCompat.getDrawable(applicationContext, R.drawable.facebook_button_bg)
                startActivity(Intent(applicationContext, EpigenticExecriseExecriseActivity::class.java))
            }
        })
    }

    private fun clickRestOption() {
        binding.optionRest.setOnClickListener(object: View.OnClickListener {
            override fun onClick(v: View?) {
                binding.view2.background = ActivityCompat.getDrawable(applicationContext, R.drawable.change_border_bg)
                binding.view1.background = ActivityCompat.getDrawable(applicationContext, R.drawable.facebook_button_bg)
                binding.view3.background = ActivityCompat.getDrawable(applicationContext, R.drawable.facebook_button_bg)
                var type="REST"
                sendDataOnServer(type)

                Log.d("data","click a")
               // startActivity(Intent(applicationContext, EpigenticExecriseExecriseActivity::class.java))
            }
        })
    }




    private fun clickInguredOption() {
        binding.optionIngured.setOnClickListener(object: View.OnClickListener {
            override fun onClick(v: View?) {
                var type="INJURED"
                Log.d("data","click a")
                binding.view3.background = ActivityCompat.getDrawable(applicationContext, R.drawable.change_border_bg)
                binding.view2.background = ActivityCompat.getDrawable(applicationContext, R.drawable.facebook_button_bg)
                binding.view1.background = ActivityCompat.getDrawable(applicationContext, R.drawable.facebook_button_bg)
                sendDataOnServer(type)
                //startActivity(Intent(applicationContext, EpigenticExecriseExecriseActivity::class.java))
                //finish()
            }
        })
    }


    private fun sendDataOnServer(type: String) {
        showProgressDialog()
        val manager = NetworkManager()
        val params = HashMap<String, String>()

        val userId = Base64.decode(Utility.getUserID(this), Base64.DEFAULT)
        val decodeUserId = String(userId, charset("UTF-8"))


        params["user_id"] = decodeUserId
        params["exercise_type"] = type
        params["training_time"] =""
        params["intense"] =""

        if (manager.isConnectingToInternet(applicationContext)) {
            manager.createApiRequest(
                ApiUtilis.getAPIInstance(applicationContext).sendExecriseData(params),
                object : ServiceListener<ExecriseResponseModel> {
                    override fun getServerResponse(response: ExecriseResponseModel, requestcode: Int) {
                        //  binding.progressBar.visibility = View.GONE
                        // setData(response)
                        Log.d("data",""+response.getCode());
                        dismissProgressDialog()
                        setResult(100)
                        finish()
                    }

                    override fun getError(error: ErrorModel, requestcode: Int) {
                        //binding.progressBar.visibility = View.GONE
                        dismissProgressDialog()
                        Utility.displayShortSnackBar(
                             binding.parentLayout,
                             error.getMessage().toString()
                         )
                    }
                })
        } else {
            //binding.progressBar.visibility = View.GONE
            dismissProgressDialog()
             Utility.displayShortSnackBar(
                 binding.parentLayout,
                 resources.getString(R.string.check_internet)
             )
        }

    }


    fun Dialog(context: Context) {
        try {
            progressDialog = ProgressDialog(context)
            progressDialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
            progressDialog!!.setProgressStyle(ProgressDialog.STYLE_SPINNER)
            progressDialog!!.setMessage(getString(R.string.please_wait))
            progressDialog!!.setCancelable(false)
        } catch (ignore: Exception) {
        }

    }

    fun showProgressDialog() {
        try {
            if (progressDialog != null) {
                progressDialog!!.show()
            }
        } catch (ignore: Exception) {
        }

    }

    fun dismissProgressDialog() {
        try {
            if (progressDialog != null) {
                progressDialog!!.dismiss()
            }
        } catch (ignore: Exception) {
        }

    }
}