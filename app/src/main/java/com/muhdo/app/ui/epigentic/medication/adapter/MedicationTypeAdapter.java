package com.muhdo.app.ui.epigentic.medication.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.muhdo.app.R;
import com.muhdo.app.ui.epigentic.medication.model.MedicationType;
import android.widget.Filter;
import java.util.ArrayList;
import java.util.List;

public class MedicationTypeAdapter extends ArrayAdapter<MedicationType> {

    Context context;
    int resource, textViewResourceId;
    List<MedicationType> items, tempItems, suggestions;

    public MedicationTypeAdapter(Context context, int resource, int textViewResourceId, List<MedicationType> items) {
        super(context, resource, textViewResourceId, items);
        this.context = context;
        this.resource = resource;
        this.textViewResourceId = textViewResourceId;
        this.items = items;
        tempItems = new ArrayList<MedicationType>(items); // this makes the difference.
        suggestions = new ArrayList<MedicationType>();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.row_autocomplete_textview, parent, false);
        }
        MedicationType drugs = items.get(position);
        if (drugs != null) {
            TextView lblName = (TextView) view.findViewById(R.id.textview_item_name);
            if (lblName != null)
                lblName.setText(drugs.getDrugsName());
        }
        return view;
    }

    @Override
    public Filter getFilter() {
        return nameFilter;
    }

    /**
     * Custom Filter implementation for custom suggestions we provide.
     */
    Filter nameFilter = new Filter() {
        @Override
        public CharSequence convertResultToString(Object resultValue) {
            String str = ((MedicationType) resultValue).getDrugsName();
            return str;
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            if (constraint != null) {
                suggestions.clear();
                for (MedicationType drugs : tempItems) {
                    if (drugs.getDrugsName().toLowerCase().contains(constraint.toString().toLowerCase())) {
                        suggestions.add(drugs);
                    }
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = suggestions;
                filterResults.count = suggestions.size();
                return filterResults;
            } else {
                return new FilterResults();
            }
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            List<MedicationType> filterList = (ArrayList<MedicationType>) results.values;
            if (results != null && results.count > 0) {
                clear();
                for (MedicationType people : filterList) {
                    add(people);
                    notifyDataSetChanged();
                }
            }
        }
    };
}