package com.muhdo.app.ui.epigenticResult

import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Html
import android.view.View
import androidx.databinding.DataBindingUtil
import com.muhdo.app.R
import com.muhdo.app.databinding.ActivityRecommDetailBinding


class RecommDetailActivity : AppCompatActivity() {
    internal lateinit var binding: ActivityRecommDetailBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this@RecommDetailActivity, R.layout.activity_recomm_detail)
        getIntentData()

        binding.btnBack.setOnClickListener(object : View.OnClickListener{
            override fun onClick(v: View?) {
                finish()

            }

        })



    }

    private fun getIntentData() {
        val intent = getIntent();
        val trackData = intent.getStringExtra("object")
        val trackTitle = intent.getStringExtra("title")

        if(!trackData!!.equals("")){
            binding.textViewRecommDetailTitle.text=trackTitle
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                binding.textViewRecommInfoDetail.text = Html.fromHtml(trackData, Html.FROM_HTML_MODE_COMPACT)
            } else {
                binding.textViewRecommInfoDetail.text = Html.fromHtml(trackData)
            }
        }
        //binding.textViewRecommInfoDetail.text=trackData
    }
}
