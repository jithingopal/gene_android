package com.muhdo.app.ui.epigentic.food.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.muhdo.app.ui.epigentic.food.model.Breakfast;
import com.muhdo.app.ui.epigentic.food.model.Dinner;
import com.muhdo.app.ui.epigentic.food.model.Lunch;
import com.muhdo.app.ui.epigentic.food.model.SnacksDrinks;

public class FoodCategoryResponseData {


    @SerializedName("BREAKFAST")
    @Expose
    private Breakfast breakfast;

    @SerializedName("LUNCH")
    @Expose
    private Lunch lunch;

    @SerializedName("DINNER")
    @Expose
    private Dinner dinner;

    @SerializedName("SNACKS_DRINKS")
    @Expose
    private SnacksDrinks snacksDrink;


    public Breakfast getBreakfast() {
        return breakfast;
    }

    public void setBreakfast(Breakfast breakfast) {
        this.breakfast = breakfast;
    }

    public Lunch getLunch() {
        return lunch;
    }

    public void setLunch(Lunch lunch) {
        this.lunch = lunch;
    }

    public Dinner getDinner() {
        return dinner;
    }

    public void setDinner(Dinner dinner) {
        this.dinner = dinner;
    }

    public SnacksDrinks getSnacksDrink() {
        return snacksDrink;
    }

    public void setSnacksDrink(SnacksDrinks snacksDrink) {
        this.snacksDrink = snacksDrink;
    }
}
