package com.muhdo.app.ui.login

import android.Manifest
import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationManager
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import android.text.*
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.util.Log
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.multidex.MultiDex
import com.facebook.*
import com.facebook.appevents.AppEventsLogger
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.android.gms.auth.GoogleAuthUtil
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import com.google.android.gms.tasks.Task
import com.muhdo.app.R
import com.muhdo.app.databinding.ActivityLoginBinding
import com.muhdo.app.ui.DashboardActivity
import com.muhdo.app.ui.login.v3.SignUpPrecheckActivity
import com.muhdo.app.ui.signUp.SignUpViewModel
import com.muhdo.app.ui.v3.BaseActivityV3
import com.muhdo.app.utils.PreferenceConnector
import com.timingsystemkotlin.backuptimingsystem.Utility
import org.jetbrains.anko.doAsync
import java.util.*


open class LoginActivity : BaseActivityV3(),
    GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,
    com.google.android.gms.location.LocationListener {
    override fun onConnected(p0: Bundle?) {
        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {

            return
        }


        startLocationUpdates()

        val fusedLocationProviderClient:
                FusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this)
        fusedLocationProviderClient.lastLocation
            .addOnSuccessListener {

               try{
                   if (Utility.getLat(applicationContext).isNullOrEmpty() && Utility.getLong(applicationContext).isNullOrEmpty()) {
                       Utility.saveLatLong(applicationContext, it.latitude.toString(), it.longitude.toString())
                   }
               }catch (e:java.lang.Exception){

               }


            }
    }

    override fun onConnectionSuspended(p0: Int) {
        mGoogleApiClient.connect()
    }

    override fun onConnectionFailed(p0: ConnectionResult) {

    }

    override fun onLocationChanged(p0: Location?) {
        if (Utility.getLat(applicationContext).isNullOrEmpty() && Utility.getLong(applicationContext) .isNullOrEmpty()) {

            if (p0!!.longitude.toString() != null && p0!!.latitude.toString() != null) {

                Utility.saveLatLong(applicationContext, p0.latitude.toString(), p0.longitude.toString())
//                Toast.makeText(this, "fetch location", Toast.LENGTH_SHORT).show()


            } else {
                Toast.makeText(this, "Unable to fetch location", Toast.LENGTH_SHORT).show()
            }
        }

    }

    lateinit var binding: ActivityLoginBinding
    private lateinit var signUpViewModel: SignUpViewModel
    private val CAMERA_REQUEST_CODE = 100
    private lateinit var mGoogleApiClient: GoogleApiClient
    private var mLocationManager: LocationManager? = null

    private var mLocationRequest: LocationRequest? = null

    private val updateInterval = (2 * 1000).toLong()  /* 10 secs */
    private val fastestInterval: Long = 2000 /* 2 sec */
    private lateinit var locationManager: LocationManager
    private lateinit var mGoogleSignInClient: GoogleSignInClient
    private var callbackManager = CallbackManager.Factory.create()!!
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        FacebookSdk.sdkInitialize(applicationContext)
        AppEventsLogger.activateApp(this)
        binding = DataBindingUtil.setContentView(this@LoginActivity, R.layout.activity_login)
        signUpViewModel = SignUpViewModel()
        binding.viewModel = signUpViewModel

        checkAndRequestPermissions()
        initParameters()
        initViews()

        MultiDex.install(this)

        mGoogleApiClient = GoogleApiClient.Builder(this)
            .addConnectionCallbacks(this)
            .addOnConnectionFailedListener(this)
            .addApi(LocationServices.API)
            .build()

        mLocationManager = this.getSystemService(Context.LOCATION_SERVICE) as LocationManager

        checkLocation()


        if (intent.getStringExtra("DisplayAlert") != null && intent.getStringExtra("DisplayAlert") == "Display") {
            displayAlert()
        }
        init()
        performClick()
        binding.txtLostPassword.setOnClickListener {
            val i = Intent(applicationContext, LostPasswordActivity::class.java)
            startActivity(i)
            finish()
        }

        //binding.rememberMeCheck.isChecked = true
       // Utility.rememberUser(applicationContext, true)

        var isUserRememberd=Utility.isUserRemembered(applicationContext)

        if(isUserRememberd==true){
            binding.rememberMeCheck.isChecked = true

            val userEmail=PreferenceConnector.readString(applicationContext,PreferenceConnector.EMAIL,"")
            val userPassword=PreferenceConnector.readString(applicationContext,PreferenceConnector.PASSWORD,"")
            //binding.edtUsername.text= Editable.Factory.getInstance().newEditable(userEmail)
            //binding.edtPassword.text= Editable.Factory.getInstance().newEditable(userPassword)
            Log.d("data","email "+userEmail+" "+userPassword)
             binding.edtUsername.setText(userEmail.toString())
             binding.edtPassword.setText(userPassword.toString())


        }else{
            binding.rememberMeCheck.isChecked = false
        }
        binding.rememberMeCheck.setOnCheckedChangeListener { _, b ->
            if (b) {
                Utility.rememberUser(applicationContext, true)
            } else {
                Utility.rememberUser(applicationContext, false)
            }
        }

        binding.txtPrivacyPolicy.makeLinks(
            Pair("privacy policy", View.OnClickListener {
                val uri = Uri.parse("https://muhdo.com/privacy-policy/") // missing 'http://' will cause crashed
                val intent = Intent(Intent.ACTION_VIEW, uri)
                startActivity(intent)
            })
        )

        binding.txtPrivacyPolicy.makeLinks(
            Pair("terms and conditions", View.OnClickListener {
                val uri = Uri.parse("https://muhdo.com/terms-conditions/") // missing 'http://' will cause crashed
                val intent = Intent(Intent.ACTION_VIEW, uri)
                startActivity(intent)
            })
        )

        Log.d("data","login status"+Utility.getLoginStatus(applicationContext))
        if (Utility.getLoginStatus(applicationContext)) {
            val i = Intent(applicationContext, DashboardActivity::class.java)
            startActivity(i)
            finish()
        }
    }


    private fun isLocationEnabled(): Boolean {
        locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(
            LocationManager.NETWORK_PROVIDER
        )
    }


    private fun startLocationUpdates() {
        // Create the location request
        mLocationRequest = LocationRequest.create()
            .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
            .setInterval(updateInterval)
            .setFastestInterval(fastestInterval)
        // Request location updates
        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(
            mGoogleApiClient,
            mLocationRequest, this
        )
    }

    private fun checkLocation(): Boolean {
        if (!isLocationEnabled())
            showAlert()
        return isLocationEnabled()
    }


    private fun showAlert() {
        val dialog = AlertDialog.Builder(this)
        dialog.setTitle("Enable Location")
            .setMessage("Your Locations Settings is set to 'Off'.\nPlease Enable Location to " + "use this app")
            .setPositiveButton("Location Settings") { _, _ ->
                val myIntent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                startActivity(myIntent)
            }
            .setNegativeButton("Cancel") { _, _ -> }
        dialog.show()
    }

    private fun TextView.makeLinks(vararg links: Pair<String, View.OnClickListener>) {
        val spannableString = SpannableString(this.text)
        for (link in links) {
            val clickableSpan = object : ClickableSpan() {
                override fun onClick(view: View) {
                    Selection.setSelection((view as TextView).text as Spannable, 0)
                    view.invalidate()
                    link.second.onClick(view)
                }
            }
            val startIndexOfLink = this.text.toString().indexOf(link.first)
            spannableString.setSpan(
                clickableSpan, startIndexOfLink, startIndexOfLink + link.first.length,
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
            )
        }
        this.movementMethod = LinkMovementMethod.getInstance() // without LinkMovementMethod, link can not click
        this.setText(spannableString, TextView.BufferType.SPANNABLE)
    }

    private fun displayAlert() {
        val dialogBuilder = AlertDialog.Builder(this@LoginActivity)
        dialogBuilder.setMessage("Please check your email inbox to activate and verify your account")
            .setCancelable(false)
            .setPositiveButton("OK") { _, _ ->

            }


        val alert = dialogBuilder.create()
        alert.show()
    }


    private fun initParameters() {
        callbackManager = CallbackManager.Factory.create()

        LoginManager.getInstance().registerCallback(callbackManager,
            object : FacebookCallback<LoginResult> {
                override fun onSuccess(loginResult: LoginResult) {
                    // App code

                    // App code
                    val request = GraphRequest.newMeRequest(
                        loginResult.accessToken
                    ) { `object`, response ->
                        Log.v("LoginActivity", response.toString())
                        signUpViewModel.fbLogin(
                            `object`,
                            this@LoginActivity,
                            binding,
                            loginResult.accessToken.token.toString()
                        )
                    }
                    val parameters = Bundle()
                    parameters.putString("fields", "id,name,email,first_name,last_name")
                    request.parameters = parameters
                    request.executeAsync()
                }

                override fun onCancel() {
                    // App code
                }

                override fun onError(exception: FacebookException) {
                    // App code
                }
            })


    }

    private fun initViews() {
        binding.btnFacebook.setOnClickListener {
            val accessToken = AccessToken.getCurrentAccessToken()
            if (accessToken != null && !accessToken.isExpired) {
                LoginManager.getInstance().logOut()

                LoginManager.getInstance().logInWithReadPermissions(
                    this,
                    Arrays.asList("public_profile", "email")
                )
            } else {
                LoginManager.getInstance().logInWithReadPermissions(
                    this,
                    Arrays.asList("public_profile", "email")
                )
            }
        }

    }

    private fun signIn() {
        binding.progressBar.visibility = VISIBLE
        val signInIntent = mGoogleSignInClient.signInIntent
        startActivityForResult(signInIntent, 16)
    }


    private fun performClick() {
        binding.btnSignUp.setOnClickListener {
            /*val i = Intent(applicationContext, SignUpActivity::class.java)
            startActivity(i)
            finish()*/

            val i = Intent(applicationContext, SignUpPrecheckActivity::class.java)
              startActivity(i)
        }

        binding.btnGoogle.setOnClickListener {
            //            binding.signInButton.performClick()
            signIn()
        }

        binding.signInButton.setOnClickListener {
            signIn()
        }

    }

    private fun init() {
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestEmail()
            .build()
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso)
    }


    private fun handleSignInResult(completedTask: Task<GoogleSignInAccount>) {
        try {
            binding.progressBar.visibility = GONE
            //  if(completedTask.isSuccessful){
            val account = completedTask.getResult(ApiException::class.java)
            Log.wtf("data", "" + account!!.email)
            val scopes = "oauth2:profile email"
            doAsync {
                val token = GoogleAuthUtil.getToken(this@LoginActivity, account!!.email, scopes)
                signUpViewModel.socialLogin(account, this@LoginActivity, binding, token)
            }
            // }

        } catch (e: ApiException) {
            com.timingsystemkotlin.backuptimingsystem.Utility.displayShortSnackBar(
                binding.parentLayout,
                "Something went wrong " + e.statusCode.toString()
            )
            Log.w("Login", "signInResult:failed code=" + e.statusCode)
        }

    }


    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 16) {
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            handleSignInResult(task)

        } else {
            try {
                callbackManager.onActivityResult(requestCode, resultCode, data)
            } catch (e: Exception) {
                // TODO: handle exception
                Toast.makeText(applicationContext, "ERROR: " + e.message, Toast.LENGTH_LONG).show()
                println("Facebook error : $e")
            }
        }
    }


    // permission
    private fun checkAndRequestPermissions(): Boolean {
        val camerapermission = ContextCompat.checkSelfPermission(applicationContext, Manifest.permission.CAMERA)
        val locationPermission =
            ContextCompat.checkSelfPermission(applicationContext, Manifest.permission.ACCESS_FINE_LOCATION)
//        val callpermission = ContextCompat.checkSelfPermission(applicationContext, Manifest.permission.CALL_PHONE)
        val listPermissionsNeeded = ArrayList<String>()
        if (camerapermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CAMERA)
        }
        if (locationPermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.ACCESS_FINE_LOCATION)
        }
//        if (callpermission != PackageManager.PERMISSION_GRANTED) {
//            listPermissionsNeeded.add(Manifest.permission.CALL_PHONE)
//        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(
                this@LoginActivity,
                listPermissionsNeeded.toTypedArray(),
                CAMERA_REQUEST_CODE
            )
            return false
        }
        return true
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>, grantResults: IntArray
    ) {
        Log.d("App", "Permission callback called-------")
        when (requestCode) {
            CAMERA_REQUEST_CODE -> {

                val perms = HashMap<String, Int>()
                perms[Manifest.permission.CAMERA] = PackageManager.PERMISSION_GRANTED
                perms[Manifest.permission.ACCESS_FINE_LOCATION] = PackageManager.PERMISSION_GRANTED

                // Fill with actual results from user
                if (grantResults.isNotEmpty()) {
                    for (i in permissions.indices)
                        perms[permissions[i]] = grantResults[i]
                    // Check for both permissions
                    if (perms[Manifest.permission.CAMERA] == PackageManager.PERMISSION_GRANTED
                        && perms[Manifest.permission.ACCESS_FINE_LOCATION] == PackageManager.PERMISSION_GRANTED
                    ) {
                        // process the normal flow
                        //else any one or both the permissions are not granted
                    } else {
                        Log.d("Act", "Some permissions are not granted ask again ")
                        //permission is denied (this is the first time, when "never ask again" is not checked) so ask again explaining the usage of permission
                        //                        // shouldShowRequestPermissionRationale will return true
                        //showDialog the dialog or snackbar saying its necessary and try again otherwise proceed with setup.
                        if (ActivityCompat.shouldShowRequestPermissionRationale(
                                this@LoginActivity,
                                Manifest.permission.CAMERA
                            )
                            || ActivityCompat.shouldShowRequestPermissionRationale(
                                this@LoginActivity,
                                Manifest.permission.ACCESS_FINE_LOCATION
                            )
                        ) {
                            showDialogOK("Service Permissions are required for this app",
                                DialogInterface.OnClickListener { dialog, which ->
                                    when (which) {
                                        DialogInterface.BUTTON_POSITIVE -> checkAndRequestPermissions()
                                        DialogInterface.BUTTON_NEGATIVE ->
                                            dialog.dismiss()
                                    }
                                })
                        }
                    }
                }
            }
        }
    }


    private fun showDialogOK(message: String, okListener: DialogInterface.OnClickListener) {
        AlertDialog.Builder(this@LoginActivity)
            .setMessage(message)
            .setPositiveButton("OK", okListener)
            .setNegativeButton("Cancel", okListener)
            .create()
            .show()
    }


    override fun onStart() {
        super.onStart()
        mGoogleApiClient.connect()
    }

    override fun onStop() {
        super.onStop()
        if (mGoogleApiClient.isConnected) {
            mGoogleApiClient.disconnect()
        }
    }

    override fun onBackPressed() {
        val intent = Intent(Intent.ACTION_MAIN)
        intent.addCategory(Intent.CATEGORY_HOME)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)
    }

}