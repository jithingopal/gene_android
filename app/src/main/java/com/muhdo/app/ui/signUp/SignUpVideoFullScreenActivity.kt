package com.muhdo.app.ui.signUp

import androidx.appcompat.app.AppCompatActivity
import com.muhdo.app.R
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.ActivityInfo
import android.graphics.Color
import android.os.Bundle
import android.provider.DocumentsContract.EXTRA_ORIENTATION
import android.util.Log
import android.view.View
import android.widget.FrameLayout
import com.ct7ct7ct7.androidvimeoplayer.listeners.VimeoPlayerReadyListener
import com.ct7ct7ct7.androidvimeoplayer.listeners.VimeoPlayerTimeListener
import com.ct7ct7ct7.androidvimeoplayer.model.PlayerState
import com.ct7ct7ct7.androidvimeoplayer.model.TextTrack
import com.ct7ct7ct7.androidvimeoplayer.view.VimeoPlayerActivity
import com.ct7ct7ct7.androidvimeoplayer.view.VimeoPlayerActivity.REQUEST_ORIENTATION_LANDSCAPE
import com.ct7ct7ct7.androidvimeoplayer.view.VimeoPlayerActivity.REQUEST_ORIENTATION_PORTRAIT
import com.ct7ct7ct7.androidvimeoplayer.view.VimeoPlayerView
import kotlinx.android.synthetic.main.workout_child_layout.*


class SignUpVideoFullScreenActivity : AppCompatActivity() {
    var REQUEST_CODE = 1234
    private var videoId: Int = 0
    private var hashKey: String? = null
    private var baseUrl: String? = null
    private var startAt: Float = 0.toFloat()
    private var endAt: Float = 0.toFloat()
    private var topicColor: Int = 0
    private var loop: Boolean = false
    private var aspectRatio: Float = 0.toFloat()
    private var orientation: String? = null
    private var frameLayout: FrameLayout? = null
    var context=this
    private val EXTRA_ORIENTATION = "EXTRA_ORIENTATION"
    private val EXTRA_VIDEO_ID = "EXTRA_VIDEO_ID"
    private val EXTRA_HASH_KEY = "EXTRA_HASH_KEY"
    private val EXTRA_BASE_URL = "EXTRA_BASE_URL"
    private val EXTRA_START_AT = "EXTRA_START_AT"
    private val EXTRA_END_AT = "EXTRA_END_AT"
    private val EXTRA_TOPIC_COLOR = "EXTRA_TOPIC_COLOR"
    private val EXTRA_LOOP = "EXTRA_LOOP"
    private val EXTRA_ASPECT_RATIO = "EXTRA_ASPECT_RATIO"
    private var vimeoPlayerView: VimeoPlayerView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        orientation = intent.getStringExtra(EXTRA_ORIENTATION)
        if (REQUEST_ORIENTATION_PORTRAIT == orientation) {
            requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        } else if (REQUEST_ORIENTATION_LANDSCAPE == orientation) {
            requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
        }
        setContentView(R.layout.activity_sign_up_video_full_screen)
        val videoView = findViewById<VimeoPlayerView>(R.id.vimeoPlayer)
        context.lifecycle.addObserver(videoView)
        val link1 = "https://vimeo.com/341308382"
        videoView.initialize(341308382,link1)
        videoView.play()
        //setupView()
    }

    private fun setupView() {
       /* lifecycle.addObserver(vimeoPlayer)
        val link1 = "https://vimeo.com/341308382"
        vimeoPlayer.initialize(341308382,link1)
        vimeoPlayer.setFullscreenVisibility(true)*/
        /*vimeoPlayer.play()
        vimeoPlayer.setFullscreenClickListener {
            var requestOrientation = VimeoPlayerActivity.REQUEST_ORIENTATION_AUTO
            startActivityForResult(VimeoPlayerActivity.createIntent(this, requestOrientation, vimeoPlayer), REQUEST_CODE)
        }*/



        val vimeoPlayerView = findViewById<VimeoPlayerView>(R.id.vimeoPlayer)
        frameLayout = findViewById<FrameLayout>(com.ct7ct7ct7.androidvimeoplayer.R.id.frameLayout)
        videoId = intent.getIntExtra(EXTRA_VIDEO_ID, 0)
        hashKey = intent.getStringExtra(EXTRA_HASH_KEY)
        baseUrl = intent.getStringExtra(EXTRA_BASE_URL)
        startAt = intent.getFloatExtra(EXTRA_START_AT, 0f)
        endAt = intent.getFloatExtra(EXTRA_END_AT, java.lang.Float.MAX_VALUE)
        topicColor = intent.getIntExtra(EXTRA_TOPIC_COLOR, Color.rgb(0, 172, 240))
        loop = intent.getBooleanExtra(EXTRA_LOOP, false)
        aspectRatio = intent.getFloatExtra(EXTRA_ASPECT_RATIO, 16f / 9)


        vimeoPlayerView.initialize(videoId, hashKey, baseUrl)
        //vimeoPlayerView.play();
        vimeoPlayerView.addReadyListener(object : VimeoPlayerReadyListener {
            override fun onReady(title: String, duration: Float, textTrackArray: Array<TextTrack>) {
                // vimeoPlayerView.seekTo(startAt);
                // vimeoPlayerView.playTwoStage();
            }

            override fun onInitFailed() {

            }
        })
        vimeoPlayerView.addTimeListener(VimeoPlayerTimeListener { second ->
            if (second >= endAt) {
                vimeoPlayer.pause()
            }
        })
        vimeoPlayerView.setFullscreenClickListener(View.OnClickListener { onBackPressed() })

    }


 /*   override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_CODE) {
            var playAt = data!!.getFloatExtra(VimeoPlayerActivity.RESULT_STATE_VIDEO_PLAY_AT, 0f)
            vimeoPlayer.seekTo(playAt)

            var playerState = PlayerState.valueOf(data!!.getStringExtra(VimeoPlayerActivity.RESULT_STATE_PLAYER_STATE))
            when (playerState) {
                PlayerState.PLAYING -> vimeoPlayer.play()
                PlayerState.PAUSED -> vimeoPlayer.pause()
            }
        }
    }*/


    override fun onBackPressed() {
         super.onBackPressed()
        finish()
    }
}
