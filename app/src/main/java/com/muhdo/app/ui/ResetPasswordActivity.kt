package com.muhdo.app.ui

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.muhdo.app.R
import com.muhdo.app.apiModel.login.VerifyOtpModel
import com.muhdo.app.databinding.ActivityResetPasswordBinding
import com.muhdo.app.repository.ApiUtilis
import com.muhdo.app.repository.NetworkManager2
import com.muhdo.app.ui.login.LoginActivity
import com.timingsystemkotlin.backuptimingsystem.Utility
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

class ResetPasswordActivity : AppCompatActivity() {
    lateinit var binding: ActivityResetPasswordBinding


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(
            this@ResetPasswordActivity,
            R.layout.activity_reset_password
        )

        if (intent != null && intent.getStringExtra("EmailID") != null) {
            binding.edtUsername.setText(intent.getStringExtra("EmailID"))
        }

        binding.btnReset.setOnClickListener {
            if (checkValidation()) {
                Log.e("Error", "Error in Login")
                resetPassword()
            }
        }


        binding.btnBack.setOnClickListener {
            val i = Intent(applicationContext, LoginActivity::class.java)
            startActivity(i)
            finish()
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        val i = Intent(applicationContext, LoginActivity::class.java)
        startActivity(i)
        finish()
    }

    @SuppressLint("NewApi")
    private fun resetPassword() {

        binding.progressBar.visibility = View.VISIBLE


        val manager = NetworkManager2()
        if (manager.isConnectingToInternet(applicationContext)) {


            val params = HashMap<String, String>()
            params["email"] = binding.edtUsername.text.toString()
            params["newPassword"] = Utility.encodeString(binding.edtPassword.text.toString())
            params["verificationCode"] = binding.edtCode.text.toString()
            ApiUtilis.getAPIInstance(applicationContext).resetPassword(params)
                .enqueue(object : Callback<VerifyOtpModel> {
                    override fun onResponse(
                        call: Call<VerifyOtpModel>,
                        response: Response<VerifyOtpModel>
                    ) {
                        binding.progressBar.visibility = View.GONE
                        if (response.body() == null) {
                            Utility.displayShortSnackBar(
                                binding.parentLayout,
                                "Bad request."
                            )
                        } else {
                            if (response.body() != null && response.body()!!.getStatusCode() == 200) {
                                val i = Intent(applicationContext, LoginActivity::class.java)
                                startActivity(i)
                                finish()
                            } else if (response.body() != null) {
                                Utility.displayShortSnackBar(
                                    binding.parentLayout,
                                    response.body()!!.getData().toString()
                                )
                            }
                        }
                    }

                    override fun onFailure(call: Call<VerifyOtpModel>, t: Throwable) {
                        // Log error here since request failed
                        binding.progressBar.visibility = View.GONE
                        Log.e("Login", t.toString())
                        Utility.displayShortSnackBar(
                            binding.parentLayout,
                            "Server connection error!!, please try again later."
                        )
                    }
                })
        } else {
            binding.progressBar.visibility = View.GONE

            Utility.displayShortSnackBar(
                binding.parentLayout,
                resources.getString(R.string.check_internet)
            )

        }

    }


    private fun checkValidation(): Boolean {
        var isValid = true
        when {

            binding.edtCode.text.isEmpty() -> {
                binding.edtCode.error = "Please enter verification code"
                binding.edtCode.requestFocus()
                isValid = false
            }


            binding.edtPassword.text.isEmpty() || binding.edtPassword.text.length < 8 || !Utility.isPasswordValid(
                binding.edtPassword.text
            ) -> {
                binding.edtPassword.error = getString(R.string.please_enter_password)
                binding.edtPassword.requestFocus()
                isValid = false
            }

        }
        return isValid
    }

}
