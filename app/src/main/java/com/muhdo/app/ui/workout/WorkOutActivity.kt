package com.muhdo.app.ui.workout

import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.viewpager.widget.ViewPager
import com.muhdo.app.R
import com.muhdo.app.adapter.MyPagerAdapter
import com.muhdo.app.apiModel.workout.WorkoutPlanModel
import com.muhdo.app.databinding.ActivityWorkoutBinding
import com.muhdo.app.fragment.FavouriteWorkoutFragment
import com.muhdo.app.fragment.MyPlanFragment
import com.muhdo.app.fragment.v3.InjuryPreventionFragment
import com.muhdo.app.fragment.v3.MyProfileFragment
import com.muhdo.app.fragment.v3.WarmUpCoolDownFragment
import com.muhdo.app.fragment.v3.guide.workout.WorkoutOverviewFragment
import com.muhdo.app.ui.DashboardActivity
import com.muhdo.app.utils.v3.Constants
import com.timingsystemkotlin.backuptimingsystem.Utility


class WorkOutActivity : AppCompatActivity() {
    lateinit var binding: ActivityWorkoutBinding
    internal var data: WorkoutPlanModel? = null
    var selectedPostion = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this@WorkOutActivity, R.layout.activity_workout)

        val fragmentAdapter = MyPagerAdapter(supportFragmentManager)
        binding.viewpagerMain.adapter = fragmentAdapter
        binding.tabsMain.setupWithViewPager(binding.viewpagerMain)
        binding.viewpagerMain.offscreenPageLimit = 0

        binding.toolbar.imgResurvey.setOnClickListener {
            confirmResurvey()
        }



        binding.viewpagerMain.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
                when (position) {
                    0 -> {
                        MyPlanFragment()

                    }
                    1 -> {
                        WorkoutOverviewFragment()
                    }
                    2 -> {
                        MyProfileFragment()
                    }
                    3 -> {
                        WarmUpCoolDownFragment()
                    }
                    4 -> {
                        InjuryPreventionFragment()
                    }
                    else -> {
                        FavouriteWorkoutFragment()

                    }
                }
            }

            override fun onPageSelected(position: Int) {

            }

            override fun onPageScrollStateChanged(state: Int) {

            }
        })


        binding.toolbar.btnNavigation.setOnClickListener {
            super.onBackPressed()
            val i = Intent(this@WorkOutActivity, DashboardActivity::class.java)
            i.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(i)
        }
        binding.viewpagerMain.setCurrentItem(selectedPostion)
    }

    private fun displayAlert(message: String?) {
        val dialogBuilder = AlertDialog.Builder(this)
        dialogBuilder.setMessage(message)
            .setCancelable(false)
            .setPositiveButton("Ok") { _, _ ->
                // val i = Intent(requireContext(), DashboardActivity::class.java)
                // startActivity(i)
            }
        val alert = dialogBuilder.create()
        alert.show()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        super.onBackPressed()
        val i = Intent(this@WorkOutActivity, DashboardActivity::class.java)
        i.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(i)
    }

    private fun confirmResurvey() {
        val dialogBuilder = AlertDialog.Builder(this@WorkOutActivity)
        dialogBuilder.setMessage("Are you sure you want to update your workout plan")
            .setCancelable(false)
            .setPositiveButton("Yes") { _, _ ->
                Utility.setWorkoutStatus(this@WorkOutActivity, false)
                val i = Intent(applicationContext, DashboardActivity::class.java)
                i.putExtra(Constants.DASHBOARD_ACTION, Constants.DASHBOARD_WORKOUT)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
                startActivity(i)
                finish()
            }
            .setNegativeButton("No") { _, _ ->
            }

        val alert = dialogBuilder.create()
        alert.show()
    }
}