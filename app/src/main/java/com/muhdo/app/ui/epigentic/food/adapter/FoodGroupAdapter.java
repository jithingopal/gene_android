package com.muhdo.app.ui.epigentic.food.adapter;

import android.content.Context;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.*;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.muhdo.app.R;
import com.muhdo.app.ui.epigentic.food.EpigenticFoodNewActivity;
import com.muhdo.app.ui.epigentic.food.model.FoodItem;

import java.util.ArrayList;
import java.util.List;

public class FoodGroupAdapter extends RecyclerView.Adapter<FoodGroupAdapter.MyViewHolder> implements Filterable {

    private List<FoodItem> supplymentInfoList;
    private List<FoodItem> supplymentInfoListFiltered;
    private supplementInfoListListener listener;
    public EpigenticFoodNewActivity mContext;
    String quantity;
    String spinnerVal;


    /**
     * View holder class
     */
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView textViewIngredientName, textViewUnit;
        public EditText unitValue;
        public LinearLayout addFood;
        public LinearLayout viewNumeric;
        public Spinner viewDropdown;




        public MyViewHolder(View view) {
            super(view);
            textViewIngredientName = (TextView) view.findViewById(R.id.textview_supplement_type);
            textViewUnit = (TextView) view.findViewById(R.id.textview_unit);
            unitValue = (EditText) view.findViewById(R.id.editText_unit_value);
            addFood = (LinearLayout) view.findViewById(R.id.layout_add_supplyment);
            viewNumeric = (LinearLayout) view.findViewById(R.id.view_numeric);
            viewDropdown = (Spinner) view.findViewById(R.id.spinner_food_group_item);


        }
    }

    public FoodGroupAdapter(List supplymentInfoList, EpigenticFoodNewActivity mContext) {
        this.supplymentInfoList = supplymentInfoList;
        this.supplymentInfoListFiltered = supplymentInfoList;
        this.mContext = (EpigenticFoodNewActivity) mContext;
    }


    // Provide listener on every view
    public void setListener(supplementInfoListListener listener) {
        this.listener = listener;
    }


    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final FoodItem supplymentInfo = supplymentInfoListFiltered.get(position);
        holder.textViewIngredientName.setText(supplymentInfo.getFoodName());
        final String[] val = {""};
        if(supplymentInfo.getValueType().equals("DROPDOWN")){
            holder.viewDropdown.setVisibility(View.VISIBLE);
            holder.viewNumeric.setVisibility(View.GONE);
            final String [] items = supplymentInfo.getMeasurement().split(",");
           // final ArrayList<String> container = (ArrayList<String>) Arrays.asList(items);
            holder.viewDropdown.setAdapter(new SpinnerAdapter(this, R.layout.row_user_list_food_spinner, items));
           /* ArrayAdapter<String> adapter = new ArrayAdapter<String>(mContext, R.layout.row_spinner_food_group, container);
            adapter.setDropDownViewResource(R.layout.row_spinner_food_group);
            holder.viewDropdown.setAdapter(adapter);
           */ holder.viewDropdown.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                                  public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                                                    spinnerVal=items[position];
                                                      Log.wtf("data", "click spinner"+spinnerVal);
                                                  }

                                                              @Override
                                                              public void onNothingSelected(AdapterView<?> parent) {

                                                              }
                                                  });


            //holder.viewDropdown.setSelection(container.indexOf(supplymentInfo.g);
        }else{
            holder.viewDropdown.setVisibility(View.GONE);
            holder.viewNumeric.setVisibility(View.VISIBLE);
            holder.textViewUnit.setText(supplymentInfo.getMeasurement());
            holder.viewNumeric.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.wtf("data", "click edit text");
                }
            });
        }

        /*holder.unitValue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                 quantity=holder.unitValue.getText().toString();

            }
        });*/

        holder.addFood.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                  mContext.getWindow().setSoftInputMode(
                           WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
                    );

                    if(supplymentInfo.getValueType().equals("NUMERICAL") || supplymentInfo.getValueType().equals("")){
                      String editTextVal = holder.unitValue.getText().toString();
                      if(!editTextVal.isEmpty()){

                          if(Integer.parseInt(editTextVal)>0){
                              quantity =editTextVal;
                              holder.unitValue.setText("");
                              //hideKeyBoard(holder.unitValue,mContext);
                              Log.wtf("data", "quantity edit"+quantity);
                              new Handler().postDelayed(runnable, 2000 );
                              listener.addSupplement(supplymentInfoListFiltered.get(position), quantity, position,holder.unitValue);
                              //mContext.getCurrentFocus().clearFocus();

                          }else{
                              Toast.makeText(mContext, "Invalid value" , Toast.LENGTH_SHORT).show();
                          }

                      }else{
                          Toast.makeText(mContext, "Please enter value" , Toast.LENGTH_SHORT).show();
                      }

                  }else{
                          quantity =spinnerVal;
                          Log.wtf("data", "quantity spiinner"+quantity);
                          //Toast.makeText(mContext, "select  " + value, Toast.LENGTH_SHORT).show();
                          listener.addSupplement(supplymentInfoListFiltered.get(position), quantity, position,holder.unitValue);
                  }
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return supplymentInfoListFiltered.size();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_food_group_item, parent, false);
        return new MyViewHolder(v);
    }


    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString().trim();
                if (charString.isEmpty()) {
                    supplymentInfoListFiltered = supplymentInfoList;
                } else {
                    List<FoodItem> filteredList = new ArrayList<>();
                    for (FoodItem row : supplymentInfoList) {
                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getFoodName().trim().toLowerCase().contains(charString.toString().toLowerCase())) {
                            filteredList.add(row);
                        }
                    }

                    supplymentInfoListFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = supplymentInfoListFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                supplymentInfoListFiltered = (ArrayList<FoodItem>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }


    public interface supplementInfoListListener {
        void addSupplement(FoodItem obj, String quantity, int position, EditText unitValue);

    }


    public class SpinnerAdapter extends ArrayAdapter<String> {
        private String[] itemList;

        public SpinnerAdapter(FoodGroupAdapter context, int textViewResourceId, String[] itemList) {
            super(mContext, textViewResourceId, itemList);
            this.itemList =  itemList;
        }


        @Override
        public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
            return getCustomView(position, convertView, parent);
        }

        @NonNull
        @Override
        public View getView(int position, View convertView, @NonNull ViewGroup parent) {
            return getCustomView(position, convertView, parent);
        }

        private View getCustomView(final int position, View convertView, ViewGroup parent) {
            View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_user_list_food_spinner, parent, false);
            final TextView label = (TextView) row.findViewById(R.id.textview_intensity);
            label.setText(itemList[position]);
            return row;
        }
    }


   /* public void hideKeyBoard(EditText unitValue, EpigenticFoodNewActivity mContext){
        InputMethodManager inputManager = (InputMethodManager) mContext. getSystemService(INPUT_METHOD_SERVICE);
        inputManager.hideSoftInputFromWindow(unitValue.getWindowToken(), InputMethodManager.SHOW_FORCED);
    }*/


    Runnable runnable = new Runnable()
    {
        @Override
        public void run()
        {
            InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm != null) {
                //imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
                mContext.getWindow().setSoftInputMode(
                        WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
                );
            }

        }
    };
}
