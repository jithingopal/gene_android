package com.muhdo.app.ui.epigentic.supplyments

import android.annotation.SuppressLint
import android.app.ProgressDialog
import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.util.Base64
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.GsonBuilder
import com.mindorks.placeholderview.ExpandablePlaceHolderView
import com.mindorks.placeholderview.annotations.Layout
import com.mindorks.placeholderview.annotations.Resolve
import com.mindorks.placeholderview.annotations.expand.ChildPosition
import com.mindorks.placeholderview.annotations.expand.ParentPosition
import com.muhdo.app.R
import com.muhdo.app.databinding.ActivityEpigenticSupplementBinding
import com.muhdo.app.repository.ApiUtilis
import com.muhdo.app.repository.ErrorModel
import com.muhdo.app.repository.NetworkManager
import com.muhdo.app.repository.ServiceListener
import com.muhdo.app.ui.epigentic.medication.MedicationActivity
import com.muhdo.app.ui.epigentic.medication.response.AddMedicationRes
import com.muhdo.app.ui.epigentic.medication.response.Feed
import com.muhdo.app.ui.epigentic.supplyments.adapter.SupplementTypeAdapter
import com.muhdo.app.ui.epigentic.supplyments.model.SupplymentInfo
import com.muhdo.app.ui.epigentic.supplyments.response.SupplementResponse
import com.muhdo.app.ui.epigentic.supplyments.response.SupplementTypeResponse
import com.muhdo.app.ui.epigentic.supplyments.response.SupplymentListResponse
import com.muhdo.app.ui.epigentic.supplyments.view.HeadingView
import com.muhdo.app.utils.v3.Constants
import com.timingsystemkotlin.backuptimingsystem.Utility
import org.json.JSONArray
import java.io.IOException
import java.io.InputStream
import java.util.*

class EpigenticSupplementActivity : AppCompatActivity() {
    private var progressDialog: ProgressDialog? = null
    private var mExpandableView: ExpandablePlaceHolderView? = null
    private var spinnerSupplement: Spinner? = null
    var supplementTypeAdapter: SupplementTypeAdapter? = null
    var mList: ArrayList<SupplymentInfo>? = ArrayList()
    val mListGeneralHealth: ArrayList<SupplymentInfo>? = ArrayList()
    val mListSport: ArrayList<SupplymentInfo>? = ArrayList()
    private var mContext: Context? = null
    //private var mOnScrollListener: ExpandablePlaceHolderView.OnScrollListener? = null
    var show = false
    lateinit var binding: ActivityEpigenticSupplementBinding
    var data = arrayOf("GENERAL HEALTH", "SPORT")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding =
            DataBindingUtil.setContentView(
                this@EpigenticSupplementActivity,
                R.layout.activity_epigentic_supplement
            )
        Dialog(this)
        mExpandableView = findViewById(R.id.expandable_view_supplyment) as ExpandablePlaceHolderView
        spinnerSupplement = findViewById(R.id.spinner_supplement) as Spinner

        binding.btnBack.setOnClickListener {
            finish()
            /*  if (show == true) {
                  binding.layoutSupplyment2.visibility = View.GONE;
                  binding.layoutShowList.visibility=View.GONE;
                  binding.layoutAddSupplyment.visibility=View.VISIBLE;
                  binding.layoutSupplyment1.visibility = View.VISIBLE;

                  show = false;
              } else {
                  finish()
              }*/
        }


        showProgressDialog()
        clickExccriseOption()
        clickShowList()

        //Get supplement spinner data from server
        // getSupplementSpinnerData()
        //Call supplement api for get user data date group wise
        callSupplementService(this@EpigenticSupplementActivity)

        //Call supplement data api for get spinner data
        callSupplementDataServiceForSpinner(this@EpigenticSupplementActivity)
        // set listener on search view
        setListenerOnSearch()

    }

    private fun setListenerOnSearch() {
        binding.searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener,
            androidx.appcompat.widget.SearchView.OnQueryTextListener {
            override fun onQueryTextChange(query: String): Boolean {
                supplementTypeAdapter?.getFilter()?.filter(query);
                return false
            }

            override fun onQueryTextSubmit(query: String): Boolean {
                return false
            }
        })

    }

    private fun getSupplementSpinnerData() {
        if (mList!!.size > 0) {

        } else {
            Handler().postDelayed(runnableNew, 0)
        }
    }

    internal var runnableNew: Runnable = Runnable {
        showProgressDialog()
        binding.searchView.clearFocus()
        val manager = NetworkManager()
        if (manager.isConnectingToInternet(applicationContext)) {
            manager.createApiRequest(
                ApiUtilis.getAPIService(Constants.SUPPLEMENT_API).getAllSupplementType(),
                object : ServiceListener<SupplementTypeResponse> {
                    override fun getServerResponse(
                        response: SupplementTypeResponse,
                        requestcode: Int
                    ) {
                        //  binding.progressBar.visibility = View.GONE
                        try {
                            // do something after 1000ms
                            mList = response.getData() as ArrayList<SupplymentInfo>?
                            for (info in response.getData()!!) {
                                // Toast.makeText(applicationContext,"select"+info.ingredientName, Toast.LENGTH_SHORT).show();
                                if (info.category.equals("GENERAL_HEALTH")) {
                                    mListGeneralHealth?.add(info)
                                    Collections.sort(mListGeneralHealth)
                                } else {
                                    mListSport?.add(info)
                                    Collections.sort(mListSport)

                                }
                            }

                            //dismissProgressDialog()
                            supplementTypeAdapter = SupplementTypeAdapter(
                                mListGeneralHealth!!,
                                this@EpigenticSupplementActivity
                            )
                            binding.recyclerViewSpplementType.adapter = supplementTypeAdapter

                            dismissProgressDialog()
                            supplementTypeAdapter!!.setListener(object :
                                SupplementTypeAdapter.supplementInfoListListener {
                                override fun addSupplement(
                                    obj: SupplymentInfo,
                                    quantity: String,
                                    position: Int
                                ) {


                                    // Toast.makeText(applicationContext,"select"+ obj!!.ingredientName+" "+quantity, Toast.LENGTH_SHORT).show();
                                    if (obj != null) {
                                        sendSupplementDataOnServer(obj, quantity)
                                    }
                                }

                            })
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }

                    override fun getError(error: ErrorModel, requestcode: Int) {
                        //binding.progressBar.visibility = View.GONE
                        Utility.displayShortSnackBar(
                            binding.parentLayout,
                            error.getMessage().toString()
                        )
                    }
                })
        } else {
            //binding.progressBar.visibility = View.GONE
            Utility.displayShortSnackBar(
                binding.parentLayout,
                resources.getString(R.string.check_internet)
            )

        }


    }

    @SuppressLint("WrongConstant")
    private fun callSupplementDataServiceForSpinner(mContext: EpigenticSupplementActivity) {
        binding.recyclerViewSpplementType.layoutManager =
            LinearLayoutManager(this, LinearLayout.VERTICAL, false)
        var spinnerAdapter = CustomDropDownAdapter(mContext!!, data)
        spinnerSupplement?.adapter = spinnerAdapter
        spinnerSupplement?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: android.view.View?,
                position: Int,
                id: Long
            ) {
                if (parent != null) {
                    var spinnerVal = data[position]
                    if (spinnerVal.equals("GENERAL HEALTH")) {
                        setDataInRecyclerView(mListGeneralHealth!!);
                    } else {
                        setDataInRecyclerView(mListSport!!);
                    }
                    /*val handler = Handler()
                    handler.postDelayed({
                        // do something after 1000ms

                    }, 1000)
*/

                }
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                //  supplementTypeAdapter!!.notifyDataSetChanged()
            }
        }

    }

    private fun setDataInRecyclerView(list: ArrayList<SupplymentInfo>) {
        supplementTypeAdapter = SupplementTypeAdapter(list!!, this@EpigenticSupplementActivity)
        binding.recyclerViewSpplementType.adapter = supplementTypeAdapter
        supplementTypeAdapter!!.setListener(object :
            SupplementTypeAdapter.supplementInfoListListener {
            override fun addSupplement(
                obj: SupplymentInfo,
                quantity: String,
                position: Int
            ) {

                // Toast.makeText(applicationContext, "select" , Toast.LENGTH_SHORT).show();
                if (obj != null) {
                    sendSupplementDataOnServer(obj, quantity)
                }

            }

        })
    }

    private fun sendSupplementDataOnServer(supplementInfo: SupplymentInfo, quantity: String) {

        showProgressDialog()

        val userId = Base64.decode(Utility.getUserID(this), Base64.DEFAULT)
        val decodeUserId = String(userId, charset("UTF-8"))

        val manager = NetworkManager()
        val params = HashMap<String, String>()
        params["user_id"] = decodeUserId
        params["supplement_id"] = supplementInfo.id
        params["category"] = supplementInfo.category
        params["micronutrient_group"] = supplementInfo.micronutrientGroup
        params["ingredient"] = supplementInfo.ingredientName
        params["dosage"] = supplementInfo.dosage
        params["quantity"] = quantity

        if (manager.isConnectingToInternet(this@EpigenticSupplementActivity)) {
            manager.createApiRequest(
                ApiUtilis.getAPIService(Constants.SUPPLEMENT_API).sendSupplementData(params),
                object : ServiceListener<SupplementResponse> {
                    override fun getServerResponse(response: SupplementResponse, requestcode: Int) {
                        //  binding.progressBar.visibility = View.GONE
                        // setData(response)
                        Log.d("data", "" + response.getCode());
                        if (response.getCode() == 200) {
                            //dismissProgressDialog()

                            binding.searchView.clearFocus()
                            binding.searchView.setFocusable(false);
                            binding.searchView.setIconified(false);
                            callSupplementService(this@EpigenticSupplementActivity)

                        } else {
                            Utility.displayShortSnackBar(
                                binding.parentLayout,
                                response.getMessage().toString()
                            )
                        }
                    }

                    override fun getError(error: ErrorModel, requestcode: Int) {
                        dismissProgressDialog()
                        //binding.progressBar.visibility = View.GONE
                        Utility.displayShortSnackBar(
                            binding.parentLayout,
                            error.getMessage().toString()
                        )
                    }
                })
        } else {
            //binding.progressBar.visibility = View.GONE
            dismissProgressDialog()
            Utility.displayShortSnackBar(
                binding.parentLayout,
                resources.getString(R.string.check_internet)
            )
        }
    }

    private fun clickExccriseOption() {
        binding.layoutAddSupplyment.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                Handler().postDelayed(runnable, 0)

                /*val handler = Handler()
                handler.postDelayed({
                    // do something after 1000ms

                }, 1000)*/


            }
        })
    }

    internal var runnable: Runnable = Runnable {

        Log.d("data", "click a")
        show = true
        binding.layoutSupplyment2.visibility = View.VISIBLE;
        binding.layoutShowList.visibility = View.VISIBLE;
        binding.layoutSupplyment1.visibility = View.GONE
        binding.layoutAddSupplyment.visibility = View.GONE;
        getSupplementSpinnerData()

    }

    private fun clickShowList() {
        binding.layoutShowList.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                Log.d("data", "click a")
                show = false
                //  Toast.makeText(applicationContext,"hello", Toast.LENGTH_SHORT).show();
                binding.layoutSupplyment2.visibility = View.GONE;
                binding.layoutShowList.visibility = View.GONE;
                binding.layoutSupplyment1.visibility = View.VISIBLE
                binding.layoutAddSupplyment.visibility = View.VISIBLE;
            }
        })
    }


    private fun loadJSONFromAsset(context: Context, jsonFileName: String): String? {
        var json: String? = null
        var `is`: InputStream? = null
        try {
            val manager = context.assets
            Log.d("data", "path $jsonFileName")
            `is` = manager.open(jsonFileName)
            val size = `is`!!.available()
            val buffer = ByteArray(size)
            `is`.read(buffer)
            `is`.close()
            json = String(buffer)
        } catch (ex: IOException) {
            ex.printStackTrace()
            return null
        }

        return json
    }


    fun loadFeeds(context: Context): List<Feed>? {
        try {
            val builder = GsonBuilder()
            val gson = builder.create()
            val array = JSONArray(loadJSONFromAsset(context, "news.json"))
            val feedList = ArrayList<Feed>()
            for (i in 0 until array.length()) {
                val feed = gson.fromJson<Feed>(array.getString(i), Feed::class.java!!)
                feedList.add(feed)
            }
            return feedList
        } catch (e: Exception) {
            Log.d("data", "seedGames parseException $e")
            e.printStackTrace()
            return null
        }

    }

    fun callSupplementService(mContext: EpigenticSupplementActivity) {
        // mContext.showProgressDialog()

        val userId = Base64.decode(Utility.getUserID(this), Base64.DEFAULT)
        val decodeUserId = String(userId, charset("UTF-8"))

        val manager = NetworkManager()
        if (manager.isConnectingToInternet(mContext)) {
            manager.createApiRequest(
                ApiUtilis.getAPIService(Constants.SUPPLEMENT_API).getSupplementData(decodeUserId),
                object : ServiceListener<SupplymentListResponse> {
                    override fun getServerResponse(
                        response: SupplymentListResponse,
                        requestcode: Int
                    ) {
                        //  binding.progressBar.visibility = View.GONE
                        // setData(response)
                        mContext.dismissProgressDialog()
                        //Log.d("data",""+response.dateGroupModelList.get(0).date_group);
                        mExpandableView?.removeAllViews();

                        try {
                            if (response.statusCode.equals("200") && response.dateGroupModelList.size > 0) {
                                Collections.reverse(response.dateGroupModelList);
                                if (response.dateGroupModelList != null) {
                                    for (dataGroup in response.dateGroupModelList) {
                                        mExpandableView!!.addView(
                                            HeadingView(
                                                mContext,
                                                dataGroup.date_group
                                            )
                                        )
                                        for (info in dataGroup.getInfoList()) {
                                            mExpandableView!!.addView(ItemView(mContext, info))
                                        }
                                    }
                                }


                                show = false
                                //  Toast.makeText(applicationContext,"hello", Toast.LENGTH_SHORT).show();
                                binding.layoutSupplyment2.visibility = View.GONE;
                                binding.layoutShowList.visibility = View.GONE;
                                binding.layoutSupplyment1.visibility = View.VISIBLE
                                binding.layoutAddSupplyment.visibility = View.VISIBLE;
                            } else {
                                Utility.displayShortSnackBar(
                                    binding.parentLayout,
                                    resources.getString(R.string.no_data)
                                )
                            }
                        } catch (e: Exception) {

                        }


                    }

                    override fun getError(error: ErrorModel, requestcode: Int) {
                        //binding.progressBar.visibility = View.GONE
                        mContext.dismissProgressDialog()
                        Utility.displayShortSnackBar(
                            binding.parentLayout,
                            error.getMessage().toString()
                        )
                    }
                })
        } else {
            mContext.dismissProgressDialog()
            //binding.progressBar.visibility = View.GONE
            Utility.displayShortSnackBar(
                binding.parentLayout,
                resources.getString(R.string.check_internet)
            )

        }
    }


    @Layout(R.layout.row_medication_heading_item)
    class ItemView(
        private val mContext: EpigenticSupplementActivity,
        private val mInfo: SupplymentInfo

    ) : MedicationActivity() {

        @ParentPosition
        private val mParentPosition: Int = 0

        @ChildPosition
        private val mChildPosition: Int = 0

        @com.mindorks.placeholderview.annotations.View(R.id.textview_medication_type)
        private val name: TextView? = null

        @com.mindorks.placeholderview.annotations.View(R.id.textview_medication_unit)
        private val unit: TextView? = null

        @com.mindorks.placeholderview.annotations.View(R.id.textview_unit_value)
        private val unitVal: TextView? = null

        @com.mindorks.placeholderview.annotations.View(R.id.view_delete)
        private val viewDelete: LinearLayout? = null

        @Resolve
        private fun onResolved() {
            name!!.setText(mInfo.ingredientName)
            unit!!.setText(mInfo.dosage)
            unitVal!!.setText(mInfo.quantity)
            viewDelete?.setOnClickListener(object : android.view.View.OnClickListener {
                override fun onClick(v: android.view.View?) {
                    Log.d("data", "click a")
                    // Toast.makeText(mContext,"delete"+ mInfo.id,Toast.LENGTH_SHORT).show();
                    callDeleteService(mContext);
                }

                private fun callDeleteService(mContext: EpigenticSupplementActivity) {
                    mContext.showProgressDialog()
                    //binding.progressBar.visibility = View.VISIBLE
                    val manager = NetworkManager()
                    if (manager.isConnectingToInternet(mContext)) {
                        manager.createApiRequest(
                            ApiUtilis.getAPIService(Constants.SUPPLEMENT_API).deleteSupplement(mInfo.id),
                            object : ServiceListener<AddMedicationRes> {
                                override fun getServerResponse(
                                    response: AddMedicationRes,
                                    requestcode: Int
                                ) {
                                    if (response.statusCode.equals("200")) {
                                        mContext.callSupplementService(mContext)
                                    } else {
                                        Utility.displayShortSnackBar(
                                            binding.parentLayout,
                                            response.getMessage().toString()
                                        )
                                    }

                                }

                                override fun getError(error: ErrorModel, requestcode: Int) {
                                    //binding.progressBar.visibility = View.GONE
                                    mContext.dismissProgressDialog()
                                    Utility.displayShortSnackBar(
                                        binding.parentLayout,
                                        error.getMessage().toString()
                                    )
                                }
                            })
                    } else {
                        mContext.dismissProgressDialog()
                        // binding.progressBar.visibility = View.GONE

                        Utility.displayShortSnackBar(
                            binding.parentLayout,
                            resources.getString(R.string.check_internet)
                        )
                    }


                }
            })


        }
    }


    open class CustomDropDownAdapter(val context: Context, var listItemsTxt: Array<String>) :
        BaseAdapter() {
        val mInflater: LayoutInflater = LayoutInflater.from(context)
        override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
            val view: View
            val vh: ItemRowHolder
            if (convertView == null) {
                view = mInflater.inflate(R.layout.row_exercise_spinner_intensity, parent, false)
                vh = ItemRowHolder(view)
                view?.tag = vh
            } else {
                view = convertView
                vh = view.tag as ItemRowHolder
            }

            // setting adapter item height programatically.

            vh.label.text = listItemsTxt.get(position)

            return view
        }

        override fun getItem(position: Int): Any? {

            return null

        }

        override fun getItemId(position: Int): Long {

            return 0

        }

        override fun getCount(): Int {
            return listItemsTxt.size
        }

        private class ItemRowHolder(row: View?) {

            val label: TextView

            init {
                this.label = row?.findViewById(R.id.textview_intensity) as TextView
            }
        }
    }


    fun Dialog(context: Context) {
        try {
            progressDialog = ProgressDialog(context)
            progressDialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
            progressDialog!!.setProgressStyle(ProgressDialog.STYLE_SPINNER)
            progressDialog!!.setMessage(getString(R.string.please_wait))
            progressDialog!!.setCancelable(false)
        } catch (ignore: Exception) {
        }

    }

    fun showProgressDialog() {
        try {
            if (progressDialog != null) {
                progressDialog!!.show()
            }
        } catch (ignore: Exception) {
        }

    }

    fun dismissProgressDialog() {
        try {
            if (progressDialog != null) {
                progressDialog!!.dismiss()
            }
        } catch (ignore: Exception) {
        }

    }

    fun View.hideKeyboard() {
        val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(windowToken, 0)
    }

}
