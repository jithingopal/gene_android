package com.muhdo.app.ui

import android.content.Intent
import android.graphics.Rect
import android.os.Bundle
import android.util.TypedValue
import android.view.View
import android.view.View.VISIBLE
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.muhdo.app.R
import com.muhdo.app.adapter.FavoriteAdapter
import com.muhdo.app.apiModel.mealModel.FavoriteListModel
import com.muhdo.app.databinding.ActivityFavoriteBinding
import com.muhdo.app.interfaces.ItemClickListener
import com.muhdo.app.repository.ApiUtilis
import com.muhdo.app.repository.ErrorModel
import com.muhdo.app.repository.NetworkManager
import com.muhdo.app.repository.ServiceListener
import com.muhdo.app.utils.v3.Constants
import com.timingsystemkotlin.backuptimingsystem.Utility


class MyFavouriteActivity : AppCompatActivity(), ItemClickListener {
    override fun onClick(position: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    lateinit var binding: ActivityFavoriteBinding
    private lateinit var favoriteAdapter: FavoriteAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding =
            DataBindingUtil.setContentView(this@MyFavouriteActivity, R.layout.activity_favorite)
        getFaouriteList()

        binding.btnBack.setOnClickListener {
            /*val i = Intent(this@MyFavouriteActivity, MealActivity::class.java)
            i.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(i)*/
            onBackPressed()
        }
    }

    fun goToRecipe(recipeID: String) {
        val i = Intent(applicationContext, RecipeActivity::class.java)
        i.putExtra(Constants.RECIPE_ID, recipeID)
        startActivity(i)
        finish()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        /*val i = Intent(this@MyFavouriteActivity, MealActivity::class.java)
        i.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(i)*/
    }


    private fun getFaouriteList() {
        binding.progressBar.visibility = VISIBLE
        val manager = NetworkManager()
        if (manager.isConnectingToInternet(applicationContext)) {
            manager.createApiRequest(
                ApiUtilis.getAPIInstance(applicationContext).getFouriteList(Utility.getUserID(applicationContext)),
                object : ServiceListener<FavoriteListModel> {
                    override fun getServerResponse(response: FavoriteListModel, requestcode: Int) {
                        binding.progressBar.visibility = View.GONE
                        setData(response)
                    }

                    override fun getError(error: ErrorModel, requestcode: Int) {
                        binding.progressBar.visibility = View.GONE
                        Utility.displayShortSnackBar(
                            binding.parentLayout,
                            error.getMessage().toString()
                        )
                    }
                })
        } else {
            binding.progressBar.visibility = View.GONE
            Utility.displayShortSnackBar(
                binding.parentLayout,
                resources.getString(R.string.check_internet)
            )
        }
    }

    private fun setData(response: FavoriteListModel) {
        if (response.getData()!!.isEmpty()) {
            binding.txtMessage.visibility = VISIBLE
        } else {
            binding.txtMessage.visibility = View.GONE
        }
        favoriteAdapter = FavoriteAdapter(this@MyFavouriteActivity, response.getData()!!)

        val mLayoutManager = GridLayoutManager(this, 2)
        binding.favouriteList.layoutManager = mLayoutManager
        binding.favouriteList.addItemDecoration(GridSpacingItemDecoration(2, dpToPx(10), true))
        binding.favouriteList.itemAnimator = DefaultItemAnimator()
        binding.favouriteList.adapter = favoriteAdapter
    }


    inner class GridSpacingItemDecoration(
        private val spanCount: Int,
        private val spacing: Int,
        private val includeEdge: Boolean
    ) : RecyclerView.ItemDecoration() {

        override fun getItemOffsets(
            outRect: Rect,
            view: View,
            parent: RecyclerView,
            state: RecyclerView.State
        ) {
            val position = parent.getChildAdapterPosition(view) // item position
            val column = position % spanCount // item column

            if (includeEdge) {
                outRect.left =
                    spacing - column * spacing / spanCount // spacing - column * ((1f / spanCount) * spacing)
                outRect.right =
                    (column + 1) * spacing / spanCount // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing
                }
                outRect.bottom = spacing // item bottom
            } else {
                outRect.left = column * spacing / spanCount // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing /
                        spanCount // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing // item top
                }
            }
        }
    }

    /**
     * Converting dp to pixel
     */
    private fun dpToPx(dp: Int): Int {
        val r = resources
        return Math.round(
            TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP,
                dp.toFloat(),
                r.displayMetrics
            )
        )
    }

}