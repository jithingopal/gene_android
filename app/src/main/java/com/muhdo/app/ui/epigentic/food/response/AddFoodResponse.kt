package com.muhdo.app.ui.epigentic.food.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.muhdo.app.ui.epigentic.food.model.FoodInfoModel

class AddFoodResponse {

    @SerializedName("code")
    @Expose
    private var code: Int? = null
    @SerializedName("message")
    @Expose
    private var message: String? = null
    @SerializedName("data")
    private var data: FoodInfoModel? = null



    fun getCode(): Int? {
        return code
    }

    fun setCode(code: Int?) {
        this.code = code
    }

    fun getMessage(): String? {
        return message
    }

    fun setMessage(message: String) {
        this.message = message
    }


    fun getData(): FoodInfoModel? {
        return data
    }

    fun setData(data: FoodInfoModel) {
        this.data = data
    }

}