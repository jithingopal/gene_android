package com.muhdo.app.ui.epigentic.supplyments.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.muhdo.app.ui.epigentic.supplyments.model.SupplymentInfo
import java.io.Serializable

class SupplementResponse : Serializable {
    @SerializedName("code")
    @Expose
    private var code: Int? = null
    @SerializedName("message")
    @Expose
    private var message: String? = null
    @SerializedName("data")
    @Expose
    private var data:SupplymentInfo? = null

    fun getCode(): Int? {
        return code
    }

    fun setCode(code: Int?) {
        this.code = code
    }

    fun getMessage(): String? {
        return message
    }

    fun setMessage(message: String) {
        this.message = message
    }

    fun getData():SupplymentInfo? {
        return data
    }

    fun setData(data: SupplymentInfo) {
        this.data = data
    }
}