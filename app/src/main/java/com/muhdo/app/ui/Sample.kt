package com.muhdo.app.ui

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.muhdo.app.R
import com.muhdo.app.databinding.SampleBinding

class Sample : AppCompatActivity(){


    lateinit var binding: SampleBinding
    private lateinit var handler: Handler
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            val w = window // in Activity's onCreate() for instance
            w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS)
        }

       binding = DataBindingUtil.setContentView(this@Sample, R.layout.sample)


        handler = Handler()
        handler.postDelayed({
            // for autoLogin user

                startActivity(Intent(this@Sample, MealActivity::class.java))
                finish()

        }, 3000)

    }
    }