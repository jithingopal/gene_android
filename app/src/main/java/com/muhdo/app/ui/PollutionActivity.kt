package com.muhdo.app.ui

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import com.github.mikephil.charting.animation.Easing
import com.github.mikephil.charting.components.YAxis
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.formatter.IFillFormatter
import com.github.mikephil.charting.utils.Utils
import com.google.gson.Gson
import com.muhdo.app.R
import com.muhdo.app.apiModel.pollution.AirForecastData
import com.muhdo.app.apiModel.pollution.AirForecastModel
import com.muhdo.app.apiModel.pollution.AirIndexData
import com.muhdo.app.databinding.ActivityPollutionBinding
import com.muhdo.app.repository.ApiUtilis
import com.muhdo.app.repository.ErrorModel
import com.muhdo.app.repository.NetworkManager
import com.muhdo.app.repository.ServiceListener
import com.timingsystemkotlin.backuptimingsystem.Utility
import java.util.*

class PollutionActivity : AppCompatActivity() {
    lateinit var binding: ActivityPollutionBinding
    var response: AirIndexData? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding =
            DataBindingUtil.setContentView(this@PollutionActivity, R.layout.activity_pollution)
        val gson = Gson()

        val data = intent.getStringExtra("PollutionData")
        response = gson.fromJson<AirIndexData>(data, AirIndexData::class.java)

 if (Utility.getLat(applicationContext).isNullOrEmpty() && Utility.getLong(applicationContext).isNullOrEmpty()) {
  Toast.makeText(applicationContext, "Unable to fetch location", Toast.LENGTH_SHORT).show()
        } else {
            getForecast(Utility.getLong(applicationContext)!!, Utility.getLat(applicationContext)!!)
        }

        binding.btnBack.setOnClickListener {
            val i = Intent(applicationContext, DashboardActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(i)
            finish()
        }

    }

    private fun getForecast(long: String, lat: String) {

        binding.progressBar.visibility = View.VISIBLE

        val params = HashMap<String, String>()
        params["latlong"] = "$lat, $long"


        val manager = NetworkManager()
        if (manager.isConnectingToInternet(this@PollutionActivity)) {
            manager.createApiRequest(
                ApiUtilis.getAPIInstance(applicationContext).getForeCast(params),
                object : ServiceListener<AirForecastModel> {
                    @RequiresApi(Build.VERSION_CODES.HONEYCOMB)
                    override fun getServerResponse(response: AirForecastModel, requestcode: Int) {
                        binding.progressBar.visibility = View.GONE
                        setData(response.getData()!!)
                    }

                    override fun getError(error: ErrorModel, requestcode: Int) {

                        binding.progressBar.visibility = View.GONE
                        Utility.displayShortSnackBar(
                            binding.parentLayout,
                            error.getMessage()!!
                        )


                    }
                })
        } else {
            binding.progressBar.visibility = View.GONE
            Utility.displayShortSnackBar(
                binding.parentLayout,
                resources.getString(R.string.check_internet)
            )

        }
    }


    @SuppressLint("ObsoleteSdkInt")
    @RequiresApi(Build.VERSION_CODES.HONEYCOMB)
    private fun setData(data: List<AirForecastData>) {

        binding.txtPollution.text = response!!.getCategory()
        binding.txtInformation.text = response!!.getDailyMessage()
        binding.txtHealthImplications.text = response!!.getHealthImplications()
        binding.txtSuggestions.text = response!!.getSuggestions()


        binding.chart.axisLeft.setDrawLabels(false)
        binding.chart.axisRight.setDrawLabels(false)
        binding.chart.xAxis.setDrawLabels(true)
        binding.chart.legend.isEnabled = false
        binding.chart.setGridBackgroundColor(Color.parseColor("#bdbdbd"))
        binding.chart.setGridBackgroundColor(Color.parseColor("#0f0ff0"))
        binding.chart.setViewPortOffsets(0f, 0f, 10f, 0f)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            binding.chart.animateY(2000, Easing.EaseInCubic)
        }
        //bat
        binding.chart.description.isEnabled = false
        binding.chart.setTouchEnabled(false)
        binding.chart.isDragEnabled = true
        binding.chart.setScaleEnabled(true)
        binding.chart.setPinchZoom(true)
        binding.chart.setDrawGridBackground(false)
        binding.chart.maxHighlightDistance = 300f
        val x = binding.chart.xAxis
        x.isEnabled = false
        val y = binding.chart.axisLeft
        y.setLabelCount(6, false)
        y.textColor = Color.WHITE
        y.setPosition(YAxis.YAxisLabelPosition.INSIDE_CHART)
        y.setDrawGridLines(false)
        y.axisLineColor = Color.WHITE
        binding.chart.axisRight.isEnabled = false
        binding.chart.legend.isEnabled = false
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            binding.chart.animateXY(200, 200)
        }
        binding.chart.invalidate()
        setPieData(data)


    }


    private fun setPieData(data: List<AirForecastData>) {
        val values = ArrayList<Entry>()


        if (data.size == 4) {
            values.add(Entry(10f, (data[0].getAqi()!! - 20).toFloat()))
            values.add(Entry(20f, data[0].getAqi()!!.toFloat()))
            values.add(Entry(30f, data[1].getAqi()!!.toFloat()))
            values.add(Entry(40f, data[2].getAqi()!!.toFloat()))
            values.add(Entry(50f, data[3].getAqi()!!.toFloat()))
            values.add(Entry(60f, (data[3].getAqi()!! - 20).toFloat()))
            binding.txtDay1.text = data[0].getDay()
            binding.txtDay2.text = data[1].getDay()
            binding.txtDay3.text = data[2].getDay()
            binding.txtDay4.text = data[3].getDay()
        }

        val set1: LineDataSet

        if (binding.chart.data != null && binding.chart.data.dataSetCount > 0) {
            set1 = binding.chart.data.getDataSetByIndex(0) as LineDataSet
            set1.values = values
            binding.chart.data.notifyDataChanged()
            binding.chart.notifyDataSetChanged()
        } else {
            // create a dataset and give it a type
            set1 = LineDataSet(values, "DataSet 1")

            set1.mode = LineDataSet.Mode.CUBIC_BEZIER
            set1.cubicIntensity = 0.2f
            set1.setDrawFilled(true)
            set1.setDrawCircles(true)
            set1.lineWidth = 1.8f
            set1.circleRadius = 4f
            set1.setCircleColor(Color.WHITE)
            set1.highLightColor = Color.rgb(244, 117, 117)
            set1.color = Color.WHITE
            set1.fillColor = Color.WHITE
            set1.fillAlpha = 100
            set1.setDrawHorizontalHighlightIndicator(false)
            set1.fillFormatter = IFillFormatter { _, _ -> binding.chart.axisLeft.axisMinimum }


            // set the filled area
            set1.setDrawFilled(true)
            set1.fillFormatter = IFillFormatter { _, _ -> binding.chart.axisLeft.axisMinimum }

            // set color of filled area
            if (Utils.getSDKInt() >= 18) {
                // drawables only supported on api level 18 and above
                val drawable = ContextCompat.getDrawable(this, R.drawable.fade_red)
                set1.fillDrawable = drawable
            } else {
                set1.fillColor = Color.BLACK
            }
            val data = LineData(set1)

            data.setValueTextSize(9f)
            data.setDrawValues(true)

            // set data
            binding.chart.data = data!!
            val xAxisLabel = ArrayList<String>()
            xAxisLabel.add("Mon")
            xAxisLabel.add("Tue")
            xAxisLabel.add("Wed")
            xAxisLabel.add("Thu")
            xAxisLabel.add("Fri")
            xAxisLabel.add("Sat")

            val xAxis = binding.chart.xAxis
            /*xAxis.setValueFormatter { value, _ ->
               println("DATA====> " + xAxisLabel[value.toInt()])
               xAxisLabel[value.toInt()]
           }*/
        }

    }
}