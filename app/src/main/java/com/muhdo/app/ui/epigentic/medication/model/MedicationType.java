package com.muhdo.app.ui.epigentic.medication.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MedicationType {

    @SerializedName("_id")
    @Expose
    private String drugsId;

    @SerializedName("drugs")
    @Expose
    private String drugsName;

    @SerializedName("dosage")
    @Expose
    private String dosage;


    public MedicationType(String drugsId, String drugsName, String dosage) {
        this.drugsId = drugsId;
        this.drugsName = drugsName;
        this.dosage = dosage;
    }


    public String getDrugsId() {
        return drugsId;
    }

    public void setDrugsId(String drugsId) {
        this.drugsId = drugsId;
    }

    public String getDrugsName() {
        return drugsName;
    }

    public void setDrugsName(String drugsName) {
        this.drugsName = drugsName;
    }

    public String getDosage() {
        return dosage;
    }

    public void setDosage(String dosage) {
        this.dosage = dosage;
    }
}

