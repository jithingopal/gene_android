package com.muhdo.app.ui

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import androidx.viewpager.widget.ViewPager
import com.bumptech.glide.Glide
import com.muhdo.app.R
import com.muhdo.app.apiModel.keyData.GenoTypeSectionModel
import com.muhdo.app.databinding.ActivityResultsBinding
import com.muhdo.app.fragment.KeyDataFragment
import com.muhdo.app.fragment.KeyDataFragment2
import com.muhdo.app.fragment.ResultKeyDataFragment
import com.muhdo.app.repository.ApiUtilis
import com.muhdo.app.repository.ErrorModel
import com.muhdo.app.repository.NetworkManager
import com.muhdo.app.repository.ServiceListener
import com.muhdo.app.utils.Events
import com.muhdo.app.utils.GlobalBus
import com.muhdo.app.utils.PreferenceConnector
import com.muhdo.app.utils.v3.Constants
import com.timingsystemkotlin.backuptimingsystem.Utility
import org.greenrobot.eventbus.Subscribe
import org.json.JSONArray
import org.json.JSONObject


class ResultsActivity : AppCompatActivity() {
    lateinit var binding: ActivityResultsBinding
    private var genoTypeId: String = ""
    private var modeTitle: String = ""
    var context = this;

    var count: Int = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this@ResultsActivity, R.layout.activity_results)

        getGenoTypeSection()

        genoTypeId = intent.getStringExtra(Constants.MODE_ID)
        modeTitle = intent.getStringExtra(Constants.MODE_TITLE)
        binding.btnBack.setOnClickListener {
            /*val intent = Intent(this, DashboardActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)*/
            finish()
        }

        binding.viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageSelected(position: Int) {
                Log.d("data", "tab postion = " + position)
                PreferenceConnector.writeInt(context, PreferenceConnector.TAB_POS, position)

            }

            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {

            }

            override fun onPageScrollStateChanged(state: Int) {

            }

        })
    }


    override fun onStart() {
        super.onStart()
        // Register this fragment to listen to event.
        GlobalBus.getBus()!!.register(this)
    }


    override fun onStop() {
        super.onStop()
        GlobalBus.getBus()!!.unregister(this)
    }

    @Subscribe
    fun getMessage(fragmentActivityMessage: Events.FragmentActivityMessage) {
        for (key in Utility.sectionList.keys) {
            if (Utility.sectionList[key] == fragmentActivityMessage.message2) {
                binding.viewPager.setCurrentItem(key + 1, true)
                Utility.categoryID = fragmentActivityMessage.message1
            }
            println("Element at key $key = ${Utility.sectionList[key]}")
        }
    }

    @SuppressLint("ResourceAsColor", "InflateParams")
    private fun setupTabIcons(data: String) {
        val fragmentAdapter = ViewPagerAdapter(supportFragmentManager)
        binding.viewPager.adapter = fragmentAdapter
        binding.userTabLayout.setupWithViewPager(binding.viewPager)
        val json = JSONObject(data)
        val arr = JSONArray(json.getString("data"))
        val adapter = ViewPagerAdapter(supportFragmentManager)
        adapter.addFrag(KeyDataFragment(), resources.getString(R.string.results_key_data))
        binding.viewPager.adapter = adapter
        for (i in 0 until arr.length()) {
            val j = JSONObject(arr.get(i).toString())
            adapter.addFrag(ResultKeyDataFragment(), j.getString("title"))
        }
        binding.viewPager.adapter = adapter

        val tabView = LayoutInflater.from(this).inflate(R.layout.custom_tab, null)
        val tabLabel = tabView.findViewById(R.id.tab) as TextView
        val tabImage = tabView.findViewById(R.id.tab_img) as ImageView
        tabLabel.text = resources.getString(R.string.results_key_data)
        tabImage.setImageResource(R.drawable.key_data_icon)
        binding.userTabLayout.getTabAt(0)!!.customView = tabView

        for (i in 0 until arr.length()) {
            count = arr.length()
            val j = JSONObject(arr.get(i).toString())
            val tabView = LayoutInflater.from(this).inflate(R.layout.custom_tab, null)
            val tabLabel = tabView.findViewById(R.id.tab) as TextView
            val tabImage = tabView.findViewById(R.id.tab_img) as ImageView
            tabLabel.text = j.getString("title_mobile")
            Glide
                .with(applicationContext)
                .load("https://s3.amazonaws.com/" + j.getString("bucket") + "/" + j.getString("icon"))
                .centerCrop()
                .into(tabImage)
            binding.userTabLayout.getTabAt(i + 1)!!.customView = tabView
            Utility.sectionList[i] = j.getString("_id")
        }
    }

    internal inner class ViewPagerAdapter(manager: FragmentManager) :
        FragmentStatePagerAdapter(manager) {
        private val mFragmentList = arrayListOf<Fragment>()
        private val mFragmentTitleList = arrayListOf<String>()

        override fun getItem(position: Int): Fragment {

            when (position) {
                0 -> {
                    val bundle = Bundle()
                    bundle.putString(Constants.MODE_ID, genoTypeId)
                    bundle.putString(Constants.MODE_TITLE, modeTitle)
                    val keyDataFragment = KeyDataFragment()
                    keyDataFragment.arguments = bundle
                    return keyDataFragment
                }
                else -> {

                    val bundle = Bundle()
                    bundle.putString(Constants.SECTION_ID, Utility.sectionList[position - 1])
                    bundle.putString(Constants.MODE_ID, genoTypeId)
                    bundle.putString(Constants.MODE_TITLE, modeTitle)
                    val resultKeyDataFragment = KeyDataFragment2()
                    resultKeyDataFragment.arguments = bundle
                    return resultKeyDataFragment
                }
            }
        }

        override fun getCount(): Int {
            return mFragmentList.size
        }

        fun addFrag(fragment: Fragment, title: String) {
            mFragmentList.add(fragment)
            mFragmentTitleList.add(title)
        }

        override fun getPageTitle(position: Int): CharSequence? {
            Log.d("data", "tab pos = " + position)
            return mFragmentTitleList[position]
        }
    }

    private fun getGenoTypeSection() {
        binding.progressBar.visibility = View.VISIBLE
        val manager = NetworkManager()
        if (manager.isConnectingToInternet(applicationContext)) {
            manager.createApiRequest(
                ApiUtilis.getAPIInstance(applicationContext).getGenoTypeSection(),
                object : ServiceListener<GenoTypeSectionModel> {
                    override fun getServerResponse(
                        response: GenoTypeSectionModel,
                        requestcode: Int
                    ) {
                        binding.progressBar.visibility = View.GONE
                        if (response.getStatusCode() == 200 && response.getBody() != null) {
                            setupTabIcons(response.getBody()!!)
                        } else {
                            // displayAlert()
                        }
                    }

                    override fun getError(error: ErrorModel, requestcode: Int) {
                        binding.progressBar.visibility = View.GONE
                    }
                })
        } else {
            binding.progressBar.visibility = View.GONE
            Utility.displayShortSnackBar(
                binding.parentLayout,
                resources.getString(R.string.check_internet)
            )
        }
    }
}