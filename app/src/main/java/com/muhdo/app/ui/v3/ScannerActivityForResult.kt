package com.muhdo.app.ui.v3

import android.Manifest
import android.app.AlertDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat.checkSelfPermission
import androidx.databinding.DataBindingUtil
import com.facebook.appevents.AppEventsLogger
import com.google.zxing.Result
import com.muhdo.app.R
import com.muhdo.app.apiModel.v3.AddChildKitIdRequest
import com.muhdo.app.apiModel.v3.BaseResponse
import com.muhdo.app.databinding.V3ActivityScannerForResultBinding
import com.muhdo.app.repository.ApiUtilis
import com.muhdo.app.repository.NetworkManager2
import com.muhdo.app.utils.v3.TempUtil
import com.muhdo.app.utils.v3.showAlertDialog
import com.timingsystemkotlin.backuptimingsystem.Utility
import me.dm7.barcodescanner.zxing.ZXingScannerView
import org.jetbrains.anko.toast
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ScannerActivityForResult : AppCompatActivity(), ZXingScannerView.ResultHandler {
    lateinit var binding: V3ActivityScannerForResultBinding
    private val FLASH_STATE = "FLASH_STATE"

    private var mScannerView: ZXingScannerView? = null
    private var mFlash: Boolean = false

    @Suppress("DEPRECATED_IDENTITY_EQUALS")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AppEventsLogger.activateApp(this)
        binding = DataBindingUtil.setContentView(
            this@ScannerActivityForResult,
            R.layout.v3_activity_scanner_for_result
        )


        if (checkSelfPermission(
                this,
                Manifest.permission.CAMERA
            ) !== PackageManager.PERMISSION_GRANTED
        ) {
            run {
                Toast.makeText(
                    this,
                    "Please grant camera permission to scan the kit",
                    Toast.LENGTH_SHORT
                )
                    .show()
                val intent = Intent(Intent.ACTION_MAIN)
                intent.addCategory(Intent.CATEGORY_HOME)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(intent)
            }
        }

        mScannerView = ZXingScannerView(this)
        binding.contentFrame.addView(mScannerView)
        binding.btnEnterKitManually.setOnClickListener {
            binding.layoutScanner.visibility = GONE
            binding.layoutEnterManually.visibility = VISIBLE
            mScannerView!!.flash = false
            binding.btnToggleFlash.setText(R.string.v3_btn_flash_on)
            mScannerView!!.stopCamera()
            mFlash = false
        }
        binding.btnScanWithCamera.setOnClickListener {
            binding.edtKitId.isEnabled = true
            binding.edtKitId.setText("")
            binding.layoutScanner.visibility = VISIBLE
            binding.layoutEnterManually.visibility = GONE

            mScannerView!!.setResultHandler(this)
            mScannerView!!.startCamera()
            mScannerView!!.flash = mFlash

        }
        binding.btnBackButton.setOnClickListener {
            onBackPressed()
        }

        binding.btnSubmit.setOnClickListener {
            if (binding.edtKitId.text.toString() != null && binding.edtKitId.text.toString().isNotEmpty()) {
                addChildKitIdToServer()
            } else {
                toast(R.string.please_enter_kit_id)
            }
        }
    }

    private fun addChildKitIdToServer() {
        binding.progressBar.visibility = View.VISIBLE
       // var jsonParams: JSONObject = JSONObject()
       // jsonParams.put("user_id", Utility.getUserID(applicationContext))
        val requestBody = AddChildKitIdRequest(
            binding.edtKitId.text.toString(),
            Utility.getUserID(applicationContext)
        )
        val manager = NetworkManager2()
        if (manager.isConnectingToInternet(binding.parentLayout.context)) {
            val call =
                ApiUtilis.getAPIInstance(applicationContext)
                    .addChildKit(requestBody)
            call.enqueue(object : Callback<BaseResponse> {
                override fun onResponse(
                    call: Call<BaseResponse>,
                    response: Response<BaseResponse>
                ) {
                    try {
                        binding.progressBar.visibility = View.GONE
                        if (response.isSuccessful) {
                            var resultPojo: BaseResponse? = response.body()
                            TempUtil.log("Jithin", resultPojo.toString())
                            if (resultPojo != null && resultPojo!!.statusCode == 200) {
                                val builder = AlertDialog.Builder(this@ScannerActivityForResult)
                                builder.setMessage(resultPojo!!.message)
                                builder.setCancelable(false)
                                builder.setPositiveButton(R.string.btn_ok) { dialog, _ ->
                                    dialog.dismiss()
                                    finish()
                                }

                                val alert = builder.create()
                                alert.show()

                            }
                        } else {

                            this@ScannerActivityForResult.showAlertDialog(
                                JSONObject(response.errorBody()!!.string()).getString(
                                    "message"
                                )
                            )
                        }


                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }

                override fun onFailure(call: Call<BaseResponse>, t: Throwable) {

                    // Log error here since request failed
                    try {
                        if (binding.progressBar != null) binding.progressBar.visibility = View.GONE
                        Utility.displayShortSnackBar(
                            binding.parentLayout,
                            "Server connection error!!, please try again later."
                        )
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }


                }
            })
        } else {
            binding.progressBar.visibility = View.GONE

            Utility.displayShortSnackBar(
                binding.parentLayout,
                binding.parentLayout.context.resources.getString(R.string.check_internet)
            )

        }

    }


    override fun onResume() {
        super.onResume()
        mScannerView!!.setResultHandler(this)
        mScannerView!!.startCamera()
        mScannerView!!.flash = mFlash
    }

    override fun onPause() {
        super.onPause()
        mScannerView!!.stopCamera()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putBoolean(FLASH_STATE, mFlash)
    }

    override fun handleResult(rawResult: Result) {
        /*val i = Intent(applicationContext, KitRegistrationActivity::class.java)
        i.putExtra(Constants.KIT_ID_VALUE, rawResult.text.toString())
        startActivity(i)*/
        binding.layoutScanner.visibility = GONE
        binding.layoutEnterManually.visibility = VISIBLE
        binding.edtKitId.setText(rawResult.text.toString())
        binding.edtKitId.isEnabled = false
        binding.btnScanWithCamera.visibility = GONE

        // Note:
        // * Wait 2 seconds to resume the preview.
        // * On older devices continuously stopping and resuming camera preview can result in freezing the app.
        // * I don't know why this is the case but I don't have the time to figure out.
        val handler = Handler()
        handler.postDelayed(
            { mScannerView!!.resumeCameraPreview(this@ScannerActivityForResult) },
            2000
        )
    }

    fun toggleFlash(v: View) {
        mFlash = !mFlash
        mScannerView!!.flash = mFlash
        if (mFlash) {
            binding.btnToggleFlash.setText(R.string.v3_btn_flash_off)
        } else {
            binding.btnToggleFlash.setText(R.string.v3_btn_flash_on)
        }

    }


}