package com.muhdo.app.ui.epigenticResult

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Color
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Html
import android.util.Log
import com.muhdo.app.R
import com.muhdo.app.databinding.ActivityEpigenticHealthTrackingDetailBinding
import android.view.View
import androidx.cardview.widget.CardView
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import com.muhdo.app.ui.epigenticResult.model.EpigenticHealth
import com.google.gson.Gson
import com.muhdo.app.ui.epigenticResult.adapter.RecommInfoAdapter
import kotlinx.android.synthetic.main.activity_epigentic_health_tracking_detail.view.*


class EpigenticHealthTrackingDetailActivity : AppCompatActivity() {
    internal lateinit var binding: ActivityEpigenticHealthTrackingDetailBinding
    var count=0
    var   isUpIntro = true;
    var   isUpOutComes = true;
    var   isUpRecomm = true;
    var   isInfo = true;
    var adapter: RecommInfoAdapter? = null
    var context=this

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this@EpigenticHealthTrackingDetailActivity, R.layout.activity_epigentic_health_tracking_detail)
        getIntentData()
        binding.cardViewIntroductionKey.setOnClickListener(object : View.OnClickListener{
            override fun onClick(v: View?) {
                /*binding.cardViewIntroductionKey.setBackgroundColor(Color.parseColor("#6D91AE"))
                binding.cardViewOutComesKey.setBackgroundColor(Color.parseColor("#B2C1CA"))
                binding.cardViewRecommendationKey.setBackgroundColor(Color.parseColor("#B2C1CA"))*/

                if (isUpIntro) {

                    slideDown(binding.cardViewIntroductionValue,binding.cardViewIntroductionKey);
                    //myButton.setText("Slide up");
                } else {

                    slideUp(binding.cardViewIntroductionValue,binding.cardViewIntroductionKey);
                   // myButton.setText("Slide down");
                }
                isUpIntro = !isUpIntro;

            }

        })


        binding.cardViewOutComesKey.setOnClickListener(object : View.OnClickListener{
            override fun onClick(v: View?) {

                /*binding.cardViewIntroductionKey.setBackgroundColor(Color.parseColor("#B2C1CA"))
                binding.cardViewOutComesKey.setBackgroundColor(Color.parseColor("#6D91AE"))
                binding.cardViewRecommendationKey.setBackgroundColor(Color.parseColor("#B2C1CA"))*/
                if (isUpOutComes) {

                    slideDown(binding.cardViewOutComesValue,binding.cardViewOutComesKey);
                    //myButton.setText("Slide up");
                } else {

                    slideUp(binding.cardViewOutComesValue,binding.cardViewOutComesKey);
                    // myButton.setText("Slide down");
                }
                isUpOutComes = !isUpOutComes;

            }

        })


        binding.cardViewRecommendationKey.setOnClickListener(object : View.OnClickListener{
            override fun onClick(v: View?) {

               /* binding.cardViewIntroductionKey.setBackgroundColor(Color.parseColor("#B2C1CA"))
                binding.cardViewOutComesKey.setBackgroundColor(Color.parseColor("#B2C1CA"))
                binding.cardViewRecommendationKey.setBackgroundColor(Color.parseColor("#6D91AE"))*/
                if (isUpRecomm) {

                    slideDown(binding.cardViewRecommendationValue,binding.cardViewRecommendationKey);
                    //myButton.setText("Slide up");
                } else {

                    slideUp(binding.cardViewRecommendationValue,binding.cardViewRecommendationKey);
                    // myButton.setText("Slide down");
                }
                isUpRecomm = !isUpRecomm;

            }

        })

        binding.btnBack.setOnClickListener(object :View.OnClickListener{
            override fun onClick(v: View?) {
                   finish()

            }

        })

    }

    private fun getIntentData() {
        val intent = getIntent();
        val trackData = intent.getStringExtra("track_key")
       // val obj: EpigResultResponse = intent.getSerializableExtra("resultObj") as EpigResultResponse
        Log.d("data","track data"+trackData)
        val gson = Gson()
        val gson1 = Gson()
        val gson2 = Gson()
        val gson3 = Gson()
        val gson4 = Gson()
        val gson5 = Gson()



        if(trackData.equals("track_eye_data")){
            val eyeHealthStr = getIntent().getStringExtra("object")
            val objEyeHealth = gson.fromJson<Any>(eyeHealthStr, EpigenticHealth::class.java)
            count = getIntent().getIntExtra("count",0)
            Log.d("data","count eye = "+count);
            showEyeHealthData(objEyeHealth as EpigenticHealth?)
        }

        else if(trackData.equals("track_hearing_data")){
            val hearingStr = getIntent().getStringExtra("object")
            val objHearing = gson.fromJson<Any>(hearingStr, EpigenticHealth::class.java)
            count = getIntent().getIntExtra("count",0)
            Log.d("data","count hear = "+count);
            showHearingData(objHearing as EpigenticHealth?)
        }

        else if(trackData.equals("track_memory_data")){
            val memoryStr = getIntent().getStringExtra("object")
            val objMemory = gson.fromJson<Any>(memoryStr, EpigenticHealth::class.java)
            count = getIntent().getIntExtra("count",0)
            Log.d("data","count memory = "+count);
            showMemoryData(objMemory as EpigenticHealth?)
        }

        else if(trackData.equals("track_inflammation_data")){
            val nflammationStr = getIntent().getStringExtra("object")
            val objInflammation = gson.fromJson<Any>(nflammationStr, EpigenticHealth::class.java)
            count = getIntent().getIntExtra("count",0)
            Log.d("data","count infla = "+count);
            showInflammationData(objInflammation as EpigenticHealth?)
        }

        else if(trackData.equals("track_age_data")){
            val ageStr = getIntent().getStringExtra("object")
            val objAge = gson.fromJson<Any>(ageStr, EpigenticHealth::class.java)
            count = getIntent().getIntExtra("count",0)
            Log.d("data","count age = "+count);
            showAgeData(objAge as EpigenticHealth?)
        }


        binding.llNext.setOnClickListener(object :View.OnClickListener{
            override fun onClick(v: View?) {
                if(count<=4){
                    Log.d("data","count = "+count)
                    count=count+1
                    if(count==5){
                        count=0
                    }

                    if(count==0){
                        val ageStr = getIntent().getStringExtra("objectAge")
                        val objAge = gson5.fromJson<Any>(ageStr, EpigenticHealth::class.java)
                        showAgeData(objAge as EpigenticHealth?)
                    }

                    if(count==1){
                        val eyeHealthStr = getIntent().getStringExtra("objectEye")
                        val objEyeHealth = gson1.fromJson<Any>(eyeHealthStr, EpigenticHealth::class.java)
                        showEyeHealthData(objEyeHealth as EpigenticHealth?)
                    }
                    if(count==2){
                        val hearingStr = getIntent().getStringExtra("objectHearing")
                        val objHearing = gson2.fromJson<Any>(hearingStr, EpigenticHealth::class.java)
                        showHearingData(objHearing as EpigenticHealth?)
                    }
                    if(count==3){
                        val hearingStr = getIntent().getStringExtra("objectMemory")
                        val objHearing = gson3.fromJson<Any>(hearingStr, EpigenticHealth::class.java)
                        showMemoryData(objHearing as EpigenticHealth?)
                    }
                    if(count==4){
                        val hearingStr = getIntent().getStringExtra("objectInfl")
                        val objHearing = gson4.fromJson<Any>(hearingStr, EpigenticHealth::class.java)
                        showInflammationData(objHearing as EpigenticHealth?)
                    }
                }
            }

        })

        binding.llPrevious.setOnClickListener(object :View.OnClickListener{
            override fun onClick(v: View?) {
                Log.d("data","count = "+count)
                if(count>=0){
                    count=count-1
                    if(count==-1){
                        count=4

                    }
                    Log.d("data","count click pre = "+count)


                    if(count==0){
                        val ageStr = getIntent().getStringExtra("objectAge")
                        val objAge = gson5.fromJson<Any>(ageStr, EpigenticHealth::class.java)
                        //count = getIntent().getIntExtra("count",0)
                        showAgeData(objAge as EpigenticHealth?)
                    }
                    if(count==1){
                        val eyeHealthStr = getIntent().getStringExtra("objectEye")
                        val objEyeHealth = gson1.fromJson<Any>(eyeHealthStr, EpigenticHealth::class.java)
                        showEyeHealthData(objEyeHealth as EpigenticHealth?)
                    }
                    if(count==2){
                        val hearingStr = getIntent().getStringExtra("objectHearing")
                        val objHearing = gson2.fromJson<Any>(hearingStr, EpigenticHealth::class.java)
                        showHearingData(objHearing as EpigenticHealth?)
                    }
                    if(count==3){
                        val hearingStr = getIntent().getStringExtra("objectMemory")
                        val objHearing = gson3.fromJson<Any>(hearingStr, EpigenticHealth::class.java)
                        showMemoryData(objHearing as EpigenticHealth?)
                    }
                    if(count==4){
                        val hearingStr = getIntent().getStringExtra("objectInfl")
                        val objHearing = gson4.fromJson<Any>(hearingStr, EpigenticHealth::class.java)
                        showInflammationData(objHearing as EpigenticHealth?)
                    }
                }
            }

        })


        binding.llInfoIcon.setOnClickListener(object :View.OnClickListener{
            override fun onClick(v: View?) {
                if (isInfo){

                    binding.llInfo.visibility=View.VISIBLE
                }else{
                    binding.llInfo.visibility=View.GONE
                }
                isInfo = !isInfo;

            }

        })
    }



    private fun showEyeHealthData(objEyeHealth: EpigenticHealth?) {
        binding.textViewToolbarTitle.setText(R.string.eyehealth)
        binding.imageViewTrackingIcon.setImageResource(R.mipmap.eye_health_b)
        binding.textViewTrackingTitle.setText(R.string.eye_score)


        if(!objEyeHealth!!.data!!.info.toString().equals("")){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                binding.textViewInfo.text = Html.fromHtml(objEyeHealth!!.data!!.info.toString(),Html.FROM_HTML_MODE_COMPACT)
            } else {
                binding.textViewInfo.text = Html.fromHtml(objEyeHealth!!.data!!.info.toString())
            }
        }

        if(!objEyeHealth!!.data!!.introduction.toString().equals("")){

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                binding.textViewIntroductionValue.text = Html.fromHtml(objEyeHealth!!.data!!.introduction.toString(),Html.FROM_HTML_MODE_COMPACT)
            } else {
                binding.textViewIntroductionValue.text = Html.fromHtml(objEyeHealth!!.data!!.introduction.toString())
            }
            //binding.textViewIntroductionValue.text= objEyeHealth!!.data!!.introduction.toString()
        }

        if(!objEyeHealth!!.data!!.outcome.toString().equals("")){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                binding.textViewOutComesValue.text = Html.fromHtml(objEyeHealth!!.data!!.outcome.toString(),Html.FROM_HTML_MODE_COMPACT)
            } else {
                binding.textViewOutComesValue.text = Html.fromHtml(objEyeHealth!!.data!!.outcome.toString())
            }
            //binding.textViewIntroductionValue.text= objEyeHealth!!.data!!.introduction.toString()
        }

        if(!objEyeHealth!!.data!!.recommendations.toString().equals("")){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                binding.textViewRecommendationValue.text = Html.fromHtml(objEyeHealth!!.data!!.recommendations.toString(),Html.FROM_HTML_MODE_COMPACT)
            } else {
                binding.textViewRecommendationValue.text = Html.fromHtml(objEyeHealth!!.data!!.recommendations.toString())
            }
            //binding.textViewIntroductionValue.text= objEyeHealth!!.data!!.introduction.toString()
        }

        if(!objEyeHealth.data!!.recommendationInfoList!!.isEmpty()){
            /*binding.rvRecomm.layoutManager = GridLayoutManager(this, 2)
            adapter = RecommInfoAdapter(this, this@EpigenticHealthTrackingDetailActivity, objEyeHealth.data!!.recommendationInfoList)
            binding.rvRecomm.adapter = adapter*/

            binding.llAge.visibility=View.GONE
            binding.llEye.visibility=View.VISIBLE
            binding.llHearing.visibility=View.GONE
            binding.llMemory.visibility=View.GONE
            binding.llInflamation.visibility=View.GONE

            binding.imageView1REye.setImageResource(R.mipmap.eye1un)
            binding.imageView2REye.setImageResource(R.mipmap.eye2un)
            //binding.imageView3REye.setImageResource(R.mipmap.eye3un)
            binding.imageView4REye.setImageResource(R.mipmap.antioxidant_unsel)
            binding.imageView5REye.setImageResource(R.mipmap.eye4un)
            binding.imageView6REye.setImageResource(R.mipmap.eye5un)


            binding.textView1REye.text=objEyeHealth.data!!.recommendationInfoList!!.get(0).title
            binding.textView2REye.text=objEyeHealth.data!!.recommendationInfoList!!.get(1).title
            binding.textView3REye.text=objEyeHealth.data!!.recommendationInfoList!!.get(2).title
            binding.textView4REye.text=objEyeHealth.data!!.recommendationInfoList!!.get(3).title
            binding.textView5REye.text=objEyeHealth.data!!.recommendationInfoList!!.get(4).title
            binding.textView6REye.text=objEyeHealth.data!!.recommendationInfoList!!.get(5).title

            binding.optionAEye.setOnClickListener(object :View.OnClickListener{
                override fun onClick(v: View?) {

                    binding.optionAEye.view1Eye.background = ActivityCompat.getDrawable(applicationContext, R.drawable.tracking_op_bg_sel)
                    binding.optionBEye.view2_Eye.background = ActivityCompat.getDrawable(applicationContext, R.drawable.tracking_op_bg_unsel)
                    binding.optionCEye.view3_Eye.background = ActivityCompat.getDrawable(applicationContext, R.drawable.tracking_op_bg_unsel)
                    binding.optionDEye.view4_Eye.background = ActivityCompat.getDrawable(applicationContext, R.drawable.tracking_op_bg_unsel)
                    binding.optionEEye.view5_Eye.background = ActivityCompat.getDrawable(applicationContext, R.drawable.tracking_op_bg_unsel)
                    binding.optionFEye.view6_Eye.background = ActivityCompat.getDrawable(applicationContext, R.drawable.tracking_op_bg_unsel)

                    binding.imageView1REye.setImageResource(R.mipmap.eye1sel)
                    binding.imageView2REye.setImageResource(R.mipmap.eye2un)
                    //binding.imageView3REye.setImageResource(R.mipmap.eye3un)
                    binding.imageView4REye.setImageResource(R.mipmap.antioxidant_unsel)
                    binding.imageView5REye.setImageResource(R.mipmap.eye4un)
                    binding.imageView6REye.setImageResource(R.mipmap.eye5un)
                    try{
                        val i = Intent(context, RecommDetailActivity::class.java)
                        i.putExtra("object", objEyeHealth.data!!.recommendationInfoList!!.get(0).text)
                        i.putExtra("title", objEyeHealth.data!!.recommendationInfoList!!.get(0).title)
                        startActivity(i)
                    }catch (e:Exception){
                        Log.d("data","exp "+e.printStackTrace())
                    }

                }
            })

            binding.optionBEye.setOnClickListener(object :View.OnClickListener{
                override fun onClick(v: View?) {

                    binding.optionAEye.view1Eye.background = ActivityCompat.getDrawable(applicationContext, R.drawable.tracking_op_bg_unsel)
                    binding.optionBEye.view2_Eye.background = ActivityCompat.getDrawable(applicationContext, R.drawable.tracking_op_bg_sel)
                    binding.optionCEye.view3_Eye.background = ActivityCompat.getDrawable(applicationContext, R.drawable.tracking_op_bg_unsel)
                    binding.optionDEye.view4_Eye.background = ActivityCompat.getDrawable(applicationContext, R.drawable.tracking_op_bg_unsel)
                    binding.optionEEye.view5_Eye.background = ActivityCompat.getDrawable(applicationContext, R.drawable.tracking_op_bg_unsel)
                    binding.optionFEye.view6_Eye.background = ActivityCompat.getDrawable(applicationContext, R.drawable.tracking_op_bg_unsel)

                    binding.imageView1REye.setImageResource(R.mipmap.eye1un)
                    binding.imageView2REye.setImageResource(R.mipmap.eye2sel)
                    //binding.imageView3REye.setImageResource(R.mipmap.eye3un)
                    binding.imageView4REye.setImageResource(R.mipmap.antioxidant_unsel)
                    binding.imageView5REye.setImageResource(R.mipmap.eye4un)
                    binding.imageView6REye.setImageResource(R.mipmap.eye5un)

                    try{
                        val i = Intent(context, RecommDetailActivity::class.java)
                        i.putExtra("object", objEyeHealth.data!!.recommendationInfoList!!.get(1).text)
                        i.putExtra("title", objEyeHealth.data!!.recommendationInfoList!!.get(1).title)
                        startActivity(i)
                    }catch (e: Exception){

                    }

                }
            })

            binding.optionCEye.setOnClickListener(object :View.OnClickListener{
                override fun onClick(v: View?) {

                    binding.optionAEye.view1Eye.background = ActivityCompat.getDrawable(applicationContext, R.drawable.tracking_op_bg_unsel)
                    binding.optionBEye.view2_Eye.background = ActivityCompat.getDrawable(applicationContext, R.drawable.tracking_op_bg_unsel)
                    binding.optionCEye.view3_Eye.background = ActivityCompat.getDrawable(applicationContext, R.drawable.tracking_op_bg_sel)
                    binding.optionDEye.view4_Eye.background = ActivityCompat.getDrawable(applicationContext, R.drawable.tracking_op_bg_unsel)
                    binding.optionEEye.view5_Eye.background = ActivityCompat.getDrawable(applicationContext, R.drawable.tracking_op_bg_unsel)
                    binding.optionFEye.view6_Eye.background = ActivityCompat.getDrawable(applicationContext, R.drawable.tracking_op_bg_unsel)

                    binding.imageView1REye.setImageResource(R.mipmap.eye1un)
                    binding.imageView2REye.setImageResource(R.mipmap.eye2un)
                    //binding.imageView3REye.setImageResource(R.mipmap.eye3un)
                    binding.imageView4REye.setImageResource(R.mipmap.antioxidant_unsel)
                    binding.imageView5REye.setImageResource(R.mipmap.eye4un)
                    binding.imageView6REye.setImageResource(R.mipmap.eye5un)

                    try{
                        val i = Intent(context, RecommDetailActivity::class.java)
                        i.putExtra("object", objEyeHealth.data!!.recommendationInfoList!!.get(2).text)
                        i.putExtra("title", objEyeHealth.data!!.recommendationInfoList!!.get(2).title)
                        startActivity(i)
                    }catch (e:Exception){

                    }

                }
            })

            binding.optionDEye.setOnClickListener(object :View.OnClickListener{
                override fun onClick(v: View?) {

                    binding.optionAEye.view1Eye.background = ActivityCompat.getDrawable(applicationContext, R.drawable.tracking_op_bg_unsel)
                    binding.optionBEye.view2_Eye.background = ActivityCompat.getDrawable(applicationContext, R.drawable.tracking_op_bg_unsel)
                    binding.optionCEye.view3_Eye.background = ActivityCompat.getDrawable(applicationContext, R.drawable.tracking_op_bg_unsel)
                    binding.optionDEye.view4_Eye.background = ActivityCompat.getDrawable(applicationContext, R.drawable.tracking_op_bg_sel)
                    binding.optionEEye.view5_Eye.background = ActivityCompat.getDrawable(applicationContext, R.drawable.tracking_op_bg_unsel)
                    binding.optionFEye.view6_Eye.background = ActivityCompat.getDrawable(applicationContext, R.drawable.tracking_op_bg_unsel)

                    binding.imageView1REye.setImageResource(R.mipmap.eye1un)
                    binding.imageView2REye.setImageResource(R.mipmap.eye2un)
                    //binding.imageView3REye.setImageResource(R.mipmap.eye3un)
                    binding.imageView4REye.setImageResource(R.mipmap.antioxidant_sel)
                    binding.imageView5REye.setImageResource(R.mipmap.eye4un)
                    binding.imageView6REye.setImageResource(R.mipmap.eye5un)



                    try{
                        val i = Intent(context, RecommDetailActivity::class.java)
                        i.putExtra("object", objEyeHealth.data!!.recommendationInfoList!!.get(3).text)
                        i.putExtra("title", objEyeHealth.data!!.recommendationInfoList!!.get(3).title)
                        startActivity(i)
                    }catch (e:Exception){

                    }


                }
            })

            binding.optionEEye.setOnClickListener(object :View.OnClickListener{
                override fun onClick(v: View?) {

                    binding.optionAEye.view1Eye.background = ActivityCompat.getDrawable(applicationContext, R.drawable.tracking_op_bg_unsel)
                    binding.optionBEye.view2_Eye.background = ActivityCompat.getDrawable(applicationContext, R.drawable.tracking_op_bg_unsel)
                    binding.optionCEye.view3_Eye.background = ActivityCompat.getDrawable(applicationContext, R.drawable.tracking_op_bg_unsel)
                    binding.optionDEye.view4_Eye.background = ActivityCompat.getDrawable(applicationContext, R.drawable.tracking_op_bg_unsel)
                    binding.optionEEye.view5_Eye.background = ActivityCompat.getDrawable(applicationContext, R.drawable.tracking_op_bg_sel)
                    binding.optionFEye.view6_Eye.background = ActivityCompat.getDrawable(applicationContext, R.drawable.tracking_op_bg_unsel)

                    binding.imageView1REye.setImageResource(R.mipmap.eye1un)
                    binding.imageView2REye.setImageResource(R.mipmap.eye2un)
                    //binding.imageView3REye.setImageResource(R.mipmap.eye3un)
                    binding.imageView4REye.setImageResource(R.mipmap.antioxidant_unsel)
                    binding.imageView5REye.setImageResource(R.mipmap.eye4sel)
                    binding.imageView6REye.setImageResource(R.mipmap.eye5un)

                    val i = Intent(context, RecommDetailActivity::class.java)
                    i.putExtra("object", objEyeHealth.data!!.recommendationInfoList!!.get(4).text)
                    i.putExtra("title", objEyeHealth.data!!.recommendationInfoList!!.get(4).title)
                    startActivity(i)
                }
            })


            binding.optionFEye.setOnClickListener(object :View.OnClickListener{
                override fun onClick(v: View?) {

                    binding.optionAEye.view1Eye.background = ActivityCompat.getDrawable(applicationContext, R.drawable.tracking_op_bg_unsel)
                    binding.optionBEye.view2_Eye.background = ActivityCompat.getDrawable(applicationContext, R.drawable.tracking_op_bg_unsel)
                    binding.optionCEye.view3_Eye.background = ActivityCompat.getDrawable(applicationContext, R.drawable.tracking_op_bg_unsel)
                    binding.optionDEye.view4_Eye.background = ActivityCompat.getDrawable(applicationContext, R.drawable.tracking_op_bg_unsel)
                    binding.optionEEye.view5_Eye.background = ActivityCompat.getDrawable(applicationContext, R.drawable.tracking_op_bg_unsel)
                    binding.optionFEye.view6_Eye.background = ActivityCompat.getDrawable(applicationContext, R.drawable.tracking_op_bg_sel)

                    binding.imageView1REye.setImageResource(R.mipmap.eye1un)
                    binding.imageView2REye.setImageResource(R.mipmap.eye2un)
                    //binding.imageView3REye.setImageResource(R.mipmap.eye3un)
                    binding.imageView4REye.setImageResource(R.mipmap.antioxidant_unsel)
                    binding.imageView5REye.setImageResource(R.mipmap.eye4un)
                    binding.imageView6REye.setImageResource(R.mipmap.eye5sel)

                    val i = Intent(context, RecommDetailActivity::class.java)
                    i.putExtra("object", objEyeHealth.data!!.recommendationInfoList!!.get(5).text)
                    i.putExtra("title", objEyeHealth.data!!.recommendationInfoList!!.get(5).title)
                    startActivity(i)
                }
            })



        }



        binding.textViewTrackingScore.text=objEyeHealth!!.data!!.scoreData!!.score
        val sliderEyeHealth= objEyeHealth!!.data!!.scoreData!!.color
        if(!sliderEyeHealth!!.isEmpty()){
            if(sliderEyeHealth.equals("Yellow")){
                binding.imageViewTrackingScoreImage.setBackgroundResource(R.mipmap.slider_yellow)
                binding.imageViewEmoji.setBackgroundResource(R.mipmap.emoji_yellow)
                binding.textViewTrackingScore.setTextColor(Color.parseColor("#FEBF00"))
                binding.textViewTrackingScoreUnit.visibility=View.GONE
                //binding.textViewTrackingScoreUnit.setTextColor(Color.parseColor("#FEBF00"))
                //binding.textViewTrackingScoreUnit.setText("")
            }

            else if(sliderEyeHealth.equals("Red")){
                binding.imageViewTrackingScoreImage.setBackgroundResource(R.mipmap.slider_red)
                binding.imageViewEmoji.setBackgroundResource(R.mipmap.emoji_red)
                binding.textViewTrackingScore.setTextColor(Color.parseColor("#E5221E"))
                binding.textViewTrackingScoreUnit.visibility=View.GONE
                //binding.textViewTrackingScoreUnit.setTextColor(Color.parseColor("#E5221E"))
                //binding.textViewTrackingScoreUnit.setText("YRS")
            }

            else if(sliderEyeHealth.equals("Green")){
                binding.imageViewTrackingScoreImage.setBackgroundResource(R.mipmap.slider_green)
                binding.imageViewEmoji.setBackgroundResource(R.mipmap.emoji_green)
                binding.textViewTrackingScore.setTextColor(Color.parseColor("#8CC34B"))
                binding.textViewTrackingScoreUnit.visibility=View.GONE
                //binding.textViewTrackingScoreUnit.setTextColor(Color.parseColor("#8CC34B"))
                //binding.textViewTrackingScoreUnit.setText("YRS")
            }
        }

    }

    private fun showHearingData(objHearing: EpigenticHealth?) {
        binding.textViewToolbarTitle.setText(R.string.hearing_health)
        binding.imageViewTrackingIcon.setImageResource(R.mipmap.hearing_b)
        binding.textViewTrackingTitle.setText(R.string.hearing_score)

        if(!objHearing!!.data!!.info.toString().equals("")){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                binding.textViewInfo.text = Html.fromHtml(objHearing!!.data!!.info.toString(),Html.FROM_HTML_MODE_COMPACT)
            } else {
                binding.textViewInfo.text = Html.fromHtml(objHearing!!.data!!.info.toString())
            }
        }

        if(!objHearing!!.data!!.introduction.toString().equals("")){

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                binding.textViewIntroductionValue.text = Html.fromHtml(objHearing!!.data!!.introduction.toString(),Html.FROM_HTML_MODE_COMPACT)
            } else {
                binding.textViewIntroductionValue.text = Html.fromHtml(objHearing!!.data!!.introduction.toString())
            }

           // binding.textViewIntroductionValue.text= objHearing!!.data!!.introduction.toString()
        }

        if(!objHearing!!.data!!.outcome.toString().equals("")){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                binding.textViewOutComesValue.text = Html.fromHtml(objHearing!!.data!!.outcome.toString(),Html.FROM_HTML_MODE_COMPACT)
            } else {
                binding.textViewOutComesValue.text = Html.fromHtml(objHearing!!.data!!.outcome.toString())
            }
            //binding.textViewIntroductionValue.text= objEyeHealth!!.data!!.introduction.toString()
        }

        if(!objHearing!!.data!!.recommendations.toString().equals("")){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                binding.textViewRecommendationValue.text = Html.fromHtml(objHearing!!.data!!.recommendations.toString(),Html.FROM_HTML_MODE_COMPACT)
            } else {
                binding.textViewRecommendationValue.text = Html.fromHtml(objHearing!!.data!!.recommendations.toString())
            }
            //binding.textViewIntroductionValue.text= objEyeHealth!!.data!!.introduction.toString()
        }

        if(!objHearing.data!!.recommendationInfoList!!.isEmpty()){
            /*binding.rvRecomm.layoutManager = GridLayoutManager(this, 2)
            adapter = RecommInfoAdapter(this, this@EpigenticHealthTrackingDetailActivity, objEyeHealth.data!!.recommendationInfoList)
            binding.rvRecomm.adapter = adapter*/
            binding.llAge.visibility=View.GONE
            binding.llEye.visibility=View.GONE
            binding.llHearing.visibility=View.VISIBLE
            binding.llMemory.visibility=View.GONE
            binding.llInflamation.visibility=View.GONE

            binding.imageView1RHearing.setImageResource(R.mipmap.hear1un)
            binding.imageView2RHearing.setImageResource(R.mipmap.antioxidant_unsel)

            binding.textView1RHearing.text=objHearing.data!!.recommendationInfoList!!.get(0).title
            binding.textView2RHearing.text=objHearing.data!!.recommendationInfoList!!.get(1).title

            binding.optionAHearing.setOnClickListener(object :View.OnClickListener{
                override fun onClick(v: View?) {

                    binding.optionAHearing.view1_Hearing.background = ActivityCompat.getDrawable(applicationContext, R.drawable.tracking_op_bg_sel)
                    binding.optionBHearing.view2_Hearing.background = ActivityCompat.getDrawable(applicationContext, R.drawable.tracking_op_bg_unsel)

                    binding.imageView1RHearing.setImageResource(R.mipmap.hear1sel)
                    binding.imageView2RHearing.setImageResource(R.mipmap.antioxidant_unsel)

                    val i = Intent(context, RecommDetailActivity::class.java)
                    i.putExtra("object", objHearing.data!!.recommendationInfoList!!.get(0).text)
                    i.putExtra("title", objHearing.data!!.recommendationInfoList!!.get(0).title)
                    startActivity(i)
                }
            })

            binding.optionBHearing.setOnClickListener(object :View.OnClickListener{
                override fun onClick(v: View?) {

                    binding.optionAHearing.view1_Hearing.background = ActivityCompat.getDrawable(applicationContext, R.drawable.tracking_op_bg_unsel)
                    binding.optionBHearing.view2_Hearing.background = ActivityCompat.getDrawable(applicationContext, R.drawable.tracking_op_bg_sel)

                    binding.imageView1RHearing.setImageResource(R.mipmap.hear1un)
                    binding.imageView2RHearing.setImageResource(R.mipmap.antioxidant_sel)

                    val i = Intent(context, RecommDetailActivity::class.java)
                    i.putExtra("object", objHearing.data!!.recommendationInfoList!!.get(1).text)
                    i.putExtra("title", objHearing.data!!.recommendationInfoList!!.get(1).title)
                    startActivity(i)
                }
            })

        }


        binding.textViewTrackingScore.text=objHearing!!.data!!.scoreData!!.score

        val sliderHearing= objHearing!!.data!!.scoreData!!.color
        if(!sliderHearing!!.isEmpty()){
            if(sliderHearing.equals("Yellow")){
                binding.imageViewTrackingScoreImage.setBackgroundResource(R.mipmap.slider_yellow)
                binding.imageViewEmoji.setBackgroundResource(R.mipmap.emoji_yellow)
                binding.textViewTrackingScore.setTextColor(Color.parseColor("#FEBF00"))
                binding.textViewTrackingScoreUnit.visibility=View.GONE
                //binding.textViewTrackingScoreUnit.setTextColor(Color.parseColor("#FEBF00"))
               // binding.textViewTrackingScoreUnit.setText("YRS")

            }

            else if(sliderHearing.equals("Red")){
                binding.imageViewTrackingScoreImage.setBackgroundResource(R.mipmap.slider_red)
                binding.imageViewEmoji.setBackgroundResource(R.mipmap.emoji_red)
                binding.textViewTrackingScore.setTextColor(Color.parseColor("#E5221E"))
                binding.textViewTrackingScoreUnit.visibility=View.GONE
                //binding.textViewTrackingScoreUnit.setTextColor(Color.parseColor("#E5221E"))
                //binding.textViewTrackingScoreUnit.setText("YRS")
            }

            else if(sliderHearing.equals("Green")){
                binding.imageViewTrackingScoreImage.setBackgroundResource(R.mipmap.slider_green)
                binding.imageViewEmoji.setBackgroundResource(R.mipmap.emoji_green)
                binding.textViewTrackingScore.setTextColor(Color.parseColor("#8CC34B"))
                binding.textViewTrackingScoreUnit.visibility=View.GONE
                //binding.textViewTrackingScoreUnit.setTextColor(Color.parseColor("#8CC34B"))
                //binding.textViewTrackingScoreUnit.setText("YRS")
            }
        }
    }



    private fun showAgeData(objAge: EpigenticHealth?) {
        binding.textViewToolbarTitle.setText(R.string.age)
        binding.imageViewTrackingIcon.setImageResource(R.mipmap.eye_health_b)
        binding.textViewTrackingTitle.setText(R.string.age_score)

        if(!objAge!!.data!!.info.toString().equals("")){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                binding.textViewInfo.text = Html.fromHtml(objAge!!.data!!.info.toString(),Html.FROM_HTML_MODE_COMPACT)
            } else {
                binding.textViewInfo.text = Html.fromHtml(objAge!!.data!!.info.toString())
            }
        }

        if(!objAge!!.data!!.introduction.toString().equals("")){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                binding.textViewIntroductionValue.text = Html.fromHtml(objAge!!.data!!.introduction.toString(),Html.FROM_HTML_MODE_COMPACT)
            } else {
                binding.textViewIntroductionValue.text = Html.fromHtml(objAge!!.data!!.introduction.toString())
            }
        }


        if(!objAge!!.data!!.outcome.toString().equals("")){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                binding.textViewOutComesValue.text = Html.fromHtml(objAge!!.data!!.outcome.toString(),Html.FROM_HTML_MODE_COMPACT)
            } else {
                binding.textViewOutComesValue.text = Html.fromHtml(objAge!!.data!!.outcome.toString())
            }
            //binding.textViewIntroductionValue.text= objEyeHealth!!.data!!.introduction.toString()
        }

        if(!objAge!!.data!!.recommendations.toString().equals("")){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                binding.textViewRecommendationValue.text = Html.fromHtml(objAge!!.data!!.recommendations.toString(),Html.FROM_HTML_MODE_COMPACT)
            } else {
                binding.textViewRecommendationValue.text = Html.fromHtml(objAge!!.data!!.recommendations.toString())
            }
            //binding.textViewIntroductionValue.text= objEyeHealth!!.data!!.introduction.toString()
        }

        if(!objAge.data!!.recommendationInfoList!!.isEmpty()){
            /*binding.rvRecomm.layoutManager = GridLayoutManager(this, 2)
            adapter = RecommInfoAdapter(this, this@EpigenticHealthTrackingDetailActivity, objEyeHealth.data!!.recommendationInfoList)
            binding.rvRecomm.adapter = adapter*/
            binding.llAge.visibility=View.VISIBLE
            binding.llEye.visibility=View.GONE
            binding.llHearing.visibility=View.GONE
            binding.llMemory.visibility=View.GONE
            binding.llInflamation.visibility=View.GONE

            binding.imageView1Age.setImageResource(R.mipmap.v_d_unsel)
            binding.imageView2Age.setImageResource(R.mipmap.calorie_unsel)
            binding.imageView3Age.setImageResource(R.mipmap.metabolic_unsel)
            binding.imageView4Age.setImageResource(R.mipmap.antioxidant_unsel)
            binding.imageView5Age.setImageResource(R.mipmap.physical_unsel)

            binding.textView1RAge.text=objAge.data!!.recommendationInfoList!!.get(0).title
            binding.textView2RAge.text=objAge.data!!.recommendationInfoList!!.get(1).title
            binding.textView3RAge.text=objAge.data!!.recommendationInfoList!!.get(2).title
            binding.textView4RAge.text=objAge.data!!.recommendationInfoList!!.get(3).title
            binding.textView5RAge.text=objAge.data!!.recommendationInfoList!!.get(4).title

            binding.optionA.setOnClickListener(object :View.OnClickListener{
               override fun onClick(v: View?) {

                   binding.optionA.view1.background = ActivityCompat.getDrawable(applicationContext, R.drawable.tracking_op_bg_sel)
                   binding.optionB.view2.background = ActivityCompat.getDrawable(applicationContext, R.drawable.tracking_op_bg_unsel)
                   binding.optionC.view3.background = ActivityCompat.getDrawable(applicationContext, R.drawable.tracking_op_bg_unsel)
                   binding.optionD.view4.background = ActivityCompat.getDrawable(applicationContext, R.drawable.tracking_op_bg_unsel)
                   binding.optionE.view5.background = ActivityCompat.getDrawable(applicationContext, R.drawable.tracking_op_bg_unsel)

                   binding.imageView1Age.setImageResource(R.mipmap.v_d_select)
                   binding.imageView2Age.setImageResource(R.mipmap.calorie_unsel)
                   binding.imageView3Age.setImageResource(R.mipmap.metabolic_unsel)
                   binding.imageView4Age.setImageResource(R.mipmap.antioxidant_unsel)
                   binding.imageView5Age.setImageResource(R.mipmap.physical_unsel)



                   val i = Intent(context, RecommDetailActivity::class.java)
                   i.putExtra("object", objAge.data!!.recommendationInfoList!!.get(0).text)
                   i.putExtra("title", objAge.data!!.recommendationInfoList!!.get(0).title)
                   startActivity(i)
               }
           })


            binding.optionB.setOnClickListener(object :View.OnClickListener{
                override fun onClick(v: View?) {

                    binding.optionA.view1.background = ActivityCompat.getDrawable(applicationContext, R.drawable.tracking_op_bg_unsel)
                    binding.optionB.view2.background = ActivityCompat.getDrawable(applicationContext, R.drawable.tracking_op_bg_sel)
                    binding.optionC.view3.background = ActivityCompat.getDrawable(applicationContext, R.drawable.tracking_op_bg_unsel)
                    binding.optionD.view4.background = ActivityCompat.getDrawable(applicationContext, R.drawable.tracking_op_bg_unsel)
                    binding.optionE.view5.background = ActivityCompat.getDrawable(applicationContext, R.drawable.tracking_op_bg_unsel)

                    binding.imageView1Age.setImageResource(R.mipmap.v_d_unsel)
                    binding.imageView2Age.setImageResource(R.mipmap.calorie_sel)
                    binding.imageView3Age.setImageResource(R.mipmap.metabolic_unsel)
                    binding.imageView4Age.setImageResource(R.mipmap.antioxidant_unsel)
                    binding.imageView5Age.setImageResource(R.mipmap.physical_unsel)


                    val i = Intent(context, RecommDetailActivity::class.java)
                    i.putExtra("object", objAge.data!!.recommendationInfoList!!.get(1).text)
                    i.putExtra("title", objAge.data!!.recommendationInfoList!!.get(1).title)
                    startActivity(i)
                }
            })

            binding.optionC.setOnClickListener(object :View.OnClickListener{
                override fun onClick(v: View?) {

                    binding.optionA.view1.background = ActivityCompat.getDrawable(applicationContext, R.drawable.tracking_op_bg_unsel)
                    binding.optionB.view2.background = ActivityCompat.getDrawable(applicationContext, R.drawable.tracking_op_bg_unsel)
                    binding.optionC.view3.background = ActivityCompat.getDrawable(applicationContext, R.drawable.tracking_op_bg_sel)
                    binding.optionD.view4.background = ActivityCompat.getDrawable(applicationContext, R.drawable.tracking_op_bg_unsel)
                    binding.optionE.view5.background = ActivityCompat.getDrawable(applicationContext, R.drawable.tracking_op_bg_unsel)

                    binding.imageView1Age.setImageResource(R.mipmap.v_d_unsel)
                    binding.imageView2Age.setImageResource(R.mipmap.calorie_unsel)
                    binding.imageView3Age.setImageResource(R.mipmap.metabolic_sel)
                    binding.imageView4Age.setImageResource(R.mipmap.antioxidant_unsel)
                    binding.imageView5Age.setImageResource(R.mipmap.physical_unsel)

                    val i = Intent(context, RecommDetailActivity::class.java)
                    i.putExtra("object", objAge.data!!.recommendationInfoList!!.get(2).text)
                    i.putExtra("title", objAge.data!!.recommendationInfoList!!.get(2).title)
                    startActivity(i)
                }
            })

            binding.optionD.setOnClickListener(object :View.OnClickListener{
                override fun onClick(v: View?) {

                    binding.optionA.view1.background = ActivityCompat.getDrawable(applicationContext, R.drawable.tracking_op_bg_unsel)
                    binding.optionB.view2.background = ActivityCompat.getDrawable(applicationContext, R.drawable.tracking_op_bg_unsel)
                    binding.optionC.view3.background = ActivityCompat.getDrawable(applicationContext, R.drawable.tracking_op_bg_unsel)
                    binding.optionD.view4.background = ActivityCompat.getDrawable(applicationContext, R.drawable.tracking_op_bg_sel)
                    binding.optionE.view5.background = ActivityCompat.getDrawable(applicationContext, R.drawable.tracking_op_bg_unsel)

                    binding.imageView1Age.setImageResource(R.mipmap.v_d_unsel)
                    binding.imageView2Age.setImageResource(R.mipmap.calorie_unsel)
                    binding.imageView3Age.setImageResource(R.mipmap.metabolic_unsel)
                    binding.imageView4Age.setImageResource(R.mipmap.antioxidant_sel)
                    binding.imageView5Age.setImageResource(R.mipmap.physical_unsel)

                    val i = Intent(context, RecommDetailActivity::class.java)
                    i.putExtra("object", objAge.data!!.recommendationInfoList!!.get(3).text)
                    i.putExtra("title", objAge.data!!.recommendationInfoList!!.get(3).title)
                    startActivity(i)
                }
            })

            binding.optionE.setOnClickListener(object :View.OnClickListener{
                override fun onClick(v: View?) {

                    binding.optionA.view1.background = ActivityCompat.getDrawable(applicationContext, R.drawable.tracking_op_bg_unsel)
                    binding.optionB.view2.background = ActivityCompat.getDrawable(applicationContext, R.drawable.tracking_op_bg_unsel)
                    binding.optionC.view3.background = ActivityCompat.getDrawable(applicationContext, R.drawable.tracking_op_bg_unsel)
                    binding.optionD.view4.background = ActivityCompat.getDrawable(applicationContext, R.drawable.tracking_op_bg_unsel)
                    binding.optionE.view5.background = ActivityCompat.getDrawable(applicationContext, R.drawable.tracking_op_bg_sel)

                    binding.imageView1Age.setImageResource(R.mipmap.v_d_unsel)
                    binding.imageView2Age.setImageResource(R.mipmap.calorie_unsel)
                    binding.imageView3Age.setImageResource(R.mipmap.metabolic_unsel)
                    binding.imageView4Age.setImageResource(R.mipmap.antioxidant_unsel)
                    binding.imageView5Age.setImageResource(R.mipmap.physical_sel)

                    val i = Intent(context, RecommDetailActivity::class.java)
                    i.putExtra("object", objAge.data!!.recommendationInfoList!!.get(4).text)
                    i.putExtra("title", objAge.data!!.recommendationInfoList!!.get(4).title)
                    startActivity(i)
                }
            })
        }


        binding.textViewTrackingScore.text=objAge!!.data!!.scoreData!!.score
        val sliderInflammation= objAge!!.data!!.scoreData!!.color
        if(!sliderInflammation!!.isEmpty()){
            if(sliderInflammation.equals("Yellow")){
                binding.imageViewTrackingScoreImage.setBackgroundResource(R.mipmap.slider_yellow)
                binding.imageViewEmoji.setBackgroundResource(R.mipmap.emoji_yellow)
                binding.textViewTrackingScore.setTextColor(Color.parseColor("#FEBF00"))

                binding.textViewTrackingScoreUnit.visibility=View.VISIBLE
                binding.textViewTrackingScoreUnit.setTextColor(Color.parseColor("#FEBF00"))
                binding.textViewTrackingScoreUnit.setText("YRS")
            }

            else if(sliderInflammation.equals("Red")){
                binding.imageViewTrackingScoreImage.setBackgroundResource(R.mipmap.slider_red)
                binding.imageViewEmoji.setBackgroundResource(R.mipmap.emoji_red)
                binding.textViewTrackingScore.setTextColor(Color.parseColor("#E5221E"))
                binding.textViewTrackingScoreUnit.visibility=View.VISIBLE
                binding.textViewTrackingScoreUnit.setTextColor(Color.parseColor("#E5221E"))
                binding.textViewTrackingScoreUnit.setText("YRS")
            }

            else if(sliderInflammation.equals("Green")){
                binding.imageViewTrackingScoreImage.setBackgroundResource(R.mipmap.slider_green)
                binding.imageViewEmoji.setBackgroundResource(R.mipmap.emoji_green)
                binding.textViewTrackingScore.setTextColor(Color.parseColor("#8CC34B"))
                binding.textViewTrackingScoreUnit.visibility=View.VISIBLE
                binding.textViewTrackingScoreUnit.setTextColor(Color.parseColor("#8CC34B"))
                binding.textViewTrackingScoreUnit.setText("YRS")
            }
        }




    }


    @SuppressLint("ResourceAsColor")
    private fun showInflammationData(objInflammation: EpigenticHealth?) {
        binding.textViewToolbarTitle.setText(R.string.inflammation_health)
        binding.imageViewTrackingIcon.setImageResource(R.mipmap.inflammation_b)
        binding.textViewTrackingTitle.setText(R.string.inflammation_score)

        if(!objInflammation!!.data!!.info.toString().equals("")){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                binding.textViewInfo.text = Html.fromHtml(objInflammation!!.data!!.info.toString(),Html.FROM_HTML_MODE_COMPACT)
            } else {
                binding.textViewInfo.text = Html.fromHtml(objInflammation!!.data!!.info.toString())
            }
        }


        if(!objInflammation!!.data!!.introduction.toString().equals("")){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                binding.textViewIntroductionValue.text = Html.fromHtml(objInflammation!!.data!!.introduction.toString(),Html.FROM_HTML_MODE_COMPACT)
            } else {
                binding.textViewIntroductionValue.text = Html.fromHtml(objInflammation!!.data!!.introduction.toString())
            }
        }


        if(!objInflammation!!.data!!.outcome.toString().equals("")){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                binding.textViewOutComesValue.text = Html.fromHtml(objInflammation!!.data!!.outcome.toString(),Html.FROM_HTML_MODE_COMPACT)
            } else {
                binding.textViewOutComesValue.text = Html.fromHtml(objInflammation!!.data!!.outcome.toString())
            }
            //binding.textViewIntroductionValue.text= objEyeHealth!!.data!!.introduction.toString()
        }

        if(!objInflammation!!.data!!.recommendations.toString().equals("")){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                binding.textViewRecommendationValue.text = Html.fromHtml(objInflammation!!.data!!.recommendations.toString(),Html.FROM_HTML_MODE_COMPACT)
            } else {
                binding.textViewRecommendationValue.text = Html.fromHtml(objInflammation!!.data!!.recommendations.toString())
            }
            //binding.textViewIntroductionValue.text= objEyeHealth!!.data!!.introduction.toString()
        }

        if(!objInflammation.data!!.recommendationInfoList!!.isEmpty()){
            /*binding.rvRecomm.layoutManager = GridLayoutManager(this, 2)
            adapter = RecommInfoAdapter(this, this@EpigenticHealthTrackingDetailActivity, objEyeHealth.data!!.recommendationInfoList)
            binding.rvRecomm.adapter = adapter*/
            binding.llAge.visibility=View.GONE
            binding.llEye.visibility=View.GONE
            binding.llHearing.visibility=View.GONE
            binding.llMemory.visibility=View.GONE
            binding.llInflamation.visibility=View.VISIBLE

            binding.imageView1RInfla.setImageResource(R.mipmap.inflm1un)
            binding.imageView2RInfla.setImageResource(R.mipmap.inflm2un)
            binding.imageView3RInfla.setImageResource(R.mipmap.inflm3un)
            binding.imageView4RInfla.setImageResource(R.mipmap.v_d_unsel)
            binding.imageView5RInfla.setImageResource(R.mipmap.inflm4un)
            binding.imageView6RInfla.setImageResource(R.mipmap.inflm5un)
            binding.imageView7RInfla.setImageResource(R.mipmap.inflm6un)

            binding.textView1RInfla.text=objInflammation.data!!.recommendationInfoList!!.get(0).title
            binding.textView2RInfla.text=objInflammation.data!!.recommendationInfoList!!.get(1).title
            binding.textView3RInfla.text=objInflammation.data!!.recommendationInfoList!!.get(2).title
            binding.textView4RInfla.text=objInflammation.data!!.recommendationInfoList!!.get(3).title
            binding.textView5RInfla.text=objInflammation.data!!.recommendationInfoList!!.get(4).title
            binding.textView6RInfla.text=objInflammation.data!!.recommendationInfoList!!.get(5).title
            binding.textView7RInfla.text=objInflammation.data!!.recommendationInfoList!!.get(6).title

            binding.optionAInfla.setOnClickListener(object :View.OnClickListener{
                override fun onClick(v: View?) {

                    binding.optionAInfla.view1_Infla.background = ActivityCompat.getDrawable(applicationContext, R.drawable.tracking_op_bg_sel)
                    binding.optionBInfla.view2_Infla.background = ActivityCompat.getDrawable(applicationContext, R.drawable.tracking_op_bg_unsel)
                    binding.optionCInfla.view3_Infla.background = ActivityCompat.getDrawable(applicationContext, R.drawable.tracking_op_bg_unsel)
                    binding.optionDInfla.view4_Infla.background = ActivityCompat.getDrawable(applicationContext, R.drawable.tracking_op_bg_unsel)
                    binding.optionEInfla.view5_Infla.background = ActivityCompat.getDrawable(applicationContext, R.drawable.tracking_op_bg_unsel)
                    binding.optionFInfla.view6_Infla.background = ActivityCompat.getDrawable(applicationContext, R.drawable.tracking_op_bg_unsel)
                    binding.optionGInfla.view7_Infla.background = ActivityCompat.getDrawable(applicationContext, R.drawable.tracking_op_bg_unsel)

                    binding.imageView1RInfla.setImageResource(R.mipmap.inflm1sel)
                    binding.imageView2RInfla.setImageResource(R.mipmap.inflm2un)
                    binding.imageView3RInfla.setImageResource(R.mipmap.inflm3un)
                    binding.imageView4RInfla.setImageResource(R.mipmap.v_d_unsel)
                    binding.imageView5RInfla.setImageResource(R.mipmap.inflm4un)
                    binding.imageView6RInfla.setImageResource(R.mipmap.inflm5un)
                    binding.imageView7RInfla.setImageResource(R.mipmap.inflm6un)

                    val i = Intent(context, RecommDetailActivity::class.java)
                    i.putExtra("object", objInflammation.data!!.recommendationInfoList!!.get(0).text)
                    i.putExtra("title", objInflammation.data!!.recommendationInfoList!!.get(0).title)
                    startActivity(i)
                }
            })
            binding.optionBInfla.setOnClickListener(object :View.OnClickListener{
                override fun onClick(v: View?) {

                    binding.optionAInfla.view1_Infla.background = ActivityCompat.getDrawable(applicationContext, R.drawable.tracking_op_bg_unsel)
                    binding.optionBInfla.view2_Infla.background = ActivityCompat.getDrawable(applicationContext, R.drawable.tracking_op_bg_sel)
                    binding.optionCInfla.view3_Infla.background = ActivityCompat.getDrawable(applicationContext, R.drawable.tracking_op_bg_unsel)
                    binding.optionDInfla.view4_Infla.background = ActivityCompat.getDrawable(applicationContext, R.drawable.tracking_op_bg_unsel)
                    binding.optionEInfla.view5_Infla.background = ActivityCompat.getDrawable(applicationContext, R.drawable.tracking_op_bg_unsel)
                    binding.optionFInfla.view6_Infla.background = ActivityCompat.getDrawable(applicationContext, R.drawable.tracking_op_bg_unsel)
                    binding.optionGInfla.view7_Infla.background = ActivityCompat.getDrawable(applicationContext, R.drawable.tracking_op_bg_unsel)

                    binding.imageView1RInfla.setImageResource(R.mipmap.inflm1un)
                    binding.imageView2RInfla.setImageResource(R.mipmap.inflm2sel)
                    binding.imageView3RInfla.setImageResource(R.mipmap.inflm3un)
                    binding.imageView4RInfla.setImageResource(R.mipmap.v_d_unsel)
                    binding.imageView5RInfla.setImageResource(R.mipmap.inflm4un)
                    binding.imageView6RInfla.setImageResource(R.mipmap.inflm5un)
                    binding.imageView7RInfla.setImageResource(R.mipmap.inflm6un)

                    val i = Intent(context, RecommDetailActivity::class.java)
                    i.putExtra("object", objInflammation.data!!.recommendationInfoList!!.get(1).text)
                    i.putExtra("title", objInflammation.data!!.recommendationInfoList!!.get(1).title)
                    startActivity(i)
                }
            })
            binding.optionCInfla.setOnClickListener(object :View.OnClickListener{
                override fun onClick(v: View?) {

                    binding.optionAInfla.view1_Infla.background = ActivityCompat.getDrawable(applicationContext, R.drawable.tracking_op_bg_unsel)
                    binding.optionBInfla.view2_Infla.background = ActivityCompat.getDrawable(applicationContext, R.drawable.tracking_op_bg_unsel)
                    binding.optionCInfla.view3_Infla.background = ActivityCompat.getDrawable(applicationContext, R.drawable.tracking_op_bg_sel)
                    binding.optionDInfla.view4_Infla.background = ActivityCompat.getDrawable(applicationContext, R.drawable.tracking_op_bg_unsel)
                    binding.optionEInfla.view5_Infla.background = ActivityCompat.getDrawable(applicationContext, R.drawable.tracking_op_bg_unsel)
                    binding.optionFInfla.view6_Infla.background = ActivityCompat.getDrawable(applicationContext, R.drawable.tracking_op_bg_unsel)
                    binding.optionGInfla.view7_Infla.background = ActivityCompat.getDrawable(applicationContext, R.drawable.tracking_op_bg_unsel)

                    binding.imageView1RInfla.setImageResource(R.mipmap.inflm1un)
                    binding.imageView2RInfla.setImageResource(R.mipmap.inflm2un)
                    binding.imageView3RInfla.setImageResource(R.mipmap.inflm3sel)
                    binding.imageView4RInfla.setImageResource(R.mipmap.v_d_unsel)
                    binding.imageView5RInfla.setImageResource(R.mipmap.inflm4un)
                    binding.imageView6RInfla.setImageResource(R.mipmap.inflm5un)
                    binding.imageView7RInfla.setImageResource(R.mipmap.inflm6un)

                    val i = Intent(context, RecommDetailActivity::class.java)
                    i.putExtra("object", objInflammation.data!!.recommendationInfoList!!.get(2).text)
                    i.putExtra("title", objInflammation.data!!.recommendationInfoList!!.get(2).title)
                    startActivity(i)
                }
            })
            binding.optionDInfla.setOnClickListener(object :View.OnClickListener{
                override fun onClick(v: View?) {

                    binding.optionAInfla.view1_Infla.background = ActivityCompat.getDrawable(applicationContext, R.drawable.tracking_op_bg_unsel)
                    binding.optionBInfla.view2_Infla.background = ActivityCompat.getDrawable(applicationContext, R.drawable.tracking_op_bg_unsel)
                    binding.optionCInfla.view3_Infla.background = ActivityCompat.getDrawable(applicationContext, R.drawable.tracking_op_bg_unsel)
                    binding.optionDInfla.view4_Infla.background = ActivityCompat.getDrawable(applicationContext, R.drawable.tracking_op_bg_sel)
                    binding.optionEInfla.view5_Infla.background = ActivityCompat.getDrawable(applicationContext, R.drawable.tracking_op_bg_unsel)
                    binding.optionFInfla.view6_Infla.background = ActivityCompat.getDrawable(applicationContext, R.drawable.tracking_op_bg_unsel)
                    binding.optionGInfla.view7_Infla.background = ActivityCompat.getDrawable(applicationContext, R.drawable.tracking_op_bg_unsel)

                    binding.imageView1RInfla.setImageResource(R.mipmap.inflm1un)
                    binding.imageView2RInfla.setImageResource(R.mipmap.inflm2un)
                    binding.imageView3RInfla.setImageResource(R.mipmap.inflm3un)
                    binding.imageView4RInfla.setImageResource(R.mipmap.v_d_select)
                    binding.imageView5RInfla.setImageResource(R.mipmap.inflm4un)
                    binding.imageView6RInfla.setImageResource(R.mipmap.inflm5un)
                    binding.imageView7RInfla.setImageResource(R.mipmap.inflm6un)

                    val i = Intent(context, RecommDetailActivity::class.java)
                    i.putExtra("object", objInflammation.data!!.recommendationInfoList!!.get(3).text)
                    i.putExtra("title", objInflammation.data!!.recommendationInfoList!!.get(3).title)
                    startActivity(i)
                }
            })
            binding.optionEInfla.setOnClickListener(object :View.OnClickListener{
                override fun onClick(v: View?) {

                    binding.optionAInfla.view1_Infla.background = ActivityCompat.getDrawable(applicationContext, R.drawable.tracking_op_bg_unsel)
                    binding.optionBInfla.view2_Infla.background = ActivityCompat.getDrawable(applicationContext, R.drawable.tracking_op_bg_unsel)
                    binding.optionCInfla.view3_Infla.background = ActivityCompat.getDrawable(applicationContext, R.drawable.tracking_op_bg_unsel)
                    binding.optionDInfla.view4_Infla.background = ActivityCompat.getDrawable(applicationContext, R.drawable.tracking_op_bg_unsel)
                    binding.optionEInfla.view5_Infla.background = ActivityCompat.getDrawable(applicationContext, R.drawable.tracking_op_bg_sel)
                    binding.optionFInfla.view6_Infla.background = ActivityCompat.getDrawable(applicationContext, R.drawable.tracking_op_bg_unsel)
                    binding.optionGInfla.view7_Infla.background = ActivityCompat.getDrawable(applicationContext, R.drawable.tracking_op_bg_unsel)

                    binding.imageView1RInfla.setImageResource(R.mipmap.inflm1un)
                    binding.imageView2RInfla.setImageResource(R.mipmap.inflm2un)
                    binding.imageView3RInfla.setImageResource(R.mipmap.inflm3un)
                    binding.imageView4RInfla.setImageResource(R.mipmap.v_d_unsel)
                    binding.imageView5RInfla.setImageResource(R.mipmap.inflm4sel)
                    binding.imageView6RInfla.setImageResource(R.mipmap.inflm5un)
                    binding.imageView7RInfla.setImageResource(R.mipmap.inflm6un)

                    val i = Intent(context, RecommDetailActivity::class.java)
                    i.putExtra("object", objInflammation.data!!.recommendationInfoList!!.get(4).text)
                    i.putExtra("title", objInflammation.data!!.recommendationInfoList!!.get(4).title)
                    startActivity(i)
                }
            })
            binding.optionFInfla.setOnClickListener(object :View.OnClickListener{
                override fun onClick(v: View?) {
                    binding.optionAInfla.view1_Infla.background = ActivityCompat.getDrawable(applicationContext, R.drawable.tracking_op_bg_unsel)
                    binding.optionBInfla.view2_Infla.background = ActivityCompat.getDrawable(applicationContext, R.drawable.tracking_op_bg_unsel)
                    binding.optionCInfla.view3_Infla.background = ActivityCompat.getDrawable(applicationContext, R.drawable.tracking_op_bg_unsel)
                    binding.optionDInfla.view4_Infla.background = ActivityCompat.getDrawable(applicationContext, R.drawable.tracking_op_bg_unsel)
                    binding.optionEInfla.view5_Infla.background = ActivityCompat.getDrawable(applicationContext, R.drawable.tracking_op_bg_unsel)
                    binding.optionFInfla.view6_Infla.background = ActivityCompat.getDrawable(applicationContext, R.drawable.tracking_op_bg_sel)
                    binding.optionGInfla.view7_Infla.background = ActivityCompat.getDrawable(applicationContext, R.drawable.tracking_op_bg_unsel)

                    binding.imageView1RInfla.setImageResource(R.mipmap.inflm1un)
                    binding.imageView2RInfla.setImageResource(R.mipmap.inflm2un)
                    binding.imageView3RInfla.setImageResource(R.mipmap.inflm3un)
                    binding.imageView4RInfla.setImageResource(R.mipmap.v_d_unsel)
                    binding.imageView5RInfla.setImageResource(R.mipmap.inflm4un)
                    binding.imageView6RInfla.setImageResource(R.mipmap.inflm5sel)
                    binding.imageView7RInfla.setImageResource(R.mipmap.inflm6un)

                    val i = Intent(context, RecommDetailActivity::class.java)
                    i.putExtra("object", objInflammation.data!!.recommendationInfoList!!.get(5).text)
                    i.putExtra("title", objInflammation.data!!.recommendationInfoList!!.get(5).title)
                    startActivity(i)
                }
            })
            binding.optionGInfla.setOnClickListener(object :View.OnClickListener{
                override fun onClick(v: View?) {

                    binding.optionAInfla.view1_Infla.background = ActivityCompat.getDrawable(applicationContext, R.drawable.tracking_op_bg_unsel)
                    binding.optionBInfla.view2_Infla.background = ActivityCompat.getDrawable(applicationContext, R.drawable.tracking_op_bg_unsel)
                    binding.optionCInfla.view3_Infla.background = ActivityCompat.getDrawable(applicationContext, R.drawable.tracking_op_bg_unsel)
                    binding.optionDInfla.view4_Infla.background = ActivityCompat.getDrawable(applicationContext, R.drawable.tracking_op_bg_unsel)
                    binding.optionEInfla.view5_Infla.background = ActivityCompat.getDrawable(applicationContext, R.drawable.tracking_op_bg_unsel)
                    binding.optionFInfla.view6_Infla.background = ActivityCompat.getDrawable(applicationContext, R.drawable.tracking_op_bg_unsel)
                    binding.optionGInfla.view7_Infla.background = ActivityCompat.getDrawable(applicationContext, R.drawable.tracking_op_bg_sel)

                    binding.imageView1RInfla.setImageResource(R.mipmap.inflm1un)
                    binding.imageView2RInfla.setImageResource(R.mipmap.inflm2un)
                    binding.imageView3RInfla.setImageResource(R.mipmap.inflm3un)
                    binding.imageView4RInfla.setImageResource(R.mipmap.v_d_unsel)
                    binding.imageView5RInfla.setImageResource(R.mipmap.inflm4un)
                    binding.imageView6RInfla.setImageResource(R.mipmap.inflm5un)
                    binding.imageView7RInfla.setImageResource(R.mipmap.inflm6sel)

                    val i = Intent(context, RecommDetailActivity::class.java)
                    i.putExtra("object", objInflammation.data!!.recommendationInfoList!!.get(6).text)
                    i.putExtra("title", objInflammation.data!!.recommendationInfoList!!.get(6).title)
                    startActivity(i)
                }
            })

        }



        binding.textViewTrackingScore.text=objInflammation!!.data!!.scoreData!!.score
        val sliderInflammation= objInflammation!!.data!!.scoreData!!.color
        if(!sliderInflammation!!.isEmpty()){
            if(sliderInflammation.equals("Yellow")){
                binding.imageViewTrackingScoreImage.setBackgroundResource(R.mipmap.slider_yellow)
                binding.imageViewEmoji.setBackgroundResource(R.mipmap.emoji_yellow)
                binding.textViewTrackingScore.setTextColor(Color.parseColor("#FEBF00"))
                binding.textViewTrackingScoreUnit.visibility=View.VISIBLE
                binding.textViewTrackingScoreUnit.setTextColor(Color.parseColor("#FEBF00"))
                binding.textViewTrackingScoreUnit.setText("/500")
            }

            else if(sliderInflammation.equals("Red")){
                binding.imageViewTrackingScoreImage.setBackgroundResource(R.mipmap.slider_red)
                binding.imageViewEmoji.setBackgroundResource(R.mipmap.emoji_red)
                binding.textViewTrackingScore.setTextColor(Color.parseColor("#E5221E"))
                binding.textViewTrackingScoreUnit.visibility=View.VISIBLE
                binding.textViewTrackingScoreUnit.setTextColor(Color.parseColor("#E5221E"))
                binding.textViewTrackingScoreUnit.setText("/500")
            }

            else if(sliderInflammation.equals("Green")){
                binding.imageViewTrackingScoreImage.setBackgroundResource(R.mipmap.slider_green)
                binding.imageViewEmoji.setBackgroundResource(R.mipmap.emoji_green)
                binding.textViewTrackingScore.setTextColor(Color.parseColor("#8CC34B"))
                binding.textViewTrackingScoreUnit.visibility=View.VISIBLE
                binding.textViewTrackingScoreUnit.setTextColor(Color.parseColor("#8CC34B"))
                binding.textViewTrackingScoreUnit.setText("/500")
            }
        }
    }

    private fun showMemoryData(objMemory: EpigenticHealth?) {
        binding.textViewToolbarTitle.setText(R.string.memory_health)
        binding.imageViewTrackingIcon.setImageResource(R.mipmap.memory_b)
        binding.textViewTrackingTitle.setText(R.string.memory_score)


        if(!objMemory!!.data!!.info.toString().equals("")){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                binding.textViewInfo.text = Html.fromHtml(objMemory!!.data!!.info.toString(),Html.FROM_HTML_MODE_COMPACT)
            } else {
                binding.textViewInfo.text = Html.fromHtml(objMemory!!.data!!.info.toString())
            }
        }

        if(!objMemory!!.data!!.introduction.toString().equals("")){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                binding.textViewIntroductionValue.text = Html.fromHtml(objMemory!!.data!!.introduction.toString(),Html.FROM_HTML_MODE_COMPACT)
            } else {
                binding.textViewIntroductionValue.text = Html.fromHtml(objMemory!!.data!!.introduction.toString())
            }

        }

        if(!objMemory!!.data!!.outcome.toString().equals("")){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                binding.textViewOutComesValue.text = Html.fromHtml(objMemory!!.data!!.outcome.toString(),Html.FROM_HTML_MODE_COMPACT)
            } else {
                binding.textViewOutComesValue.text = Html.fromHtml(objMemory!!.data!!.outcome.toString())
            }
            //binding.textViewIntroductionValue.text= objEyeHealth!!.data!!.introduction.toString()
        }

        if(!objMemory!!.data!!.recommendations.toString().equals("")){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                binding.textViewRecommendationValue.text = Html.fromHtml(objMemory!!.data!!.recommendations.toString(),Html.FROM_HTML_MODE_COMPACT)
            } else {
                binding.textViewRecommendationValue.text = Html.fromHtml(objMemory!!.data!!.recommendations.toString())
            }
            //binding.textViewIntroductionValue.text= objEyeHealth!!.data!!.introduction.toString()
        }


        if(!objMemory.data!!.recommendationInfoList!!.isEmpty()){
            /*binding.rvRecomm.layoutManager = GridLayoutManager(this, 2)
            adapter = RecommInfoAdapter(this, this@EpigenticHealthTrackingDetailActivity, objEyeHealth.data!!.recommendationInfoList)
            binding.rvRecomm.adapter = adapter*/
            binding.llAge.visibility=View.GONE
            binding.llEye.visibility=View.GONE
            binding.llHearing.visibility=View.GONE
            binding.llMemory.visibility=View.VISIBLE
            binding.llInflamation.visibility=View.GONE

            binding.imageView1RMemory.setImageResource(R.mipmap.memory1un)
            binding.imageView2RMemory.setImageResource(R.mipmap.memory3un)
            binding.imageView3RMemory.setImageResource(R.mipmap.memory4un)

            binding.textView1Memory.text=objMemory.data!!.recommendationInfoList!!.get(0).title
            binding.textView2Memory.text=objMemory.data!!.recommendationInfoList!!.get(1).title
            binding.textView3Memory.text=objMemory.data!!.recommendationInfoList!!.get(2).title


            binding.optionAMemory.setOnClickListener(object :View.OnClickListener{
                override fun onClick(v: View?) {

                    binding.optionAMemory.view1_Memory.background = ActivityCompat.getDrawable(applicationContext, R.drawable.tracking_op_bg_sel)
                    binding.optionBMemory.view2_Memory.background = ActivityCompat.getDrawable(applicationContext, R.drawable.tracking_op_bg_unsel)
                    binding.optionCMemory.view3_Memory.background = ActivityCompat.getDrawable(applicationContext, R.drawable.tracking_op_bg_unsel)

                    binding.imageView1RMemory.setImageResource(R.mipmap.memory1sel)
                    binding.imageView2RMemory.setImageResource(R.mipmap.memory3un)
                    binding.imageView3RMemory.setImageResource(R.mipmap.memory4un)

                    val i = Intent(context, RecommDetailActivity::class.java)
                    i.putExtra("object", objMemory.data!!.recommendationInfoList!!.get(0).text)
                    i.putExtra("title", objMemory.data!!.recommendationInfoList!!.get(0).title)
                    startActivity(i)
                }
            })

            binding.optionBMemory.setOnClickListener(object :View.OnClickListener{
                override fun onClick(v: View?) {

                    binding.optionAMemory.view1_Memory.background = ActivityCompat.getDrawable(applicationContext, R.drawable.tracking_op_bg_unsel)
                    binding.optionBMemory.view2_Memory.background = ActivityCompat.getDrawable(applicationContext, R.drawable.tracking_op_bg_sel)
                    binding.optionCMemory.view3_Memory.background = ActivityCompat.getDrawable(applicationContext, R.drawable.tracking_op_bg_unsel)

                    binding.imageView1RMemory.setImageResource(R.mipmap.memory1un)
                    binding.imageView2RMemory.setImageResource(R.mipmap.memory3sel)
                    binding.imageView3RMemory.setImageResource(R.mipmap.memory4un)

                    val i = Intent(context, RecommDetailActivity::class.java)
                    i.putExtra("object", objMemory.data!!.recommendationInfoList!!.get(1).text)
                    i.putExtra("title", objMemory.data!!.recommendationInfoList!!.get(1).title)
                    startActivity(i)
                }
            })

            binding.optionCMemory.setOnClickListener(object :View.OnClickListener{
                override fun onClick(v: View?) {

                    binding.optionAMemory.view1_Memory.background = ActivityCompat.getDrawable(applicationContext, R.drawable.tracking_op_bg_unsel)
                    binding.optionBMemory.view2_Memory.background = ActivityCompat.getDrawable(applicationContext, R.drawable.tracking_op_bg_unsel)
                    binding.optionCMemory.view3_Memory.background = ActivityCompat.getDrawable(applicationContext, R.drawable.tracking_op_bg_sel)

                    binding.imageView1RMemory.setImageResource(R.mipmap.memory1un)
                    binding.imageView2RMemory.setImageResource(R.mipmap.memory3un)
                    binding.imageView3RMemory.setImageResource(R.mipmap.memory4sel)


                    val i = Intent(context, RecommDetailActivity::class.java)
                    i.putExtra("object", objMemory.data!!.recommendationInfoList!!.get(2).text)
                    i.putExtra("title", objMemory.data!!.recommendationInfoList!!.get(2).title)
                    startActivity(i)
                }
            })
        }


        binding.textViewTrackingScore.text=objMemory!!.data!!.scoreData!!.score
        val sliderMemory= objMemory!!.data!!.scoreData!!.color
        if(!sliderMemory!!.isEmpty()){
            if(sliderMemory.equals("Yellow")){
                binding.imageViewTrackingScoreImage.setBackgroundResource(R.mipmap.slider_yellow)
                binding.imageViewEmoji.setBackgroundResource(R.mipmap.emoji_yellow)
                binding.textViewTrackingScore.setTextColor(Color.parseColor("#FEBF00"))
                binding.textViewTrackingScoreUnit.visibility=View.GONE

                //binding.textViewTrackingScoreUnit.setTextColor(Color.parseColor("#FEBF00"))
                //binding.textViewTrackingScoreUnit.setText("YRS")
            }

            else if(sliderMemory.equals("Red")){
                binding.imageViewTrackingScoreImage.setBackgroundResource(R.mipmap.slider_red)
                binding.imageViewEmoji.setBackgroundResource(R.mipmap.emoji_red)
                binding.textViewTrackingScore.setTextColor(Color.parseColor("#E5221E"))
                binding.textViewTrackingScoreUnit.visibility=View.GONE
                //binding.textViewTrackingScoreUnit.setTextColor(Color.parseColor("#E5221E"))
               // binding.textViewTrackingScoreUnit.setText("YRS")
            }

            else if(sliderMemory.equals("Green")){
                binding.imageViewTrackingScoreImage.setBackgroundResource(R.mipmap.slider_green)
                binding.imageViewEmoji.setBackgroundResource(R.mipmap.emoji_green)
                binding.textViewTrackingScore.setTextColor(Color.parseColor("#8CC34B"))
                binding.textViewTrackingScoreUnit.visibility=View.GONE
                //binding.textViewTrackingScoreUnit.setTextColor(Color.parseColor("#8CC34B"))
               // binding.textViewTrackingScoreUnit.setText("YRS")
            }
        }
    }


    // slide the view from below itself to the current position
    fun slideUp(view: View, cardViewKey: CardView) {
        //view.setVisibility(View.VISIBLE)
        /*val animate = TranslateAnimation(
            0f, // fromXDelta
            0f, // toXDelta
            view.getHeight().toFloat(), // fromYDelta
            0f
        )                // toYDelta
        animate.duration = 500
        animate.fillAfter = true
        view.startAnimation(animate)*/
        cardViewKey.setCardBackgroundColor(ContextCompat.getColor(this, R.color.card_unselect))
        view.visibility=View.GONE
    }


    // slide the view from its current position to below itself
    fun slideDown(view: View, cardViewKey: CardView) {
        /*val animate = TranslateAnimation(
            0f, // fromXDelta
            0f, // toXDelta
            0f, // fromYDelta
            view.getHeight().toFloat()
        ) // toYDelta
        animate.duration = 500
        animate.fillAfter = true
        view.startAnimation(animate)*/
       // cardViewKey.setBackgroundColor(Color.parseColor("#6D91AE"))
        cardViewKey.setCardBackgroundColor(ContextCompat.getColor(this, R.color.card_select))
        view.visibility=View.VISIBLE
    }
}
