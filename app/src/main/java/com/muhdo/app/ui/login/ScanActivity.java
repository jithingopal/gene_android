package com.muhdo.app.ui.login;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import com.muhdo.app.R;

public class ScanActivity extends AppCompatActivity {

    public static TextView tvResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scann);

        tvResult = findViewById(R.id.tvresult);

        Button btn = findViewById(R.id.btn);

        btn.setOnClickListener(v -> {
            Intent intent = new Intent(ScanActivity.this, Scanner.class);
            startActivity(intent);
        });

    }
}