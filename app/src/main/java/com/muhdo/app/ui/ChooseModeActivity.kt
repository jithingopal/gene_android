package com.muhdo.app.ui

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.muhdo.app.R
import com.muhdo.app.databinding.ActivityChooseModeBinding

class ChooseModeActivity : AppCompatActivity() {
    lateinit var binding: ActivityChooseModeBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this@ChooseModeActivity, R.layout.activity_choose_mode)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        val i = Intent(this@ChooseModeActivity, DashboardActivity::class.java)
        i.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(i)
    }
}