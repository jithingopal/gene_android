package com.muhdo.app.ui.epigentic.food

import android.annotation.SuppressLint
import android.app.ProgressDialog
import android.content.Context
import android.os.Build
import android.os.Bundle
import android.util.Base64
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.GsonBuilder
import com.mindorks.placeholderview.ExpandablePlaceHolderView
import com.mindorks.placeholderview.annotations.Layout
import com.mindorks.placeholderview.annotations.Resolve
import com.mindorks.placeholderview.annotations.expand.ChildPosition
import com.mindorks.placeholderview.annotations.expand.ParentPosition
import com.muhdo.app.R
import com.muhdo.app.databinding.ActivityEpigenticFoodNewBinding
import com.muhdo.app.repository.ApiUtilis
import com.muhdo.app.repository.ErrorModel
import com.muhdo.app.repository.NetworkManager
import com.muhdo.app.repository.ServiceListener
import com.muhdo.app.ui.epigentic.food.adapter.FoodGroupAdapter
import com.muhdo.app.ui.epigentic.food.model.FoodDeleteResponse
import com.muhdo.app.ui.epigentic.food.model.FoodInfoModel
import com.muhdo.app.ui.epigentic.food.model.FoodItem
import com.muhdo.app.ui.epigentic.food.response.AddFoodResponse
import com.muhdo.app.ui.epigentic.food.response.FoodCategoryResponse
import com.muhdo.app.ui.epigentic.food.response.FoodListResponse
import com.muhdo.app.ui.epigentic.food.view.FoodHeadingView
import com.muhdo.app.ui.epigentic.medication.response.Feed
import com.muhdo.app.utils.v3.Constants
import com.timingsystemkotlin.backuptimingsystem.Utility
import kotlinx.android.synthetic.main.activity_epigentic_food_new.view.*
import org.json.JSONArray
import java.io.IOException
import java.io.InputStream
import java.util.*

class EpigenticFoodNewActivity : AppCompatActivity() {
    private var progressDialog: ProgressDialog? = null
    private var mExpandableView: ExpandablePlaceHolderView? = null
    var foodGroupAdapter: FoodGroupAdapter? = null
    private var mContext: Context? = null
    //private var mOnScrollListener: ExpandablePlaceHolderView.OnScrollListener? = null
    var show = false
    var foodCategoy="BREAKFAST"
    var cate=""
    var foodGroupResponse: FoodCategoryResponse? = null
    var spinnerVal = "MEAT"

    var editText: EditText? = null

    var data = arrayOf("MEAT", "FISH", "DAIRY", "CEREALS,BREADS", "VEGETABLES", "FRUIT/NUTS")
    //var data = arrayOf("")

    lateinit var binding: ActivityEpigenticFoodNewBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this@EpigenticFoodNewActivity, R.layout.activity_epigentic_food_new)
        Dialog(this)
        mExpandableView = findViewById(R.id.expandable_view_food) as ExpandablePlaceHolderView
        binding.btnBack.setOnClickListener {
            finish()
            /* if(show==true){
                 binding.layoutExecrise2.visibility= View.GONE
                 binding.layoutAddExecrise.visibility=View.GONE
                 binding.layoutExecrise1.visibility= View.VISIBLE
                 binding.layoutNotAddExecrise.visibility= View.VISIBLE
                 show=false;
             }else{
                 finish()
             }*/
        }
        //hideKeyboard()
        clickAddFoodView()
        showUserFoodList()
        clickOnBreakfastTab()
        clickOnLunchTab()
        clickOnDinnerTab()
        clickOnSnacksAndDrinks()
        showProgressDialog()
        getFoodGroupData(this@EpigenticFoodNewActivity)
        //Get user data from server


        //Call food category data api for get spinner data
        callSupplementDataServiceForSpinner(this@EpigenticFoodNewActivity, FoodCategoryResponse())
        // set listener on search view
        setListenerOnSearch()

    }




    private fun setListenerOnSearch() {
        binding.searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener,
            androidx.appcompat.widget.SearchView.OnQueryTextListener {
            override fun onQueryTextChange(query: String): Boolean {
                foodGroupAdapter?.getFilter()?.filter(query);
                return false
            }
            override fun onQueryTextSubmit(query: String): Boolean {
                return false
            }
        })

    }

    private fun getFoodGroupData(mContext: EpigenticFoodNewActivity) {

        val manager = NetworkManager()
        if (manager.isConnectingToInternet(mContext)) {
            manager.createApiRequest(
                ApiUtilis.getAPIService(Constants.FOOD_API).getFoodGroupData(),
                object : ServiceListener<FoodCategoryResponse> {
                    override fun getServerResponse(response: FoodCategoryResponse, requestcode: Int) {
                        //  binding.progressBar.visibility = View.GONE
                        try {
                            if(response.statusCode.equals("200") && response.data!=null){
                                foodGroupResponse=response
                                try{
                                    setDataInRecyclerView(response.data.breakfast.meatItemList!!, this@EpigenticFoodNewActivity);
                                    callSupplementDataServiceForSpinner(mContext,response)
                                }catch (e: Exception){

                                }

                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }

                    override fun getError(error: ErrorModel, requestcode: Int) {
                        //binding.progressBar.visibility = View.GONE
                        Utility.displayShortSnackBar(
                            binding.parentLayout,
                            error.getMessage().toString()
                        )
                    }
                })
        } else {
            //binding.progressBar.visibility = View.GONE
            Utility.displayShortSnackBar(
                binding.parentLayout,
                resources.getString(R.string.check_internet)
            )

        }
        callFoodService(this@EpigenticFoodNewActivity)
    }


    @SuppressLint("WrongConstant")
    private fun callSupplementDataServiceForSpinner(mContext: EpigenticFoodNewActivity, response: FoodCategoryResponse?) {
        binding.recyclerViewFoodType.layoutManager = LinearLayoutManager(this, LinearLayout.VERTICAL, false) as RecyclerView.LayoutManager?
        var spinnerAdapter = CustomDropDownAdapter(mContext!!, data)
        binding.spinnerFood?.adapter = spinnerAdapter
        binding.spinnerFood?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>?, view: android.view.View?, position: Int, id: Long) {
                if (parent != null) {
                    spinnerVal = data[position]

                    if(foodCategoy.equals("BREAKFAST")){

                        if (spinnerVal.equals("MEAT")) {
                            setDataInRecyclerView(response!!.data.breakfast.meatItemList!!, this@EpigenticFoodNewActivity);
                        }
                        else if (spinnerVal.equals("FISH")) {
                            setDataInRecyclerView(response!!.data.breakfast.fishItemList!!, this@EpigenticFoodNewActivity)
                        }
                        else if (spinnerVal.equals("DAIRY")) {
                            setDataInRecyclerView(response!!.data.breakfast.dairyItemList!!, this@EpigenticFoodNewActivity)
                        }
                        else if (spinnerVal.equals("CEREALS,BREADS")) {
                            setDataInRecyclerView(response!!.data.breakfast.cerealBreadsItemList!!, this@EpigenticFoodNewActivity)
                        }
                        else if (spinnerVal.equals("VEGETABLES")) {
                            setDataInRecyclerView(response!!.data.breakfast.vegetablesItemList!!, this@EpigenticFoodNewActivity)
                        }
                        else if (spinnerVal.equals("FRUIT/NUTS")) {
                            setDataInRecyclerView(response!!.data.breakfast.fruitNutsItemList!!, this@EpigenticFoodNewActivity)
                        }
                    }

                    else if(foodCategoy.equals("LUNCH")){

                        if (spinnerVal.equals("MEAT")) {
                            setDataInRecyclerView(response!!.data.lunch.meatItemList!!, this@EpigenticFoodNewActivity)
                        }
                        else if (spinnerVal.equals("FISH")) {
                            setDataInRecyclerView(response!!.data.lunch.fishItemList!!, this@EpigenticFoodNewActivity)
                        }
                        else if (spinnerVal.equals("DAIRY")) {
                            setDataInRecyclerView(response!!.data.lunch.dairyItemList!!, this@EpigenticFoodNewActivity)
                        }
                        else if (spinnerVal.equals("CEREALS,BREADS")) {
                            setDataInRecyclerView(response!!.data.lunch.cerealBreadsItemList!!, this@EpigenticFoodNewActivity)
                        }
                        else if (spinnerVal.equals("VEGETABLES")) {
                            setDataInRecyclerView(response!!.data.lunch.vegetablesItemList!!, this@EpigenticFoodNewActivity)
                        }
                        else if (spinnerVal.equals("FRUIT/NUTS")) {
                            setDataInRecyclerView(response!!.data.lunch.fruitNutsItemList!!, this@EpigenticFoodNewActivity)
                        }
                    }


                    else if(foodCategoy.equals("DINNER")){

                        if (spinnerVal.equals("MEAT")) {
                            setDataInRecyclerView(response!!.data.dinner.meatItemList!!, this@EpigenticFoodNewActivity)
                        }
                        else if (spinnerVal.equals("FISH")) {
                            setDataInRecyclerView(response!!.data.dinner.fishItemList!!, this@EpigenticFoodNewActivity)
                        }
                        else if (spinnerVal.equals("DAIRY")) {
                            setDataInRecyclerView(response!!.data.dinner.dairyItemList!!, this@EpigenticFoodNewActivity)
                        }
                        else if (spinnerVal.equals("CEREALS,BREADS")) {
                            setDataInRecyclerView(response!!.data.dinner.cerealBreadsItemList!!, this@EpigenticFoodNewActivity)
                        }
                        else if (spinnerVal.equals("VEGETABLES")) {
                            setDataInRecyclerView(response!!.data.dinner.vegetablesItemList!!, this@EpigenticFoodNewActivity)
                        }
                        else if (spinnerVal.equals("FRUIT/NUTS")) {
                            setDataInRecyclerView(response!!.data.dinner.fruitNutsItemList!!, this@EpigenticFoodNewActivity)
                         }
                    }



                }
            }

            override fun onNothingSelected(parent: AdapterView<*>) {

            }
        }

    }


    private fun setDataInRecyclerView(list: MutableList<FoodItem>,epigenticFoodNewActivity: EpigenticFoodNewActivity) {

        Collections.sort(list)
        foodGroupAdapter = FoodGroupAdapter(list!!, this@EpigenticFoodNewActivity)
        binding.recyclerViewFoodType.adapter = foodGroupAdapter
        foodGroupAdapter!!.setListener(object : FoodGroupAdapter.supplementInfoListListener {
            override fun addSupplement(
                obj: FoodItem,
                quantity: String,
                position: Int,
                unitValue: EditText
            ) {

                // Toast.makeText(applicationContext, "select" , Toast.LENGTH_SHORT).show();
                if (obj != null) {
                    sendFoodItemToServer(obj, quantity)
                    editText=unitValue;
                    Log.wtf("data", "obj---"+obj.id);
                }

            }

        })

    }

    private fun sendFoodItemToServer(obj: FoodItem, quantity: String) {

        showProgressDialog()
        val manager = NetworkManager()
        val params = HashMap<String, String>()


        val userId = Base64.decode(Utility.getUserID(this), Base64.DEFAULT)
        val decodeUserId = String(userId, charset("UTF-8"))
        params["user_id"] = decodeUserId
        params["nutrition_id"] = obj.id
        params["user_value"] =quantity
        //   Toast.makeText(applicationContext,"send", Toast.LENGTH_SHORT).show();

        if (manager.isConnectingToInternet(applicationContext)) {
            manager.createApiRequest(
                ApiUtilis.getAPIService(Constants.FOOD_API).sendFoodData(params),
                object : ServiceListener<AddFoodResponse> {
                    override fun getServerResponse(response: AddFoodResponse, requestcode: Int) {
                        binding.searchView.clearFocus()
                        binding.searchView.setFocusable(false);
                        binding.searchView.setIconified(false);

                        if(response.getCode()==200){
                            callFoodService(this@EpigenticFoodNewActivity)
                            binding.layoutExecrise2.visibility= View.VISIBLE
                            binding.layoutAddExecrise.visibility=View.VISIBLE
                            binding.scrollHorizontal.visibility=View.VISIBLE
                            binding.layoutFood2.visibility= View.GONE
                            binding.layoutNotAddExecrise.visibility= View.GONE
                            binding.searchView.clearFocus()
                        }else{
                            Utility.displayShortSnackBar(
                                binding.parentLayout,
                                response.getMessage().toString()
                            )
                        }
                    }

                    override fun getError(error: ErrorModel, requestcode: Int) {
                        dismissProgressDialog()
                        //binding.progressBar.visibility = View.GONE
                        Utility.displayShortSnackBar(
                            binding.parentLayout,
                            error.getMessage().toString()
                        )
                    }
                })
        } else {
            //binding.progressBar.visibility = View.GONE
            dismissProgressDialog()
            Utility.displayShortSnackBar(
                binding.parentLayout,
                resources.getString(R.string.check_internet)
            )
        }

    }


    open class CustomDropDownAdapter(val context: Context, var listItemsTxt: Array<String>) : BaseAdapter() {
        val mInflater: LayoutInflater = LayoutInflater.from(context)
        override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
            val view: View
            val vh: ItemRowHolder
            if (convertView == null) {
                view = mInflater.inflate(R.layout.row_spinner_food_group, parent, false)
                vh = ItemRowHolder(view)
                view?.tag = vh
            } else {
                view = convertView
                vh = view.tag as ItemRowHolder
            }

            // setting adapter item height programatically.

            vh.label.text = listItemsTxt.get(position)

            return view
        }

        override fun getItem(position: Int): Any? {

            return null

        }

        override fun getItemId(position: Int): Long {

            return 0

        }

        override fun getCount(): Int {
            return listItemsTxt.size
        }

        private class ItemRowHolder(row: View?) {

            val label: TextView

            init {
                this.label = row?.findViewById(R.id.textview_intensity) as TextView
            }
        }
    }

   private fun clickAddFoodView() {
        binding.layoutAddExecrise.setOnClickListener(object: View.OnClickListener {
            override fun onClick(v: View?) {
                Log.d("data","click a")
                show=true
                // Toast.makeText(applicationContext,"hello", Toast.LENGTH_SHORT).show();
                binding.layoutExecrise2.visibility= View.GONE
                binding.layoutAddExecrise.visibility= View.GONE
                binding.layoutLoggedExcriseView.visibility= View.GONE

                binding.scrollHorizontal.visibility= View.VISIBLE
                binding.layoutFood2.visibility= View.VISIBLE
                binding.layoutNotAddExecrise.visibility= View.VISIBLE
                binding.layoutGymRun.visibility= View.VISIBLE

            }
        })
    }

    private fun showUserFoodList() {
        binding.layoutNotAddExecrise.setOnClickListener(object: View.OnClickListener {
            override fun onClick(v: View?) {
                Log.d("data","click a")
                show=false
                // Toast.makeText(applicationContext,"hello", Toast.LENGTH_SHORT).show();
                binding.layoutExecrise2.visibility= View.VISIBLE
                binding.layoutAddExecrise.visibility= View.VISIBLE
                binding.scrollHorizontal.visibility= View.VISIBLE
                binding.layoutGymRun.visibility= View.VISIBLE

                // binding.layoutLoggedExcriseView.visibility= View.VISIBLE
                binding.layoutFood2.visibility= View.GONE
                binding.layoutNotAddExecrise.visibility= View.GONE


            }
        })
    }

    private fun loadJSONFromAsset(context: Context, jsonFileName: String): String? {
        var json: String? = null
        var `is`: InputStream? = null
        try {
            val manager = context.assets
            Log.d("data", "path $jsonFileName")
            `is` = manager.open(jsonFileName)
            val size = `is`!!.available()
            val buffer = ByteArray(size)
            `is`.read(buffer)
            `is`.close()
            json = String(buffer)
        } catch (ex: IOException) {
            ex.printStackTrace()
            return null
        }

        return json
    }

    fun loadFeeds(context: Context): List<Feed>? {
        try {
            val builder = GsonBuilder()
            val gson = builder.create()
            val array = JSONArray(loadJSONFromAsset(context, "news.json"))
            val feedList = ArrayList<Feed>()
            for (i in 0 until array.length()) {
                val feed = gson.fromJson<Feed>(array.getString(i), Feed::class.java!!)
                feedList.add(feed)
            }
            return feedList
        } catch (e: Exception) {
            Log.d("data", "seedGames parseException $e")
            e.printStackTrace()
            return null
        }

    }

    private fun clickOnBreakfastTab(){
        binding.layoutBreakfast.setOnClickListener(object : View.OnClickListener{
            @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
            override fun onClick(v: View?) {
                foodCategoy="BREAKFAST"
                binding.layoutBreakfast.imageView_breakfast.setBackgroundTintList(getResources().getColorStateList(R.color.white));
                binding.layoutLunch.imageView_lunch.setBackgroundTintList(getResources().getColorStateList(R.color._execrise_img_defult_color));
                binding.layoutDinner.imageView_dinner.setBackgroundTintList(getResources().getColorStateList(R.color._execrise_img_defult_color));
                binding.layoutSnacksDrinks.imageView_snack_drinks.setBackgroundTintList(getResources().getColorStateList(R.color._execrise_img_defult_color));

                binding.layoutBreakfast.textView_breakfast.setTextColor(resources.getColor(R.color.white))
                binding.layoutLunch.textView_lunch.setTextColor(resources.getColor(R.color._execrise_img_defult_color))
                binding.layoutDinner.textView_dinner.setTextColor(resources.getColor(R.color._execrise_img_defult_color))
                binding.layoutSnacksDrinks.textView_snack_drinks.setTextColor(resources.getColor(R.color._execrise_img_defult_color))
                binding.spinnerFood.visibility=View.VISIBLE
                setBreakfastDataInRecyclerView(mContext as EpigenticFoodNewActivity?,foodGroupResponse,foodCategoy)
            }
        })
    }



    private fun clickOnLunchTab(){
        binding.layoutLunch.setOnClickListener(object : View.OnClickListener{
            @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
            override fun onClick(v: View?) {
                foodCategoy="LUNCH"
                binding.layoutBreakfast.imageView_breakfast.setBackgroundTintList(getResources().getColorStateList(R.color._execrise_img_defult_color));
                binding.layoutLunch.imageView_lunch.setBackgroundTintList(getResources().getColorStateList(R.color.white));
                binding.layoutDinner.imageView_dinner.setBackgroundTintList(getResources().getColorStateList(R.color._execrise_img_defult_color));
                binding.layoutSnacksDrinks.imageView_snack_drinks.setBackgroundTintList(getResources().getColorStateList(R.color._execrise_img_defult_color));

                binding.layoutBreakfast.textView_breakfast.setTextColor(resources.getColor(R.color._execrise_img_defult_color))
                binding.layoutLunch.textView_lunch.setTextColor(resources.getColor(R.color.white))
                binding.layoutDinner.textView_dinner.setTextColor(resources.getColor(R.color._execrise_img_defult_color))
                binding.layoutSnacksDrinks.textView_snack_drinks.setTextColor(resources.getColor(R.color._execrise_img_defult_color))
                binding.spinnerFood.visibility=View.VISIBLE
                setBreakfastDataInRecyclerView(mContext as EpigenticFoodNewActivity?,foodGroupResponse,foodCategoy)
            }
        })
    }

    private fun clickOnDinnerTab(){
        binding.layoutDinner.setOnClickListener(object : View.OnClickListener{
            @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
            override fun onClick(v: View?) {
                foodCategoy="DINNER"
                binding.layoutBreakfast.imageView_breakfast.setBackgroundTintList(getResources().getColorStateList(R.color._execrise_img_defult_color));
                binding.layoutLunch.imageView_lunch.setBackgroundTintList(getResources().getColorStateList(R.color._execrise_img_defult_color));
                binding.layoutDinner.imageView_dinner.setBackgroundTintList(getResources().getColorStateList(R.color.white));
                binding.layoutSnacksDrinks.imageView_snack_drinks.setBackgroundTintList(getResources().getColorStateList(R.color._execrise_img_defult_color));

                binding.layoutBreakfast.textView_breakfast.setTextColor(resources.getColor(R.color._execrise_img_defult_color))
                binding.layoutLunch.textView_lunch.setTextColor(resources.getColor(R.color._execrise_img_defult_color))
                binding.layoutDinner.textView_dinner.setTextColor(resources.getColor(R.color.white))
                binding.layoutSnacksDrinks.textView_snack_drinks.setTextColor(resources.getColor(R.color._execrise_img_defult_color))
                binding.spinnerFood.visibility=View.VISIBLE
                setBreakfastDataInRecyclerView(mContext as EpigenticFoodNewActivity?,foodGroupResponse,foodCategoy)
            }
        })
    }

    private fun clickOnSnacksAndDrinks(){
        binding.layoutSnacksDrinks.setOnClickListener(object : View.OnClickListener{
            @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
            override fun onClick(v: View?) {
                foodCategoy="SNACKAS DRINKS"
                binding.layoutBreakfast.imageView_breakfast.setBackgroundTintList(getResources().getColorStateList(R.color._execrise_img_defult_color));
                binding.layoutLunch.imageView_lunch.setBackgroundTintList(getResources().getColorStateList(R.color._execrise_img_defult_color));
                binding.layoutDinner.imageView_dinner.setBackgroundTintList(getResources().getColorStateList(R.color._execrise_img_defult_color));
                binding.layoutSnacksDrinks.imageView_snack_drinks.setBackgroundTintList(getResources().getColorStateList(R.color.white));

                binding.layoutBreakfast.textView_breakfast.setTextColor(resources.getColor(R.color._execrise_img_defult_color))
                binding.layoutLunch.textView_lunch.setTextColor(resources.getColor(R.color._execrise_img_defult_color))
                binding.layoutDinner.textView_dinner.setTextColor(resources.getColor(R.color._execrise_img_defult_color))
                binding.layoutSnacksDrinks.textView_snack_drinks.setTextColor(resources.getColor(R.color.white))

                binding.spinnerFood.visibility=View.GONE
                setBreakfastDataInRecyclerView(mContext as EpigenticFoodNewActivity?,foodGroupResponse,foodCategoy)

               // callSupplementDataServiceForSpinner(mContext as EpigenticFoodNewActivity,foodGroupResponse)
            }
        })
    }



    private fun setBreakfastDataInRecyclerView(mContext: EpigenticFoodNewActivity?,response: FoodCategoryResponse?,foodCategoy: String){

        if(foodCategoy.equals("BREAKFAST")){

            if (spinnerVal.equals("MEAT")) {
                setDataInRecyclerView(response!!.data.breakfast.meatItemList!!, this@EpigenticFoodNewActivity)
            }
            else if (spinnerVal.equals("FISH")) {
                setDataInRecyclerView(response!!.data.breakfast.fishItemList!!, this@EpigenticFoodNewActivity)
            }
            else if (spinnerVal.equals("DAIRY")) {
                setDataInRecyclerView(response!!.data.breakfast.dairyItemList!!, this@EpigenticFoodNewActivity)
            }
            else if (spinnerVal.equals("CEREALS,BREADS")) {
                setDataInRecyclerView(response!!.data.breakfast.cerealBreadsItemList!!, this@EpigenticFoodNewActivity)
            }
            else if (spinnerVal.equals("VEGETABLES")) {
                setDataInRecyclerView(response!!.data.breakfast.vegetablesItemList!!, this@EpigenticFoodNewActivity)
            }
            else if (spinnerVal.equals("FRUIT/NUTS")) {
                setDataInRecyclerView(response!!.data.breakfast.fruitNutsItemList!!, this@EpigenticFoodNewActivity)
            }
        }

        else if(foodCategoy.equals("LUNCH")){

            if (spinnerVal.equals("MEAT")) {
                setDataInRecyclerView(response!!.data.lunch.meatItemList!!, this@EpigenticFoodNewActivity)
            }
            else if (spinnerVal.equals("FISH")) {
                setDataInRecyclerView(response!!.data.lunch.fishItemList!!, this@EpigenticFoodNewActivity)
            }
            else if (spinnerVal.equals("DAIRY")) {
                setDataInRecyclerView(response!!.data.lunch.dairyItemList!!, this@EpigenticFoodNewActivity)
            }
            else if (spinnerVal.equals("CEREALS,BREADS")) {
                setDataInRecyclerView(response!!.data.lunch.cerealBreadsItemList!!, this@EpigenticFoodNewActivity)
            }
            else if (spinnerVal.equals("VEGETABLES")) {
                setDataInRecyclerView(response!!.data.lunch.vegetablesItemList!!, this@EpigenticFoodNewActivity)
            }
            else if (spinnerVal.equals("FRUIT/NUTS")) {
                setDataInRecyclerView(response!!.data.lunch.fruitNutsItemList!!, this@EpigenticFoodNewActivity)
            }
        }


        else if(foodCategoy.equals("DINNER")){

            if (spinnerVal.equals("MEAT")) {
                setDataInRecyclerView(response!!.data.dinner.meatItemList!!, this@EpigenticFoodNewActivity)
            }
            else if (spinnerVal.equals("FISH")) {
                setDataInRecyclerView(response!!.data.dinner.fishItemList!!, this@EpigenticFoodNewActivity)
            }
            else if (spinnerVal.equals("DAIRY")) {
                setDataInRecyclerView(response!!.data.dinner.dairyItemList!!, this@EpigenticFoodNewActivity)
            }
            else if (spinnerVal.equals("CEREALS,BREADS")) {
                setDataInRecyclerView(response!!.data.dinner.cerealBreadsItemList!!, this@EpigenticFoodNewActivity)
            }
            else if (spinnerVal.equals("VEGETABLES")) {
                setDataInRecyclerView(response!!.data.dinner.vegetablesItemList!!, this@EpigenticFoodNewActivity)
            }
            else if (spinnerVal.equals("FRUIT/NUTS")) {
                setDataInRecyclerView(response!!.data.dinner.fruitNutsItemList!!, this@EpigenticFoodNewActivity)
            }
        }


        if(foodCategoy.equals("SNACKAS DRINKS")){
            setDataInRecyclerView(response!!.data.snacksDrink.otherItemList!!, this@EpigenticFoodNewActivity)

        }

    }




    //Get user food data from server
    fun callFoodService(mContext: EpigenticFoodNewActivity) {
        // mContext.showProgressDialog()

        Log.wtf("data","userid"+Utility.getUserID(this));

        val userId = Base64.decode(Utility.getUserID(this), Base64.DEFAULT)
        val decodeUserId = String(userId, charset("UTF-8"))
        val manager = NetworkManager()
        if (manager.isConnectingToInternet(mContext)) {
            manager.createApiRequest(
                ApiUtilis.getAPIService(Constants.FOOD_API).getFoodData(decodeUserId),
                object : ServiceListener<FoodListResponse> {
                    override fun getServerResponse(response: FoodListResponse, requestcode: Int) {
                        // dismissProgressDialog()
                        mContext.dismissProgressDialog()

                        mExpandableView?.removeAllViews();
                        if(response.statusCode.equals("200")&& response.dateGroupModelList.size>0){

                            Collections.reverse(response.dateGroupModelList)
                            if(response.dateGroupModelList!=null){
                                for (dateGroup in response.dateGroupModelList) {
                                    // System.out.print("------date group =" + dateGroup.date_group);
                                    mContext.cate=""
                                    mExpandableView!!.addView(FoodHeadingView(mContext, dateGroup.date_group))
                                    Collections.sort(dateGroup.getInfoList());
                                    for (info in dateGroup.getInfoList()) {
                                        mExpandableView!!.addView(ItemView(mContext,info))


                                    }
                                }
                            }
                        }else{
                            Utility.displayShortSnackBar(
                                binding.parentLayout,
                                resources.getString(R.string.no_data)
                            )
                        }


                    }
                    override fun getError(error: ErrorModel, requestcode: Int) {
                        //binding.progressBar.visibility = View.GONE
                        // dismissProgressDialog()
                        mContext.dismissProgressDialog()
                        Utility.displayShortSnackBar(
                            binding.parentLayout,
                            error.getMessage().toString()
                        )
                    }
                })
        } else {
            mContext.dismissProgressDialog()
            //dismissProgressDialog()
            //binding.progressBar.visibility = View.GONE
            Utility.displayShortSnackBar(
                binding.parentLayout,
                resources.getString(R.string.check_internet)
            )

        }
    }



    @Layout(R.layout.row_food_item)
    class ItemView(private val mContext: EpigenticFoodNewActivity, private val mInfo: FoodInfoModel) {

        @ParentPosition
        private val mParentPosition: Int = 0

        @ChildPosition
        private val mChildPosition: Int = 0

        @com.mindorks.placeholderview.annotations.View(R.id.spinner_food_group_item)
        private val spinnerFoodGroupItem: Spinner? = null


        @com.mindorks.placeholderview.annotations.View(R.id.view_category)
        private val viewCategory: LinearLayout? = null

        @com.mindorks.placeholderview.annotations.View(R.id.view_dropdown)
        private val viewDropdown: LinearLayout? = null


        @com.mindorks.placeholderview.annotations.View(R.id.view_numeric)
        private val viewNewmeric: LinearLayout? = null

        @com.mindorks.placeholderview.annotations.View(R.id.textview_meal_time)
        private val mealTime: TextView? = null

        @com.mindorks.placeholderview.annotations.View(R.id.textview_medication_type)
        private val foodName: TextView? = null

        @com.mindorks.placeholderview.annotations.View(R.id.textview_measurement_unit)
        private val unit: TextView? = null

        @com.mindorks.placeholderview.annotations.View(R.id.textview_unit_value)
        private val unitVal: TextView? = null

        @com.mindorks.placeholderview.annotations.View(R.id.view_delete)
        private val viewDelete: LinearLayout? = null


        @Resolve
        private fun onResolved() {
            Log.wtf("data", "count");

            if(mContext.cate.equals("")){
                viewCategory!!.visibility=View.VISIBLE
                mContext.cate=mInfo.mealTime
                Log.wtf("data"," "+mContext.cate);
                mealTime!!.text = mInfo.mealTime

            }else{
                if(mContext.cate.equals(mInfo.mealTime)){
                    if (viewCategory != null) {
                        viewCategory.visibility=View.GONE
                    }
                }else{
                    viewCategory!!.visibility=View.VISIBLE
                    mealTime!!.text = mInfo.mealTime
                    mContext.cate=mInfo.mealTime
                }
            }

                foodName!!.text=mInfo.foodName

                if(mInfo.valueType.equals("") || mInfo.valueType.equals("NUMERICAL")){
                    unit!!.text=mInfo.measurement
                    viewDropdown!!.visibility=View.GONE
                    viewNewmeric!!.visibility=View.VISIBLE
                }

                else if(mInfo.valueType.equals("DROPDOWN")){
                    var defaultSelPosIntenseSpiner = 0;
                    viewDropdown!!.visibility=View.VISIBLE
                    viewNewmeric!!.visibility=View.GONE


                    var foodGroupData: List<String> = mInfo.measurement.split(",").map { it.trim() }

                    val names = listOf(foodGroupData)
                    for (name in names) {
                        Log.wtf("data", ""+name);
                    }



                    for (foodGroup in foodGroupData) {
                        Log.wtf("data", " all food group="+foodGroup);
                    }

                    mInfo.userValue = mInfo.userValue.trim();

                    defaultSelPosIntenseSpiner = foodGroupData.indexOf(mInfo.userValue);

                    Log.wtf("data", "userval="+mInfo.userValue+" : defalut sel =" + defaultSelPosIntenseSpiner);

                    if(defaultSelPosIntenseSpiner < 0) defaultSelPosIntenseSpiner = 0

                    var spinnerAdapter: CustomDropDownAdapterFoodGroup =CustomDropDownAdapterFoodGroup(mContext!!,foodGroupData.toTypedArray())

                    spinnerFoodGroupItem?.adapter = spinnerAdapter
                    spinnerFoodGroupItem?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                        override fun onItemSelected(
                            parent: AdapterView<*>?,
                            view: android.view.View?,
                            position: Int,
                            id: Long
                        ) {
                            var spinnerValIntense = foodGroupData[position]
                            var text = spinnerFoodGroupItem?.getSelectedItem().toString();
                            // programmatically set the drop down : no need to call web service

                            if(defaultSelPosIntenseSpiner == position){
                                Log.wtf("data", "no need to call API"+defaultSelPosIntenseSpiner+" == "+position);
                            }
                            // user chnage the drop down call the webservice
                            else{
                                Log.wtf("data", "Yes call API"+defaultSelPosIntenseSpiner+" == "+position);

                                 updateExercisedata(spinnerValIntense,mInfo)
                            }

                        }

                        private fun updateExercisedata(spinnerVal: String, mInfo: FoodInfoModel) {

                            mContext.showProgressDialog()
                            val manager = NetworkManager()
                            val params = HashMap<String, String>()
                            params["_id"] = mInfo.id
                            params["user_value"] =spinnerVal
                            //   Toast.makeText(applicationContext,"send", Toast.LENGTH_SHORT).show();

                            if (manager.isConnectingToInternet(mContext)) {
                                manager.createApiRequest(
                                    ApiUtilis.getAPIService(Constants.FOOD_API).updateFood(params),
                                    object : ServiceListener<FoodDeleteResponse> {
                                        override fun getServerResponse(response: FoodDeleteResponse, requestcode: Int) {
                                            if(response.statusCode.equals("200")){
                                                mContext.callFoodService(mContext)
                                            }else{
                                                Utility.displayShortSnackBar(
                                                    mContext.binding.parentLayout,
                                                    response.getMessage().toString()
                                                )
                                            }
                                        }

                                        override fun getError(error: ErrorModel, requestcode: Int) {
                                            mContext.dismissProgressDialog()
                                            //binding.progressBar.visibility = View.GONE
                                            Utility.displayShortSnackBar(
                                                mContext.binding.parentLayout,
                                                error.getMessage().toString()
                                            )
                                        }
                                    })
                            } else {
                                //binding.progressBar.visibility = View.GONE
                                mContext. dismissProgressDialog()
                                 Utility.displayShortSnackBar(
                                     mContext.binding.parentLayout,
                                    mContext.resources.getString(R.string.check_internet)
                                 )
                            }

                        }

                        override fun onNothingSelected(parent: AdapterView<*>) {
                        }
                    }

                    // set default value
                    Log.wtf("data", "mInfo.userValue" +mInfo.userValue);
                    spinnerFoodGroupItem?.setSelection(defaultSelPosIntenseSpiner);

                }

                unitVal!!.text=mInfo.userValue



            viewDelete?.setOnClickListener(object: android.view.View.OnClickListener {
                override fun onClick(v: android.view.View?) {
                    Log.d("data","click a")
                    //  Toast.makeText(mContext,""+mInfo.id, Toast.LENGTH_SHORT).show()
                    callDeleteService(mContext);
                }

                fun callDeleteService(mContext: EpigenticFoodNewActivity) {
                    mContext.showProgressDialog()
                    val manager = NetworkManager()
                    if (manager.isConnectingToInternet(mContext)) {
                        manager.createApiRequest(
                            ApiUtilis.getAPIService(Constants.FOOD_API).deleteFood(mInfo.id),
                            object : ServiceListener<FoodDeleteResponse> {
                                override fun getServerResponse(response: FoodDeleteResponse, requestcode: Int) {
                                    Log.d("data",""+response.statusCode);
                                    if(response.statusCode.equals("200")){
                                         mContext.callFoodService(mContext)
                                    }
                                }
                                override fun getError(error: ErrorModel, requestcode: Int) {
                                    //binding.progressBar.visibility = View.GONE
                                    mContext.dismissProgressDialog()
                                    Utility.displayShortSnackBar(
                                        mContext.binding.parentLayout,
                                        error.getMessage().toString()
                                    )
                                }
                            })
                    } else {
                        mContext.dismissProgressDialog()
                        // binding.progressBar.visibility = View.GONE
                        Utility.displayShortSnackBar(
                            mContext.binding.parentLayout,
                            mContext.resources.getString(R.string.check_internet)
                        )
                    }
                }

            })

        }
    }


    class CustomDropDownAdapterFoodGroup(val context: Context, var listItemsTxt: Array<String>) : BaseAdapter() {


        val mInflater: LayoutInflater = LayoutInflater.from(context)

        override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
            val view: View
            val vh: ItemRowHolder
            if (convertView == null) {
                view = mInflater.inflate(R.layout.row_user_list_food_spinner, parent, false)
                vh = ItemRowHolder(view)
                view?.tag = vh
            } else {
                view = convertView
                vh = view.tag as ItemRowHolder
            }

/*
            // setting adapter item height programatically.
            val params = view.layoutParams
            params.height = 60
            view.layoutParams = params
*/

            vh.label.text = listItemsTxt.get(position)

            return view
        }

        override fun getItem(position: Int): Any? {

            return null

        }

        override fun getItemId(position: Int): Long {

            return 0

        }

        override fun getCount(): Int {
            return listItemsTxt.size
        }

        private class ItemRowHolder(row: View?) {

            val label: TextView

            init {
                this.label = row?.findViewById(R.id.textview_intensity) as TextView
            }
        }
    }


    fun Dialog(context: Context) {
        try {
            progressDialog = ProgressDialog(context)
            progressDialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
            progressDialog!!.setProgressStyle(ProgressDialog.STYLE_SPINNER)
            progressDialog!!.setMessage(getString(R.string.please_wait))
            progressDialog!!.setCancelable(false)
        } catch (ignore: Exception) {
        }

    }

    fun showProgressDialog() {
        try {
            if (progressDialog != null) {
                progressDialog!!.show()
            }
        } catch (ignore: Exception) {
        }

    }

    fun dismissProgressDialog() {
        try {
            if (progressDialog != null) {
                progressDialog!!.dismiss()
            }
        } catch (ignore: Exception) {
        }

    }

}
