package com.muhdo.app.callback

interface TabSelectedListener {

    fun onTabSelected(position: Int)
}