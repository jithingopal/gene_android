package com.muhdo.app.callback

interface SnackbarEventCallback {

    fun onClick()

}