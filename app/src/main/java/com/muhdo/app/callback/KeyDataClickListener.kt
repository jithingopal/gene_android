package com.muhdo.app.callback

import com.muhdo.app.apiModel.keyData.Indicator

interface KeyDataClickListener {


    fun OnItemClickListener(categoryId: String?, sectionId: String?, modeId: String?)

    fun onHeaderItemChange(indicator: Indicator)

    fun dataChangeFromSpinner(title: String?, status: String?, percent: Int?)

}