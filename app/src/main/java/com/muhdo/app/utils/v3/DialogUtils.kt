package com.muhdo.app.utils.v3

import android.app.AlertDialog
import android.content.Context
import androidx.annotation.StringRes

fun Context.showAlertDialog(message: String) {
    val builder = AlertDialog.Builder(this)
    builder.setMessage(message)

    builder.setPositiveButton("OK") { _, _ ->

    }

    val alert = builder.create()
    alert.show()
}

fun Context.showAlertDialog(@StringRes message: Int) {
    val builder = AlertDialog.Builder(this)
    builder.setMessage(message)

    builder.setPositiveButton("OK") { _, _ ->

    }

    val alert = builder.create()
    alert.show()
}