package com.muhdo.app.utils.v3

import android.content.Context
import com.muhdo.app.utils.PreferenceConnector

object Constants {

    const val BASE_URL_V3 = "BaseURL"
    const val GET_USER = "V2/user/get"
    const val UPDATE_USER_POST = "V2/user/update"
    const val READ_GENETIC_OVERVIEW_POST = "V2/genetic-overview"
    const val READ_GENETIC_ACTION_PLAN_POST = "V2/actionplan/getactionplan"
    const val QUESTIONNAIRE_ANSWERS_POST = "V2/user/setanswers"
    const val GET_QUESTIONNAIRE_STATUS = "V2/user/answercount/{user_id}"
    // const val GENO_TYPE_API = "V2/genotype/getgenotypemodes"
    const val HEALTH_INSIGHTS_API = "V2/user/healthInsights"
    const val WORKOUT_QUESTIONNAIRE_API_V3 = "V2/generate-workout-plan"
    const val MEAL_QUESTIONNAIRE_API_V3 = "V2/generate-meal-plan"
    const val CHOOSE_MEAL_API_V3 = "V2/meal-plan/{userID}?choose_your_meal=true"
    const val WORKOUT_INJURY_PREVENTION = "V2/user/warmup"
    const val WORKOUT_OVERVIEW = "V2/training-guide/overview"
    const val EPIGEN_HOME = "V2/biological-age"
    const val EPIGEN_RESULT = "V2/epigenetic-result"
    const val WORKOUT_PLAN = "V2/workout-plan/{user_id}"
    const val MEAL_PLAN_API_V2 = "V2/meal-plan/{user_id}?meal_plan=true"
    const val MEAL_PLAN_API_V2_SECOND = "V2/generate-meal-plan-v2"
    const val DELETE_WORKOUT_PLAN = "V2/delete-workout-plan"
    const val DELETE_MEAL_PLAN = "V2/delete-meal-plan"
    const val KIT_ID_INFO = "V2/kit/get-child-kits"
    const val ADD_CHILD_KIT_ID = "V2/kit/add-child-kits"

    const val LOGIN = "login"
    const val SIGN_UP = "signUp"
    const val USER_ACTION = "userAction"
    const val KIT_ID_VALUE = "kitIdValue"

    const val SECTION_ID = "sectionID"
    const val MODE_ID = "modeID"
    const val MODE_TITLE = "modeTitle"

    const val SIGN_IN_API = "auth/auth/signin_new"
    const val SOCIAL_API = "auth/sociallogin"
    const val LOST_PASSWORD = "auth/auth/forgotPassword"
    const val RESET_PASSWORD = "auth/auth/confirmPassword"
    const val SIGN_UP_API = "auth/auth/signup"
    const val OTP_VERIFY = "auth/auth/verify"
    const val KIT_REGISTRATION_API = "auth/auth/activatedkit/get/{userID}"

    const val CALORIE_API = "meal-plan/dna/mealplan/getmealplangraph/{userID}"
    const val CHOOSE_MEAL_API = "meal-plan/mmealplan/get/{userID}?choose_your_meal=true"
    const val UPDATE_FAVOURITE_API_RECIPE = "meal-plan/dna/favourite/update"
    const val UPDATE_FAVOURITE_API = "workout/favourite/update"

    const val CONTINUE_MEAL_PLAN = "meal-plan/dna/mealplan/update"
    const val YOUR_MEAL_PLAN_API = "meal-plan/mealplan/get/{userID}?meal_plan=true"
    const val FAVOURITE_API = "meal-plan/dna/favourite/getall/{userID}"
    const val RECIPE_API = "meal-plan/dna/recipe/get/{recipeID}"
    //update MEAL_SURVEY
    //const val MEAL_SURVEY = "usersurveyanswer/addmeal"
    const val MEAL_SURVEY = "surveys2/usersurveyanswer/addmealv2"
    // gen meal plan
    const val MEAL_PLAN = "mealplan/generatev2"

    const val COUNTRY_API = "countries/dna/countries/getall"
    const val API_COUNTRY = "apiCountry"
    const val KIT_API = "kitAPI"
    const val GENO_TYPE_API = "genoTypeApi"
    const val KEY_DATA_API = "keyDataApi"
    const val POLLUTION_API = "pollutionApi"
    const val GENO_TYPE_SECTION = "genoTypeSection"
    const val VALIDATE_EMAIL = "validate_email"

    const val API_WORKOUT = "apiWorkout"

    //workout
    const val WORKOUT_PLAN_API = "workout/get/{userID}"
    const val WORKOUT_FAVOURITE_API = "workout/workout/get/{userID}?favourite=true"
    //const val WORKOUT_QUESTIONNAIRE_API = "usersurveyanswer/addworkout"
    //update workout questionnaire api
    const val WORKOUT_QUESTIONNAIRE_API = "surveys2/usersurveyanswer/addworkoutv2"

    const val SEND_KIT_API = "kit-logestics/dna/kitlogistics/kitregistrationandactivation"

    //result

    const val GENOTYPE_API = "V2/genotype/getgenotypemodes"
    const val GENOTYPE_SECTION = "genotype-content/genotype/getGenotypeSection"
    const val AIR_QUALITY_API = "pollution/pollution/airquality"
    //  const val AIR_QUALITY_API_V2 = "pollution2/airqualityv2"
    const val AIR_QUALITY_API_V2 = "pollution2/dna/pollution/airqualityv2"
    const val FORECAST_API = "pollution/pollution/forecasts"
    const val GENERIC_API = "genotype-content/genotype/getgenotypecategorybysectionid/{sectionID}"
    const val USER_EMAIL_VALIDATION = "user/user/GetUserEmail"

    //    dashboard
    const val DASHBOARD_ACTION = "dashboardAction"
    const val DASHBOARD_RESULT = "dashboardResult"
    const val DASHBOARD_MEAL_PLAN = "dashboardMealPlan"
    const val DASHBOARD_WORKOUT = "dashboardWorkout"
    const val DASHBOARD_WORKOUT_MY_PLAN = "dashboardWorkoutMyPlan"
    const val DASHBOARD_EPIGENETIC = "dashboardEpigenetic"
    const val LIFESTYLE_TRACKING = "lifestyleTracking"
    const val DASHBOARD_HEALTHINSIGHT = "DASHBOARD_HEALTHINSIGHT"
    const val DASHBOARD_EPIGENETIC_RESULT = "dashboardEpigeneticResult"
    const val DASHBOARD_GENETIC_OVERVIEW = "dashboardGeneticOverview"
//    const val DASHBOARD_ACTION = "dashboardAction"
//    const val DASHBOARD_ACTION = "dashboardAction"

    const val API_KEYDATA = "dashboard/user/keydatawithdegrees"
//    const val WORKOUT_PLAN_API = "workout/get/{userID}"

    // for base url
    const val AUTH_API = "authApi"
    const val DNA_API = "dnaApi"
    const val MEAL_API = "mealApi"
    //update meal questionire api
    const val MEAL_QUESTIONNAIRE_API = "mealQuestionnaireApi"
    // generate meal plan
    const val MEAL_PLAN_GEN_API = "mealPlanGenApi"
    //activity fragment communication
    const val ACTIVITY_FRAGMENT_MESSAGE = "activity_fragment_message"
    const val WEEKLY_WORKOUT_MESSAGE = "weekly_workout_msg"
    const val ACTIVITY_FRAGMENT_WEEK = "activity_fragment_week"
    const val ACTIVITY_FRAGMENT_WEEK_LIST = "ACTIVITY_FRAGMENT_WEEK_LIST"
    const val ACTIVITY_FRAGMENT_RECIPE = "activity_fragment_recipe"
    const val DAYS_WORKOUT_MESSAGE = "days_workout_msg"
    const val MY_PLAN_DATA = "my_plan_data"
    const val WORKOUT_FAVOURITE_DATA = "workout_data"
    const val WORKOUT_DAY = "workout_day"

    //IDs
    const val RECIPE_ID = "recipeID"

    //Meal_Questionnaire
    const val MEAL_QUESTION_TYPE = "meal_question_type"
    const val MEAL_QUESTION_TYPE_GOAL = "meal_question_type_goal"
    const val MEAL_QUESTION_TYPE_DIET = "meal_question_type_diet"
    const val MEAL_QUESTION_TYPE_FOOD = "meal_question_type_food"
    const val MEAL_QUESTION_TYPE_ALCOHOL = "meal_question_type_alcohol"
    const val MEAL_QUESTION_TYPE_MEALS = "meal_question_type_meals"
    const val MEAL_QUESTION_TYPE_BMI = "meal_question_type_bmi"
    const val MEAL_QUESTION_TYPE_PERSONAL = "meal_question_type_personal"
    const val MEAL_QUESTION_TYPE_ACTIVITY = "meal_question_type_activity"

    //Workout_Questionnaire
    const val WORKOUT_QUESTION_TYPE = "workout_question_type"
    const val WORKOUT_QUESTION_TYPE_GOAL = "workout_question_type_goal"
    const val WORKOUT_QUESTION_TYPE_PERSONAL = "workout_question_type_personal"
    const val WORKOUT_QUESTION_TYPE_BMI = "workout_question_type_bmi"
    const val WORKOUT_QUESTION_TYPE_TRAINING = "workout_question_type_training"
    const val WORKOUT_QUESTION_TYPE_EQUIPMENT = "workout_question_type_equipment"
    const val WORKOUT_QUESTION_TYPE_EXPERIENCE = "workout_question_type_experience"
    const val WORKOUT_QUESTION_TYPE_MEDICAL = "workout_question_type_medical"
    const val WORKOUT_QUESTION_TYPE_DIAGNOSED = "workout_question_type_diagnosed"


    //medication api
    const val MEDICATION_API = "epigen/medicationdata/usersurveyanswer/getbydate/{user_id}"
    // get all medication type
    const val MEDICATION_TYPE_API = "epigen/medicationdata/medications/get"

    // ADD MEDICATION
    const val ADD_MEDICATION = "epigen/medicationdata/usersurveyanswer/add"

    // DELETE MEDICATION
    const val DELETE_MEDICATION = "epigen/medicationdata/usersurveyanswer/delete/{id}"

    // add life style data api
    const val ADD_LIFESTYLE = "epigen/lifestyledata/usersurveyanswer/add"

    // add execrise data api
    const val ADD_EXECRISE = "epigen/exercisedata/usersurveyanswer/add"

    // update execrise data api
    const val UPDATE_EXECRISE = "epigen/exercisedata/usersurveyanswer/update"


    // Get exercise data of user
    const val GET_EXERCISE_DATA = "epigen/exercisedata/usersurveyanswer/getbydate/{user_id}"

    // DELETE EXERCISE
    const val DELETE_EXERCISE = "epigen/exercisedata/usersurveyanswer/delete/{id}"

    // Get supplement data of user
    const val GET_SUPPLEMENT_DATA = "epigen/supplementdata/usersurveyanswer/getbydate/{user_id}"

    // get all supplement type
    const val SUPPLEMENT_TYPE_API = "epigen/supplementdata/supplements/get"

    // add  supplement data
    const val ADD_SUPPLEMENT_DATA = "epigen/supplementdata/usersurveyanswer/add"

    // DELETE SUPPLEMENT
    const val DELETE_SUPPLEMENT = "epigen/supplementdata/usersurveyanswer/delete/{id}"

    // Get food data
    const val GET_FOOD_DATA = "epigen/nutritiondata/usersurveyanswer/getbydate/{user_id}"


    // Get food group data
    const val GET_FOOD_GROUP_DATA = "epigen/nutritiondata/nutritions/getbymealtime"

    // DELETE FOOD
    const val DELETE_FOOD = "epigen/nutritiondata/usersurveyanswer/delete/{id}"


    // add food data api
    const val ADD_FOOD = "epigen/nutritiondata/usersurveyanswer/add"


    // update food data api
    const val UPDATE_FOOD = "epigen/nutritiondata/usersurveyanswer/update"


    // Get user raw data
    //const val GET_USER_RAW_DATA = "user/rawdata/get/{user_id}"
    const val GET_USER_RAW_DATA =
        "kit-raw-data/dna/user/rawdata/get/{user_id}"
    //  "user/rawdata/get/{user_id}"

    const val EPIGENTIC_API = "epigenticApi"
    const val LIFESTYLE_API = "lifestyleApi"
    const val EXECRISE_API = "execriseApi"
    const val FOOD_API = "foodApi"
    const val SUPPLEMENT_API = "supplementApi"
    const val USER_RAW_DATA_API = "userRawDataApi"


    //Epigentic_Lifestyle_Questionnaire
    const val LIFESTYLE_QUESTION_TYPE = "lifestyle_question_type"
    const val LIFESTYLE_QUESTION_TYPE_SLEEP = "lifestyle_question_type_sleep"
    const val LIFESTYLE_QUESTION_TYPE_CRAVINGS = "lifestyle_question_type_cravings"
    const val LIFESTYLE_QUESTION_TYPE_ENERGY = "lifestyle_question_type_energy"
    const val LIFESTYLE_QUESTION_TYPE_PARTY = "lifestyle_question_type_party"
    const val LIFESTYLE_QUESTION_TYPE_GEN_HEALTH = "lifestyle_question_type_gen_health"
    const val LIFESTYLE_QUESTION_TYPE_MENTAL_HEALTH = "lifestyle_question_type_mental_health"
    const val LIFESTYLE_QUESTION_TYPE_SOCIAL = "lifestyle_question_type_social"
    const val LIFESTYLE_QUESTION_TYPE_MOTIVATION = "lifestyle_question_type_motivation"
    const val LIFESTYLE_QUESTION_TYPE_ANXIETY = "lifestyle_question_type_anxiety"
    const val LIFESTYLE_QUESTION_TYPE_STEPS = "lifestyle_question_type_steps"

    //
    const val SLEEP_ALGO_CALCULATES_SCORE =
        "parser-sleep/dna/parser/sleepalgo/calculatescoreupdated/get/{user_id}"


    const val SLEEP_INDEX_BAR_CHART =
        "parser-sleep/dna/parser/sleepalgo/sleepIndexBarsValue/get/{user_id}"

    //const val SLEEP_ALGO_CALCULATES_SCORE = "https://5fmstl21n1.execute-api.eu-west-2.amazonaws.com/live/dna/parser/sleepalgo/calculatescore/get/5c6d0db38786ae68d1626c95"

    const val EPIGENTIC_RESULT_DATA =
        "biological-age/epegentic/biologicalage/get/{kit_id}"

    var lastLoadedResultModeText: String? = ""

    fun getLastLoadedResultText(context: Context): String? {
        if (lastLoadedResultModeText == "" || lastLoadedResultModeText!!.isEmpty()) {
            lastLoadedResultModeText =
                PreferenceConnector.readString(context, PreferenceConnector.MODE_TEXT, "")
        }

        return lastLoadedResultModeText
    }

    fun setLastLoadedResultText(modeText: String) {
        lastLoadedResultModeText = modeText

    }

    val GENETIC_OVERVIEW_TYPES =
        arrayOf("DIET", "VITAMIN", "HEALTH_RISK", "HEALTH_GIFT", "HEALTH_WARNING", "SLEEP")
    val ACTION_PLAN_TYPES =
        arrayOf("DIET", "VITAMIN", "HEALTH_RISK", "HEALTH_WARNING")

    object GENERAL_HEALTH {
        const val REGULARLY_ILL = "regularly_ill"
        const val MONTHLY_PAIN = "monthly_pain"
        const val ACHIEVE_GOAL = "achieve_goal"
    }

    object QUESTIONNAIRE {
        const val GENERAL_HEALTH = "general"
        const val DIET = "diet"
        const val EXERCISE = "exercise"
        const val LIFESTYLE = "lifestyle"
    }

    object EXERCISE {
        const val YOURSELF_ACTIVE = "yourself_active"
        const val EFFORT_LEVEL = "effort_level"
        const val STOPPING_YOU = "stopping_you"
    }

    object LIFESTYLE {
        const val DISEASE = "disease"
        const val PHYSICIAN_NO_EXERCISE = "physician_no_exercise"
        const val MUSCLE_INJURY = "muscle_injury"
        const val PREGNANT = "pregnant"
        const val SLEEP_DURATION = "sleep_duration"
        const val CONSUME_ALCOHOL = "consume_alcohol"
        const val SMOKE = "smoke"
        const val STRESS_LEVEL = "stress_level"
    }

    object DIET {
        const val HEALTHY_DIET = "healthy_diet"
        const val EAT_5_VEG_FRUIT = "eat_5_veg_fruit"
        const val TAKE_VITAMIN_DAILY = "take_vitamin_daily"
        const val FITNESS_SUPPLEMENTS = "fitness_supplements"
        const val DRINK_WATER = "drink_water"
        const val DRINK_COFFEE = "drink_coffee"
        const val VEGAN = "vegan"
        const val VEGETARIAN = "vegetarian"
        const val EAT_FISH = "eat_fish"
        const val OMIT_FOOD = "omit_food"
    }

    object ANSWER {
        const val YES = "YES"
        const val NO = "NO"
        const val SKIP = "SKIP"
    }

    object WEEKS {
        const val WEEKS_6 = "1"
        const val WEEKS_12 = " 2"
        const val WEEKS_18 = " 3"
        const val WEEKS_24 = " 4"
    }

    object HEALTH_INSIGHTS {
        const val STRESS = "STRESS"
        const val SLEEP = "SLEEP"
        const val ANTI_AGEING = "AGEING"
        const val INJURY_RISK = "INJURY_PREVENTION"
        const val MENTAL_HEALTH = "MENTAL_HEALTH"
        const val GUT_HEALTH = "GUT_HEALTH"
        const val HEART_HEALTH = "HEART_HEALTH"
        const val EYE_HEALTH = "EYE_HEALTH"
        const val SKIN_HEALTH = "SKIN_HEALTH"
        const val MUSCLE_HEALTH = "MUSCLE_HEALTH"
    }

    fun getWeekConstantValue(textValue: String): String {
        return when (textValue) {
            "6 WEEKS" -> WEEKS.WEEKS_6
            "12 WEEKS" -> WEEKS.WEEKS_12
            "18 WEEKS" -> WEEKS.WEEKS_18
            else -> WEEKS.WEEKS_24
        }
    }

    fun createExerciseImpactsArray(impactItems: List<String>): ArrayList<String> {
        val items = arrayListOf<String>()
        for (element in impactItems)
            when (element) {
                "FREE TIME" -> items.add("1")
                "KNOWLEDGE" -> items.add("2")
                "WORK COMMITMENTS" -> items.add("3")
                "DIET" -> items.add("4")
                "MOTIVATION" -> items.add("5")
                else -> items.add("6")
            }
        return items
    }

    fun createDiseaseArray(impactItems: List<String>): ArrayList<String> {
        val items = arrayListOf<String>()
        for (element in impactItems)
            when (element) {
                "ALCOHOLISM" -> items.add("1")
                "ALZHEIMER\\'S" -> items.add("2")
                "ANEMIA" -> items.add("3")
                "ARTHRITIS" -> items.add("4")
                "ASTHMA" -> items.add("5")
                "BLOOD DISORDER" -> items.add("6")
                "BONE ISSUES" -> items.add("7")
                "CANCER" -> items.add("8")
                "CATARACTS" -> items.add("9")
                "CRON\\'S" -> items.add("10")
                "DIABETES" -> items.add("11")
                "DEPRESSION" -> items.add("12")
                "DEMENTIA" -> items.add("13")
                "EPILEPSY" -> items.add("14")
                "FIBROMYALGIA" -> items.add("15")
                "HIGH B\\'PRESSURE" -> items.add("19")
                "JAUNDICE" -> items.add("20")
                "LUNG DISEASE" -> items.add("21")
                "PROSTATE DISEASE" -> items.add("22")
                "SEIZURES" -> items.add("23")
                "STROKE" -> items.add("24")
                "THYROID DISEASE" -> items.add("25")
                "TUBERCULOSIS" -> items.add("26")
                "OTHERS" -> items.add("0")
            }
        return items
    }

    fun createSleepHoursConstantValue(sleepItem: String): String {
        return when (sleepItem) {
            "LESS THAN 4 HOURS" -> "1"
            "4 - 6 HOURS" -> "2"
            "6 - 8 HOURS" -> "3"
            else -> "4"
        }
    }

    fun createOmitFoodsArray(impactItems: List<String>): ArrayList<String> {
        val items = arrayListOf<String>()
        for (element in impactItems)
            when (element) {
                "WHEAT/GLUTEN" -> items.add("1")
                "NUTS" -> items.add("2")
                "DAIRY PRODUCTS" -> items.add("3")
                "EGGS" -> items.add("4")
                "SHELLFISH" -> items.add("5")
                "SOY" -> items.add("6")
                "BEEF" -> items.add("7")
                "PORK" -> items.add("8")
                "CHICKEN" -> items.add("9")
            }
        return items
    }

    //Gesture Constants
    const val SWIPE_LEFT_TO_RIGHT = 100
    const val SWIPE_RIGHT_TO_LEFT = 101
}