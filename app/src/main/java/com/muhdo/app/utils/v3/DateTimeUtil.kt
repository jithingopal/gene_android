package com.muhdo.app.utils.v3

import android.util.Log
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*


fun convertDateStringFormat(date: String?, srcFormat: String, resultFormat: String): String? {
    try {
        Log.e("Jithin","Src:"+date+" Src Format:"+srcFormat+" resultFormat"+ resultFormat)
         val originalFormat = SimpleDateFormat(srcFormat, Locale.ENGLISH)
        val targetFormat = SimpleDateFormat(resultFormat)

        val resultDate = originalFormat.parse(date)
        val result =targetFormat.format(resultDate)
        Log.e("Jithin","Src:"+date+" Src Format:"+srcFormat+" resultFormat"+ resultFormat+" result "+result)

        return result
    } catch (e: ParseException) {
        e.printStackTrace()
    }

    return null
}
