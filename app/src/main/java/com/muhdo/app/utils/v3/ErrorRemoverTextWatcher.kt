package com.muhdo.app.utils.v3

import android.text.Editable
import android.text.TextWatcher
import android.widget.EditText

class ErrorRemoverTextWatcher(private val view: EditText) : TextWatcher {
    override fun afterTextChanged(s: Editable?) {
        if (s.toString().isNotEmpty())
            view.error = null
    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
    }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
    }

}