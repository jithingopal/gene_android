package com.muhdo.app.utils

class Events {


    /*// Event used to send message from fragment to activity.
    object FragmentActivityMessage{
        private var message: String? = null

        fun FragmentActivityMessage(message: String) {
            this.message = message
        }

        fun getMessage(): String? {
            return message
        }
    }
*/

    // Event used to send message from activity to fragment.
    class sendPageCount(val page: Int)


    // Event used to send message from activity to fragment.
    class ActivityFragmentMessage(val message: String)

    // Event used to send message from fragment to activity.

    class FragmentActivityMessage(val message1: String, val message2: String, val message: String)

}