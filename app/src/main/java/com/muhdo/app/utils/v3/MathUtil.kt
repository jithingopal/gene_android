package com.muhdo.app.utils.v3

 fun lbsToKg(lbs: Double): Double {
    val kg = 2.205
    return lbs / kg
}

fun kgToLbs(kg: Double): Double {
    val lbs = 2.205
    return lbs * kg
}

fun cmToFt(cm: Double): Double {
    val ft = 30.48
    return cm * ft
}

fun ftToCm(ft: Double): Double {
    val cm = 30.48
    return ft / cm
}
