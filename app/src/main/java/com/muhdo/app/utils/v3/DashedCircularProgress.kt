package com.muhdo.app.utils.v3

import android.annotation.TargetApi
import android.content.Context
import android.graphics.*
import android.os.Build
import android.text.Layout
import android.text.StaticLayout
import android.text.TextPaint
import android.util.AttributeSet
import android.util.Log
import android.view.View
import androidx.core.graphics.withTranslation
import com.muhdo.app.R
import java.util.*
import kotlin.math.roundToInt


class DashedCircularProgress : View {

    private var airQualityIndexValue: String = ""
    private lateinit var topTextValueHeight: Rect
    private val degree = 90
    private lateinit var airPollutionValueHeight: Rect
    private lateinit var topStaticLayout: StaticLayout
    private lateinit var bottomStaticLayout: StaticLayout
    private val START_ANGLE = -90
    private lateinit var circleBounds: RectF
    private var mLayoutHeight = 100
    private var mLayoutWidth = 100
    private var STROKE_WIDTH = 50f
    var progressValue = 0f
    private val airPollution: String =
        resources.getString(R.string.airquality_index_progressbar_top_text)
    private val airQualityText: String = resources.getString(R.string.airquality)
    private val airQualityIndexText: String = resources.getString(R.string.index)
    var bottomPollutionText: String = ""
    /*  set(value) {
          field = value
          invalidate()
      }*/
    private lateinit var airPollutionValue: String

    private lateinit var paint: Paint
    private lateinit var greyCirclePaint: Paint
    private lateinit var topTextPaint: TextPaint
    private lateinit var bottomTextPaint: TextPaint
    private lateinit var airPollutionValuetextPaint: TextPaint

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init(attrs, 0)
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        init(attrs, defStyleAttr)
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int, defStyleRes: Int) : super(
        context,
        attrs,
        defStyleAttr,
        defStyleRes
    ) {
        init(attrs, defStyleRes)
    }

    private fun init(attrs: AttributeSet, defStyleRes: Int) {
        /*  val ta = context.obtainStyledAttributes(
              attrs,
              R.styleable.DashedCircularProgress, defStyleRes, 0
          )
          progressValue =
              ta.getFloat(
                  R.styleable.DashedCircularProgress_dcp_progress,
                  progressValue
              )*/
        paint = Paint(Paint.ANTI_ALIAS_FLAG)
        val dashPath = DashPathEffect(floatArrayOf(100f, 5f), 5.0.toFloat())
        //  paint.color = getRandomColor()
        //  paint.setPathEffect(dashPath)
        paint.style = Paint.Style.STROKE
        paint.strokeWidth = STROKE_WIDTH
        greyCirclePaint = Paint(Paint.ANTI_ALIAS_FLAG)
        greyCirclePaint.color = Color.GRAY
        //  paint.setPathEffect(dashPath)
        greyCirclePaint.style = Paint.Style.STROKE
        greyCirclePaint.strokeWidth = STROKE_WIDTH
        //    ta.recycle()

        bottomTextPaint = TextPaint(Paint.ANTI_ALIAS_FLAG)
        bottomTextPaint.typeface = Typeface.DEFAULT_BOLD

        topTextPaint = TextPaint(Paint.ANTI_ALIAS_FLAG)
        topTextPaint.color = Color.parseColor("#67BCEB")
        topTextPaint.textAlign = Paint.Align.CENTER

        circleBounds = RectF(
            paddingLeft.toFloat() + STROKE_WIDTH * 2,
            paddingTop.toFloat() + STROKE_WIDTH * 2,
            mLayoutWidth.toFloat() - STROKE_WIDTH * 2 - paddingRight,
            mLayoutHeight.toFloat() - STROKE_WIDTH * 2 - paddingBottom
        )
    }

    private fun getRandomColor(): Int {
        val rnd = Random()
        val color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256))
        return color
    }

    override fun onSizeChanged(newWidth: Int, newHeight: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(newWidth, newHeight, oldw, oldh)
        mLayoutWidth = newWidth
        mLayoutHeight = newHeight
        setupBounds()
    }

    private fun setupBounds() {

        val minValue = Math.min(mLayoutWidth, mLayoutHeight)

        val xOffset = mLayoutWidth - minValue
        val yOffset = mLayoutHeight - minValue

        val paddingTop = this.paddingTop + yOffset / 2
        val paddingBottom = this.paddingBottom + yOffset / 2
        val paddingLeft = this.paddingLeft + xOffset / 2
        val paddingRight = this.paddingRight + xOffset / 2

        val width = width
        val height = height

        circleBounds = RectF(
            paddingLeft.toFloat() + STROKE_WIDTH * 2,
            paddingTop.toFloat() + STROKE_WIDTH * 2,
            mLayoutWidth.toFloat() - STROKE_WIDTH * 2 - paddingRight,
            mLayoutHeight.toFloat() - STROKE_WIDTH * 2 - paddingBottom
        )
        topTextPaint.textSize = (0.10 * width / 2).toFloat()

        bottomTextPaint.textSize = (0.13 * width / 2).toFloat()
        bottomTextPaint.color = getColorForProgress(progressValue = progressValue.toInt())

        airPollutionValuetextPaint = TextPaint(Paint.ANTI_ALIAS_FLAG)
        airPollutionValuetextPaint.textSize = (0.40 * width / 2).toFloat()
        airPollutionValuetextPaint.color =
            getColorForProgress(progressValue = progressValue.toInt())
        airPollutionValue = progressValue.toInt().toString()
        airPollutionValuetextPaint.color = Color.rgb(254, 191, 0)
        airPollutionValuetextPaint.textAlign = Paint.Align.CENTER
        airPollutionValuetextPaint.typeface = Typeface.DEFAULT_BOLD
        airPollutionValueHeight = Rect()
        airPollutionValuetextPaint.getTextBounds(
            airPollutionValue,
            0,
            airPollutionValue.length,
            airPollutionValueHeight
        )
        topTextValueHeight = Rect()
        airPollutionValuetextPaint.getTextBounds(
            airQualityText,
            0,
            airQualityText.length,
            topTextValueHeight
        )
        val rect = Rect()
        //  topTextPaint.getTextBounds(airPollution, 0, airPollution.length, rect)
        /*    while (circleBounds.width() / 2 < topTextPaint.measureText(airPollution)) {
                Log.d(
                    "Bounds",
                    "circleBound " + circleBounds.width() / 2 + "text bound " + topTextPaint.measureText(
                        airPollution
                    )
                )
                //     Handler().postDelayed({
                topTextPaint.textSize = topTextPaint.textSize - 1
                invalidate()
                //      }, 2000)
            }*/
        val widthSL: Int = (circleBounds.width() / 2).roundToInt()
        topStaticLayout = StaticLayout(
            airPollution,
            topTextPaint,
            widthSL,
            Layout.Alignment.ALIGN_CENTER,
            1f,
            0f,
            false
        )
        bottomStaticLayout = StaticLayout(
            bottomPollutionText,
            bottomTextPaint,
            widthSL,
            Layout.Alignment.ALIGN_CENTER,
            1f,
            0f,
            false
        )
        /*  val staticLayoutRect = Rect()
          val staticLayoutRect2 = Rect()
          topStaticLayout.getLineBounds(0,staticLayoutRect)
          topStaticLayout.getLineBounds(1,staticLayoutRect2)*/

        /*  if (circleBounds.width() / 2 < rect.width()) {
              Handler().postDelayed({
                  topTextPaint.textSize = 18f
                  // invalidate()
              }, 5000)
          }*/
        /* val textWidth = topTextPaint.measureText(airPollution)
         if (circleBounds.width() /2< textWidth) {
             Handler().postDelayed({
                 topTextPaint.textSize = 18f
                 invalidate()
             }, 5000)
         }*/
        //  topTextPaint.getTextBounds(airPollution,0,airPollution.length,circleBounds.toRect())
        Log.d("Values", "circleBounds")
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        //Grey Circle
        for (greyI in 0 until (360 / 90) + 1) {
            val degreeValue = (degree * greyI)
            for (k in 0 until 4 step 1) {
                canvas.drawArc(
                    circleBounds,
                    (START_ANGLE + degreeValue).toFloat() + (22 * k),
                    if (k == 3) 22f else 20f,
                    false,
                    greyCirclePaint
                )
            }
        }
        //Main ProgressBar here
        val progressAngle = getProgressAngle(progressValue)
        for (i in 0 until (progressAngle / 90f).toInt() + 1) {
            paint.color = getColorForIndex(i)
            var progressToLoad = 90f
            val degreeValue = (degree * i)

            if (i == (progressAngle / 90).toInt()) {
                progressToLoad = (progressAngle - degreeValue)
                for (j in 0 until (progressToLoad / 22.5).toInt() + 1) {
                    Log.d("drawProgressAngle", "test")
                    val drawProgressAngle: Float = when (j) {
                        0 -> {
                            if (progressToLoad < 20f) progressToLoad else 20f
                        }
                        1, 2 -> {
                            if (progressToLoad > (20f * (j + 1))) 20f else (progressToLoad - (20f * j))
                        }
                        else -> {
                            if (progressToLoad > (20f * 4)) 22f else (progressToLoad - (20f * j))
                        }
                    }
                    /* val drawProgressAngle = when {
                         j == 3 -> if (progressToLoad < 22f) progressToLoad else 22f
                         progressToLoad < 20f -> progressToLoad
                         else -> 20f
                     }*/
                    Log.d("drawProgressAngle", drawProgressAngle.toString())
                    canvas.drawArc(
                        circleBounds,
                        (START_ANGLE + degreeValue).toFloat() + (22 * j),
                        drawProgressAngle,
                        false,
                        paint
                    )
                }
            } else {
                for (k in 0 until 4 step 1) {
                    canvas.drawArc(
                        circleBounds,
                        (START_ANGLE + degreeValue).toFloat() + (22 * k),
                        if (k == 3) 22f else 20f,
                        false,
                        paint
                    )
                }
            }
            canvas.drawText(
                airQualityIndexValue,
                (width / 2).toFloat(),
                (height / 2).toFloat() + (airPollutionValueHeight.height() / 2),
                airPollutionValuetextPaint
            )

            canvas.drawText(
                airQualityIndexText,
                (width / 2).toFloat(),
                (height / 2).toFloat() - (airPollutionValueHeight.height() / 2 + topTextValueHeight.height() / 2),
                topTextPaint
            )

            canvas.drawText(
                airQualityText,
                (width / 2).toFloat(),
                (height / 2).toFloat() - (airPollutionValueHeight.height() / 2 + topTextValueHeight.height()),
                topTextPaint
            )
            // -(airPollutionValueHeight.height()/2)
            /*    topStaticLayout.draw(
                    canvas,
                    ((width / 2) - topStaticLayout.width / 2).toFloat(),
                    (height / 2).toFloat() - (airPollutionValueHeight.height() + topStaticLayout.height)
                )*/
            bottomStaticLayout.draw(
                canvas,
                ((width / 2) - bottomStaticLayout.width / 2).toFloat(),
                (height / 2).toFloat() + (airPollutionValueHeight.height())
            )
        }
    }


    private fun getProgressAngle(percent: Float): Float {
        return percent / 100.toFloat() * 360
    }

    fun setProgress(airQualityIndexValue: Int, progressValue: Float, bottomText: String) {
        this.bottomPollutionText = bottomText
        bottomStaticLayout = StaticLayout(
            bottomPollutionText,
            bottomTextPaint,
            (circleBounds.width() / 2).roundToInt(),
            Layout.Alignment.ALIGN_CENTER,
            1f,
            0f,
            false
        )
        this.progressValue = progressValue
        this.airQualityIndexValue = airQualityIndexValue.toString()
        this.airPollutionValue = progressValue.toInt().toString()
        this.airPollutionValuetextPaint.color =
            getColorForProgress(progressValue = progressValue.toInt())
        this.bottomTextPaint.color =
            getColorForProgress(progressValue = progressValue.toInt())
        //  this.layoutParams  = LinearLayout.LayoutParams(abs(progressValue.toInt()),abs(progressValue.toInt()))
        invalidate()
    }

    fun setStrokeWidth(value: Float) {
        this.STROKE_WIDTH = value
        paint.strokeWidth = STROKE_WIDTH
        greyCirclePaint.strokeWidth = STROKE_WIDTH
        invalidate()
    }

    private fun getColorForIndex(index: Int) = when (index) {
        0 -> {
            context.getColor(R.color.first_quarter)
        }
        1 -> {
            context.getColor(R.color.second_quarter)
        }
        2 -> {
            context.getColor(R.color.third_quarter)
        }
        else -> {
            context.getColor(R.color.fourth_quarter)
        }
    }

    private fun getColorForProgress(progressValue: Int) =
        when (progressValue) {
            in 0..25 -> {
                context.getColor(R.color.first_quarter)
            }
            in 26..50 -> {
                context.getColor(R.color.second_quarter
                )
            }
            in 51..75 -> {
                context.getColor(R.color.third_quarter)
            }
            else -> {
                context.getColor(R.color.fourth_quarter)
            }
        }

/*    fun setBottomText(value:String){
        this.bottomPollutionText = value
        invalidate()
    }*/

    private fun StaticLayout.draw(canvas: Canvas, x: Float, y: Float) {
        canvas.withTranslation(x, y) {
            draw(this)
        }
    }
}