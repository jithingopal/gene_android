package com.muhdo.app.utils.v3

import android.animation.Animator
import android.view.View


fun View.toggleVisibility() {
    val view = this
    when (this.visibility) {
        View.VISIBLE -> {
            view.animate()
                .translationY(0f)
                 .alpha(0.0f)
                .setDuration(500)
                .setListener(object : Animator.AnimatorListener {
                    override fun onAnimationRepeat(animation: Animator?) {

                    }

                    override fun onAnimationCancel(animation: Animator?) {
                    }

                    override fun onAnimationStart(animation: Animator?) {
                    }

                    override fun onAnimationEnd(animation: Animator?) {
                       // view.clearAnimation()
                        view.visibility = View.GONE
                    }
                })
        }
        View.GONE -> {
            view.animate()
                .translationY(5f)
                .alpha(1.0f)
                .setDuration(500)
                .setListener(object : Animator.AnimatorListener {
                    override fun onAnimationRepeat(animation: Animator?) {

                    }

                    override fun onAnimationCancel(animation: Animator?) {
                    }

                    override fun onAnimationStart(animation: Animator?) {
                    }

                    override fun onAnimationEnd(animation: Animator?) {
                      //  view.clearAnimation()
                        view.visibility = View.VISIBLE
                    }
                })
        }

    }
}