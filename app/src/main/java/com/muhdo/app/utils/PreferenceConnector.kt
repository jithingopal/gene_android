package com.muhdo.app.utils

import android.content.Context
import android.content.SharedPreferences

object PreferenceConnector {

    val FORCE_UPDATE_STATUS: String="Force_checkout_status"
    val FORCE_LOGOUT_STATUS: String="Force_logout_status"
    val PLAY_STORE_URL: String="Playstore Url"
    val BASE_URL: String="base_url_from_firebase"
    private const val MODE = Context.MODE_PRIVATE
    private const val PREF_NAME = "DNA_App"
    const val MEAL_QUESTION_GENDER = "meal_question_gender"
    const val MEAL_QUESTION_TYPE_GOAL = "meal_question_type_goal"
    const val MEAL_QUESTION_TYPE_DIET = "meal_question_type_diet"
    const val MEAL_QUESTION_TYPE_DIET1 = "meal_question_type_diet1"
    const val MEAL_QUESTION_TYPE_DIET2 = "meal_question_type_diet2"
    const val MEAL_QUESTION_TYPE_FOOD = "meal_question_type_food"
    const val MEAL_QUESTION_TYPE_ALCOHOL = "meal_question_type_alcohol"
    const val MEAL_QUESTION_TYPE_MEALS = "meal_question_type_meals"
    const val MEAL_QUESTION_TYPE_BMI = "meal_question_type_bmi"
    const val MEAL_QUESTION_TYPE_BMI1 = "meal_question_type_bmi1"
    const val MEAL_QUESTION_TYPE_PERSONAL = "meal_question_type_personal"
    const val MEAL_QUESTION_TYPE_ACTIVITY = "meal_question_type_activity"
    const val DNA_USER_ID = "dna_user_id"
    const val USER_LAT = "user_lat"
    const val USER_LONG = "user_long"
    const val REMEMBER_USER_STATUS = "rememberUserStatus"
    //
    const val WORKOUT_QUESTION_TRAINING = "workout_question_training"
    const val WORKOUT_QUESTION_TRAINING_RANGE = "workout_question_training_range"
    const val WORKOUT_QUESTION_EQUIPMENT = "workout_question_equipment"
    const val WORKOUT_QUESTION_EXPERIENCE = "workout_question_experience"
    const val WORKOUT_QUESTION_PREGNANT = "workout_question_pregnant"
    const val WORKOUT_QUESTION_DR = "workout_question_dr"
    const val WORKOUT_QUESTION_INJURY = "workout_question_injury"
    const val WORKOUT_QUESTION_DIAGNOSED = "workout_question_diagnosed"


    const val MEAL_QUESTIONNAIRE_STATUS = "meal_questionnaire_status"
    const val WORKOUT_QUESTIONNAIRE_STATUS = "workout_questionnaire_status"



    // prefs keys
    const val IS_FIRST_TIME_LAUNCH = "IsFirstTimeLaunch"
    const val V3_IS_FIRST_TIME_AT_EPIGEN_HOME = "V3_IS_FIRST_TIME_AT_EPIGEN_HOME"
    const val REMEMBER_USER = "remember_user"
    const val USER_ID = "user_id"
    const val REMEMBER_LOGIN_STATUS = "remember_login_status"
    const val MODE_TEXT = "mode_text"
    const val MODE_ID = "mode_id"
    const val TAB_POS = "tab_pos"
    //age
    const val BIOLOGICAL_AGE = "biological_age"
    const val SLEEP_INDEX = "sleep_index"
    const val KIT_ID = "kit_id"


    //Login email password
    const val EMAIL = "login_email"
    const val PASSWORD = "login_password"

    // for future use
    fun writeString(context: Context, key: String, value: String) {
        getEditor(context).putString(key, value).commit()
    }

    // for future use
    public fun readString(context: Context, key: String, defValue: String): String? {
        return getPreferences(context)!!.getString(key, defValue)
    }


    fun writeBoolean(context: Context, key: String, value: Boolean) {
        getEditor(context).putBoolean(key, value).commit()
    }




    fun readBoolean(context: Context, key: String, defValue: Boolean): Boolean? {
        return getPreferences(context)!!.getBoolean(key, defValue)
    }


    fun writeInt(context: Context, key: String, value: Int) {
        getEditor(context).putInt(key, value).commit()
    }

    fun readInt(context: Context, key: String, defValue: Int): Int? {
        return getPreferences(context)!!.getInt(key, defValue)
    }

    private fun getPreferences(context: Context?): SharedPreferences? {
        return context?.getSharedPreferences(PREF_NAME, MODE)

    }

    fun getEditor(context: Context): SharedPreferences.Editor {
        return getPreferences(context)!!.edit()
    }

}