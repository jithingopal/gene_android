package com.muhdo.app.utils.v3;

import android.util.Log;

import org.jetbrains.annotations.Nullable;

public class TempUtil {
    public static String lastSignedUserGender = "";
    public static int lastSelectedSpinnerPosition = 0;
    public static String[] types = {"SLEEP", "VITAMIN", "HEALTH_RISK", "DIET", "HEALTH_WARNING", "HEALTH_GIFT"};
    public static int currentFragment = 0;
    public static boolean isDietQuestionnaireCompleted = false;
    public static boolean isGeneralHealthQuestionnaireCompleted = false;
    public static boolean isExerciseQuestionnaireCompleted = false;
    public static boolean isLifestyleQuestionnaireCompleted = false;
    public static boolean epiGenDialogAlreadyChangedOnce = false;
    public static final int NO_FRAGMENTS_SELECTED = 1000;
    @Nullable
    public static final String EXTRA_FEED_BACK = "feedback";
    public static int currentSelectedEpigeneticTab = 0;

    public static String markerTextOnChart = "";

    public static int getLastSelectedSpinnerPosition() {
        return lastSelectedSpinnerPosition;
    }

    public static void setLastSelectedSpinnerPosition(int lastSelectedSpinnerPosition) {
        TempUtil.lastSelectedSpinnerPosition = lastSelectedSpinnerPosition;
    }

    public static String getLastSignedUserGender() {
        return lastSignedUserGender;
    }

    public static void setLastSignedUserGender(String lastSignedUserGender) {
        TempUtil.lastSignedUserGender = lastSignedUserGender;
    }

    public static int getCurrentFragment() {
        return currentFragment;
    }

    public static void setCurrentFragment(int currentFragment) {
        TempUtil.currentFragment = currentFragment;
    }

    public static boolean isIsDietQuestionnaireCompleted() {
        return isDietQuestionnaireCompleted;
    }

    public static boolean isIsGeneralHealthQuestionnaireCompleted() {
        return isGeneralHealthQuestionnaireCompleted;
    }

    public static boolean isIsExerciseQuestionnaireCompleted() {
        return isExerciseQuestionnaireCompleted;
    }

    public static boolean isIsLifestyleQuestionnaireCompleted() {
        return isLifestyleQuestionnaireCompleted;


    }

    public static void log(String tag, String text) {
         Log.e(tag,text);
    }
}
