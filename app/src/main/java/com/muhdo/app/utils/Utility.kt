package com.timingsystemkotlin.backuptimingsystem

import android.content.Context
import android.os.Build
import android.text.TextUtils
import android.view.View
import androidx.annotation.RequiresApi
import com.google.android.material.snackbar.Snackbar
import com.muhdo.app.data.MealData
import com.muhdo.app.data.WorkoutData
import com.muhdo.app.room.AppDatabase
import com.muhdo.app.room.MealDAO
import com.muhdo.app.room.WorkoutDAO
import com.muhdo.app.utils.PreferenceConnector
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.text.DecimalFormat
import java.util.*
import java.util.regex.Pattern
import kotlin.collections.HashMap

object Utility {
    var categoryID: String = ""
    var modeID_V3: String? = "5c8c96a79c54381add6884dd"//default "Health & Wellbeing"
    var selectedKeydataCategoryID_V3: String? = ""
    var selectedKeydataSectionID_V3: String? = ""
    var countryID: String = ""
    var sectionId: String = ""
    var categoryStatus: Boolean = false
    var list: ArrayList<Boolean> = arrayListOf()
    var listValue: ArrayList<String> = arrayListOf()
    var idList: ArrayList<String> = arrayListOf()
    var favouriteIdList: ArrayList<String> = arrayListOf()
    var unFavouriteIdList: ArrayList<String> = arrayListOf()
    var diseaseList: MutableList<String> = arrayListOf()
    var sectionList: HashMap<Int, String> = HashMap()
    var modeList: HashMap<Int, String> = HashMap()
    lateinit var jsonList: JSONArray

    fun setFirstTimeLaunch(context: Context, isFirstTime: Boolean) {
        PreferenceConnector.writeBoolean(
            context,
            PreferenceConnector.IS_FIRST_TIME_LAUNCH,
            isFirstTime
        )
    }

    fun isFirstTimeLaunch(context: Context): Boolean {
        return PreferenceConnector.readBoolean(
            context,
            PreferenceConnector.IS_FIRST_TIME_LAUNCH,
            true
        ) ?: true

    }

    fun setFirstTimeEpigenDialog(context: Context, isFirstTime: Boolean) {
        PreferenceConnector.writeBoolean(
            context,
            PreferenceConnector.V3_IS_FIRST_TIME_AT_EPIGEN_HOME,
            isFirstTime
        )
    }

    fun isFirstTimeEpigenDialog(context: Context): Boolean {
        return PreferenceConnector.readBoolean(
            context,
            PreferenceConnector.V3_IS_FIRST_TIME_AT_EPIGEN_HOME,
            true
        ) ?: true

    }


    fun rememberUser(context: Context, isFirstTime: Boolean) {
        PreferenceConnector.writeBoolean(context, PreferenceConnector.REMEMBER_USER, isFirstTime)

    }

    fun isUserRemembered(context: Context): Boolean {
        return PreferenceConnector.readBoolean(context, PreferenceConnector.REMEMBER_USER, true)
            ?: true

    }


    fun setGoalDone(context: Context, isFirstTime: Boolean) {
        PreferenceConnector.writeBoolean(
            context,
            PreferenceConnector.MEAL_QUESTION_TYPE_GOAL,
            isFirstTime
        )
    }

    fun isGoalDone(context: Context): Boolean {
        return PreferenceConnector.readBoolean(
            context,
            PreferenceConnector.MEAL_QUESTION_TYPE_GOAL,
            false
        ) ?: false

    }


    fun setDietDone(context: Context, isFirstTime: Boolean) {
        PreferenceConnector.writeBoolean(
            context,
            PreferenceConnector.MEAL_QUESTION_TYPE_DIET,
            isFirstTime
        )

    }

    fun isDietDone(context: Context): Boolean {
        return PreferenceConnector.readBoolean(
            context,
            PreferenceConnector.MEAL_QUESTION_TYPE_DIET,
            true
        ) ?: true

    }


    fun setDietDone1(context: Context, isFirstTime: Boolean) {
        PreferenceConnector.writeBoolean(
            context,
            PreferenceConnector.MEAL_QUESTION_TYPE_DIET1,
            isFirstTime
        )

    }

    fun isDietDone1(context: Context): Boolean {
        return PreferenceConnector.readBoolean(
            context,
            PreferenceConnector.MEAL_QUESTION_TYPE_DIET1,
            true
        ) ?: true

    }


    fun setDietDone2(context: Context, isFirstTime: Boolean) {
        PreferenceConnector.writeBoolean(
            context,
            PreferenceConnector.MEAL_QUESTION_TYPE_DIET2,
            isFirstTime
        )

    }

    fun isDietDone2(context: Context): Boolean {
        return PreferenceConnector.readBoolean(
            context,
            PreferenceConnector.MEAL_QUESTION_TYPE_DIET2,
            true
        ) ?: true

    }


    fun setFood(context: Context, isFirstTime: Boolean) {
        PreferenceConnector.writeBoolean(
            context,
            PreferenceConnector.MEAL_QUESTION_TYPE_FOOD,
            isFirstTime
        )

    }

    fun setAlcohol(context: Context, isFirstTime: Boolean) {
        PreferenceConnector.writeBoolean(
            context,
            PreferenceConnector.MEAL_QUESTION_TYPE_ALCOHOL,
            isFirstTime
        )

    }

    fun isAlcohol(context: Context): Boolean {
        return PreferenceConnector.readBoolean(
            context,
            PreferenceConnector.MEAL_QUESTION_TYPE_ALCOHOL,
            true
        ) ?: true

    }

    fun setMeals(context: Context, isFirstTime: Boolean) {
        PreferenceConnector.writeBoolean(
            context,
            PreferenceConnector.MEAL_QUESTION_TYPE_MEALS,
            isFirstTime
        )

    }

    fun isMeals(context: Context): Boolean {
        return PreferenceConnector.readBoolean(
            context,
            PreferenceConnector.MEAL_QUESTION_TYPE_MEALS,
            true
        ) ?: true

    }

    fun setBMIIndex(context: Context, isFirstTime: Boolean) {
        PreferenceConnector.writeBoolean(
            context,
            PreferenceConnector.MEAL_QUESTION_TYPE_BMI,
            isFirstTime
        )

    }

    fun isBMIIndex(context: Context): Boolean {
        return PreferenceConnector.readBoolean(
            context,
            PreferenceConnector.MEAL_QUESTION_TYPE_BMI,
            true
        ) ?: true

    }

    fun setBMIIndex1(context: Context, isFirstTime: Boolean) {
        PreferenceConnector.writeBoolean(
            context,
            PreferenceConnector.MEAL_QUESTION_TYPE_BMI1,
            isFirstTime
        )

    }

    fun isBMIIndex1(context: Context): Boolean {
        return PreferenceConnector.readBoolean(
            context,
            PreferenceConnector.MEAL_QUESTION_TYPE_BMI1,
            true
        ) ?: true

    }


    fun setPersonalDetails(context: Context, isFirstTime: Boolean) {
        PreferenceConnector.writeBoolean(
            context,
            PreferenceConnector.MEAL_QUESTION_TYPE_PERSONAL,
            isFirstTime
        )

    }

    fun isPersonalDetails(context: Context): Boolean {
        return PreferenceConnector.readBoolean(
            context,
            PreferenceConnector.MEAL_QUESTION_TYPE_PERSONAL,
            true
        ) ?: true

    }


    fun setGender(context: Context, isFirstTime: Boolean) {
        PreferenceConnector.writeBoolean(
            context,
            PreferenceConnector.MEAL_QUESTION_GENDER,
            isFirstTime
        )

    }

    fun isGender(context: Context): Boolean {
        return PreferenceConnector.readBoolean(
            context,
            PreferenceConnector.MEAL_QUESTION_GENDER,
            true
        ) ?: true

    }

    fun setActivity(context: Context, isFirstTime: Boolean) {
        PreferenceConnector.writeBoolean(
            context,
            PreferenceConnector.MEAL_QUESTION_TYPE_ACTIVITY,
            isFirstTime
        )

    }

    fun isActivity(context: Context): Boolean {
        return PreferenceConnector.readBoolean(
            context,
            PreferenceConnector.MEAL_QUESTION_TYPE_ACTIVITY,
            true
        ) ?: true

    }


    fun saveUserID(context: Context, userID: String) {
        PreferenceConnector.writeString(context, PreferenceConnector.DNA_USER_ID, userID)

    }

    fun getUserID(context: Context): String {
        return PreferenceConnector.readString(context, PreferenceConnector.DNA_USER_ID, "") ?: ""

    }


    fun saveLatLong(context: Context, lat: String, long: String) {
        PreferenceConnector.writeString(context, PreferenceConnector.USER_LAT, lat)
        PreferenceConnector.writeString(context, PreferenceConnector.USER_LONG, long)

    }

    fun getLat(context: Context): String? {
        return PreferenceConnector.readString(context, PreferenceConnector.USER_LAT, "") ?: ""

    }


    fun getLong(context: Context): String? {
        return PreferenceConnector.readString(context, PreferenceConnector.USER_LONG, "") ?: ""

    }

    fun saveUserStatus(context: Context, status: Boolean) {
        PreferenceConnector.writeBoolean(context, PreferenceConnector.REMEMBER_USER_STATUS, status)

    }

    fun getUserStatus(context: Context): Boolean {
        return PreferenceConnector.readBoolean(
            context,
            PreferenceConnector.REMEMBER_USER_STATUS,
            false
        ) ?: false

    }

    fun saveLoginStatus(context: Context, status: Boolean) {
        PreferenceConnector.writeBoolean(context, PreferenceConnector.REMEMBER_LOGIN_STATUS, status)

    }

    fun getLoginStatus(context: Context): Boolean {
        return PreferenceConnector.readBoolean(
            context,
            PreferenceConnector.REMEMBER_LOGIN_STATUS,
            false
        ) ?: false

    }


    // workout
    fun setTraining(context: Context, isFirstTime: Boolean) {
        PreferenceConnector.writeBoolean(
            context,
            PreferenceConnector.WORKOUT_QUESTION_TRAINING,
            isFirstTime
        )
    }

    fun isTraining(context: Context): Boolean {
        return PreferenceConnector.readBoolean(
            context,
            PreferenceConnector.WORKOUT_QUESTION_TRAINING,
            true
        ) ?: true

    }

    fun setKitID(context: Context, kitID: String) {
        PreferenceConnector.writeString(context, PreferenceConnector.KIT_ID, kitID)
            .toString()
    }

    fun getKitID(context: Context): String {
        return PreferenceConnector.readString(context, PreferenceConnector.KIT_ID, "0")
            .toString()
    }

    fun setTrainingRange(context: Context, isFirstTime: Boolean) {
        PreferenceConnector.writeBoolean(
            context,
            PreferenceConnector.WORKOUT_QUESTION_TRAINING_RANGE,
            isFirstTime
        )
    }

    fun isTrainingRange(context: Context): Boolean {
        return PreferenceConnector.readBoolean(
            context,
            PreferenceConnector.WORKOUT_QUESTION_TRAINING_RANGE,
            true
        )
            ?: true

    }


    fun setEquipment(context: Context, isFirstTime: Boolean) {
        PreferenceConnector.writeBoolean(
            context,
            PreferenceConnector.WORKOUT_QUESTION_EQUIPMENT,
            isFirstTime
        )
    }

    fun isEquipment(context: Context): Boolean {
        return PreferenceConnector.readBoolean(
            context,
            PreferenceConnector.WORKOUT_QUESTION_EQUIPMENT,
            false
        ) ?: false

    }

    fun setExperience(context: Context, isFirstTime: Boolean) {
        PreferenceConnector.writeBoolean(
            context,
            PreferenceConnector.WORKOUT_QUESTION_EXPERIENCE,
            isFirstTime
        )
    }

    fun isExperience(context: Context): Boolean {
        return PreferenceConnector.readBoolean(
            context,
            PreferenceConnector.WORKOUT_QUESTION_EXPERIENCE,
            false
        ) ?: false

    }

    fun setPregnant(context: Context, isFirstTime: Boolean) {
        PreferenceConnector.writeBoolean(
            context,
            PreferenceConnector.WORKOUT_QUESTION_PREGNANT,
            isFirstTime
        )
    }

    fun isPregnant(context: Context): Boolean {
        return PreferenceConnector.readBoolean(
            context,
            PreferenceConnector.WORKOUT_QUESTION_PREGNANT,
            false
        ) ?: false

    }

    fun setDR(context: Context, isFirstTime: Boolean) {
        PreferenceConnector.writeBoolean(
            context,
            PreferenceConnector.WORKOUT_QUESTION_DR,
            isFirstTime
        )
    }

    fun isDR(context: Context): Boolean {
        return PreferenceConnector.readBoolean(
            context,
            PreferenceConnector.WORKOUT_QUESTION_DR,
            false
        ) ?: false

    }

    fun setInjured(context: Context, isFirstTime: Boolean) {
        PreferenceConnector.writeBoolean(
            context,
            PreferenceConnector.WORKOUT_QUESTION_INJURY,
            isFirstTime
        )
    }

    fun isInjured(context: Context): Boolean {
        return PreferenceConnector.readBoolean(
            context,
            PreferenceConnector.WORKOUT_QUESTION_INJURY,
            false
        ) ?: false

    }

    fun setDiagnosed(context: Context, isFirstTime: Boolean) {
        PreferenceConnector.writeBoolean(
            context,
            PreferenceConnector.WORKOUT_QUESTION_DIAGNOSED,
            isFirstTime
        )
    }

    fun isDiagnosed(context: Context): Boolean {
        return PreferenceConnector.readBoolean(
            context,
            PreferenceConnector.WORKOUT_QUESTION_DIAGNOSED,
            false
        ) ?: false

    }


    // questionnaire status
    fun setMealStatus(context: Context, isFirstTime: Boolean) {
        PreferenceConnector.writeBoolean(
            context,
            PreferenceConnector.MEAL_QUESTIONNAIRE_STATUS,
            isFirstTime
        )
    }

    fun getMealStatus(context: Context): Boolean {
        return PreferenceConnector.readBoolean(
            context,
            PreferenceConnector.MEAL_QUESTIONNAIRE_STATUS,
            false
        ) ?: false

    }

    fun setWorkoutStatus(context: Context, isFirstTime: Boolean) {
        PreferenceConnector.writeBoolean(
            context,
            PreferenceConnector.WORKOUT_QUESTIONNAIRE_STATUS,
            isFirstTime
        )
    }

    fun getWorkoutStatus(context: Context): Boolean {
        return PreferenceConnector.readBoolean(
            context,
            PreferenceConnector.WORKOUT_QUESTIONNAIRE_STATUS,
            false
        )
            ?: false

    }


    fun getString(context: Context): String {
        return PreferenceConnector.readString(context, PreferenceConnector.IS_FIRST_TIME_LAUNCH, "")
            ?: ""
    }


    fun displayShortSnackBar(view: View, msg: String) {
        try {
            val json = JSONObject(msg)
            val snackBar = Snackbar
                .make(view, json.getString("message"), Snackbar.LENGTH_LONG)
            snackBar.show()
        } catch (e: JSONException) {
            val snackBar = Snackbar
                .make(view, msg, Snackbar.LENGTH_LONG)
            snackBar.show()
        }
    }

    fun displayLongSnackBar(view: View, msg: String) {
        try {
            val json = JSONObject(msg)
            val snackBar = Snackbar
                .make(view, json.getString("message"), Snackbar.LENGTH_LONG)
            snackBar.show()
        } catch (e: JSONException) {
            val snackBar = Snackbar
                .make(view, msg, Snackbar.LENGTH_LONG)
            snackBar.show()
        }
    }

    fun isPasswordValid(input: CharSequence): Boolean {
        val p =
            Pattern.compile(
                //        "^(?=.{8,})(?=.*[A-Z])(?=.*[RecipeIngredient-z])(?=.*[0-9])(?=.*[!@#\$&*]).*$",  Pattern.CASE_INSENSITIVE
                "^(?=.*[a-z])(?=.*[0-9])(?=.*[A-Z])(?=.*[@#!\$%^&+=])(?=\\S+\$).{8,}$"
            )
        val m = p.matcher(input)
        return m.matches()
    }

    @RequiresApi(Build.VERSION_CODES.O)
    fun encodeString(s: String): String {
        var encodedString: String = ""
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            encodedString = Base64.getEncoder().encodeToString(s.toByteArray())
        } else {
            try {
                encodedString =
                    android.util.Base64.encodeToString(s.toByteArray(), android.util.Base64.DEFAULT)
                        .toString()
            } catch (e: IllegalArgumentException) {
                encodedString
            }

        }
        return encodedString

    }


    fun toTitleCase(str: String?): String? {

        if (str == null) {
            return null
        }

        var space = true
        val builder = StringBuilder(str)
        val len = builder.length

        for (i in 0 until len) {
            val c = builder[i]
            if (space) {
                if (!Character.isWhitespace(c)) {
                    // Convert to title case and switch out of whitespace mode.
                    builder.setCharAt(i, Character.toTitleCase(c))
                    space = false
                }
            } else if (Character.isWhitespace(c)) {
                space = true
            } else {
                builder.setCharAt(i, Character.toLowerCase(c))
            }
        }

        return builder.toString()
    }

    fun centimeterToFeet(centimeter: String): String {
        return if (centimeter == "") {
            ""
        } else {
            var feetPart = 0
            var inchesPart = 0
            if (!TextUtils.isEmpty(centimeter)) {
                val dCentimeter = java.lang.Double.valueOf(centimeter)
                feetPart = Math.floor(dCentimeter / 2.54 / 12).toInt()
                println(dCentimeter / 2.54 - feetPart * 12)
                inchesPart = Math.ceil(dCentimeter / 2.54 - feetPart * 12).toInt()
            }
            feetPart.toString() + "." + inchesPart
        }
    }


    fun poundTokG(pounds: String): String {
        return if (pounds == "") {
            ""
        } else {
            val i = pounds.toDouble() * 0.4536
            DecimalFormat("##.##").format(i)
        }
    }

    fun kGToPound(kg: String): String {
        return if (kg == "") {
            ""
        } else {
            val i = kg.toDouble() / 0.4536
            DecimalFormat("##.##").format(i)
        }

    }


    fun convertFeetAndInchesToCentimeter(height: String?): String {
        if (height == "") {
            return ""
        } else {
            var height = height
            if (!height!!.contains(".")) {
                height = "$height.0"
            }
            val parts = height.split(".")
            val feet = parts[0]
            val inches = parts[1]
            var heightInFeet = 0.0
            var heightInInches = 0.0
            try {
                if (feet.trim { it <= ' ' }.isNotEmpty()) {
                    heightInFeet = java.lang.Double.parseDouble(feet)
                }
                if (inches.trim { it <= ' ' }.isNotEmpty()) {
                    heightInInches = java.lang.Double.parseDouble(inches)
                }
            } catch (nfe: NumberFormatException) {

            }
            val i = heightInFeet * 30.48 + heightInInches * 2.54
            return DecimalFormat("##.##").format(i)
        }

    }


    fun formatDate(date: String): String {

        return date.substring(6, 10) + "-" + date.substring(3, 5) + "-" + date.substring(0, 2)
    }


    fun saveMealInfo(context: Context, mealData: MealData) {
        val appDb: AppDatabase? = AppDatabase.getInstance(context)
        if (appDb != null) {
            val mealDAO: MealDAO = appDb.mealData()
            mealDAO.insert(mealData)
        }
    }


    fun deleteMealInfo(context: Context, mealData: MealData) {
        val appDb: AppDatabase? = AppDatabase.getInstance(context)
        if (appDb != null) {
            val mealDAO: MealDAO = appDb.mealData()
            mealDAO.delete(mealData)
        }
    }

    fun getMealData(context: Context): MealData? {
        val appDb: AppDatabase? = AppDatabase.getInstance(context)
        if (appDb != null) {
            val mealDAO: MealDAO = appDb.mealData()
            return mealDAO.getMealData()
        }
        return null
    }


    fun saveWorkoutInfo(context: Context, workoutData: WorkoutData) {
        val appDb: AppDatabase? = AppDatabase.getInstance(context)
        if (appDb != null) {
            val workoutDAO: WorkoutDAO = appDb.workoutData()
            workoutDAO.insert(workoutData)
        }
    }

    fun deleteWorkoutInfo(context: Context, workoutData: WorkoutData) {
        val appDb: AppDatabase? = AppDatabase.getInstance(context)
        if (appDb != null) {
            val workoutDAO: WorkoutDAO = appDb.workoutData()
            workoutDAO.delete(workoutData)
        }
    }

    fun getWorkoutData(context: Context): WorkoutData? {
        val appDb: AppDatabase? = AppDatabase.getInstance(context)
        if (appDb != null) {
            val workoutDAO: WorkoutDAO = appDb.workoutData()
            return workoutDAO.getWorkoutData()
        }
        return null
    }


    fun setBiologicalAge(context: Context, userID: String) {
        PreferenceConnector.writeString(context, PreferenceConnector.DNA_USER_ID, userID)
    }

    fun getBiologicalAge(context: Context): String {
        return PreferenceConnector.readString(context, PreferenceConnector.DNA_USER_ID, "") ?: ""

    }

    fun setSleepIndex(context: Context, sleepIndex: String) {
        PreferenceConnector.writeString(context, PreferenceConnector.SLEEP_INDEX, sleepIndex)

    }

    fun getSleepIndex(context: Context): String {
        return PreferenceConnector.readString(context, PreferenceConnector.SLEEP_INDEX, "") ?: ""

    }


}