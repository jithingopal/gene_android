package com.muhdo.app.utils.v3

import android.view.View

interface DayMealClickListener {

    fun onFavouriteClick(id: String, view: View, status: Boolean)

    fun onCardViewClick(recipeID: String)
}