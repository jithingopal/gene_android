package com.muhdo.app.utils

class EventMessage {

    private lateinit var notification: String

    fun EventMessage(notification: String){
        this.notification = notification
    }

    fun getNotification(): String {
        return notification
    }
}