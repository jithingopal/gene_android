package com.muhdo.app.utils

import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe


object GlobalBus {
    private var sBus: EventBus? = null
    @Subscribe
    fun getBus(): EventBus? {
        if (sBus == null)
            sBus = EventBus.getDefault()
        return this!!.sBus
    }


}