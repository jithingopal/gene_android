package com.muhdo.app.utils;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatDialog;

import com.muhdo.app.R;

public class CustomProgress {
    private static CustomProgress s_m_oCustomProgress;

    public Dialog m_Dialog;
    private AppCompatDialog progressDialog;
    private ImageView img_loading_frame;
    private AnimationDrawable frameAnimation;
    private static Context context;

    private CustomProgress(Context ctx) {
        context = ctx;
    }

    public static CustomProgress getInstance() {
        if (s_m_oCustomProgress == null) {
            s_m_oCustomProgress = new CustomProgress(context);
        }
        return s_m_oCustomProgress;
    }

    public void showProgress(Context m_Context) {
        progressDialog = new AppCompatDialog(m_Context);
        progressDialog.setCancelable(false);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        progressDialog.setContentView(R.layout.prograss_bar_dialog);
        progressDialog.show();

        img_loading_frame = progressDialog.findViewById(R.id.iv_frame_loading);
        frameAnimation = (AnimationDrawable) img_loading_frame.getBackground();
        img_loading_frame.post(new Runnable() {
            @Override
            public void run() {
                frameAnimation.start();
            }
        });
     /* m_Dialog = new Dialog(m_Context, R.style.TransparentProgressDialog);
      m_Dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
      m_Dialog.setContentView(R.layout.prograss_bar_dialog);
      m_ProgressBar = m_Dialog.findViewById(R.id.progressBar);
      m_ProgressBar.setIndeterminateDrawable(m_Context.getResources().getDrawable(R.drawable.circular_progress));
      m_ProgressBar.setVisibility(View.VISIBLE);
      m_ProgressBar.setIndeterminate(true);
      m_Dialog.setCancelable(false);
      m_Dialog.setCanceledOnTouchOutside(false);
      m_Dialog.show();*/
    }

    public void hideDialog() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
        if (img_loading_frame != null) {
            img_loading_frame.setVisibility(View.GONE);
        }
    }
}
