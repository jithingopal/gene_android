package com.muhdo.app.utils.v3

import android.widget.RadioButton
import android.widget.RadioGroup


fun RadioGroup.getSelectedRadioButtonText(): String =
    (findViewById<RadioButton>(this.checkedRadioButtonId)).text.toString()

