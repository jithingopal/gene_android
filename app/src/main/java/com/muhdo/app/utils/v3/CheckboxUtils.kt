package com.muhdo.app.utils.v3

import android.widget.CheckBox


fun atleastOneCheckBoxSelected(vararg checkboxes: CheckBox): Boolean {
    for (i in checkboxes.indices) {
        if (checkboxes[i].isChecked)
            return true
    }
    return false
}

fun getAllSelectedCheckboxTexts(vararg checkboxes: CheckBox): ArrayList<String> {
    val checkboxLabels = arrayListOf<String>()
    for (i in checkboxes.indices) {
        checkboxes[i].takeIf { it.isChecked }?.let { checkBox ->
            checkboxLabels.add(checkBox.text.toString())
        }
    }
    return checkboxLabels
}

fun getNumberOfSelectedCheckbox(vararg checkboxes: CheckBox): String {
    var selectedCount: Int = 0
    for (i in checkboxes.indices) {
        if (checkboxes[i].isChecked)
            selectedCount++
    }
    return selectedCount.toString()
}

fun uncheckAllCheckboxes(vararg checkboxes: CheckBox) {
    for (i in checkboxes.indices) {
        if (checkboxes[i].isChecked)
            checkboxes[i].isChecked = false
    }
}