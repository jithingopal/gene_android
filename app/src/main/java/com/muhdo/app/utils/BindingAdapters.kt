package com.muhdo.app.utils

import android.text.Editable
import android.text.TextWatcher
import android.widget.EditText
import androidx.databinding.BindingAdapter
import com.google.android.material.textfield.TextInputLayout


@BindingAdapter("errorText")
fun setErrorMessage(view: TextInputLayout, errorMessage: String?) {
    view.error = errorMessage
}

@BindingAdapter("textChangeListener")
fun setTextChangeListener(view: EditText, enabled: Boolean?): Unit? {
    return view.addTextChangedListener(object : TextWatcher {
        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
//            view.isErro = s.isEmpty()
        }
        override fun afterTextChanged(s: Editable) {}
    })
}

@BindingAdapter("passwordChangeListener")
fun setPasswordChangeListener(view: EditText, enabled: Boolean): Unit? {
    return view.addTextChangedListener(object : TextWatcher {
        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
            if (s.isNotEmpty()) {
               /* view.setErrorTextAppearance(R.style.error_appearance)
                if (isPasswordValid(s.toString())) {
                    view.isErrorEnabled = false
                } else {
                    view.isErrorEnabled = true
                    setErrorMessage(
                        view,
                        "Password should contain minimun 8 characters, 1 uppercase letter, 1 lowercase letter, 1 number, 1 special character."
                    )
                }*/
            } else {
//                view.set(R.style.error_appearance_gray)
//                view.isErrorEnabled = true
            }
        }

        override fun afterTextChanged(s: Editable) {}
    })
}







