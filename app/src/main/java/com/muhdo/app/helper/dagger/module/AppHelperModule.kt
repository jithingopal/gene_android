package com.muhdo.app.helper.dagger.module


import com.muhdo.app.helper.dagger.AppHelper
import com.muhdo.app.helper.dagger.scope.AppScope
import dagger.Module
import dagger.Provides

@Module
class AppHelperModule {

    @Provides @AppScope
    fun provideAppHelper(): AppHelper {
        return AppHelper()
    }

}