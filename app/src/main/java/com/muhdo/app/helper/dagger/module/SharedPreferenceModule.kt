package com.muhdo.app.helper.dagger.module

import android.content.Context
import com.muhdo.app.helper.dagger.SharedPreferenceHelper
import com.muhdo.app.helper.dagger.scope.AppScope
import dagger.Module
import dagger.Provides

@Module
class SharedPreferenceModule(var context: Context) {

    @Provides @AppScope
    fun provideSharedPreference(): SharedPreferenceHelper {
        val preference = context.getSharedPreferences("dna", Context.MODE_PRIVATE)
        return SharedPreferenceHelper(preference)
    }

}