package com.muhdo.app.helper.dagger.component

import com.muhdo.app.helper.dagger.module.ApiHelperModule
import com.muhdo.app.helper.dagger.scope.FragmentScope
import dagger.Component

@FragmentScope
@Component(modules = arrayOf(ApiHelperModule::class))
interface FragmentComponent {

//    fun inject(app: LoginFragment)

}