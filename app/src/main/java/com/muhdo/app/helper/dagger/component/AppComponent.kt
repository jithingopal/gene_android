package com.muhdo.app.helper.dagger.component


import com.muhdo.app.helper.dagger.module.ApiHelperModule
import com.muhdo.app.helper.dagger.module.AppHelperModule
import com.muhdo.app.helper.dagger.module.SharedPreferenceModule
import com.muhdo.app.helper.dagger.scope.AppScope
import com.muhdo.app.ui.BaseActivity
import dagger.Component

@AppScope
@Component(modules = [AppHelperModule::class, SharedPreferenceModule::class, ApiHelperModule::class])
interface AppComponent {
    fun inject(app: BaseActivity)
}