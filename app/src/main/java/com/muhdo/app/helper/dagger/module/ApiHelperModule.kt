package com.muhdo.app.helper.dagger.module

import android.content.Context
import com.muhdo.app.helper.dagger.ApiHelper
import com.muhdo.app.helper.dagger.scope.AppScope
import dagger.Module
import dagger.Provides

@Module
class ApiHelperModule(var context: Context) {

    @Provides @AppScope
    fun provideApiHelper(): ApiHelper {
        return ApiHelper(context)
    }

}