package com.muhdo.app.interfaces.v3

interface OnHealthInsightsHeaderValuesChanged {
    fun headerValuesChanged(
        infoText: String?,
        riskStatus: String?,
        indicators:Int?,
        tabPosition: Int)
}