package com.muhdo.app.interfaces.v3

interface OnGenoTypeItemSelectedListener {
    fun onItemSelected(id:String?,name:String?)
}