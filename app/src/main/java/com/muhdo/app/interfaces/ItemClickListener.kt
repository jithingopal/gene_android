package com.muhdo.app.interfaces

interface ItemClickListener {

    fun onClick( position: Int)
}