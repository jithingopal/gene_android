package com.muhdo.app.interfaces

import androidx.fragment.app.Fragment

interface IFragmentCallback {
    fun onQuickAccessTabClicked(fragment: Fragment, nav_id: Int, string: String)
}