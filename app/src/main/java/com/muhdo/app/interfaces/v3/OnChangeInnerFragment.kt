package com.muhdo.app.interfaces.v3

import androidx.fragment.app.Fragment

interface OnChangeInnerFragment {
    fun onFragmentChanged(position:Int,fragment:Fragment)
}