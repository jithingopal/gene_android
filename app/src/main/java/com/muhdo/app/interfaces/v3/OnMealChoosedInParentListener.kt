package com.muhdo.app.interfaces.v3

import com.muhdo.app.apiModel.mealModel.Recipe

interface OnMealChoosedInParentListener {
    fun onMealChoosed(listData:ArrayList<Recipe>, mealPosition:Int)
}