package com.muhdo.app.interfaces.v3

import com.muhdo.app.apiModel.mealModel.Recipe

interface OnMealChoosedInChildListener {
    fun onMealChoosed(recipe:Recipe, mealPosition:Int)
}