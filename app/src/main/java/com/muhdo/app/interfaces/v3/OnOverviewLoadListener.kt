package com.muhdo.app.interfaces.v3

interface OnOverviewLoadListener {
    fun onLoadingCompleted( isLoaded:Boolean)
}