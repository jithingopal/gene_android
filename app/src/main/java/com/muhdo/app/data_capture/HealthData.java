package com.muhdo.app.data_capture;


import android.database.Cursor;
import com.samsung.android.sdk.healthdata.HealthConstants;
import com.muhdo.app.data_capture.datareader.TimeRangeDataReader;
import com.muhdo.app.data_capture.datareader.TimeRangeGroupedDataReader;
import com.muhdo.app.data_capture.model.ListWrapper;
import com.muhdo.app.data_capture.model.TimeRange;
import com.samsung.android.sdk.healthdata.HealthPermissionManager;

import java.util.ArrayList;
import java.util.Date;

public class HealthData extends AbstractHealthData {

    public void readSleep(Date from, Date to, String deviceUid, final ResultListener listener) {
        new TimeRangeDataReader(
                mStore,
                HealthConstants.Sleep.HEALTH_DATA_TYPE,
                new String[] {HealthConstants.Sleep.END_TIME, HealthConstants.Sleep.START_TIME},
                from,
                to,
                deviceUid
        ) {

            private ArrayList<TimeRange> list = new ArrayList<TimeRange>();

            @Override
            protected void handleEntry(Cursor c) {
                list.add(new TimeRange(
                        c.getLong(c.getColumnIndex(HealthConstants.Sleep.START_TIME)),
                        c.getLong(c.getColumnIndex(HealthConstants.Sleep.END_TIME))
                ));
            }

            @Override
            protected void handleFinished() {
                listener.onSuccess(new ListWrapper(list));
            }
        }.readData();

    }


    public void readStepCount(final Date from, final Date to, String deviceUid, final ResultListener listener) {
        new TimeRangeGroupedDataReader(
                mStore,
                HealthConstants.StepCount.HEALTH_DATA_TYPE,
                HealthConstants.StepCount.COUNT,
                from,
                to,
                deviceUid
        ) {

            @Override
            protected void handleFinished() {
                if(list.size() == 1) {
                    listener.onSuccess(list.get(0));
                } else {
                    listener.onSuccess(new ListWrapper(list));
                }
            }
        }.readData();

    }

    public void readGlucose(final Date from, final Date to, String deviceUid, final ResultListener listener) {
        new TimeRangeGroupedDataReader(
                mStore,
                HealthConstants.BloodGlucose.HEALTH_DATA_TYPE,
                HealthConstants.BloodGlucose.GLUCOSE,
                from,
                to,
                deviceUid
        ) {

            @Override
            protected void handleFinished() {
                if(list.size() == 1) {
                    listener.onSuccess(list.get(0));
                } else {
                    listener.onSuccess(new ListWrapper(list));
                }
            }
        }.readData();

    }


    public void readWeight(final Date from, final Date to, String deviceUid, final ResultListener listener) {
        new TimeRangeGroupedDataReader(
                mStore,
                HealthConstants.Weight.HEALTH_DATA_TYPE,
                HealthConstants.Weight.WEIGHT,
                from,
                to,
                deviceUid
        ) {

            @Override
            protected void handleFinished() {
                if(list.size() == 1) {
                    listener.onSuccess(list.get(0));
                } else {
                    listener.onSuccess(new ListWrapper(list));
                }
            }
        }.readData();

    }

    public void readWater(final Date from, final Date to, String deviceUid, final ResultListener listener) {
        new TimeRangeGroupedDataReader(
                mStore,
                HealthConstants.WaterIntake.HEALTH_DATA_TYPE,
                HealthConstants.WaterIntake.UNIT_AMOUNT,
                from,
                to,
                deviceUid
        ) {

            @Override
            protected void handleFinished() {
                if(list.size() == 1) {
                    listener.onSuccess(list.get(0));
                } else {
                    listener.onSuccess(new ListWrapper(list));
                }
            }
        }.readData();

    }



    public void readCalories(final Date from, final Date to, String deviceUid, final ResultListener listener) {
        new TimeRangeGroupedDataReader(
                mStore,
                HealthConstants.Exercise.HEALTH_DATA_TYPE,
                HealthConstants.Exercise.CALORIE,
                from,
                to,
                deviceUid
        ) {

            @Override
            protected void handleFinished() {
                if(list.size() == 1) {
                    listener.onSuccess(list.get(0));
                } else {
                    listener.onSuccess(new ListWrapper(list));
                }
            }
        }.readData();

    }

    public void readHeartRate(final Date from, final Date to, String deviceUid, final ResultListener listener) {
        new TimeRangeGroupedDataReader(
                mStore,
                HealthConstants.HeartRate.HEALTH_DATA_TYPE,
                HealthConstants.HeartRate.HEART_BEAT_COUNT,
                from,
                to,
                deviceUid
        ) {

            @Override
            protected void handleFinished() {
                if(list.size() == 1) {
                    listener.onSuccess(list.get(0));
                } else {
                    listener.onSuccess(new ListWrapper(list));
                }
            }
        }.readData();

    }


    public void readDistance(final Date from, final Date to, String deviceUid, final ResultListener listener) {
        new TimeRangeGroupedDataReader(
                mStore,
                HealthConstants.Exercise.HEALTH_DATA_TYPE,
                HealthConstants.Exercise.DISTANCE,
                from,
                to,
                deviceUid
        ) {

            @Override
            protected void handleFinished() {
                if(list.size() == 1) {
                    listener.onSuccess(list.get(0));
                } else {
                    listener.onSuccess(new ListWrapper(list));
                }
            }
        }.readData();

    }

}
