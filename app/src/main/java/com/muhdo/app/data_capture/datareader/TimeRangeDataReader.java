package com.muhdo.app.data_capture.datareader;


import com.samsung.android.sdk.healthdata.HealthConstants;
import com.samsung.android.sdk.healthdata.HealthDataResolver.Filter;
import com.samsung.android.sdk.healthdata.HealthDataStore;
import com.samsung.android.sdk.healthdata.HealthPermissionManager;

import java.util.Date;

public abstract class TimeRangeDataReader extends DataReader {

    public TimeRangeDataReader(HealthDataStore dataStore, String dataType, String[] properties, Date from, Date to, String deviceUid) {
        super(
                dataStore,
                dataType,
                Filter.and(Filter.greaterThanEquals(HealthConstants.Sleep.START_TIME, from.getTime()),
                    Filter.lessThanEquals(HealthConstants.Sleep.START_TIME, to.getTime())),
                properties);

        if(deviceUid != null){
            this.filter = Filter.and(this.filter, Filter.eq(HealthConstants.StepCount.DEVICE_UUID, deviceUid));
        }
    }

    public TimeRangeDataReader(HealthDataStore dataStore, String dataType, HealthPermissionManager.PermissionType read, String[] properties, Date from, Date to, String deviceUid) {
        super(
                dataStore,
                dataType,
               properties);

        if(deviceUid != null){
            this.filter = Filter.and(this.filter, Filter.eq(HealthConstants.StepCount.DEVICE_UUID, deviceUid));
        }
    }

}
