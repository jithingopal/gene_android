package com.muhdo.app.data_capture.model;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.google.gson.Gson;
import com.samsung.android.sdk.healthdata.HealthConnectionErrorResult;
import com.muhdo.app.R;
import com.muhdo.app.data_capture.HealthData;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DataCaptureActivity extends Activity {
    private HealthData healthData;
    private Gson gson = new Gson();
    private Button button1;
    private TextView step, sleep, weight, water, glucose, calorie, heart, distance;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_capture);
        init();
        getData();
    }

    private void init(){
        step = findViewById(R.id.txt_steps);
        sleep =  findViewById(R.id.txt_sleep);
        weight =  findViewById(R.id.txt_weight);
        water =  findViewById(R.id.txt_water);
        glucose = findViewById(R.id.txt_glucose);
        heart = findViewById(R.id.txt_heart);
        calorie =  findViewById(R.id.txt_calories);
        button1 =  findViewById(R.id.btn_sample);
        distance =  findViewById(R.id.txt_distance);
    }
    private void getData(){

        healthData = new HealthData();
        healthData.connect(getApplicationContext(), new HealthData.ConnectionListener() {

            @Override
            public void onPermissionMissing() {
                requestHealthDataPermission();
            }

            @Override
            public void onConnected() {
            }

            @Override
            public void onConnectionFailed(HealthConnectionErrorResult error) {
                showConnectionFailureDialog(error);
            }

            @Override
            public void onDisconnected() {
            }
        });


        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // today's step count
                Calendar from = Calendar.getInstance();
                Calendar to = Calendar.getInstance();
                setStartOfDay(from);
                to.add(Calendar.DAY_OF_WEEK, 1);
                setStartOfDay(to);

                //read Steps
                healthData.readStepCount(from.getTime(), to.getTime(), "", new HealthData.ResultListener() {
                    @Override
                    public void onSuccess(Object result) {

                        try {
                            JSONObject jObj = new JSONObject(gson.toJson(result));
                            step.setText(jObj.getString("count"));
                            Log.d("data","sleep samsung" + jObj.getString("count"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Log.d("data","sleep result=>  " + gson.toJson(result));
                    }

                });

                // read sleep
                healthData.readSleep(from.getTime(), to.getTime(), "", new HealthData.ResultListener() {
                    @Override
                    public void onSuccess(Object result) {
                        String startDate = "", endDate = "";
                        try {
                            JSONObject jObj = new JSONObject(gson.toJson(result));
                            JSONArray a = jObj.getJSONArray("collection");
                            JSONObject item = a.getJSONObject(0);
                            startDate = item.getString("from");
                            endDate = item.getString("to");

                            Log.d("data","samsung" +gson.toJson(result));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MMM dd, yyyy hh:mm:ss aa");
                        try {
                            Date date1 = simpleDateFormat.parse(startDate);
                            Date date2 = simpleDateFormat.parse(endDate);
                            sleep.setText("" + getHours(date1, date2) + " hrs");

                        } catch (ParseException e) {
                            e.printStackTrace();
                        }

                    }

                });


                // Weight
                healthData.readWeight(from.getTime(), to.getTime(), "", new HealthData.ResultListener() {
                    @Override
                    public void onSuccess(Object result) {
                        try {
                            JSONObject jObj = new JSONObject(gson.toJson(result));
                            JSONArray a = jObj.getJSONArray("collection");
                            JSONObject item = a.getJSONObject(0);
                            weight.setText(item.getString("count") + " kg");

                            System.out.println("Data2=>  " + jObj.getString("count"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        System.out.println("Data4=>  " + gson.toJson(result));
                    }

                });

                // read heart beat
                to.add(Calendar.DAY_OF_WEEK, 1);
                setMidOfDay(to);
                healthData.readHeartRate(from.getTime(), to.getTime(), "", new HealthData.ResultListener() {
                    @Override
                    public void onSuccess(Object result) {
                        try {
                            JSONObject jObj = new JSONObject(gson.toJson(result));
                            JSONArray a = jObj.getJSONArray("collection");
                            JSONObject item = a.getJSONObject(0);
                            heart.setText(item.getString("count") + "");

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                });


                // water
                healthData.readWater(from.getTime(), to.getTime(), "", new HealthData.ResultListener() {
                    @Override
                    public void onSuccess(Object result) {
                        try {
                            JSONObject jObj = new JSONObject(gson.toJson(result));

                            JSONArray a = jObj.getJSONArray("collection");
                            JSONObject item = a.getJSONObject(0);
                            water.setText(item.getString("count") + " ml");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        System.out.println("Data6=>  " + gson.toJson(result));
                    }

                });

                // glucose
                healthData.readGlucose(from.getTime(), to.getTime(), "", new HealthData.ResultListener() {
                    @Override
                    public void onSuccess(Object result) {
                        try {
                            JSONObject jObj = new JSONObject(gson.toJson(result));
                            JSONArray a = jObj.getJSONArray("collection");
                            JSONObject item = a.getJSONObject(0);
                            glucose.setText(item.getString("count") + " mm/l");

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                });


                // read calories
                to.add(Calendar.DAY_OF_WEEK, 1);
                setMidOfDay(to);
                healthData.readCalories(from.getTime(), to.getTime(), "", new HealthData.ResultListener() {
                    @Override
                    public void onSuccess(Object result) {
                        try {
                            JSONObject jObj = new JSONObject(gson.toJson(result));
                            JSONArray a = jObj.getJSONArray("collection");
                            JSONObject item = a.getJSONObject(0);
                            calorie.setText(item.getString("count") + " cal");

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                });


                // read distance
                to.add(Calendar.DAY_OF_WEEK, 1);
                setMidOfDay(to);
                healthData.readDistance(from.getTime(), to.getTime(), "", new HealthData.ResultListener() {
                    @Override
                    public void onSuccess(Object result) {
                        try {
                            JSONObject jObj = new JSONObject(gson.toJson(result));
                            JSONArray a = jObj.getJSONArray("collection");
                            JSONObject item = a.getJSONObject(0);
                            distance.setText(item.getString("count") + "");

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                });


            }


        });
    }



    void setStartOfDay(Calendar cal) {
        cal.set(Calendar.MILLISECOND, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.HOUR_OF_DAY, 0);
    }

    void setMidOfDay(Calendar cal) {
        cal.set(Calendar.MILLISECOND, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.HOUR_OF_DAY, 12);
    }

    void requestHealthDataPermission() {
        healthData.requestPermission(this, new HealthData.PermissionListener() {
            @Override
            public void onPermissionGranted() {

            }

            @Override
            public void onPermissionDenied() {

            }
        });
    }

    private void showConnectionFailureDialog(final HealthConnectionErrorResult error) {

        AlertDialog.Builder alert = new AlertDialog.Builder(getApplicationContext());
        //mConnError = error;
        String message = "Connection with Samsung Health is not available";

        if (error.hasResolution()) {
            switch (error.getErrorCode()) {
                case HealthConnectionErrorResult.PLATFORM_NOT_INSTALLED:
                    message = "Please install Samsung Health";
                    break;
                case HealthConnectionErrorResult.OLD_VERSION_PLATFORM:
                    message = "Please upgrade Samsung Health";
                    break;
                case HealthConnectionErrorResult.PLATFORM_DISABLED:
                    message = "Please enable Samsung Health";
                    break;
                case HealthConnectionErrorResult.USER_AGREEMENT_NEEDED:
                    message = "Please agree with Samsung Health policy";
                    break;
                default:
                    message = "Please make Samsung Health available";
                    break;
            }
        }

        alert.setMessage(message);

        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                if (error.hasResolution()) {
                    error.resolve(DataCaptureActivity.this);
                }
            }
        });

        if (error.hasResolution()) {
            alert.setNegativeButton("Cancel", null);
        }

        alert.show();
    }


    public long getHours(Date startDate, Date endDate) {
        //milliseconds
        long different = endDate.getTime() - startDate.getTime();

        System.out.println("startDate : " + startDate);
        System.out.println("endDate : " + endDate);
        System.out.println("different : " + different);

        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;

        long elapsedHours = different / hoursInMilli;

        return elapsedHours;

    }

}
