package com.muhdo.app.apiModel.v3

data class UpdateUserProfileReq(
    val user_id: String? = null,
    val birthday: String? = null,
    val gender: String,
    val weight: String? = null,
    val weight_unit: String? = null,
    val height: String? = null,
    val height_unit: String? = null
)