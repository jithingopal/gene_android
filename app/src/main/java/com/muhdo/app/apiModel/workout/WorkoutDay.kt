package com.muhdo.app.apiModel.workout

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class WorkoutDay {


    @SerializedName("_id")
    @Expose
    private var id: String? = null
    @SerializedName("goal_type")
    @Expose
    private var goalType: String? = null
    @SerializedName("day_count")
    @Expose
    private var dayCount: Int? = null
    @SerializedName("day")
    @Expose
    private var day: Int? = null
    @SerializedName("exercise")
    @Expose
    private var exercise: Int? = null
    @SerializedName("if_applicable")
    @Expose
    private var ifApplicable: Int? = null
    @SerializedName("repetition")
    @Expose
    private var repetition: String? = null
    @SerializedName("sets")
    @Expose
    private var sets: Int? = null
    @SerializedName("weight")
    @Expose
    private var weight: String? = null
    @SerializedName("rest")
    @Expose
    private var rest: Any? = null
    @SerializedName("exercises")
    @Expose
    private var exercises: Exercises? = null
    @SerializedName("newexercise")
    @Expose
    private var newexercise: Int? = null
    @SerializedName("equipmentWeight")
    @Expose
    private var equipmentWeight: Int? = null
    @SerializedName("session")
    @Expose
    private var session: Int? = null
    @SerializedName("video_link")
    @Expose
    private var videoLink: String? = null

    fun getId(): String? {
        return id
    }

    fun setId(id: String) {
        this.id = id
    }

    fun getGoalType(): String? {
        return goalType
    }

    fun setGoalType(goalType: String) {
        this.goalType = goalType
    }

    fun getDayCount(): Int? {
        return dayCount
    }

    fun setDayCount(dayCount: Int?) {
        this.dayCount = dayCount
    }

    fun getDay(): Int? {
        return day
    }

    fun setDay(day: Int?) {
        this.day = day
    }

    fun getExercise(): Int? {
        return exercise
    }

    fun setExercise(exercise: Int?) {
        this.exercise = exercise
    }

    fun getIfApplicable(): Int? {
        return ifApplicable
    }

    fun setIfApplicable(ifApplicable: Int?) {
        this.ifApplicable = ifApplicable
    }

    fun getRepetition(): String? {
        return repetition
    }

    fun setRepetition(repetition: String) {
        this.repetition = repetition
    }

    fun getSets(): Int? {
        return sets
    }

    fun setSets(sets: Int?) {
        this.sets = sets
    }

    fun getWeight(): String? {
        return weight
    }

    fun setWeight(weight: String) {
        this.weight = weight
    }

    fun getRest(): Any? {
        return rest
    }

    fun setRest(rest: Any?) {
        this.rest = rest
    }

    fun getExercises(): Exercises? {
        return exercises
    }

    fun setExercises(exercises: Exercises) {
        this.exercises = exercises
    }

    fun getNewexercise(): Int? {
        return newexercise
    }

    fun setNewexercise(newexercise: Int?) {
        this.newexercise = newexercise
    }

    fun getEquipmentWeight(): Int? {
        return equipmentWeight
    }

    fun setEquipmentWeight(equipmentWeight: Int?) {
        this.equipmentWeight = equipmentWeight
    }

    fun getSession(): Int? {
        return session
    }

    fun setSession(session: Int?) {
        this.session = session
    }

    fun getVideoLink(): String? {
        return videoLink
    }

    fun setVideoLink(videoLink: String) {
        this.videoLink = videoLink
    }

}