package com.muhdo.app.apiModel.login

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class LoginErrorModel {


    @SerializedName("statusCode")
    @Expose
    private var statusCode: Any? = null
    @SerializedName("headers")
    @Expose
    private var headers: Any? = null
    @SerializedName("data")
    @Expose
    private var data: Any? = null
    @SerializedName("body")
    @Expose
    private var body: Any? = null

    fun getStatusCode(): Any? {
        return statusCode
    }

    fun setStatusCode(statusCode: Any?) {
        this.statusCode = statusCode
    }

    fun getHeaders(): Any? {
        return headers
    }

    fun setHeaders(headers: Any) {
        this.headers = headers
    }

    fun getData(): Any? {
        return data
    }

    fun setData(data: Any?) {
        this.data = data
    }

    fun getBody(): Any? {
        return body
    }

    fun setBody(body: Any?) {
        this.body = body
    }

}