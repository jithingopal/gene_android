package com.muhdo.app.apiModel.signupSignIn

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class User {


    @SerializedName("username")
    @Expose
    private var username: String? = null
    @SerializedName("pool")
    @Expose
    private var pool: Pool? = null
    @SerializedName("Session")
    @Expose
    private var session: Any? = null
    @SerializedName("client")
    @Expose
    private var client: Client_? = null
    @SerializedName("signInUserSession")
    @Expose
    private var signInUserSession: Any? = null
    @SerializedName("authenticationFlowType")
    @Expose
    private var authenticationFlowType: String? = null
    @SerializedName("keyPrefix")
    @Expose
    private var keyPrefix: String? = null
    @SerializedName("userDataKey")
    @Expose
    private var userDataKey: String? = null

    fun getUsername(): String? {
        return username
    }

    fun setUsername(username: String) {
        this.username = username
    }

    fun getPool(): Pool? {
        return pool
    }

    fun setPool(pool: Pool) {
        this.pool = pool
    }

    fun getSession(): Any? {
        return session
    }

    fun setSession(session: Any) {
        this.session = session
    }

    fun getClient(): Client_? {
        return client
    }

    fun setClient(client: Client_) {
        this.client = client
    }

    fun getSignInUserSession(): Any? {
        return signInUserSession
    }

    fun setSignInUserSession(signInUserSession: Any) {
        this.signInUserSession = signInUserSession
    }

    fun getAuthenticationFlowType(): String? {
        return authenticationFlowType
    }

    fun setAuthenticationFlowType(authenticationFlowType: String) {
        this.authenticationFlowType = authenticationFlowType
    }

    fun getKeyPrefix(): String? {
        return keyPrefix
    }

    fun setKeyPrefix(keyPrefix: String) {
        this.keyPrefix = keyPrefix
    }

    fun getUserDataKey(): String? {
        return userDataKey
    }

    fun setUserDataKey(userDataKey: String) {
        this.userDataKey = userDataKey
    }
}