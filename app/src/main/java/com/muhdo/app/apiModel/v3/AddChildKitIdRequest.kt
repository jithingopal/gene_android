package com.muhdo.app.apiModel.v3

data class AddChildKitIdRequest(
    val child_kit_id: String?=null,
    val user_id: String?=null
)