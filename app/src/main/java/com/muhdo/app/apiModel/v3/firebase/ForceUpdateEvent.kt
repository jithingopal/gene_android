package com.muhdo.app.apiModel.v3.firebase

data class ForceUpdateEvent(
    var message: String? = "",
    var url: String? = "",
    var status: Boolean? = false
)