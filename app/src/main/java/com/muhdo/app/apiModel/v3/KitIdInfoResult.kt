package com.muhdo.app.apiModel.v3

import com.google.gson.JsonObject

data class KitIdInfoResult(
    val statusCode: Int,
    val message: String,
    val data: KitID_DATA
)
data class KitID_DATA(
    val status: String,
    val _id: String,
    val kit_id: String,
    val date_of_scanning: String,
    val children:MutableList<KitID>?
)