package com.muhdo.app.apiModel.signupSignIn

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.muhdo.app.data.User


class Body {

    @SerializedName("user")
    @Expose
    private var user: User? = null
    @SerializedName("userConfirmed")
    @Expose
    private var userConfirmed: Boolean? = null
    @SerializedName("userSub")
    @Expose
    private var userSub: String? = null

    fun getUser(): User? {
        return user
    }

    fun setUser(user: User) {
        this.user = user
    }

    fun getUserConfirmed(): Boolean? {
        return userConfirmed
    }

    fun setUserConfirmed(userConfirmed: Boolean?) {
        this.userConfirmed = userConfirmed
    }

    fun getUserSub(): String? {
        return userSub
    }

    fun setUserSub(userSub: String) {
        this.userSub = userSub
    }
}