package com.muhdo.app.apiModel.mealModel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class FavoriteListModelData {


    @SerializedName("is_deleted")
    @Expose
    private var isDeleted: Boolean? = null
    @SerializedName("is_favourite")
    @Expose
    private var isFavourite: Boolean? = null
    @SerializedName("_id")
    @Expose
    private var id: String? = null
    @SerializedName("recipe_id")
    @Expose
    private var recipeId: RecipeId? = null
    @SerializedName("user_id")
    @Expose
    private var userId: String? = null
    @SerializedName("__v")
    @Expose
    private var v: Int? = null
    @SerializedName("createdAt")
    @Expose
    private var createdAt: String? = null
    @SerializedName("updatedAt")
    @Expose
    private var updatedAt: String? = null

    fun getIsDeleted(): Boolean? {
        return isDeleted
    }

    fun setIsDeleted(isDeleted: Boolean?) {
        this.isDeleted = isDeleted
    }

    fun getIsFavourite(): Boolean? {
        return isFavourite
    }

    fun setIsFavourite(isFavourite: Boolean?) {
        this.isFavourite = isFavourite
    }

    fun getId(): String? {
        return id
    }

    fun setId(id: String) {
        this.id = id
    }

    fun getRecipeId(): RecipeId? {
        return recipeId
    }

    fun setRecipeId(recipeId: RecipeId) {
        this.recipeId = recipeId
    }

    fun getUserId(): String? {
        return userId
    }

    fun setUserId(userId: String) {
        this.userId = userId
    }

    fun getV(): Int? {
        return v
    }

    fun setV(v: Int?) {
        this.v = v
    }

    fun getCreatedAt(): String? {
        return createdAt
    }

    fun setCreatedAt(createdAt: String) {
        this.createdAt = createdAt
    }

    fun getUpdatedAt(): String? {
        return updatedAt
    }

    fun setUpdatedAt(updatedAt: String) {
        this.updatedAt = updatedAt
    }


}