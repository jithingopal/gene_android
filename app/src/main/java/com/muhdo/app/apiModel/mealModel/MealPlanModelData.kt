package com.muhdo.app.apiModel.mealModel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.muhdo.app.apiModel.mealModel.daysModel.Monday


class MealPlanModelData {


    @SerializedName("Friday")
    @Expose
    private var friday: List<Monday>? = null
    @SerializedName("Sunday")
    @Expose
    private var sunday: List<Monday>? = null
    @SerializedName("Thursday")
    @Expose
    private var thursday: List<Monday>? = null
    @SerializedName("Monday")
    @Expose
    private var monday: List<Monday>? = null
    @SerializedName("Saturday")
    @Expose
    private var saturday: List<Monday>? = null
    @SerializedName("Tuesday")
    @Expose
    private var tuesday: List<Monday>? = null
    @SerializedName("Wednesday")
    @Expose
    private var wednesday: List<Monday>? = null

    fun getFriday(): List<Monday>? {
        return friday
    }

    fun setFriday(friday: List<Monday>) {
        this.friday = friday
    }

    fun getSunday(): List<Monday>? {
        return sunday
    }

    fun setSunday(sunday: List<Monday>) {
        this.sunday = sunday
    }

    fun getThursday(): List<Monday>? {
        return thursday
    }

    fun setThursday(thursday: List<Monday>) {
        this.thursday = thursday
    }

    fun getMonday(): List<Monday>? {
        return monday
    }

    fun setMonday(monday: List<Monday>) {
        this.monday = monday
    }

    fun getSaturday(): List<Monday>? {
        return saturday
    }

    fun setSaturday(saturday: List<Monday>) {
        this.saturday = saturday
    }

    fun getTuesday(): List<Monday>? {
        return tuesday
    }

    fun setTuesday(tuesday: List<Monday>) {
        this.tuesday = tuesday
    }

    fun getWednesday(): List<Monday>? {
        return wednesday
    }

    fun setWednesday(wednesday: List<Monday>) {
        this.wednesday = wednesday
    }


}