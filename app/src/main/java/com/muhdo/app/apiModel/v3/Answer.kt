package com.muhdo.app.apiModel.v3

data class Answer(
    val answer: List<String>,
    val question: String
)