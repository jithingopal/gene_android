package com.muhdo.app.apiModel.v3

import com.google.gson.JsonObject

data class KitID(
    val status: String,
    val _id: String,
    val kit_id: String,
    val date_of_scanning: String
)