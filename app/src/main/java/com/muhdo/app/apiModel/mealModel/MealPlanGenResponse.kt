package com.muhdo.app.apiModel.mealModel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class MealPlanGenResponse {

    @SerializedName("statusCode")
    @Expose
    private var code: Int? = null

    @SerializedName("message")
    @Expose
    private var message: String? = null

    @SerializedName("data")
    @Expose
    private var data: MealPlanData? = null

    fun getCode(): Int? {
        return code
    }

    fun setCode(code: Int?) {
        this.code = code
    }

    fun getMessage(): String? {
        return message
    }

    fun setMessage(message: String) {
        this.message = message
    }

    fun getData(): MealPlanData? {
        return data
    }

    fun setData(data: MealPlanData) {
        this.data = data
    }
}

class MealPlanData {

    @SerializedName("macros")
    @Expose
    var macrosData: Macros? = null
}

class Macros{

    @SerializedName("carbs")
    @Expose
    var carbs: Int? = null

    @SerializedName("satFats")
    @Expose
    var satFats: Float? = null

    @SerializedName("unsatFats")
    @Expose
    var unsatFats: Int? = null

    @SerializedName("protein")
    @Expose
    var protein: Float? = null


}
