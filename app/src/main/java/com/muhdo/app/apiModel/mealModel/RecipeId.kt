package com.muhdo.app.apiModel.mealModel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class RecipeId {


    @SerializedName("_id")
    @Expose
    private var id: String? = null
    @SerializedName("name")
    @Expose
    private var name: String? = null
    @SerializedName("image")
    @Expose
    private var image: String? = null
    @SerializedName("bucket")
    @Expose
    private var bucket: String? = null

    fun getId(): String? {
        return id
    }

    fun setId(id: String) {
        this.id = id
    }

    fun getName(): String? {
        return name
    }

    fun setName(name: String) {
        this.name = name
    }

    fun getImage(): String? {
        return image
    }

    fun setImage(image: String) {
        this.image = image
    }

    fun getBucket(): String? {
        return bucket
    }

    fun setBucket(bucket: String) {
        this.bucket = bucket
    }
}