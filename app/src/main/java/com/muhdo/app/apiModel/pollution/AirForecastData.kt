package com.muhdo.app.apiModel.pollution

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class AirForecastData {

    @SerializedName("day")
    @Expose
    private var day: String? = null
    @SerializedName("aqi")
    @Expose
    private var aqi: Int? = null
    @SerializedName("category")
    @Expose
    private var category: String? = null
    @SerializedName("color")
    @Expose
    private var color: String? = null
    @SerializedName("method")
    @Expose
    private var method: String? = null

    fun getDay(): String? {
        return day
    }

    fun setDay(day: String) {
        this.day = day
    }

    fun getAqi(): Int? {
        return aqi
    }

    fun setAqi(aqi: Int?) {
        this.aqi = aqi
    }

    fun getCategory(): String? {
        return category
    }

    fun setCategory(category: String) {
        this.category = category
    }

    fun getColor(): String? {
        return color
    }

    fun setColor(color: String) {
        this.color = color
    }

    fun getMethod(): String? {
        return method
    }

    fun setMethod(method: String) {
        this.method = method
    }

}