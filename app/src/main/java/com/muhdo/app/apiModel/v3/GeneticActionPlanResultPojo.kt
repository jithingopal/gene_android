package com.muhdo.app.apiModel.v3

data class GeneticActionPlanResultPojo (
    val statusCode: Int? ,
    val message: String? ,
    val data: MutableList<GeneticActionPlanResultData>?
)