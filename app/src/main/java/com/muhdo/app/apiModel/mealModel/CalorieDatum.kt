package com.muhdo.app.apiModel.mealModel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class CalorieDatum {
    @SerializedName("_id")
    @Expose
    private var id: String? = null
    @SerializedName("type")
    @Expose
    private var type: String? = null
    @SerializedName("kcal")
    @Expose
    private var kcal: Double? = null
    @SerializedName("percent")
    @Expose
    private var percent: Int? = null

    fun getId(): String? {
        return id
    }

    fun setId(id: String) {
        this.id = id
    }

    fun getType(): String? {
        return type
    }

    fun setType(type: String) {
        this.type = type
    }

    fun getKcal(): Double? {
        return kcal
    }

    fun setKcal(kcal: Double?) {
        this.kcal = kcal
    }

    fun getPercent(): Int? {
        return percent
    }

    fun setPercent(percent: Int?) {
        this.percent = percent
    }
}