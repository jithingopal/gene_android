package com.muhdo.app.apiModel.v3

data class BaseResponse(
    val message: String,
    val statusCode: Int
)