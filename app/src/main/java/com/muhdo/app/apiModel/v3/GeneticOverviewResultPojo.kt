package com.muhdo.app.apiModel.v3

data class GeneticOverviewResultPojo (
     var statusCode: Int? ,
     var message: String? ,
     var data: MutableList<GeneticOverviewResultData>?
)