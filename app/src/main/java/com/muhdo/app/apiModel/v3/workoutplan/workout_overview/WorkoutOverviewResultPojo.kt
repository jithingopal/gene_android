package com.muhdo.app.apiModel.v3.workoutplan.workout_overview


data class WorkoutOverviewResultPojo (
    var statusCode: Int? ,
    var message: String? ,
    var data: MutableList<WorkoutOverviewData>?
)