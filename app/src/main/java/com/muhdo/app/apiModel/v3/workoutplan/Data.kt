package com.muhdo.app.apiModel.v3.workoutplan

data class Data(
    val exercise_description: String,
    val exercise_name: String,
    val repetition: String,
    val rest: String,
    val sets: String,
    val video_link: String,
    val foam_rolling:List<Data>?,
    val flexibility:List<Data>?
)