package com.muhdo.app.apiModel.v3.workoutplan.workout_overview

import com.google.gson.JsonObject

data class DeletePlanResult(
    val statusCode: Int,
    val message: String,
    val data: JsonObject
)