package com.muhdo.app.apiModel.v3


data class WorkoutTrainingRequest(
    val goal: String? = null,
    val train_duration: Array<Int>? = null,
    val trains_per_week: Int? = null,
    val equipment: Int? = null,
    val training_experience: Int? = null,
    val user_id: String? = null
)