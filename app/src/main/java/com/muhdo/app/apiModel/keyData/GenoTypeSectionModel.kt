package com.muhdo.app.apiModel.keyData

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.muhdo.app.apiModel.signupSignIn.Headers


class GenoTypeSectionModel {


    @SerializedName("statusCode")
    @Expose
    private var statusCode: Int? = null
    @SerializedName("headers")
    @Expose
    private var headers: Headers? = null
    @SerializedName("body")
    @Expose
    private var body: String? = null

    fun getStatusCode(): Int? {
        return statusCode
    }

    fun setStatusCode(statusCode: Int?) {
        this.statusCode = statusCode
    }

    fun getHeaders(): Headers? {
        return headers
    }

    fun setHeaders(headers: Headers) {
        this.headers = headers
    }

    fun getBody(): String? {
        return body
    }

    fun setBody(body: String) {
        this.body = body
    }

}