package com.muhdo.app.apiModel.v3.epigenetics


data class EpigenHomeResponse(
    val message: String,
    val statusCode: Int,
    val data: EpiHomeData


)

data class EpiHomeData(
    val current: EpigenItem?,
    val previous: EpigenItem?,
    val samples: EpiSampleInfo?
)

data class EpigenItem(
    val biological_info: ScoreDataHome,
    val eye_info: ScoreDataHome,
    val hearing_info: ScoreDataHome,
    val memory_info: ScoreDataHome,
    val inflammation_info: ScoreDataHome,
    val kit_id: String,
    val age_at_sample: Int

)

data class ScoreDataHome(
    val score: Float,
    val color: String,
    val title: String
)

data class ScoreDataChart(
    val score: Float,
    val kit_id: String,
    val date_of_scanning: String

)

data class EpiSampleInfo(
    val biological_score: ArrayList<ScoreDataChart>?,
    val eye_score: ArrayList<ScoreDataChart>?,
    val hearing_score: ArrayList<ScoreDataChart>?,
    val memory_score: ArrayList<ScoreDataChart>?,
    val inflammation_score: ArrayList<ScoreDataChart>?
)