package com.muhdo.app.apiModel.login.socialLogin


import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class SocialLoginRequest {
    @SerializedName("auth")
    @Expose
    private var auth: Auth? = null
    @SerializedName("user_data")
    @Expose
    private var userData: UserData? = null
    @SerializedName("login_via")
    @Expose
    private var loginVia: String? = null

    constructor(auth: Auth?, userData: UserData?, loginVia: String?) {
        this.auth = auth
        this.userData = userData
        this.loginVia = loginVia
    }

    fun getAuth(): Auth? {
        return auth
    }

    fun setAuth(auth: Auth) {
        this.auth = auth
    }

    fun getUserData(): UserData? {
        return userData
    }

    fun setUserData(userData: UserData) {
        this.userData = userData
    }

    fun getLoginVia(): String? {
        return loginVia
    }

    fun setLoginVia(loginVia: String) {
        this.loginVia = loginVia
    }
}