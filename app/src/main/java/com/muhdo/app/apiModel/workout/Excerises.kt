package com.muhdo.app.apiModel.workout

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class Excerises {
    @SerializedName("1")
    @Expose
    private var day1: List<WorkoutDay>? = null
    @SerializedName("2")
    @Expose
    private var day2: List<WorkoutDay>? = null
    @SerializedName("3")
    @Expose
    private var day3: List<WorkoutDay>? = null
    @SerializedName("4")
    @Expose
    private var day4: List<WorkoutDay>? = null
    @SerializedName("5")
    @Expose
    private var day5: List<WorkoutDay>? = null
    @SerializedName("6")
    @Expose
    private var day6: List<WorkoutDay>? = null
    @SerializedName("7")
    @Expose
    private var day7: List<WorkoutDay>? = null

    fun get1(): List<WorkoutDay>?{
        return day1
    }

    fun set1(day1: List<WorkoutDay>) {
        this.day1 = day1
    }

    fun get2(): List<WorkoutDay>?{
        return day2
    }

    fun set2(day2: List<WorkoutDay>) {
        this.day2 = day2
    }
    fun get3(): List<WorkoutDay>?{
        return day3
    }

    fun set3(day3: List<WorkoutDay>) {
        this.day3 = day3
    }

    fun get4(): List<WorkoutDay>?{
        return day4
    }

    fun set4(day4: List<WorkoutDay>) {
        this.day4 = day4
    }

    fun get5(): List<WorkoutDay>?{
        return day5
    }

    fun set5(day5: List<WorkoutDay>) {
        this.day5 = day5
    }

    fun get6(): List<WorkoutDay>?{
        return day6
    }

    fun set6(day6: List<WorkoutDay>) {
        this.day6 = day6
    }

    fun get7(): List<WorkoutDay>?{
        return day7
    }

    fun set7(day7: List<WorkoutDay>) {
        this.day7 = day7
    }




}