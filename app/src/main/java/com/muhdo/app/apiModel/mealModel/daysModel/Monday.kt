package com.muhdo.app.apiModel.mealModel.daysModel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class Monday{


    @SerializedName("name")
    @Expose
    private var name: String? = null

    @SerializedName("bucket")
    @Expose
    private var bucket: String? = null
    @SerializedName("image")
    @Expose
    private var image: String? = null
    @SerializedName("_id")
    @Expose
    private var id: String? = null
    @SerializedName("time")
    @Expose
    private var time: String? = null
    @SerializedName("is_favourite")
    @Expose
    private var isFavourite: Boolean? = null
    @SerializedName("is_selected")
    @Expose
    private var isSelected: Boolean? = null

    fun getIsSelected(): Boolean? {
        return isSelected
    }

    fun setIsSelected(isSelected: Boolean?) {
        this.isSelected = isSelected
    }


    fun getBucket(): String {
        return this.bucket!!
    }

    fun setBucket(bucket: String) {
        this.bucket = bucket
    }

    fun getName(): String? {
        return name
    }

    fun setName(name: String) {
        this.name = name
    }

    fun getImage(): String? {
        return image
    }

    fun setImage(image: String) {
        this.image = image
    }

    fun getId(): String? {
        return id
    }

    fun setId(id: String) {
        this.id = id
    }

    fun getTime(): String? {
        return time
    }

    fun setTime(time: String) {
        this.time = time
    }

    fun getIsFavourite(): Boolean? {
        return isFavourite
    }

    fun setIsFavourite(isFavourite: Boolean?) {
        this.isFavourite = isFavourite
    }


}