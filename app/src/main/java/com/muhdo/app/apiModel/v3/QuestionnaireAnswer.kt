package com.muhdo.app.apiModel.v3

data class QuestionnaireAnswer(
    val answers: List<Answer>,
    val user_id: String,
    val category: String
)