package com.muhdo.app.apiModel.v3

data class UpdateUserData(
    val user_id: String? = null,
    val address_lane1: String? = null,
    val city: String? = null,
 //   val country: String? = null,
    val country_region: String? = null,
  //  val email: String? = null,
    val first_name: String? = null,
    val last_name: String? = null,
  //  val phone: String? = null,
    val zipcode: String? = null
)