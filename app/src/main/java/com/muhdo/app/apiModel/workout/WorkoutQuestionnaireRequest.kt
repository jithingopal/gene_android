package com.muhdo.app.apiModel.workout

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class WorkoutQuestionnaireRequest {

    @SerializedName("goal")
    @Expose
    private var goal: String? = null
    @SerializedName("dob")
    @Expose
    private var dob: String? = null
    @SerializedName("height")
    @Expose
    private var height: Int? = null
    @SerializedName("height_unit")
    @Expose
    private var heightUnit: String? = null
    @SerializedName("weight")
    @Expose
    private var weight: Int? = null
    @SerializedName("weight_unit")
    @Expose
    private var weightUnit: String? = null
    @SerializedName("train_days")
    @Expose
    private var trainDays: Int? = null
    @SerializedName("train_time")
    @Expose
    private var trainTime: List<Int>? = null
    @SerializedName("equipment")
    @Expose
    private var equipment: Int? = null
    @SerializedName("physician_advice")
    @Expose
    private var physicianAdvice: Boolean? = null
    @SerializedName("gender")
    @Expose
    private var gender: String? = null
    @SerializedName("injury")
    @Expose
    private var injury: Boolean? = null
    @SerializedName("pregnant")
    @Expose
    private var pregnant: Boolean? = null
    @SerializedName("diagnose")
    @Expose
    private var diagnose: List<String>? = null
    @SerializedName("experience")
    @Expose
    private var experience: Int? = null
    @SerializedName("survey_type")
    @Expose
    private var surveyType: String? = null
    @SerializedName("user_id")
    @Expose
    private var userId: String? = null

    constructor(
        goal: String?,
        dob: String?,
        height: Int?,
        heightUnit: String?,
        weight: Int?,
        weightUnit: String?,
        trainDays: Int?,
        trainTime: List<Int>?,
        equipment: Int?,
        physicianAdvice: Boolean?,
        gender: String?,
        injury: Boolean?,
        pregnant: Boolean?,
        diagnose: List<String>?,
        experience: Int?,
        surveyType: String?,
        userId: String?
    ) {
        this.goal = goal
        this.dob = dob
        this.height = height
        this.heightUnit = heightUnit
        this.weight = weight
        this.weightUnit = weightUnit
        this.trainDays = trainDays
        this.trainTime = trainTime
        this.equipment = equipment
        this.physicianAdvice = physicianAdvice
        this.gender = gender
        this.injury = injury
        this.pregnant = pregnant
        this.diagnose = diagnose
        this.experience = experience
        this.surveyType = surveyType
        this.userId = userId
    }

    fun getGoal(): String? {
        return goal
    }

    fun setGoal(goal: String) {
        this.goal = goal
    }

    fun getDob(): String? {
        return dob
    }

    fun setDob(dob: String) {
        this.dob = dob
    }

    fun getHeight(): Int? {
        return height
    }

    fun setHeight(height: Int?) {
        this.height = height
    }

    fun getHeightUnit(): String? {
        return heightUnit
    }

    fun setHeightUnit(heightUnit: String) {
        this.heightUnit = heightUnit
    }

    fun getWeight(): Int? {
        return weight
    }

    fun setWeight(weight: Int?) {
        this.weight = weight
    }

    fun getWeightUnit(): String? {
        return weightUnit
    }

    fun setWeightUnit(weightUnit: String) {
        this.weightUnit = weightUnit
    }

    fun getTrainDays(): Int? {
        return trainDays
    }

    fun setTrainDays(trainDays: Int?) {
        this.trainDays = trainDays
    }

    fun getTrainTime(): List<Int>? {
        return trainTime
    }

    fun setTrainTime(trainTime: List<Int>) {
        this.trainTime = trainTime
    }

    fun getEquipment(): Int? {
        return equipment
    }

    fun setEquipment(equipment: Int?) {
        this.equipment = equipment
    }

    fun getPhysicianAdvice(): Boolean? {
        return physicianAdvice
    }

    fun setPhysicianAdvice(physicianAdvice: Boolean?) {
        this.physicianAdvice = physicianAdvice
    }

    fun getGender(): String? {
        return gender
    }

    fun setGender(gender: String) {
        this.gender = gender
    }

    fun getInjury(): Boolean? {
        return injury
    }

    fun setInjury(injury: Boolean?) {
        this.injury = injury
    }

    fun getPregnant(): Boolean? {
        return pregnant
    }

    fun setPregnant(pregnant: Boolean?) {
        this.pregnant = pregnant
    }

    fun getDiagnose(): List<String>? {
        return diagnose
    }

    fun setDiagnose(diagnose: List<String>) {
        this.diagnose = diagnose
    }

    fun getExperience(): Int? {
        return experience
    }

    fun setExperience(experience: Int?) {
        this.experience = experience
    }

    fun getSurveyType(): String? {
        return surveyType
    }

    fun setSurveyType(surveyType: String) {
        this.surveyType = surveyType
    }

    fun getUserId(): String? {
        return userId
    }

    fun setUserId(userId: String) {
        this.userId = userId
    }
}