package com.muhdo.app.apiModel.mealModel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class RecipeType {


    @SerializedName("_id")
    @Expose
    private var id: String? = null
    @SerializedName("type")
    @Expose
    private var type: String? = null

    fun getId(): String? {
        return id
    }

    fun setId(id: String) {
        this.id = id
    }

    fun getType(): String? {
        return type
    }

    fun setType(type: String) {
        this.type = type
    }

}