package com.muhdo.app.apiModel.mealModel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class UpdateFavouriteRequest {


    @SerializedName("is_favourite")
    @Expose
    private var isFavourite: Boolean? = null
    @SerializedName("recipe_id")
    @Expose
    private var recipeId: String? = null
    @SerializedName("user_id")
    @Expose
    private var userId: String? = null

    constructor(isFavourite: Boolean?, recipeId: String?, userId: String?) {
        this.isFavourite = isFavourite
        this.recipeId = recipeId
        this.userId = userId
    }

    fun getIsFavourite(): Boolean? {
        return isFavourite
    }

    fun setIsFavourite(isFavourite: Boolean?) {
        this.isFavourite = isFavourite
    }

    fun getRecipeId(): String? {
        return recipeId
    }

    fun setRecipeId(recipeId: String) {
        this.recipeId = recipeId
    }

    fun getUserId(): String? {
        return userId
    }

    fun setUserId(userId: String) {
        this.userId = userId
    }

}