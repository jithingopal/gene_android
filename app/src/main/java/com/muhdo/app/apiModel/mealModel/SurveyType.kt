package com.muhdo.app.apiModel.mealModel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable


class SurveyType : Serializable {


    @SerializedName("_id")
    @Expose
    private var id: String? = null
    @SerializedName("is_deleted")
    @Expose
    private var isDeleted: Boolean? = null
    @SerializedName("type")
    @Expose
    private var type: String? = null
    @SerializedName("createdAt")
    @Expose
    private var createdAt: String? = null
    @SerializedName("updatedAt")
    @Expose
    private var updatedAt: String? = null
    @SerializedName("__v")
    @Expose
    private var v: Int? = null

    fun getId(): String? {
        return id
    }

    fun setId(id: String) {
        this.id = id
    }

    fun getIsDeleted(): Boolean? {
        return isDeleted
    }

    fun setIsDeleted(isDeleted: Boolean?) {
        this.isDeleted = isDeleted
    }

    fun getType(): String? {
        return type
    }

    fun setType(type: String) {
        this.type = type
    }

    fun getCreatedAt(): String? {
        return createdAt
    }

    fun setCreatedAt(createdAt: String) {
        this.createdAt = createdAt
    }

    fun getUpdatedAt(): String? {
        return updatedAt
    }

    fun setUpdatedAt(updatedAt: String) {
        this.updatedAt = updatedAt
    }

    fun getV(): Int? {
        return v
    }

    fun setV(v: Int?) {
        this.v = v
    }


}