package com.muhdo.app.apiModel.mealModel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class RecipeIngredient {

    @SerializedName("_id")
    @Expose
    private var id: String? = null
    @SerializedName("is_deleted")
    @Expose
    private var isDeleted: Boolean? = null
    @SerializedName("ingredient")
    @Expose
    private var ingredient: String? = null
    @SerializedName("measurement")
    @Expose
    private var measurement: String? = null
    @SerializedName("recipe_id")
    @Expose
    private var recipeId: String? = null
    @SerializedName("__v")
    @Expose
    private var v: Int? = null
    @SerializedName("createdAt")
    @Expose
    private var createdAt: String? = null
    @SerializedName("updatedAt")
    @Expose
    private var updatedAt: String? = null

    fun getId(): String? {
        return id
    }

    fun setId(id: String) {
        this.id = id
    }

    fun getIsDeleted(): Boolean? {
        return isDeleted
    }

    fun setIsDeleted(isDeleted: Boolean?) {
        this.isDeleted = isDeleted
    }

    fun getIngredient(): String? {
        return ingredient
    }

    fun setIngredient(ingredient: String) {
        this.ingredient = ingredient
    }

    fun getMeasurement(): String? {
        return measurement
    }

    fun setMeasurement(measurement: String) {
        this.measurement = measurement
    }

    fun getRecipeId(): String? {
        return recipeId
    }

    fun setRecipeId(recipeId: String) {
        this.recipeId = recipeId
    }

    fun getV(): Int? {
        return v
    }

    fun setV(v: Int?) {
        this.v = v
    }

    fun getCreatedAt(): String? {
        return createdAt
    }

    fun setCreatedAt(createdAt: String) {
        this.createdAt = createdAt
    }

    fun getUpdatedAt(): String? {
        return updatedAt
    }

    fun setUpdatedAt(updatedAt: String) {
        this.updatedAt = updatedAt
    }

}