package com.muhdo.app.apiModel.keyData

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class Indicator {


    @SerializedName("title")
    @Expose
    private var title: String? = null
    @SerializedName("percent")
    @Expose
    private var percent: Int? = null
    @SerializedName("mode_id")
    @Expose
    private var modeId: String? = null
    @SerializedName("section_id")
    @Expose
    private var sectionId: String? = null
    @SerializedName("result")
    @Expose
    private var result: String? = null
    @SerializedName("category_id")
    @Expose
    private var categoryId: String? = null

    fun getTitle(): String? {
        return title
    }

    fun setTitle(title: String) {
        this.title = title
    }

    fun getPercent(): Int? {
        return percent
    }

    fun setPercent(percent: Int?) {
        this.percent = percent
    }

    fun getModeId(): String? {
        return modeId
    }

    fun setModeId(modeId: String) {
        this.modeId = modeId
    }

    fun getSectionId(): String? {
        return sectionId
    }

    fun setSectionId(sectionId: String) {
        this.sectionId = sectionId
    }

    fun getResult(): String? {
        return result
    }

    fun setResult(result: String) {
        this.result = result
    }

    fun getCategoryId(): String? {
        return categoryId
    }

    fun setCategoryId(categoryId: String) {
        this.categoryId = categoryId
    }
}