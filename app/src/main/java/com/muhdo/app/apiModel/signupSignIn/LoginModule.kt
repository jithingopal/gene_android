package com.muhdo.app.apiModel.signupSignIn

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class LoginModule {

    @SerializedName("statusCode")
    @Expose
    private var statusCode: Int? = null

    @SerializedName("headers")
    @Expose
    private var headers: Headers? = null

    @SerializedName("data")
    @Expose
    private var data1: LoginData? = null

    @SerializedName("body")
    @Expose
    private var body: String? = null

    fun getStatusCode(): Int? {
        return statusCode
    }

    fun setStatusCode(statusCode: Int?) {
        this.statusCode = statusCode
    }

    fun getHeaders(): Headers? {
        return headers
    }

    fun setHeaders(headers: Headers) {
        this.headers = headers
    }

    fun getData(): LoginData? {
        return data1
    }

    fun setData(data: LoginData) {
        this.data1 = data
    }

    fun getBody(): String? {
        return body
    }

    fun setBody(body: String) {
        this.body = body
    }




}

class LoginData  {

    @SerializedName("user_id")
    @Expose
    var userId: String? = null

    @SerializedName("user_data")
    @Expose
    var user: UserDataNew? = null

}

class UserDataNew {
    @SerializedName("suspended")
    @Expose
    var suspended: Boolean? = null


}

