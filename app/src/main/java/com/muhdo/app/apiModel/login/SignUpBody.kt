package com.muhdo.app.apiModel.login

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.muhdo.app.apiModel.signupSignIn.User


class SignUpBody {


    @SerializedName("user")
    @Expose
    private var user: User? = null
    @SerializedName("userConfirmed")
    @Expose
    private var userConfirmed: Boolean? = null
    @SerializedName("userSub")
    @Expose
    private var userSub: String? = null
//    @SerializedName("message")
//    @Expose
//    private var message: Message? = null

    fun getUser(): User? {
        return user
    }

    fun setUser(user: User) {
        this.user = user
    }

    fun getUserConfirmed(): Boolean? {
        return userConfirmed
    }

    fun setUserConfirmed(userConfirmed: Boolean?) {
        this.userConfirmed = userConfirmed
    }

    fun getUserSub(): String? {
        return userSub
    }

    fun setUserSub(userSub: String) {
        this.userSub = userSub
    }
//
//    fun getMessage(): Message? {
//        return message
//    }
//
//    fun setMessage(message: Message) {
//        this.message = message
//    }


}