package com.muhdo.app.apiModel.v3.workoutplan

data class ResponseInjuryPrevention(
    val `data`: List<Data>,
    val message: String,
    val statusCode: Int
)