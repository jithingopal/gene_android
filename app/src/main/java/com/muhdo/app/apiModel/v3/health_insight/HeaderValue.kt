package com.muhdo.app.apiModel.v3.health_insight

import com.google.gson.JsonObject

data class HeaderValue(
   val infoText: String?,
   val riskStatus: String?,
   val riskIndicators: Int?
)
