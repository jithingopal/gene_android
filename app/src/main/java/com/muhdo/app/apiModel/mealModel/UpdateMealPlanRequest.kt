package com.muhdo.app.apiModel.mealModel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class UpdateMealPlanRequest {


    @SerializedName("recipes")
    @Expose
    private var recipes: ArrayList<Recipe>? = null
    @SerializedName("user_id")
    @Expose
    private var userId: String? = null

    constructor(recipes: ArrayList<Recipe>?, userId: String?) {
        this.recipes = recipes
        this.userId = userId
    }

    fun getRecipes(): ArrayList<Recipe>? {
        return recipes
    }

    fun setRecipes(recipes: ArrayList<Recipe>) {
        this.recipes = recipes
    }

    fun getUserId(): String? {
        return userId
    }

    fun setUserId(userId: String) {
        this.userId = userId
    }

    override fun toString(): String {
        return "UpdateMealPlanRequest(recipes=$recipes, userId=$userId)"
    }
}