package com.muhdo.app.apiModel.v3


data class MealGuideSurveyRequest(
    val goal: String? = null,
    val meal_per_day: Int? = null,
    val activity: Int? = null,
    val user_id: String? = null,
    val survey_type : String? = null
)