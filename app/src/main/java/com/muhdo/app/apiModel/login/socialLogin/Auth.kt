package com.muhdo.app.apiModel.login.socialLogin

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class Auth {


    @SerializedName("client_id")
    @Expose
    private var clientId: String? = null
    @SerializedName("access_token")
    @Expose
    private var accessToken: String? = null

    constructor(clientId: String?, accessToken: String?) {
        this.clientId = clientId
        this.accessToken = accessToken
    }

    fun getClientId(): String? {
        return clientId
    }

    fun setClientId(clientId: String) {
        this.clientId = clientId
    }

    fun getAccessToken(): String? {
        return accessToken
    }

    fun setAccessToken(accessToken: String) {
        this.accessToken = accessToken
    }

}