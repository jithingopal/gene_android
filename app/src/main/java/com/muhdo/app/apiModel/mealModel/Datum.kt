package com.muhdo.app.apiModel.mealModel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable


class Datum : Serializable {

    @SerializedName("_id")
    @Expose
    private var id: String? = null
    @SerializedName("question")
    @Expose
    private var question: String? = null
    @SerializedName("survey_type")
    @Expose
    private var surveyType: SurveyType? = null
    @SerializedName("html_type")
    @Expose
    private var htmlType: String? = null
    @SerializedName("createdAt")
    @Expose
    private var createdAt: String? = null
    @SerializedName("updatedAt")
    @Expose
    private var updatedAt: String? = null
    @SerializedName("is_deleted")
    @Expose
    private var isDeleted: Boolean? = null
    @SerializedName("value_type")
    @Expose
    private var valueType: String? = null
    @SerializedName("options")
    @Expose
    private var options: List<Option>? = null

    fun getId(): String? {
        return id
    }

    fun setId(id: String) {
        this.id = id
    }

    fun getQuestion(): String? {
        return question
    }

    fun setQuestion(question: String) {
        this.question = question
    }

    fun getSurveyType(): SurveyType? {
        return surveyType
    }

    fun setSurveyType(surveyType: SurveyType) {
        this.surveyType = surveyType
    }

    fun getHtmlType(): String? {
        return htmlType
    }

    fun setHtmlType(htmlType: String) {
        this.htmlType = htmlType
    }

    fun getCreatedAt(): String? {
        return createdAt
    }

    fun setCreatedAt(createdAt: String) {
        this.createdAt = createdAt
    }

    fun getUpdatedAt(): String? {
        return updatedAt
    }

    fun setUpdatedAt(updatedAt: String) {
        this.updatedAt = updatedAt
    }

    fun getIsDeleted(): Boolean? {
        return isDeleted
    }

    fun setIsDeleted(isDeleted: Boolean?) {
        this.isDeleted = isDeleted
    }

    fun getValueType(): String? {
        return valueType
    }

    fun setValueType(valueType: String) {
        this.valueType = valueType
    }

    fun getOptions(): List<Option>? {
        return options
    }

    fun setOptions(options: List<Option>) {
        this.options = options
    }

}