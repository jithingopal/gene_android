package com.muhdo.app.apiModel.keyData

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class SectionData {


    @SerializedName("tag")
    @Expose
    private var tag: String? = null
    @SerializedName("title")
    @Expose
    private var title: String? = null
    @SerializedName("description")
    @Expose
    private var description: String? = null
    @SerializedName("indicator_description")
    @Expose
    private var indicatorDescription: String? = null
    @SerializedName("message")
    @Expose
    private var message: String? = null
    @SerializedName("interests")
    @Expose
    private var interests: String? = null
    @SerializedName("_id")
    @Expose
    private var id: String? = null
    @SerializedName("mode_id")
    @Expose
    private var modeId: String? = null
    @SerializedName("section_id")
    @Expose
    private var sectionId: String? = null
    @SerializedName("user_id")
    @Expose
    private var userId: String? = null
    @SerializedName("activeIndicator")
    @Expose
    private var activeIndicator: ActiveIndicator? = null
    @SerializedName("category_id")
    @Expose
    private var categoryId: String? = null
    @SerializedName("__v")
    @Expose
    private var v: Int? = null

    fun getTag(): String? {
        return tag
    }

    fun setTag(tag: String) {
        this.tag = tag
    }

    fun getTitle(): String? {
        return title
    }

    fun setTitle(title: String) {
        this.title = title
    }

    fun getDescription(): String? {
        return description
    }

    fun setDescription(description: String) {
        this.description = description
    }

    fun getIndicatorDescription(): String? {
        return indicatorDescription
    }

    fun setIndicatorDescription(indicatorDescription: String) {
        this.indicatorDescription = indicatorDescription
    }

    fun getMessage(): String? {
        return message
    }

    fun setMessage(message: String) {
        this.message = message
    }

    fun getInterests(): String? {
        return interests
    }

    fun setInterests(interests: String) {
        this.interests = interests
    }

    fun getId(): String? {
        return id
    }

    fun setId(id: String) {
        this.id = id
    }

    fun getModeId(): String? {
        return modeId
    }

    fun setModeId(modeId: String) {
        this.modeId = modeId
    }

    fun getSectionId(): String? {
        return sectionId
    }

    fun setSectionId(sectionId: String) {
        this.sectionId = sectionId
    }

    fun getUserId(): String? {
        return userId
    }

    fun setUserId(userId: String) {
        this.userId = userId
    }

    fun getActiveIndicator(): ActiveIndicator? {
        return activeIndicator
    }

    fun setActiveIndicator(activeIndicator: ActiveIndicator) {
        this.activeIndicator = activeIndicator
    }

    fun getCategoryId(): String? {
        return categoryId
    }

    fun setCategoryId(categoryId: String) {
        this.categoryId = categoryId
    }

    fun getV(): Int? {
        return v
    }

    fun setV(v: Int?) {
        this.v = v
    }

}