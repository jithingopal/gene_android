package com.muhdo.app.apiModel.healthinsights

data class ResponseHealthInisghts(
    val `data`: List<Data>,
    val message: String,
    val statusCode: Int
)