package com.muhdo.app.apiModel.result

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class SleepScore {
    @SerializedName("sleep_type")
    @Expose
    private var sleepType: String? = null

    @SerializedName("sleep_title")
    @Expose
    private var sleepTitle: String? = null

    @SerializedName("score")
    @Expose
    private var score: Int? = null

    @SerializedName("description")
    @Expose
    private var description: String? = null

    @SerializedName("genes_of_interest")
    @Expose
    private var genesOfInterest: String? = null

    @SerializedName("aspect_intro")
    @Expose
    private var aspectIntro: String? = null

    @SerializedName("result")
    @Expose
    private var result: String? = null

    @SerializedName("recommendation")
    @Expose
    private var recommendation: String? = null

    @SerializedName("food_detail")
    @Expose
    open var foodDetail: List<FoodDetail>? = null

    @SerializedName("recipe_detail")
    @Expose
    open  var recipeDetail: List<RecipeDetail>? = null

    @SerializedName("degree")
    @Expose
    private var degree: Int? = null

    fun getDegree(): Int? {
        return degree
    }

    fun setDegree(degree: Int) {
        this.degree = degree
    }

    fun getSleepType(): String? {
        return sleepType
    }

    fun setSleepType(sleepType: String) {
        this.sleepType = sleepType
    }

    fun getSleepTitle(): String? {
        return sleepTitle
    }

    fun setSleepTitle(sleepTitle: String) {
        this.sleepTitle = sleepTitle
    }

    fun getScore(): Int? {
        return score
    }

    fun setScore(score: Int) {
        this.score = score
    }

    fun getDescription(): String? {
        return description
    }

    fun setDescription(description: String) {
        this.description = description
    }

    fun getGenesOfInterest(): String? {
        return genesOfInterest
    }

    fun setGenesOfInterest(genesOfInterest: String) {
        this.genesOfInterest = genesOfInterest
    }

    fun getAspectIntro(): String? {
        return aspectIntro
    }

    fun setAspectIntro(aspectIntro: String) {
        this.aspectIntro = aspectIntro
    }

    fun getResult(): String? {
        return result
    }

    fun setResult(result: String) {
        this.result = result
    }

    fun getRecommendation(): String? {
        return recommendation
    }

    fun setRecommendation(recommendation: String) {
        this.recommendation = recommendation
    }


    inner class FoodDetail {
        @SerializedName("title")
        @Expose
        open var foodDetailTitle: String? = null

        @SerializedName("foods")
        @Expose
        open var foodsList: List<Foods>? = null


    }

    inner class Foods {
        @SerializedName("name")
        @Expose
        open var foodsName: String? = null

        @SerializedName("quantity_unit1")
        @Expose
        open var foodsQtyUnit1: String? = null

        @SerializedName("quantity_unit2")
        @Expose
        open var foodsQtyUnit2: String? = null

        @SerializedName("img")
        @Expose
        open var foodsImgSvg: String? = null

        @SerializedName("png_img")
        @Expose
        open var foodsImg: String? = null
    }



    inner  class RecipeDetail {

        @SerializedName("_id")
        @Expose
        open var recipeDetail_Id: String? = null

        @SerializedName("id")
        @Expose
        open var recipeDetailId: String? = null

        @SerializedName("recipe_id")
        @Expose
        open var recipeDetailRecipeId: String? = null

        @SerializedName("image")
        @Expose
        open var recipeDetailImage: String? = null

        @SerializedName("name")
        @Expose
        open var recipeDetailName: String? = null

        @SerializedName("time")
        @Expose
        open var recipeDetailTime: String? = null

        @SerializedName("serving_size")
        @Expose
        open var recipeDetailServingSize: String? = null


        @SerializedName("prep_time")
        @Expose
        open var recipeDetailPrepTime: String? = null

        @SerializedName("difficulty")
        @Expose
        open var recipeDetailDifficulty: String? = null

        @SerializedName("isfreezable")
        @Expose
        open var recipeDetailIsFreezable: String? = null

        @SerializedName("hints")
        @Expose
        open var recipeDetailHints: String? = null

        @SerializedName("keywords")
        @Expose
        open var recipeDetailKeywords: String? = null

        @SerializedName("calories")
        @Expose
        open var recipeDetailCalories: String? = null

        @SerializedName("protein")
        @Expose
        open var recipeDetailProtein: String? = null

        @SerializedName("carbs")
        @Expose
        open var recipeDetailCarbs: String? = null

        @SerializedName("fat")
        @Expose
        open var recipeDetailFat: String? = null

    }

}





