package com.muhdo.app.apiModel.v3

data class GeneticOverviewReq(
    val user_id: String?=null,
    val type: String?=null
    )