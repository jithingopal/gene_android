package com.muhdo.app.apiModel.v3.workoutplan.workout_overview



data class WorkoutOverviewData (
    var title: String? ,
    var description: String? ,
    var values: MutableList<WorkoutDataValues>?
)