package com.muhdo.app.apiModel.login.socialLogin

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class UserData {


    @SerializedName("first_name")
    @Expose
    private var firstName: String? = null
    @SerializedName("last_name")
    @Expose
    private var lastName: String? = null
    @SerializedName("name")
    @Expose
    private var name: String? = null
    @SerializedName("picture")
    @Expose
    private var picture: String? = null
    @SerializedName("thumbnail")
    @Expose
    private var thumbnail: String? = null
    @SerializedName("id")
    @Expose
    private var id: String? = null
    @SerializedName("email")
    @Expose
    private var email: String? = null
    @SerializedName("enterpriser")
    @Expose
    private var enterpriser: String? = null

    constructor(
        firstName: String?,
        lastName: String?,
        name: String?,
        picture: String?,
        thumbnail: String?,
        id: String?,
        email: String?,
        enterpriser: String?
    ) {
        this.firstName = firstName
        this.lastName = lastName
        this.name = name
        this.picture = picture
        this.thumbnail = thumbnail
        this.id = id
        this.email = email
        this.enterpriser = enterpriser
    }

    fun getFirstName(): String? {
        return firstName
    }

    fun setFirstName(firstName: String) {
        this.firstName = firstName
    }

    fun getLastName(): String? {
        return lastName
    }

    fun setLastName(lastName: String) {
        this.lastName = lastName
    }

    fun getName(): String? {
        return name
    }

    fun setName(name: String) {
        this.name = name
    }

    fun getPicture(): String? {
        return picture
    }

    fun setPicture(picture: String) {
        this.picture = picture
    }

    fun getThumbnail(): String? {
        return thumbnail
    }

    fun setThumbnail(thumbnail: String) {
        this.thumbnail = thumbnail
    }

    fun getId(): String? {
        return id
    }

    fun setId(id: String) {
        this.id = id
    }

    fun getEmail(): String? {
        return email
    }

    fun setEmail(email: String) {
        this.email = email
    }

    fun getEnterpriser(): String? {
        return enterpriser
    }

    fun setEnterpriser(enterpriser: String) {
        this.enterpriser = enterpriser
    }
}