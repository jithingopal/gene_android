package com.muhdo.app.apiModel.mealModel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class RecipeDescriptionModelData {


    @SerializedName("_id")
    @Expose
    private var id: String? = null
    @SerializedName("description")
    @Expose
    private var description: String? = null
    @SerializedName("preparation_steps")
    @Expose
    private var preparationSteps: List<PreparationStep>? = null
    @SerializedName("calories")
    @Expose
    private var calories: Any? = null
    @SerializedName("protein")
    @Expose
    private var protein: Any? = null
    @SerializedName("carbs")
    @Expose
    private var carbs: Any? = null
    @SerializedName("sugars")
    @Expose
    private var sugars: Any? = null
    @SerializedName("fat")
    @Expose
    private var fat: Any? = null
    @SerializedName("saturated_fat")
    @Expose
    private var saturatedFat: Any? = null
    @SerializedName("fibre")
    @Expose
    private var fibre: Any? = null
    @SerializedName("sodium")
    @Expose
    private var sodium: Any? = null
    @SerializedName("beef_free")
    @Expose
    private var beefFree: Boolean? = null
    @SerializedName("egg_free")
    @Expose
    private var eggFree: Boolean? = null
    @SerializedName("gluten_free")
    @Expose
    private var glutenFree: Boolean? = null
    @SerializedName("alcohol_free")
    @Expose
    private var alcoholFree: Boolean? = null
    @SerializedName("pork_free")
    @Expose
    private var porkFree: Boolean? = null
    @SerializedName("dairy_free")
    @Expose
    private var dairyFree: Boolean? = null
    @SerializedName("nuts_free")
    @Expose
    private var nutsFree: Boolean? = null
    @SerializedName("soy_free")
    @Expose
    private var soyFree: Boolean? = null
    @SerializedName("shellfish_free")
    @Expose
    private var shellfishFree: Boolean? = null
    @SerializedName("is_deleted")
    @Expose
    private var isDeleted: Boolean? = null
    @SerializedName("name")
    @Expose
    private var name: String? = null
    @SerializedName("time")
    @Expose
    private var time: String? = null
    @SerializedName("recipe_type")
    @Expose
    private var recipeType: RecipeType? = null
    @SerializedName("cuisine_recipes")
    @Expose
    private var cuisineRecipes: String? = null
    @SerializedName("serving_size")
    @Expose
    private var servingSize: String? = null
    @SerializedName("macros_percent_ratio")
    @Expose
    private var macrosPercentRatio: String? = null
    @SerializedName("bucket")
    @Expose
    private var bucket: String? = null
    @SerializedName("createdAt")
    @Expose
    private var createdAt: String? = null
    @SerializedName("updatedAt")
    @Expose
    private var updatedAt: String? = null
    @SerializedName("image")
    @Expose
    private var image: String? = null
    @SerializedName("video")
    @Expose
    private var video: String? = null
    @SerializedName("recipe_ingredients")
    @Expose
    private var recipeIngredients: List<RecipeIngredient>? = null

    fun getId(): String? {
        return id
    }

    fun setId(id: String) {
        this.id = id
    }

    fun getDescription(): String? {
        return description
    }

    fun setDescription(description: String) {
        this.description = description
    }

    fun getPreparationSteps(): List<PreparationStep> {
        return this!!.preparationSteps!!
    }

    fun setPreparationSteps(preparationSteps: List<PreparationStep>) {
        this.preparationSteps = preparationSteps
    }


    fun getCalories(): Any? {
        return calories
    }

    fun setCalories(calories: Any?) {
        this.calories = calories
    }

    fun getProtein(): Any? {
        return protein
    }

    fun setProtein(protein: Int?) {
        this.protein = protein
    }

    fun getCarbs(): Any? {
        return carbs
    }

    fun setCarbs(carbs: Int?) {
        this.carbs = carbs
    }

    fun getSugars(): Any? {
        return sugars
    }

    fun setSugars(sugars: Int?) {
        this.sugars = sugars
    }

    fun getFat(): Any? {
        return fat
    }

    fun setFat(fat: Double?) {
        this.fat = fat
    }

    fun getSaturatedFat(): Any? {
        return saturatedFat
    }

    fun setSaturatedFat(saturatedFat: Double?) {
        this.saturatedFat = saturatedFat
    }

    fun getFibre(): Any? {
        return fibre
    }

    fun setFibre(fibre: Int?) {
        this.fibre = fibre
    }

    fun getSodium(): Any? {
        return sodium
    }

    fun setSodium(sodium: Double?) {
        this.sodium = sodium
    }

    fun getBeefFree(): Boolean? {
        return beefFree
    }

    fun setBeefFree(beefFree: Boolean?) {
        this.beefFree = beefFree
    }

    fun getEggFree(): Boolean? {
        return eggFree
    }

    fun setEggFree(eggFree: Boolean?) {
        this.eggFree = eggFree
    }

    fun getGlutenFree(): Boolean? {
        return glutenFree
    }

    fun setGlutenFree(glutenFree: Boolean?) {
        this.glutenFree = glutenFree
    }

    fun getAlcoholFree(): Boolean? {
        return alcoholFree
    }

    fun setAlcoholFree(alcoholFree: Boolean?) {
        this.alcoholFree = alcoholFree
    }

    fun getPorkFree(): Boolean? {
        return porkFree
    }

    fun setPorkFree(porkFree: Boolean?) {
        this.porkFree = porkFree
    }

    fun getDairyFree(): Boolean? {
        return dairyFree
    }

    fun setDairyFree(dairyFree: Boolean?) {
        this.dairyFree = dairyFree
    }

    fun getNutsFree(): Boolean? {
        return nutsFree
    }

    fun setNutsFree(nutsFree: Boolean?) {
        this.nutsFree = nutsFree
    }

    fun getSoyFree(): Boolean? {
        return soyFree
    }

    fun setSoyFree(soyFree: Boolean?) {
        this.soyFree = soyFree
    }

    fun getShellfishFree(): Boolean? {
        return shellfishFree
    }

    fun setShellfishFree(shellfishFree: Boolean?) {
        this.shellfishFree = shellfishFree
    }

    fun getIsDeleted(): Boolean? {
        return isDeleted
    }

    fun setIsDeleted(isDeleted: Boolean?) {
        this.isDeleted = isDeleted
    }

    fun getName(): String? {
        return name
    }

    fun setName(name: String) {
        this.name = name
    }

    fun getTime(): String? {
        return time
    }

    fun setTime(time: String) {
        this.time = time
    }

    fun getRecipeType(): RecipeType? {
        return recipeType
    }

    fun setRecipeType(recipeType: RecipeType) {
        this.recipeType = recipeType
    }

    fun getCuisineRecipes(): String? {
        return cuisineRecipes
    }

    fun setCuisineRecipes(cuisineRecipes: String) {
        this.cuisineRecipes = cuisineRecipes
    }

    fun getServingSize(): String? {
        return servingSize
    }

    fun setServingSize(servingSize: String) {
        this.servingSize = servingSize
    }

    fun getMacrosPercentRatio(): String? {
        return macrosPercentRatio
    }

    fun setMacrosPercentRatio(macrosPercentRatio: String) {
        this.macrosPercentRatio = macrosPercentRatio
    }


    fun getBucket(): String? {
        return bucket
    }

    fun setBucket(bucket: String) {
        this.bucket = bucket
    }

    fun getCreatedAt(): String? {
        return createdAt
    }

    fun setCreatedAt(createdAt: String) {
        this.createdAt = createdAt
    }

    fun getUpdatedAt(): String? {
        return updatedAt
    }

    fun setUpdatedAt(updatedAt: String) {
        this.updatedAt = updatedAt
    }

    fun getImage(): String? {
        return image
    }

    fun setImage(image: String) {
        this.image = image
    }

    fun getVideo(): String? {
        return video
    }

    fun setVideo(video: String) {
        this.video = video
    }

    fun getRecipeIngredients(): List<RecipeIngredient>? {
        return recipeIngredients
    }

    fun setRecipeIngredients(recipeIngredients: List<RecipeIngredient>) {
        this.recipeIngredients = recipeIngredients
    }


}