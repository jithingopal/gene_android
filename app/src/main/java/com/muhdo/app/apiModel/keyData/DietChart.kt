package com.muhdo.app.apiModel.keyData

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class DietChart {


    @SerializedName("title")
    @Expose
    private var title: String? = null
    @SerializedName("result")
    @Expose
    private var result: Double? = null
    @SerializedName("_id")
    @Expose
    private var id: String? = null

    fun getTitle(): String? {
        return title
    }

    fun setTitle(title: String) {
        this.title = title
    }

    fun getResult(): Double? {
        return result
    }

    fun setResult(result: Double?) {
        this.result = result
    }

    fun getId(): String? {
        return id
    }

    fun setId(id: String) {
        this.id = id
    }

}