package com.muhdo.app.apiModel.v3

data class EpigenBaseReq(
    val user_id: String?=null,
    val kit_id: String?=null
    )