package com.muhdo.app.apiModel.v3

data class GetUserConditionReq(
    val condition: GetUserReq?=null
    )
data class GetUserReq(
    val _id: String?=null
)