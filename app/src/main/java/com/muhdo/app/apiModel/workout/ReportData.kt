package com.muhdo.app.apiModel.workout

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class ReportData {
    @SerializedName("mp")
    @Expose
    private var mp: Any? = null
    @SerializedName("ms")
    @Expose
    private var ms: Any? = null
    @SerializedName("rest")
    @Expose
    private var rest: Any? = null
    @SerializedName("session")
    @Expose
    private var session: Any? = null

    fun getMp(): Any? {
        return mp
    }

    fun setMp(mp: Any?) {
        this.mp = mp
    }

    fun getMs(): Any? {
        return ms
    }

    fun setMs(ms: Any?) {
        this.ms = ms
    }

    fun getRest(): Any? {
        return rest
    }

    fun setRest(rest: Any?) {
        this.rest = rest
    }

    fun getSession(): Any? {
        return session
    }

    fun setSession(session: Any?) {
        this.session = session
    }
}