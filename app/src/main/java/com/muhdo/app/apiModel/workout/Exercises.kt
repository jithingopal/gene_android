package com.muhdo.app.apiModel.workout

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class Exercises {


    @SerializedName("_id")
    @Expose
    private var id: String? = null
    @SerializedName("id")
    @Expose
    private var id1: Any? = null
    @SerializedName("exercise_name")
    @Expose
    private var exerciseName: String? = null
    @SerializedName("exercise_name_de")
    @Expose
    private var exerciseNameDe: String? = null
    @SerializedName("exercise_description")
    @Expose
    private var exerciseDescription: String? = null
    @SerializedName("exercise_description_de")
    @Expose
    private var exerciseDescriptionDe: String? = null
    @SerializedName("group_name")
    @Expose
    private var groupName: String? = null
    @SerializedName("exercise_type")
    @Expose
    private var exerciseType: String? = null

    @SerializedName("male")
    @Expose
    private var male: Boolean? = null
    @SerializedName("female")
    @Expose
    private var female: Boolean? = null
    @SerializedName("image_link")
    @Expose
    private var imageLink: String? = null
    @SerializedName("last_updated")
    @Expose
    private var lastUpdated: String? = null
    @SerializedName("is_favourite")
    @Expose
    private var isFavourite: Boolean? = null

    fun getId(): String? {
        return id
    }

    fun setId(id: String) {
        this.id = id
    }

    fun getId1(): Any? {
        return id1
    }

    fun setId1(id1: Any?) {
        this.id1 = id1
    }

    fun getExerciseName(): String? {
        return exerciseName
    }

    fun setExerciseName(exerciseName: String) {
        this.exerciseName = exerciseName
    }

    fun getExerciseNameDe(): String? {
        return exerciseNameDe
    }

    fun setExerciseNameDe(exerciseNameDe: String) {
        this.exerciseNameDe = exerciseNameDe
    }

    fun getExerciseDescription(): String? {
        return exerciseDescription
    }

    fun setExerciseDescription(exerciseDescription: String) {
        this.exerciseDescription = exerciseDescription
    }

    fun getExerciseDescriptionDe(): String? {
        return exerciseDescriptionDe
    }

    fun setExerciseDescriptionDe(exerciseDescriptionDe: String) {
        this.exerciseDescriptionDe = exerciseDescriptionDe
    }

    fun getGroupName(): String? {
        return groupName
    }

    fun setGroupName(groupName: String) {
        this.groupName = groupName
    }

    fun getExerciseType(): String? {
        return exerciseType
    }

    fun setExerciseType(exerciseType: String) {
        this.exerciseType = exerciseType
    }

    fun getMale(): Boolean? {
        return male
    }

    fun setMale(male: Boolean?) {
        this.male = male
    }

    fun getFemale(): Boolean? {
        return female
    }

    fun setFemale(female: Boolean?) {
        this.female = female
    }

    fun getImageLink(): String? {
        return imageLink
    }

    fun setImageLink(imageLink: String) {
        this.imageLink = imageLink
    }

    fun getLastUpdated(): String? {
        return lastUpdated
    }

    fun setLastUpdated(lastUpdated: String) {
        this.lastUpdated = lastUpdated
    }

    fun getIsFavourite(): Boolean? {
        return isFavourite
    }

    fun setIsFavourite(isFavourite: Boolean?) {
        this.isFavourite = isFavourite
    }

}