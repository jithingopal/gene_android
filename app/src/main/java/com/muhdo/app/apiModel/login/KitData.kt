package com.muhdo.app.apiModel.login

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class KitData {


    @SerializedName("kit_status")
    @Expose
    private var kitStatus: Int? = null
    @SerializedName("user_id")
    @Expose
    private var userId: String? = null
    @SerializedName("name")
    @Expose
    private var name: String? = null
    @SerializedName("gender")
    @Expose
    private var gender: String? = null
    @SerializedName("tenant_id")
    @Expose
    private var tenantId: String? = null
    @SerializedName("kit_id")
    @Expose
    private var kitId: String? = null
    @SerializedName("date")
    @Expose
    private var date: Any? = null

    fun getKitStatus(): Int? {
        return kitStatus
    }

    fun setKitStatus(kitStatus: Int?) {
        this.kitStatus = kitStatus
    }

    fun getUserId(): String? {
        return userId
    }

    fun setUserId(userId: String) {
        this.userId = userId
    }

    fun getName(): String? {
        return name
    }

    fun setName(name: String) {
        this.name = name
    }

    fun getGender(): String? {
        return gender
    }

    fun setGender(gender: String) {
        this.gender = gender
    }

    fun getTenantId(): String? {
        return tenantId
    }

    fun setTenantId(tenantId: String) {
        this.tenantId = tenantId
    }

    fun getKitId(): String? {
        return kitId
    }

    fun setKitId(kitId: String) {
        this.kitId = kitId
    }

    fun getDate(): Any? {
        return date
    }

    fun setDate(date: Any?) {
        this.date = date
    }


}