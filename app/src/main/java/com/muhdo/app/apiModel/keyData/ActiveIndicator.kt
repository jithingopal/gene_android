package com.muhdo.app.apiModel.keyData

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class ActiveIndicator {


    @SerializedName("title")
    @Expose
    private var title: String? = null

    @SerializedName("value")
    @Expose
    private var value: Int? = null

    @SerializedName("active")
    @Expose
    private var active: Boolean? = null

    @SerializedName("totolIndicators")
    @Expose
    private var totolIndicators: Int? = null
    @SerializedName("index")
    @Expose
    private var index: Int? = null

    @SerializedName("degree")
    @Expose
    private var degree: Int? = null


    fun getDegree(): Int? {
        return degree
    }

    fun setDegree(degree: Int?) {
        this.degree = degree
    }


    fun getTitle(): String? {
        return title
    }

    fun setTitle(title: String) {
        this.title = title
    }

    fun getValue(): Int? {
        return value
    }

    fun setValue(value: Int?) {
        this.value = value
    }

    fun getActive(): Boolean? {
        return active
    }

    fun setActive(active: Boolean?) {
        this.active = active
    }

    fun getTotolIndicators(): Int? {
        return totolIndicators
    }

    fun setTotolIndicators(totolIndicators: Int?) {
        this.totolIndicators = totolIndicators
    }

    fun getIndex(): Int? {
        return index
    }

    fun setIndex(index: Int?) {
        this.index = index
    }

}