package com.muhdo.app.apiModel.keyData

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class GenoTypeData {


    @SerializedName("_id")
    @Expose
    private var id: String? = null
    @SerializedName("id1")
    @Expose
    private var id1: Int? = null
    @SerializedName("title")
    @Expose
    private var title: String? = null
    @SerializedName("image")
    @Expose
    private var image: String? = null
    @SerializedName("description")
    @Expose
    private var description: String? = null
    @SerializedName("title_de")
    @Expose
    private var titleDe: String? = null
    @SerializedName("description_de")
    @Expose
    private var descriptionDe: String? = null
    @SerializedName("image_url")
    @Expose
    private var imageUrl: String? = null
    @SerializedName("is_deleted")
    @Expose
    private var isDeleted: Boolean? = null
    @SerializedName("updatedAt")
    @Expose
    private var updatedAt: String? = null
    @SerializedName("icon")
    @Expose
    private var icon: String? = null

    fun getId(): String? {
        return id
    }

    fun setId(id: String) {
        this.id = id
    }

    fun getId1(): Int? {
        return id1
    }

    fun setId1(id1: Int?) {
        this.id1 = id1
    }

    fun getTitle(): String? {
        return title
    }

    fun setTitle(title: String) {
        this.title = title
    }

    fun getImage(): String? {
        return image
    }

    fun setImage(image: String) {
        this.image = image
    }

    fun getDescription(): String? {
        return description
    }

    fun setDescription(description: String) {
        this.description = description
    }

    fun getTitleDe(): String? {
        return titleDe
    }

    fun setTitleDe(titleDe: String) {
        this.titleDe = titleDe
    }

    fun getDescriptionDe(): String? {
        return descriptionDe
    }

    fun setDescriptionDe(descriptionDe: String) {
        this.descriptionDe = descriptionDe
    }

    fun getImageUrl(): String? {
        return imageUrl
    }

    fun setImageUrl(imageUrl: String) {
        this.imageUrl = imageUrl
    }

    fun getIsDeleted(): Boolean? {
        return isDeleted
    }

    fun setIsDeleted(isDeleted: Boolean?) {
        this.isDeleted = isDeleted
    }

    fun getUpdatedAt(): String? {
        return updatedAt
    }

    fun setUpdatedAt(updatedAt: String) {
        this.updatedAt = updatedAt
    }

    fun getIcon(): String? {
        return icon
    }

    fun setIcon(icon: String) {
        this.icon = icon
    }
}