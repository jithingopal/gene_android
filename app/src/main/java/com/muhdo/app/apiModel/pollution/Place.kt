package com.muhdo.app.apiModel.pollution

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class Place {

    @SerializedName("name")
    @Expose
    private var name: String? = null
    @SerializedName("state")
    @Expose
    private var state: Any? = null
    @SerializedName("country")
    @Expose
    private var country: String? = null

    fun getName(): String? {
        return name
    }

    fun setName(name: String) {
        this.name = name
    }

    fun getState(): Any? {
        return state
    }

    fun setState(state: Any) {
        this.state = state
    }

    fun getCountry(): String? {
        return country
    }

    fun setCountry(country: String) {
        this.country = country
    }
}