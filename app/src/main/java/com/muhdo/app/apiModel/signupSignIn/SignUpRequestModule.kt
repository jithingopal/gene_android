package com.muhdo.app.apiModel.signupSignIn

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class SignUpRequestModule {

    @SerializedName("email")
    @Expose
    private var email: String? = null

    @SerializedName("username")
    @Expose
    private var username: String? = null

    @SerializedName("terms_and_condition")
    @Expose
    var termsCondition: Boolean? = null


    @SerializedName("password")
    @Expose
    private var password: String? = null
    @SerializedName("phone")
    @Expose
    private var phone: String? = null
    @SerializedName("gender")
    @Expose
    private var gender: String? = null
    @SerializedName("first_name")
    @Expose
    private var firstName: String? = null
    @SerializedName("last_name")
    @Expose
    private var lastName: String? = null
    @SerializedName("address_lane1")
    @Expose
    private var addressLane1: String? = null
    @SerializedName("city")
    @Expose
    private var city: String? = null
    @SerializedName("zipcode")
    @Expose
    private var zipcode: String? = null
    @SerializedName("enterpriser")
    @Expose
    private var enterpriser: String? = null
    @SerializedName("subscribed")
    @Expose
    private var subscribed: String? = null
    @SerializedName("subscribed_type")
    @Expose
    private var subscribedType: String? = null
    @SerializedName("birthday")
    @Expose
    private var dob: String? = null
    @SerializedName("country")
    @Expose
    private var country: String? = null

    @SerializedName("height")
    @Expose
    private var height: String? = null
    @SerializedName("weight")
    @Expose
    private var weight: String? = null
    @SerializedName("height_unit")
    @Expose
    private var heightUnit: String? = null
    @SerializedName("weight_unit")
    @Expose
    private var weightUnit: String? = null
    @SerializedName("country_region")
    @Expose
    private var countryRegion: String? = null
    @SerializedName("checkbox")
    @Expose
    private var updatesAndOffers: String? = null

    constructor(
        email: String?,
        username: String?,
        password: String?,
        phone: String?,
        gender: String?,
        firstName: String?,
        lastName: String?,
        addressLane1: String?,
        city: String?,
        zipcode: String?,
        enterpriser: String?,
        subscribed: String?,
        subscribedType: String?,
        country: String?,
        termsAndConditions: Boolean?,
        updatesAndOffers: String?,
        height: String?,
        weight: String?,
        heightUnit: String?,
        weightUnit: String?,
        countryRegion: String?,
        dob:String?
    ) {
        this.email = email
        this.username = username
        this.password = password
        this.phone = phone
        this.gender = gender
        this.firstName = firstName
        this.lastName = lastName
        this.addressLane1 = addressLane1
        this.city = city
        this.zipcode = zipcode
        this.enterpriser = enterpriser
        this.subscribed = subscribed
        this.subscribedType = subscribedType
        this.country = country
        this.termsCondition = termsAndConditions
        this.updatesAndOffers=updatesAndOffers
        this.height=height
        this.weight=weight
        this.heightUnit=heightUnit
        this.weightUnit=weightUnit
        this.countryRegion=countryRegion
        this.dob=dob

    }


    fun getEmail(): String? {
        return email
    }

    fun setEmail(email: String) {
        this.email = email
    }


    fun getUsername(): String? {
        return username
    }

    fun setUsername(username: String) {
        this.username = username
    }

    fun getPassword(): String? {
        return password
    }

    fun setPassword(password: String) {
        this.password = password
    }

    fun getPhone(): String? {
        return phone
    }

    fun setPhone(phone: String) {
        this.phone = phone
    }

    fun getGender(): String? {
        return gender
    }

    fun setGender(gender: String) {
        this.gender = gender
    }

    fun getFirstName(): String? {
        return firstName
    }

    fun setFirstName(firstName: String) {
        this.firstName = firstName
    }

    fun getLastName(): String? {
        return lastName
    }

    fun setLastName(lastName: String) {
        this.lastName = lastName
    }

    fun getAddressLane1(): String? {
        return addressLane1
    }

    fun setAddressLane1(addressLane1: String) {
        this.addressLane1 = addressLane1
    }

    fun getCity(): String? {
        return city
    }

    fun setCity(city: String) {
        this.city = city
    }

    fun getZipcode(): String? {
        return zipcode
    }

    fun setZipcode(zipcode: String) {
        this.zipcode = zipcode
    }

    fun getEnterpriser(): String? {
        return enterpriser
    }

    fun setEnterpriser(enterpriser: String) {
        this.enterpriser = enterpriser
    }

    fun getSubscribed(): String? {
        return subscribed
    }

    fun setSubscribed(subscribed: String) {
        this.subscribed = subscribed
    }

    fun getSubscribedType(): String? {
        return subscribedType
    }

    fun setSubscribedType(subscribedType: String) {
        this.subscribedType = subscribedType
    }
}