package com.muhdo.app.apiModel.v3

data  class GeneticOverviewResultData (

      val title: String?,
      val type: String? ,
      val show: Boolean? ,
      val content: String?
)