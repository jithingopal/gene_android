package com.muhdo.app.apiModel.mealModel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class MealRequest {

    @SerializedName("goal")
    @Expose
    private var goal: String? = null
    @SerializedName("is_vegetarian")
    @Expose
    private var isVegetarian: Boolean? = null
    @SerializedName("is_vegan")
    @Expose
    private var isVegan: Boolean? = null
    @SerializedName("is_fish")
    @Expose
    private var isFish: Boolean? = null
    @SerializedName("omit")
    @Expose
    private var omit: Omit? = null
    @SerializedName("alcohol_free")
    @Expose
    private var alcoholFree: Boolean? = null
    @SerializedName("meal_per_day")
    @Expose
    private var mealPerDay: Int? = null
    @SerializedName("height")
    @Expose
    private var height: Int? = null
    @SerializedName("height_unit")
    @Expose
    private var heightUnit: String? = null
    @SerializedName("weight")
    @Expose
    private var weight: Int? = null
    @SerializedName("weight_unit")
    @Expose
    private var weightUnit: String? = null
    @SerializedName("activity")
    @Expose
    private var activity: Int? = null
    @SerializedName("user_id")
    @Expose
    private var userId: String? = null
    @SerializedName("survey_type")
    @Expose
    private var surveyType: String? = null
    @SerializedName("dob")
    @Expose
    private var dob: String? = null
    @SerializedName("gender")
    @Expose
    private var gender: String? = null

    constructor(
        goal: String?,
        isVegetarian: Boolean?,
        isVegan: Boolean?,
        isFish: Boolean?,
        omit: Omit?,
        alcoholFree: Boolean?,
        mealPerDay: Int?,
        height: Int?,
        heightUnit: String?,
        weight: Int?,
        weightUnit: String?,
        activity: Int?,
        userId: String?,
        surveyType: String?,
        dob: String?,
        gender: String?
    ) {
        this.goal = goal
        this.isVegetarian = isVegetarian
        this.isVegan = isVegan
        this.isFish = isFish
        this.omit = omit
        this.alcoholFree = alcoholFree
        this.mealPerDay = mealPerDay
        this.height = height
        this.heightUnit = heightUnit
        this.weight = weight
        this.weightUnit = weightUnit
        this.activity = activity
        this.userId = userId
        this.surveyType = surveyType
        this.dob = dob
        this.gender = gender
    }

    fun getGoal(): String? {
        return goal
    }

    fun setGoal(goal: String) {
        this.goal = goal
    }

    fun getIsVegetarian(): Boolean? {
        return isVegetarian
    }

    fun setIsVegetarian(isVegetarian: Boolean?) {
        this.isVegetarian = isVegetarian
    }

    fun getIsVegan(): Boolean? {
        return isVegan
    }


    fun getDob(): String? {
        return dob
    }

    fun setDob(dob: String) {
        this.dob = dob
    }

    fun setIsVegan(isVegan: Boolean?) {
        this.isVegan = isVegan
    }

    fun getIsFish(): Boolean? {
        return isFish
    }

    fun setIsFish(isFish: Boolean?) {
        this.isFish = isFish
    }

    fun getOmit(): Omit? {
        return omit
    }

    fun setOmit(omit: Omit) {
        this.omit = omit
    }

    fun getAlcoholFree(): Boolean? {
        return alcoholFree
    }

    fun setAlcoholFree(alcoholFree: Boolean?) {
        this.alcoholFree = alcoholFree
    }

    fun getMealPerDay(): Int? {
        return mealPerDay
    }

    fun setMealPerDay(mealPerDay: Int?) {
        this.mealPerDay = mealPerDay
    }

    fun getHeight(): Int? {
        return height
    }

    fun setHeight(height: Int?) {
        this.height = height
    }

    fun getHeightUnit(): String? {
        return heightUnit
    }

    fun setHeightUnit(heightUnit: String) {
        this.heightUnit = heightUnit
    }

    fun getWeight(): Int? {
        return weight
    }

    fun setWeight(weight: Int?) {
        this.weight = weight
    }

    fun getWeightUnit(): String? {
        return weightUnit
    }

    fun setWeightUnit(weightUnit: String) {
        this.weightUnit = weightUnit
    }

    fun getActivity(): Int? {
        return activity
    }

    fun setActivity(activity: Int?) {
        this.activity = activity
    }

    fun getUserId(): String? {
        return userId
    }

    fun setUserId(userId: String) {
        this.userId = userId
    }

    fun getSurveyType(): String? {
        return surveyType
    }

    fun setSurveyType(surveyType: String) {
        this.surveyType = surveyType
    }

    fun getGender(): String? {
        return gender
    }

    fun setGender(gender: String) {
        this.gender = gender
    }
}