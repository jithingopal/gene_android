package com.muhdo.app.apiModel.v3.questionnairestatus

data class QuestionnaireStatus(
    val `data`: List<Data>?,
    val message: String,
    val statusCode: Int
)