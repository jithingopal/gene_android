package com.muhdo.app.apiModel.mealModel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.muhdo.app.apiModel.mealModel.daysModel.*


class ChooseMealModelData {


    @SerializedName("Wednesday")
    @Expose
    private var wednesday: List<List<Monday>>? = null
    @SerializedName("Sunday")
    @Expose
    private var sunday: List<List<Monday>>? = null
    @SerializedName("Thursday")
    @Expose
    private var thursday: List<List<Monday>>? = null
    @SerializedName("Friday")
    @Expose
    private var friday: List<List<Monday>>? = null
    @SerializedName("Saturday")
    @Expose
    private var saturday: List<List<Monday>>? = null
    @SerializedName("Monday")
    @Expose
    private var monday: List<List<Monday>>? = null
    @SerializedName("Tuesday")
    @Expose
    private var tuesday: List<List<Monday>>? = null
    @SerializedName("total_meals")
    @Expose
    private var totalMeals: Int? = null

    fun getWednesday(): List<List<Monday>>? {
        return wednesday
    }

    fun setWednesday(wednesday: List<List<Monday>>) {
        this.wednesday = wednesday
    }

    fun getSunday(): List<List<Monday>>? {
        return sunday
    }

    fun setSunday(sunday: List<List<Monday>>) {
        this.sunday = sunday
    }

    fun getThursday(): List<List<Monday>>? {
        return thursday
    }

    fun setThursday(thursday: List<List<Monday>>) {
        this.thursday = thursday
    }

    fun getFriday(): List<List<Monday>>? {
        return friday
    }

    fun setFriday(friday: List<List<Monday>>) {
        this.friday = friday
    }

    fun getSaturday(): List<List<Monday>>? {
        return saturday
    }

    fun setSaturday(saturday: List<List<Monday>>) {
        this.saturday = saturday
    }

    fun getMonday(): List<List<Monday>>? {
        return monday
    }

    fun setMonday(monday: List<List<Monday>>) {
        this.monday = monday
    }

    fun getTuesday(): List<List<Monday>>? {
        return tuesday
    }

    fun setTuesday(tuesday: List<List<Monday>>) {
        this.tuesday = tuesday
    }

    fun getTotalMeals(): Int? {
        return totalMeals
    }

    fun setTotalMeals(totalMeals: Int?) {
        this.totalMeals = totalMeals
    }
}