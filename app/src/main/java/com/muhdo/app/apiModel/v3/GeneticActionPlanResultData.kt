package com.muhdo.app.apiModel.v3


data class GeneticActionPlanResultData(

     var title: String? ,
     var value: Int?,
     var description: String?
)