package com.muhdo.app.apiModel.result

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.muhdo.app.apiModel.mealModel.FavoriteListModelData

class SleepBarChartResponse {
    @SerializedName("code")
    @Expose
    private var code: Int? = null
    @SerializedName("message")
    @Expose
    private var message: String? = null
    @SerializedName("data")
    @Expose
    open var sleepBarChartList: List<SleepBarChart>? = null




    fun getCode(): Int? {
        return code
    }

    fun setCode(code: Int?) {
        this.code = code
    }

    fun getMessage(): String? {
        return message
    }

    fun setMessage(message: String) {
        this.message = message
    }


    inner class SleepBarChart {
        @SerializedName("date")
        @Expose
        open var barChartDate: String? = null

        @SerializedName("day")
        @Expose
        open var barChartDay: String? = null

        @SerializedName("sleep_hours")
        @Expose
        open var barChartSleepHours: String? = null


    }


}