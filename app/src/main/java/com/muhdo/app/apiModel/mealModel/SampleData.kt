package com.muhdo.app.apiModel.mealModel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class SampleData {


    @SerializedName("show_alert")
    private var message: String? = null

    fun getMessage(): String? {
        return message
    }

    fun setMessage(message: String) {
        this.message = message
    }


}