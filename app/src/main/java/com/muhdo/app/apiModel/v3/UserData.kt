package com.muhdo.app.apiModel.v3

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class UserData {

     var id: String? = null 
     var gender: String? = null 
     var last_name: String? = null 
     var first_name: String? = null 
     var kit_id: String? = null 
     var kit_activated: Boolean? = null 
     var file_parsed: Boolean? = null 
     var phone: String? = null 
     var address_lane1: String? = null 
     var address_lane2: String? = null 
     var city: String? = null 
     var zipcode: String? = null 
     var state: String? = null 
     var country: String? = null 
     var country_region: String? = null 
     var username: String? = null 
     var weight: Float? = null 
     var weight_unit: String? = null 
     var height: Float? = null 
     var height_unit: String? = null 
     var timezone: String? = null 
     var timezone_offset: Int? = null 
     var confirm_token: String? = null 
     var status: String? = null 
     var email_confirmed: Boolean? = null 
     var newsletter: Boolean? = null 
     var terms_and_condition: Boolean? = null 
     var subscribed: Boolean? = null 
     var subscribed_type: String? = null 
     var suspended: Boolean? = null 
     var email: String? = null 
     var birthday: String? = null 
     var enterpriser: String? = null 
     var createdAt: String? = null 
     var updatedAt: String? = null 
     var last_login: String? = null 
     var key_data_generated: Boolean? = null


}