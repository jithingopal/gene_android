package com.muhdo.app.apiModel.login

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class Message {


    @SerializedName("code")
    @Expose
    private var code: String? = null
    @SerializedName("name")
    @Expose
    private var name: String? = null
    @SerializedName("message")
    @Expose
    private var message: String? = null

    fun getCode(): String? {
        return code
    }

    fun setCode(code: String) {
        this.code = code
    }

    fun getName(): String? {
        return name
    }

    fun setName(name: String) {
        this.name = name
    }

    fun getMessage(): String? {
        return message
    }

    fun setMessage(message: String) {
        this.message = message
    }
}