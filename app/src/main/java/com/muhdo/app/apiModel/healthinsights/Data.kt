package com.muhdo.app.apiModel.healthinsights

data class Data(
    val genes_of_interest: String,
    val intro: String,
    val output: Output?,
    val title: String,
    val type: String,
    val indicators:List<String>,
    val status: String,
    val value: Double
)