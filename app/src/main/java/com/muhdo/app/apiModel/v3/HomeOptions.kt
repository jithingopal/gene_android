package com.muhdo.app.apiModel.v3

data class HomeOptions(val imageRes: Int, val title: String)