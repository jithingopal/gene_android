package com.muhdo.app.apiModel.v3

data class GeneticActionPlanReq(
    val user_id: String?=null,
    val category: String?=null
)