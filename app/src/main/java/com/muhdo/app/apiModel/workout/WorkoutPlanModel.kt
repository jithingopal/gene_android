package com.muhdo.app.apiModel.workout

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class WorkoutPlanModel {
    @SerializedName("statusCode")
    @Expose
    private var code: Int? = null
    @SerializedName("message")
    @Expose
    private var message: String? = null
    @SerializedName("data")
    @Expose
    private var data: WorkoutPlanData? = null

    fun getCode(): Int? {
        return code
    }

    fun setCode(code: Int?) {
        this.code = code
    }

    fun getMessage(): String? {
        return message
    }

    fun setMessage(message: String) {
        this.message = message
    }

    fun getData(): WorkoutPlanData? {
        return data
    }

    fun setData(data: WorkoutPlanData) {
        this.data = data
    }

}