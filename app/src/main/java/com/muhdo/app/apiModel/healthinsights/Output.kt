package com.muhdo.app.apiModel.healthinsights

data class Output(
    val recommondation: String,
    val result: String,
    val indicator: Int
)