package com.muhdo.app.apiModel.v3.questionnairestatus

data class Data(
    val category: String,
    val status: Boolean
)