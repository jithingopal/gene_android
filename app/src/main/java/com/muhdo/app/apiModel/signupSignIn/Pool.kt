package com.muhdo.app.apiModel.signupSignIn

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class Pool {


    @SerializedName("userPoolId")
    @Expose
    private var userPoolId: String? = null
    @SerializedName("clientId")
    @Expose
    private var clientId: String? = null
    @SerializedName("client")
    @Expose
    private var client: Client? = null
    @SerializedName("advancedSecurityDataCollectionFlag")
    @Expose
    private var advancedSecurityDataCollectionFlag: Boolean? = null

    fun getUserPoolId(): String? {
        return userPoolId
    }

    fun setUserPoolId(userPoolId: String) {
        this.userPoolId = userPoolId
    }

    fun getClientId(): String? {
        return clientId
    }

    fun setClientId(clientId: String) {
        this.clientId = clientId
    }

    fun getClient(): Client? {
        return client
    }

    fun setClient(client: Client) {
        this.client = client
    }

    fun getAdvancedSecurityDataCollectionFlag(): Boolean? {
        return advancedSecurityDataCollectionFlag
    }

    fun setAdvancedSecurityDataCollectionFlag(advancedSecurityDataCollectionFlag: Boolean?) {
        this.advancedSecurityDataCollectionFlag = advancedSecurityDataCollectionFlag
    }

}