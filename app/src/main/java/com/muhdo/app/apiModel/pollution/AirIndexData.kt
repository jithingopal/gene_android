package com.muhdo.app.apiModel.pollution

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class AirIndexData {


    @SerializedName("place")
    @Expose
    private var place: Place? = null
    @SerializedName("aqi")
    @Expose
    private var aqi: Int? = null
    @SerializedName("category")
    @Expose
    private var category: String? = null
    @SerializedName("color")
    @Expose
    private var color: String? = null
    @SerializedName("daily_message")
    @Expose
    private var dailyMessage: String? = null
    @SerializedName("health_implications")
    @Expose
    private var healthImplications: String? = null
    @SerializedName("suggestions")
    @Expose
    private var suggestions: String? = null
    @SerializedName("recommendations")
    @Expose
    private var recommendations: String? = null

    fun getRecommendations() = recommendations
    fun setRecommendations(value: String) {
        recommendations = value
    }

    fun getPlace(): Place? {
        return place
    }

    fun setPlace(place: Place) {
        this.place = place
    }

    fun getAqi(): Int? {
        return aqi
    }

    fun setAqi(aqi: Int?) {
        this.aqi = aqi
    }

    fun getCategory(): String? {
        return category
    }

    fun setCategory(category: String) {
        this.category = category
    }

    fun getColor(): String? {
        return color
    }

    fun setColor(color: String) {
        this.color = color
    }

    fun getDailyMessage(): String? {
        return dailyMessage
    }

    fun setDailyMessage(dailyMessage: String) {
        this.dailyMessage = dailyMessage
    }

    fun getHealthImplications(): String? {
        return healthImplications
    }

    fun setHealthImplications(healthImplications: String) {
        this.healthImplications = healthImplications
    }

    fun getSuggestions(): String? {
        return suggestions
    }

    fun setSuggestions(suggestions: String) {
        this.suggestions = suggestions
    }
}