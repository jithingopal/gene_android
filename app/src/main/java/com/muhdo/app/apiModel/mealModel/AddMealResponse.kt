package com.muhdo.app.apiModel.mealModel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.muhdo.app.ui.epigenticResult.model.EpigenticHealth

class AddMealResponse {

    @SerializedName("statusCode")
    @Expose
    private var code: Int? = null

    @SerializedName("message")
    @Expose
    private var message: String? = null

    @SerializedName("data")
    @Expose
    var data: AddMealData? = null

    fun getCode(): Int? {
        return code
    }

    fun setCode(code: Int?) {
        this.code = code
    }

    fun getMessage(): String? {
        return message
    }

    fun setMessage(message: String) {
        this.message = message
    }


}

class AddMealData {

    @SerializedName("dob")
    @Expose
     var dob: String? = null

    @SerializedName("gender")
    @Expose
     var gender: String? = null

    @SerializedName("height")
    @Expose
     var height: Float? = null

    @SerializedName("height_unit")
    @Expose
     var height_unit: String? = null

    @SerializedName("is_fish")
    @Expose
     var is_fish: Boolean? = null

    @SerializedName("is_vegan")
    @Expose
    var is_vegan: Boolean? = null

    @SerializedName("is_vegetarian")
    @Expose
     var is_vegetarian: Boolean? = null

    @SerializedName("meal_per_day")
    @Expose
     var mealPerDay: Int? = null

    @SerializedName("survey_type")
    @Expose
     var survey_type: String? = null

    @SerializedName("user_id")
    @Expose
     var userId: String? = null

    @SerializedName("weight")
    @Expose
     var weight: Float? = null

    @SerializedName("weight_unit")
    @Expose
     var weightUnit: String? = null

    @SerializedName("activity_value")
    @Expose
    var activityValue: Int? = null

    @SerializedName("exclusion")
    @Expose
     var exclusionObj: Exclusion? = null

    @SerializedName("goal_type")
    @Expose
     var goal_type: String? = null

    @SerializedName("decoded_user_id")
    @Expose
     var decoded_user_id: String? = null

    @SerializedName("section_id")
    @Expose
     var section_id: String? = null


    @SerializedName("ReportBySectionId")
    @Expose
    var reportBySectionIdObj: List<ReportedBySection>? = null

    class ReportedBySection {
        @SerializedName("_id")
        @Expose
        var _id: String? = null

        @SerializedName("category_id")
        @Expose
        var category_id: String? = null

        @SerializedName("genotype")
        @Expose
        var genotype: String? = null

        @SerializedName("comment")
        @Expose
        var comment: String? = null

        @SerializedName("index")
        @Expose
        var index: String? = null

        @SerializedName("title")
        @Expose
        var title: String? = null

        @SerializedName("description")
        @Expose
        var description: String? = null

        @SerializedName("result")
        @Expose
        var result: Int? = null

        @SerializedName("result_id")
        @Expose
        var resultId: String? = null

        @SerializedName("tag")
        @Expose
        var tag: String? = null


    }

    class Exclusion {
        @SerializedName("dairy_free")
        @Expose
        var isDairyfree: Boolean? = null

        @SerializedName("egg_free")
        @Expose
        var isEgg_free: Boolean? = null

        @SerializedName("pork_free")
        @Expose
        var isPork_free: Boolean? = null

        @SerializedName("shellfish_free")
        @Expose
        var isshellfish_free: Boolean? = null

        @SerializedName("soy_free")
        @Expose
        var issoy_free: Boolean? = null

        @SerializedName("alcohol_free")
        @Expose
        var isAlcohol_free: Boolean? = null

        @SerializedName("is_vegetarian")
        @Expose
        var is_vegetarian: Boolean? = null

        @SerializedName("is_fish")
        @Expose
        var is_fish: Boolean? = null

        @SerializedName("is_vegan")
        @Expose
        var is_vegan: Boolean? = null


    }

}


