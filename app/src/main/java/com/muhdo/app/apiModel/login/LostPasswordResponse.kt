package com.muhdo.app.apiModel.login

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.muhdo.app.apiModel.signupSignIn.Headers


class LostPasswordResponse {
    @SerializedName("statusCode")
    @Expose
    private var statusCode: Int? = null
    @SerializedName("headers")
    @Expose
    private var headers: Headers? = null

    fun getStatusCode(): Int? {
        return statusCode
    }

    fun setStatusCode(statusCode: Int?) {
        this.statusCode = statusCode
    }

    fun getHeaders(): Headers? {
        return headers
    }

    fun setHeaders(headers: Headers) {
        this.headers = headers
    }
}