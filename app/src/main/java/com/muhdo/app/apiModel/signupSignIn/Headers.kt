package com.muhdo.app.apiModel.signupSignIn

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class Headers {


    @SerializedName("Content-Type")
    @Expose
    private var contentType: String? = null
    @SerializedName("Access-Control-Allow-Origin")
    @Expose
    private var accessControlAllowOrigin: String? = null
    @SerializedName("Access-Control-Allow-Credentials")
    @Expose
    private var accessControlAllowCredentials: Boolean? = null
    @SerializedName("Access-Control-Allow-Methods")
    @Expose
    private var accessControlAllowMethods: String? = null

    fun getContentType(): String? {
        return contentType
    }

    fun setContentType(contentType: String) {
        this.contentType = contentType
    }

    fun getAccessControlAllowOrigin(): String? {
        return accessControlAllowOrigin
    }

    fun setAccessControlAllowOrigin(accessControlAllowOrigin: String) {
        this.accessControlAllowOrigin = accessControlAllowOrigin
    }

    fun getAccessControlAllowCredentials(): Boolean? {
        return accessControlAllowCredentials
    }

    fun setAccessControlAllowCredentials(accessControlAllowCredentials: Boolean?) {
        this.accessControlAllowCredentials = accessControlAllowCredentials
    }

    fun getAccessControlAllowMethods(): String? {
        return accessControlAllowMethods
    }

    fun setAccessControlAllowMethods(accessControlAllowMethods: String) {
        this.accessControlAllowMethods = accessControlAllowMethods
    }


}