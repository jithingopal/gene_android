package com.muhdo.app.apiModel.v3

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class UserDataPojo {

    public var statusCode: Int? = null
    public var message: String? = null
    public var data: List<UserData>? = null
}