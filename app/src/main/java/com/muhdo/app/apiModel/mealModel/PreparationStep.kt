package com.muhdo.app.apiModel.mealModel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class PreparationStep {


    @SerializedName("_id")
    @Expose
    private var id: String? = null
    @SerializedName("value")
    @Expose
    private var value: String? = null
    @SerializedName("step")
    @Expose
    private var step: Int? = null
    @SerializedName("recipe_id")
    @Expose
    private var recipeId: String? = null
    @SerializedName("is_deleted")
    @Expose
    private var isDeleted: Boolean? = null

    fun getId(): String? {
        return id
    }

    fun setId(id: String) {
        this.id = id
    }

    fun getValue(): String? {
        return value
    }

    fun setValue(value: String) {
        this.value = value
    }

    fun getStep(): Int? {
        return step
    }

    fun setStep(step: Int?) {
        this.step = step
    }

    fun getRecipeId(): String? {
        return recipeId
    }

    fun setRecipeId(recipeId: String) {
        this.recipeId = recipeId
    }

    fun getIsDeleted(): Boolean? {
        return isDeleted
    }

    fun setIsDeleted(isDeleted: Boolean?) {
        this.isDeleted = isDeleted
    }
}