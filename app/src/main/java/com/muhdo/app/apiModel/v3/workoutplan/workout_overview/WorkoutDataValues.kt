package com.muhdo.app.apiModel.v3.workoutplan.workout_overview

import com.muhdo.app.apiModel.v3.GeneticOverviewResultData

data class WorkoutDataValues (
    var title: String? ,
    var description: String? ,
    var type: String? ,
    var subTitle: Array<String>?,
    var content: Array<String>?
)