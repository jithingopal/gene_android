package com.muhdo.app.apiModel.keyData

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class KeyData {

    @SerializedName("relevantTags")
    @Expose
    private var relevantTags: List<String>? = null
    @SerializedName("_id")
    @Expose
    private var id: String? = null
    @SerializedName("mode_id")
    @Expose
    private var modeId: String? = null
    @SerializedName("user_id")
    @Expose
    private var userId: String? = null
    @SerializedName("__v")
    @Expose
    private var v: Int? = null
    @SerializedName("indicators")
    @Expose
    private var indicators: List<Indicator>? = null
    @SerializedName("chart")
    @Expose
    private var chart: List<DietChart>? = null

    fun getRelevantTags(): List<String>? {
        return relevantTags
    }

    fun setRelevantTags(relevantTags: List<String>) {
        this.relevantTags = relevantTags
    }

    fun getId(): String? {
        return id
    }

    fun setId(id: String) {
        this.id = id
    }

    fun getModeId(): String? {
        return modeId
    }

    fun setModeId(modeId: String) {
        this.modeId = modeId
    }

    fun getUserId(): String? {
        return userId
    }

    fun setUserId(userId: String) {
        this.userId = userId
    }

    fun getV(): Int? {
        return v
    }

    fun setV(v: Int?) {
        this.v = v
    }

    fun getIndicators(): List<Indicator>? {
        return indicators
    }

    fun setIndicators(indicators: List<Indicator>) {
        this.indicators = indicators
    }

    fun getChart(): List<DietChart>? {
        return chart
    }

    fun setChart(chart: List<DietChart>) {
        this.chart = chart
    }

}