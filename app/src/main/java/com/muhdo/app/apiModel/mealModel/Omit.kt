package com.muhdo.app.apiModel.mealModel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable


class Omit : Serializable{

    @SerializedName("beef_free")
    @Expose
    private var beefFree: Boolean? = null
    @SerializedName("egg_free")
    @Expose
    private var eggFree: Boolean? = null
    @SerializedName("dairy_free")
    @Expose
    private var dairyFree: Boolean? = null
    @SerializedName("gluten_free")
    @Expose
    private var glutenFree: Boolean? = null
    @SerializedName("alcohol_free")
    @Expose
    private var alcoholFree: Boolean? = null
    @SerializedName("pork_free")
    @Expose
    private var porkFree: Boolean? = null
    @SerializedName("nuts_free")
    @Expose
    private var nutsFree: Boolean? = null
    @SerializedName("soy_free")
    @Expose
    private var soyFree: Boolean? = null
    @SerializedName("shellfish_free")
    @Expose
    private var shellfishFree: Boolean? = null

    constructor(
        beefFree: Boolean?,
        eggFree: Boolean?,
        dairyFree: Boolean?,
        glutenFree: Boolean?,
        porkFree: Boolean?,
        nutsFree: Boolean?,
        soyFree: Boolean?,
        shellfishFree: Boolean?
    ) {
        this.beefFree = beefFree
        this.eggFree = eggFree
        this.dairyFree = dairyFree
        this.glutenFree = glutenFree
        this.porkFree = porkFree
        this.nutsFree = nutsFree
        this.soyFree = soyFree
        this.shellfishFree = shellfishFree
    }

    fun getBeefFree(): Boolean? {
        return beefFree
    }

    fun setBeefFree(beefFree: Boolean?) {
        this.beefFree = beefFree
    }

    fun getEggFree(): Boolean? {
        return eggFree
    }

    fun setEggFree(eggFree: Boolean?) {
        this.eggFree = eggFree
    }

    fun getDairyFree(): Boolean? {
        return dairyFree
    }

    fun setDairyFree(dairyFree: Boolean?) {
        this.dairyFree = dairyFree
    }

    fun getGlutenFree(): Boolean? {
        return glutenFree
    }

    fun setGlutenFree(glutenFree: Boolean?) {
        this.glutenFree = glutenFree
    }

    fun getAlcoholFree(): Boolean? {
        return alcoholFree
    }

    fun setAlcoholFree(alcoholFree: Boolean?) {
        this.alcoholFree = alcoholFree
    }

    fun getPorkFree(): Boolean? {
        return porkFree
    }

    fun setPorkFree(porkFree: Boolean?) {
        this.porkFree = porkFree
    }

    fun getNutsFree(): Boolean? {
        return nutsFree
    }

    fun setNutsFree(nutsFree: Boolean?) {
        this.nutsFree = nutsFree
    }

    fun getSoyFree(): Boolean? {
        return soyFree
    }

    fun setSoyFree(soyFree: Boolean?) {
        this.soyFree = soyFree
    }

    fun getShellfishFree(): Boolean? {
        return shellfishFree
    }

    fun setShellfishFree(shellfishFree: Boolean?) {
        this.shellfishFree = shellfishFree
    }

}