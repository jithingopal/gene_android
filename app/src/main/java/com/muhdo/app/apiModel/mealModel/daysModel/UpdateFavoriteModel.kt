package com.muhdo.app.apiModel.mealModel.daysModel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.muhdo.app.apiModel.mealModel.SampleData


class UpdateFavoriteModel {

    @SerializedName("statusCode")
    @Expose
    private var code: Int? = null
    @SerializedName("message")
    @Expose
    private var message: String? = null
    @SerializedName("data")
    @Expose
    private var data: SampleData? = null

    fun getCode(): Int? {
        return code
    }

    fun setCode(code: Int?) {
        this.code = code
    }

    fun getMessage(): String? {
        return message
    }

    fun setMessage(message: String) {
        this.message = message
    }

    fun getData(): SampleData? {
        return data
    }

    fun setData(data: SampleData) {
        this.data = data
    }
}