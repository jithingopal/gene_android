package com.muhdo.app.apiModel.signupSignIn

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class Client {


    @SerializedName("endpoint")
    @Expose
    private var endpoint: String? = null
    @SerializedName("userAgent")
    @Expose
    private var userAgent: String? = null

    fun getEndpoint(): String? {
        return endpoint
    }

    fun setEndpoint(endpoint: String) {
        this.endpoint = endpoint
    }

    fun getUserAgent(): String? {
        return userAgent
    }

    fun setUserAgent(userAgent: String) {
        this.userAgent = userAgent
    }

}