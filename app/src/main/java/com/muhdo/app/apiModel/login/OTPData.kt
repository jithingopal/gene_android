package com.muhdo.app.apiModel.login

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class OTPData {


    @SerializedName("accessToken")
    @Expose
    private var accessToken: String? = null
    @SerializedName("idToken")
    @Expose
    private var idToken: String? = null
    @SerializedName("refreshToken")
    @Expose
    private var refreshToken: String? = null
    @SerializedName("user_id")
    @Expose
    private var userId: String? = null

    fun getAccessToken(): String? {
        return accessToken
    }

    fun setAccessToken(accessToken: String) {
        this.accessToken = accessToken
    }

    fun getIdToken(): String? {
        return idToken
    }

    fun setIdToken(idToken: String) {
        this.idToken = idToken
    }

    fun getRefreshToken(): String? {
        return refreshToken
    }

    fun setRefreshToken(refreshToken: String) {
        this.refreshToken = refreshToken
    }

    fun getUserId(): String? {
        return userId
    }

    fun setUserId(userId: String) {
        this.userId = userId
    }


}