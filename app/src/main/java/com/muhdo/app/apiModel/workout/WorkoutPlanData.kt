package com.muhdo.app.apiModel.workout

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class WorkoutPlanData {

    @SerializedName("week_days")
    @Expose
    private var weekDays: List<Int>? = null
    @SerializedName("is_deleted")
    @Expose
    private var isDeleted: Boolean? = null
    @SerializedName("_id")
    @Expose
    private var id: Any? = null
    @SerializedName("user_id")
    @Expose
    private var userId: String? = null
    @SerializedName("__v")
    @Expose
    private var v: Int? = null
    @SerializedName("createdAt")
    @Expose
    private var createdAt: String? = null
    @SerializedName("day_count")
    @Expose
    private var dayCount: Int? = null
    @SerializedName("excerises")
    @Expose
    private var excerises: Excerises? = null
    @SerializedName("report_data")
    @Expose
    private var reportData: ReportData? = null
    @SerializedName("updatedAt")
    @Expose
    private var updatedAt: String? = null

    fun getWeekDays(): List<Int>? {
        return weekDays
    }

    fun setWeekDays(weekDays: List<Int>) {
        this.weekDays = weekDays
    }

    fun getIsDeleted(): Boolean? {
        return isDeleted
    }

    fun setIsDeleted(isDeleted: Boolean?) {
        this.isDeleted = isDeleted
    }

    fun getId(): Any? {
        return id
    }

    fun setId(id: Any) {
        this.id = id
    }

    fun getUserId(): String? {
        return userId
    }

    fun setUserId(userId: String) {
        this.userId = userId
    }

    fun getV(): Int? {
        return v
    }

    fun setV(v: Int?) {
        this.v = v
    }

    fun getCreatedAt(): String? {
        return createdAt
    }

    fun setCreatedAt(createdAt: String) {
        this.createdAt = createdAt
    }

    fun getDayCount(): Int? {
        return dayCount
    }

    fun setDayCount(dayCount: Int?) {
        this.dayCount = dayCount
    }

    fun getExcerises(): Excerises? {
        return excerises
    }

    fun setExcerises(excerises: Excerises) {
        this.excerises = excerises
    }

    fun getReportData(): ReportData? {
        return reportData
    }

    fun setReportData(reportData: ReportData) {
        this.reportData = reportData
    }

    fun getUpdatedAt(): String? {
        return updatedAt
    }

    fun setUpdatedAt(updatedAt: String) {
        this.updatedAt = updatedAt
    }

}