package com.muhdo.app.apiModel.mealModel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class Recipe {


    @SerializedName("recipe_id")
    @Expose
    private var recipeId: String? = null
    @SerializedName("day")
    @Expose
    private var day: String? = null

    constructor(recipeId: String?, day: String?) {
        this.recipeId = recipeId
        this.day = day
    }

    fun getRecipeId(): String? {
        return recipeId
    }

    fun setRecipeId(recipeId: String) {
        this.recipeId = recipeId
    }

    fun getDay(): String? {
        return day
    }

    fun setDay(day: String) {
        this.day = day
    }
}