package com.muhdo.app.apiModel.login

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class CountryData {

    @SerializedName("id")
    @Expose
    private var countryId: Int? = null
    @SerializedName("_id")
    @Expose
    private var id: String? = null
    @SerializedName("name")
    @Expose
    private var name: String? = null
    @SerializedName("iso3")
    @Expose
    private var iso3: String? = null
    @SerializedName("iso2")
    @Expose
    private var iso2: String? = null
    @SerializedName("phone_code")
    @Expose
    private var phoneCode: String? = null
    @SerializedName("capital")
    @Expose
    private var capital: String? = null
    @SerializedName("currency")
    @Expose
    private var currency: String? = null

    fun getCountryId(): Int? {
        return countryId
    }

    fun setCountryId(countryId: Int?) {
        this.countryId = countryId
    }


    fun getId(): String? {
        return id
    }

    fun setId(id: String) {
        this.id = id
    }

    fun getName(): String? {
        return name
    }

    fun setName(name: String) {
        this.name = name
    }

    fun getIso3(): String? {
        return iso3
    }

    fun setIso3(iso3: String) {
        this.iso3 = iso3
    }

    fun getIso2(): String? {
        return iso2
    }

    fun setIso2(iso2: String) {
        this.iso2 = iso2
    }

    fun getPhoneCode(): String? {
        return phoneCode
    }

    fun setPhoneCode(phoneCode: String) {
        this.phoneCode = phoneCode
    }

    fun getCapital(): String? {
        return capital
    }

    fun setCapital(capital: String) {
        this.capital = capital
    }

    fun getCurrency(): String? {
        return currency
    }

    fun setCurrency(currency: String) {
        this.currency = currency
    }

}