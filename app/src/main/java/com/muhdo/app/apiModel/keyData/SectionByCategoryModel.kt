package com.muhdo.app.apiModel.keyData

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class SectionByCategoryModel {


    @SerializedName("code")
    @Expose
    private var code: Int? = null
    @SerializedName("message")
    @Expose
    private var message: String? = null
    @SerializedName("data")
    @Expose
    private var data: SectionByCategoryData? = null

    fun getCode(): Int? {
        return code
    }

    fun setCode(code: Int?) {
        this.code = code
    }

    fun getMessage(): String? {
        return message
    }

    fun setMessage(message: String) {
        this.message = message
    }

    fun getData(): SectionByCategoryData? {
        return data
    }

    fun setData(data: SectionByCategoryData) {
        this.data = data
    }

}