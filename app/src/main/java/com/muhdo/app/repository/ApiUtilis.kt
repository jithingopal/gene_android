package com.muhdo.app.repository

import android.content.Context
import com.muhdo.app.utils.PreferenceConnector
import com.muhdo.app.utils.v3.Constants
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit


/**
 *
 * @B ApiUtilis :  This class contain the Base url of server as well as singletone values required.
 **/


class ApiUtilis {
    companion object {

        var BASE_URL = "https://api1.muhdo.com/"
        val httpClient = OkHttpClient.Builder()

        fun getAPIInstance(context: Context): APIService {
            val builder = Retrofit.Builder()
                .baseUrl(
                    PreferenceConnector.readString(context, PreferenceConnector.BASE_URL, BASE_URL)
                        ?: BASE_URL
                ).addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())

            val logging = HttpLoggingInterceptor()
            logging.level = HttpLoggingInterceptor.Level.BODY
            val interceptor = ApiLogsInterceptor()
            httpClient.addInterceptor(logging)
            val client = httpClient.connectTimeout(150, TimeUnit.SECONDS)
                .readTimeout(150, TimeUnit.SECONDS)
                .writeTimeout(150, TimeUnit.SECONDS).build()
            val retrofit = builder.client(client).build()
            return retrofit.create(APIService::class.java)
        }

        fun getAPIService(type: String): APIService {

            when (type) {

                Constants.MEAL_API -> {
                    BASE_URL = "https://ctwileejzj.execute-api.eu-west-2.amazonaws.com/live/dna/"
                }
                Constants.MEAL_QUESTIONNAIRE_API -> {
//                    ***
                    //update meal questionire api
                    //BASE_URL =  "https://tytlki7wni.execute-api.eu-west-2.amazonaws.com/live/dna/"
                    BASE_URL = "https://5yqkp8w5m9.execute-api.eu-west-2.amazonaws.com/live/dna/"


                }

                Constants.MEAL_PLAN_GEN_API -> {
//                    ***
                    //update meal questionire api
                    //BASE_URL =  "https://tytlki7wni.execute-api.eu-west-2.amazonaws.com/live/dna/"
                    BASE_URL = "https://c31pap6eg7.execute-api.eu-west-2.amazonaws.com/live/dna/"
                }
                Constants.API_COUNTRY -> {
                    BASE_URL = "https://lcw6l5o87j.execute-api.eu-west-2.amazonaws.com/live/dna/"
//                    BASE_URL = "https://9qfh5qg168.execute-api.us-east-1.amazonaws.com/dev/dna/"
//                    BASE_URL = "https://restcountries.eu/rest/v2/"
                }


                Constants.KIT_API -> {
                    BASE_URL = "https://bjo15jzqja.execute-api.eu-west-2.amazonaws.com/live/dna/"
                }
                Constants.GENO_TYPE_API -> {
                    BASE_URL = "https://kg5cye1tqj.execute-api.eu-west-2.amazonaws.com/live/"
//                    BASE_URL = "https://ybm50fmq90.execute-api.eu-west-2.amazonaws.com/live/"

                }
                Constants.KEY_DATA_API -> {
                    BASE_URL = "https://ha06m3h8q5.execute-api.eu-west-2.amazonaws.com/live/user/"
                }
                Constants.POLLUTION_API -> {
                    BASE_URL = "https://3lusqief9g.execute-api.eu-west-2.amazonaws.com/live/dna/"
                }
                Constants.GENO_TYPE_SECTION -> {
                    BASE_URL = "https://nxzoezfxoi.execute-api.eu-west-2.amazonaws.com/live/"
                }

                // medication
                Constants.EPIGENTIC_API -> {
                    BASE_URL = "https://2zpt484fu0.execute-api.us-east-1.amazonaws.com/dev/dna/"
                }

                Constants.LIFESTYLE_API -> {
                    BASE_URL = "https://r1n0cma130.execute-api.us-east-1.amazonaws.com/dev/dna/"
                }

                Constants.EXECRISE_API -> {
                    BASE_URL = "https://2zpt484fu0.execute-api.us-east-1.amazonaws.com/dev/dna/"
                }

                Constants.SUPPLEMENT_API -> {
                    BASE_URL = "https://2zpt484fu0.execute-api.us-east-1.amazonaws.com/dev/dna/"
                }


                Constants.FOOD_API -> {
                    BASE_URL = "https://r1n0cma130.execute-api.us-east-1.amazonaws.com/dev/dna/"
                }

                Constants.USER_RAW_DATA_API -> {
                    BASE_URL = "https://fvllq3q4tk.execute-api.us-east-1.amazonaws.com/dev/dna/"
                }

                Constants.VALIDATE_EMAIL -> {
//                    **

                    BASE_URL = "https://n5l2ysqv28.execute-api.eu-west-2.amazonaws.com/live/user/"

                }
                Constants.VALIDATE_EMAIL -> {
//                    **

                    BASE_URL = "https://n5l2ysqv28.execute-api.eu-west-2.amazonaws.com/live/user/"

                }


                /*com.muhdo.app.utils.v3.Constants.MEAL_PLAN_API_V2 -> {
//                    **
                    BASE_URL ="https://wfb2g8wnjc.execute-api.eu-west-2.amazonaws.com/live-v2/"

                }*/
                com.muhdo.app.utils.v3.Constants.BASE_URL_V3 -> {
//                    **
                    BASE_URL = "https://wfb2g8wnjc.execute-api.eu-west-2.amazonaws.com/live-v2/"

                }

                //Please note that the BASE_URL have changed to a common entry point from v3, before that there were multiple base url,
                // and the file is RetrofitHelper

            }

            val builder = Retrofit.Builder()
                .baseUrl(BASE_URL).addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())

            val logging = HttpLoggingInterceptor()
            logging.level = HttpLoggingInterceptor.Level.BODY
//            val interceptor = ApiLogsInterceptor()
            httpClient.addInterceptor(logging)
            val client = httpClient.connectTimeout(150, TimeUnit.SECONDS)
                .readTimeout(150, TimeUnit.SECONDS)
                .writeTimeout(150, TimeUnit.SECONDS).build()
            val retrofit = builder.client(client).build()
            return retrofit.create(APIService::class.java)

        }
    }

}