package com.muhdo.app.repository

import com.google.gson.JsonObject
import com.muhdo.app.apiModel.healthinsights.ResponseHealthInisghts
import com.muhdo.app.apiModel.keyData.*
import com.muhdo.app.apiModel.login.CountryModel
import com.muhdo.app.apiModel.login.KitResponse
import com.muhdo.app.apiModel.login.LostPasswordResponse
import com.muhdo.app.apiModel.login.VerifyOtpModel
import com.muhdo.app.apiModel.login.socialLogin.SocialLoginRequest
import com.muhdo.app.apiModel.mealModel.*
import com.muhdo.app.apiModel.mealModel.daysModel.UpdateFavoriteModel
import com.muhdo.app.apiModel.pollution.AirForecastModel
import com.muhdo.app.apiModel.pollution.AirIndexModel
import com.muhdo.app.apiModel.result.SleepBarChartResponse
import com.muhdo.app.apiModel.result.SleepCalculatorModelResponse
import com.muhdo.app.apiModel.signupSignIn.LoginModule
import com.muhdo.app.apiModel.signupSignIn.SignUpModule
import com.muhdo.app.apiModel.signupSignIn.SignUpRequestModule
import com.muhdo.app.apiModel.v3.*
import com.muhdo.app.apiModel.v3.questionnairestatus.QuestionnaireStatus
import com.muhdo.app.apiModel.v3.workoutplan.ResponseInjuryPrevention
import com.muhdo.app.apiModel.v3.workoutplan.workout_overview.DeletePlanResult
import com.muhdo.app.apiModel.v3.UserIdReq
import com.muhdo.app.apiModel.v3.epigenetics.EpigenHomeResponse
import com.muhdo.app.apiModel.v3.workoutplan.workout_overview.WorkoutOverviewResultPojo
import com.muhdo.app.apiModel.workout.WorkoutFavouriteModel
import com.muhdo.app.apiModel.workout.WorkoutPlanModel
import com.muhdo.app.apiModel.workout.WorkoutQuestionnaireRequest
import com.muhdo.app.ui.epigentic.execrise.response.ExecriseResponseModel
import com.muhdo.app.ui.epigentic.execrise.response.ExerciseListResponse
import com.muhdo.app.ui.epigentic.food.model.FoodDeleteResponse
import com.muhdo.app.ui.epigentic.food.response.AddFoodResponse
import com.muhdo.app.ui.epigentic.food.response.FoodCategoryResponse
import com.muhdo.app.ui.epigentic.food.response.FoodListResponse
import com.muhdo.app.ui.epigentic.lifestyle.response.LifestyleResponseModel
import com.muhdo.app.ui.epigentic.medication.response.AddMedicationRes
import com.muhdo.app.ui.epigentic.medication.response.MedicationListRosponse
import com.muhdo.app.ui.epigentic.medication.response.MedicationTypeResponse
import com.muhdo.app.ui.epigentic.supplyments.response.SupplementResponse
import com.muhdo.app.ui.epigentic.supplyments.response.SupplementTypeResponse
import com.muhdo.app.ui.epigentic.supplyments.response.SupplymentListResponse
import com.muhdo.app.ui.epigenticResult.model.EpigResultResponse
import com.muhdo.app.ui.userrawdata.RawDataResponse
import com.muhdo.app.utils.v3.Constants
import io.reactivex.Observable
import io.reactivex.Single
import retrofit2.Call
import retrofit2.http.*
import java.util.*

interface APIService {

    @POST(Constants.SIGN_IN_API)
    fun login(@Body params: Map<String, String>): Call<JsonObject>


    @POST(Constants.SOCIAL_API)
    fun socialLogin(@Body socialLoginRequest: SocialLoginRequest): Call<LoginModule>

    @POST(Constants.LOST_PASSWORD)
    fun lostPassword(@Body params: Map<String, String>): Call<LostPasswordResponse>

    @GET(Constants.OTP_VERIFY)
    fun verifyOtp(
        @Query("user_id") userID: String,
        @Query("otp_code") otpCode: String
    ): Call<VerifyOtpModel>

    @GET(Constants.KIT_REGISTRATION_API)
    fun getKitStatus(@Path("userID") userID: String): Call<JsonObject>// Call<KitRegistrationResponse>

    @POST(Constants.SIGN_UP_API)
    fun signUp(@Body signUpRequestModule: SignUpRequestModule): Call<SignUpModule>


    @POST(Constants.RESET_PASSWORD)
    fun resetPassword(@Body params: Map<String, String>): Call<VerifyOtpModel>

    @GET(Constants.CALORIE_API)
    fun getCalorieResponse(@Path("userID") userID: String): Observable<CalorieModel>

    //choose meal plan
    @GET(Constants.CHOOSE_MEAL_API)
    fun getChoosePlanList(@Path("userID") userID: String): Observable<ChooseMealModel>

    @PATCH(Constants.CONTINUE_MEAL_PLAN)
    fun continueWithMeal(@Body updateMealPlanRequest: UpdateMealPlanRequest): Observable<ChooseMealSuccess>

    @PATCH(Constants.UPDATE_FAVOURITE_API_RECIPE)
    fun updateFavouriteRecipe(@Body updateFavouriteRequest: UpdateFavouriteRequest): Observable<UpdateFavoriteModel>

    //your meal plan
    @GET(Constants.YOUR_MEAL_PLAN_API)
    fun getMealPlan(@Path("userID") userID: String): Observable<MealPlanModel>

    //get favorite list
    @GET(Constants.FAVOURITE_API)
    fun getFouriteList(@Path("userID") userID: String): Observable<FavoriteListModel>

    //recipe description
    @GET(Constants.RECIPE_API)
    fun getRecipeDescription(@Path("recipeID") recipeID: String): Observable<RecipeDescriptionModel>

    //get favorite list
    @POST(Constants.MEAL_SURVEY)
    fun sendMealSurvey(@Body mealRequest: MealRequest): Observable<AddMealResponse>

    @POST(Constants.MEAL_PLAN_API_V2_SECOND)
    fun sendMealPlanResponse(@Body requestParams: AddMealData): Observable<MealPlanGenResponse>

    @GET(Constants.COUNTRY_API)
    fun getAllCountry(): Observable<CountryModel>

    // workout
    @GET(Constants.WORKOUT_PLAN)
    fun getWorkoutPlan(@Path("user_id") userID: String): Observable<WorkoutPlanModel>

    @POST(Constants.WORKOUT_QUESTIONNAIRE_API)
    fun sendWorkOutSurvey(@Body workoutQuestionnaireRequest: WorkoutQuestionnaireRequest): Observable<UpdateFavoriteModel>


    @POST(Constants.SEND_KIT_API)
    fun sendKit(@Body params: Map<String, String>): Observable<KitResponse>

    @PATCH(Constants.UPDATE_FAVOURITE_API)
    fun updateFavouriteWorkout(@Body params: Map<String, String>): Observable<UpdateFavoriteModel>

    @GET(Constants.WORKOUT_FAVOURITE_API)
    fun getFavouriteWorkout(@Path("userID") userID: String): Observable<WorkoutFavouriteModel>

    //pollution API
    @POST(Constants.AIR_QUALITY_API)
    fun getPollutionData(@Body params: Map<String, String>): Observable<AirIndexModel>

    //pollution API
    @POST(Constants.AIR_QUALITY_API_V2)
    fun getPollutionDataV2(@Body params: Map<String, String>): Observable<AirIndexModel>

    @POST(Constants.FORECAST_API)
    fun getForeCast(@Body params: Map<String, String>): Observable<AirForecastModel>


    // results
    @GET(Constants.GENOTYPE_API)
    fun getGenoType(): Observable<GenoTypeModel>

    @GET(Constants.API_KEYDATA)
    fun getKeyData(
        @Query("user_id") userID: String,
        @Query("mode_id") modeId: String,
        @Query("keydata") keydata: String,
        @Query("mobile") mobile: String
    ): Observable<KeyDataModel>

    @GET(Constants.API_KEYDATA)
    fun getKeyDataByCategory(
        @Query("user_id") userID: String,
        @Query("mode_id") modeId: String,
        @Query("keydata") keydata: String,
        @Query("mobile") mobile: String,
        @Query("category_id") categoryId: String,
        @Query("section_id") sectionId: String
    ): Observable<SectionByCategoryModel>


    @GET(Constants.GENOTYPE_SECTION)
    fun getGenoTypeSection(): Observable<GenoTypeSectionModel>

    @GET(Constants.GENERIC_API)
    fun getGenericProfile(@Path("sectionID") userID: String): Observable<GenericProfileModel>

//
//    @GET(Constants.API_KEYDATA)
//    fun getSectionData(
//        @Query("user_id") userID: String,
//        @Query("mode_id") modeId: String,
//        @Query("mobile") mobile: String,
//        @Query("section_id") sectionId: String): Observable<SectionDataModel>

    @POST(Constants.USER_EMAIL_VALIDATION)
    fun validateEmail(@Body params: Map<String, String>): Observable<VerifyOtpModel>


    @GET(Constants.MEDICATION_API)
    fun getMedicationList(@Path("user_id") userID: String): Observable<MedicationListRosponse>

    // get all medication type

    @GET(Constants.MEDICATION_TYPE_API)
    fun getAllMedicationType(): Observable<MedicationTypeResponse>


    @POST(Constants.ADD_MEDICATION)
    fun addMedication(@Body params: Map<String, String>): Observable<AddMedicationRes>

    @POST(Constants.ADD_LIFESTYLE)
    fun addLifestyle(@Body params: Map<String, String>): Observable<LifestyleResponseModel>


    @POST(Constants.ADD_EXECRISE)
    fun sendExecriseData(@Body params: Map<String, String>): Observable<ExecriseResponseModel>

    @PUT(Constants.UPDATE_EXECRISE)
    fun updateExecriseData(@Body params: Map<String, String>): Observable<ExecriseResponseModel>

    // Get user exercise data from server
    @GET(Constants.GET_EXERCISE_DATA)
    fun getExerciseData(@Path("user_id") userID: String): Observable<ExerciseListResponse>

    @DELETE(Constants.DELETE_EXERCISE)
    fun deleteExercise(@Path("id") exerciseId: String): Observable<ExecriseResponseModel>

    // Get user data for supplement
    @GET(Constants.GET_SUPPLEMENT_DATA)
    fun getSupplementData(@Path("user_id") userID: String): Observable<SupplymentListResponse>

    // Get all supplement type data
    @GET(Constants.SUPPLEMENT_TYPE_API)
    fun getAllSupplementType(): Observable<SupplementTypeResponse>


    // Send supplement type data
    @POST(Constants.ADD_SUPPLEMENT_DATA)
    fun sendSupplementData(@Body params: HashMap<String, String>): Observable<SupplementResponse>

    @DELETE(Constants.DELETE_MEDICATION)
    fun deleteMedication(@Path("id") medicationId: String): Observable<AddMedicationRes>

    @DELETE(Constants.DELETE_SUPPLEMENT)
    fun deleteSupplement(@Path("id") supplementId: String): Observable<AddMedicationRes>

    //Get food data for user
    // Get user data for supplement
    @GET(Constants.GET_FOOD_DATA)
    fun getFoodData(@Path("user_id") userID: String): Observable<FoodListResponse>


    @DELETE(Constants.DELETE_FOOD)
    fun deleteFood(@Path("id") exerciseId: String): Observable<FoodDeleteResponse>

    // Get food group data
    @GET(Constants.GET_FOOD_GROUP_DATA)
    fun getFoodGroupData(): Observable<FoodCategoryResponse>

    @POST(Constants.ADD_FOOD)
    fun sendFoodData(@Body params: Map<String, String>): Observable<AddFoodResponse>


    @PUT(Constants.UPDATE_FOOD)
    fun updateFood(@Body params: Map<String, String>): Observable<FoodDeleteResponse>

    // Get user raw data
    @GET(Constants.GET_USER_RAW_DATA)
    fun getUserRawData(@Path("user_id") userID: String): Observable<RawDataResponse>

    // Get Sleep Algorithm Calculates Score
    @GET(Constants.SLEEP_ALGO_CALCULATES_SCORE)
    fun getSleepAlgoCalculatesScore(@Path("user_id") userID: String): Observable<SleepCalculatorModelResponse>

    // Get bar chart data for sleep index
    @GET(Constants.SLEEP_INDEX_BAR_CHART)
    fun getBarChartDataForSleepIndex(@Path("user_id") userID: String): Observable<SleepBarChartResponse>

    // Get eig result data
    @GET(Constants.EPIGENTIC_RESULT_DATA)
    fun getEpigenticResultData(@Path("kit_id") kitId: String): Observable<EpigResultResponse>

    // Get eig result data
    @POST(Constants.EPIGEN_HOME)
    fun getEpigenticResultDataV3(@Body epigenBaseRequest: EpigenBaseReq): Observable<EpigResultResponse>

    // Get epigenetic result data for particular KITID
    @POST(com.muhdo.app.utils.v3.Constants.EPIGEN_RESULT)
    fun getEpigenticResultDataForSingleKitIdV3(@Body epigenBaseRequest: EpigenBaseReq): Observable<EpigResultResponse>

    // Get epigen data for  result data
    @POST(com.muhdo.app.utils.v3.Constants.EPIGEN_RESULT)
    fun getEpigenticResultDataForHomeV3(@Body userIdRequest: UserIdReq): Observable<EpigenHomeResponse>


    //v3 apis
    //your meal plan
    @POST(Constants.GET_USER)
    fun getUserDetails(@Body condition: GetUserConditionReq): Call<UserDataPojo>

    @POST(Constants.UPDATE_USER_POST)
    fun postUserDetails(@Body userDetails: UpdateUserData): Single<BaseResponse>

    @POST(Constants.UPDATE_USER_POST)
    fun postUserprofileDetails(@Body userDetails: UpdateUserProfileReq): Single<BaseResponse>

    @POST(Constants.READ_GENETIC_OVERVIEW_POST)
    fun postGeneticOverviewList(@Body params: GeneticOverviewReq): Call<GeneticOverviewResultPojo>

    @POST(Constants.QUESTIONNAIRE_ANSWERS_POST)
    fun postReviewQuestionnaireAnswers(@Body params: QuestionnaireAnswer): Single<BaseResponse>

    @GET(Constants.GET_QUESTIONNAIRE_STATUS)
    fun getQuestionnaireStatus(@Path("user_id") userID: String): Single<QuestionnaireStatus>

    @POST(Constants.READ_GENETIC_ACTION_PLAN_POST)
    fun postGeneticActionPlanList(@Body params: GeneticActionPlanReq): Call<GeneticActionPlanResultPojo>

    // results
    @GET(Constants.GENO_TYPE_API)
    fun getGenoTypeNewApi(): Observable<GenoTypeModel>

    @POST(Constants.HEALTH_INSIGHTS_API)
    fun getDataForHealthInsights(@Body requestBody: JsonObject): Single<ResponseHealthInisghts>

    @POST(Constants.WORKOUT_QUESTIONNAIRE_API_V3)
    fun sendWorkOutSurveyV3(@Body workoutTrainingRequest: WorkoutTrainingRequest): Observable<UpdateFavoriteModel>

    //get favorite list
    @POST(Constants.MEAL_QUESTIONNAIRE_API_V3)
    fun sendMealSurveyV3(@Body mealRequest: MealGuideSurveyRequest): Observable<AddMealResponse>

    //choose meal plan
    @GET(Constants.CHOOSE_MEAL_API_V3)
    fun getChoosePlanListV3(@Path("userID") userID: String): Observable<ChooseMealModel>

    @POST(Constants.WORKOUT_INJURY_PREVENTION)
    fun getWorkoutInjuryPrevention(@Body requestBody: JsonObject): Observable<ResponseInjuryPrevention>

    @POST(Constants.WORKOUT_OVERVIEW)
    fun getWorkoutOverview(@Body requestBody: UserIdReq): Call<WorkoutOverviewResultPojo>

    @GET(Constants.MEAL_PLAN_API_V2)
    fun getMealPlanV3(@Path("user_id") userID: String): Observable<MealPlanModel>

    @POST(Constants.DELETE_WORKOUT_PLAN)
    fun deleteWorkoutPlan(@Body requestBody: UserIdReq): Call<DeletePlanResult>

    @POST(Constants.DELETE_MEAL_PLAN)
    fun deleteMealPlan(@Body requestBody: UserIdReq): Call<DeletePlanResult>

    @POST(Constants.KIT_ID_INFO)
    fun fetchAllKitIdInfo(@Body requestBody: UserIdReq): Call<KitIdInfoResult>

    @POST(Constants.ADD_CHILD_KIT_ID)
    fun addChildKit(@Body requestBody: AddChildKitIdRequest): Call<BaseResponse>

    @POST(Constants.ADD_CHILD_KIT_ID)
    fun addChildKitID(@Body requestBody: AddChildKitIdRequest): Call<BaseResponse>

}


