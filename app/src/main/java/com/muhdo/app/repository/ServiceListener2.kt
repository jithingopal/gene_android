package com.muhdo.app.repository

import com.muhdo.app.apiModel.login.LoginErrorModel

/**
 * it contain the common method  @getServerResponse ,getError for handling the api response of api.
 */
interface ServiceListener2<T> {
    abstract fun getServerResponse(response: T, requestcode: Int)
    abstract fun getError(error: LoginErrorModel, requestcode: Int)
}