package com.muhdo.app.repository

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit


object ApiClientNew {


    //    val BASE_URL = "https://mw65yqiwkk.execute-api.us-east-1.amazonaws.com/dev/auth/"
    val BASE_URL = "https://xk33klorh6.execute-api.eu-west-2.amazonaws.com/live/auth/"
    private var retrofit: Retrofit? = null
    private val httpClient = OkHttpClient.Builder()
    //    https://mw65yqiwkk.execute-api.us-east-1.amazonaws.com/dev/auth/activatedkit/get/fgjgdslf
    fun getClient(): Retrofit? {


        val builder = Retrofit.Builder()
            .baseUrl("https://xk33klorh6.execute-api.eu-west-2.amazonaws.com/live/auth/")
            .addConverterFactory(GsonConverterFactory.create())

        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BODY
//            val interceptor = ApiLogsInterceptor()
        httpClient.addInterceptor(logging)
        val client = ApiUtilis.httpClient.connectTimeout(150, TimeUnit.SECONDS)
            .readTimeout(150, TimeUnit.SECONDS)
            .writeTimeout(150, TimeUnit.SECONDS).build()
        val retrofit = builder.client(client).build()
        return retrofit


//        if (retrofit == null) {
//            retrofit = Retrofit.Builder()
//                .baseUrl(BASE_URL)
//                .addConverterFactory(GsonConverterFactory.create())
//                .build()
//        }
//        return retrofit
    }


}