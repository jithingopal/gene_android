package com.muhdo.app.model

data class HomeItem(val imageUrl: Int, val label: String)
