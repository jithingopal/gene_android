package com.muhdo.app.model

class ResultGridModel(name: String, image: Int) {
    var name: String? = name
    var image: Int? = image
}