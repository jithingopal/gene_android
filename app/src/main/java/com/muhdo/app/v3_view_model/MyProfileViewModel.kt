package com.muhdo.app.v3_view_model

import android.content.Context
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.lifecycle.ViewModel


class MyProfileViewModel : ViewModel() {



    fun hideKeyboard(view1: View) {
        val imm =
            view1.context!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager?
        imm!!.hideSoftInputFromWindow(view1.windowToken, 0)

    }
}